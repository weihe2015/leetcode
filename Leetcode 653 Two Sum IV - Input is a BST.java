/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
  /*
Given a Binary Search Tree and a target number, return true if there exist two elements in the BST such that their sum is equal to the given target.

Example 1:
Input:
    5
   / \
  3   6
 / \   \
2   4   7

Target = 9

Output: True
Example 2:
Input:
    5
   / \
  3   6
 / \   \
2   4   7

Target = 28

Output: False
*/
class Solution {
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public boolean findTarget(TreeNode root, int k) {
        Set<Integer> set = new HashSet<Integer>();
        return findTarget(set, root, k);
    }
    // Running time: O(n), Space: O(n) * logN for recursion space complexity
    private boolean findTarget(Set<Integer> set, TreeNode node, int k) {
        if (node == null) {
            return false;
        }
        int diff = k - node.val;
        if (set.contains(diff)) {
            return true;
        }
        set.add(node.val);
        return findTarget(set, node.left, k) || findTarget(set, node.right, k);
    }

    // Solution 2: Running Time Complexity: O(Nh), where h is the height of the tree. Space Complexity: O(N) for recursion.
    public boolean findTarget(TreeNode root, int k) {
        return findTarget(root, root, k);
    }

    private boolean findTarget(TreeNode baseNode, TreeNode currNode, int k) {
        if (baseNode == null) {
            return false;
        }
        return findTarget(baseNode.left, currNode, k) || findTarget(baseNode.right, currNode, k) ||
            find(baseNode, currNode, k-baseNode.val);
    }

    private boolean find(TreeNode baseNode, TreeNode currNode, int diff) {
        if (currNode == null) {
            return false;
        }
        else if (currNode != baseNode) {
            if (currNode.val == diff) {
                return true;
            }
            else if (currNode.val < diff) {
                return find(baseNode, currNode.right, diff);
            }
            else {
                return find(baseNode, currNode.left, diff);
            }
        }
        else {
            return false;
        }
    }

    // Solution 3: Use inorder traversal to get all nums into a list, and use normal two sum solution to solve it.
    public boolean findTarget(TreeNode root, int k) {
        List<Integer> nums = new ArrayList<Integer>();
        // Inorder Traversal: left, val, right
        Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
        TreeNode currNode = root;
        while (!stack.isEmpty() || currNode != null) {
            while (currNode != null) {
                stack.push(currNode);
                currNode = currNode.left;
            }
            currNode = stack.pop();
            nums.add(currNode.val);
            currNode = currNode.right;
        }

        int low = 0, high = nums.size() - 1;
        while (low < high) {
            int sum = nums.get(low) + nums.get(high);
            if (sum == k) {
                return true;
            }
            else if (sum < k) {
                low++;
            }
            else {
                high--;
            }
        }
        return false;
    }
}