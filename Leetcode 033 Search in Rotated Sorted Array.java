/**
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.

Your algorithm's runtime complexity must be in the order of O(log n).

Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1
 */
public class Solution {
    /**
     * My solution on 6-17-18
     * So when doing binary search, we can make a judgement
     *  that which part is ordered and whether the target is in that range,
     * if yes, continue the search in that half, if not continue in the other half.
    */
    public int search(int[] nums, int target) {
        int n = nums.length;
        if (n == 0) {
            return -1;
        }
        int low = 0;
        int high = n - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            // check if right side subarray is sorted or not.
            if (nums[mid] < nums[high]) {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
            // check if left side subarray is sorted or not.
            else {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
        }
        return -1;
    }

    // Another solution:
    public int search(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        int res = -1;
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                res = mid;
                break;
            }
            // If left array is in order: It needs to have <=, but not <
            // Without = sign, the test case will failed: nums=[3,1], target = 1
            if (nums[low] <= nums[mid]) {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
            // If right array is in order:
            else {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
        }
        return res;
    }

    // solution 1
    public int search(int[] nums, int target) {
        return search(nums, 0, nums.length-1, target);
    }

    public int search(int[] nums, int low, int high, int target){
        if (low > high) {
            return -1;
        }
        int mid = low + (high + low)/2;
        if (nums[mid] == target) {
            return mid;
        }
        // Right half sorted
        if (nums[mid] < nums[high]) {
            if (nums[mid] < target && target <= nums[high]) {
                return search(nums, mid+1, high, target);
            }
            else {
                return search(nums, low, mid-1, target);
            }
        }
        // left half sorted
        else {
            if (nums[low] <= target && target < nums[mid]) {
                return search(nums, low, mid-1, target);
            }
            else {
                return search(nums, mid+1, high, target);
            }
        }
    }

    // solution 2
    /*
    We cannot do this because for the case of [3,1]
    */
    public int search(int[] nums, int target) {
        int n = nums.length;
        if (n == 0) {
            return -1;
        }
        int low = 0;
        int high = n-1;

        while (low <= high) {
            int mid = low + (high - low)/2;
            if (nums[mid] == target) {
                return mid;
            }
            // left part is in correct order.
            if (nums[low] <= nums[mid]) {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
            else {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
        }
        return -1;
    }
}