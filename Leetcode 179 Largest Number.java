Given a list of non negative integers, arrange them such that they form the largest number.

For example, given [3, 30, 34, 5, 9], the largest formed number is 9534330.

Note: The result may be very large, so you need to return a string instead of an integer.

public class Solution {
    public String largestNumber(int[] nums) {
        if(nums == null || nums.length == 0)
            return "";
        String[] str = new String[nums.length];
        for(int i = 0; i < nums.length; i++)
            str[i] = nums[i] + "";
        
        Comparator<String> comp = new Comparator<String>(){
            public int compare(String str1, String str2){
                String s1 = str1+str2;
                String s2 = str2+str1;
                return s1.compareTo(s2);
            }
        };
        
        Arrays.sort(str,comp);
        if(str[str.length-1].charAt(0) == '0')
            return "0";
        StringBuilder sb = new StringBuilder();
        for(String s : str)
            sb.insert(0,s);
        return sb.toString();
    }
}