/**
Convert a BST to a sorted circular doubly-linked list in-place. Think of the left and right pointers 
as synonymous to the previous and next pointers in a doubly-linked list.

Let's take the following BST as an example, it may help you understand the problem better:


We want to transform this BST into a circular doubly linked list. Each node in a doubly linked list has a predecessor and successor. For a circular doubly linked list, the predecessor of the first element is the last element, and the successor of the last element is the first element.

The figure below shows the circular doubly linked list for the BST above. 
The "head" symbol means the node it points to is the smallest element of the linked list.

Specifically, we want to do the transformation in place. After the transformation, 
the left pointer of the tree node should point to its predecessor, and the right pointer should point to its successor. 
We should return the pointer to the first element of the linked list.

The figure below shows the transformed BST. The solid line indicates the successor relationship, 
while the dashed line means the predecessor relationship.
*/

// https://leetcode.com/problems/convert-binary-search-tree-to-sorted-doubly-linked-list
// https://www.lintcode.com/problem/convert-binary-search-tree-to-sorted-doubly-linked-list

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */
public class Solution {

    // Recursive:
    private TreeNode head;
    private TreeNode prev;
    public TreeNode treeToDoublyList(TreeNode root) {
        // Write your code here.
        if (root == null) {
            return null;
        }
        head = null;
        prev = null;
        inorderTraversal(root);
        
        prev.right = head;
        head.left = prev;
        
        return head;
    }
    
    private void inorderTraversal(TreeNode node) {
        if (node == null) {
            return;
        }
        inorderTraversal(node.left);
        if (head == null) {
            head = node;
            prev = node;
        }
        else {
            prev.right = node;
            node.left = prev;
            prev = node;
        }
        inorderTraversal(node.right);
    }

    // Iterative:
    public TreeNode treeToDoublyList(TreeNode root) {
        // Write your code here.
        if (root == null) {
            return null;
        }
        TreeNode head = null;
        TreeNode prev = null;
        
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = root;
        while (!stack.isEmpty() || curr != null) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if (head == null) {
                head = curr;
                prev = curr;
            }
            else {
                prev.right = curr;
                curr.left = prev;
                prev = curr;
            }
            curr = curr.right;
        }

        head.left = prev;
        prev.right = head;
        return head;
    }
}