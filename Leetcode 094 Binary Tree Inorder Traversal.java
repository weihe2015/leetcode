/**
Given a binary tree, return the inorder traversal of its nodes values.

For example:
Given binary tree {1,#,2,3},
   1
    \
     2
    /
   3
return [1,3,2].

Note: Recursive solution is trivial, could you do it iteratively?
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Iterative:
    /**
     * Inorder Traversal: left child, curr node, right child
     * Basic Idea, 
     *  1. From root node, add it to the stack and move to left child, until it reaches the leaf
     *  2. Then from the top TreeNode of the stack, add it into result, move the currNode to its right child
    */
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node = root;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.pop();
            result.add(currNode.val);
            // Move node to currNode.right to continue.
            node = currNode.right;
        }
        return result;
    }

    // Recursive:
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        inorderTraversal(result, root);
        return result;
    }
    
    private void inorderTraversal(List<Integer> result, TreeNode node) {
        if (node == null) {
            return;
        }
        inorderTraversal(result, node.left);
        result.add(node.val);
        inorderTraversal(result, node.right);
    } 

}