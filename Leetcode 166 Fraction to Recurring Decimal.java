/**
Given two integers representing the numerator and denominator of a fraction, return the fraction in string format.

If the fractional part is repeating, enclose the repeating part in parentheses.

If multiple answers are possible, return any of them.

It is guaranteed that the length of the answer string is less than 104 for all the given inputs.

Example 1:

Input: numerator = 1, denominator = 2
Output: "0.5"
Example 2:

Input: numerator = 2, denominator = 1
Output: "2"
Example 3:

Input: numerator = 2, denominator = 3
Output: "0.(6)"
Example 4:

Input: numerator = 4, denominator = 333
Output: "0.(012)"
Example 5:

Input: numerator = 1, denominator = 5
Output: "0.2"
*/

public class Solution {
    public String fractionToDecimal(int numerator, int denominator) {
        if (numerator == 0) {
            return "0";
        }
        else if (denominator == 0) {
            return "NaN";
        }
        
        long n = Long.valueOf(numerator);
        long d = Long.valueOf(denominator);
        
        StringBuffer sb = new StringBuffer();
        // if result is negative, we append - sign to the string
        if (n * d < 0) {
            sb.append('-');
        }
        
        n = Math.abs(n);
        d = Math.abs(d);
        
        sb.append(String.valueOf(n/d));
        // remainder of n / d
        long r = n % d;
        if (r == 0) {
            return sb.toString();
        }
        
        sb.append('.');
        // find pattern:
        // deal with the float part, 
        // key is the remainder, value is the start positon of possible repeat numbers
        Map<Long, Integer> map = new HashMap<>();
        while (r > 0) {
            if (map.containsKey(r)) {
                int idx = map.get(r);
                sb.insert(idx, '(');
                sb.append(')');
                break;
            }
            map.put(r, sb.length());
            r *= 10;
            sb.append(String.valueOf(r / d));
            r %= d;
        }
        
        return sb.toString();
    }
}