/**
Given an integer array nums, find the contiguous subarray within an array
(containing at least one number) which has the largest product.

Example 1:

Input: [2,3,-2,4]
Output: 6
Explanation: [2,3] has the largest product 6.
Example 2:

Input: [-2,0,-1]
Output: 0
Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
*/
public class Solution {
    public int maxProduct(int[] nums) {
        int n = nums.length;
        int maxProd = nums[0];
        int minProd = nums[0];
        int res = nums[0];

        for (int i = 1; i < n; i++) {
            if (nums[i] >= 0) {
                maxProd = Math.max(maxProd * nums[i], nums[i]);
                minProd = Math.min(minProd * nums[i], nums[i]);
            }
            else {
                int prev = maxProd;
                maxProd = Math.max(minProd * nums[i], nums[i]);
                minProd = Math.min(prev * nums[i], nums[i]);
            }
            res = Math.max(res, maxProd);
        }
        return res;
    }

    public int maxProduct(int[] nums) {
        int n = nums.length;
        // store the result that is the max we have found so far
        // imax/imin stores the max/min product of
        // subarray that ends with the current number A[i]
        int maxProduct = nums[0], minProduct = maxProduct, maxResult = maxProduct;
        for (int i = 1; i < n; i++) {
            if (nums[i] < 0) {
                // swap min and max
                // multiplied by a negative makes big number smaller, small number bigger
                // so we redefine the extremums by swapping them
                int temp = maxProduct;
                maxProduct = minProduct;
                minProduct = temp;
            }
            // max/min product for the current number is either the current number itself
            // or the max/min by the previous number times the current one
            maxProduct = Math.max(maxProduct * nums[i], nums[i]);
            minProduct = Math.min(minProduct * nums[i], nums[i]);
            // the newly computed max value is a candidate for our global result
            maxResult = Math.max(maxResult, maxProduct);
        }
        return maxResult;
    }
}