/**
There are N network nodes, labelled 1 to N.

Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the target node, and w is the time it takes for a signal to travel from source to target.

Now, we send a signal from a certain node K. How long will it take for all nodes to receive the signal? If it is impossible, return -1.

Ex: 1

// time[0]: from, time[1]: to, time[2]: cost
Input: times = [[2,1,1],[2,3,1],[3,4,1]], N = 4, K = 2
Output: 2

Note:

N will be in the range [1, 100].
K will be in the range [1, N].
The length of times will be in the range [1, 6000].
All edges times[i] = (u, v, w) will have 1 <= u, v <= N and 0 <= w <= 100.
*/

public class Solution {
    // key -> nodeId, val -> dist from node k to itself
    private Map<Integer, Integer> distMap;
    // DFS:
    // Time Complexity: O(N^N + ElogE), where E is the length of int[] times
    // Space Complexity: O(N + E), graph size is O(E) + the DFS stack depth O(N)
    public int networkDelayTime(int[][] times, int N, int K) {
        // build graph:
        Map<Integer, List<int[]>> graph = new HashMap<>();
        for (int[] time : times) {
            int from = time[0];
            int to = time[1];
            int cost = time[2];
            List<int[]> children = graph.getOrDefault(from, new ArrayList<>());
            children.add(new int[]{to, cost});
            graph.put(from, children);
        }
        for (int key : graph.keySet()) {
            List<int[]> list = graph.get(key);
            // sort by the cost in the children
            Collections.sort(list, Comparator.comparingInt(l -> l[1]));
        }
        this.distMap = new HashMap<>();
        for (int i = 1; i <= N; i++) {
            distMap.put(i, Integer.MAX_VALUE);
        }

        dfs(graph, K, 0);

        int maxVal = 0;
        for (int val : distMap.values()) {
            if (val == Integer.MAX_VALUE) {
                return -1;
            }
            maxVal = Math.max(maxVal, val);
        }
        return maxVal;
    }

    private void dfs(Map<Integer, List<int[]>> graph, int from, int cost) {
        if (distMap.get(from) <= cost) {
            return;
        }
        distMap.put(from, cost);
        if (!graph.containsKey(from)) {
            return;
        }
        List<int[]> children = graph.get(from);
        for (int[] child : children) {
            dfs(graph, child[0], cost + child[1]);
        }
    }

    // Dijkstra's Algorithm:
    public int networkDelayTime(int[][] times, int N, int K) {
        // build graph:
        Map<Integer, List<int[]>> graph = new HashMap<>();
        for (int[] time : times) {
            int from = time[0];
            int to = time[1];
            int cost = time[2];
            List<int[]> children = graph.getOrDefault(from, new ArrayList<>());
            children.add(new int[]{to, cost});
            graph.put(from, children);
        }
        // check whether each node has the min dist or not.
        boolean[] visited = new boolean[N+1];
        // distance from node K to node i
        int[] dist = new int[N+1];
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[K] = 0;

        Queue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(l -> l[1]));
        pq.offer(new int[]{K, 0});

        while (!pq.isEmpty()) {
            int[] info = pq.poll();
            int u    = info[0];
            int cost = info[1];
            if (visited[from] || !graph.containsKey(u)) {
                continue;
            }
            visited[u] = true;
            for (int[] child : graph.get(from)) {
                int v = child[0];
                int w = child[1];
                if (dist[v] > cost + w) {
                    dist[v] = cost + w;
                    pq.offer(new int[]{v, dist[v]});
                }
            }
        }

        int maxVal = 0;
        dist[0] = 0;
        for (int val : dist) {
            if (val == Integer.MAX_VALUE) {
                return -1;
            }
            maxVal = Math.max(maxVal, val);
        }
        return maxVal;
    }
}