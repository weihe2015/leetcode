/**
There are a total of n courses you have to take, labeled from 0 to n-1.

Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?

Example 1:

Input: 2, [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take.
             To take course 1 you should have finished course 0. So it is possible.
Example 2:

Input: 2, [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take.
             To take course 1 you should have finished course 0, and to take course 0 you should
             also have finished course 1. So it is impossible.
Note:

The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a graph is represented.
You may assume that there are no duplicate edges in the input prerequisites.
*/
public class Solution {
    /**
    Kahn's algorithm for Topological Sorting:
    A DAG has at least one vertex with in-degree 0 and one vertex with out-degree 0
        Otherwise, it contains a cycle
    1. Compute in-degree (number of incoming edges) for each of the vertex present in DAG
       and intialize the count of visited nodes as 0
    2. Pick all the vertices with in-degree as 0 and add them into a queue
    3. Remove a vertex from the queue and then.
       3.1 Increment count of visited nodes by 1
       3.2 Decrease in-degree by 1 for all its neighboring nodes
       3.3 If in-degree of a neighboring nodes is reduced to zero, then add it to queue
    4. Repeat step 3 until queue is empty
    5. If count of visited nodes is not equals to the number of nodes in the graph:
       then it has cycle, topological sort is not possible for given graph.
    */
    // adjacent matrice solution
    // Running Time Complexity: O(|V| + |E|)
    // BFS:
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        // Build Graph:
        int[][] graph = new int[numCourses][numCourses];
        int[] indegree = new int[numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph[from][to] = 1;
            // We assume that there is no duplicate edges
            indegree[to]++;
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        // Add all vertex with in-degree = 0 into queue, these vertex are the source vertex
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int count = 0;
        while (!queue.isEmpty()) {
            int from = queue.poll();
            count++;
            // Explore all neighbors:
            for (int i = 0; i < numCourses; i++) {
                if (graph[from][i] == 1) {
                    indegree[i]--;
                    if (indegree[i] == 0) {
                        queue.offer(i);
                    }
                }
            }
        }
        return count == numCourses;
    }

    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int i = 0; i < numCourses; i++) {
            graph.put(i, new ArrayList<>());
        }
        int[] indegree = new int[numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to   = prerequisite[0];
            graph.get(from).add(to);
            indegree[to]++;
        }
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int cnt = 0;
        while (!queue.isEmpty()) {
            int u = queue.poll();
            cnt++;
            for (int v : graph.get(u)) {
                indegree[v]--;
                if (indegree[v] == 0) {
                    queue.offer(v);
                }
            }
        }
        return cnt == numCourses;
    }

    // DFS: Use DFS to detect whether there is a cycle
    // Adjacent List:
    // https://www.youtube.com/watch?v=joqmqvHC_Bo
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        // Build Graph:
        int[][] graph = new int[numCourses][numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph[from][to] = 1;
        }
        boolean[] visited = new boolean[numCourses];
        boolean[] recStack = new boolean[numCourses];
        for (int i = 0; i < numCourses; i++) {
            if (isCirclic(graph, visited, recStack, i, numCourses)) {
                return false;
            }
        }
        return true;
    }

    private boolean isCirclic(int[][] graph, boolean[] visited, boolean[] recStack, int from, int numCourses) {
        if (!visited[from]) {
            visited[from] = true;
            recStack[from] = true;
            for (int i = 0; i < numCourses; i++) {
                if (graph[from][i] == 0) {
                    continue;
                }
                if (!visited[i] && isCyclic(graph, visited, recStack, i, numCourses)) {
                    return true;
                }
                else if (recStack[i]) {
                    return true;
                }
            }
        }
        recStack[from] = false;
        return false;
    }

    // Another solution of DFS:
    // Running Time Complexity: O(|V| + |E|), Space Complexity: O(V)
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < numCourses; i++) {
            graph.add(new ArrayList<Integer>());
        }
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph.get(from).add(to);
        }
        int[] visited = new int[numCourses];
        for (int i = 0; i < numCourses; i++) {
            if (isCirclic(graph, visited, i)) {
                return false;
            }
        }
        return true;
    }
    /**
        0 -> not yet visited
        1 -> is visiting:
        2 -> already visited:
    */
    private boolean isCyclic(List<List<Integer>> graph, int[] visited, int u) {
        if (visited[u] == 0) {
            visited[u] = 1;
            List<Integer> neighbors = graph.get(u);
            for (int v : neighbors) {
                if (isCyclic(graph, visited, v)) {
                    return true;
                }
            }
            visited[u] = 2;
            return false;
        }
        else if (visited[u] == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean isCirclic(int[][] graph, int[] visited, int from) {
        if (visited[from] == 2) {
            return false;
        }
        else if (visited[from] == 1) {
            return true;
        }
        else {
            visited[from] = 1;
            for (int i = 0; i < numCourses; i++) {
                if (graph[from][i] == 0) {
                    continue;
                }
                if (isCyclic(graph, visited, neighbor)) {
                    return true;
                }
            }
            visited[from] = 2;
            return false;
        }
    }
}