/*    
Given a set of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.

The same repeated number may be chosen from C unlimited number of times.

Note:
All numbers (including target) will be positive integers.
Elements in a combination (a1, a2, … , ak) must be in non-descending order. (ie, a1 ≤ a2 ≤ … ≤ ak).
The solution set must not contain duplicate combinations.
For example, given candidate set 2,3,6,7 and target 7, 
A solution set is: 
[7] 
[2, 2, 3] 
*/
public class Solution {
    // Running Time Complexity: O(size_of_array^target)
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> list = new ArrayList<>();

        Arrays.sort(candidates);
        int n = candidates.length;
        combinationSum(result, candidates, list, target, 0, 0, n);
        return result;
    }
    
    private void combinationSum(List<List<Integer>> result, int[] candidates, List<Integer> list, int target, int level, int sum, int n) {
        if (sum > target) {
            return;
        }
        if (target == 0) {
            result.add(new ArrayList<>(list));
            return;
        }
        
        for (int i = level; i < n; ++i) {
            int val = candidates[i];
            list.add(val);
            combinationSum(result, candidates, list, target, i, sum+val, n);
            list.remove(list.size()-1);
        }
    }
}