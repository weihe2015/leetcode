/**
You are climbing a stair case. It takes n steps to reach to the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

Note: Given n will be a positive integer.

Example 1:

Input: 2
Output: 2
Explanation: There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps
Example 2:

Input: 3
Output: 3
Explanation: There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step
*/

public class Solution {
    // DP solution, Time Complexity: O(N), Space Complexity: O(N)
    public int climbStairs(int n) {
        if (n < 0) {
            return 0;
        }
        else if (n <= 2) {
            return n;
        }
        int[] dp = new int[n];
        dp[0] = 1; 
        dp[1] = 2;
        for (int i = 2; i < n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n-1];
    }

    // DP solution, Time Complexity: O(N), Space Complexity: O(1)
    public int climbStairs(int n) {
        if (n < 0) {
            return 0;
        }
        else if (n <= 2) {
            return n;
        }
        int dp_0 = 1;
        int dp_1 = 2;
        for (int i = 2; i < n; i++) {
            int dp_i = dp_0 + dp_1;
            dp_0 = dp_1;
            dp_1 = dp_i;
        }
        return dp_1;
    }

    // solution 3
    // a: ways to current step
    // b: ways to next step
    public int climbStairs(int n) {
        int a = 1, b = 1;
        while (n > 0){
            b += a;
            a = b - a;
            n--;
        }
        return a;
    }
    // use math formula
    public int climbStairs(int n){
        n++;
        double phi = (1 + Math.pow(5,0.5)) / 2;
        return Math.round(phi,n) / Math.pow(5,0.5);
    }
}