
public class Solution {
    class Node {
        public int val;
        public List<Node> children;

        public Node() {}

        public Node(int _val,List<Node> _children) {
            val = _val;
            children = _children;
        }
    };

    // Recursive:
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }
        int max = 0;
        for (Node childNode : root.children) {
            int depth = maxDepth(childNode);
            if (depth > max) {
                max = depth;
            }
        }
        return max+1;
    }

    // Iterative Solution: Use Level Order Traversal:
    public int maxDepth(Node root) {
        int depth = 0;
        if (root == null) {
            return depth;
        }
        Queue<Node> queue = new LinkedList<Node>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            depth++;
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                Node currNode = queue.poll();
                for (Node childNode : currNode.children) {
                    queue.offer(childNode);
                }
            }
        }
        return depth;
    }
}