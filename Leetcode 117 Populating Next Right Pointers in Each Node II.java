/*
Follow up for problem "Populating Next Right Pointers in Each Node".

What if the given tree could be any binary tree? Would your previous solution still work?

Note:

You may only use constant extra space.
For example,
Given the following binary tree,
         1
       /  \
      2    3
     / \    \
    4   5    7
After calling your function, the tree should look like:
         1 -> NULL
       /  \
      2 -> 3 -> NULL
     / \    \
    4-> 5 -> 7 -> NULL
 */   
/**
 * Definition for binary tree with next pointer.
 * public class TreeLinkNode {
 *     int val;
 *     TreeLinkNode left, right, next;
 *     TreeLinkNode(int x) { val = x; }
 * }
 */
public class Solution {
    public void connect(TreeLinkNode root) {
        if(root == null)
            return;
        // the new head of each level
        TreeLinkNode newHead = new TreeLinkNode(0);
        TreeLinkNode prev = newHead;
        while(root != null){
            if(root.left != null){
                prev.next = root.left;
                prev = prev.next;
            }
            if(root.right != null){
                prev.next = root.right;
                prev = prev.next;
            }
            // move right
            root = root.next;
            // reach the end of each level
            if(root == null){
                prev = newHead;
                root = newHead.next;
                newHead.next = null;
            }
        }
    }
}