/**
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

You may assume nums1 and nums2 cannot be both empty.

Example 1:

nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2:

nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5
*/
public class Solution {
    // Bruth Force Solution:
    // Merge two array together and calculate the median:
    // Running Time Complexity: O(m+n), Space Complexity: O(m+n)
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int n1 = nums1.length, n2 = nums2.length, n = n1 + n2;
        int[] nums = new int[n];
        // Merge two arrays together:
        int i = 0, j = 0, idx = 0;
        while (i < n1 && j < n2) {
            if (nums1[i] < nums2[j]) {
                nums[idx] = nums1[i];
                i++;
            }
            else {
                nums[idx] = nums2[j];
                j++;
            }
            idx++;
        }
        while (i < n1) {
            nums[idx++] = nums1[i++];
        }
        while (j < n2) {
            nums[idx++] = nums2[j++];
        }
        // If new array's length is odd:
        double result = 0.0;
        if (n % 2 == 1) {
            idx = n/2;
            result = (nums[idx]) * 1.0;
        }
        else {
            idx = n/2;
            result = (nums[idx] + nums[idx-1])/ 2.0;
        }
        return result;
    }

    /**
    https://www.youtube.com/watch?v=LPFhl65R7ww
    思路：
        X array: x1 x2 | x3 x4 x5 x6
        Y array: y1 y2 y3 y4 y5 | y6 y7 y8
        如果我们这样分X和Y array，并且保证x2 <= y6 和 y5 <= x3，这样median就是有关于x2, x3, y5, 和y6
           1. 如果合并的数组是偶数的长度，那么median = (Math.max(x2, y5) + Math.min(x3, y6)) / 2.0
           2. 如果合并的数组是奇数长度的，最后找出来的分界线的左边会多一个item，那么median = Math.max(x2, y5)

        partitionX + partitionY = (x + y + 1) / 2;
        start = 0, end = nums1.length;
        partitionX = (start + end) / 2
        Found:
           maxLeftX <= minRightY
           maxLeftY <= minRightX
           if is odd length:
              return max(maxLeftX, maxLeftY)
           else
              return (max(maxLeftX, maxLeftY) + min(minRightX, minRightY) ) / 2;
        else if (maxLeftX > minRightY) 
           move start = partitionX + 1
        else 
           move end = partitionX - 1
    */
    // Solution 2: Running Time Complexity: O(log(N)) where N = Math.min(m,n)
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        // assume nums1 is the shorter length of array
        if (m > n) {
            return findMedianSortedArrays(nums2, nums1);
        }
        boolean isOdd = (m + n) % 2 == 1;
        int low = 0;
        int high = nums1.length;
        int halfLen = (m + n + 1) / 2;
        while (low <= high) {
            int midX = low + (high - low) / 2;
            int midY = halfLen - midX;

            // If partitionX is 0 it means nothing is there on the left side, use -INF:
            int maxLeftX = Integer.MIN_VALUE;
            if (midX > 0) {
                maxLeftX = nums1[midX-1];
            }
            // If partitioX is length of nums1, then there is nothing on the right side, use +INF for minRightX:
            int minRightX = Integer.MAX_VALUE;
            if (midX < m) {
                minRightX = nums1[midX];
            }
            
            int maxLeftY = Integer.MIN_VALUE;
            if (midY > 0) {
                maxLeftY = nums2[midY-1];
            }
            
            int minRightY = Integer.MAX_VALUE;
            if (midY < n) {
                minRightY = nums2[midY];
            }
            /** 
                We have partitioned array at the correct place:
                Now we get the max of left elements and min of right elements to get the median 
                in case even length combined array size
                or we get the max of left for odd ledth combined array size.
            */
            if (maxLeftX <= minRightY && maxLeftY <= minRightX) {
                if (isOdd) {
                    return Math.max(maxLeftX, maxLeftY) * 1.0;
                }
                else {
                    return (Math.max(maxLeftX, maxLeftY) + Math.min(minRightX, minRightY)) / 2.0;
                }
            }
            // we are too far on the right side for partitionX, need to move left for 1
            else if (maxLeftX > minRightY) {
                high = midX - 1;
            }
            // we are too far on the left size for partitionX, need to move right for 1.
            else {
                low = midX + 1;
            }
        }
        return 0.0;
    }

    // Solution 2: Running Time Complexity: O(log(m+n))
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length, n = nums2.length;
        // ensure m <= n
        if (m > n) {
            return findMedianSortedArrays(nums2, nums1);
        }
        boolean isOdd = (m+n) % 2 == 1;
        int iMin = 0, iMax = m, halfLen = (m + n + 1)/2;
        while (iMin <= iMax) {
            int i = (iMin + iMax) / 2;
            int j = halfLen - i;
            if (i < iMax && nums2[j-1] > nums1[i]) {
                // i is too small
                iMin = i + 1;
            }
            else if (i > iMin && nums1[i-1] > nums2[j]) {
                // i is too big
                iMax = i - 1;
            }
            // i is perfect:
            else {
                int maxLeft = 0;
                if (i == 0) {
                    maxLeft = nums2[j-1];
                }
                else if (j == 0) {
                    maxLeft = nums1[i-1];
                }
                else {
                    maxLeft = Math.max(nums1[i-1], nums2[j-1]);
                }
                if (isOdd) {
                    return maxLeft;
                }
                
                int minRight = 0;
                if (i == m) {
                    minRight = nums2[j];
                }
                else if (j == n) {
                    minRight = nums1[i];
                }
                else {
                    minRight = Math.min(nums1[i], nums2[j]);
                }
                return (maxLeft + minRight) / 2.0;
            }
        }
        return 0.0;
    }
}