
/**
Given an array of words and a width maxWidth, format the text such that each line has exactly maxWidth characters and is fully (left and right) justified.
You should pack your words in a greedy approach; that is, 
pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.

Extra spaces between words should be distributed as evenly as possible. 
If the number of spaces on a line do not divide evenly between words, 
the empty slots on the left will be assigned more spaces than the slots on the right.

For the last line of text, it should be left justified and no extra space is inserted between words.
Note:

A word is defined as a character sequence consisting of non-space characters only.
Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
The input array words contains at least one word.
Example 1:

Input:
words = ["This", "is", "an", "example", "of", "text", "justification."]
maxWidth = 16
Output:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]
Example 2:

Input:
words = ["What","must","be","acknowledgment","shall","be"]
maxWidth = 16
Output:
[
  "What   must   be",
  "acknowledgment  ",
  "shall be        "
]
Explanation: Note that the last line is "shall be    " instead of "shall     be",
             because the last line must be left-justified instead of fully-justified.
             Note that the second line is also left-justified becase it contains only one word.
Example 3:

Input:
words = ["Science","is","what","we","understand","well","enough","to","explain",
         "to","a","computer.","Art","is","everything","else","we","do"]
maxWidth = 20
Output:
[
  "Science  is  what we",
  "understand      well",
  "enough to explain to",
  "a  computer.  Art is",
  "everything  else  we",
  "do                  "
]
*/
public class Solution {
    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> list = new LinkedList<String>();
        int n = words.length;
        for (int i = 0, w; i < n; i = w) {
            int len = -1;
            for (w = i; w < n && len + words[w].length() + 1 <= maxWidth; w++) {
                len += words[w].length() + 1;
            }
            StringBuffer sb = new StringBuffer(words[i]);
            int space = 1, extra = 0;
            if (w != i + 1 && w != n) { // not 1 char, not last line
                space = (maxWidth - len) / (w - i - 1) + 1;
                extra = (maxWidth - len) % (w - i - 1);
            }
            for (int j = i + 1; j < w; j++) {
                for (int s = space; s > 0; s--) {
                    sb.append(' ');
                }
                if (extra > 0) {
                   sb.append(' ');
                   extra--;
                } 
                sb.append(words[j]);
            }
            int strLen = maxWidth - sb.length();
            while (strLen > 0) {
                sb.append(' ');
                strLen--;
            }
            list.add(sb.toString());
        }
        return list;
    }

    // Different Output, NOT the solution:
    // https://www.youtube.com/watch?v=RORuwHiblPc
    // Or MIT course: https://www.youtube.com/watch?v=ENyox7kNKeY&t=2625s
    /**
      * Greedy Algorithm: From first line, fit as many words as possible,
      * But it is not optimal.
      * 
      * Dynamic Programming:
      * 
      * costs[i][j], use words[i..j] as line, = (maxWidth - total width) ^ 2
      *                                         INF if words[i..j] does not fit into this line with maxWidth
      * Goal: Minimize the sum of the costs of each lines
      * 1. Subproblem:
      *    Suffixes words[i..n]
      *    # Subproblem: n
      * 2. Guess: where to start second line: 
      *    # of choices <= n - i = O(n)
      * 3. Recurrence:
      *     minCost[i] = min { 
      *                        m[j] + cost[i][j-1] where j from i+1 to n
      *                      }
      *     j is the point where we break the words into other line:
      */
    // Running Time Complexity: O(N^2), Space Complexity: O(N^2)
    public List<String> fullJustify(String[] words, int maxWidth) {
        int n = words.length;
        int[][] costs = new int[n][n];
        // square of number of spaces after adding words[i..j] into same line.
        // If max out of maxWidth, we mark it as Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            int width = 0;
            for (int j = i; j < n; j++) {
                width += words[j].length();
                int diff = maxWidth - width;
                if (diff >= 0) {
                    cost[i][j] = (int)Math.pow(diff, 2);
                    // add one space for next word;
                    width++;
                }
                else {
                    cost[i][j] = Integer.MAX_VALUE;
                }
            }
        }
        int[] minCost = new int[n];
        int[] result = new int[n];
        // Scan the words list from right to left:
        for (int i = n-1; i >= 0; i--) {
            minCost[i] = cost[i][n-1];
            result[i] = n;
            for (int j = n-1; j > i; j--) {
                if (cost[i][j-1] == Integer.MAX_VALUE) {
                    continue;
                }
                if (minCost[j] + cost[i][j-1] < minCost[i]) {
                    minCost[i] = minCost[j] + cost[i][j-1];
                    result[i] = j;
                }
            }
        }
        List<String> res = new ArrayList<>();
        int i = 0, j = 0;
        do {
            StringBuffer sb = new StringBuffer();
            j = result[i];
            for (int k = i; k < j; k++) {
                sb.append(words[k] + " ");
            }
            res.add(sb.toString());
            i = j;
        }
        while(j < n);
        return res;
    }
    public List<String> fullJustify(String[] words, int maxWidth) {
        int n = words.length;
        int[][] cost = new int[n][n];
        for (int i = 0; i < n; i++) {
            cost[i][i] = maxWidth - words[i].length();
            for (int j = i+1; j < n; j++) {
                cost[i][j] = cost[i][j-1] - words[j].length() - 1;
            }
        }
        
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (cost[i][j] < 0) {
                    cost[i][j] = Integer.MAX_VALUE;
                }
                else {
                    cost[i][j] = (int)Math.pow(cost[i][j], 2);
                }
            }
        }
        int[] minCost = new int[n];
        int[] result = new int[n];
        // Scan the words list from right to left:
        for (int i = n-1; i >= 0; i--) {
            minCost[i] = cost[i][n-1];
            result[i] = n;
            for (int j = n-1; j > i; j--) {
                if (cost[i][j-1] == Integer.MAX_VALUE) {
                    continue;
                }
                if (minCost[j] + cost[i][j-1] < minCost[i]) {
                    minCost[i] = minCost[j] + cost[i][j-1];
                    result[i] = j;
                }
            }
        }
        List<String> res = new ArrayList<>();
        int i = 0, j = 0;
        do {
            StringBuffer sb = new StringBuffer();
            j = result[i];
            for (int k = i; k < j; k++) {
                sb.append(words[k] + " ");
            }
            res.add(sb.toString());
            i = j;
        } while(j < n);
        return res;
    }
}