/**
Given a 2D grid, each cell is either a wall 'W', an enemy 'E' or empty '0' (the number zero), return the maximum enemies you can kill using one bomb.
The bomb kills all the enemies in the same row and column from the planted point until it hits the wall since the wall is too strong to be destroyed.
Note: You can only put the bomb at an empty cell.

Example:

Input: [["0","E","0","0"],["E","0","W","E"],["0","E","0","0"]]
Output: 3 
Explanation: For the given grid,

0 E 0 0 
E 0 W E 
0 E 0 0

Placing a bomb at (1,1) kills 3 enemies.
*/
public class Solution {
    // Running Time Complexity: O(m * n)
    public int maxKilledEnemies(char[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int[] colHit = new int[n];
        int rowHit = 0, res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // only when it is the starting point of each row, 
                // or previous left item is a wall, recalculate the number of enemies in the row:
                if (j == 0 || grid[i][j-1] == 'W') {
                    rowHit = 0;
                    for (int k = j; k < n; k++) {
                        if (grid[i][k] == 'W') {
                            break;
                        }
                        else if (grid[i][k] == 'E') {
                            rowHit++;
                        }
                    }
                }
                // Only when it is the starting point of each column,
                // or previous up item is a wall, recalculate the number of enemies in the column
                if (i == 0 || grid[i-1][j] == 'W') {
                    colHit[j] = 0;
                    for (int k = i; k < m; k++) {
                        if (grid[k][j] == 'W') {
                            break;
                        }
                        else if (grid[k][j] == 'E') {
                            colHit[j]++;
                        }
                    }
                }
                if (grid[i][j] == '0') {
                    res = Math.max(res, rowHit + colHit[j]);
                }
            }
        }
        return res;
    }

    /**
     * @param grid: Given a 2D grid, each cell is either 'W', 'E' or '0'
     * @return: an integer, the maximum enemies you can kill using one bomb
     */
    // DFS on each '0' cell,
    // Running Time Complexity: O(m * n * (m + n))
    public int maxKilledEnemies(char[][] grid) {
        // write your code here
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int maxVal = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '0') {
                    int currVal = maxKilledEnemies(grid, i, j, m, n);
                    maxVal = Math.max(maxVal, currVal);
                }
            }
        }
        return maxVal;
    }
    
    private int maxKilledEnemies(char[][] grid, int i, int j, int m, int n) {
        int res = 0;
        // scan the same row to right:
        for (int x = j+1; x < n; x++) {
            if (grid[i][x] == 'W') {
                break;
            }
            else if (grid[i][x] == 'E') {
                res++;
            }
        }
        // scan the same row to the left:
        for (int x = j-1; x >= 0; x--) {
            if (grid[i][x] == 'W') {
                break;
            }
            else if (grid[i][x] == 'E') {
                res++;
            }
        }
        // scan the same column up:
        for (int y = i-1; y >= 0; y--) {
            if (grid[y][j] == 'W') {
                break;
            }
            else if (grid[y][j] == 'E') {
                res++;
            }
        }
        // scan the same column down:
        for (int y = i+1; y < m; y++) {
            if (grid[y][j] == 'W') {
                break;
            }
            else if (grid[y][j] == 'E') {
                res++;
            }
        }
        return res;
    }
}