/*
Given a collection of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.

Each number in C may only be used once in the combination.

Note:
All numbers (including target) will be positive integers.
Elements in a combination (a1, a2, … , ak) must be in non-descending order. (ie, a1 ≤ a2 ≤ … ≤ ak).
The solution set must not contain duplicate combinations.
For example, given candidate set 10,1,2,7,6,1,5 and target 8, 
A solution set is: 
[1, 7] 
[1, 2, 5] 
[2, 6] 
[1, 1, 6] 
*/
public class Solution {
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> list = new ArrayList<>();

        Arrays.sort(candidates);

        int n = candidates.length;
        combinationSum(result, candidates, target, 0, 0, list);
        return result;
    }
    
    private void combinationSum(List<List<Integer>> result, int[] candidates, int target, int level, int sum, int n, List<Integer> list) {
        if (sum > target) {
            return;
        }
        if (sum == target) {
            result.add(new ArrayList<>(list));
            return;
        }
        for (int i = level; i < n; i++) {
            if (i > level && candidates[i] == candidates[i-1]) {
                continue;
            }
            int val = candidates[i];
            list.add(val);
            // i + 1 because each number in the set is used only once.
            combinationSum(result, candidates, target, i+1, sum + val, n, list);
            list.remove(list.size()-1);
        }
    }
}