/**
Given a binary tree, return the zigzag level order traversal of its nodes values. (ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree {3,9,20,#,#,15,7},
    3
   / \
  9  20
    /  \
   15   7
return its zigzag level order traversal as:
[
  [3],
  [20,9],
  [15,7]
]
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursion
    /**
     * Basic Idea, reverse the list when level is odd, root node is at level 0.
     *  reverse the list can be done by list.add(0, currNode.val)
    */
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        zigzagLevelOrder(result, root, 0);
        return result;
    }

    private void zigzagLevelOrder(List<List<Integer>> result, TreeNode node, int level) {
        if (node == null) {
            return;
        }
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);        
        // if level is odd: add item to the beginning of the list, to reverse the list
        if (level % 2 != 0) {
            list.add(0, node.val);
        }
        else {
            list.add(node.val);
        }
        zigzagLevelOrder(result, node.left, level+1);
        zigzagLevelOrder(result, node.right, level+1);
    }
    
    // Iterative
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        int level = 0;
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<Integer>();
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                list.add(currNode.val);
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
            }
            // Reverse the odd level list
            if (level % 2 != 0) {
                Collections.reverse(list);
            }
            result.add(list);
            level++;
        }
        return result;
    }
    
}