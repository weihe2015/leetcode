A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).

Write a function to determine if a number is strobogrammatic. The number is represented as a string.

For example, the numbers "69", "88", and "818" are all strobogrammatic.

public class solution{
	public boolean isStrobogrammatic(String num) {
	    for (int i=0, j=num.length()-1; i <= j; i++, j--)
	        if (!"00 11 88 696".contains(num.charAt(i) + "" + num.charAt(j)))
	            return false;
	    return true;
	}
	// iterative
	public boolean isStrobogrammatic(String num) {
		HashMap<Character,Character> map = new HashMap<Character,Character>();
		map.put('6','9');
		map.put('9','6');
		map.put('1','1');
		map.put('0','0');
		map.put('8','8');
		int lo = 0, hi = num.length() - 1;
		while(lo <= hi){
			if(!map.containsKey(num.charAt(lo)))
				return false;
			if(map.get(num.charAt(lo)) != num.charAt(hi))
				return false;
			lo++; hi--;
		}
		return true;
	}
}