/**
Serialization is the process of converting a data structure or object into a sequence of bits 
so that it can be stored in a file or memory buffer, 
or transmitted across a network connection link to be reconstructed later 
in the same or another computer environment.

Design an algorithm to serialize and deserialize an N-ary tree. 
An N-ary tree is a rooted tree in which each node has no more than N children. 
There is no restriction on how your serialization/deserialization algorithm should work. 
You just need to ensure that an N-ary tree can be serialized to a string and 
this string can be deserialized to the original tree structure.

For example, you may serialize the following 3-ary tree
         1
    3    2    4
  5   6
as [1 [3[5 6] 2 4]]. You do not necessarily need to follow this format, 
so please be creative and come up with different approaches yourself.
*/
// https://www.lintcode.com/problem/serialize-and-deserialize-n-ary-tree
// https://leetcode.com/problems/serialize-and-deserialize-n-ary-tree

public class Solution {
   class Node {
      int label;
      List<Node> neighbors;
      public Node(int label) { 
          this.label = label; 
          this.neighbors = new ArrayList<>(); 
      }
    }
    
    private static final String BAR = "|";
    private static final String REGX_BAR = "\\|";
    private static final String COMMA = ",";
    private static final String POUND = "#";
    
    // 1|3,2,4|#3|5,6|#2||#4||
    public String serialize(Node root) {
        if (root == null) {
            return "";
        }
        // Level order traversal
        StringBuffer sb = new StringBuffer();
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                Node currNode = queue.poll();
                sb.append(currNode.label).append(BAR);
                for (Node neighbor : currNode.neighbors) {
                    sb.append(neighbor.label).append(COMMA);
                    queue.offer(neighbor);
                }
                if (!currNode.neighbors.isEmpty()) {
                    sb.setLength(sb.length()-1);
                }
                sb.append(BAR).append(POUND);
            }
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    public Node deserialize(String data) {
       if (data == null || data.length() == 0) {
           return null;
       }    
       String[] datas = data.split(POUND);
       Node root = convertTextToNode(datas[0], null);
       if (root == null) {
           return null;
       }
       int idx = 1;
       int n = datas.length;
       
       Queue<Node> queue = new LinkedList<>();
       queue.offer(root);
       
      while (idx < n) {
          int size = queue.size();
          for (int i = 1; i <= size; i++) {
              Node currNode = queue.poll();
              for (Node neighborNode : currNode.neighbors) {
                  convertTextToNode(datas[idx++], neighborNode);
                  queue.offer(neighborNode);
              }
          }
      }
       
      return root;
    }
    
    private Node convertTextToNode(String text, Node root) {
        String[] datas = text.split(REGX_BAR);
        int n = datas.length;
        if (n == 0) {
            return null;
        }
        int num = Integer.parseInt(datas[0]);
        // for initial root node, we create the root node.
        // But for neighbors the node is created and we don't need to 
        // create them again.
        if (root == null) {
            root = new Node(num);
        }
        if (n == 1) {
            return root;
        }
        String[] neighborTexts = datas[1].split(COMMA);
       
        for (String neighborText : neighborTexts) {
            int neighborNum = Integer.parseInt(neighborText);
            Node neighborNode = new Node(neighborNum);
            root.neighbors.add(neighborNode);
        }
        return root;
    }
}