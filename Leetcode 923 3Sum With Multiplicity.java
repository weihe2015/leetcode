/**
Given an integer array A, and an integer target, return the number of tuples i, j, k  such that i < j < k and A[i] + A[j] + A[k] == target.

As the answer can be very large, return it modulo 10^9 + 7.

Example 1:

Input: A = [1,1,2,2,3,3,4,4,5,5], target = 8
Output: 20
Explanation:
Enumerating by the values (A[i], A[j], A[k]):
(1, 2, 5) occurs 8 times;
(1, 3, 4) occurs 8 times;
(2, 2, 4) occurs 2 times;
(2, 3, 3) occurs 2 times.
Example 2:

Input: A = [1,1,2,2,2,2], target = 5
Output: 12
Explanation:
A[i] = 1, A[j] = A[k] = 2 occurs 12 times:
We choose one 1 from [1,1] in 2 ways,
and two 2s from [2,2,2,2] in 6 ways.
*/

class Solution {
    public int threeSumMulti(int[] A, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        int res = 0;
        int mod = 1000000007;
        for (int i = 0, max = A.length; i < max; i++) {
            int val = A[i];
            res = (res + map.getOrDefault(target-val, 0)) % mod;
            for (int j = 0; j < i; j++) {
                int sum = A[i] + A[j];
                map.put(sum, map.getOrDefault(sum, 0) + 1);
            }
        }
        return res;
    }

    // My solution:
    public int threeSumMulti(int[] A, int target) {
        Arrays.sort(A);
        // key -> num, val -> feq
        Map<Integer, Integer> map = new LinkedHashMap<>();
        for (int num : A) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        long cnt = 0;
        List<List<Integer>> lists = threeSum(A, target);
        for (List<Integer> subList : lists) {
            Map<Integer, Integer> subMap = new LinkedHashMap<>();
            for (int num : subList) {
                subMap.put(num, subMap.getOrDefault(num, 0) + 1);
            }
            int fac = 1;
            for (int num : subMap.keySet()) {
                int n = map.get(num);
                int r = subMap.get(num);
                long dom = factorial(r) * factorial(n-r);
                if (dom != 0) {
                    long comb = factorial(n) / dom;
                    fac *= comb;
                }
            }
            cnt += fac;
        }

        return (int) cnt % 1000000007;
    }

    private int factorial(int n) {
        int fact = 1;
        int i = 1;
        while(i <= n) {
            fact *= i;
            i++;
        }
        return fact;
   }

    private List<List<Integer>> threeSum(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0, max = nums.length; i < max-2; i++) {
            if (i > 0 && nums[i] == nums[i-1]) {
                continue;
            }
            int j = i+1, k = max-1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == target) {
                    res.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    while (j < k && nums[j] == nums[j+1]) {
                        j++;
                    }
                    while (j < k && nums[k] == nums[k-1]) {
                        k--;
                    }
                    j++;
                    k--;
                }
                else if (sum < target) {
                    j++;
                }
                else {
                    k--;
                }
            }
        }
        return res;
    }
}