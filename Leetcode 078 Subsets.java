/**
Given a set of distinct integers, nums, return all possible subsets.

Note:
Elements in a subset must be in non-descending order.
The solution set must not contain duplicate subsets.
For example,
If nums = [1,2,3], a solution is:

[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]
*/
public class Solution {

    // solution 1
    /**
     * While iterating through all numbers, for each new number, we can either pick it or not pick it
        1, if pick, just add current number to every existing subset.
        2, if not pick, just leave all existing subsets as they are.
        We just combine both into our result.

        For example, {1,2,3} intially we have an emtpy set as result [ [ ] ]
        Considering 1, if not use it, still [ ], if use 1, add it to [ ], so we have [1] now
        Combine them, now we have [ [ ], [1] ] as all possible subset

        Next considering 2, if not use it, we still have [ [ ], [1] ], if use 2, just add 2 to each previous subset, we have [2], [1,2]
        Combine them, now we have [ [ ], [1], [2], [1,2] ]

        Next considering 3, if not use it, we still have [ [ ], [1], [2], [1,2] ], if use 3, just add 3 to each previous subset, we have [ [3], [1,3], [2,3], [1,2,3] ]
        Combine them, now we have [ [ ], [1], [2], [1,2], [3], [1,3], [2,3], [1,2,3] ]
     * 
    */
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        result.add(new ArrayList<Integer>());
        for (int num : nums) {
            int size = result.size();
            for (int i = 0; i < size; i++) {
                List<Integer> subList = new ArrayList<>(result.get(i));
                subList.add(num);
                result.add(subList);
            }
        }
        return result;
    }

    // solution 2, backtracking 11-15-17:
    public List<List<Integer>> subsets2(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        generateSubSets(result, nums, 0, new ArrayList<Integer>());
        return result;
    }
    
    private void generateSubSets(List<List<Integer>> result, int[] nums, int level, ArrayList<Integer> list) {
        result.add(new ArrayList<Integer>(list));
        
        for (int i = level, max = nums.length; i < max; i++) {
            list.add(nums[i]);
            generateSubSets(result, nums, i+1, list);
            list.remove(list.size()-1);
        }
    }

    // solution 3, bit operation
    public List<List<Integer>> subsets3(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        Arrays.sort(nums); 
        int n = nums.length;
        int total = 1 << n;
        for(int i = 0; i < total; i++){
            List<Integer> temp = new ArrayList<Integer>();
            for(int j = 0; j < n; j++){
                if(((1 << j) & i) > 0){
                    temp.add(nums[j]);
                }
            }
            result.add(temp);
        }
        return result;
     }
}