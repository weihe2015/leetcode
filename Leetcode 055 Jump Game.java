/*
Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Determine if you are able to reach the last index.

Example 1:

Input: [2,3,1,1,4]
Output: true
Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
Example 2:

Input: [3,2,1,0,4]
Output: false
Explanation: You will always arrive at index 3 no matter what. Its maximum
             jump length is 0, which makes it impossible to reach the last index.
*/
public class Solution {
	// solution 1
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public boolean canJump(int[] nums) {
        int reach = 0;
        for (int i = 0, n = nums.length; i < n; i++) {
            // the maximum idx that we can reach from current idx:
            int currMaxReach = i + nums[i];
            reach = Math.max(reach, currMaxReach);
            // If max position we can reach is further than last idx, we return true
            if (reach >= n-1) {
                return true;
            }
            // current reach could only reach up to current position, so break and return false
            if (reach <= i) {
                return false;
            }
        }
        return false;
    }
    // solution 2
    public boolean canJump(int[] nums) {
        int dis = 0, n = nums.length;
        for(int i = 0; i <= dis; i++){
            dis = Math.max(dis,i+nums[i]);
            if(dis >= n-1)
                return true;
        }
        return false;
    }
}