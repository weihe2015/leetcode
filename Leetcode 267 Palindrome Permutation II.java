/*
Given a string s, return all the palindromic permutations (without duplicates) of it. Return an empty list if no palindromic permutation could be form.

For example:

Given s = "aabb", return ["abba", "baab"].

Given s = "abc", return [].

Show Hint 
If a palindromic permutation exists, we just need to generate the first half of the string.Show More Hint 
To generate all distinct permutations of a (half of) string, use a similar approach from: Permutations II or Next Permutation.
*/
class Solution {
    // Brute Force:
    // Running Time Complexity: O(N!)
    public List<String> generatePalindromes(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        if (!canBePalindrome(s)) {
            return res;
        }
        char[] cList = s.toCharArray();
        Arrays.sort(cList);
        
        int n = s.length();
        List<String> list = new ArrayList<>();
        boolean[] visited = new boolean[n];
        StringBuffer sb = new StringBuffer();
        permutateStrings(list, cList, n, sb, visited);
        
        for (String str : list) {
            if (isPalindrome(str)) {
                res.add(str);
            }
        }
        return res;
    }
    
    private void permutateStrings(List<String> list, char[] cList, int n, StringBuffer sb, boolean[] visited) {
        if (sb.length() == n) {
            list.add(sb.toString());
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            if (i > 0 && visited[i-1] && cList[i] == cList[i-1]) {
                continue;
            }
            visited[i] = true;
            sb.append(cList[i]);
            permutateStrings(list, cList, n, sb, visited);
            visited[i] = false;
            sb.setLength(sb.length()-1);
        }
    } 
    
    private boolean canBePalindrome(String s) {
        Set<Character> set = new HashSet<>();
        for (char c : s.toCharArray()) {
            if (!set.contains(c)) {
                set.add(c);
            }
            else {
                set.remove(c);
            }
        }
        return set.size() <= 1;
    }
    
    private boolean isPalindrome(String s) {
        int i = 0, j = s.length()-1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++; j--;
        }
        return true;
    }

    // Solution 2:
    /**
        1. Check if original String can be form palindrome or not
        2. If it cannot form palindrome, return empty list
        3. It it can form palindrome:
           3.1 Distinguish characters that has odd or even occurrence
           3.2 put 1 char of each odd occurrence as middle string
           3.3 form all permutation of all strings that has half occurrence:
    Ex: abcccba
        1. Generate all permutation of abc
        2. for each permutation, result string will be abc + midStr + abc.reverse();
    Note: Use HashSet or boolean array to avoid generate duplicate string
    */
    // Running Time Complexity: O(N!), Space Complexity: O(N)
    public List<String> generatePalindromes(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        if (!canBePalindrome(s)) {
            return res;
        }
        // Count frequency of each character
        Map<Character, Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        // Permutation: even char + odd char + even char
        // We only need to find all permutation of first half of str, append with odd char and reverse of first half of str
        StringBuffer oddStr = new StringBuffer();
        StringBuffer str = new StringBuffer();
        for (char key : map.keySet()) {
            int freq = map.get(key);
            if (freq % 2 != 0) {
                oddStr.append(key);
            }
            for (int i = 0; i < freq/2; i++) {
                str.append(key);
            }
        }
        int n = str.length();
        char[] cList = str.toString().toCharArray();
        Arrays.sort(cList);
        StringBuffer sb = new StringBuffer();
        boolean[] visited = new boolean[n];
        genernatePermutation(res, n, cList, sb, oddStr, visited);
        return res;
    }
    
    private void genernatePermutation(List<String> res, int n, char[] cList, StringBuffer sb, StringBuffer oddStr, boolean[] visited) {
        if (sb.length() == n) {
            String str = sb.toString() + oddStr + sb.reverse().toString();
            res.add(str);
            sb.reverse();
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            if (i > 0 && visited[i-1] && cList[i] == cList[i-1]) {
                continue;
            }
            sb.append(cList[i]);
            visited[i] = true;
            genernatePermutation(res, n, cList, sb, oddStr, visited);
            sb.setLength(sb.length()-1);
            visited[i] = false;
        }
    }
    
    private boolean canBePalindrome(String s) {
        Set<Character> set = new HashSet<>();
        for (char c : s.toCharArray()) {
            if (!set.contains(c)) {
                set.add(c);
            }
            else {
                set.remove(c);
            }
        }
        return set.size() <= 1;
    }
    
    private boolean isPalindrome(String s) {
        int i = 0, j = s.length()-1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++; j--;
        }
        return true;
    }
}