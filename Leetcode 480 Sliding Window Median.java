/**
Median is the middle value in an ordered integer list.
If the size of the list is even, there is no middle value.
So the median is the mean of the two middle value.

Examples:
[2,3,4] , the median is 3

[2,3], the median is (2 + 3) / 2 = 2.5

Given an array nums, there is a sliding window of size k which is moving from the very left of the array to the very right. You can only see the k numbers in the window. Each time the sliding window moves right by one position. Your job is to output the median array for each window in the original array.

For example,
Given nums = [1,3,-1,-3,5,3,6,7], and k = 3.

Window position                Median
---------------               -----
[1  3  -1] -3  5  3  6  7       1
 1 [3  -1  -3] 5  3  6  7       -1
 1  3 [-1  -3  5] 3  6  7       -1
 1  3  -1 [-3  5  3] 6  7       3
 1  3  -1  -3 [5  3  6] 7       5
 1  3  -1  -3  5 [3  6  7]      6
Therefore, return the median sliding window as [1,-1,-1,3,5,6].

Note:
You may assume k is always valid, ie: k is always smaller than input array's size for non-empty array.
Answers within 10^-5 of the actual value will be accepted as correct.
*/
public class Solution {
    // min heap: for nums >= current median
    // max heap: for nums <= current median
    // https://leetcode.com/problems/sliding-window-median/discuss/96348/Java-solution-using-two-PriorityQueues
    Queue<Integer> minQueue = new PriorityQueue<>();
    Queue<Integer> maxQueue = new PriorityQueue<>(Comparator.reverseOrder());
    // Similar to  https://leetcode.com/problems/find-median-from-data-stream/
    // Running Time Complexity: O(nk)
    public double[] medianSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        double[] res = new double[n-k+1];
        for (int i = 0; i < n; i++) {
            add(nums[i]);
            if (i >= k) {
                // Remove the left most item of the window:
                remove(nums[i-k]);
            }
            // When i reaches the right most of the window, start recording the median.
            if (i-k+1 >= 0) {
                res[i-k+1] = getMedian();
            }
        }
        return res;
    }

    private void remove(int num) {
        if (minQueue.peek() <= num) {
            minQueue.remove(num);
        }
        else {
            maxQueue.remove(num);
        }
        rebalance();
    }

    private void add(int num) {
        if (minQueue.isEmpty()) {
            minQueue.add(num);
            return;
        }
        else if (minQueue.peek() <= num) {
            minQueue.offer(num);
        }
        else {
            maxQueue.offer(num);
        }
        rebalance();
    }

    // Always ensure that minQueue size is >= maxQueue size
    private void rebalance() {
        if (minQueue.size() - maxQueue.size() > 1) {
            maxQueue.offer(minQueue.poll());
        }
        else if (maxQueue.size() > minQueue.size()) {
            minQueue.offer(maxQueue.poll());
        }
    }

    private double getMedian() {
        if (minQueue.size() == maxQueue.size()) {
            return ((double) minQueue.peek() + (double) maxQueue.peek()) / 2.0;
        }
        else {
            return (double) minQueue.peek();
        }
    }

    // Bruth Force Solution:
    // Running Time Complexity: O(N*NlogN), Space Complexity: O(K)
    public double[] medianSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        double[] res = new double[n-k+1];
        for (int i = 0; i <= n-k; i++) {
            res[i] = getMedian(nums, i, i+k-1, k);
        }
        return res;
    }

    private double getMedian(int[] nums, int i, int j, int k) {
        int[] temp = new int[k];
        int idx = 0;
        while (i <= j) {
            temp[idx++] = nums[i++];
        }
        Arrays.sort(temp);
        if (k % 2 == 1) {
            return (double) temp[k/2];
        }
        else {
            return ((double) temp[k/2-1] + (double) temp[k/2]) / 2.0;
        }
    }
}