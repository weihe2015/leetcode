/**
Suppose you have a random list of people standing in a queue.
Each person is described by a pair of integers (h, k), where h is the height of the person and k is the number of people in front of this person who have a height greater than or equal to h. Write an algorithm to reconstruct the queue.

Note:
The number of people is less than 1,100.

Example

Input:
[[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]

Output:
[[5,0], [7,0], [5,2], [6,1], [4,4], [7,1]]
*/
    class Person {
        int height;
        int num;
        public Person(int height, int num) {
            this.height = height;
            this.num = num;
        }
    }

    class PersonComparator implements Comparator<Person> {
        public int compare(Person p1, Person p2) {
            if (p1.height == p2.height) {
                return Integer.compare(p1.num, p2.num);
            }
            else {
                return Integer.compare(p2.height, p1.height);
            }
        }
    }

    public int[][] reconstructQueue(int[][] people) {
        List<Person> list = new ArrayList<>();
        for (int[] arr : people) {
            list.add(new Person(arr[0], arr[1]));
        }
        Collections.sort(list, new PersonComparator());

        // placing people based on the K value
        List<Person> newList = new ArrayList<>();
        for (Person p : list) {
            int idx = p.num;
            newList.add(idx, p);
        }

        int n = people.length;
        int[][] res = new int[n][2];
        for (int i = 0; i < n; i++) {
            res[i][0] = newList.get(i).height;
            res[i][1] = newList.get(i).num;
        }
        return res;
    }