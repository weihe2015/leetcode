/**
Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You may assume no duplicates in the array.

Here are few examples.
[1,3,5,6], 5 → 2
[1,3,5,6], 2 → 1
[1,3,5,6], 7 → 4
[1,3,5,6], 0 → 0
*/

public class Solution {
    // Find the element that is equals or just larger than target.
    // 找刚好大于target的元素的位置
    // Running Time Complexity: O(logN), Space Complexity: O(1)
    public int searchInsert(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int res = -1;
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                ans = mid;
                high = mid - 1;
            }
        }
        // 如果 res == -1, 说明 target大于nums的最大值，那么while loop就会一直在else if (nums[mid] < target)
        // 而且一直向右移动low pointer
        // 或者这里可以initialize ans = nums.length
        if (res == -1) {
            res = nums.length;
        }
        return ans;
    }

    // Binary Search. Left Most Search
    public int searchInsert2(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        int res = -1, low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] >= target) {
                res = mid;
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        if (res == -1) {
            res = nums.length;
        }
        return res;
    }

    // Search insertion: no need to consider boundary:
    public int searchInsert3(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int low = 0, high = nums.length - 1;
        while (low + 1 < high) {
            int mid = low + (high - low)/2;
            if (nums[mid] == target) {
                return mid;
            }
            else if (nums[mid] < target) {
                low = mid;
            }
            else {
                high = mid;
            }
        }
        // need to compare nums[low], nums[high] with target to determine the place.
        if (target <= nums[low]){
            return low;
        }
        else if (target <= nums[high]){
            return high;
        }
        else if (target > nums[high]){
            return high+1;
        }
    }
}