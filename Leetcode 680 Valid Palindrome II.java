/**
Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.

Example 1:
Input: "aba"
Output: True
Example 2:
Input: "abca"
Output: True
Explanation: You could delete the character 'c'.
*/
public class Solution {
    // Running Time Complexity: O(n), Space Complexity: O(1)
    // before calling subroutine we have gone through n-m and we have 2 subroutines 2m, resulting in n-m+2m = n+m < 2n
    public boolean validPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }
        int i = 0, j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return validPalindrome(s, i+1, j) || validPalindrome(s, i, j-1);
            }
            i++; j--;
        }
        return true;
    }
    
    private boolean validPalindrome(String s, int i, int j) {
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++; j--;
        }
        return true;
    }
}