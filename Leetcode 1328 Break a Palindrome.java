/**
Given a palindromic string palindrome, replace exactly one character by any lowercase English letter so that the string becomes the lexicographically smallest possible string that isn't a palindrome.

After doing so, return the final string.  If there is no way to do so, return the empty string.



Example 1:

Input: palindrome = "abccba"
Output: "aaccba"
Example 2:

Input: palindrome = "a"
Output: ""
*/
class Solution {
    public String breakPalindrome(String palindrome) {
        if (palindrome == null || palindrome.length() <= 1) {
            return "";
        }
        int n = palindrome.length();
        char[] cList = palindrome.toCharArray();
        for (int i = 0; i < n/2; i++) {
            char ch = cList[i];
            if (ch != 'a') {
                cList[i] = 'a';
                return String.valueOf(cList);
            }
        }
        // replace last character to b:
        cList[n-1] = 'b';
        return String.valueOf(cList);
    }
}