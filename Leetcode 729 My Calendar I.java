/**

Implement a MyCalendar class to store your events. A new event can be added if adding the event will not cause a double booking.

Your class will have the method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

A double booking happens when two events have some non-empty intersection (ie., there is some time that is common to both events.)

For each call to the method MyCalendar.book, return true if the event can be added to the calendar successfully without causing a double booking. Otherwise, return false and do not add the event to the calendar.

Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
Example 1:
MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(15, 25); // returns false
MyCalendar.book(20, 30); // returns true
Explanation: 
The first event can be booked.  The second can't because time 15 is already booked by another event.
The third event can be booked, as the first event takes every time less than 20, but not including 20.
Note:

The number of calls to MyCalendar.book per test case will be at most 1000.
In calls to MyCalendar.book(start, end), start and end are integers in the range [0, 10^9].
*/
public class MyCalendar {
    // Bruth Force: Use solution of Leetcode 252, Meeting Room
    class Interval {
        int start, end;
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
    List<Interval> list;
    public MyCalendar() {
        list = new ArrayList<Interval>();
    }
     
    public boolean book(int start, int end) {
        list.add(new Interval(start, end));
        if (canAttendMeetings(list)) {
            return true;
        }
        else {
            list.remove(list.size()-1);
            return false;
        }
    }
    public boolean canAttendMeetings(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return true;
        }
        int n = intervals.size();
        int[] starts = new int[n], ends = new int[n];
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        Arrays.sort(starts);
        Arrays.sort(ends);
        for (int i = 0; i < n-1; i++) {
            if (ends[i] > starts[i+1]) {
                return false;
            }
        }
        return true;
    }

    // Solution 2: TreeMap:
    // Runining Time: O(logN)
    // key -> start time, val -> end time
    // key is sorted in TreeMap, floorKey method and ceilingKey are performed with binary search
    // Running time is O(logN)
    TreeMap<Integer, Integer> map;
    public MyCalendar() {
        map = new TreeMap<>();
    }
    
    public boolean book(int start, int end) {
        Integer floorKey = map.floorKey(start);
        // [1,5] [7,10] [4,8]
        // end time is within new interval
        if (floorKey != null && map.get(floorKey) > start) {
            return false;
        }
        // [1,3] [7,10] [4,8]
        // start time is within interval
        Integer ceilingKey = map.ceilingKey(start);
        if (ceilingKey != null && ceilingKey < end) {
            return false;
        }
        map.put(start, end);
        return true;
    }

    // Solution 3: Construct a Binary Search Tree, where we use start and end as key:
    class Node {
        Node left;
        Node right;
        int start;
        int end;
        public Node(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
    private Node root;
    public MyCalendar() {
        
    }
    
    public boolean book(int start, int end) {
        if (root == null) {
            root = new Node(start, end);
            return true;
        }
        else {
            return insertNode(root, start, end);
        }
    }
    
    private boolean insertNode(Node node, int start, int end) {
        // If the new time slot end time is less than the root one's start time, so move to the left:
        if (node.start >= end) {
            if (node.left == null) {
                node.left = new Node(start, end);
                return true;
            }
            else {
                return insertNode(node.left, start, end);
            }
        }
        // If the new time slot start time is greater than the root one's end time, so move to the left:
        else if (node.end <= start) {
            if (node.right == null) {
                node.right = new Node(start, end);
                return true;
            }
            else {
                return insertNode(node.right, start, end);
            }
        }
        // Time Slots are overrlap with Node, cannot place the new calendar
        else {
            return false;
        }
    }
}

/**
 * Your MyCalendar object will be instantiated and called as such:
 * MyCalendar obj = new MyCalendar();
 * boolean param_1 = obj.book(start,end);
 */