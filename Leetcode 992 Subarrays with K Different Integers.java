/**
Given an array A of positive integers, call a (contiguous, not necessarily distinct) subarray of 
A good if the number of different integers in that subarray is exactly K.

(For example, [1,2,3,1,2] has 3 different integers: 1, 2, and 3.)

Return the number of good subarrays of A.

 
Example 1:

Input: A = [1,2,1,2,3], K = 2
Output: 7
Explanation: Subarrays formed with exactly 2 different integers: [1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2].
Example 2:

Input: A = [1,2,1,3,4], K = 3
Output: 3
Explanation: Subarrays formed with exactly 3 different integers: [1,2,1,3], [2,1,3], [1,3,4].
 

Note:

1 <= A.length <= 20000
1 <= A[i] <= A.length
1 <= K <= A.length
*/
public class Solution {
    public int subarraysWithKDistinct(int[] A, int K) {
        return subarraysWithMostKDistinct(A, K) - subarraysWithMostKDistinct(A, K-1);
    }
    
    private int subarraysWithMostKDistinct(int[] A, int K) {
        Map<Integer, Integer> freqMap = new HashMap<>();
        int l = 0;
        int r = 0;
        int n = A.length;
        int cnt = 0;
        int uniqueNum = 0;
        while (r < n) {
            if (uniqueNum <= K) {
                int num = A[r];
                int freq = freqMap.getOrDefault(num, 0);
                if (freq == 0) {
                    uniqueNum++;
                }
                freq++;
                freqMap.put(num, freq);
                r++;
            }
            while (uniqueNum > K) {
                int num = A[l];
                int freq = freqMap.get(num);
                if (freq == 1) {
                    uniqueNum--;
                }
                freq--;
                freqMap.put(num, freq);
                l++;
            }
            cnt += r - l;
        }
        return cnt;
    } 
}