/*
Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

For example,
"A man, a plan, a canal: Panama" is a palindrome.
"race a car" is not a palindrome.

Note:
Have you consider that the string might be empty? This is a good question to ask during an interview.

For the purpose of this problem, we define empty string as valid palindrome.
*/
public class Solution {
    public boolean isPalindrome(String s) {
        s = s.replaceAll("[^a-zA-Z0-9]","").toLowerCase();
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) != s.charAt(s.length() - i - 1))
                return false;
        }
        return true;
    }

    // my solution
    public boolean isPalindrome(String s) {
        String str = s.replaceAll("[^a-zA-Z0-9]","");
        int n = str.length();
        for(int i = 0, j = n-1; i < n/2; i++,j--){
            char c1 = Character.toLowerCase(str.charAt(i));
            char c2 = Character.toLowerCase(str.charAt(j));
            if(c1 != c2)
                return false;
        }
        return true;
    }

    // not change origin input
    public boolean isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }
        int i = 0, j = s.length()-1;
        while (i < j) {
            char c1 = s.charAt(i);
            char c2 = s.charAt(j);
            if (!isValidChar(c1)) {
                i++;
                continue;
            }
            if (!isValidChar(c2)) {
                j--;
                continue;
            }
            if (Character.toLowerCase(c1) != Character.toLowerCase(c2)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    private boolean isValidChar(char c) {
        return Character.isLetter(c) || Character.isDigit(c);
    }
}