/**
Given a string that consists of only uppercase English letters,
you can replace any letter in the string with another letter at most k times.
Find the length of a longest substring containing all repeating letters you can get after performing the above operations.

Note:
Both the string's length and k will not exceed 104.

Example 1:

Input:
s = "ABAB", k = 2

Output:
4

Explanation:
Replace the two 'A's with two 'B's or vice versa.
Example 2:

Input:
s = "AABABBA", k = 1

Output:
4

Explanation:
Replace the one 'A' in the middle with 'B' and form "AABBBBA".
The substring "BBBB" has the longest repeating letters, which is 4.
*/
class Solution {
    /**
    Thought process:
    1. Given a string convert it to a string with all same characters with minimal changes:
         => length of the entire string - number of times of the maximum occurring character in the string
    2. Given a substring, convert it to a string with all same characters with at most k changes:
         => (length of the substring - number of times of the maximum occurring character in the substring) <= k
    */
    public int characterReplacement(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        // Count of one character in current sliding window.
        int maxCharCount = 0;
        int l = 0, r = 0, n = s.length(), maxLen = 0;
        int[] charMap = new int[256];
        boolean flag = true;
        while (r < n) {
            if (flag) {
                char rc = s.charAt(r);
                charMap[rc]++;
                maxCharCount = Math.max(maxCharCount, charMap[rc]);
                // Window size - countOfSameChar = countOfChar can be perform operations.
                if (r-l+1-maxCharCount > k) {
                    flag = false;
                }
                r++;
            }
            while (!flag) {
                char lc = s.charAt(l);
                charMap[lc]--;
                maxCharCount = Arrays.stream(charMap).max().getAsInt();
                if (r-l-1- maxCharCount <= k) {
                    flag = true;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }
    public int characterReplacement(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int l = 0, r = 0, n = s.length(), maxLen = 0, maxCharCount = 0;
        int[] charMap = new int[26];
        while (r < n) {
            char rc = s.charAt(r);
            charMap[rc-'A']++;
            maxCharCount = Math.max(maxCharCount, charMap[rc-'A']);
            while (r-l+1-maxCharCount > k) {
                char lc = s.charAt(l);
                charMap[lc-'A']--;
                l++;
                maxCharCount = Arrays.stream(charMap).max().getAsInt();
            }
            r++;
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }
}