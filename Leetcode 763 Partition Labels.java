/**
A string S of lowercase English letters is given. 
We want to partition this string into as many parts as possible so that each letter appears in at most one part, and return a list of integers representing the size of these parts.

Example 1:

Input: S = "ababcbacadefegdehijhklij"
Output: [9,7,8]
Explanation:
The partition is "ababcbaca", "defegde", "hijhklij".
This is a partition so that each letter appears in at most one part.
A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it splits S into less parts.
*/
class Solution {
    // https://leetcode-cn.com/problems/partition-labels/solution/763-hua-fen-zi-mu-qu-jian-ji-lu-zui-yuan-wei-zhi-y/
    // https://leetcode-cn.com/problems/partition-labels/solution/shou-hua-tu-jie-hua-fen-zi-mu-qu-jian-ji-lu-zui-yu/
    /**
    在遍历的过程中相当于是要找每一个字母的边界，如果找到之前遍历过的所有字母的最远边界，说明这个边界就是分割点了。此时前面出现过所有字母，最远也就到这个边界了。

    可以分为如下两步：

    统计每一个字符最后出现的位置
    从头遍历字符，并更新字符的最远出现下标，如果找到字符最远出现位置下标和当前下标相等了，则找到了分割点
    */
    public List<Integer> partitionLabels(String S) {
        if (S == null || S.length() == 0) {
            return null;
        }
        List<Integer> res = new ArrayList<>();
        int n = S.length();
        // record the last index 
        int[] charMap = new int[26];
        for (int i = 0; i < n; i++) {
            char c = S.charAt(i);
            charMap[c-'a'] = i;
        }
        
        // record the end index of the current substring:
        int l = 0;
        int r = 0;
        for (int i = 0; i < n; i++) {
            char c = S.charAt(i);
            r = Math.max(r, charMap[c-'a']);
            if (i == r) {
                res.add(r-l+1);
                l = r + 1;
            }
        }
        
        return res;
    }
}