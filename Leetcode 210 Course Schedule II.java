/**
There are a total of n courses you have to take, labeled from 0 to n-1.

Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

Given the total number of courses and a list of prerequisite pairs, return the ordering of courses you should take to finish all courses.

There may be multiple correct orders, you just need to return one of them. If it is impossible to finish all courses, return an empty array.

Example 1:

Input: 2, [[1,0]]
Output: [0,1]
Explanation: There are a total of 2 courses to take. To take course 1 you should have finished
             course 0. So the correct course order is [0,1] .
Example 2:

Input: 4, [[1,0],[2,0],[3,1],[3,2]]
Output: [0,1,2,3] or [0,2,1,3]
Explanation: There are a total of 4 courses to take. To take course 3 you should have finished both
             courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
             So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3] .
Note:

The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a graph is represented.
You may assume that there are no duplicate edges in the input prerequisites.
*/
public class Solution {
    // BFS Kahn's algorithm for Topological Sorting:
    // Running Time Complexity: O(|V| + |E|)
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < numCourses; i++) {
            graph.add(new ArrayList<>());
        }
        int[] indegree = new int[numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph.get(from).add(to);
            indegree[to]++;
        }
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int idx = 0;
        int[] res = new int[numCourses];
        while (!queue.isEmpty()) {
            int u = queue.poll();
            // We always starts with source node, which indegree = 0, so it is at the beginning in topological sorting.
            res[idx++] = u;
            for (int v : graph.get(u)) {
                indegree[v]--;
                if (indegree[v] == 0) {
                    queue.offer(v);
                }
            }
        }
        return idx == numCourses ? res : new int[0];
    }

    public int[] findOrder(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        
        Map<Integer, Integer> indegreeMap = new HashMap<>();
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            List<Integer> list = graph.getOrDefault(from, new ArrayList<>());
            list.add(to);
            graph.put(from, list);
            
            int indegreeCnt = indegreeMap.getOrDefault(to, 0) + 1;
            indegreeMap.put(to, indegreeCnt);
        }
                
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < numCourses; i++) {
            if (!indegreeMap.containsKey(i)) {
                queue.offer(i);
            }
        }
        
        int[] res = new int[numCourses];
        int idx = 0;
        
        while (!queue.isEmpty()) {
            int v = queue.poll();
            res[idx] = v;
            idx++;
            if (!graph.containsKey(v)) {
                continue;
            }
            List<Integer> list = graph.get(v);
            for (int u : list) {
                int indegreeCnt = indegreeMap.get(u) - 1;
                if (indegreeCnt == 0) {
                    queue.offer(u);
                    indegreeMap.remove(u);
                }
                else {
                    indegreeMap.put(u, indegreeCnt);
                }
            }
        }
        return idx == numCourses ? res : new int[0];
    }

    //  DFS Solution 1:
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        int[][] graph = new int[numCourses][numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph[from][to] = 1;
        }

        int[] visited = new int[numCourses];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < numCourses; i++) {
            if (isCyclic(graph, visited, i, stack)) {
                return new int[0];
            }
        }
        int[] res = new int[numCourses];
        int idx = 0;
        while (!stack.isEmpty()) {
            res[idx++] = stack.pop();
        }
        return res;
    }
    /**
    0 -> not visit yet
    1 -> is visiting
    2 -> visited
    */
    private boolean isCyclic(int[][] graph, int[] visited, int from, Stack<Integer> stack) {
        if (visited[from] == 1) {
            return true;
        }
        else if (visited[from] == 2) {
            return false;
        }
        // visited[from] == 0
        visited[from] = 1;
        for (int i = 0, n = graph.length; i < n; i++) {
            if (graph[from][i] == 0) {
                continue;
            }
            if (isCyclic(graph, visited, i, stack)) {
                return true;
            }
        }
        visited[from] = 2;
        stack.push(from);
        return false;
    }

    // DFS Solution:
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        int[][] graph = new int[numCourses][numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph[from][to] = 1;
        }

        boolean[] visited = new boolean[numCourses];
        boolean[] recStack = new boolean[numCourses];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < numCourses; i++) {
            if (isCyclic(graph, visited, recStack, i, stack)) {
                return new int[0];
            }
        }
        int[] result = new int[numCourses];
        int idx = 0;
        while (!stack.isEmpty()) {
            result[idx++] = stack.pop();
        }
        return result;
    }

    private boolean isCyclic(int[][] graph, boolean[] visited, boolean[] recStack, int from, Stack<Integer> stack) {
        if (!visited[from]) {
            visited[from] = true;
            recStack[from] = true;
            for (int i = 0, n = graph.length; i < n; i++) {
                if (graph[from][i] == 0) {
                    continue;
                }
                if (!visited[i] && isCyclic(graph, visited, recStack, i, stack)) {
                    return true;
                }
                else if (recStack[i]) {
                    return true;
                }
            }
            stack.push(from);
        }
        recStack[from] = false;
        return false;
    }
}