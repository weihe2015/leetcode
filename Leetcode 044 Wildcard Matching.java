/**
Given an input string (s) and a pattern (p), implement wildcard pattern matching with support for '?' and '*'.

'?' Matches any single character.
'*' Matches any sequence of characters (including the empty sequence).
The matching should cover the entire input string (not partial).

Note:

s could be empty and contains only lowercase letters a-z.
p could be empty and contains only lowercase letters a-z, and characters like ? or *.
Example 1:

Input:
s = "aa"
p = "a"
Output: false
Explanation: "a" does not match the entire string "aa".
Example 2:

Input:
s = "aa"
p = "*"
Output: true
Explanation: '*' matches any sequence.
Example 3:

Input:
s = "cb"
p = "?a"
Output: false
Explanation: '?' matches 'c', but the second letter is 'a', which does not match 'b'.
Example 4:

Input:
s = "adceb"
p = "*a*b"
Output: true
Explanation: The first '*' matches the empty sequence, while the second '*' matches the substring "dce".
Example 5:

Input:
s = "acdcb"
p = "a*c?b"
Output: false
*/

public class Solution {

dp[i][j] = str[0...i-1] & pat[0...j-1]
dp[i-1][j-1] = prev
*:
dp[i-1][j] match 1 or more
dp[i][j-1] match 0
    // 2d dp solution
    public boolean isMatch(String s, String p) {
        int m = s.length();
        int n = p.length();
        boolean[][] dp = new boolean[m+1][n+1];
        // empty regex matches empty string:
        dp[0][0] = true;
        
        // scan the regex string and initialize the * match with 1 character before:
        // s = "", p = "*"
        for (int j = 1; j <= n; j++) {
            if (p.charAt(j-1) == '*') {
                dp[0][j] = dp[0][j-1];
            }
        }
        
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if two characters from s and p at same index are matching,
                // or current regex character is ?
                if (s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) == '?') {
                    dp[i][j] = dp[i-1][j-1];
                }
                // if current regex character is *
                else if (p.charAt(j-1) == '*') {
                    // dp[i-1][j]: match previous character with p.substring(0,j) regex
                    // dp[i][j-1]: a* or .* matches empty string
                    dp[i][j] = dp[i-1][j] || dp[i][j-1];
                }
                else {
                    dp[i][j] = false;
                }
            }
        }
        return dp[m][n];
    }

    public boolean isMatch(String str, String pat) {
        // match --> num of char that * match
        // startIdx --> * start index
        int s = 0, p = 0, startIdx = -1, match = 0;
        while (s < str.length()){
            if (p < pat.length() && (pat.charAt(p) == '?' || str.charAt(s) == pat.charAt(p))) {
                s++;
                p++;
            }
            else if (p < pat.length() && pat.charAt(p) == '*'){
                startIdx = p++;
                match = s;
            }
            else if (startIdx != -1){
                p = startIdx + 1;
                s = ++match;
            }
            else {
                return false;
            }
        }
        while (p < pat.length() && pat.charAt(p) == '*') {
            p++
        }
        return p == pat.length();
    }

    // 1d dp solution
    public boolean isMatch(String str, String pat) {
        int m = str.length(), n = pat.length();
        int count = 0;
        for (char c : pat.toCharArray()){
            if (c == '*') {
                count++;
            }
        }
        if (count == 0 && m != n) {
            return false;
        }
        if (n - count > m) {
            return false;
        }
        boolean[] dp = new boolean[m+1];
        dp[0] = true;
        for (int i = 0; i < n; i++) {
            char c = pat.charAt(i);
            if (c == '*') {
                for (int j = 0; j < m; j++) {
                    dp[j+1] = dp[j] || dp[j+1];
                }
            }
            else {
                for (int j = m-1; j >= 0; j--) {
                    dp[j+1] = (c == '?' || str.charAt(j) == c) && dp[j];
                }
                dp[0] = false;
            }
        }
        return dp[m];
    }

    /**
    Recursive Solution (TLE):
    */ 
    public boolean isMatch(String s, String p) {
        int m = s.length();
        int n = p.length();        
        return isMatch(s, p, 0, 0, m, n);
    }
    
    private boolean isMatch(String s, String p, int i, int j, int m, int n) {
        if (i == m && j == n) {
            return true;
        }
        if (j == n) {
            return false;
        } 
        // the remaining in p are *
        if (i == m) {
            for (int k = j; k < n; k++) {
                if (p.charAt(k) != '*') {
                    return false;
                }
            }
            return true;
        }
        if (p.charAt(j) == '*') {
            return isMatch(s, p, i, j+1, m, n) || isMatch(s, p, i+1, j, m, n);
        }
        else {
            return isMatchCharacter(s, p, i, j, m) && isMatch(s, p, i+1, j+1, m, n);
        }
    }
    
    private boolean isMatchCharacter(String s, String p, int i, int j, int m) {
        return i < m && (s.charAt(i) == p.charAt(j) || p.charAt(j) == '?');
    }

    // more concise solution
    public boolean isMatch(String str, String pat) {
        int m = str.length(), n = pat.length();
        boolean[] dp = new boolean[m+1];
        dp[0] = true;
        for (int j = 1; j <= n; j++) {
            boolean prev = dp[0];
            dp[0] = dp[0] && pat.charAt(j-1) == '*';
            for (int i = 1; i <= m; i++){
                boolean temp = dp[i];
                if (pat.charAt(j-1) != '*') {
                    dp[i] = prev && (str.charAt(i-1) == pat.charAt(j-1) || pat.charAt(j-1) == '?');
                }
                else {
                    dp[i] = dp[i-1] || dp[i];
                }
                prev = temp;
            }
        }
        return dp[m];
    }
    
    // support + , *, ?
    public boolean isMatchExp(String str, String pat) {
        int s = 0, p = 0, startIdx = -1, match = 0;
        while (s < str.length()){
            if (p < pat.length() && (pat.charAt(p) == '?' || pat.charAt(p) == str.charAt(s))) {
                s++;
                p++;
            }
            else if (p < pat.length() && (pat.charAt(p) == '*' || pat.charAt(p) == '+')) {
                startIdx = p++;
                match = s;
            }
            else if (startIdx != -1){
                p = startIdx + 1;
                s = ++match;
            }
            else {
                return false;
            }
                
        }
        while (p < pat.length() && pat.charAt(p) == '*') {
            p++;
        }
            
        if (pat.length() > str.length()) {
            if (pat.charAt(startIdx+1) - 'a' > 0) {
                return false;
            }
        }
        return p == pat.length();
    }    
}