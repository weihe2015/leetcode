/**
Given a string S, we can transform every letter individually to be lowercase or uppercase to create another string.  Return a list of all possible strings we could create.

Examples:
Input: S = "a1b2"
Output: ["a1b2", "a1B2", "A1b2", "A1B2"]

Input: S = "3z4"
Output: ["3z4", "3Z4"]

Input: S = "12345"
Output: ["12345"]
*/

public class Solution {
    // Running Time Complexity: O(N!)
    public List<String> letterCasePermutation(String S) {
        List<String> result = new ArrayList<>();
        if (S == null || S.length() == 0) {
            result.add(S);
            return result;
        }
        letterCasePermutation(result, S, 0, new StringBuffer());
        return result;
    }
    
    private void letterCasePermutation(List<String> result, String S, int index, StringBuffer sb) {
        if (index == S.length() && sb.length() == S.length()) {
            result.add(sb.toString());
            return;
        }
        for (int i = index, max = S.length(); i < max; i++) {
            char c = S.charAt(i);
            sb.append(c);
            if (Character.isLetter(c)) {
                letterCasePermutation(result, S, i+1, sb);
                sb.setLength(sb.length()-1);
                if (Character.isLowerCase(c)) {
                    sb.append(Character.toUpperCase(c));
                }
                else {
                    sb.append(Character.toLowerCase(c));
                }
            }
            letterCasePermutation(result, S, i+1, sb);
            sb.setLength(sb.length()-1);
        }
    }
    // Use BFS, Running Time Complexity: O(2^N * N), N = S.length()
    public List<String> letterCasePermutation2(String S) {
        if (S == null) {
            return new LinkedList<>();
        }
        Queue<String> q = new LinkedList<String>();
        q.offer(S);
        for (int i = 0, max = S.length(); i < max; i++) {
            char c = S.charAt(i);
            if (!Character.isLetter(c)) {
                continue;
            }
            int size = q.size();
            for (int j = 1; j <= size; j++) {
                char[] cList = q.poll().toCharArray();
                cList[i] = Character.toUpperCase(cList[i]);
                q.offer(String.valueOf(cList));
                
                cList[i] = Character.toLowerCase(cList[i]);
                q.offer(String.valueOf(cList));
                
            }
        }
        return new LinkedList<String>(q);
    }
}