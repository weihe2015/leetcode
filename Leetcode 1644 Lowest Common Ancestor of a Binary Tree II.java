/**
Given the root of a binary tree, return the lowest common ancestor (LCA) of two given nodes, p and q. 
If either node p or q does not exist in the tree, return null. 
All values of the nodes in the tree are unique.

According to the definition of LCA on Wikipedia: 
"The lowest common ancestor of two nodes p and q in a binary tree T is the lowest node that has both p and q as descendants 
(where we allow a node to be a descendant of itself)". 
A descendant of a node x is a node y that is on the path from node x to some leaf node.

*/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /*
     * @param root: The root of the binary tree.
     * @param A: A TreeNode
     * @param B: A TreeNode
     * @return: Return the LCA of the two nodes.
     */
    private boolean foundA;
    private boolean foundB;

    public TreeNode lowestCommonAncestor3(TreeNode root, TreeNode A, TreeNode B) {
        // write your code here
        TreeNode res = helper(root, A, B);
        if (foundA && foundB) {
            return res;
        }
        return null;
    }
    
    private TreeNode helper(TreeNode root, TreeNode A, TreeNode B) {
        if (root == null) {
            return null;
        }
        
        TreeNode left = helper(root.left, A, B);
        TreeNode right = helper(root.right, A, B);
        
        if (root == A || root == B) {
            if (root == A) {
                foundA = true;
            }
            if (root == B) {
                foundB = true;
            }
            return root;
        }
        if (left == null && right == null) {
            return null;
        }
        else if (left == null) {
            return right;
        }
        else if (right == null) {
            return left;
        }
        return root;
    }
}