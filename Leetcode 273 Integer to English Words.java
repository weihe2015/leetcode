/*
Convert a non-negative integer to its english words representation. Given input is guaranteed to be less than 231 - 1.

For example,
123 -> "One Hundred Twenty Three"
12345 -> "Twelve Thousand Three Hundred Forty Five"
1234567 -> "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
*/
public class Solution {    
    private static final String[] LESSTHANTWENTY = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    private static final String[] TENS = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    private static final String[] HUNDREDS = {"", "One Hundred", "Two Hundred", "Three Hundred", "Four Hundred", "Five Hundred", "Six Hundred", "Seven Hundred", "Eight Hundred", "Nine Hundred"};
    private static final String[] THOUSANDS = {"", "Thousand", "Million", "Billion"};

    public String numberToWords(int num) {
        if (num == 0) {
            return "Zero";
        }
        
        StringBuffer sb = new StringBuffer();
        int thousandsIndex = 0;
        while (num > 0) {
            if (num % 1000 != 0) {
                String suffix = convertNumToWords(num % 1000);
                sb.insert(0, " ");
                sb.insert(0, THOUSANDS[thousandsIndex]);
                sb.insert(0, " ");
                sb.insert(0, suffix);
            }
            num /= 1000;
            thousandsIndex++;
        }
        return sb.toString().trim();
    }
    
    private String convertNumToWords(int num) {
        StringBuffer sb = new StringBuffer();
        if (num < 20) {
            sb.append(LESSTHANTWENTY[num]);
        }
        else if (num < 100) {
            sb.append(TENS[num/10]);
            sb.append(" ");
            sb.append(convertNumToWords(num%10));
        }
        else {
            sb.append(HUNDREDS[num/100]);
            sb.append(" ");
            sb.append(convertNumToWords(num%100));
        }
        return sb.toString().trim();
    }
}