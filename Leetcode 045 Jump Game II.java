/**
Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Your goal is to reach the last index in the minimum number of jumps.

Example:

Input: [2,3,1,1,4]
Output: 2
Explanation: The minimum number of jumps to reach the last index is 2.
    Jump 1 step from index 0 to 1, then 3 steps to the last index.
Note:

You can assume that you can always reach the last index.
*/
public class Solution {
    // Greedy algorithm, local optimal => global optimal
    // https://leetcode-cn.com/problems/jump-game-ii/solution/tiao-yue-you-xi-ii-by-leetcode-solution/
    /**
    在具体的实现中，我们维护当前能够到达的最大下标位置，记为边界。我们从左到右遍历数组，到达边界时，更新边界并将跳跃次数增加 1。
    
    在遍历数组时，我们不访问最后一个元素，这是因为在访问最后一个元素之前，我们的边界一定大于等于最后一个位置，否则就无法跳到最后一个位置了。如果访问最后一个元素，在边界正好为最后一个位置的情况下，我们会增加一次「不必要的跳跃次数」，因此我们不必访问最后一个元素
    */
    // Running Time Complexity: O(N) Space Complexity: O(1)
    public int jump(int[] nums) {
        int reach = 0, furthest = 0, count = 0;
        for (int i = 0, n = nums.length; i < n-1; i++) {
            int currMaxReach = i + nums[i];
            reach = Math.max(reach, currMaxReach);
            if (i == furthest) {
                furthest = reach;
                count++;
                if (furthest >= n-1) {
                    break;
                }
            }
        }
        return count;
    }

    // BFS:
    public int jump(int[] nums) {
        if (nums == null || nums.length <= 1) {
            return 0;
        }
        int count = 0, currMaxReach = 0, i = 0, n = nums.length;
        while (i <= currMaxReach) {
            int reach = currMaxReach;
            while (i <= currMaxReach) {
                reach = Math.max(reach, nums[i]+i);
                if (reach >= n-1) {
                    return count + 1;
                }
                i++;
            }
            count++;
            currMaxReach = reach;
        }
        return -1;
    }
}