/**
Given a binary tree, return the bottom-up level order traversal of its nodes values. (ie, from left to right, level by level from leaf to root).

For example:
Given binary tree {3,9,20,#,#,15,7},
    3
   / \
  9  20
    /  \
   15   7
return its bottom-up level order traversal as:
[
  [15,7],
  [9,20],
  [3]
]
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Iterative:
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<Integer>();
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                list.add(currNode.val);
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
            }
            result.add(0, list);
        }
        return result;
    }

    // Recursive:
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrderBottom(result, root, 0);
        Collections.reverse(result);
        return result;
    }
    
    private void levelOrderBottom(List<List<Integer>> result, TreeNode node, int level) {
        if (node == null) {
            return;
        }
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);
        list.add(node.val);
        levelOrderBottom(result, node.left, level+1);
        levelOrderBottom(result, node.right, level+1);
    }
    // DFS
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if(root == null)
            return result;
        levelOrderBottom(result,root,0);
        return result;    
    }
    
    public void levelOrderBottom(List<List<Integer>> result, TreeNode root, int level){
        if(root == null)
            return;
        if(result.size() == level){
            result.add(0,new ArrayList<Integer>());
        }
        levelOrderBottom(result,root.left,level+1);
        levelOrderBottom(result,root.right,level+1);
        List<Integer> list = result.get(result.size()-1-level);
        list.add(root.val);
    } 
}