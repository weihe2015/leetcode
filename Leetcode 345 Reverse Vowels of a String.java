Write a function that takes a string as input and reverse only the vowels of a string.

Example 1:
Given s = "hello", return "holle".

Example 2:
Given s = "leetcode", return "leotcede".

public class Solution {
    public String reverseVowels(String s) {
        if(s.length() == 0 || s == null)
            return "";
        char[] cLists = s.toCharArray();
        int front = 0;
        int end = cLists.length-1;
        while(front < end){
            while(front < end && !isVowel(cLists[front])){
                front++;
            }
            while(front < end && !isVowel(cLists[end])){
                end--;
            }
            swap(cLists,front,end);
            front++;
            end--;
                
        }
        return String.valueOf(cLists);
    }
    
    public boolean isVowel(char c){
        c = Character.toLowerCase(c);
        if(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
            return true;
        else
            return false;
    }
    
    public void swap(char[] cLists, int i, int j){
        char c = cLists[i];
        cLists[i] = cLists[j];
        cLists[j] = c;
    }

    // my solution
    public String reverseVowels(String s) {
        String vowels = "aeiouAEIOU";
        if(s.length() == 0 || s == null)
            return "";
        char[] cLists = s.toCharArray();
        int i = 0, j = cLists.length - 1;
        while(i < j){
            while(i < j && vowels.indexOf(cLists[i]) < 0)
                i++;
            while(i < j && vowels.indexOf(cLists[j]) < 0)
                j--;
            if(cLists[i] != cLists[j])
                swap(cLists,i,j);
            i++; j--;
        }
        return String.valueOf(cLists);
    }
    public void swap(char[] cList, int i, int j){
        char c = cList[i];
        cList[i] = cList[j];
        cList[j] = c;
    } 
}