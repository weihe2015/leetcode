/**
Given an array of integers nums and a positive integer k, find whether it's possible to divide this array into k non-empty subsets whose sums are all equal.

Example 1:

Input: nums = [4, 3, 2, 3, 5, 2, 1], k = 4
Output: True
Explanation: It's possible to divide it into 4 subsets (5), (1, 4), (2,3), (2,3) with equal sums.
 

Note:

1 <= k <= len(nums) <= 16.
0 < nums[i] < 10000.
*/
public class Solution {
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int n = nums.length;
        if (n == 0) {
            return false;
        }
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum % k != 0) {
            return false;
        }
        boolean[] visited = new boolean[n];
        
        int target = sum / k;
        return helper(nums, k, target, 0, 0, n, visited);
    }
    
    private boolean helper(int[] nums, int k, int target, int currSum, int start, int n, boolean[] visited) {
        // it can be k == 1 or k == 0:
        // because we have ensure that total sum of array is divisible by k
        if (k == 1) {
            return true;
        }
        if (currSum > target) {
            return false;
        }
        if (currSum == target) {
            return helper(nums, k-1, target, 0, 0, n, visited);
        }
        for (int i = start; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            visited[i] = true;
            if (helper(nums, k, target, currSum + nums[i], i+1, n, visited)) {
                return true;
            }
            visited[i] = false;
        }
        return false;
    }
}