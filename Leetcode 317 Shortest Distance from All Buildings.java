/*
You want to build a house on an empty land which reaches all buildings in the shortest amount of distance. 
You can only move up, down, left and right. You are given a 2D grid of values 0, 1 or 2, where:

Each 0 marks an empty land which you can pass by freely.
Each 1 marks a building which you cannot pass through.
Each 2 marks an obstacle which you cannot pass through.
For example, given three buildings at (0,0), (0,4), (2,2), and an obstacle at (0,2):

1 - 0 - 2 - 0 - 1
|   |   |   |   |
0 - 0 - 0 - 0 - 0
|   |   |   |   |
0 - 0 - 1 - 0 - 0
The point (1,2) is an ideal empty land to build a house, as the total travel distance of 3+3+1=7 is minimal. So return 7.

Note:
There will be at least one building. If it is not possible to build such house according to the above rules, return -1.
*/
public class Solution {
    /**
    我们还是用BFS来做，其中dist是累加距离场，cnt表示某个位置已经计算过的建筑数，变量buildingCnt为建筑的总数，
    我们还是用queue来辅助计算，注意这里的dist的更新方式跟上面那种方法的不同，这里的dist由于是累积距离场，
    所以不能用dist其他位置的值来更新，而是需要直接加上和建筑物之间的距离，这里用level来表示，每遍历一层，level自增1，
    这样我们就需要所加个for循环，来控制每一层中的level值是相等的

    for each building, perform BFS on the whole graph, and mark the shorest distance from this building to
    cell with value 0.
    Whenever the cell has the lowest distance and have reachable count the same as total number
    of building, it is the answer.
    */
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int shortestDistance(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int[][] distance = new int[m][n];
        int[][] reach = new int[m][n];
        int buildingCnt = 0;
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != 1) {
                    continue;
                }
                buildingCnt++;
                Queue<int[]> queue = new LinkedList<>();
                q.offer(new int[]{i, j});
                
                boolean[][] visited = new boolean[m][n];
                int level = 1;
                
                while (!queue.isEmpty()) {
                    int size = queue.size();
                    for (int s = 1; s <= size; s++) {
                        int[] curr = q.poll();
                        for (int[] dir : dirs) {
                            int x = curr[0] + dir[0];
                            int y = curr[1] + dir[1];
                            if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] != 0 || visited[x][y]) {
                                continue;
                            }
                            distance[x][y] += level;
                            reach[x][y]++;
                            visited[x][y] = true;
                            queue.offer(new int[]{x, y});
                        }
                    }
                    level++;
                }
            }
        }
        
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0 && reach[i][j] == buildingCnt) {
                    res = Math.min(res, distance[i][j]);
                }
            }
        }
        return res == Integer.MAX_VALUE ? -1 : res;
    }
}