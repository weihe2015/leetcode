/**
Given a positive 32-bit integer n, you need to find the smallest 32-bit integer which has exactly the same digits 
existing in the integer n and is greater in value than n. If no such positive 32-bit integer exists, 
you need to return -1.

Example 1:

Input: 12
Output: 21
 

Example 2:

Input: 21
Output: -1
*/
public class Solution {
    public int nextGreaterElement(int n) {
        String s = String.valueOf(n);
        char[] cList = s.toCharArray();
        int m = s.length();
        // check if it is decending order:
        int k = -1;
        for (int i = m-1; i > 0; i--) {
            if (cList[i] > cList[i-1]) {
                k = i-1;
                break;
            }
        }
        if (k == -1) {
            return -1;
        }
        // find the smallest element to the right of k
        // that is larger than nums[k]:
        int l = -1;
        for (int i = m-1; i > k; i--) {
            if (cList[i] > cList[k]) {
                l = i;
                break;
            }
        }
        
        swap(cList, k, l);
        reverse(cList, k+1, m-1);

        // Ex: 1999999999
        Long res = Long.parseLong(String.valueOf(cList));
        if (res > Integer.MAX_VALUE) {
            return -1;
        }
        return res.intValue();
    }
    
    private void swap(char[] cList, int i, int j) {
        char temp = cList[i];
        cList[i] = cList[j];
        cList[j] = temp;
    }
    
    private void reverse(char[] cList, int i, int j){
        while (i < j){
            swap(cList, i, j);
            i++;
            j--;
        }
    }
}