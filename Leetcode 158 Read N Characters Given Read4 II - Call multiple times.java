/**
The API: int read4(char *buf) reads 4 characters at a time from a file.

The return value is the actual number of characters read. For example, it returns 3 if there is only 3 characters left in the file.

By using the read4 API, implement the function int read(char *buf, int n) that reads n characters from the file.

Note:
The read function may be called multiple times.

Example 1: 

Given buf = "abc"
read("abc", 1) // returns "a"
read("abc", 2); // returns "bc"
read("abc", 1); // returns ""
Example 2: 

Given buf = "abc"
read("abc", 4) // returns "abc"
read("abc", 1); // returns ""
*/

public class solution extends Reader4 {

	@Test
	public void ReadNCharactersGivenRead4Test1() throws Exception {
		String str = "abc";
		char[] buff = str.toCharArray();
		setContent(buff);
		Assert.assertEquals(3, read(buff, 4));
		
		str = "abcd";
		buff = str.toCharArray();
		setContent(buff);
		Assert.assertEquals(2, read(buff, 2));
		
		str = "abcde";
		buff = str.toCharArray();
		setContent(buff);
		Assert.assertEquals(5, read(buff, 5));
	}
    // Solution 1:
    private int buffPtr = 0;
    private int buffCnt = 0;
    private char[] cache = new char[4];
    public int read(char[] buf, int n) {
        int i = 0;
        while (i < n) {
            // When all characters in cache are read, get new 4 characters from read4
            if (buffPtr == 0) {
                buffCnt = read4(cache);
            }
            // There are no more new characters, break
            if (buffCnt == 0) {
                break;
            }
            // copy cache characters to buf
            while (i < n && buffPtr < buffCnt) {
                buf[i] = cache[buffPtr];
                i++;
                buffPtr++;
            }
            // When all characters are transfered to buf, reset buffPtr
            if (buffPtr == buffCnt) {
                buffPtr = 0;
            }
        }
        return i;
    }
    
    // Solution 2:
    private Queue<Character> queue = new LinkedList<>();
    public int read(char[] buf, int n) {
        // Write your code here
        int i = 0;
        while (i < n && !queue.isEmpty()) {
            buf[i] = queue.poll();
            i++;
        }
        while (i < n) {
            char[] cache = new char[4];
            int buffCnt = read4(cache);
            // When no new character, break and return i
            if (buffCnt == 0) {
                break;
            }
            int j = 0;
            while (i < n && j < buffCnt) {
                buf[i] = cache[j];
                i++; j++;
            }
            // Set all remaining unused characters in cache into a global queue
            while (j < buffCnt) {
                queue.offer(cache[j]);
                j++;
            }
        }
        return i;
    }
}