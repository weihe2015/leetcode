/*
Validate if a given string is numeric.

Some examples:
"0" => true
" 0.1 " => true
"abc" => false
"1 a" => false
"2e10" => true

Note: It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one.

Update (2015-02-10):
The signature of the C++ function had been updated. 
If you still see your function signature accepts a const char * argument, 
please click the reload button to reset your code definition.
*/
public class Solution {
    // Use Regular Expression:
    public boolean isNumber(String s) {
        // ".9" true
        // "0.9" true
        // "3." true
        // "2e+10" true
        // "2e-10" true
        String numRegex = "(\\s*)[+-]?((\\.[0-9]+)|([0-9]+(\\.[0-9]*)?))(e[+-]?[0-9]+)?(\\s*)";
        return s.matches(numRegex);
    }

    public boolean isNumber(String s) {
        s = s.trim();
        if (s.length() == 0) {
            return false;
        }
        int i = 0, n = s.length();
        // skip leading +|- sign:
        if (s.charAt(i) == '+' || s.charAt(i) == '-') {
            i++;
        }
        boolean isNum = false; // is a digit
        boolean isDot = false; // is a '.'
        boolean isExp = false; // is a 'e'
        while (i < n) {
            char c = s.charAt(i);
            if (Character.isDigit(c)) {
                isNum = true;
            }
            else if (c == '.') {
                // handle "..1"
                if (isExp || isDot) {
                    return false;
                }
                isDot = true;
            }
            else if (c == 'e') {
                // handle "e9" -> false;
                if (isExp || !isNum) {
                    return false;
                }
                isExp = true;
                isNum = false;
            }
            else if (c == '+' || c == '-') {
                if (i > 0 && s.charAt(i-1) != 'e') {
                    return false;
                }
            }
            else {
                return false;
            }
            i++;
        }
        // "." is false
        return isNum;
    }
}