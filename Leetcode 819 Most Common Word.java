/**
Given a paragraph and a list of banned words, 
return the most frequent word that is not in the list of banned words.  
It is guaranteed there is at least one word that isn't banned, and that the answer is unique.

Words in the list of banned words are given in lowercase, and free of punctuation.  
Words in the paragraph are not case sensitive.  The answer is in lowercase.

Example:

Input: 
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
banned = ["hit"]
Output: "ball"
Explanation: 
"hit" occurs 3 times, but it is a banned word.
"ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph. 
Note that words in the paragraph are not case sensitive,
that punctuation is ignored (even if adjacent to words, such as "ball,"), 
and that "hit" isn't the answer even though it occurs more because it is banned.
 

Note:

1 <= paragraph.length <= 1000.
0 <= banned.length <= 100.
1 <= banned[i].length <= 10.
The answer is unique, and written in lowercase (even if its occurrences in paragraph may have uppercase symbols, and even if it is a proper noun.)
paragraph only consists of letters, spaces, or the punctuation symbols !?',;.
There are no hyphens or hyphenated words.
Words only consist of letters, never apostrophes or other punctuation symbols.

https://leetcode.com/problems/most-common-word/
*/

class Solution {
    public String mostCommonWord(String paragraph, String[] banned) {
        Set<String> bannedSet = new HashSet<>();
        for (String ban : banned) {
            bannedSet.add(ban);
        }
        Map<String, Integer> freqMap = new HashMap<>();
        int i = 0;
        int j = 0;
        int n = paragraph.length();
        while (j < n) {
            char c = paragraph.charAt(j);
            if (Character.isLetter(c)) {
                j++;
            }
            else {
                updateFreqMap(freqMap, paragraph, i, j, bannedSet);
                j++;
                i = j;
            }
        }
        updateFreqMap(freqMap, paragraph, i, j, bannedSet);
        
        String res = "";
        int maxFreq = -1;
        for (String key : freqMap.keySet()) {
            int freq = freqMap.get(key);
            if (freq > maxFreq) {
                maxFreq = freq;
                res = key;
            }
        }
        return res;
    }
    
    private void updateFreqMap(Map<String, Integer> freqMap, String paragraph, int i, int j, Set<String> bannedSet) {
        if (i >= j) {
            return;
        }
        String key = paragraph.substring(i, j).toLowerCase();
        if (!bannedSet.contains(key)) {
            int freq = freqMap.getOrDefault(key, 0);
            freq++;
            freqMap.put(key, freq);
        }
    }
}