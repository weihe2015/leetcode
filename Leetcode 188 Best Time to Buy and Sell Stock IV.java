/*
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most k transactions.

Note:
You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).

Example 1:

Input: [2,4,1], k = 2
Output: 2
Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit = 4-2 = 2.
Example 2:

Input: [3,2,6,5,0,3], k = 2
Output: 7
Explanation: Buy on day 2 (price = 2) and sell on day 3 (price = 6), profit = 6-2 = 4.
             Then buy on day 5 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
*/

public class Solution {
    // Same solution of Leetcode 123: Best Tme to Buy and Sell Stock III:
    public int maxProfit(int K, int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        if (K >= n/2) {
            return maxProfitWithInfinityTransaction(prices);
        }
        
        int[][][] dp = new int[n][K+1][2];
        for (int i = 0; i < n; i++) {
            for (int k = K; k >= 1; k--) {
                if (i == 0) {
                    dp[i][k][0] = 0;
                    dp[i][k][1] = -prices[i];
                    continue;
                }
                dp[i][k][0] = Math.max(dp[i-1][k][0], dp[i-1][k][1] + prices[i]);
                dp[i][k][1] = Math.max(dp[i-1][k][1], dp[i-1][k-1][0] - prices[i]);
            }
        }
        return dp[n-1][K][0];
    }
    // Reuse the solution of Leetcode 122 unlimited transactions.
    private int maxProfitWithInfinityTransaction(int[] prices) {
        int n = prices.length;
        int dp_i_0 = 0;
        int dp_i_1 = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int temp = dp_i_0;
            dp_i_0 = Math.max(dp_i_0, dp_i_1 + prices[i]);
            dp_i_1 = Math.max(dp_i_1, temp - prices[i]);
        }
        return dp_i_0;
    }

    // Solution 2:
    // Running Time Complexity: O(k * n), Space Complexity: O(k * n)
    public int maxProfit(int k, int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        // Add this method to avoid Memory Limit Exceed: 
        if (k >= n/2) {
            return maxProfit(prices);
        }
        int[][] dp = new int[k+1][n];
        for (int i = 1; i <= k; i++) {
            int maxDiff = -prices[0];
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.max(dp[i][j-1], prices[j] + maxDiff);
                maxDiff = Math.max(maxDiff, dp[i-1][j] - prices[j]);
            }
        }
        return dp[k][n-1];
    }

    private int maxProfit(int[] prices) {
        int maxProfit = 0, n = prices.length;
        for (int i = 1; i < n; i++) {
            if (prices[i] > prices[i-1]) {
                maxProfit += prices[i] - prices[i-1];
            }
        }
        return maxProfit;
    }

    // Solution 3:
    // Running Time Complexity: O(n * k), Space Complexity: O(k)
    public int maxProfit(int k, int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        if (k >= n/2) {
            return maxProfit(prices);
        }
        // 这个数组是代表j次交易的总收益减去买了第m天的股票, 0 <= m <= i-1
        int[] maxDiff = new int[k+1];
        int[] dp = new int[k+1];
        Arrays.fill(maxDiff, -prices[0]);
        for (int i = 1; i < n; i++) {
            for (int j = 1; j <= k; j++) {
                // 当前j次的最大收益，是之前天数的dp[j]，和交易了j次的最大收益，加上卖出第i天的股票
                dp[j] = Math.max(dp[j], maxDiff[j] + prices[i]);
                // 当前j次的最大收益，或者是j-1次最大收益，再买day i的股票prices[i]
                maxDiff[j] = Math.max(maxDiff[j], dp[j-1] - prices[i]);
            }
        }
        return dp[k];
    }
    
    private int maxProfit(int[] prices) {
        int maxProfit = 0, n = prices.length;
        for (int i = 1; i < n; i++) {
            if (prices[i] > prices[i-1]) {
                maxProfit += prices[i] - prices[i-1];
            }
        }
        return maxProfit;
    }
}