/**
We are given a list schedule of employees, which represents the working time for each employee.

Each employee has a list of non-overlapping Intervals, and these intervals are in sorted order.

Return the list of finite intervals representing common, positive-length free time for all employees, also in sorted order.

Example 1:

Input: schedule = [[[1,2],[5,6]],[[1,3]],[[4,10]]]
Output: [[3,4]]
Explanation:
There are a total of three employees, and all common
free time intervals would be [-inf, 1], [3, 4], [10, inf].
We discard any intervals that contain inf as they aren't finite.
 

Example 2:

Input: schedule = [[[1,3],[6,7]],[[2,4]],[[2,5],[9,12]]]
Output: [[5,6],[7,9]]

(Even though we are representing Intervals in the form [x, y], the objects inside are Intervals, not lists or arrays. For example, schedule[0][0].start = 1, schedule[0][0].end = 2, and schedule[0][0][0] is not defined.)

Also, we wouldn't include intervals like [5, 5] in our answer, as they have zero length.

Note:

schedule and schedule[i] are lists with lengths in range [1, 50].
0 <= schedule[i].start < schedule[i].end <= 10^8.
*/
// https://www.lintcode.com/problem/850/
// https://leetcode.com/problems/employee-free-time
public class Solution {

    class Interval {
        int start;
        int end;
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "[" + start + "," + end + "]";
        }
    }

    // Running Time Complexity: O(NlogN)
    public List<Interval> employeeFreeTime(List<List<Interval>> schedule) {
        int n = schedule.size();
        List<Interval> res = new ArrayList<>();
        if (n == 0) {
            return res;
        }
        List<Interval> intervals = new ArrayList<>();
        for (List<Interval> subList : schedule) {
            intervals.addAll(subList);
        }

        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));

        Interval prev = intervals.get(0);
        for (Interval curr : intervals) {
            if (prev.end < curr.start) {
                res.add(new Interval(prev.end, curr.start));
                prev = curr;
            }
            else if (prev.end < curr.end) {
                prev.end = curr.end;
            }
        }

        return res;
    }

    @Test
    public void test1() {
        // [[[1,3],[6,7]],[[2,4]],[[2,5],[9,12]]]
        List<List<Interval>> schedule = Arrays.asList(
                Arrays.asList(new Interval(1,3), new Interval(6,7)),
                Arrays.asList(new Interval(2,4)),
                Arrays.asList(new Interval(2,5), new Interval(9,12)));
        List<Interval> res = employeeFreeTime(schedule);
        System.out.println(res);
    }
}