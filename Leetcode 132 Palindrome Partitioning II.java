/**
Given a string s, partition s such that every substring of the partition is a palindrome.

Return the minimum cuts needed for a palindrome partitioning of s.

Example:

Input: "aab"
Output: 1
Explanation: The palindrome partitioning ["aa","b"] could be produced using 1 cut.
*/

public class Solution {
    /**
    Here is the basic idea of the solution:

    Initialize the 'cut' array: For a string with n characters s[0, n-1], it needs at most n-1 cut.
    Therefore, the 'cut' array is initialized as cut[i] = i-1

    Use two variables in two loops to represent a palindrome:
    The external loop variable 'i' represents the center of the palindrome.
    The internal loop variable 'j' represents the 'radius' of the palindrome. Apparently, j <= i is a must.
    This palindrome can then be represented as s[i-j, i+j].
    If this string is indeed a palindrome, then one possible value of cut[i+j] is cut[i-j] + 1,
    where cut[i-j] corresponds to s[0, i-j-1] and 1 correspond to the palindrome s[i-j, i+j];

    When the loops finish, you'll get the answer at cut[s.length]
    */
    // Running Time Complexity: O(N^2), Space Complexity: O(N)
    public int minCut(String s) {
        int n = s.length();
        int[] dp = new int[n+1];
        for (int i = 0; i <= n; i++) {
            dp[i] = i-1;
        }

        for (int i = 0; i < n; i++) {
            // odd length palindrome
            int j = 0;
            while (i-j >= 0 && i+j < n) {
                if (s.charAt(i-j) != s.charAt(i+j)) {
                    break;
                }
                dp[i+j+1] = Math.min(dp[i+j+1], dp[i-j] + 1);
                j++;
            }

            // even length palindrome
            j = 1;
            while (i-j+1 >= 0 && i+j < n) {
                if (s.charAt(i-j+1) != s.charAt(i+j)) {
                    break;
                }
                dp[i+j+1] = Math.min(dp[i+j+1], dp[i-j+1] + 1);
                j++;
            }
        }

        return dp[n];
    }
    /**
    if [j,i] is palindrome:
        dp[i] is the min cut of dp[j-1] + 1, j <= i
    [j, i] is palindrome is [j+1, i-1] is palindrome and s.charAt(j) == s.charAt(i)
    */
    // Running Time Complexity: O(N^2), Space Complexity: O(N^2)
    public int minCut(String s) {
        int n = s.length();
        int[] dp = new int[n];
        // pal[i][j]: substring(i, j) is the palindrome
        boolean[][] pal = new boolean[n][n];
        for (int r = 0; r < n; r++) {
            int min = r;
            for (int l = 0; l <= r; l++) {
                if (s.charAt(l) == s.charAt(r) && (l + 1 > r - 1 || pal[l+1][r-1])) {
                    pal[l][r] = true;
                    if (l == 0) {
                        min = 0;
                    }
                    else {
                        min = Math.min(min, dp[l-1]+1);
                    }
                }
            }
            dp[r] = min;
        }
        return dp[n-1];
    }

    // Best Solution:
    // Running Time Complexity: O(N ^ 2), Space Complexity: O(N)
    public int minCut(String s) {
        int n = s.length();
        char[] cList = s.toCharArray();
        int[] dp = new int[n];
        // initial array:
        for (int i = 0; i < n; i++) {
            dp[i] = i;
        }

        for (int i = 0; i < n; i++) {
            expandString(cList, i, i, n, dp);
            expandString(cList, i, i+1, n, dp);
        }
        return dp[n-1];
    }

    private void expandString(char[] cList, int l, int r, int n, int[] dp) {
        while (l >= 0 && r < n) {
            if (cList[l] != cList[r]) {
                break;
            }
            if (l == 0) {
                dp[r] = 0;
            }
            else {
                dp[r] = Math.min(dp[r], dp[l-1]+1);
            }
            l--;
            r++;
        }
    }

    // Bruth Force DP solution:
    // Running Time Complexity: O(N^3)
    // https://www.youtube.com/watch?v=lDYIvtBVmgo
    /**
    dp[i][j] represents the min cut of substring(i,j) so that substring(i, j) is palindrome
    1. For each string with 1 character, min cut = 0
    2. For each string with 2 characters,
            if two characters are the same, min cut = 1
            else, min cut = 1
    3.  For each string with 3 or more characters:
        if substring(i,j) is palindrom,
            dp[i][j] = 0
        else:
            dp[i][j] = 1 + min {
                        dp[i][k] + T[k+1][j], where k = i ... j-1
                    }
    */
    public int minCut(String s) {
        int n = s.length();
        if (isPalindrome(s, 0, n-1)) {
            return 0;
        }
        // dp[i][j] represents the min cut of substring(i,j) so that substring(i, j) is palindrome
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dp[i], n+1);
        }

        // For each string with 1 character, min cut = 0
        for (int i = 0; i < n; i++) {
            dp[i][i] = 0;
        }

        // For each string with 2 characters:
        for (int i = 0; i < n-1; i++) {
            if (s.charAt(i) == s.charAt(i+1)) {
                dp[i][i+1] = 0;
            }
            else {
                dp[i][i+1] = 1;
            }
        }

        // For each string with 3 or more characters:
        for (int l = 3; l <= n; l++) {
            for (int i = 0; i <= n-l; i++) {
                int j = i+l-1;
                if (isPalindrome(s, i, j)) {
                    dp[i][j] = 0;
                }
                else {
                    for (int k = i; k < j; k++) {
                        int curr = 1 + dp[i][k] + dp[k+1][j];
                        dp[i][j] = Math.min(dp[i][j], curr);
                    }
                }
            }
        }
        return dp[0][n-1];
    }

    private boolean isPalindrome(String s, int i, int j) {
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    // Running Time Complexity: O(N^3), Space Complexity: O(N^2)
    // https://www.youtube.com/watch?v=lDYIvtBVmgo
    public int minCut(String s) {
        int n = s.length();
        if (isPalindrome(s)) {
            return 0;
        }
        // dp[i][j]: minCut that makes substring(i, j) to be palindrome
        int[][] dp = new int[n][n];
        // pal[i][j]: substring(i, j) is the palindrome
        boolean[][] pal = new boolean[n][n];

        // length of each substring:
        for (int l = 1; l <= n; l++) {
            // start idx:
            for (int i = 0; i+l-1 < n; i++) {
                int j = i+l-1;
                if (l >= 3) {
                    pal[i][j] = s.charAt(i) == s.charAt(j) && pal[i+1][j-1];
                }
                else {
                    pal[i][j] = s.charAt(i) == s.charAt(j);
                }

                if (pal[i][j]) {
                    dp[i][j] = 0;
                }
                else {
                    int minVal = n-1;
                    for (int k = i; k < j; k++) {
                        int curr = 1 + dp[i][k] + dp[k+1][j];
                        minVal = Math.min(minVal, curr);
                    }
                    dp[i][j] = minVal;
                }
            }
        }
        return dp[0][n-1];
    }
}