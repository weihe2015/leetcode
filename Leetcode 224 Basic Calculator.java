/**
Implement a basic calculator to evaluate a simple expression string.

The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

You may assume that the given expression is always valid.

Some examples:
"1 + 1" = 2
" 2-1 + 2 " = 3
"(1+(4+5+2)-3)+(6+8)" = 23

1. digit: it should be one digit from the current number
2. '+': number is over, we can add the previous number and start a new number
3. '-': same as above
4. '(': push the previous result and the sign into the stack, set result to 0, just calculate the new result within the parenthesis.
5. '): pop out the top two numbers from stack, first one is the sign before this pair of parenthesis, 
second is the temporary result before this pair of parenthesis. We add them together.
*/
public class Solution {
    /**
     * Basic Idea: 
     *  1. for each digit, use num = 10 * num + (c - '0') to accumulate number
     *  2. For either + or -, use previous sign to add into result, and set sign to current char
     *  3. For (, push sign and res int stack
     *  4. For ), pop sign and res for previous result. calculate current res, acculate current res as res = prevResult + sign * res;
     *    Reset num = 0 to mark finish calculation
     *  5. If at the end, num != 0, use prevSign to calculate res and add it into res.
     */
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<Integer>();
        // Set first number as positive:
        int sum = 0, num = 0, sign = 1;
        for (char c : s.toCharArray()) {
            if(Character.isDigit(c)){
                // read number like "23" "412"
                num = 10 * num + (int)(c - '0');
            }
            else if(c == '+'){
                // save previous num into result with previous sign:
                sum += sign * number;
                // Reset positive sign for next number
                sign = 1;
                // reset num:
                num = 0;
            }
            else if(c == '-'){
                // save previous num into result with previous sign:
                sum += sign * num;
                // set negative sign for next number
                sign = -1;
                // reset num:
                num = 0;
            }
            else if(c == '('){
                // push previous result and previous sign into stack to save it for later
                stack.push(sum);
                stack.push(sign);
                // reset sign for positive:
                sign = 1;
                // reset sum and num:
                sum = 0; num = 0;
            }
            else if(c == ')'){
                // calculate res for current expression in parentheses
                sum += sign * num;
                int prevSign = stack.pop();
                int prevResult = stack.pop();
                sum = prevResult + prevSign * sum;
                // reset num to indicate finish of calculation
                num = 0;
            }
        }
        // handle the last number:
        if (num != 0) {
            sum += sign * num;
            // 
            sign = 1; num = 0;
        }
        return sum;
    }
}