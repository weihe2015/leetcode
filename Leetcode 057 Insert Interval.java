/**
Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

You may assume that the intervals were initially sorted according to their start times.

Example 1:

Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
Output: [[1,5],[6,9]]
Example 2:

Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
Output: [[1,2],[3,10],[12,16]]
Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].
*/
/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
public class Solution {

    /**
        Running Time Complexity: O(nlogn), Space Complexity: O(N)
    */
    public int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> intervalList = new ArrayList<>(Arrays.asList(intervals));
        intervalList.add(newInterval);
        
        // sort by start time
        Collections.sort(intervalList, Comparator.comparingInt(l -> l[0]));
        
        List<int[]> res = new ArrayList<>();
        int[] prev = new int[]{-1, -1};
        for (int[] curr : intervalList) {
            if (prev[0] == -1 || prev[1] < curr[0]) {
                res.add(curr);
                prev = curr;
            }
            else if (prev[1] <= curr[1]) {
                prev[1] = curr[1];
                int n = res.size();
                res.get(n-1)[1] = curr[1];
            }
        }
        int n = res.size();
        return res.toArray(new int[n][2]);
    }

    /**
    Running Time Complexity: O(N), Space Complexity: O(N);
    */ 
    public int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> res = new ArrayList<>();
        boolean insert = false;
        
        for (int[] interval : intervals) {
            if (insert) {
                res.add(interval);
            }
            else {
                // currInterval end times is far less than newInterval
                // currInterval ... newInterval,
                if (interval[1] < newInterval[0]) {
                    res.add(interval);
                }
                // newInterval is before the current interval:
                else if (newInterval[1] < interval[0]) {
                    res.add(newInterval);
                    res.add(interval);
                    insert = true;
                }
                // overlapped:
                else {
                    newInterval[0] = Math.min(newInterval[0], interval[0]);
                    newInterval[1] = Math.max(newInterval[1], interval[1]);
                }
            }
        }
        if (!insert) {
            res.add(newInterval);
        }
        int n = res.size();
        return res.toArray(new int[n][2]);
    }

    // Running Time Complexity: O(N)
    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> result = new ArrayList<Interval>();
        if (intervals == null || intervals.isEmpty()) {
            result.add(newInterval);
            return result;
        }
        int i = 0；
        int n = intervals.size();
        int start = newInterval.start;
        int end = newInterval.end;
        for (; i < n; i++) {
            Interval currInterval = intervals.get(i);
            if (currInterval.end < start) {
                result.add(currInterval);
            }
            // no overlap with new interval, break and add the rest intervals at the end.
            else if (currInterval.start > end) {
                break;
            }
            // Current Interval overlaps with newInterval:
            else {
                start = Math.min(start, currInterval.start);
                end = Math.max(end, currInterval.end);
            }
        }
        result.add(new Interval(start, end));
        // add the rest 
        for (int j = i; j < n; j++) {
            result.add(intervals.get(j));
        }
        return result;
    }

    // Use the idea of Merge Interval Leetcode 56
    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        intervals.add(newInterval);
        return merge(intervals);
    }
    
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> res = new ArrayList<>();
        if (intervals == null || intervals.isEmpty()) {
            return res;
        }
        // sort by starting time.
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        Interval prevInterval = null;
        for (Interval interval : intervals) {
            // add non-overlap interval:
            if (prevInterval == null || prevInterval.end < interval.start) {
                res.add(interval);
                prevInterval = interval;
            }
            else if (prevInterval.end <= interval.end) {
                prevInterval.end = interval.end;
            }
        }
        return res;
    }

    private List<Interval> merge(List<Interval> intervals) {
        int n = intervals.size();
        int[] starts = new int[n];
        int[] ends = new int[n];
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        
        Arrays.sort(starts);
        Arrays.sort(ends);
        List<Interval> result = new ArrayList<Interval>();
        int j = 0;
        for (int i = 0; i < n; i++) {
            if (i == n-1 || starts[i+1] > ends[i]) {
                result.add(new Interval(starts[j], ends[i]));
                j = i+1;
            }
        }
        return result;
    }
}