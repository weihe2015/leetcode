/**
Given a list of non-negative numbers and a target integer k, 
write a function to check if the array has a continuous subarray of size at least 2 that sums up to the multiple of k, 
that is, sums up to n*k where n is also an integer.

Example 1:
Input: [23, 2, 4, 6, 7],  k=6
Output: True
Explanation: Because [2, 4] is a continuous subarray of size 2 and sums up to 6.
Example 2:
Input: [23, 2, 6, 4, 7],  k=6
Output: True
Explanation: Because [23, 2, 6, 4, 7] is an continuous subarray of size 5 and sums up to 42.
*/

public class Solution {
    public boolean checkSubarraySum(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return false;
        }
        // key -> sum, value -> index:
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int sum = 0;
        for (int i = 0, max = nums.length; i < max; i++) {
            sum += nums[i];
            if (k != 0) {
                sum %= k;
            }
            if (!map.containsKey(sum)) {
                map.put(sum, i);
            }
            else {
                int index_j = map.get(sum);
                // size of array is at least 2
                if (i - index_j >= 2) {
                    return true;
                }
            }
        }
        return false;
    }
}