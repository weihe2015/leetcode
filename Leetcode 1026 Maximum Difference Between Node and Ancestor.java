/**
Given the root of a binary tree, find the maximum value V for
which there exist different nodes A and B where V = |A.val - B.val| and A is an ancestor of B.

A node A is an ancestor of B if either: any child of A is equal to B,
or any child of A is an ancestor of B.

Example 1:
Input: root = [8,3,10,1,6,null,14,null,null,4,7,13]
Output: 7
Explanation: We have various ancestor-node differences, some of which are given below :
|8 - 3| = 5
|3 - 7| = 4
|8 - 1| = 7
|10 - 13| = 3
Among all possible differences, the maximum value of 7 is obtained by |8 - 1| = 7.

Example 2:
Input: root = [1,null,2,null,0,3]
Output: 3
*/
public class Solution {
    /**
    就一个节点来说所谓最大差值，就是祖先的最大值或者最小值和自己的val的差值。
    只需要知道所有祖先可能的最大值和最小值，在遍历时携带传递即可。
    */
    private int res;
    public int maxAncestorDiff(TreeNode root) {
        res = Integer.MIN_VALUE;
        helper(root, root.val, root.val);
        return res;
    }

    private void helper(TreeNode node, int min, int max) {
        if (node == null) {
            return;
        }
        int val1 = Math.abs(node.val - max);
        int val2 = Math.abs(node.val - min);

        res = Math.max(res, Math.max(val1, val2));

        min = Math.min(min, node.val);
        max = Math.max(max, node.val);

        helper(node.left, min, max);
        helper(node.right, min, max);
    }
}