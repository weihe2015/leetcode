Given a non-empty binary search tree and a target value, find the value in the BST that is closest to the target.

Note:
Given target value is a floating point.
You are guaranteed to have only one unique value in the BST that is closest to the target.

public class solution {
	// recursive
	public int closestValue(TreeNode root, double target) {
	    int a = root.val;
	    TreeNode kid = target < a ? root.left : root.right;
	    if (kid == null) return a;
	    int b = closestValue(kid, target);
	    return Math.abs(a - target) < Math.abs(b - target) ? a : b;
	}

	// iterative
	public int closestValue(TreeNode root, double target) {
		int closest = root.val;
		while(root != null){
			if(Math.abs(closest - target) >= Math.abs(root.val - target))
				closest = root.val;
			root = target < root.val ? root.left : root.right;
		}
		return closest;
	}
}