/**
Given a list of unique words, find all pairs of distinct indices (i, j) in the given list, 
so that the concatenation of the two words, i.e. words[i] + words[j] is a palindrome.

Example 1:

Input: ["abcd","dcba","lls","s","sssll"]
Output: [[0,1],[1,0],[3,2],[2,4]] 
Explanation: The palindromes are ["dcbaabcd","abcddcba","slls","llssssll"]
Example 2:

Input: ["bat","tab","cat"]
Output: [[0,1],[1,0]] 
Explanation: The palindromes are ["battab","tabbat"]
*/

public class Solution {
    // Brute Force Solution:
    // Running Time Complexity: O(N^2 * 2L)
    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> result = new ArrayList<>();
        if (words.length < 2) {
            return result;
        }
        int n = words.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    continue;
                }
                else {
                    StringBuffer sb = new StringBuffer();
                    sb.append(words[i]);
                    sb.append(words[j]);
                    if (isPalindrome(sb.toString())) {
                        List<Integer> list = new ArrayList<>();
                        list.add(i);
                        list.add(j);
                        result.add(list);
                    }
                }
            }
        }
        return result;
    }
    
    private boolean isPalindrome(String s) {
        int l = 0, r = s.length() - 1;
        while (l < r) {
            if (s.charAt(l) != s.charAt(r)) {
                return false;
            }
            l++; r--;
        }
        return true;
    }

    // Solution 2: Use Trie
    /**
     *  1) Create an empty Trie.
        2) Do following for every word:-
            a) Insert reverse of current word.
            b) Also store up to which index it is a palindrome.
        3) Traverse list of words again and do following for every word.
            a) If it is available in Trie then return true
            b) If it is partially available
                Check the remaining word is palindrome or not 
                If yes then return true that means a pair
                forms a palindrome.
                Note: Position upto which the word is palindrome
                    is stored because of these type of cases.
     * */ 
    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> res = new ArrayList<>();
        if (words == null || words.length == 0) {
            return res;
        }
        TrieNode root = new TrieNode();
        int n = words.length;
        for (int i = 0; i < n; i++) {
            String word = words[i];
            addWord(root, word, i);
        }
        
        for (int i = 0; i < n; i++) {
            String word = words[i];
            searchWord(res, root, word, i);
        }
        return res;
    }
    
    private void searchWord(List<List<Integer>> res, TrieNode root, String word, int idx) {
        TrieNode curr = root;
        for (int i = 0, n = word.length(); i < n; i++) {
            char c = word.charAt(i);
            if (curr.idx != -1 && curr.idx != idx && isPalindrome(word, i, n-1)) {
                res.add(Arrays.asList(idx, curr.idx));
            }
            if (curr.next[c-'a'] == null) {
                return;
            }
            curr = curr.next[c-'a'];
        }
        // abcd -> dcba case:
        if (curr.idx != -1 && curr.idx != idx) {
            res.add(Arrays.asList(idx, curr.idx));
        }
        // s -> sslls case:
        for (int pos : curr.palindromePositions) {
            if (pos != idx) {
                res.add(Arrays.asList(idx, pos));
            }
        }
    }
    
    private void addWord(TrieNode root, String word, int idx) {
        TrieNode curr = root;
        for (int n = word.length(), i = n-1; i >= 0; i--) {
            char c = word.charAt(i);
            if (curr.next[c-'a'] == null) {
                curr.next[c-'a'] = new TrieNode();
            }
            // if word[0, i] is palindrome, we add current word idx to palindrome positions lists:
            if (isPalindrome(word, 0, i)) {
                curr.palindromePositions.add(idx);
            }
            curr = curr.next[c-'a'];
        }
        curr.idx = idx;
    }
    
    private boolean isPalindrome(String str, int i, int j) {
        while (i < j) {
            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
            i++; j--;
        }
        return true;
    }
    
    class TrieNode {
        // index of this word of original words array
        int idx;
        TrieNode[] next;
        // To store palindrome positions of words
        List<Integer> palindromePositions;
        public TrieNode() {
            this.idx = -1;
            this.next = new TrieNode[26];
            this.palindromePositions = new ArrayList<>();
        }
    }
}