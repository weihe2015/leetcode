/**
Given two arrays, write a function to compute their intersection.

Example 1:

Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]
Example 2:

Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [9,4]
Note:

Each element in the result must be unique.
The result can be in any order.
*/
public class Solution {

	// O(m+n) time, O(max(m,n)) space
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set = new LinkedHashSet<>();
        for (int num : nums1) {
            set.add(num);
        }
        Set<Integer> intersec = new LinkedHashSet<>();
        for (int num : nums2) {
            if (set.contains(num)) {
                intersec.add(num);
            }
        }
        int[] res = new int[intersec.size()];
        int i = 0;
        for (int num : intersec) {
            res[i++] = num;
        }
        return res;
    }

    // Two nested for loop:
    // Runtime: O(m*n), Space: O(max(m,n))
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set = new LinkedHashSet<>();
        for (int i = 0, max_i = nums1.length; i < max_i; i++) {
            for (int j = 0, max_j = nums2.length; j < max_j; j++) {
                if (nums1[i] == nums2[j]) {
                    set.add(nums1[i]);
                }
            }
        }
        int[] res = new int[set.size()];
        int i = 0;
        for (int num : set) {
            res[i++] = num;
        }
        return res;
    }

    // Use Binary Search:
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> intersec = new LinkedHashSet<>();
        Arrays.sort(nums2);
        for (int num : nums1) {
            if (binarySearch(nums2, num)) {
                intersec.add(num);
            }
        }
        int[] res = new int[intersec.size()];
        int i = 0;
        for (int num : intersec) {
            res[i++] = num;
        }
        return res;
    }

    private boolean binarySearch(int[] nums, int target) {
        int low = 0, high = nums.length-1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return true;
            }
            if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return false;
    }

    // O(nlogn) time. O(n) space
    public int[] intersection(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        Set<Integer> set = new HashSet<Integer>();
        int i = 0, j = 0, n = nums1.length, m = nums2.length;
        while(i < n && j < m){
            if(nums1[i] < nums2[j])
                i++;
            else if(nums1[i] > nums2[j])
                j++;
            else{
                set.add(nums1[i]);
                i++;
                j++;
            }
        }
        int[] result = new int[set.size()];
        i = 0;
        for(int num : set){
            result[i++] = num;
        }
        return result;
    }
}