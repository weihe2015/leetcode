/**
Given a 2D board containing 'X' and 'O' (the letter O), capture all regions surrounded by 'X'.

A region is captured by flipping all 'O's into 'X's in that surrounded region.

Example:

X X X X
X O O X
X X O X
X O X X
After running your function, the board should be:

X X X X
X X X X
X X X X
X O X X
Explanation:

Surrounded regions shouldn’t be on the border, which means that any 'O' on the border of the board are not flipped to 'X'.
Any 'O' that is not on the border and it is not connected to an 'O' on the border will be flipped to 'X'.
Two cells are connected if they are adjacent cells connected horizontally or vertically.
*/
public class Solution {
    // Union Find Solution:
    /**
    The idea comes from the observation that if a region is NOT captured,
    it is connected to the boundry. So if we connect all the 'O' nodes on the boundry to a dummy node,
    and then connect each 'O' node to its neighbour 'O' nodes,
    then we can tell directly whether a 'O' node is captured by checking whether it is connected to the dummy node.
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public void solve(char[][] board) {
        if (board == null || board.length == 0) {
            return;
        }
        int m = board.length, n = board[0].length;
        int N = m * n + 1;
        UnionFind uf = new UnionFind(N);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    int idx = i * n + j;
                    // connect any 'O' node which is on the boundary to a dumy node:
                    if (i == 0 || i == m-1 || j == 0 || j == n-1) {
                        uf.union(idx, N-1);
                    }
                    else {
                        // connect a 'O' node to its neighbour 'O' nodes
                        for (int[] dir : dirs) {
                            int x = i + dir[0];
                            int y = j + dir[1];
                            int newIdx = x * n + y;
                            if (board[x][y] == 'O') {
                                uf.union(idx, newIdx);
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int idx = i * n + j;
                // if a 'O' node is not connected to the dummy node, it is captured and convert it to 'X'
                if (board[i][j] == 'O' && !uf.isConnected(idx, N-1)) {
                    board[i][j] = 'X';
                }
            }
        }
    }

    class UnionFind {
        int[] id;
        int[] size;

        public UnionFind(int N) {
            id = new int[N];
            size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }

        public boolean isConnected(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            return pRoot == qRoot;
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
        }
    }

    // Solution 2:
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public void solve(char[][] board) {
        if (board == null || board.length == 0) {
            return;
        }
        int m = board.length, n = board[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // Any 'O' connected to the boundary cannot be turned to 'X', so mark them as '*'
                if (i == 0 || i == m-1 || j == 0 || j == n-1) {
                    dfs(board, i, j, m, n);
                }
            }
        }

        // post-processing the board: turn any '*' back to 'O' and turn any 'O' to 'X'
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
                else if (board[i][j] == '*') {
                    board[i][j] = 'O';
                }
            }
        }
    }

    private void dfs(char[][] board, int i, int j, int m, int n) {
        if (i < 0 || i >= m || j < 0 || j >= n) {
            return;
        }
        if (board[i][j] != 'O') {
            return;
        }
        board[i][j] = '*';
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(board, x, y, m, n);
        }
    }
}