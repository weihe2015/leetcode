/**
You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.

Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.

Example 1:

Input: [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
             Total amount you can rob = 1 + 3 = 4.
Example 2:

Input: [2,7,9,3,1]
Output: 12
Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
Total amount you can rob = 2 + 9 + 1 = 12.
*/

public class Solution {

    // DP solution O(N) time O(N) space
    public int rob(int[] nums) {
        int n = nums.length;
        if (n <= 1) {
            return n == 0 ? 0 : nums[0];
        }
        int[] dp = new int[n];
        dp[0] = nums[0];
        // Either rob the first one or the second one.
        dp[1] = Math.max(nums[0], nums[1]);
        for(int i = 2; i < n; i++){
            // either rob the current one, nums[i], and add dp[i-2]
            // or not rob the current one, = dp[i-1]
            dp[i] = Math.max(dp[i-1], dp[i-2] + nums[i]);
        }
        return dp[n-1];
    }

    // DP solution O(N) time O(1) space
    public int rob(int[] nums) {
        int n = nums.length;
        if (n <= 1) {
            return n == 0 ? 0 : nums[0];
        }
        int dp_0 = nums[0];
        int dp_1 = Math.max(nums[0], nums[1]);
        for (int i = 2; i < n; i++) {
            int dp_i = Math.max(dp_1, nums[i] + dp_0);
            dp_0 = dp_1;
            dp_1 = dp_i;
        }
        return dp_1;
    }

    // DFS with memo:
    private int[] memo;
    public int rob(int[] nums) {
        int n = nums.length;
        this.memo = new int[n];
        return dfs(nums, 0, n);
    }
    
    private int dfs(int[] nums, int idx, int n) {
        if (idx >= nums.length) {
            return 0;
        }
        if (memo[idx] != -1) {
            return memo[idx];
        }
        // 不抢, 去下家 | 抢这家，去下下家：
        int res = Math.max(dfs(nums, idx+1, n), nums[idx] + dfs(nums, idx+2, n));
        memo[idx] = res;
        return res;
    }

    public int rob(int[] nums) {
        if (nums.length <= 0 || nums == null) {
            return 0;
        }
        int even = 0;
        int odd = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i % 2 == 0) {
                even += nums[i];
                even = even > odd ? even : odd;
            }
            else {
                odd += nums[i];
                odd = even > odd ? even : odd;
            }
        }
        return even > odd ? even : odd;
    }

    // math
    public int rob(int[] nums) {
        int rob = 0, notRob = 0;
        for(int n : nums){
            int temp = notRob;
            notRob = Math.max(rob, notRob);
            rob = n + temp;
        }
        return Math.max(rob,notRob);
    }
}