/**
Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Example:

Input: [0,1,0,3,12]
Output: [1,3,12,0,0]
Note:

You must do this in-place without making a copy of the array.
Minimize the total number of operations.
*/

public class Solution {
    // Solution on 1-8-19:
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public void moveZeroes(int[] nums) {
        int n = nums.length;
        if (n == 0) {
            return;
        }
        int idx = 0;
        for (int i = 0; i < n; i++) {
            if (nums[i] != 0) {
               nums[idx++] = nums[i];
            }
        }
        // mark the rest of array as 0.
        for (int i = idx; i < n; i++) {
            nums[i] = 0;
        }
    }

    // my solution
    public void moveZeroes(int[] nums) {
        int n = nums.length;
        if (n < 2)
            return;
        int i = 0;
        int j = 1;
        while (j <= n - 1) {
            if (nums[i] != 0) {
                i++;
                j++;
            }
            else if (nums[j] == 0) {
                j++;
            }
            else {
                 swap(nums,i,j);
                 i++;
                 j++;
            }
        }
    }

    public void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}