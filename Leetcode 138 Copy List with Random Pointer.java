/**
A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.

Return a deep copy of the list.

The Linked List is represented in the input/output as a list of n nodes. Each node is represented as a pair of [val, random_index] where:

val: an integer representing Node.val
random_index: the index of the node (range from 0 to n-1) where random pointer points to, or null if it does not point to any node.
 

Example 1:

Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]

Example 2:
Input: head = [[1,1],[2,1]]
Output: [[1,1],[2,1]]

Example 3:
Input: head = [[3,null],[3,0],[3,null]]
Output: [[3,null],[3,0],[3,null]]

Example 4:
Input: head = []
Output: []

Explanation: Given linked list is empty (null pointer), so return null.
*/
/**
 * Definition for singly-linked list with a random pointer.
 * class RandomListNode {
 *     int label;
 *     RandomListNode next, random;
 *     RandomListNode(int x) { this.label = x; }
 * };
 */
public class Solution {

    // Solution 1: Without using hashMap:
    /**
    Ex: 1 -> 2 -> 3
        1.random = 3
        3.random = 2
        2.random = null

    1. 根据遍历到的原节点创建对应的新节点，每个新创建的节点是在原节点后面，比如下图中原节点1不再指向原原节点2，而是指向新节点1
    2. 是最关键的一步，用来设置新链表的随机指针
       2.1 原节点1的随机指针指向原节点3，新节点1的随机指针指向的是原节点3的next
       2.2 原节点3的随机指针指向原节点2，新节点3的随机指针指向的是原节点2的next
    
       原节点i的随机指针(如果有的话)，指向的是原节点j
       那么新节点i的随机指针，指向的是原节点j的next
    3. 只要将两个链表分离开，再返回新链表就可以了
       
    */
    // https://leetcode-cn.com/problems/copy-list-with-random-pointer/solution/liang-chong-shi-xian-tu-jie-138-fu-zhi-dai-sui-ji-/
    public Node copyRandomList(Node head) {
        if (head == null) {
            return null;
        }
        Node curr = head;
        // 1. create new node after each original node:
        while (curr != null) {
            Node node = new Node(curr.val);
            node.next = curr.next;
            curr.next = node;

            curr = node.next;
        }
        
        curr = head;
        // 2. create random pointer of these new nodes:
        while (curr != null) {
            if (curr.random != null) {
                curr.next.random = curr.random.next;
            }
            curr = curr.next.next;
        }
        
        // 3. separate two lists:
        Node dummyHead = new Node(0);
        curr = head;
        Node newCurr = dummyHead;

        while (curr != null) {
            newCurr.next = curr.next;
            newCurr = curr.next;
            curr.next = newCurr.next;
            curr = newCurr.next;
        }

        return dummyHead.next;
    }

    // Solution 2: with HashMap:

    /**
    1. 原节点作为key，新节点作为value放入哈希表中
    2. 再遍历原链表，这次我们要将新链表的next和random指针给设置上

    map.get(原节点)，得到的就是对应的新节点
    map.get(原节点.next)，得到的就是对应的新节点.next
    map.get(原节点.random)，得到的就是对应的新节点.random

    新节点.next -> map.get(原节点.next)
    新节点.random -> map.get(原节点.random)
    */
    public Node copyRandomList(Node head) {
        if (head == null) {
            return null;
        }
        // key -> original node, val -> new copied node
        Map<Node, Node> map = new HashMap<>();
        Node curr = head;
        while (curr != null) {
            Node node = new Node(curr.val);
            map.put(curr, node);
            curr = curr.next;
        }
        
        // set next and random pointer:
        curr = head;
        while (curr != null) {
            Node node = map.get(curr);
            if (curr.random != null) {
                node.random = map.get(curr.random);
            }
            if (curr.next != null) {
                node.next = map.get(curr.next);
            }
            curr = curr.next;
        }
        return map.get(head);
    }

    public RandomListNode copyRandomList(RandomListNode head) {
        RandomListNode iter = head, next;
        // First round: make copy of each node,
        // and link them together side-by-side in a single list.
        while (iter != null) {
            next = iter.next;
            RandomListNode copy = new RandomListNode(iter.label);
            iter.next = copy;
            copy.next = next;
            iter = next;
        }
        // Second round: assign random pointers for the copy nodes.
        iter = head;
        while (iter != null) {
            if (iter.random != null) {
                iter.next.random = iter.random.next;
            }
            iter = iter.next.next;
        }
        // Third round: restore the original list, and extract the copy list.
        iter = head;
        RandomListNode pseudoHead = new RandomListNode(0);
        RandomListNode copy, copyIter = pseudoHead;
        while (iter != null) {
            next = iter.next.next;
            // extract the copy
            copy = iter.next;
            copyIter.next = copy;
            copyIter = copy;
            // restore the original list
            iter.next = next;
            iter = next;
        }
        return pseudoHead.next;
    }
}