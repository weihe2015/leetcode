/**
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times) with the following restrictions:

You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
After you sell your stock, you cannot buy stock on next day. (ie, cooldown 1 day)
Example:

Input: [1,2,3,0,2]
Output: 3 
Explanation: transactions = [buy, sell, cooldown, buy, sell]
*/

public class Solution {
    /*
    转移方程:
    无限制交易次数，跟Leetcode 122 相似
    但是买的时候，要从i-2 的状态转移:

    dp[i][k][0] = Math.max(dp[i-1][k][0], dp[i-1][k][1] + prices[i])
    dp[i][k][1] = Math.max(dp[i-1][k][1], dp[i-2][k-1][0] - prices[i])
                = Math.max(dp[i-1][k][1], dp[i-2][k][0] - prices[i])

    我们发现数组中的 k 已经不会改变了，也就是说不需要记录 k 这个状态了：
    dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i])
    dp[i][1] = Math.max(dp[i-1][1], dp[i-2][0] - prices[i])

    */
    // O(N) time, O(N) space
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int[][] dp = new int[n][2];
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                // 没有持有股票
                dp[i][0] = 0;
                // 买入股票
                dp[i][1] = -prices[i];
            }
            else if (i == 1) {
                // max(休息，选择sell)
                dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i]);
                // max(休息, 选择buy)
                dp[i][1] = Math.max(dp[i-1][1], -prices[i]);
            }
            else {
                dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i]);
                dp[i][1] = Math.max(dp[i-1][1], dp[i-2][0] - prices[i]);
            }
        }
        return dp[n-1][0];
    }

    // O(N) time, O(1) Space:
    public int maxProfit(int[] prices) {
        int n = prices.length;
        if (n == 0) {
            return 0;
        }
        int dp_i_0 = 0;
        int dp_i_1 = -prices[0];
        // 代表 dp[i-2][0]
        int dp_pre_0 = 0;
        
        for (int i = 1; i < n; i++) {
            int temp = dp_i_0;
            dp_i_0 = Math.max(dp_i_0, dp_i_1 + prices[i]);
            dp_i_1 = Math.max(dp_i_1, dp_pre_0 - prices[i]);
            dp_pre_0 = temp;
        }
        return dp_i_0;
    }

    // Solution 2:
    /* Define dp[i] - maximum profit can be made on day i  following the cool down rule*/
     
    // O(N) time, O(N) space
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int[] dp = new int[n+1];
        int maxDiff = -prices[0];
        for (int i = 1; i < n; i++) {
            // i+1 day profit = i day profit, or sell the stock prices[i]
            dp[i+1] = Math.max(dp[i], prices[i] + maxDiff);
            // 当前i天的最大收益，或者是i-1天的最大收益，再买day i的股票prices[i]
            maxDiff = Math.max(maxDiff, dp[i-1] - prices[i]);
        }
        return dp[n];
    }

    // state machine with O(n) space
    // "https://leetcode.com/discuss/72030/share-my-dp-solution-by-state-machine-thinking" 
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int[] s0 = new int[n], s1 = new int[n], s2 = new int[n];
        // At the start, you don't have any stock if you just rest
        s0[0] = 0;
        // After buy, you should have -prices[0] profit.
        s1[0] = -prices[0];
        // Lower base case
        s2[0] = Integer.MIN_VALUE;
        for (int i = 1; i < n; i++) {
            // Stay at s0, or rest from s2
            s0[i] = Math.max(s0[i-1], s2[i-1]);
            // Stay at s1, or buy from s0
            s1[i] = Math.max(s1[i-1], s0[i-1] - prices[i]);
            s2[i] = s1[i-1] + prices[i];
        }
        return Math.max(s0[n-1], s2[n-1]);
    }
    
    // state machine with O(1) space
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        // At the start, you don't have any stock if you just rest
        int s0 = 0;
        // After buy, you should have -prices[0] profit.
        int s1 = -prices[0];
        // Lower base case
        int s2= Integer.MIN_VALUE;
        for (int i = 1; i < n; i++) {
            int temp = s2;
            // Only one way from s1 to s2
            s2 = s1 + prices[i];
            // Stay at s1, or buy from s0
            s1 = Math.max(s1, s0 - prices[i]);
            // Stay at s0, or rest from s2
            s0 = Math.max(s0, temp);
        }
        return Math.max(s0, s2);
    }
    
}