/**
Given a singly linked list, return a random node's value from the linked list. 
Each node must have the same probability of being chosen.

Follow up:
What if the linked list is extremely large and its length is unknown to you? 
Could you solve this efficiently without using extra space?

Example:

// Init a singly linked list [1,2,3].
ListNode head = new ListNode(1);
head.next = new ListNode(2);
head.next.next = new ListNode(3);
Solution solution = new Solution(head);

// getRandom() should return either 1, 2, or 3 randomly. Each element should have equal probability of returning.
solution.getRandom();
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
public class Solution {
    /**
    https://leetcode-cn.com/problems/linked-list-random-node/solution/zhong-ju-si-wei-by-li-zi-he/

    当走到最后一个节点 n 时，要保证最后一个节点被选取的概率是1/n
    这时候以 1/n 概率选取这个值，并且替换当前的值
    这时候再往钱一步，要保证n-1 最终被选取的概率是1/n，那么当走到n-1的时候，n-1最终被选取的概率是x * (1-1/n) = 1/n => x = 1/n-1
    含义是，n-1被选到，且n没有被选到

    以上思维，可以推广选取k个，在看n-1是否最终被保留下来，x * (1-k/n * 1/k) = k/n => x = k/n
    含义就是n 被选取了，并且将集合中的n-1 给替换掉了，概率就是从k 个里面选1个，n-1 先被x 概率选取，而后又被n 替换掉
    */

    static private final Random rand = new Random();
    private ListNode head;
    
    /** @param head The linked list's head.
        Note that the head is guaranteed to be not null, so it contains at least one node. */
    public Solution(ListNode head) {
        this.head = head;
    }
    
    /** Returns a random node's value. */
    public int getRandom() {
        if (head == null) {
            return -1;
        }
        ListNode curr = head;
        //首先默认抽取第 1 个节点
        int val = curr.val;
        curr = curr.next;
        //已经存在的节点数为 1
        int idx = 1;
        while (curr != null) {
            //节点数 + 1
            idx++;
            /*
                根据随机数，判断当前节点是否能够替换已经选择的节点，有 1/i 的几率能够替换
                random.nextInt(i) == 0 ：
                random.nextInt(i) 表示获取等概率获取 [0, i - 1] 任意一个整数，
                这里实际上 == [0, i - 1] 任意一个数都行，因为都是 1/i 的概率，
                如果要说语义的话，我们开一个大小为 1 的数组，0 号位置的数据是我们抽取的那一节点
                == 0 的语义是表示将该节点替换到 0 号位置，即替换掉我们抽取到的那一节点
                
                根据这个语义去理解 抽取 m 个的更好理解
            */
            if (rand.nextInt(idx) == 0) {
                val = curr.val;
            }
            curr = curr.next;
        }
        return val;
    }
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(head);
 * int param_1 = obj.getRandom();
 */