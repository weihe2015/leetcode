/**
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
*/
public class Solution {
    // "abcabcbb" => abc => 3
    // "bbbbb" => b => 1
    // Running Time Complexity: O(n);
    // Space Complexity: O(1)
    public int lengthOfLongestSubstring(String s) {
        int n = s.length()
        if (n == 0) {
            return 0;
        }
        int[] map = new int[256];
        int maxLen = 0;
        int l = 0;
        int r = 0;
        // number of non-unique character in substring(l,r)
        int duplicateCount = 0;
        while (r < n) {
            if (duplicateCount == 0) {
                char rc = s.charAt(r);
                // When there is duplicate character, meaning map[rc] >= 1, we increment the count by 1
                if (map[rc] == 1) {
                    duplicateCount++;
                }
                map[rc]++;
                r++;
            }
            // when substring(l,r) contains duplicate character, shrink the window to find the match: 
            while (duplicateCount > 0) {
                char lc = s.charAt(l);
                if (map[lc] == 2) {
                    duplicateCount--;
                }
                map[lc]--;
                l++;
            }
            // when substring(l,r) contains only unique character, calculate the length:
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // O(n) time O(n) space
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int max = 0;
        int l = 0, r = 0, n = s.length();
        Set<Character> set = new HashSet<Character>();
        // 设下标 l 和 r, 把左开右闭 [l, r) 想象成一个窗口。
        // 当 s[r] 和窗口内字符重复时， 则 l 向右滑动，缩小窗口。
        // 当s[r] 和窗口内字符不重复时，则 r 向右滑动，扩大窗口，此时窗口内的字符串一个无重复子字符串。
        while (r < n) {
            char lc = s.charAt(l);
            char rc = s.charAt(r);
            if (!set.contains(rc)) {
                set.add(rc);
                r++;
            }
            else {
                set.remove(lc);
                l++;
            }
            max = Math.max(max, set.size());
        }
        return max;
    }

    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int max = 0, l = 0, r = 0, n = s.length();
        // Key -> character, Value -> its index of last apperance.
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        while (r < n) {
            char c = s.charAt(r);
            if (!map.containsKey(c)) {
                max = Math.max(max, r-l+1);
            }
            else {
                // If the duplicated element is in current window, move left pointer to its right.
                int prevIdx = map.get(c);
                if (prevIdx >= l && prevIdx <= r) {
                    l = prevIdx + 1;
                }
                // If the duplicated element is not in current window, include it into the window:
                else {
                    max = Math.max(max, r-l+1);
                }
            }
            map.put(c, r);
            r++;
        }
        return max;
    }

    // O(n) time, O(1) space
    public int lengthOfLongestSubstring(String s){
        if(s == null || s.length() == 0)
            return 0;
        int[] lastIndex = new int[256];
        Arrays.fill(lastIndex,-1);
        int max = 0;
        int curr = 0;
        int start = 0;
        int best_Start = 0;
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(lastIndex[c] < start){
                curr++;
            }
            else{
                int last = lastIndex[c];
                start = last+1;
                curr = i - start + 1;
            }
            lastIndex[c] = i;
            if(curr > max){
                max = curr;
                best_Start = start;
            }
        }
        return max;
    }
}


public class Longest_Repeated_Character_Substring {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        String s = "abbbcbbbbcccccc";
        System.out.println(find(s));
    }
    // abbbc -> bbb, abbbcbb -> bbb
    public static String find(String s){
        int j = 0;
        int i = 1;
        int max = 0;
        char c = '#';
        for(i = 1; i < s.length(); i++){
            char c1 = s.charAt(j);
            char c2 = s.charAt(i);
            if(c1 != c2){
                j = i;
            }
            else{
                max = Math.max(max, i - j + 1);
                c = c1;
            }
        }
        StringBuilder sb = new StringBuilder();
        while(max > 0){
            sb.append(c);
            max--;
        }
        return sb.toString();
    }
}
