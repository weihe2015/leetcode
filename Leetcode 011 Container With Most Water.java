/*
Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai). 
n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). 
Find two lines, which together with x-axis forms a container, such that the container contains the most water.

Note: You may not slant the container.
*/

public class Solution {
    // Running Time Complexity: O(n^2), Space Complexity: O(1)
    public int maxArea(int[] heights) {
        int maxArea = 0;
        for (int i = 0, max = heights.length; i < max; i++) {
            for (int j = i+1; j < max; j++) {
                int minHeight = Math.min(heights[i], heights[j]);
                int currArea = minHeight * (j - i);
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }
    /**
    Initially we consider the area constituting the exterior most lines. 
    Now, to maximize the area, we need to consider the area between the lines of larger lengths. 
    If we try to move the pointer at the longer line inwards, we won't gain any increase in area, 
    since it is limited by the shorter line. But moving the shorter line's pointer could turn out to be beneficial, 
    as per the same argument, despite the reduction in the width. 
    This is done since a relatively longer line obtained by moving the shorter line's 
    pointer might overcome the reduction in area caused by the width reduction.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    // One Pass: use two pointer, low & high. Calculate the rectangle area
    // Move low pointer when height[low] < height[high]
    // Move high pointer when height[low] >= height[high]
    public int maxArea(int[] height) {
        int n = height.length;
        int maxArea = 0;
        int low = 0;
        int high = n - 1;

        while (low < high) {
            int minHeight = Math.min(height[low],height[high]);
            int currArea = minHeight * (high - low);
            maxArea = Math.max(maxArea, currArea);
            if (height[low] < height[high]) {
                low++;
            }
            else {
                high--;
            }
        }
        return maxArea;
    }
}