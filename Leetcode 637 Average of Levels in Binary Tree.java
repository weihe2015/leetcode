/**
Given a non-empty binary tree, return the average value of the nodes on each level in the form of an array.
Example 1:
Input:
    3
   / \
  9  20
    /  \
   15   7
Output: [3, 14.5, 11]
Explanation:
The average value of nodes on level 0 is 3,  on level 1 is 14.5, and on level 2 is 11. Hence return [3, 14.5, 11].
Note:
The range of node's value is in the range of 32-bit signed integer. 
*/
public class Solution {
    // Iterative:
    public List<Double> averageOfLevels(TreeNode root) {
        List<Double> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();
            double total = 0;
            for (int s = 1; s <= size; s++) {
                TreeNode curr = queue.poll();
                total += curr.val;
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
            double avg = total / size;
            res.add(avg);
        }
        return res;
    }

    // Recursive:
    public List<Double> averageOfLevels(TreeNode root) {
        List<Double> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        List<List<Integer>> lists = new ArrayList<>();
        helper(lists, root, 0);
        for (List<Integer> list : lists) {
            double total = 0;
            for (int num : list) {
                total += num;
            }
            res.add(total / list.size());
        }
        return res;
    }
    
    private void helper(List<List<Integer>> lists, TreeNode node, int level) {
        if (node == null) {
            return;
        }
        if (lists.size() == level) {
            lists.add(new ArrayList<>());
        }
        List<Integer> list = lists.get(level);
        list.add(node.val);
        helper(lists, node.left, level+1);
        helper(lists, node.right, level+1);
    }
}