/**
Given an array of strings, group anagrams together.

Example:

Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
Output:
[
  ["ate","eat","tea"],
  ["nat","tan"],
  ["bat"]
]
Note:

All inputs will be in lowercase.
The order of your output does not matter.
*/

public class Solution {
    // use sort, solution 1
    // Running Time Complexity: O(N * mlogm), Space Complexity: O(N)
    // char array cannot be key, because Array equality is based on referential equality
    // Two array with same value are not the same.
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> res = new ArrayList<>();
        int n = strs.length;
        if (n == 0) {
            return res;
        }
        // key -> sorted string, value -> index of res list
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] cList = str.toCharArray();
            Arrays.sort(cList);
            // char array cannot be key, string is unmutable, more like symbol in ruby
            String key = String.valueOf(cList);
            
            List<String> list = map.getOrDefault(key, new ArrayList<>());
            list.add(str);
            map.put(key, list);
        }
        
        return new ArrayList<List<String>>(map.values());
    }

    // not use sort solution 2
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<ArrayKey, List<String>> map = new HashMap<>();
        Arrays.sort(strs);
        for (String s : strs){
            ArrayKey key = new ArrayKey();
            for (char c : s.toCharArray()){
                key.array[c-'a']++;
            }
            if (!map.containsKey(key)){
                map.put(key, new ArrayList<String>());
            }
            map.get(key).add(s);
        }
        return new ArrayList<List<String>>(map.values());
    }
    private class ArrayKey {
        int[] array = new int[26];

        @Override
        public int hashCode() {
            return Arrays.hashCode(array);
        }

        @Override
        public boolean equals(Object obj) {
            return Arrays.equals(array, ((ArrayKey)obj).array);
        }
    }

    // My brute force solution:
    // Running Time Complexity: O(n * 2L)
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> result = new ArrayList<>();
        for (String str : strs) {
            if (result.isEmpty()) {
                List<String> tmp = new ArrayList<String>();
                tmp.add(str);
                result.add(tmp);
            }
            else {
                boolean isAdded = false;
                for (List<String> list : result) {
                    String head = list.get(0);
                    if (isAnagram(head, str)) {
                        list.add(str);
                        isAdded = true;
                        break;
                    }
                }
                if (!isAdded) {
                    List<String> tmp = new ArrayList<String>();
                    tmp.add(str);
                    result.add(tmp);
                }
            }
        }
        return result;
    }

    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] cList = new int[256];
        for (char c : s.toCharArray()) {
            cList[c]++;
        }
        for (char c : t.toCharArray()) {
            cList[c]--;
            if (cList[c] < 0) {
                return false;
            }
        }
        return true;
    }
}