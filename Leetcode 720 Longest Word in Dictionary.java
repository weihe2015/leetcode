/**
Given a list of strings words representing an English Dictionary, 
find the longest word in words that can be built one character at a time by other words in words. 
If there is more than one possible answer, 
return the longest word with the smallest lexicographical order.

If there is no answer, return the empty string.

Example 1:
Input: 
words = ["w","wo","wor","worl", "world"]
Output: "world"

Explanation: 
The word "world" can be built one character at a time by "w", "wo", "wor", and "worl".

Example 2:
Input: 
words = ["a", "banana", "app", "appl", "ap", "apply", "apple"]
Output: "apple"

Explanation: 
Both "apply" and "apple" can be built from other words in the dictionary. However, "apple" is lexicographically smaller than "apply".
Note:

All the strings in the input will only contain lowercase letters.
The length of words will be in the range [1, 1000].
The length of words[i] will be in the range [1, 30].

*/
public class Solution {
    class TrieNode {
        TrieNode[] children;
        String word;
        boolean isWord;
        public TrieNode() {
            this.children = new TrieNode[26];
            this.word = "";
        }
    }
    
    private TrieNode root = new TrieNode();

    // BFS solution:
    public String longestWord(String[] words) {
        insertWords(words);

        Queue<TrieNode> queue = new LinkedList<>();
        queue.offer(root);
        String res = "";
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TrieNode curr = queue.poll();
                // iterate from the back to front because we need to keep the 
                // result lexicographically smaller
                for (int j = 25; j >= 0; j--) {
                    if (curr.children[j] != null && curr.children[j].isWord) {
                        res = curr.children[j].word;
                        queue.offer(curr.children[j]);
                    }
                }
            }
        }
        return res;
    }

    // DFS solution:
    public String longestWord(String[] words) {
        insertWords(words);

        return dfs(root);
    }

    private String dfs(TrieNode curr) {
        if (curr == null) {
            return "";
        }
        String res = curr.word;
        for (TrieNode child : curr.children) {
            if (child == null || child.word.length() == 0) {
                continue;
            }
            String childWord = dfs(child);
            if (childWord.length() > res.length() || (childWord.length() == res.length() && childWord.compareTo(res) < 0)) {
                res = childWord;
            }
        }
        return res;
    }

    private void insertWords(String[] words) {
        for (String word : words) {
            insert(word);
        }
    }

    private void insert(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (curr.children[c - 'a'] == null) {
                curr.children[c - 'a'] = new TrieNode();
            }
            curr = curr.children[c - 'a'];
        }
        curr.word = word;
        curr.isWord = true;
    }
}