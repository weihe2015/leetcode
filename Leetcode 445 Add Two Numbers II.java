/**
You are given two non-empty linked lists representing two non-negative integers. The most significant digit comes first and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Follow up:
What if you cannot modify the input lists? In other words, reversing the lists is not allowed.

Example:

Input: (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 8 -> 0 -> 7
*/

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    // Running Time Complexity: O(m * n), Space Complexity O(max(m,n))
    // Reverse two linkedlists and use method of Leetcode 2.
    // then reverse the result linkedlist to get the final result.
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode newL1 = reverseLinkedList(l1);
        ListNode newL2 = reverseLinkedList(l2);
        ListNode head = new ListNode(0), curr = head;
        int sum = 0;
        while (newL1 != null || newL2 != null) {
            if (newL1 != null) {
                sum += newL1.val;
                newL1 = newL1.next;
            }
            if (newL2 != null) {
                sum += newL2.val;
                newL2 = newL2.next;
            }
            curr.next = new ListNode(sum % 10);
            curr = curr.next;
            sum /= 10;
        }
        if (sum == 1) {
            curr.next = new ListNode(sum);
        }
        return reverseLinkedList(head.next);
    }
    
    private ListNode reverseLinkedList(ListNode node) {
        Stack<Integer> s = new Stack<Integer>();
        while (node != null) {
            s.push(node.val);
            node = node.next;
        }
        ListNode head = new ListNode(0), curr = head;
        while (!s.isEmpty()) {
            curr.next = new ListNode(s.pop());
            curr = curr.next;
        }
        return head.next;
    }

    // Solution 2, use Stack. Running Time Complexity: O(m + n), Space Complexity: O(m + n)
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Stack<Integer> s1 = new Stack<Integer>();
        Stack<Integer> s2 = new Stack<Integer>();
        
        ListNode curr = l1;
        while (curr != null) {
            s1.push(curr.val);
            curr = curr.next;
        }
        
        curr = l2;
        while (curr != null) {
            s2.push(curr.val);
            curr = curr.next;
        }
        
        ListNode head = new ListNode(0);
        int sum = 0;
        while (!s1.isEmpty() || !s2.isEmpty()) {
            if (!s1.isEmpty()) {
                sum += s1.pop();
            }
            if (!s2.isEmpty()) {
                sum += s2.pop();
            }
            head.val = sum % 10;
            ListNode newHead = new ListNode(sum / 10);
            newHead.next = head;
            head = newHead;
            sum /= 10;
        }
        if (head.val == 0) {
            head = head.next;
        }
        return head;
    }
} 