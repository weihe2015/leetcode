/*
This is a follow up of Shortest Word Distance.
The only difference is now you are given the list of words and
your method will be called repeatedly many times with different parameters.
How would you optimize it?

Design a class which receives a list of words in the constructor,
and implements a method that takes two words word1 and word2 and return the shortest distance between these two words in the list.

For example,
Assume that words = ["practice", "makes", "perfect", "coding", "makes"].

Given word1 = “coding”, word2 = “practice”, return 3.
Given word1 = "makes", word2 = "coding", return 1.

Note:
You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.
*/
public class WordDistance {
	// key: word, value: list of indexes of the key in the original string array:
	private Map<String, List<Integer>> map;

	// Constructor Running Time Complexity: O(N)
	public WordDistance(String[] words) {
		this.map = new HashMap<>();
		for (int i = 0, n = words.length; i < n; i++) {
			String word = words[i];
			List<Integer> list = null;
			if (!map.containsKey(word)) {
				list = new ArrayList<>();
			}
			else {
				list = map.get(word);
			}
			list.add(i);
			map.put(word, list);
		}
	}

	// Running Time Complexity: O(N^2)
	public int shortest(String word1, String word2) {
		int dist = Integer.MAX_VALUE;
		if (!map.containsKey(words1) || !map.containsKey(word2)) {
			return -1;
		}
		List<Integer> list1 = map.get(words1);
		List<Integer> list2 = map.get(words2);
		for (int num1 : list1) {
			for (int num2 : list2) {
				dist = Math.min(dist, Math.abs(num1-num2));
			}
		}
		return dist;
	}

	private int shortestDistance2(String word1, String word2) {
        if (!indexMap.containsKey(word1) || !indexMap.containsKey(word2)) {
            return -1;
        }
        List<Integer> fstIndexes = indexMap.get(word1);
        List<Integer> sndIndexes = indexMap.get(word2);
        int i = 0;
        int j = 0;
        int m = fstIndexes.size();
        int n = sndIndexes.size();
        int minDist = Integer.MAX_VALUE;

        while (i < m && j < n) {
            int fstIdx = fstIndexes.get(i);
            int sndIdx = sndIndexes.get(j);
            int dist = Math.abs(fstIdx - sndIdx);
            minDist = Math.min(minDist, dist);
            // move forward the smaller idx:
            if (fstIdx < sndIdx) {
                i++;
            }
            else {
                j++;
            }
        }

        return minDist;
    }
}