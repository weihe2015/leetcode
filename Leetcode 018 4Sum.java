/*
Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.

Note:
Elements in a quadruplet (a,b,c,d) must be in non-descending order. (ie, a ≤ b ≤ c ≤ d)
The solution set must not contain duplicate quadruplets.
    For example, given array S = {1 0 -1 0 -2 2}, and target = 0.

    A solution set is:
    (-1,  0, 0, 1)
    (-2, -1, 1, 2)
    (-2,  0, 0, 2)
*/
    
public class Solution {
    // O(n^3) running time
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length < 4) {
            return result;
        }
        Arrays.sort(nums);
        for (int i = 0, n = nums.length; i < n-3; i++) {
            // Skip the duplicates:
            if (i > 0 && nums[i] == nums[i-1]) {
                continue;
            }
            // Three Sum:
            for (int j = i+1; j < n-2; j++) {
                // Skip the duplicates:
                if (j > i+1 && nums[j] == nums[j-1]) {
                    continue;
                }
                int low = j+1, high = n-1;
                while (low < high) {
                    int sum = nums[i] + nums[j] + nums[low] + nums[high];
                    if (sum == target) {
                        // Skip the duplicates:
                        result.add(Arrays.asList(nums[i], nums[j], nums[low], nums[high]));
                        while (low < high && nums[low] == nums[low+1]) {
                        	low++;
                        }
                        // Skip the duplicates:
                        while (low < high && nums[high] == nums[high-1]) {
                        	high--;
                        }
                        low++; high--;
                    }
                    else if (sum < target) {
                        low++;
                    }
                    else {
                        high--;
                    }
                }
            }
        }
        return result;
    }
}