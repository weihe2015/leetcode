public class solution{
	// 266 Palindrome Permutation
	public boolean canPermutePalindrome(String s) {
		int[] cList = new int[256];
		int count = 0;
		for(char c : s.toCharArray()){
			if(cList[c] > 0){
				count--;
				cList[c]--;
			}
			else{
				count++;
				cList[c]++;
			}
		}
		return count <= 1;
	}
	// 243 Shortest Word Distance
	public int shortestDistance(String[] words, String word1, String word2) {
		int min = Integer.MAX_VALUE;
		int p1 = -1, p2 = -1, n = words.length;
		for(int i = 0; i < n; i++){
			if(words[i].equals(word1))
				p1 = i;
			if(words[i].equals(word2))
				p2 = i;
			if(p1 != -1 && p2 != -1)
				min = Math.min(min,Math.abs(p1-p2));
		}
		return min;
	}
	public class Interval{
		int start;
		int end;
	}
	// 252 Meeting Rooms
	private boolean canAttendMeetings(Interval[] intervals) {
		int n = intervals.length;
		int[] begin = new int[n];
		int[] end = new int[n];
		for(int i = 0; i < n; i++){
			begin[i] = intervals[i].begin;
			end[i] = intervals[i].end;
		}
		Arrays.sort(begin); Arrays.sort(end);
		for(int i = 1; i < n; i++){
			if(begin[i] < end[i])
				return false;
		}
		return true;
	}
	// use comparator
	private boolean canAttendMeetings(Interval[] intervals) {
		Arrays.sort(intervals, new Comparator<Interval>(){
			@Override
			public int compare(Interval a, Interval b){
				return a.start - b.start;
			}
		});
		for(int i = 1; i < intervals.length; i++){
			if(intervals[i].start < intervals[i-1].end)
				return false;
		}
		return true;
	}
	// 325 Maximum Size Subarray Sum Equals k
	// O(n^2)
	public int maxSubArrayLen(int[] nums, int k) {
		int max = 0;
		int n = nums.length;
		for(int i = 0; i < n; i++){
			int sum = nums[i];
			for(int j = i + 1; j < n; j++){
				sum += nums[j];
				if(sum == k){
					int len = j - i + 1;
					if(len > max)
						max = len;
				}
			}
		}
		return max;
	}
	public int maxSubArrayLen(int[] nums, int k) {
		// key-> sum to i, value: i
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		int max = 0, sum = 0, n = nums.length;
		// first n item = 0, len + 1
		map.put(0,-1);
		for(int i = 0; i < n; i++){
			sum += nums[i];
			if(!map.containsKey(nums[i]))
				map.put(sum,i);
			if(map.containsKey(sum-k))
				max = Math.max(max,i-map.get(sum-k));
		}
		return max;
	}
	// 246 Strobogrammatic Number
	public boolean isStrobogrammatic(String num) {
		HashMap<Character,Character> map = new HashMap<Character,Character>();
		map.put('6','9');
		map.put('9','6');
		map.put('1','1');
		map.put('0','0');
		map.put('8','8');
		int lo = 0, hi = num.length() - 1;
		while(lo <= hi){
			if(!map.containsKey(num.charAt(lo)))
				return false;
			if(map.get(num.charAt(lo)) != num.charAt(hi))
				return false;
			lo++; hi--;
		}
		return true;
	}
	// 346 Moving Average from Data Stream
	public class MovingAverage {
		Queue<Integer> queue;
		int size;

		public MovingAverage(int size){
			queue = new Linkedlist<Integer>();
			this.size = size;
		}
		public double next(int val) {
			queue.offer(val);
			if(queue.size() > size)
				queue.poll();
			int sum = 0;
			for(int num : q){
				sum += num;
			}
			return (double)sum/size;
		}
	}
	// 270 Closest Binary Search Tree Value
	// iterative
	public int closestValue(TreeNode root, double target) {
		int closest = root.val;
		while(root != null){
			if(Math.abs(closet - target) >= Math.abs(root.val - target))
				closest = root.val;
			root = root.val > target ? root.left : root.right;
		}
		return closest;
	}
	// recursive
	public int closestValue(TreeNode root, double target) {
		int a = root.val;
		root = root.val > target ? root.left : root.right;
		if(root == null)
			return root.val;
		int b = closestValue(root,target);
		return Math.abs(a - target) < Math.abs(b - target) ? a : b;
	}
	// 276 Paint Fence
	public int numWays(int n, int k) {
		if(n <= 1)
			return n == 0 ? 0 : k;
		int[] same = new int[n];
		int[] diff = new int[n];
		same[1] = k;
		diff[1] = k*(k-1);
		for(int i = 2; i < n; i++){
			same[i] = diff[i];
			diff[i] = (same[i-1] + diff[i-1]) * (k-1);
		}
		return same[n-1] + diff[n-1]
	}
	public int numWays(int n, int k) {
		if(n <= 1)
			return n == 0 ? 0 : k;
		int same = k, diff = k*(k-1);
		for(int i = 2; i < n; i++){
			int temp = diff;
			diff = (diff + same) * (k-1);
			same = temp;
		}
		return same + diff;
	}
	// 249 Group Shifted Strings
	public List<List<String>> groupStrings(String[] strings) {
		List<List<String>> result = new ArrayList<List<String>>();
		Map<String,List<String>> map = new HashMap<String,List<String>>();
		for(String str : strings){
			int offset = str.charAt(0) - 'a';
			StringBuffer sb = new StringBuffer();
			for(char c : str.toCharArray()){
				char ch = c - offset;
				if(ch < 'a'){
					ch += 26;
				}
				sb.append(ch);
			}
			String key = sb.toString();
			if(!map.containsKey(key)){
				map.put(key,new ArrayList<String>());
			}
			map.get(key).add(str);
		}
		for(String key : map.keySet()){
			List<String> list = map.get(key);
			Collections.sort(list);
			result.add(list);
		}
		return result;
 	}
 	// 291 Word Pattern II
 	public boolean wordPatternMatch(String pattern, String str) {
 		Map<Character,String> map = new HashMap<>();
 		return isMatch(pattern,0,str,0,map);
 	}
 	public boolean isMatch(String pat, int i, String str, int j, 
 		Map<Character,String> map){
 		if(i == pat.length() && j == str.length())
 			return true;
 		if(i == pat.length() || j == str.length())
 			return false;
 		char c = pat.charAt(i);
 		if(map.containsKey(c)){
 			String s = map.get(c);
 			if(!str.startWith(s,j))
 				return false;
 			return isMatch(pat,i+1,str,j+s.length(),map);
 		}
 		else{
 			for(int k = j; k < str.length(); k++){
 				String s = str.substring(j,k+1);
 				if(map.containsValue(s))
 					continue;
 				map.put(c,s);
 				if(isMatch(pat,i+1,str,k+1,map))
 					return true;
 				map.remove(c);
 			}
 		}
 		return false;
 	}

 	// 167 Two Sum II-- input array is sorted
 	public int[] twoSum(int[] num, int target) {
 		int[] result = new int[2];
 		if(num == null || num.length < 2)
 			return result;
 		int left = 0, right = nums.length - 1;
 		while(left < right){
 			int r = num[left] + num[right];
 			if(r > target){
 				right--;
 			}
 			else if(r < target){
 				left++;
 			}
 			else{
 				result[0] = left + 1; // index no zero-based
 				result[1] = right + 1; // index no zero based
 				break;
 			}
 		}
 		return result;
 	}
 	
 	// 170 Two Sum II Data Structure Design
 	List<Integer> list = new ArrayList<>();
 	public void add(int number) {
 		list.add(number);
 	} 
 	public boolean find(int value) {
 		Map<Integer,Integer> map = new HashMap<>();
 		for(int i = 0; i < list.size(); i++){
 			int num = list.get(i);
 			int diff = value - num;
 			if(map.containsKey(diff)){
 				return true;
 			}
 			map.put(num,i);
 		}
 		return false;
 	}
 	// 288 Unique Word Abbreviation
 	public class ValidWordAbbr {
 		// store abbreviation.
 		Set<String> set = new HashSet<>();
 		public ValidWordAbbr(String[] dictionary) {
 			for(String word : dictionary){
 				int n = word.length();
 				String key = "" + word.charAt(0) + n + word.charAt(n-1);
 				if(!set.contains(key))
 					set.add(key);
 			}
 		}
 		public boolean isUnique(String word) {
 			int n = word.length();
 			String key = "" + word.charAt(0) + n + word.charAt(n-1); 
 			return !set.contains(key);			
 		}
 	}
 	// 320 Generalized Abbreviation

}