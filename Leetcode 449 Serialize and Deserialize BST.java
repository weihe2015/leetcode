/**
Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, 
or transmitted across a network connection link to be reconstructed later in the same or another computer environment.
Design an algorithm to serialize and deserialize a binary search tree. There is no restriction on how your serialization/deserialization algorithm should work. 
You just need to ensure that a binary search tree can be serialized to a string and this string can be deserialized to the original tree structure.

The encoded string should be as compact as possible. (CANNOT use NULL to represent null node)

Note: Do not use class member/global/static variables to store states. Your serialize and deserialize algorithms should be stateless.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {
    // Time Complexity: O(N), where N is the total number of nodes in the BST.
    private static String COMMA = ",";
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuffer sb = new StringBuffer();
        if (root == null) {
            return sb.toString();
        }
        // Use Preorder traversal to add nodes into string.
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            sb.append(curr.val).append(COMMA);
            if (curr.right != null) {
                stack.push(curr.right);
            }
            if (curr.left != null) {
                stack.push(curr.left);
            }
        }
        // remove the last comma
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data == null || data.length() == 0) {
            return null;
        }
        String[] arr = data.split(COMMA);
        int n = arr.length;
        int[] pos = new int[1];
        return buildTree(arr, pos, n, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    
    private TreeNode buildTree(String[] arr, int[] pos, int n, int minVal, int maxVal) {
        if (pos[0] == n) {
            return null;
        }
        int val = Integer.parseInt(arr[pos[0]]);
        if (val < minVal || val > maxVal) {
            return null;
        }
        TreeNode root = new TreeNode(val);
        pos[0]++;
        root.left = buildTree(arr, pos, n, minVal, val);
        root.right = buildTree(arr, pos, n, val, maxVal);
        return root;
    }

    // Iterative:
    public TreeNode deserialize(String data) {
        if (data == null || data.length() == 0) {
            return null;
        }
        String[] nodes = data.split(COMMA);
        int val = Integer.parseInt(nodes[0]);
        TreeNode root = new TreeNode(val);
        Stack<TreeNode> stack = new Stack<TreeNode>();
        stack.push(root);
        int i = 1, n = nodes.length;
        while (i < n) {
            val = Integer.parseInt(nodes[i++]);
            TreeNode node = new TreeNode(val);
            TreeNode curr = stack.peek();
            
            if (val < curr.val) {
                curr.left = node;
            }
            else {
                // new node's value is greater than previous node, find its parent node in the stack.
                // If not found, use the one near the root node for parent node.
                TreeNode parNode = null;
                do {
                	parNode = stack.pop();
                } 
                while (!stack.isEmpty() && stack.peek().val < val);
                parNode.right = node;
            }
            stack.push(node);
        }
        return root;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));