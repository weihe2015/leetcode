/*
Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
getMin() -- Retrieve the minimum element in the stack.

*/
// solution 1, push 2 items 1 time
class MinStack {
    private int minNum;
    private Stack<Integer> stack;
    /** initialize your data structure here. */
    public MinStack() {
        this.minNum = Integer.MAX_VALUE;
        stack = new Stack<Integer>();
    }
    /**
     * For each item, if it is minItem, push previous min into stack, and push this item on the stack.
     * So that when it pops out, if pop out number is minNum, then we will know previous min num by popping out another item
    */
    public void push(int x) {
        if (x <= minNum) {
            stack.push(minNum);
            minNum = x;
        }
        stack.push(x);
    }

    public void pop() {
        int val = stack.pop();
        // if the last item is the min item, pop another item from stack.
        if (val == minNum) {
            minNum = stack.pop();
        }
        if (stack.isEmpty()) {
            minNum = Integer.MAX_VALUE;
        }
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return this.minNum;
    }
}

// solution 2, better, cause only push 1 item 1 time
public class MinStack {
    private long minVal;
    private Stack<Long> s;
    /** initialize your data structure here. */
    public MinStack() {
        s = new Stack<>();
    }

    public void push(int x) {
        if (s.empty()) {
            minVal = x;
            s.push(0L);
        }
        else {
            s.push(x - minVal);
            if (x < minVal) {
                minVal = x;
            }
        }
    }

    public void pop() {
        if (s.isEmpty()) {
            return;
        }
        long val = s.pop();
        if (val < 0) {
            minVal = minVal - val;
        }
    }

    public int top() {
        long val = s.peek();
        if (val > 0) {
            return (int) (val + minVal);
        }
        else {
            return (int) minVal;
        }
    }

    public int getMin() {
        return (int) minVal;
    }
}

// Solution 3: Use minQueue:
class MinStack {
    private Stack<Integer> stack;
    private Queue<Integer> pq;

    /** initialize your data structure here. */
    public MinStack() {
        this.stack = new Stack<>();
        this.pq = new PriorityQueue<>();
    }

    public void push(int x) {
        stack.push(x);
        pq.offer(x);
    }

    public void pop() {
        int x = stack.pop();
        pq.remove(x);
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return pq.peek();
    }
}

// Brute Force: when push, update minNum. When pop, iterate whole stack to update minNum
class MinStack {
    private int minNum;
    private Stack<Integer> stack;
    /** initialize your data structure here. */
    public MinStack() {
        minNum = Integer.MAX_VALUE;
        stack = new Stack<>();
    }

    public void push(int x) {
        if (x <= minNum) {
            minNum = x;
        }
        stack.push(x);
    }

    public void pop() {
        stack.pop();
        minNum = Integer.MAX_VALUE;
        if (!stack.isEmpty()) {
            for (int num : stack) {
                if (num <= minNum) {
                    minNum = num;
                }
            }
        }
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return minNum;
    }
}
