
/**
 * Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

    For example, given array S = {-1 2 1 -4}, and target = 1.

    The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
**/
public class Solution {

    // Running Time O(n^2), Space(1)
    public int threeSumClosest(int[] nums, int target) {
        int n = nums.length
        Arrays.sort(nums);
        // Initilize result, cannot use Integer.MAX_VALUE because it may max out int in result-target when target is negative.
        int res = nums[0] + nums[1] + nums[2];
        for (int i = 0; i < n-2; i++) {
            if (i > 0 && nums[i] == nums[i-1]) {
                i++;
            }
            int j = i+1, 
            int k = n-1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == target) {
                    res = sum;
                    return res;
                }
                else if (sum < target) {
                    while (j < k && nums[j] == nums[j+1]) {
                        j++;
                    }
                    j++;
                }
                else {
                    while (j < k && nums[k] == nums[k-1]) {
                        k--;
                    }
                    k--;
                }
                if (Math.abs(sum - target) < Math.abs(res - target)) {
                    res = sum;
                }
            }
        }
        return res;
    }
}