/*
A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.

Example: 19 is a happy number

12 + 92 = 82
82 + 22 = 68
62 + 82 = 100
12 + 02 + 02 = 1

Floyd Cyle Detection Algorithm
*/
/*
https://leetcode.com/problems/happy-number/discuss/56919/explanation-of-why-those-posted-algorithms-are-mathematically-valid

First of all, it is easy to argue that starting from a number I, if some value - say a - appears again during the process after k steps, the initial number I cannot be a happy number. Because a will continuously become a after every k steps.

Therefore, as long as we can show that there is a loop after running the process continuously, the number is not a happy number.

There is another detail not clarified yet: For any non-happy number, will it definitely end up with a loop during the process? This is important, because it is possible for a non-happy number to follow the process endlessly while having no loop.

To show that a non-happy number will definitely generate a loop, we only need to show that for any non-happy number, all outcomes during the process are bounded by some large but finite integer N. If all outcomes can only be in a finite set (2,N], and since there are infinitely many outcomes for a non-happy number, there has to be at least one duplicate, meaning a loop!

Suppose after a couple of processes, we end up with a large outcome O1 with D digits where D is kind of large, say D>=4, i.e., O1 > 999 (If we cannot even reach such a large outcome, it means all outcomes are bounded by 999 ==> loop exists). We can easily see that after processing O1, the new outcome O2 can be at most 9^2*D < 100D, meaning that O2 can have at most 2+d(D) digits, where d(D) is the number of digits D have. It is obvious that 2+d(D) < D. We can further argue that O1 is the maximum (or boundary) of all outcomes afterwards. This can be shown by contradictory: Suppose after some steps, we reach another large number O3 > O1. This means we process on some number W <= 999 that yields O3. However, this cannot happen because the outcome of W can be at most 9^2*3 < 300 < O1.
*/

public class Solution {
    /**
    Surprisingly, we can apply the Floyd Cycle Detection (the one we used in Detect Linked List Cycle) on this problem: think of what is a cycle in this case:

    from a number A, we can get to another B using the ways given in this case
    from number B, when we doing the transformation, we will eventually get back to B again ---> this forms a cycle (infinite loop)

    for example:
    1^2 + 1^2 = 2
    2^2 = 4 ------> notice that from here we are starting with 4
    4^2 = 16
    1^2 + 6^2 = 37
    3^2 + 7^2 = 58
    5^2 + 8^2 = 89
    8^2 + 9^2 = 145
    1^2 + 4^2 + 5^2 = 42
    4^2 + 2^2 = 20
    2^2 + 0^2 = 4 -------> notice that we just get back to 4 again

    Using Floyd Cycle Detection algorithm (fast and slow pointer), we will be able to actually get the value of B. Then the rest of task would be very simple, we simply check whether this value will be 1 or not.

    You may ask: what if value "1" also appears in the cycle and we are skipping over it. Well, in that case, the value that slow and fast are equal to will be 1, as transformation of 1 is still 1, so we still cover this case.
    7 -> 49 -> 97 -> 130 -> 10 -> 1
    4 -> 16 -> 37 -> 58 -> 89 -> 145 -> 42 -> 20 -> 4
    */
    // adapt the Floyd Cycle detection algorithm
    public boolean isHappy(int n) {
        int slow = n;
        int fast = n;
        do {
            slow = digitSquareSum(slow);
            fast = digitSquareSum(fast);
            fast = digitSquareSum(fast);
        } while (slow != fast);

        return slow == 1;
    }

    private int digitSquareSum(int n) {
        int sum = 0;
        while (n > 0) {
            int digit = n % 10;
            sum += digit * digit;
            n /= 10;
        }
        return sum;
    }
    /**
    true  (1) -> 1
	false (2) -> 4 -> 16 -> 37 -> 58 -> 89 -> 145 -> 42 -> 20 -> 4
	false (3) -> 9 -> 81 -> 65 -> 61 -> 37 (look at 2)
	false (4) -> (look at 2)
	false (5) -> 25 -> 29 -> 85 -> 89 (look at 2)
	false (6) -> 36 -> 45 -> 41 -> 17 -> 50 -> 25  (look at 5)
	true  (7) -> 49 -> 97 -> 10
	false (8) -> 64 -> 52 -> 29 (look at 5)
	false (9) -> 9 -> 81 -> 65 (look at 3)

	All other n >= 10, while computing will become [1-9],
	So there are two cases 1 and 7 which are true.
    and when n == 4, it automatically leads to false
    */
    public boolean isHappy(int n) {
        while (true) {
            n = digitSquareSum(n);
            if (n == 1) {
                break;
            }
            else if (n == 4) {
                return false;
            }
        }
        return true;
    }

    public boolean isHappy(int n) {
        if (n == 1) {
            return true;
        }
        else if (n == 4) {
            return false;
        }
        n = digitSquareSum(n);
        return isHappy(n);
    }

    private int digitSquareSum(int n) {
        int sum = 0;
        while (n > 0) {
            int digit = n % 10;
            sum += digit * digit;
            n /= 10;
        }
        return sum;
    }
}
