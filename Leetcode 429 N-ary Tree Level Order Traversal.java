/**
Given an n-ary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).

For example, given a 3-ary tree:
         1
   3      2     4
5    6
We should return its level order traversal:

[
     [1],
     [3,2,4],
     [5,6]
]
*/
/*
// Definition for a Node.
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val,List<Node> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
    // Recursive:
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrder(result, root, 0);
        return result;
    }
    
    private void levelOrder(List<List<Integer>> result, Node node, int level) {
        if (node == null) {
            return;
        }
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);
        list.add(node.val);
        for (Node childNode : node.children) {
            levelOrder(result, childNode, level+1);
        }
    }

    // Iterative:
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<Node> queue = new LinkedList<Node>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<Integer>();
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                Node currNode = queue.poll();
                list.add(currNode.val);
                for (Node childNode : currNode.children) {
                    queue.offer(childNode);
                }
            }
            result.add(list);
        }
        return result;
    }
}