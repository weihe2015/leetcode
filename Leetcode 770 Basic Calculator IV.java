/**
Given an expression such as expression = "e + 8 - a + 5" and an evaluation map such as {"e": 1} (given in terms of evalvars = ["e"] and evalints = [1]), return a list of tokens representing the simplified expression, such as ["-1*a","14"]

An expression alternates chunks and symbols, with a space separating each chunk and symbol.
A chunk is either an expression in parentheses, a variable, or a non-negative integer.
A variable is a string of lowercase letters (not including digits.) Note that variables can be multiple letters, and note that variables never have a leading coefficient or unary operator like "2x" or "-x".
Expressions are evaluated in the usual order: brackets first, then multiplication, then addition and subtraction. For example, expression = "1 + 2 * 3" has an answer of ["7"].

The format of the output is as follows:

For each term of free variables with non-zero coefficient, we write the free variables within a term in sorted order lexicographically. For example, we would never write a term like "b*a*c", only "a*b*c".
Terms have degree equal to the number of free variables being multiplied, counting multiplicity. (For example, "a*a*b*c" has degree 4.) We write the largest degree terms of our answer first, breaking ties by lexicographic order ignoring the leading coefficient of the term.
The leading coefficient of the term is placed directly to the left with an asterisk separating it from the variables (if they exist.)  A leading coefficient of 1 is still printed.
An example of a well formatted answer is ["-2*a*a*a", "3*a*a*b", "3*b*b", "4*a", "5*c", "-6"] 
Terms (including constant terms) with coefficient 0 are not included.  For example, an expression of "0" has an output of [].
Examples:

Input: expression = "e + 8 - a + 5", evalvars = ["e"], evalints = [1]
Output: ["-1*a","14"]

Input: expression = "e - 8 + temperature - pressure",
evalvars = ["e", "temperature"], evalints = [1, 12]
Output: ["-1*pressure","5"]

Input: expression = "(e + 8) * (e - 8)", evalvars = [], evalints = []
Output: ["1*e*e","-64"]

Input: expression = "7 - 7", evalvars = [], evalints = []
Output: []

Input: expression = "a * b * c + b * a * c * 4", evalvars = [], evalints = []
Output: ["5*a*b*c"]

Input: expression = "((a - b) * (b - c) + (c - a)) * ((a - b) + (b - c) * (c - a))",
evalvars = [], evalints = []
Output: ["-1*a*a*b*b","2*a*a*b*c","-1*a*a*c*c","1*a*b*b*b","-1*a*b*b*c","-1*a*b*c*c","1*a*c*c*c","-1*b*b*b*c","2*b*b*c*c","-1*b*c*c*c","2*a*a*b","-2*a*a*c","-2*a*b*b","2*a*c*c","1*b*b*b","-1*b*b*c","1*b*c*c","-1*c*c*c","-1*a*a","1*a*b","1*a*c","-1*b*c"]
Note:

expression will have length in range [1, 250].
evalvars, evalints will have equal lengths in range [0, 100].
*/
public class Solution {
    
    class Poly {
        Map<List<String>, Integer> count;
        public Poly() {
            this.count = new HashMap<>();
        }

        public Poly(Map<List<String>, Integer> map) {
            this.count = new HashMap<>(map);
        }

        public void update(List<String> key, int val) {
            int oldVal = count.getOrDefault(key, 0);
            count.put(key, oldVal + val);
        }

        public Poly add(Poly that) {
            Poly res = new Poly(this.count);

            for (List<String> key : that.count.keySet()) {
                int val = that.count.get(key);
                res.update(key, val);
            }

            return res;
        }

        public Poly sub(Poly that) {
            Poly res = new Poly(this.count);

            for (List<String> key : that.count.keySet()) {
                int val = that.count.get(key) * -1;
                res.update(key, val);
            }

            return res;
        }

        public Poly mul(Poly that) {
            Poly res = new Poly();
            for (List<String> keys1 : this.count.keySet()) {
                int val1 = this.count.get(keys1);
                for (List<String> keys2 : that.count.keySet()) {
                    List<String> newKeys = new ArrayList<>(keys1);
                    newKeys.addAll(keys2);
                    Collections.sort(newKeys);

                    int val2 = that.count.get(keys2);
                    res.update(newKeys, val1 * val2);
                }
            }
            return res;
        }

        public Poly evaluate(Map<String, Integer> evalMap) {
            Poly res = new Poly();
            for (List<String> keys : this.count.keySet()) {
                int val = this.count.get(keys);
                List<String> freeKeys = new ArrayList<>();
                for (String key : keys) {
                    if (evalMap.containsKey(key)) {
                        val *= evalMap.get(key);
                    }
                    else {
                        freeKeys.add(key);
                    }
                }
                res.update(freeKeys, val);
            }
            return res;
        }


        public List<String> toList() {
            List<String> res = new ArrayList<>();
            List<List<String>> keys = new ArrayList<>(this.count.keySet());
            
            ListComparator comp = new ListComparator();
            Collections.sort(keys, comp);

            for (List<String> key : keys) {
                int val = this.count.get(key);
                if (val == 0) {
                    continue;
                }
                StringBuffer sb = new StringBuffer();
                sb.append(val);
                for (String token : key) {
                    sb.append("*");
                    sb.append(token);
                }
                res.add(sb.toString());
            }

            return res;
        }
    }
    
    public class ListComparator implements Comparator<List<String>> {
        @Override
        public int compare(List<String> list1, List<String> list2) {
            if (list1.size() != list2.size()) {
                return Integer.compare(list2.size(), list1.size());
            }
            else {
                return compareList(list1, list2);
            }
        }

        public int compareList(List<String> list1, List<String> list2) {
            int i = 0;
            for (String x : list1) {
                String y = list2.get(i++);
                int res = x.compareTo(y);
                if (res != 0) {
                    return res;
                }
            }
            return 0;
        }
    }

    public List<String> basicCalculatorIV(String expression, String[] evalvars, int[] evalints) {
        Map<String, Integer> evalMap = new HashMap<>();
        for (int i = 0, n = evalvars.length; i < n; i++) {
            String key = evalvars[i];
            int val = evalints[i];
            evalMap.put(key, val);
        }
        return parse(expression).evaluate(evalMap).toList();
    }

    private Poly make(String expression) {
        Poly res = new Poly();
        List<String> list = new ArrayList<>();
        char firstChar = expression.charAt(0);
        if (Character.isDigit(firstChar)) {
            res.update(list, Integer.valueOf(expression));
        }
        else {
            list.add(expression);
            res.update(list, 1);
        }
        return res;
    }

    private Poly combine(Poly left, Poly right, char symbol) {
        if (symbol == '+') {
            return left.add(right);
        }
        else if (symbol == '-') {
            return left.sub(right);
        }
        else if (symbol == '*') {
            return left.mul(right);
        }
        return null;
    }

    private Poly parse(String expression) {
        List<Poly> bucket = new ArrayList<>();
        List<Character> symbols = new ArrayList<>();

        int i = 0;
        int n = expression.length();
        while (i < n) {
            char c = expression.charAt(i);
            if (c == '(') {
                int bal = 0;
                int j = i;
                for (; j < n; j++) {
                    char c2 = expression.charAt(j);
                    if (c2 == '(') {
                        bal++;
                    }
                    else if (c2 == ')') {
                        bal--;
                    }
                    if (bal == 0) {
                        break;
                    }
                }
                Poly subPoly = parse(expression.substring(i+1, j));
                bucket.add(subPoly);
                i = j;
            }
            else if (Character.isLetterOrDigit(c)) {
                int j = i;
                search : {
                    for (; j < n; j++) {
                        char c2 = expression.charAt(j);
                        if (c2 == ' ') {
                            Poly subPoly = make(expression.substring(i, j));
                            bucket.add(subPoly);
                            break search;
                        }
                    }
                    Poly subPoly = make(expression.substring(i));
                    bucket.add(subPoly);
                }
                i = j;
            }
            else if (c != ' ') {
                symbols.add(c);
            }
            i++;
        }

        for (int j = symbols.size() - 1; j >= 0; j--) {
            if (symbols.get(j) == '*') {
                Poly leftPoly = bucket.get(j);
                Poly rightPoly = bucket.remove(j+1);
                char symbol = symbols.remove(j);
                Poly subPoly = combine(leftPoly, rightPoly, symbol);
                bucket.set(j, subPoly);
            }
        }

        if (bucket.isEmpty()) {
            return new Poly();
        }
        Poly res = bucket.get(0);
        for (int j = 0, max = symbols.size(); j < max; j++) {
            Poly rightPoly = bucket.get(j+1);
            char symbol = symbols.get(j);
            res = combine(res, rightPoly, symbol);
        }
        return res;
    }
}