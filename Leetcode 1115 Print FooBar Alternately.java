/**
Suppose you are given the following code:

class FooBar {
  public void foo() {
    for (int i = 0; i < n; i++) {
      print("foo");
    }
  }

  public void bar() {
    for (int i = 0; i < n; i++) {
      print("bar");
    }
  }
}
The same instance of FooBar will be passed to two different threads. 
Thread A will call foo() while thread B will call bar(). 
Modify the given program to output "foobar" n times.

Example 1:

Input: n = 1
Output: "foobar"
Explanation: There are two threads being fired asynchronously. 
One of them calls foo(), while the other calls bar(). "foobar" is being output 1 time.
Example 2:

Input: n = 2
Output: "foobarfoobar"
Explanation: "foobar" is being output 2 times.
*/
// Solution 1: LTE:
class FooBar {
    private int n;
    private volatile int count;
    
    public FooBar(int n) {
        this.n = n;
        this.count = 0;
    }

    public void foo(Runnable printFoo) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            while (count != 0) {
                
            }
        	// printFoo.run() outputs "foo". Do not change or remove this line.
        	printFoo.run();
            count++;
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            while (count != 1) {
                
            }
            // printBar.run() outputs "bar". Do not change or remove this line.
        	printBar.run();
            count--;
        }
    }
}

// Solution 2: Use semaphore:
import java.util.concurrent.Semaphore;

class FooBar {
    private int n;
    private Semaphore semaphore1;
    private Semaphore semaphore2;

    public FooBar(int n) {
        this.n = n;
        this.semaphore1 = new Semaphore(1);
        this.semaphore2 = new Semaphore(0);
    }

    public void foo(Runnable printFoo) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            semaphore1.acquire();
        	  // printFoo.run() outputs "foo". Do not change or remove this line.
        	  printFoo.run();
            semaphore2.release();
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            semaphore2.acquire();
            // printBar.run() outputs "bar". Do not change or remove this line.
        	  printBar.run();
            semaphore1.release();
        }
    }
}


// Alternatively: Print Foo first, then print Bar:
import java.util.concurrent.Semaphore;

class FooBar {
    private int n;
    private Semaphore semaphore;
    
    public FooBar(int n) {
        this.n = n;
        this.semaphore = new Semaphore(0);
    }

    public void foo(Runnable printFoo) throws InterruptedException {
        for (int i = 0; i < n; i++) {
        	  // printFoo.run() outputs "foo". Do not change or remove this line.
        	  printFoo.run();
        }
        semaphore.release();
    }

    public void bar(Runnable printBar) throws InterruptedException {
        semaphore.acquire();
        for (int i = 0; i < n; i++) {
            // printBar.run() outputs "bar". Do not change or remove this line.
        	  printBar.run();
        }
        semaphore.release();
    }
}