/**
Given two sparse vectors, compute their dot product.

Implement class SparseVector:

SparseVector(nums) Initializes the object with the vector nums
dotProduct(vec) Compute the dot product between the instance of SparseVector and vec
A sparse vector is a vector that has mostly zero values, you should store the sparse vector efficiently and compute the dot product between two SparseVector.

Follow up: What if only one of the vectors is sparse?

Example 1:

Input: nums1 = [1,0,0,2,3], nums2 = [0,3,0,4,0]
Output: 8
Explanation: v1 = SparseVector(nums1) , v2 = SparseVector(nums2)
v1.dotProduct(v2) = 1*0 + 0*3 + 0*0 + 2*4 + 3*0 = 8
Example 2:

Input: nums1 = [0,1,0,0,0], nums2 = [0,0,0,0,2]
Output: 0
Explanation: v1 = SparseVector(nums1) , v2 = SparseVector(nums2)
v1.dotProduct(v2) = 0*0 + 1*0 + 0*0 + 0*0 + 0*2 = 0
Example 3:

Input: nums1 = [0,1,0,0,2,0,0], nums2 = [1,0,0,0,3,0,4]
Output: 6
*/
public class Solution {

    class SparseVector {
        // key : index, val -> value
        private Map<Integer, Integer> map;

        public SparseVector(int[] nums) {
            this.map = new HashMap<>();

            int n = nums.length;
            for (int i = 0; i < n; i++) {
                if (nums[i] != 0) {
                    map.put(i, nums[i]);
                }
            }
        }

        /**
        Always ensured that this object is the larger vector than the given one.
        */ 
        public int dotProduct(SparseVector vec) {
            if (map.size() < vec.map.size()) {
                return vec.dotProduct(this);
            }
            int res = 0;

            for (int idx : vec.map.keySet()) {
                if (!map.containsKey(idx)) {
                    continue;
                }
                res += vec.map.get(idx) * map.get(idx);
            }
            return res;
        }
    }

    // Use two pointers and list:
    class SparseVector {
        List<int[]> points;

        public SparseVector(int[] nums) {
            this.points = new ArrayList<>();
            int n = nums.length;
            for (int i = 0; i < n; i++) {
                if (nums[i] != 0) {
                    points.add(new int[]{i, nums[i]});
                }
            }

            public int dotProduct(SparseVector vec) {
                int res = 0;

                int i = 0;
                int j = 0;
                int m = points.length;
                int n = vec.points.length;

                while (i < m && j < n) {
                    int idx1 = points.get(i)[0];
                    int idx2 = vec.points.get(j)[0];
                    if (idx1 == idx2) {
                        res += points.get(i)[1] * vec.points.get(j)[1];
                        i++;
                        j++;
                    }
                    else if (idx1 < idx2) {
                        i++;
                    }
                    else {
                        j++;
                    }
                }
                return res;
            }
        }
    }
}