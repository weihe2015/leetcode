/*
Implement a trie with insert, search, and startsWith methods.

Note:
You may assume that all inputs are consist of lowercase letters a-z.
*/
/**
Not Code Format Version:
*/
class Trie {
    class TrieNode {
        boolean isWord;
        TrieNode[] children;
        public TrieNode() {
            this.isWord = false;
            this.children = new TrieNode[26];
        }
    }
    
    private TrieNode root;
    
    /** Initialize your data structure here. */
    public Trie() {
        root = new TrieNode();
    }
    
    /** Inserts a word into the trie. */
    public void insert(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (curr.children[c-'a'] == null) {
                curr.children[c-'a'] = new TrieNode();
            }
            curr = curr.children[c-'a'];
        }
        curr.isWord = true;
    }
    
    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (curr.children[c-'a'] == null) {
                return false;
            }
            curr = curr.children[c-'a'];
        }
        return curr.isWord;
    }
    
    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        TrieNode curr = root;
        for (char c : prefix.toCharArray()) {
            if (curr.children[c-'a'] == null) {
                return false;
            }
            curr = curr.children[c-'a'];
        }
        return true;
    }
}

// array version
public class Trie {
    
    private TrieNode root;
    /** Initialize your data structure here. */
    public Trie() {
        root = new TrieNode();
    }
    
    /** Inserts a word into the trie. */
    public void insert(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExist(c)) {
                curr.setNextTrieNode(c);
            }
            curr = curr.getNextTrieNode(c);
        }
        curr.setIsWord(true);
    }
    
    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExist(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return curr.getIsWord();
    }
    
    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        TrieNode curr = root;
        for (char c : prefix.toCharArray()) {
            if (!curr.isExist(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return true;
    }
    
    class TrieNode {
        private boolean isWord;
        private TrieNode[] next;

        public TrieNode() {
            this.isWord = false;
            this.next = new TrieNode[26];
        }

        public boolean getIsWord() {
            return this.isWord;
        }

        public void setIsWord(boolean isWord) {
            this.isWord = isWord;
        }
        
        public boolean isExist(char c) {
            return this.next[c-'a'] != null;
        }

        public TrieNode getNextTrieNode(char c) {
            return this.next[c-'a'];
        }

        public void setNextTrieNode(char c) {
            this.next[c-'a'] = new TrieNode();
        }
    }
}

// Your Trie object will be instantiated and called as such:
// Trie trie = new Trie();
// trie.insert("somestring");
// trie.search("key");

// hashmap version
public class Trie {
    
    private TrieNode root;
    /** Initialize your data structure here. */
    public Trie() {
        root = new TrieNode();
    }
    
    /** Inserts a word into the trie. */
    public void insert(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExist(c)) {
                curr.setNextTrieNode(c);
            }
            curr = curr.getNextTrieNode(c);
        }
        curr.setIsWord(true);
    }
    
    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExist(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return curr.getIsWord();
    }
    
    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        TrieNode curr = root;
        for (char c : prefix.toCharArray()) {
            if (!curr.isExist(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return true;
    }
    
    class TrieNode {
        private boolean isWord;
        private Map<Character, TrieNode> map;
        
        public TrieNode() {
            this.isWord = false;
            this.map = new HashMap<>();
        }

        public boolean getIsWord() {
            return this.isWord;
        }

        public void setIsWord(boolean isWord) {
            this.isWord = isWord;
        }

        public boolean isExist(char c) {
            return this.map.containsKey(c);
        }

        public void setNextTrieNode(char c) {
            this.map.put(c, new TrieNode());
        }

        public TrieNode getNextTrieNode(char c) {
            if (!this.map.containsKey(c)) {
                return null;
            }
            else {
                return this.map.get(c);
            }
        }
    }
}
