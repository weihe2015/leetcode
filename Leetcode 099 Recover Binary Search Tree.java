/*
Two elements of a binary search tree (BST) are swapped by mistake.

Recover the tree without changing its structure.

Follow up:
A solution using O(n) space is pretty straight forward.
Could you devise a constant space solution?
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Iterative:
    /**
     * Basic Idea: User in order traversal to find the first node in wrong order
     *    then find secondNode = currNode when nodes are in wrong order
     *    At the end, swap first and second Nodes
     */
    public void recoverTree(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = root;
        TreeNode prev = null;
        TreeNode node1 = null;
        TreeNode node2 = null;
    
        while (!stack.isEmpty() || curr != null)  {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            // find the incorrect order nodes:
            if (prev != null && prev.val >= curr.val) {
                // [1,3,null,null,2]
                if (node1 == null) {
                    node1 = prev;
                }
                node2 = curr;
            }
            prev = curr;
            node = curr.right;
        }
        swap(node1, node2);
    }

    private void swap(TreeNode node1, TreeNode node2) {
        int temp = node1.val;
        node1.val = node2.val;
        node2.val = temp;
    }

    // Follow up: Space Complexity: O(1), use Morris Traversal for in order traversal.
}