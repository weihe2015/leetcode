/*
Given a binary tree, return all root-to-leaf paths.

For example, given the following binary tree:

   1
 /   \
2     3
 \
  5
All root-to-leaf paths are:

["1->2->5", "1->3"]
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    /**
     * @param root the root of the binary tree
     * @return all root-to-leaf paths
     */
    // Solution 1: DFS:
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        binaryTreePaths(res, root, root.val + "");
        return res;
    }
    
    private void binaryTreePaths(List<String> res, TreeNode curr, String path) {
        if (curr == null) {
            return;
        }
        if (curr.left == null && curr.right == null) {
            res.add(path);
        }
        if (curr.left != null) {
            binaryTreePaths(res, curr.left, path + "->" + curr.left.val);
        }
        if (curr.right != null) {
            binaryTreePaths(res, curr.right, path + "->" + curr.right.val);
        }       
    }

    // Solution 2: StringBuffer
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<String>();
        binaryTreePaths(result, root, new StringBuffer());
        return result;
    }
    
    private void binaryTreePaths(List<String> result, TreeNode node, StringBuffer sb) {
        if (node == null) {
            return;
        }
        int n = sb.length();
        if (node.left == null && node.right == null) {
            sb.append(node.val);
            result.add(sb.toString());
            // Delete Characters from index n to index sb.length();
            sb.delete(n, sb.length());
            return;
        }
        sb.append(node.val);
        sb.append("->");
        binaryTreePaths(result, node.left, sb);
        binaryTreePaths(result, node.right, sb);
        // Delete Characters from index n to index sb.length();
        sb.delete(n, sb.length());
    }

    // Solution 3: Level Order Traversal
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> res = new ArrayList<String>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        Queue<String> pathQueue = new LinkedList<>();
        queue.offer(root); 
        pathQueue.offer(root.val + "");
        
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                String currPath = pathQueue.poll();
                if (currNode.left == null && currNode.right == null) {
                    res.add(currPath);
                }
                
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                    pathQueue.offer(currPath + "->" + currNode.left.val);
                }
                
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                    pathQueue.offer(currPath + "->" + currNode.right.val);
                }
            }
        }
        return res;
    }
}