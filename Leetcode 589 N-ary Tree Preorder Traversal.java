/**
 * Given an n-ary tree, return the preorder traversal of its nodes' values.

 
For example, given a 3-ary tree:
        1
   3      2     4
5    6
Return its preorder traversal as: [1,3,5,6,2,4].
 * 
*/
/*
// Definition for a Node.
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val,List<Node> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
    // Recursive:
    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        preorder(result, root);
        return result;
    }
    
    private void preorder(List<Integer> result, Node node) {
        if (node == null) {
            return;
        }
        result.add(node.val);
        for (Node childNode : node.children) {
            preorder(result, childNode);
        }
    }

    // Iterative:
    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<Node>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node currNode = stack.pop();
            result.add(currNode.val);
            for (int i = currNode.children.size() - 1; i >= 0; i--) {
                stack.push(currNode.children.get(i));
            }
        }
        return result;
    }
}