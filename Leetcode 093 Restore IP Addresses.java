/*
Given a string containing only digits, restore it by returning all possible valid IP address combinations.

For example:
Given "25525511135",

return ["255.255.11.135", "255.255.111.35"]. (Order does not matter)
*/
public class Solution {
    public List<String> restoreIpAddresses(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        int[] paths = new int[4];
        int n = s.length();
        dfs(res, s, 0, n, 0, paths);
        return res;
    }

    private void dfs(List<String> res, String s, int idx, int n, int segmentIdx, int[] paths) {
        if (idx == n && segmentIdx == 4) {
            String ipAddressStr = buildStringFromArray(paths);
            res.add(ipAddressStr);
            return;
        }
        if (idx >= n || segmentIdx == 4) {
            return;
        }
        for (int i = 1; i <= 3 && idx + i <= n; i++) {
            String segment = s.substring(idx, idx+i);
            int num = Integer.parseInt(segment);
            if (num > 255 || (i >= 2 && s.charAt(idx) == '0')) {
                break;
            }
            paths[segmentIdx] = num;
            dfs(res, s, idx+i, n, segmentIdx+1, paths);
            paths[segmentIdx] = -1;
        }
    }
    
    private String buildStringFromArray(int[] paths) {
        StringBuffer sb = new StringBuffer();
        for (int path : paths) {
            if (path != -1) {
                sb.append(path).append('.');
            }
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    // Backtracking, Running Time Complexity: O(1), ipv4: 32bits, 2^32 total numbers of IP address. 
    // Space Complexity: O(1)
    public List<String> restoreIpAddresses2(String s) {
        List<String> result = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return result;
        }
        restoreIpAddresses(result, s, 0, 0, new StringBuffer());
        return result;
    }
    
    private void restoreIpAddresses(List<String> result, String s, int index, int numCount, StringBuffer sb) {
        // If there are more than 4 numbers, invalid case, return
        if (numCount > 4) {
            return;
        }
        if (numCount == 4 && index == s.length()) {
            result.add(sb.toString());
            return;
        }
        // Find 4 numbers for 3 times:
        for (int i = 1; i <= 3; i++) {
            if (index + i > s.length()) {
                break;
            }
            String numStr = s.substring(index, index + i);
            // If number is starts with 0 and the number's length is greater than 1, skip. Ex: avoid 022, 013 ...
            // If the number is greater than 255, skip.
            if ((numStr.charAt(0) == '0' && numStr.length() > 1) || (i == 3 && Integer.parseInt(numStr) > 255)) {
                continue;
            }
            sb.append(numStr);
            // If there are less than 3 numbers, add . at the end
            if (numCount < 3) {
            	sb.append(".");
            }
            restoreIpAddresses(result, s, index + i, numCount+1, sb);
            // reset stringbuffer back to previous state.
            if (numCount < 3) {
                sb.setLength(sb.length() - i - 1);
            }
            else {
            	sb.setLength(sb.length() - i);
            }
        }
    }
}