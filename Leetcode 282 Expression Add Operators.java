/**
Given a string that contains only digits 0-9 and a target value, return all possibilities to add binary operators (not unary) +, -, or * between the digits so they evaluate to the target value.

Example 1:

Input: s = "123", target = 6
Output: ["1+2+3", "1*2*3"] 
Example 2:

Input: s = "232", target = 8
Output: ["2*3+2", "2+3*2"]
Example 3:

Input: s = "105", target = 5
Output: ["1*0+5","10-5"]
Example 4:

Input: s = "00", target = 0
Output: ["0+0", "0-0", "0*0"]
Example 5:

Input: s = "3456237490", target = 9191
Output: []
*/

public class Solution {
    static private final String ADD = "+";
    static private final String MINUS = "-";
    static private final String TIMES = "*";
    static private final String DIVIDE = "/";
    // Running Time Complexity: O(N * 4^(N - 1))
    public List<String> addOperators(String num, int target) {
        List<String> result = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        int level = 0;
        long sum = 0;
        long prevNum = 0;
        addOperators(result, new StringBuffer(), num, target, level, sum, prevNum);
        return result;
    }
    
    private void addOperators(List<String> result, StringBuffer sb, String s, int target, 
                              int level, long sum, long prevNum) {
        int n = s.length();
        if (level == n) {
            if (target == sum) {
                result.add(sb.toString());
            }
            return;
        }
        long num = 0;
        for (int i = level; i < n; i++) {
            char c = s.charAt(i);
            // stop when leading zero found
            // to skip case like: "105", target = 5
            num = num * 10 + (c - '0');
            if (i > level && num < 10) {
                break;
            }
            int prevLen = sb.length();
            if (level == 0) {
                sb.append(num);
                addOperators(result, sb, s, target, i+1, sum + num, num);
                sb.setLength(prevLen);
            }
            else {
                // Add
                sb.append(ADD).append(num);
                addOperators(result, sb, s, target, i+1, sum + num, num);
                sb.setLength(prevLen);
                
                // Minus
                sb.append(MINUS).append(num);
                addOperators(result, sb, s, target, i+1, sum - num, -num);
                sb.setLength(prevLen);
                
                // Multiply
                sb.append(TIMES).append(num);
                addOperators(result, sb, s, target, i+1, sum - prevNum + prevNum * num, prevNum * num);
                sb.setLength(prevLen);
                
                // Divide:
                // TODO: how to handle precision? 
                // Example: 3456237490
                //          9191
                // output: ["34/5+62*37*4+9+0","34/5+62*37*4+9-0"]
                if (num != 0) {
                    sb.append(DIVIDE).append(num);
                    addOperators(res, sb, s, target, i+1, sum - prevNum + prevNum / num, prevNum / num);
                    sb.setLength(prevLen);
                }
            } 
        }
    }

    // Char Array Version Solution:
    public List<String> addOperators(String num, int target) {
        List<String> result = new ArrayList<String>();
        if (num == null || num.length() == 0) {
            return result;
        }        
        // The number of digits is num.length(), so the largest possible number of operators is num.length() - 1. 
        // Therefore, a num.length() + num.length() - 1 = 2 * num.length() - 1 length of buffer is needed.
        char[] cList = new char[2 * num.length() - 1];
        addOperators(result, cList, num, target, 0, 0, 0, 0);
        return result;
    }
    
    private void addOperators(List<String> result, char[] cList, String s, int target, int level, long sum, long prevNum, int len) {
        int n = s.length();
        if (level == n) {
            if (sum == target) {
                result.add(new String(cList, 0, len));
            }
            return;
        }
        int j = level == 0 ? len : len + 1;
        long num = 0;
        for (int i = level; i < n; i++) {
            char c = s.charAt(i);
            num = num * 10 + (c - '0');
            if (i > level && num < 10) {
                break;
            }
            cList[j] = c;
            j++;
            if (level == 0) {
                addOperators(result, cList, s, target, i+1, sum + num, num, j);
            }
            else {
                cList[len] = '+';
                addOperators(result, cList, s, target, i+1, sum + num, num, j);
                
                cList[len] = '-';
                addOperators(result, cList, s, target, i+1, sum - num, -num, j);
                
                cList[len] = '*';
                addOperators(result, cList, s, target, i+1, sum - prevNum + prevNum * num, prevNum * num, j);
            }
        }
    }
}