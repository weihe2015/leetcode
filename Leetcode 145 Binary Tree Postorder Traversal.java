/**
 * Given a binary tree, return the postorder traversal of its nodes' values.

Example:

Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [3,2,1]
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive:
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        postorderTraversal(result, root);
        return result;
    }

    private void postorderTraversal(List<Integer> result, TreeNode node) {
        if (node == null) {
            return;
        }
        postorderTraversal(result, node.left);
        postorderTraversal(result, node.right);
        result.add(node.val);
    }

    // Iterative Solution:
    /**
     * Basic Idea: the same as Binary Tree Inorder Traversal,
     *  1. iterate until the end of left leaf and add those nodes into stack.
     *  2. Use peek to get currNode
     *  3. If current Node does not have right child, pop it out and add its value into list.
     *  4. Use prevNode to mark currNode whose value added into list, to avoid infinite loop
     *  5. Mark node as currNode.right if currNode has right child and this right child has not been visited. (prevNode != currNode.right)
    */
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode node = root;
        TreeNode prev = null;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode curr = stack.peek();
            // Check if currNode has right child, if not, pop it out and add the val into list
            if (curr.right != null && prev != curr.right) {
                node = curr.right;
            }
            else {
                // add currNode into list and mark prevNode as currNode;
                curr = stack.pop();
                result.add(curr.val);
                prev = curr;
            }
        }
        return result;
    }

    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            result.add(curr.val);
            if (curr.left != null) {
                stack.push(curr.left);
            }
            if (curr.right != null) {
                stack.push(curr.right);
            }
        }
        Collections.reverse(result);
        return result;
    }
}