/**
Given a m * n grid, where each cell is either 0 (empty) or 1 (obstacle). In one step, you can move up, down, left or right from and to an empty cell.

Return the minimum number of steps to walk from the upper left corner (0, 0) to the lower right corner (m-1, n-1) given that you can eliminate at most k obstacles. If it is not possible to find such walk return -1.


Example 1:

Input: 
grid = 
[[0,0,0],
 [1,1,0],
 [0,0,0],
 [0,1,1],
 [0,0,0]], 
k = 1
Output: 6
Explanation: 
The shortest path without eliminating any obstacle is 10. 
The shortest path with one obstacle elimination at position (3,2) is 6. Such path is (0,0) -> (0,1) -> (0,2) -> (1,2) -> (2,2) -> (3,2) -> (4,2).
 

Example 2:

Input: 
grid = 
[[0,1,1],
 [1,1,1],
 [1,0,0]], 
k = 1
Output: -1
Explanation: 
We need to eliminate at least two obstacles to find such a walk.

Constraints:

grid.length == m
grid[0].length == n
1 <= m, n <= 40
1 <= k <= m*n
grid[i][j] == 0 or 1
grid[0][0] == grid[m-1][n-1] == 0

https://leetcode-cn.com/problems/shortest-path-in-a-grid-with-obstacles-elimination/solution/wang-ge-zhong-de-zui-duan-lu-jing-bfssuan-fa-shi-x/
*/
class Solution {

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    /**
    Run Time Complexity: O(m * n * min(m+n, k))
    */
    public int shortestPath(int[][] grid, int k) {
        int m = grid.length;
        int n = grid[0].length;
        // k 的上限是 m * n, 但考虑一条从 (0, 0) 向下走到 (m - 1, 0) 再向右走到 (m - 1, n - 1) 的路径，它经过了 m + n - 1 个位置，其中起点 (0, 0) 和终点 (m - 1, n - 1) 没有障碍物，那么这条路径上最多只会有 m + n - 3 个障碍物
        k = Math.min(k, m+n-3);

        boolean[][][] visited = new boolean[m][n][k+1];
        int[] start = new int[]{0,0};
        int[] end = new int[]{m-1, n-1};
        
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{0,0,0});
        visited[0][0][0] = true;
        
        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] curr = queue.poll();
                int i = curr[0];
                int j = curr[1];
                int oneCnt = curr[2];
                
                if (i == end[0] && j == end[1]) {
                    return steps;
                }
                
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    // 越界 或者 穿越障碍次数已满
                    if (x < 0 || x >= m || y < 0 || y >= n || (grid[x][y] == 1 && oneCnt >= k)) {
                        continue;
                    }
                    int newCnt = grid[x][y] == 1 ? oneCnt + 1 : oneCnt;
                    // 四个方向节点是否被访问过（第三维度）
                    if (visited[x][y][newCnt]) {
                        continue;
                    }
                    visited[x][y][newCnt] = true;
                    queue.offer(new int[]{x,y,newOneCnt});
                }
            }
            steps++;
        }
        return -1;
    }
}