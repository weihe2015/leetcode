/**
Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.

The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are not zero-based.

You may assume that each input would have exactly one solution.

Input: numbers={2, 7, 11, 15}, target=9
Output: index1=1, index2=2
*/
public class Solution {
	// Use Two pointers
	// Running Time: O(n)
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        int low = 0, high = nums.length-1;
        while (low < high) {
            int sum = nums[low] + nums[high];
            if (sum == target) {
                res[0] = low+1;
                res[1] = high+1;
                break;
            }
            else if (sum < target) {
                low++;
            }
            else {
                high--;
            }
        }
        return res;
    }

    // Use Binary Search, For each index, binary search its target on the right.
    // Running Time Complexity: O(NlogN)
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        if (nums == null || nums.length == 0) {
            return res;
        }
        for (int i = 0, max = nums.length; i < max; i++) {
            int diff = target - nums[i];
            int low = i+1, high = max-1;
            while (low <= high) {
                int mid = low + (high - low) / 2;
                if (nums[mid] == diff) {
                    res[0] = i+1;
                    res[1] = mid+1;
                    break;
                }
                else if (nums[mid] < diff) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
        }
        return res;
    }

    // Use Binary search for finding first appearence of item in an array, which may have duplicates
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        for (int i = 0, n = nums.length; i < n-1; i++) {
            int low = i+1, high = n-1;
            int diff = target - nums[i];
            while (low < high) {
                int mid = low + (high - low)/2;
                if (nums[mid] < diff) {
                    low = mid + 1;
                }
                else if (nums[mid] > diff) {
                    high = mid - 1;
                }
                else {
                    high = mid;
                }
            }
            if (nums[low] == diff) {
                res[0] = i+1;
                res[1] = low+1;
                break;
            }
        }
        return res;
    }
    // Naive way to solve this problem:
    // Running Time Complexity: O(N^2), Space Complexity: O(1)
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        if (nums == null || nums.length == 0) {
            return res;
        }
        for (int i = 0, max = nums.length; i < max-1; i++){
            for (int j = i+1; j < max; j++){
                int sum = nums[i] + nums[j];
                if (sum == target){
                    res[0] = i+1;
                    res[1] = j+1;
                    break;
                }
            }
        }
        return res;
    }

    // Use hashMap to solve the problem:
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        if (nums == null || nums.length == 0) {
            return res;
        }
        // Use hashMap:
        // key -> num, value -> index
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, max = nums.length; i < max; i++) {
            int num = nums[i];
            int diff = target - num;
            if (!map.containsKey(diff)) {
                map.put(num, i);
            }
            else {
                res[0] = map.get(diff)+1;
                res[1] = i+1;
                break;
            }
        }
        return res;
    }
}