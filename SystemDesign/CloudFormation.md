### Github URL:
- https://github.com/ACloudGuru-Resources/course-mastering-aws-cloudformation

#### Terminology:
- AWSTemplateFormatVersion
- Description
- Metadata
- Parameters
- Mappings
- Conditions
- Transform
- Resources
  - Logical ID:
    - Type
    - Properties
```
Resources:
  MyEC2Instances:
    Type: AWS::EC2::Instance
    Properties:
      ImageId: ami-43874721
      InstanceType: t2.micro
        - Key: "Name"
          Value: "Simple Value"

Resources:
  MyS3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: HelloWorld
      AccessControl: PublicRead
      WebsiteConfiguration:
        IndexDocument: index.html

Resources:
  MySecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Enable SSH access via port 22
      SecurityGroupIngress:
        -  IpProtocol: tcp
           FromPort: '22'
           ToPort: '22'
           Cidrlp: 0.0.0.0/0
```
- Outputs

### Multiple Resources:
```
Resources:
  Ec2Instance:
    Type: 'AWS::EC2::Instance'
    Properties:
      InstanceType: t2.micro
      ImageId: ami-43874721 # Amazon Linux AMI in Sydney
      Tags:
        - Key: "Name"
          Value: !Join [ " ", [ EC2, Instance, with, Fn, Join ] ]
      SecurityGroups:
        - !Ref MySecurityGroup
  MySecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Enable SSH access via port 22
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: 0.0.0.0/0
```

### Mappings:
```
!FindInMap [MapName, TopLevelKey, SecondLevelKey]

Mappings:
  RegionMap:
    us-east-1:
      AMI: ami-76f0061f
    us-west-1:
      AMI: ami-655a0a20

Resources:
  Ec2Instance:
    Type: 'AWS::EC2::Instance'
    Properties:
      ImageId: !FindInMap
        - RegionMap
        - !Ref AWS::Region
        - AMI
```

### Input Parameters:
- Supported Parameter Types:
  - String
  - Number
  - List<Number>
  - CommaDelimitedList
  - AWS-specific types (AWS::EC2::Image::Id)
  - Systems Manager Parameter types

```
Parameters:
  InstTypeParam:
    Type: String
    Default: t2.micro
    AllowedValues:
      - t2.micro
      - m1.small
      - m1.large
    Description:
      EC2 Instance Type

Resources:
  EC2Instance:
    Type:
      AWS::EC2::Instance
    Properties:
      InstanceType:
        Ref:
          InstTypeParam
      ImageId: ami-2f726546
```

#### Output:
```
Outputs:
  InstanceDns:
    Description:
      The instance Dns
    Value:
      !GetAtt
        - EC2Instance
        - PublicDnsName
```

### UserData
- A resource of type AWS::EC2::Instance has a property called UserData
- UserData is Base64 encoded
- Works for both Linux and Windows
- Only runs on the first boot cycle
- Beware of the time it takes to execute

```
Resources:
  EC2Instance:
    Type: AWS::EC2::Instance
    Properties:
      UserData:
        !Base64 |
         #!/bin/bash
         yum update -y
         yum install httpd -y
         service httpd start
```

### CloudFormation Helper Script
- cfn-init:
  - Read and interprets Metadata to exeucte AWS::CloudFormation::Init
- cfn-signal:
  - Used to signal when resource or application is ready
- cfn-get-metadata:
  - Used to retrieve metadata based on a specific key
- cfn-hup:
  - Used to check for updates to metadata and execute custom hooks


#### CloudFormation Init
- cfn-init:
```
Resources:
  MyInstance:
    Type: "AWS::EC2::Instance"
    Metadata:
      AWS::CloudFormation::Init
        config:
          packages:
          groups:
          users:
          sources:
          files:
          commands:
          services:
    Properties:
```
- Single config key: config
- Multiple config keys: Configsets
```
installweb:
  packages:
    yum:
      httpd: []
  services:
    sysvinit:
      httpd:
        enabled: true
        ensureRunning: true

installphp:
  packages:
    yum:
      php: []

AWS::CloudFormation::Init:
  configSets:
    webphp:
      - "installphp"
      - "installweb"
```

#### Change Sets
- Four Operations of a Change Set:
  - Create:
    - Create a change set for an existing stack by submitting a modified template, and / or parameter values. This does not modify existing stack
  - View:
    - After creating, you can view proposed changes
  - Execute:
    - Executing a Change Set updates the changes on the existing stack.
  - Delete:
    - You can delete a Change Set without performing the changes
- Resource Change:
  - Action:
    - Action taken on the resource (Add | Modify | Remove)
  - Logical | ResourceID & PhysicalResourceId
    - Resource's Logical Id (EC2Instance) and Physcial Id
  - ResourceType
    - The type of CloudFormation resource (AWS::EC2:Instance)
  - Replacement:
    - For Modify action, will CloudFormation delete the old resource and create a new one
  - Scope:
    - Which resource attribue is triggering the update
  - Details:
    - Description of the change to the resource

#### Intrinsic Functions:
- Long:
  - Fn::GetAtt: [ logicalNameOfResource, attributeName ]
- Short:
  - !GetAtt logicalNameOfResoure.attributeName
- !Ref:
  - Return the value of the specified parameter or resource
  - Using a **parameter's** logical name, returns the value of the parameter
  - Using **resource's** logical name, returns a value that is typically used to refer to that resource, such as a physical ID.
```
Resources:
  SomeBucket:
    Type: AWS::S3::Bucket
Outputs:
  MyBucketResourceName:
    Value: !Ref: SomeBucket
```
- !GetAtt:
  - Return the value of an attribute from a resource in the template
  - Most resources have multiple attributes that you can choose from
```
Resources:
  SomeBucket:
    Type: AWS::S3::Bucket
Outputs:
  MyBucketResourceName:
    Value: !GetAtt: SomeBucket.Arn
```
- !Join:
  - Appends a set of values into a single value, separated by the specified delimiter
```
Resources:
  SomeBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: Dufresne
Outputs:
  WhoeEscaped:
    Value: !Join [" ", ["Andy", !Ref SomeBucket]]
```
- !Sub:
  - Subsitute variables in an input string with values that you specify
```
!Sub
  - www.${Domain}
  - Domain: !Ref RootDomainName

!Sub "arn:aws:ec2:${AWS::Region}:${AWS::AccountId}:vpc/${vpc}"
```
- !Select
  - Return a single object from a list of objects by index
  - Does not check for null alues or if the index is out of bounds of the array. Both conditions will result in a stack error.
```
!Select ["1", ["L","A"]]

AvailabilityZone: !Select
  - 0
  - Fn::GetAZs: !Ref AWS::Region
```
- Split:
  - Split a string into a list of string values at the locations of a chosen delimiter.
```
Outputs:
  FriendOfKobayashi:
    Value: !Select [2, !Split [",", !Ref UsualSuspectsParam]]
```
- !If
```
Outputs:
  StorageBucket:
    Description: xxxx
    Value: !If [ CreateNewBucket, !Ref NewBucket, !Ref ExistingBucket ]
```
- !Equals
```
Conditions:
  IsProd: !Equals [!Ref EnvParam, prod]
```
- !Not
```
Conditions:
  IsNotProd: !Not [!Equals [!Ref EnvParam, prod]]
```
- !Or
```
MovieOver:
  !Or [!Equals [gloveOn, !Ref ThanoState], !Condition DidThorAim]
```
- !And
Conditions:
  IsProd: !And
    - !Equals [!Ref EnvParam, prod]
    - !Condition SomeOtherCondition

- Export:
  - Declare output values that you can import into other stacks, return in response, or view on the AWS CloudFormation console
```
Outputs:
  StorageBucket:
    Description: xxx
    Value: !Ref MyBucket
    Export:
      Name: !Sub ${AWS::StackName}-Bucket
```

### Custom Resources:
- What they are
- arn::aws:lambda:[region]:[account_id]:function:[function_name]
```
My CustomResource:
  Type: Custom::MyCustomResource
  Version: 1.0
  Properties:
    ServiceToken: !ImportValue mycustomresource-lambda-arn
```
- Request:
```
{
  "RequestType": "Create",
  "RequestId": "unique id for this create request",
  "responseURL": "pre-signed-url-for-create-response",
  "ResourceType": "Custom::MyCustomResourceType",
  "LogicalResourceId": "name of resource in template",
  "StackId": "arn:aws:cloudformation:[region]:namespace:[namespace]",
  "ResourceProperties": {
    "key1": "string",
    "key2": ["list"],
    "key3": ["key4": "map"]
  }
}

{
  "RequestType": "Update",
  "RequestId": "unique id for this create request",
  "responseURL": "pre-signed-url-for-create-response",
  "ResourceType": "Custom::MyCustomResourceType",
  "LogicalResourceId": "name of resource in template",
  "StackId": "arn:aws:cloudformation:[region]:namespace:[namespace]",
  "PhysicalResourceId": "custom resource provider-defined physical id",
  "ResourceProperties": {
    "key1": "string",
    "key2": ["new-list"],
    "key3": ["key4": "map"]
  },
  "OldResourceProperties": {
    "key1": "string",
    "key2": ["list"],
    "key3": ["key4": "map"]
  }
}

{
  "RequestType": "Delete",
  "RequestId": "unique id for this create request",
  "responseURL": "pre-signed-url-for-create-response",
  "ResourceType": "Custom::MyCustomResourceType",
  "LogicalResourceId": "name of resource in template",
  "StackId": "arn:aws:cloudformation:[region]:namespace:[namespace]",
  "PhysicalResourceId": "custom resource provider-defined physical id",
  "ResourceProperties": {
    "key1": "string",
    "key2": ["list"],
    "key3": ["key4": "map"]
  }
}
```
- Response:
```
{
  "Status": "SUCCESS",
  "RequestId": "unique id for this create request (copied from request)",
  "LogicalResourceId": "name of resource in template (copied from request)",
  "StackId": "arn:aws:cloudformation:[region]:namespace:[namespace]",
  "PhysicalResourceId": "custom resource provider-defined physical id",
  "Data": {
    "keyThatCanBeUsedInGetAtt1": "data for key 1",
    "keyThatCanBeUsedInGetAtt2": "data for key 2"
  }
}

{
  "Status": "FAILED",
  "Reason": "Required failure reason string"
  "RequestId": "unique id for this create request (copied from request)",
  "LogicalResourceId": "name of resource in template (copied from request)",
  "StackId": "arn:aws:cloudformation:[region]:namespace:[namespace]",
  "PhysicalResourceId": "custom resource provider-defined physical id"
}
```
- Feature & Use Cases:
  - Support Resource Missing
    - Transcoder pipelines or presets
    - A Cognito User Pool Domain
    - IAM role tags
  - Custom Solution within AWS
    - Look up subnets and write to DDB mapping
    - Provisioning and validating an ACM certificate
  - Support an External Service
    - Algolia vs AWS ElasticSearch
    - Auth0 vs AWS Cognito
    - Github vs CodeCommit
    - Orchestrate onPrem resource
  - Non-infrastructure Provisioning Steps
    - Database initialization or migration scripts
    - Cache busting or CloudFront invalidations
- Limit:
  - Custom Resource Response:
    - Maximum amount of data that a custom resource provider can pass: 4096 bytes
  - Default timeout:
    - Maximum amount of time before resource will timeout: 1-2 hours
  - VPC endpoints:
    - If using VPC Endpoints feature the custom resource must have access to CloudFormation-specific S3 buckets.

- Important Notes:
  - Timeouts & Catching Errors:
    - Exceptions can cause your function to exit without sending a response. If CloudFormation does not receive a response to confirm that the operation is a success or a failure, it will wait until the operation times out before starting a stack rollback.
    - If the exception occurs again on rollback, CloudFormation waits again for a timeout before ending in a rollback failure. During this time, your stack is unusable.
  - How CloudFormation Indentifies and Replaces Resources.
    - CloudFormation compares the new and previous **PhysicalResourceId** returned by your function to determine if a new resource needs to be created.
    - However, the old resource is not implicitly removed to allow a rollback if necessary. When the stack update is completed successfully, a **delete** event request is sent with the old physical ID as an identifier.
  - Design Functions for Idempotency
    - An idempotent function can be called any number of times and with the same inputs, will result in the same output as if it had been done only once.
    - Idempotency is valuable when working with CloudFormation to ensure that retries, updates and rollbacks don't create duplicate resources or introduce errors.

- Helper functions:
  - custom-resource-helper
    - A python based library provided by AWS that uses decorators
  - cfn-lambda
    - A Node.js-based library that provides an easy way to build custom resources with Javascript. Loads of additional feature as well.
  - cfn-wrapper-python
    - Another python-based library that was the inspiration for custom-resource-helper
  - cfn-custom-resource
    - Another python-based library uses classes over decorators

#### Macros & Transforms
- Macros: String Operations:
  - String functions
    - A simple transform that you can use to manipulate strings with a list of functions like `Capitalize`, `Replace`, `Uppercase`, `Strip` etc.
    - Snippet Transform
    - Multiple operations in a string transform
```
Resources:
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      Tags:
        - Key: Capitalize
          Value:
            "Fn::Transform":
              - Name: String
                Parameters:
                  InputString: This is a small string
                  Operation: Capitalize

# This Is A Small String
```
  - Common Tags:
    - A simple transform that you provide global tags as parameters and it will ensure all resources in your template that supports tags has them applied. It will not touch any existing tags on resources.
    - Template-Level Transform
```
Transform:
  Name: CommonTags
  Parameters:
    Name: MySuperCoolApp
    CreatedBy: Me
    CostAllocation: MySuperCoolApp

Resources:
  Bucket1:
    Type: AWS::S3::Bucket
    Properties:
      Tags:
        - Key: CostAllocation
          Value: override

# Name: MySuperCoolApp
# CreatedBy: Me
# CostAllocation: override
```
  - Custom Resource Types
    - AWS::S3::Object
      - When you couple Custom Resources and Transforms, you can effectly create your own CloudFormation resources that complete align to the syntax of offical resources.
        - Template-Level Transform
          - Used to transform to convert the non-official resource (AWS::S3::Object) into an accepted Custom Resource. (Custom::S3Object)
        - Custom Resource:
          - Provision/action on the provided properties to create/update/delete files into the chosen S3 bucket.

```
Resources:
  Bucket:
    Type: AWS::S3::Bucket
  Object1:
    Type: AWS::S3::Object
    Properties:
      Target:
        Bucket: !Ref Bucket
        Key: README.md
        ContentType: text/markdown
      Body:
        # My Text File
        This is my text file;
  Object3:
    Type: AWS::S3::Object
    Properties:
      Source:
        Bucket: !GetAttr Object1.Bucket
        Key: !GetAttr Object1.key
      Target:
        Bucket: !Ref Bucket
        Key: README-copy.md
        ACL: public-read
```

#### Nested Stacks:
- Create:
  - Anatomy:
    - Nested stacks are declared like any other in the resources section using the `AWS::CloudFormation::Stack` type.
      - Parent stacks can pass in parameters into nested child stacks
      - Parent stacks can also pull data out via stack outputs of the child.
```
# Root Stack
Resources:
  ComputeStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      Parameters:
        LCImageId: ami-xxxxxx
      TemplateURL: https://xxxx

# ComputeStack:
Parameters:
  LCImageId:
    Type: String
    Description: Load Conf Image ID
Resources:
  LC:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      ImageId: !Ref LCImageId
      InstanceType: t2.large
```
  - Use cases:
    - Reuse Common Template Patterns
    - Isolate Information sharing

- Nested Stacks
  - Recover
    - Delete Stack:
      - Using the DeleteStack API operation with the **RetainResources** option listing the resources that failed to roll back
      - **Consideration**:
        - Scorched earth approach, that will cause you to have to regenerate the stack again.
    - Non-Stack Impacting Changes:
      - If the changes needed to correct the stack failure are to the account (limits etc), then you can simply make those changes and then perform a **ContinueUpdateRollback** to have the stack try again.
      - **Consideration**:
        - When this is possible, it really is the base case scenario.
    - Stack Impacting Changes:
      - If you need to make changes to the underlying stack resources to address the issue then use **ContinueUpdateRollback** along with the ResourceToSkip option.
      - **Consideration**:
        - Required you bring the stack back into sync. Any deviation once stack has settled, may cause future issues when updating due to stack drift.

### Working with Secrets
- Compare SSM Simple System Manager with Secrets Manager:
  - This SSM service primarily handles a variety of EC2 management tasks
    - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Working_with_secrets.png)

- Nested vs Expects vs AWS::Include
  - Template Strategies:
  - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Template_Strategies.png)
  - Framework:
    - Multi-layer architecture
      - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Template_Strategies_MultiLayer_Architecture.png)
    - Service-oriented architecture (SOA)
      - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Template_Strategies_ServiceOrientedArchitecture.png)
  - Reuse & Stack Separation:
    - ways to find and reuse repeated patterns in the infrastructure
      - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Template_Strategies_Reuse_Stack_Separatation.png)
      - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Template_Strategies_Reuse_Stack_Separatation-reused.png)

- Template Storage and Revisions
  - Test:
    - validate-template (AWS CLI)
      - Expected parameters for intrinsic functions
      - Resources Types
      - General JSON/YAML structure
    - cfn-lint:
      - Read JSON + YAML (Including YAML short form)
      - Detect invalid property names + types
      - Detect invalid Ref
      - Detect invalid mappings
      - Detect invalid format
      - Missing or invalid AWSTemplateFormatVersion
      - Intrinsic Function Support
      - Condition Support
      - Ref
      - Detect missing required properties
      - SAM template validation (experimental support)
  - Workflow:
    - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Template_Storage_Workflow.png)

### Service Roles:
- What are they?
  - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_ServiceRoles.png)

### Working with EC2 instance:
- CloudFormationInit
  - UserData vs CloudFormationInit
    - UserData:
      - OS & distribution specific
      - Procedural steps A -> B -> C
      - Designed to run once
      - Not scale well for complicate function
    - CloudformationInit (cfn-init)
      - cfn-init
      - OS independent
      - Desired state engine -> C
      - Supports config sets
      - Suports order
      - Control timing and reboots
      - Allows authentication
      - Can run multiple times
```
EC2Instance:
  Type: AWS::EC2::Instance
  CreationPolicy:
    ResourceSignal:
      Count: 1
      Timeout: PT5M
  Metadata:
    AWS::CloudFormation::Init:
      configSets:
        full_install:
          - install_thing1
          - install_thing2
        partial_install:
          - install_things:
  Properties:
  xxx
  UserData:
    "Fn::Base64": !Sub |
      #!/bin/base -xe
```

- ConfigSets:
  - Overview:
```
Resources:
  EC2Instance:
    Type: AWS::EC2::Instance
    Properties:
      ...
    Metadata:
      AWS::CloudFormation::Init
        configSets:
          full_install:
            - install_thing1
            - install_thing2
          partial_install:
            - install_thing2
        install_thing1:
          packages:
          files:
          commands:
        install_thing2:
          groups:
          users:
          sources:
```
  - Package:
    - Install packages with typical package managers like apt, msi, python, rpm, rubygems, and yum.
```
packages:
  yum:
    nginx: []
    nginx:
      - "8.0"
      - "7.4"
  msi:
    awscli: xxxxx
```
  - Groups:
    - Create and configure Groups:
```
groups:
  groupA: {}
  groupB:
    gid: "67"
```
  - Users:
    - Create and configure users
```
users:
  xxx:
    groups:
      - groupA
      - groupB
    uid: "666"
    homeDir: /root
```
  - Sources:
    - Download and unpack archive files into a local directory.
      - Eg: tar, tar+gzip, tar+bz2 and zip
```
sources:
  /tmp/temp1: https://github.com/...
  /tmp/temp2: https://....
```
  - Files:
    - Create Files from inline content or external sources and then attach permission to them
```
files:
  /tmp/file.tmp:
    source: https://xxx
    mode: "000644"
    owner: xxx
    group: groupB
  /etc/cfn/cfn-hup.conf:
    content: !Sub |
      [main]
      stack=${AWS::StackId}
      region=${AWS::Region}
      verbose=true
      interval=5
    mode: "000644"
    owner: root
    group: root
```
  - Commands:
    - Execute Commands on the EC2 isntance in alphabetical order
  - Services:
    - Define which services should be enabled or disabled when the instance is launched.
```
services:
  sysvinit:
    nginx:
      enabled: true
      ensuringRunning: true
      files:
        - /etc/nginx/nginx.conf
      sources:
        - /var/www/html
  windows:
    cfn-hup:
      enabled: true
      ensureRunning: true
      files:
        - "C:\\cfn\\cfn-hup.conf"
        - "C:\\cfn\\hook.d"
```
### Resource Policies:
- Why we need them:
  - ![CloudFormation](aws-solution-architecture-assciate/CloudFormation_Resource_Policy_purpose.png)
  - Creation Policy:
    - The creation policy is invoked only when AWS CloudFormation creates the associated resource
    - Support Resources:
      - AWS::AutoScaling::AutoScalingGroup
      - AWS::EC2::Instance
      - AWS::CloudFormation::WaitCondition
```
EC2Instance:
  Type: AWS::EC2::Instance
  CreationPolicy:
    ResourceSignal:
      Count: 1
      Timeout: PT5M // timeout 5 mins
  Properties:
    xxx
```
  - Update Policy:
    - Used to specify how AWS Cloudformation handles updates to the following resources:
    - Supported Resources:
      - AWS::AutoScaling::AutoScalingGroup
      - AWS::ElasticCache::ReplicationGroup
      - AWS::Elasticsearch::Domain
      - AWS::Lambda::Alias
```
UpdatePolicy:
  AutoScalingScheduleAction:
    IgnoreUnmodifiedGroupSizeProperties: 'true'
  AutoScalingRollingUpdate:
    MinInstancesInService: '1'
    MaxBatchSize: '2'
    PauseTime: PT1M
    WaitOnResourceSignals: 'true'
```
  - UpdateReplacePolicy:
    - Used to specify how AWS CloudFormation handles resources that are set to be replaced as part of an update:
    - Options:
      - Delete
      - Retain
      - Snapshot
```
DBInstance:
  Type: AWS::RDS::DBInstance
  UpdateReplacePolicy: Retain
  Properties:
  xxx
```
  - DeletionPolicy:
    - Use to preserve or backup a resource if it is deleted from the stack or the stack is deleted.
    - If a resource has no DeletionPolicy attribute, Cloudformation deletes the resource by default.
      - Snapshot (DBInstance, EC2::Volume etc)
      - Retain (S3 etc)
    - **Note**: This does not apply to resources whose physical instance is replaced during stack update operations.
```
NewVolume:
  Type: AWS::EC2::Volume
  Properties:
    Size: 100
    Encrypted: true
    AvailabilityZone: !GetAtt Ec2.AvailabilityZone
    Tags:
      - Key: MyTag
        Value: TagValue
  DeletionPolicy: Snapshot
```

- cfn-hup:
  - Overview:
    - cfn-init is used to act on directives to fulfill a desired state
    - cfn-hup's job is to detect stack changes and then take actions by running a desired command. which could perhaps be to tell CFN init to run an updated config set.
    - cfn-hup.conf:
      - The primary config file used by cfn-hup that includes the name of the stack and the AWS credentials that the cfn-hup daemon targets.
```
files:
  /etc/cfn/cfn-hup.conf:
    content: !Sub |
      [main]
      stack=${AWS::StackId}
```
  - hooks.d/*.conf
    - A file containing actions performed by the cfn-hup daemon based on stack triggers.
    - This file defined a hook or trigger as well as a command to perform on the instance in the case that the trigger is fired.
    - Possible values for this trigger include stack creation, update and deletion.
    - The file also defines which part of the template to track for updates.
    - This hook would cause cfn-init to rerun anytime a stack update occurs.
```
/etc/cfn/hooks.d/cfn-post-update.conf:
  content: !Sub |
    [cfn-post-update]
    triggers=post.update
    path=Resources.
            EC2.Metadata.
            AWS::CloudFormation::Init
    action=/opt/aws/bin/cfn-init -v
    --stack ${AWS::StackName}
    --resource EC2Instance
    --configsets partial_install
    --region ${AWS::Region}
  mode: "00400"
  owner: root
  group: root
```

### Serverless Application Repository:
  - Overview:
    - Search and Discover:
      - Browser or search through a huge collection of open source application templates or find one privately stored within your team.
    - Configure:
      - Choose one, or nest multiple applications to result in a more complex solution. Set environment variables, parameter values and more before deploying the app to any of you accounts.
    - Deploy and Manage
      - Deploy your application to your account and manage it from the AWS Management console, or with the CloudFormation tools you're used to. Take control of your applications with complete IAM integration.