/**
1. Handling ambiguity, some detail left out. Ask clarify questions

    ** System design?
    ** Class hierarchy ?
    ** Specific Question and write some methods?

2. Systematic Approach: How you approach the problem, clarify assumptions.

    ** How is the parking lot designed? Building, open space?
    ** Accessiblity? If all the parking lots are free?
    ** How many spots in the parking lot?
    ** Multiple levels
    ** Multiple entrance ? (Concurrency)
    ** Dependency between levels?
    ** Optimize fill up particular area first?
    ** Price, multiple size of slots? 2 or 3 difference sizes
    ** Pricing strategy? bigger slot more expensive? 
    ** Fair price strategy? Charge type of custom's vehicle, but not the actual spot he occupies
    ** Parking model? Premiums? Parking for people with disabilities, in front of entrance?

Ex: 4 different sizes of parking spots:
    * 1) Small:  motorcycles 
      2) medium: small car
      3) large: big car
      4) X-large: bus
    * Allow put smaller cars in bigger lots. 
    * Not allow large car put into smaller slots

    Class hierachy

    1. Parking lot Class
        * String zipCode
        * Method placeVehicle(Vehicle vehicle)

    2. Vehicles: cars, motorcycle, Truck, buses (Inheritance Strategy)
        Abstract Vehicles:
            * String License Plate (Main form of idenfication) Have in common
            * Enum Color
        4 Classes inheritance it
    
    3. Spot Class
        * Long ID
        * int level
        * enum size

    What kind of goals we have with this?
        * Place and retrieve of a car as efficient as possible, by passing license plate, retrieveing vehicles and freeing up the spot again
        *

3. Coding: Finding the right data structure

4. Testing
*/