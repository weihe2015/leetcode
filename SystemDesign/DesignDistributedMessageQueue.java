/**
    Distributed Message Queue:
    https://www.youtube.com/watch?v=iJLL-KPqBpM&t=59s

    * Problem Statement:
        Producer  <------>  Consumer

        * Need to communicate with each other
        * Option 1: Synchonous communication, wait for the response
            * Pros: easier and faster to implement
            * Cons: harder to handle consumer service failure
                -- when and how to properly retry failed requests
                -- how not to overwhelm consumer service with too many requests
                -- how to deal with a slow consumer service host.

        * Option 2: Asynchronous communication Queue
            * Producer sends data to that component
            * One consumer gets this data short time after
            * Data is distribubted, data is stored across several machines.

            * Difference from topic of Kafka: message that is published goes to each and every subscriber.
            * Message: is received by one and only one consumer

    * Functional Requirement:
        * SendMessage(messageBody)
        * ReceivedMessage()
        * DeleteMessage
        * CreateQueue
        * DeleteQueue

        * Avoid duplicate submissions
        * Security requirements
        * Specific ordering guaranteed

    * Non-functional requirements: have a priority over others
        * Scalable (handles load increases, more queues and messages)
        * Highly Available (survices hardward/network failure)
        * Highly Performant (single digit latency for main operations)
        * Durable (Once submitted, data is not lost)

        * Service Level Agreement Number
            * defines the level of service expected by a customer from a supplier, laying out the metrics by which that service is measured, and the remedies or penalties, if any, should the agreed-on service levels not be achieved
            * Ex: Minimum throughput our system needs to support

        * Minimize hardware cost or operational support cost

    * High Level Architecture:
        * Virtual IP and Load Balancer
            * Explain how load balanncer will help us achieve high throughput and availablity
            * When domain name is hit, request is transfer to one of the Virtual IP registered in DNS  
            * VIP resolved to a load balancer device, which knows about Front-end hosts.
            * Problems:
                * Single point of failure:
                * Limit with number of requests they can process and number of bytes they can trasfer, popular messages coming in, reach the limits.
            * Primary and seconary node:
                * Primary node: accepts connections and serve requests.
                * Seconard node: monitors the primary
                * If any reason the primary node is unable to accept connections, the secondary node will take over.
            * Scalability:
                * Multiple Virtual IPs partitioning,
                * One host name can have multiple virtual IPs.
                * Assign multiple A records to the same DNS name for the service
                * Requests are partitioned across several load balancers.
                * Spread load balancer across several data centers, improve both availability and Performant

        * Frontend web service:
            * Lightwight web service
            * Consisting of stateless machines located across data centers
            * Rule of thumb:
                -- Keep it as simply as possible
            * Actions:
                * Request validation
                    -- Required parameters are present
                        -- Ex: Queue name existed in parameter
                        -- Ex: Message size does not exceed a specified threshold
                    -- Data falls within an acceptable range
                * Authentication / Authorization
                    -- Authentication is the process of validating the identity of a user or a service
                        -- Message sender is a registered customer of our distributed queue service
                    -- Authorization is the process of determining whether or not a specific actor is permitted to take a certain action.
                        -- Verify that sender is allowed to publish message to the message queue it claims
                * TLS (SSL) termination
                    -- TLS is a protocol that aims to provide privacy and data integrity
                    -- TLS termination refers to the process of decrypting request and passing on an unencrypted request to the back end service
                    -- SSL on the load balancer is expensive 
                        -- Handled not by a Frontend service itself, but a separate HTTP proxy that runs as a process on the same host.
                * Server side encryption
                    -- Message are encrypted as soon as Front-end services receive them
                    -- Message are stored in encrypted form and Front-end decrypts
                * Caching
                    -- Cache stores copies of source data.
                    -- Store metadata information about the mostly actively used queues
                    -- Helps to reduce load to backend service, increases overall system throughput and availability, decrease latency
                    -- stores user ID information to save on calls to auth services
                * Rating limiting (throttling)
                    -- Throttling is the process of limiting the number of requests you can submit to a given operation in a given amount of time
                    -- Throttling protects the webservice from being overwhelmed with requests.
                    -- Leaky bucket algorithm is one of the most famous
                * Request dispatching
                    -- Responsible for all the activities associated with sending requests to backend services (client management, respnose handling, resource isolation etc)
                    -- Bulkhead pattern helps to isolate elements of an application into pools so that if one fails, the others will continue function.
                    -- Circuit Breaker pattern prevents an application from repeatedly trying to execute an operation that's likely to fail
                * Request deduplication
                    -- May occur when a response from a successful sendMessage request failed to reach a client
                    -- Lesser an issue for 'at least once' delivery semantics, a bigger issue for 'exactly once' and 'at most once' delivery semantics
                    -- caching is usually used to store previously seen request ids.
                * Usage data collection
                    -- Gather real-time information that can be used for audit and billing (invoices)

        * Metadata Service:
            * Store information about queue
            * Everytime a queue is created, we store information about it in the database.
            * Caching layer between the front-end and a persistent storage.
            * Handle many reads and a relative small number of writes.
                * Reads every time message arrives and write only when new queue is created.
            * Strongly consistent storage is not required:
                * It can avoid potential concurrent updates.

            * Option 1:
                * Cache is small we can store whole dataset into every cluster node. 
                * Frontend cluster calls a random chosen Metadata service host.
            * Option 2:
                * Data set is too big and cannot be placed into a memory of a single host.
                * Partition data into small chunks, called shards. Store each chunk of data on a separate node in a cluster.
                * Front-end service then knows which shard stores the data and calls the shard directly.
                * Consistent Hash Ring, 
            * Option 3:
                * Similar to option 2, but Front-end service calls a random Metadata service host
                * Metadata service host will know where to forward this requests to

        * Backend Service:
            * Questions:
                * Where and how do we store messages?
                    * Database is an option?
                        * Not the best one. Distributed message queue, should be able to handle a very high throughput.
                        * This throughput will be offloaded to the database. => 
                            -- problem of distributed message queue <==> problem of DB that can handle high throughput.
                            -- Exist highly available and scalable databases.
                    * RAM and a local disk of a backend host
                * How to replicate data?
                    * Replicate within a group of hosts.
                    * Survive host hardware and software failure
                * How front-end host select backend host to send data to?
                * How does Front-end know where to retrieve data from?
                    * Metadata service.
                    * Send:
                        * Message comes to Front end, which consults to Metadata service what backennd host to send
                        * Message is sent to a selected backend host and data is replicated.
                    * Receive:
                        * FrontEnd talks to Metadata service to identify a backend host that stores the data.

            * Option A: Leader-follower relationship:
                * Each backend instance is considered a leader for a particular set of queue
                * Ex: instance B is the leader for queue q1
                * Message sent to the leader, leader is responsible for data replication
                * Message retrieved from the leader instance, leader is responsible for cleaning up the original message and all the replicas.
                * Need a component to help us with leader election and management:
                    * In-cluster manager (Ex: ZooKeeper)
                        * Maintain a mapping between queues, leaders and followers.
                    * Sophisticated component:
                    * Need to be reliable, scalable, and performant
            * Option B: Small cluster of independent hosts:
                * Each cluster consists of 3-4 machines distributed across several data centers.
                * Metadata service knows which cluster is responsible for storing messages for the queue (q1)
                * Front End service makes a call to a random selected instance in the cluster
                * Instance is responsible for data replication across all nodes in the cluster.
                * Selected host is responsible for data replication (Send) and message cleanup (Receive)
                * Need a component to manage queue to cluster assignment:
                    -- Out-cluster Manager
                        -- Maintain a mapping between queues and clusters.

            * In-cluster Manager:
                * Manage queue assignment within the cluster
                * Maintain a list of hosts in the cluster
                * Monitors heartbeats from hosts, listens to heartbeats from each instances.
                * Deals with leader and follower failures
                * Big message in queue:
                    * Split queue into parts (partitions) and each partition gets a leader server
            * Out-cluster Manager:
                * Manages queue assignment among clusters.
                * Maintain a list of clusters. May not know about each particular instance, but need to know about each cluster
                * Monitor each independent cluster health
                * Deals with overheated clusters.
                    * New queue may no longer be assigned to clusters that reaches their capacity limit
                * Big message in queue:
                    * Split queue across several clusters.
                    * Message for the same queue are equally distributed between several clusters.

    * Other important topics:
        * Queue creation and deletion:
            * Queue Creation:
                * Auto-created: first message hits Front-end Service
                * API for queue creation:
                    * Better option: have more control over queue configuration parameters.
            * Queue Deletion:
                * Controversial, cause a lot of harm and must be executed with caution.
            * Message Deletion
                * Option 1:
                    * Not delete a message right after it was consumed.
                    * Consumers have to be responsible for what they already consumed.
                    * Need to maintain some kind of an order for messages in the queue to keep track of the offset, which is the position of a message within a queue.
                    * Messages can then be deleted several days later by a scheduled job.
                    * Used by Apache Kafka.
                * Option 2:
                    * Messages are not deleted immediately, but marked as invisible, so that other consumers may not get already retrieved messages
                    * Consumer that retrieve the messages, need to call delete message API to delete the message from a backend host.
                    * If the message was not explicitly deleted by a consumer, message becomes visible and may be delivered and processed twice
                    * Used by Amazon SQS
        
        * Message replication:
            * Can be replicated synchronously or asynchronously
            * Synchronously: 
                * When backend host receives new message, it waits until data is replicated to other hosts.
                * Only if replication is fully completed, successful response is returned to a producer.
                * Pros:
                    * Higher durability
                * Cons:
                    * Higher latency for send message
            * Asynchronous:
                * Response is returned back to a producer as soon as message is stored on a single backend host.
                * Message is later replicated to other hosts.
                * Pros:
                    * More performant
                * Cons:
                    * Not guarantee that message will survive backend host failure

        * Message delivery semantics:
            * At most once:
                * When messages may be lost but are never redelivered.
            * At least once:
                * When messages are never lost but may be redelivered.
            * Exactly once:
                * When each message is delivered once and only once.
            * Reason to have three semantics:
                * Hard to achieve exactly once in practice.
                * Many potential points of failure.
                    * Producer may fail to deliver or deliver multiple times
                    * Data replication may fail
                    * Consumers may fail to retrieve or process the message
                * All these adds complexity 
                * Most distributed queue solutions today support at least once delivery.
                * Good balance between durability, availability and performant
        
        * Push vs Pull:
            * Pull:
                * Consumer constantly sends retrieve messages requests, new message is available in the queue, it is sent back to a consumer.
                * Pull is easiler to implement than a push
                * From consumer perspective, need to do more work if we pull
            * Push:
                * Consumer is not constantly bombarding FrontEnd Service with receive calls. Consumers is notified as soon as new message arrives to the queue.

        * FIFO:
            * In distribubted system, hard to maintain a strict order.
            * Message A may be produced prior to message B 
            * But hard to guarantee that message A will be stored and consumed prior to message B.
                * Many distributed queue solutions do not guarantee a strict order
                * Or have limitation around throughput, as queue cannot be fast while doing additional validation and coordination to guarantee a strict order.
        
        * Security:
            * Messages are secure to transfer and retrieve from a queue.
            * SSL over HTTPS
            * Encrypt message in backend service

        * Monitoring:
            * Need monitor components that in the system (Front-end, metadata, backend service)
            * Need to monitor health of our distributed queue system and give customers ability to track state of their queue.
            * Each service writes Log
            * Create dashboards for each microservice and setup alerts.

    * Conclusion:
        * Non functional requirement:
            * Scalale? 
                * Yes -> each component is scalable
            * Highly Available? 
                * Yes -> No single point failure, each component is deployed across several data centers
            * Highly Performant:
                * Yes -> Each individual microservice needs to be fast.
                * Need to run our software in high-performant data centers.
            * Durable? 
                * Yes, we replicate data while storing and ensure messages are not losing during transfer from a producer to consumer
*/