/**

Design TikTok:
    Link: https://www.youtube.com/watch?v=Z-0g_aJL5Fw

    * Requirement:
        * Q:
            * Q1: quick overview of what we are looking for 
            * Q2: be specific on portion of TikTok we are focusing on
        * Mobile app, video sharing between users
        * Be able to upload videos
        * Be able to view a feed of videos
        * Follow users, see videos they post
        * Perform actions on videos like favoriting, comment them
        
    * Backend Infrastructure

    * Functional requirement:
        * Top level buckets of work that we want to have
        1. Upload videos, create an API, client agnostic, accept user data to upload videos
            * Max 1 minute long
            * can add text and caption to it, text data associated with it
        2. View video feeds:
            * aggregate videos, videos of people you follow
            * interested to know of trending or video recommendation
            * Favorite video, follow particular video creator
        3. Video interactions

    * Non-functional requirement:
        * Availability, Latency, Scale
        * Highly available 99.999% system, (Weekly: 6s) because of the scale of the users that are going to be using it.
            * https://uptime.is/five-nines
            * balance our budget of compute resource
        * Latency:
            * Cache content on mobile app itself
            * Pull content in the background
        * Scale
            * A million active users per day (DAU)
            * Couple estimate: give the idea of how to store everything
                * 1 min video, compressed 360p video resolution
                * Each user uploads 2 videos per day
                * User metadata, 1K per user per day
        * Read heavy system

    * API endpoints:
        * uploadVideo:
        * Video Feed:
            * Pre-fetch first three videos, without letting user to wait a long time
            * Preload a list of top t10 videos url that we are going to load for that user, before user gets to feed page
            * Cache:
                * Run on demand or on schedule
                * Compile playlist for users
    
    * Database Schema;
        * Video storage: blob storage like S3, fully qualified link
            * Video: 
                * userId, videoUrl, metadata:
        * Relational vs non-relational:
            * More structured data, user data object, linking different tables together
                * one user have many videos, link two different tables by join queries
            * No-SQL:
                * Log data, free form of structure, free from searching for key value

    * Bottleneck:
        * Region:
            * Geolocation
            * Regional data centers
        * Put video content to CDN (Content Delivery Network), to cache the content
            * route internet traffic, avoid overly hit the system
        * Load balancer in front of API endpoints
            * Multiple services running the API endpoints
        * Database Sharding:
            * Database Shard service
            * Split load between databases

*/