/**
Cloud Load Balancing
 -- Distributed to multiple regions
 -- Meet HA requirements
 -- Autoscaling
 -- CDN

 With load balancing, System 1 mmillion queries per second, as close as possible to the user 

 Global vs Regional Load Balancing

 Global Load Balancing: backend endpoints lives in multiple regions

 Regional load balancing: backend endpoints live in a single region

 Traffic Types: HTTP, HTTPS, SSL, TCP, UDP

 Internal / External Load Balancing:
    External Load Balancing to direct traffic to GCP network.
    Internal Load Balancing: distribute traffic within GCP network

  Four types of load balancing:
  -- HTTP(S) LB: L7 traffic
                 Look at the application layer to decide how to distribute requests.
                 Terminates network traffic, reads the message, makes a load balancing decision
  -- TCP Proxy TCP traffic for ports other than 80 or 8080 without SSL offload
  -- SSL Proxy, on SSL offload onn ports other than 80 or 8080.
  -- Network LB: for TCP or UDP traffic
                 Layer 4 traffic,
                 Look at info at the transport layer to decide how to distribute requests
                 Involves source, destination IP address, ports in the header.

    Global HTTPS LB: cross regionnnal failover and overflow
                     Distribution algorithm directs traffice to the next closest instance with available capacity.  
*/    