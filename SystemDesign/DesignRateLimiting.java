/**
https://www.youtube.com/watch?v=FU4WlwfS3G0

Design Rate Limiting Solution:

* Problem Statement:
    -- Webservice hosting
    -- One or several clients started sending much more requests than they did previously.
        -- Various reasons:
            -- Client is another popular webservice and it experienced a sudden traffic spike
            -- Devs run load test
        -- Noisy Neighbor Problem:
            -- When one client utilizes too much shared resources on a service host:
                -- CPU, memory, disk or network I/O
            -- Other clients may have high latency for their requests, or high rate of failed requests.
    -- Throttling:
        -- Limit X requests per second per client
        -- Requests out of limit will be either rejected or process delayed.

* Requirement Clarification:
    * Don't we need to have a system that scales to handle high load?
        -- Not happen immediately
    * Alternative to throttling: max connections in Load Balancer and max threads count on the service endpoint.
        -- Load Balancer rejects any requests over the limit or send the requests to a wait queue to process later.
        -- indiscriminate (do not choose requests based on user)
        -- WebService expose several different operations, some fast and some slow heavy, take a lot of resource processing
        -- Load Balancer does not have knowledge about the cost of each operation
        -- Limit # of requests for a particular operation can only be done on application level, not Load Balancer

    * Algorithmic Problem Or Object Oriented Design Problem?

    * Real World Problem?
        * Enough to throttle on each host individually?
        * Ideal world:
            * Load Balancer distributed requests to servers evenly, no need in any distributed solution, Applications do not need to talk to each other. Throttle requests individually.
        * Real World:
            * Load Balancer cannnot distributed requests evenly manner
            * Application servers have different resources. Server A can process 7 but server B can only process 2
        * WebServers will communicate with each other and share informations about # of client requests each one of them process so far.

    * Functional Requirement:
        * Boolean allowRequest(request):
            -- Should return a boolean value of whether this request is acceptable or not.

    * Non-Functional:
        * Low Latency: (Make decision as soon as possible)
        * Accurate (as accurate as we can get) do not want to throttle customers unless it is absolutely required
        * Scalable (rate limiter scales out together with the service itself).
            -- Supports an arbitrarily large number of hosts in the cluster.
        * Not important:
            -- High Availability:
            -- Fault Tolerance:
                * If rate limiter cannot make decision quickly, it is always allowed, not to throttle

* Start with a simple solution:
    * Or mention to the interviewer a simple solution and evolve the solution along the interview
    * Single server rate limiting solution:
        -- No communication between servers just yet.
        -- Throttle Rules Retriever:
            -- Each rule specifies a number of requests allowed for a particular client per second.
            -- Rules Service + Rules database: (Service owners)
                * Client X is allowed to make 50 requests per second
            -- Background process that polls Rules service periodically if there is any new or modified rules.
            -- Store rules in memory on the host.

        -- Throttle Rules Cache:
            -- All the rules grap by rules retriever will be stored in this cache.

        -- Client Identifier Builder
            -- Key: login for registered clients, remote IP address or some other combination of attributes uniquely identify the client.
            -- Separate component to stress that:
            -- Client identification is an important step of the whole process
        
        -- Rate Limiter:
            -- Check key againsts rules in rules cache
            -- Allow -> Request Processor, further process requests.
            -- Reject -> 503: Service unavailable
                         429: too many requests.
                         add it to a wait queue to process it later.
                         drop the requests on the floor.

* Three directions:
    * Rate Limiter Algorithm
        -- Analogy of a bucket filled with tokenns
        -- Three fields:
            * Maximum amount of tokens it can hold
            * Amount of tokens currently available
            * A refill rate
            * Tokens added to bucket arrive at constant rate
        -- Every request takes a token until it is empty
    * Object-oriented design
        -- Main classes and interfaces of the throttling library
        -- JobScheduler:
            -- Runs every several seconds, retrieve rules from Rules Service
        -- RulesCache:
            -- Stores rules in memory
        -- ClientIdentifier:
            -- Builds a key that uniquely identifier
        -- RateLimiter:
            -- Responsible for decision Making
    * Distributed throttling solution:
        -- How services host share data between each other 
        -- Services can handle 4 requests per second per client
        -- Each bucket should have 4 tokens initially.
            -- All requests for same bucket may land on the same hosts
            -- Requests for the same key will not be evenly distributed.
            -- Bucket Servers communicate with each other, substract the sum of tokens of other servers
        -- Expect that system may be processing more requests that we expect. 
            -- Allow negative number of available tokens

    * How clusters communicate with each other:
        -- Message Broadcasting: (Full Mesh)
            * Every host in the cluster knows about every other host in the cluster, share messages with each one of them
            * How does everyone knows if a new server is added or removed?
                * Option 1:
                    -- Use 3rd party service to listen to heartbeats coming from each host
                    -- Each host asks this service for full list of hosts
                * Option 2:
                    -- Resolve some user provided information
                    -- User specifies a host, which knows all the hosts behind it.
                * Option 3:
                    -- User provides a list of hosts via some configuration file.
                    -- Need to update this file everytime this list changes.
            * Cons:
                * Not scalable
                * Number of messages grows quadratically with the number of hosts in the clusters.
                * Not be able to support big clusters.
        -- Grossip Protocol: Communication
            * Based on the way that epidemics spread
            * Random Peer selection:
                * With a given frequency, each machine picks another machine at random and share data.
        -- Distributed Cache:
            * Redis, In-memory cache
            * Pros:
                -- Distributed Cache cluster is relatively small, service cluster can scale out independently
                -- Can be shared among many different service teams in the organization
                -- Each team can setup their own small cluster

        -- Coordination Service:
            * 3rd party that helps to choose a leader
            * Choose a leader helps to decrease number of messages broadcasted within the cluster
            * Leaders ask everyone to send it all infos
            * Each host only needs to talk to a leader, or a set of leaders, where each leader is for own range of keys.
            * Censenus Algorithm:
                * Paxos and Raft
                * Implement Corrdination Service
            * Cons:
                * Setup and maintain Coordination Service
        
        -- Random Leader Selection:
            * More than one leader are selected
            * Each leader will calculate rate and share with everyone else
                -- Cause unnecessary messagin overhead

        -- Ways about communication between hosts:
            * TCP / UDP
            * TCP: guarantees delivery of data and guarantees that packets will be delivered in the same order they were sent.
            * UDP: not guarantees you get all the packets and the order is not guaranteed.
                -- Faster, no error checking.

    * Integrate with services:
        * Two Options:
            * Option 1:
                        Service Host
                    ------------------- 
                    |  Service Process |
                    |                  |
                    |   Rate Limiter   |
                    |  ----------------|
                    
                    -- Run Rate Limiter as a part of the service process 
                    -- Rate Limiter is distributed as a collection of classes, 
                    -- a library that should be integrated with the service code
                    -- Pros:
                        -- Faster, no need to do any inter-process call
                        -- resilient to the inter-process call failures, no such calls

            * Option 2:
                       Service Host
                    ------------------- 
                    |  Service Process |
                    |                  |
                    |   Rate Limiter   |
                    |      client      |
                    |  ----------------|
                    |        ^         |
                    |        |         |
                    |        v         |
                    |   Rate Limiter   |
                    |      Process     |
                    | -----------------|

                -- Its own process (daemon) 
                -- Two libraries: Daemon itself and the client
                -- responsible for inter-process communication between service process and the daemon
                -- Pros:
                    -- Programming language agnostic
                    -- No need to do integration on code level
                    -- Use its own memory space
                    -- Helps better control behavior for both the service and the daemon 

    * Other possible questions:
        * Store millions of buckkets in memory?
            -- No need to keep buckets in memory if there are no requests coming from the client for some period of time.
            -- Only keep the bucket in memory if client keep sending request within an interval 
        * Failure scenarios:
            -- Daemon can fail, causing other hosts in the cluster lose visibility of this failed daemon.
                * Less requests throttled in total.
        * Synchronization:
            -- Synchronization in the token bucket
            -- Atomic reference in thread-safe class
        * What clients of our service should do with throttled calls?
            -- Queue the request and re-send them later
            -- Retry throttled requests
            -- Smart way: exponential backoff and jitter
                -- Retry requests exponentially, increasing the waiting time between retries up to a maximum backoff time.
                -- Retry requests several times, but wait a bit longer with every retry attempt.
                -- Add random wait time to spread out the load. Otherwise, retry requests will happen at the same time.

    * Experiment:
        -- For majority of clusters where size is less than several thousands of nodes, and number of active buckets per second is less than tens of thousands, gossip communication over UDP will work really fast and is quite accurate.
        -- In case of rally large clusters, like tens of thousands of hosts, no longer rely on host to host communincation in the service cluster, as it become costly. Need separated cluster for making a throttling decision.
            -- Increase latency and operational cost.

*/ 

public class TokenBucket {
    private final long maxBucketSize;
    private final long refillRate;

    private volatile double currentBucketSize;
    private volatile long lastRefillTimestamp;

    public TokenBucket(long maxBucketSize, long refillRate) {
        this.maxBucketSize = maxBucketSize;
        this.refillRate = refillRate;

        currentBucketSize = maxBucketSize;
        lastRefillTimestamp = System.nanoTime();
    }

    /**
        Normally the token will be 1, but slow operation can take more token
    */
    public synchronized boolean allowRequest(int tokens) {
        refill();

        if (currentBucketSize >= tokens) {
            currentBucketSize -= tokens;
            return true;
        }

        return false;
    }

    private void refill() {
        long now = System.nanoTime();
        double tokensToAdd = (now - lastRefillTimestamp) * refillRate / 1e9;
        currentBucketSize = Math.min(currentBucketSize + tokensToAdd, maxBucketSize);
        lastRefillTimestamp = now;
    }
}