/**
https://eng.uber.com/postgres-to-mysql-migration/

    * Postgres limitation:
        -- Inefficient architecture for write
            * Immutable row data: row -> tuple.
            * Unique identifier ctid, represents on-disk location
            * The B-tree is defined on the id field, and each node in the B-tree holds the ctid value.
            * Internally, Postgres stores within each tuple a version field and pointer to the previous tuple
            * Under the hood, Postgres uses another field holding the row version to determine which tuple is most recent
            * A small logical update becomes a much larger, costlier update when translated to the physical layer
                * Add new row tuple to table
                * Update primary key index to add a record for the new tuple
                * Update secondary key index to add a record
            * Each of these writes needs to be reflected in the write ahead log as well.
        
        -- Inefficient data replication
            * Already maintain write-ahead-log and use it to implement two-phase commit
            * Allows atomicity and druability aspects of ACID.
            * Postgres implements streaming replication by sending the WAL on the master database to replicas
            * Refuse to serve any queries until database instance finishes crash recovery process
            * Write amplification problemm also translates into a replication problem.
            * Network bandwidth problem with data replication within datacenters

        -- Issues with table corruption
            * Postgre 9.2 issue, replicas followed timeline switches incorrectly -> misapply some WAL records
            * The duplicated results returned from the database caused application logic to fail in a number of cases.
            * potential to spread into all of the databases in a replication hierarchy.

        -- Poor relica multiversion concurrency control (MVCC) support
            * No true replica MVCC support
            * Open transaction, Updates to DB are blocked if they affect rows held open by the transcation.
            * Postgres pauses WAL application until transaction has ended.
            * Postgres applies a timeout in such situations: if a transaction blocks the WAL application for a set amount of time, Postgres kills that transaction.

        -- Difficulty upgrading to newer releases
            * Cannot replicate data between different version of Postgres

    * Relational database key tasks:
        -- Provide insert/update/delete capabiities
        -- Provide capabilities for making schema changes
        -- Implement a multiversion concurrency control (MVCC) mechanism so that different connections have a transactional view of data they work with

    
    * MySQL Architecture:
        * InnoDB storage engine:
            * Supports advanced features like MVCC and mutable data
            * InnoDB secondary index records hold a pointer to the primary key value
            * Need two lookups:
                * Finds primary key for a record
                * A second lookup searches the primary key index to find the on-disk location for the row.
            * InnoDB does row updates in place
            * Old row will be copied into special area -> rollback segment
        * Multiple different replication modes
            * Statement based replication replicates logical SQL statements
                -- Most compact but can require replicas to apply expensive statements to update small amounts of data
            * Row-based replication replicates altered row records
            * Mixed replication mixes these two modes

        * MySQL only primary index has a pointer to the on-disk offsets of rows.
            * Only needs to contain information about logical updates to rows.
            * Postgres:
                * Every physical change made to the disk needs to be included in the WAL stream. Small logical changes (such as updating a timestamp) necessitate many on-disk changes: Postgres must insert the new tuple and update all indexes to point to that tuple.

        * MySQL replication stream has logical updates: 
            * Read queries on replicas won’t block the replication stream
        
        * MySQL only increments its version if the replication format changes

    * Buffer Pool:
        * Postgres:
            -- Postgres allocates some memory for internal caches, but these caches are typically small compared to the total amount of memory on a machine
            -- Accessing data via the page cache is actually somewhat expensive compared to accessing RSS memory
            -- Each of these system calls incurs a context switch, which is more expensive than accessing data from main memory
        * MySQL:
            -- The InnoDB storage engine implements its own LRU in something it calls the InnoDB buffer pool
            -- Possible to implement a custom LRU design
            -- Fewer context switches
            -- Data accessed via the InnoDB buffer pool doesn’t require any user/kernel context switches.

    * Connection Handling:
        * MySQL:
            -- Implements concurrent connections by spawning a thread-per-connection
            -- Not uncommon to scale MySQL to 10,000 or so concurrent connections
        * Postgres:
            -- use a process-per-connection design
            -- More expensive:
                -- Forking a new process occupies more memory than spawning a new thread
                -- Interprocess communication (IPC) is much more expensive between processes than between threads
            -- Have poor support for handling large connection counts even when there is sufficient memory available


*/