/**
Design Yelp Geospatial Geohash:

    * A user can pan around on a map and reveal new points of interest
    * A user can zoom in and out on a map and see points of interest.
    * The map defaults to showing points of interest near their current location
    * A user can look at points of interest in different locations and countries

Scale:
    * How many daily active users can you reasonably expect? 10M users, 86,400 seconds in a day, an average of 12 users per second
    * 30 users per second for 1 million users at peak load
    * Volumn of data: 32 bits of integers ID (4 bytes), name: 24 characters, 1 character 1 bytes ~= 28 bytes.
    * Floats: 32 bits / 4 bytes, Double: 64 bits / 8 bytes
    * Add GeoHash, 5 characters 5 bytes,
    * 1 Billion POI ~= 33 GB data

Service and Storage:
    * Mobile Client -> Load Balancer -> API gateway -> Query Server -> Databases

APIs:
    * Write out method signatures with input parameters and return types
    * Write out queries if we choose RDMBS

Scaling:
    * Can we horizontally scale the back-end and mid-tiers? At approximately what ratio?
    * Are there replicas? 
    * Is there sharding?
    * How would you shard?
    * If you chose to shard into regions, what does that imply about the balance of your data?
    * What does your sharding imply about your read and write load?

Test cases:
    * Where you can anticipate on-call issues to arise?
    * What falls over and how might that change your approach?
    * how long does it take for tracking to make it to both data centers and how does that affect things? 

Extra Credits:
    Multiple Datacenters:
        * What would run where?
        * Should both data centers have a complete copy of all the data?
        * When new points of interest are added, how do they get replicated across data centers?
        * What happens if there's a natural disaster?

    Personalized results:
        * recommender systems:
        * Where and how does the training happen?
        * When are recommendations computed and how are they served?

    Tracking:
        * A/B test new designs
        * Generate data for training a machine learned personalized recommender you'll need to track how users are behaving.

Naive solution:
    * Store latitude and longitude for every POI, floating point of number, uniquely ping point each POI.
    * Same for user to get their current latitude and longitude
    * Make a range query to have latitude between [x_i, x_j] [y_i, y_j]
    * return the results with two criterias

暴力方法：
    * 计算位置与所有POI的距离，并保留距离小于50米的POI。
    * Cons: 
        * 计算经纬度之间的距离不能像求欧式距离那样平方开根号，因为地球是个不规整的球体
        * 执行了40万次复杂的距离计算函数
    * 先用矩形框过滤 只要进行两次判断（LtMin<lat<LtMax; LnMin<lng<LnMax），落在矩形框内的POI个数为n
        * 经度或纬度每隔0.001度，距离相差约100米
        * Cons:
            * 40万*矩形过滤函数 + n*距离函数（n<<40万）

Geospatial database:
    * Goal: simple query which gives us all the POI in a given range

    * Properties that you are looking for to solve such problems.
    * Optimized indexes, Geospatial indexes
    * Proximity queries, shape queries, distance queries
    
    * Geohashing: defines an area on the world map. World is divided into rectangles, represented as one character:
        -- 9 -> United States, 
        -- U -> Europe
        -- W -> East Asian
        -- Grid: 26 characters and numbers, 32 grids
        
        * Each grid is divided into smaller grid with certain size.
            -- these smaller grid has one more character after it.
            -- 9Y, 9A, 9C, 9I, 9D, 9E
        * Telescoping
            -- String of geohashing is unique, by zooming into this one grid, unique across whole world.
            -- This tring can be easily index, quick lookup

        * More columns:
            -- A column for every POI, six characters
        * Transform user location to geohashing 
        * Query the POI by having one less characters, which will give all the POI on the grids.
        * Denormolization: Store 6 characters, 5 characters with POI

        * Another solution: Geohash with integer ranges instead of 32 encoding.

    * Sharding:
        * Split up the databases into multiple databases or multiple tables
        * Shard the database into regions, 
            * One instance for north america
            * Another Instance for europe / Asia
*/