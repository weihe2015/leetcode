/**
 * Complex Architecture to implement, last optimization to try with: https://www.youtube.com/watch?v=iHNovZUZM3A
 * Sharding is a particular type of partitioning. (Horizontal sharding)

 * Problem: table is large, too many rows, read is slow.
    * Solution: partition horizontally:
        * partition table, split this large table into several smaller tables.
 
 * Problem: too much read:
    * Solution: create slave database to read only, master database to write, and replicate to slave databases.
 
 * Problem: too much write:
    * Solution: Scale by region, mutiple replication, two masters in different regions

 * Sharding:
    * splitting tables into different servers.
    * Tradeoff: no transaction, no ACID, no rollback, no commit
    * client application has the logic to know which server to write to.

https://www.youtube.com/watch?v=hdxdhCpgYo8

 * Options:
   * Scaling up with hardware
   * Add read Replicas:
      * Introduce eventual consistency problem, introduce stale data
   * Sharding
      * Foundamental: key that is predictable, use key as the input of hashing function to determine which shard it is
      * or use a table to store the mapping of the key to the shard --> introduce single point of failure.

* Pros:
   * Scalability, chunk data into a smaller subset of the problem
   * Availability
   * Fault tolenrance

* Cons:
   * Complexity
      * Partition mapping
      * Routing
      * Non-uniformity, unevenly growth
         * shard size can grow larger than others
   * Resharding the data
   * Analytics:
      * Ex: find all accounts with balances > 500
      * Scatter and Getter
      * Application layer:
         * knows where these informations are located
         * have ability to perform the query
         * Summarize the query
         * Aggregate the results and return it to the caller


 * Tool: vitess https://vitess.io/ 

 * Design a sharding schema for key-value storage 
 *   * Feature:
 *       * Size: 100 TB
 *       * Data growth per day: 1TB
 *       * Machine: 72GB of RAM, 10 TB
 *       * Num of machines: 20
 *       * key value entries are independent
 *    * Estimate:
 *       * Total storage size: 100TB, storage with each single machine: 10TB
 *            min number of machine to store data: 20
 *       * Data grow 1TB per day, utilization <= 80%, so add new machine every 8 days
 *    * Deep Dive:
 *       * Note: Don't directly say that we will use distributed file system like HDFS.
 *       * Fixed num of shards?
 *           No, because the data will keep growing and exceed 10TB size
 *       * num of shards and how to distributed data?
 *           Solution 1: Total num of shard: S, for each key, hash val = Hash(key)
 *                       assign key to Hash(key) % S.
 *                       If we add shard, all keys will change, since Hash(key) % (S+1)
 *           Solution 2: Consistent hashing: A ring of array https://www.youtube.com/watch?v=zaRkONvyGr8, http://michaelnielsen.org/blog/consistent-hashing/
 *                       * to have a hashing schema that allows us to add or remove a 
 *                         server without having changed the hash function
 *                       * hash(k) has value range of [0..R], val/R => [0...1] range
 *                       * machine 0...n, hash(n) / R => key of machine ID
 *                       * key -> hash(key) / R, counter clock-wise to find the macine to store it
 *                       * When you add new server, only the data of last server need to move,
 *                           The rest of data on other servers does not need to move
 *                       * It solves two problem: 
 *                             1. data are evenly distributed on each server, because hashing function is uniformed random, 
 *                                so each hash value will be uniformed having equal distance
 *                             2. Easy to add or remove servers, only one server will be affected.
 *                       * Advanced consistent hashing:
 *                             * Use multiple hashing functions: Hash1, Hash2, Hash3 .. k hash functions, k can be log(R)
 *                                   Use the same method previously, so each server will have k points
 *                                   The posibility that one single server having lots of requests will be lesser
 *                             * When one server is down, the requests can be going evenly to other servers
 *                                  * Technique to solve database failure is master-slaves / master-master modules
*/

/**
 * High Available Distributed key value store, which is network partition tolerant.
 *     * Feature: 
 *         * Size: 100TB
 *         * Update: support value update:
 *         * Size of value: It can be changed during update of value, they grew to a size where all of them don't fit on single machine
 *                 A value is needed to fit in one machine, so upper limit of value is 1GB
 *         * QPS:
 *              * Around 100K
 *     * Estimation:
 *         * Total storage: 100TB
 *         * Total estimate QPS: 100k,
 *         * Num of machines to store data: 100TB / 10TB = 10, but we need to have more machines within shards to lower QPS load on every shard.
 *     * Design Goal: CAP Theorm: Consistency, Availability and Partition Tolerance
 *         * Consistency vs availbility: 
 *                 We need high availability, so no consistency,
 *                 but availibility and partition tolerance with eventually consistent
 *         * Latency: The database should be available all the time, with low latency
 *     * Deep dive:
 *         * Note: Don't say that we will use NoSQL DB like Cassandra.
 *         * Sharding:
 *             * Shard the data and distributed the load amongst all machines
 *         * Normalization:
 *             * Database normalization: a technique of organizing the data into multiple related tables, 
 *                                to minimize data redundancy, not eliminating data redundancy
 *                        repetition of data increase size of database, cause insert/delete/update anomaly
 *                  * Split data into multiple tables
 *             * If data is normalized, need join across tables and across rows to fetch data.
 *             * If data is already sharded across machine, any join across machine is undesirable.
 *             * With denormalization, we would store same field at more than one place. => low latency
 *                 => have denomalized rows make shard easier.
 *         * Num of machine:
 *             * Solution 1: master node in each shard with slave node. client can read either master or slave
 *                           cons: when master is down, write has high latency, slave -> master, 
 *                                but it implies some time of unavailability
 *                                also data loss when master is down, and it was writing to slave
 *             * Solution 2: Multi master: both machines accept write and read traffic
 *                           cons: huge consistency concern, 
 *             * Solution 3: peer to peer system:
 *                           Single point of failure:
 *                              Peer to peer system where every node is equally priviledged and any two nodes can communicate
 *                           Example: Dynamo and Cassandra DB
 *                           Use consistent hashing to shard data for peer to peer system
 *        * Redundant data:
 *              Let P as our replication factor (P copies of data). For data D, we choose P nodes to store copies of D.
 *              Choose P nodes: choose the P clockwise consecutive nodes starting from the node where D is mapped by hash function.
 *              * virtual node is not master node for this data for either read or write.
 *        * Read/Write in system:
 *              * Write request: client -> write request -> coordinate node -> forward requests to mapping nodes -> wait for acknowledgement
 *              * Read request: client -> read request -> any node in the ring -> ask replica nodes for this data and return value
 *              * Read/Write consistency: W => min num of nodes where coordinating node should get back before write success
 *                      R -> min num of nodes where coordinating node should get back before read success
 *                      For consistency: W + R > P [replication factor (P copies of data)]
 *                          For fast write: W = 1 and R = P
 *                          For fast read: R = 1 and W = P
 *                          For write and read equally distributed, R = W = (P+1)/2
 *              * Cassandra: It use gossip protocol to ensure eventual consistency. 
 *                              Peer to peer communication protocol where nodes periodically exchange state info
 *                               about themselves and about other nodes they know about.
 *                               It will sync the data to all other replica in background
 *         * When one machine goes down:
 *                No node is only responsible for a piece of data, as long as W > P, some nodes are available for update and read.
 *                In case of less than W nodes to write, we can relax write consistency (sacrificing data consistency for availability)
 *                * Dynamo: "Hinted handoff": "sloppy quorum"
 *                       * distributed database with eventual consistency:
 *                          * If replicas set for item X is node A, B, C, but A is unreachable, sloppy quorum allows writes of X to be sent to B, C, D
 *                            Write to D will be stored with a hint that it should ideally have been sent to A.
 *                            Once A is detected to have recovered, D would attempt to handle off that data or operations.
 *                            => maintain database's expected levels of availability and durability across failure states. 
 *         * Kind of consistency:
 *                If keep W = P, we can provide strong consistency, but no available for writes
 *                So W < P, the best is eventual consistency. 
 *                   To solve this: 1. use criteria of last write wins, update the server with state with newer data.
 *                       2. Store augmented data for each row indicating all coordinating nodes for row till now.
 *                          Compare the augmented data, if one is the subset of others (all other writes seen by one of the row have been seen by the other rows)
 *                            We can safely ignore the one with smaller augmented data,
 *                               Otherwise, we have a conflict for the row and need application level logic to resolve it.
 * 
 */

/**
 * Distributed key value store, highly consistent and network partition tolerant
 *     * Features:
 *          * Amount of data to store: 100TB
 *          * Support data update
 *          * Size of the value increases with updates.
 *             * It has the limit of 1GB of upper cap
 *          * Estimated QPS: 10M
 *     * Estimation:
 *          * Total storage size: 100TB
 *          * Total estimated QPS: 10M
 *          * Min num of machines: assume each machine has 10TB, need more than 10 to lower QPS load on every shard
 *     * Design Goal:
 *          * Latency, Consistent and availability, Partition Tolerant
 *          * Latency: No, latency is not important, but it would be good to have lower latency
 *          * Consistency vs availability: Consistency, with no data loss
 *     * Deep Dive:
 *          * Sharding:
 *              *  Shard the data and distribute the load amongst multiple machines
  *         * Normalization:
 *              * Database normalization: a technique of organizing the data into multiple related tables, 
 *                                to minimize data redundancy, not eliminating data redundancy
 *                        repetition of data increase size of database, cause insert/delete/update anomaly
 *                  * Split data into multiple tables
 *              * If data is normalized, need join across tables and across rows to fetch data.
 *              * If data is already sharded across machine, any join across machine is undesirable.
 *              * With denormalization, we would store same field at more than one place. => low latency
 *                 => have denomalized rows make shard easier.
 *         * Num of machine per shard? read/write in every shard?
 *              * One copy of data, reading is consistent. read/write -> row -> shard -> machine IP
 *         * Problem may raise if we keep multiple copies of data:
 *              * Assume we have 3 copies of data: 
 *                  To maintain all copies in absolute sync, write will not succeed unless it writes to all 3 copies
 *                    => Result: high write latency, unreliable write
 *                    if we allow writes succeed on majority machines write succeed, But if the master dies, the data will lost and will be unavailable
 *         * If master keep track of where blocks are stored dies:
 *              master-master Database: 
 *                Keep standby master => active master when failure happens
 *                  Active master has shared network file system with standby master
 *                  When any namespace modification is performed by active master, it logs the record and stored it in standby master
 *                  Standby master applies them to its own namespace.
 *                  When failure happened, standby master ensure that it reads all edits from shared storage before promoting itself to active master   
 */