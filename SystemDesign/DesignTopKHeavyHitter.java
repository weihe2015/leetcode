/**
https://www.youtube.com/watch?v=kx-XDoPjoHw

Top K most freqent items.

Ex: 
    -- Find 100 of most searched keywords on Google
    -- Find top 100 viewed videos on Youtubes
    -- Find top 100 played songs on Spotify
    -- Find top 100 shard posts on Facebook
    -- most retweeted tweets on Twitters
    -- Most like photos on Instagram

With such scale, database or distributed cache is not an option here. 

Hundreds of thousands requests per second, even millions at peak. Database can handle millions of requests per second.

Big data processing problem.

* Problem: 
    -- return a list of most viewed videos for the last several minutes. Stream processing problem.

* Requirement:
    -- Functionality Requirement:
        -- API:
            * topK(k, startTime, endTime) (The list changes over time, We need to provide time interval, start and end time interval)
            
    -- Non functional Requirement:
        * Scalable (scales out together with increasing amount of data: videos, tweets, posts, etc)
        * Highly Available (survives hardware/network failures, no single point of failure)
        * Highly Performant (few tens of milliseconds to return top 100 list)
            -- Should be pre-calculated, avoid heavy calculation while calling this API.
        * Accurate (as accurate as we can get)
            -- Ex: By using data sampling, we may not count every event, but only a small fraction of all events.

* Start with single host:
    -- Let's assume whole data set can be loaded into a memory of that single host.
    -- List of events. When user opens a video, log such event by adding video identifier to the list of events.
    -- Use Priority Queue or sort the hash table entries by frequency. Make sure that heap only contains K elements.

* Scalable:
    * Process events in parallel:
        -- Load Balancer or a distributed queue.
        -- Each event then goes to one of the hosts in a cluster, we call it Processor Hosts
        -- Each Processor needs to flush accumulated data to a single storage host.
        -- Total throughput of the system has increased.
        -- Cons: 
            * Use too much memory on each processor as well as storage host.
    * Memory shortage:
        -- Partition data into smaller chunks
        -- Data Partitioner:
            -- Routing each individual video identifier to its own processor host.
                -- (B, F, A) -> Host 1
                -- (C, D, E) -> Host 2
            -- Each processor host only stores a subset of all the data.
            -- Each processor host contains its ownn list of k heavy hitters. Each such list is sorted.
            -- Create final list that combines information from every processor host?
                * External Merge N sorted lists.
                * Each processor hosts only pass a list of size k to the storage host
            -- Not accumulate data on a single host.
            -- By partitioning the data, we increased both scalability and throughput.
            -- Have some serious drawbacks and needs further improvement.
        
    * Bounded data sets or data sets of limited size.
        * Indeed can be split into chunks, find top K of each trunk and merge results together.

    * Unbound data sets, essentially infinite
        * Processor hosts can accumulate data only for some period of time.
        * Need whole data set for a particular time period
        * Store all the data on disk and use batch processing framework to calculate a top K
        * MapReduce Problem

    * Data Partitioning:
        -- Deal with data replication, copies of each partition are stored on multiple nodes.
        -- Deal with rebalancing, when a node is added to the cluster or removed
        -- Deal with hot partitions

    * Simple solution: 
        -- Make sacrifices along the way (Main sacrifice is Accuracy)
        -- Count-min sketch.
            * 2 Dimensional array:
                - Width: is in thousand
                - Height is small and represents a number of hash functions.
            * New element added in, calculate hash function value and increment 1 to the cell
            * In case there is a collision, increment that cell as well.
            * Among all cells of A, take the minimal value
                -- Because of collisons, some cells will contain overestimated values.
                -- By taking the minimum, we decrease a chance of overestimation.
                -- By using more hash function, we decrease the error
                -- There are formulas that help to calculate both parameters (Width and Height) based on the accurarcy we want to achieve and the probability with which we reach the accuracy.
                    * e: accuracy we want to have, d: certainty with which we reach the accuracy
                    * Formula:
                        -- width = Math.ceil(2.0 / e)
                        -- height = Math.ceil(-Math.log(1-d) / Math.log(2))
            * Has predefined size, does not grow over time.
        -- Heap:
            * Still need a heap to store a list of heavy hitters.
            * We replace a hash table, that could grow big, with a count-min sketch that always have a predefined size and never grow in size even when data set size increases.

* High Level Architecture
                                                           /  fast path -> Fast Processor -> Storage Service
    * User -> API Gateway -> Distributed Messaging System -
                                                           \  slow path -> Data Partitioner -> Distributed Message System -> Partition processor -> Distributed File System -> Frequency Count Map Reduce Job -> Top K MapReduce Job

    * When user clicks on a video, request goes through API gateway, component that represents a single entry point into a video content delivery system.
    * API Gateway routes client requests to backend services.
    * In this use case, we are interested in one specific function of API Gateways, log generation, when every call to API is logged. We will use these logs for counting how many times each video was viewed.
    * Implement background process that reads data from logs, does some initial aggregation and sends this data for further processing.
    * Allocate a buffer memory on API Gateway host, read every log entry and build a frequency count hash table we discussed before.
    * Buffer should have limited size, when buffer is full, data is flushed. If buffer is not full for a specified period of time, we can flush based on time.
    * Other options:
        -- Aggregating data on the fly without writing to logs.
        -- Completely skip all the aggregation on the API gateway side and send info of video for further down processing.
    * Better serialize data into a compact binary format. 
        -- Save network I/O utilization if request rate is very high.
        -- All these consideration depend on what resources are available on the API Gateway Host: Memory, CPU, network or disk IO.
    * Aggregated data is then sent to a distributed messaging system (Apache Kafka, AWS Kinesis)
        -- Kafka splits message across several partitions, where each partition can be placed on a separate machine in a cluster.
        -- Default random partitioning will help to distribute messages uniformly across partitions.
    * Split data after distributed Message System:
        * Slow Path Option 1:
            -- Calculate a list of K heavy hitters precisely.
            -- Let MapReduce do the work
            -- Dump all data to the distributed file system, HDFS or object storage (S3)
            -- Run two jobs:
                * Frequency Count MapReduce Job
                * Top K MapReduce Job
            -- Cons: Slow

        * Slow Path Option 2:
            -- Data Partitioner:
                -- Read batches of events, parse them into individual events, 
                -- take each video identifier and send info to correspondent partition in another Kafka cluster.
                -- Hash Partitioning (e.g. video identifier + time window)
                -- Deal with hot partitions
                -- Each partition of Kafka / shard in Kinesis stores a subset of data.
                -- Kafka and Kinesis will take care of data replication.

            -- Partition Processor:
                -- Read data from each partition and aggregate it further
                -- Aggregate data in memory
                -- batch this information into files of the predefined size
                -- Send the files to the distributed file system
                -- Further processed by MapReduce jobs.
                -- May also send aggregate information to the Storage Service.

            * MapReduce Jobs:
                -- Input -> Split -> Map -> Shuffle and Sort -> Reduce -> Output -> split -> local top K list (mapper) -> global top K list (reducer)
                -- MapReduce data processing split into phases.
                -- Input of MapReduce Jobs:
                    -- Set of files split into independent chunks
                -- Split and processed by map tasks in parallel, in form of key/value pair
                -- Partitioner takes intermediate key/value pairs from mapper, and assign the partition.
                -- All values of each key are grouped together. All values of a single key go to the same reducer.
                -- Partitioned data is written to local disk for each mapper.
                -- Reducer takes files after shuffle and sort, sum up all the values for that key
                -- Output of the reduce task is written to the file system.

                -- Split the output file to mapper to calculate Local top K list 
                -- Reducer will takes the output of mapper to calculate global top K list.

        * Fast path:
            -- Calculate a list of K heavy hitters approximately
            -- Fast Processor, create count-min-sketch and aggregate data for a short period of time
            -- No memory issue, no need to partition data (Count min sketch has predefined size)
            -- Data replication is nice to have, but may not be strictly required.
                * Require to have if data is in memory, in case data is lost.
                * Otherwise, we cannot claim high availablity for a service, as data may be lost due to hardware failures.
                * Absense of replication greatly simplifies the system.
            -- Will have accurate results in slow path several hours later.
            -- Every several seconds Fast Processor flushes data to the Storage.

        * Choice of slow / fast path:
            -- Depends on the requirement and more specifically how soon top k heavy hitters should be identified.
            -- Choose a path that suits us the best.
            -- If we can tolerate approximate results, choose fast path over slow path.
            -- If accuracy is important and result should be calculated in a matter of minutes:
                -- Partition the data and aggregate in memory.
            -- If time is not an issue but we still need the accurate results, data is big
                -- Hadoop, MapReduce should be our choice.

        * Storage:
            * Storage Component is a service in front of a database. 
            * Stores the final top K list for some time interval ex: every 1 minutes or 5 minutes
            * SQL or NoSQL
            * Builds the final count-min sketch and stores a list of top K elements for a period of time.
            * Data replication is required.

        * Data Processing: (Stream processing and Data Aggregation)
            * Our data processing pipelinee gradually decreases request rate,
            * May be millions of users clicking videos in the current minute.
            * All requests go to the API gateway hosts.
            * Cluster may be big, may contain thousands of hosts.
            * Pre-aggregate data on each host -> help to decrease number of messages that go into Kafka.
            * Fast Aggregator clusters, much smaller size that API gate way, aggregate data further.
            * When we arrive to storage, we only deal with a small fraction of requests that landed initially on API gateway hosts.
        
    * Data Retrieval:
        * API Gateway:
            -- Expose topK operation
            -- Route data retrieval calls to storage service
            -- Ex:   
               ** Approximate 1:00 (Top K List), 1:01 (Top K List), 1:02 (Top K List)
               ** Accurate: (1-hour Top K List) (1-hour Top K List)

    * API Gateway:
        * Does lots of work
            -- Authentication, SSL termination, rate limiting, request routing, response caching
        * May not have enough capacity to run data aggragation process in the background.
        * A process that offloads log files from these hosts. 
        * Logs files sent to storage or separated cluster to process Log files further.
        * Run log parsing and aggregation process on that cluster.

    * How big is K?
        * several of thousand should be good.
        * K cannot be arbitrary large.
        * Larger number, tens of thousands may start causing performance degradation.
        * Remember network bandwidth and storage space


    * Lambda Architecture:
        -- An approach to build stream processing applications on top of MapReduce and stream processing engines
        -- Send events to batch system and a stream processing system in parallel.
        -- Stitch together the results from both systems at query time.
        -- In 2011, Nathan Marz, the creator of Apache Storm, wrote a famous article called "How to beat the CAP theorem", where he introduces the idea of Lambda Architecture.

        * Cons:
            - Complexity of the system
*/