/** 
 *  Design Search Typeahead:
 *     * Features:
 *         * Number of typeahead suggestions provided: 5
 *         * Account for spelling check
 *         * Criteria for choosing the 5 suggestions:
 *            * All suggestions have typed phrase/query as prefix, rank by frequency of suggestion
 *         * Result to be real time, meaning the results must come from recent true fact
 *         * Personalization with suggestions: (My interest/queries)
 *            * We don't support it for now
 *     * Estimation:
 *         *  Number of search queries per day:
 *              * Assume the scale of Google, we expect 2 to 4 billions queries per day. Let say 4 billions
 *              * Avg length of words: 5 characters, 5 words avg per query, so 25 characters
 *              * Num of query: 25 * 4 billions = 100GB
 *         *  Size of data we need to store:
 *              * Assume 15% search queries are new (around 500 millions), 25 total characters on avg per query
 *                500 millions * 25 chars = 12.5GB new data per day:
 *      * Design Goal:
 *         *  Latency, Consistency and Availability:
 *         *  Latency: We expect to have low latency, the search suggestions need to come out fast
 *         *  Consistency vs Availability:
 *              * Availability is important than consistency.
 *                   * Not really matter if two people use same queries and return different results.
 *                   * Glad to see eventually consistent
 *      * Skeleton of Design:
 *         *  Two essential parts of the system:
 *             1. Given a search query, return 5 most frequent search terms with the query as prefix
 *                 List<String> getTopSuggestions(String query)
 *             2. Given a search query, update its frequency.
 *                 void updateQueryFrequency(String query)
 *         *  Data structure:
 *              * We will use Trie/Prefix Tree. 
 *         *  Read Query:
 *              *  Client (mobile app, browser etc) calls getTopSuggestions(String query)
 *              *  Client -> call API -> application -> getSuggestions from database -> database.
 *         *  Write Query:
 *              *  Client (mobile app, browser etc) calls getTopSuggestions(String query)
 *              *  Client -> call getTopSuggestions(String query) -> Application -> updateQueryFrequency(String query) -> Database
 *      * Deep Dive:
 *         *  Handle server failure:
 *              *  Have multiple servers, don't store any data: stateless, when one goes down, other server can up.
 *         *  Handle selecting server to talk to:
 *              *  We use load balancer in this case: it keep tracks of which server is down and forward the request to available server
 *                       * Requests are evenly distributed, because it used consistent hashing.
 *         *  Read query:
 *              *  Brute force: Search all subtrees of node n1 and save value and freq into Priority Queue, then return the first 5 search suggestions
 *              *  Better solution: store more data on each node of the Trie:
 *                   * Store the first 5 search suggestions on each node, and when application traverses to this node, we just return the results.
 *         *  Write query:
 *              *  Update frequency of a search term.
 *                   1. Traverse down to reach that search term and update its frequency
 *                   2. Traverse up, for each parent
 *                       Solution 1: Along with 5 seach suggestions nodes, we store their frequency. When a node's frequency is changed, we traverse up to its parents,
 *                           For every parent, we check if it is the top 5 search suggestions. If it is, we replace the corresponding frequency and update the freq.
 *                             If it is not, check if current query is high enough to be in top 5. If it is, we update it into top 5 list
 *                       Solution 2: On Every node, we store the top pointer to end node of these five most frequent queries. 
 *                             Update process: compare current node's frequence with the lowest node's frequency, and update the node pointer with current query pointer, if the new frequency is higher.
 *         *  Read query:
 *             *  Frequently writes will affect read efficiency:
 *                  * For write query, we need to have lock on each node to prevent inconsistency value
 *             *  Improve read efficiency:
 *                  * Use sampling: Instead of updating frequency of each word every time it appears, 
 *                        update the frequency of a single word if it appears in a specific time period (100ms) or query amount (100 queries)
 *                  * Offline update: have an offline hashmap to keep maintaining a map from query to freq.
 *                      Only when the freq over a threshold that we go and update the query in the trie with the new freq.
 *                      The hashmap is separated from datastore and would not collide with Trie reading.
 *             *  Have two Tries, one for updates and one for read?
 *                  * No, this will not work. It may be the case that a search query become popular and it was not reflected for an hour
 *                    Copy over trie cannot be atomic operations. We cannot make sure that reads are consistent while still processing writes.
 *             *  Data sharding:
 *                  * Total size of data: 5 years: 12.5GB per day * 365 * 5 = 22 TB
 *                  * Num of shards:
 *                     Solution 1: 26, first level of Trie. Not a good solution, => load imbalanced, because query starts with 'a' are more likely than 'x'
 *                     Solution 2: Keep traversing 2 letters prefix in order: a, aa, ab, ac.. and break when total load exceeds an threshold load
 *                                 and assign that range to a shard.
 *                                 Need a master which has this mapping with it, so that it can route prefix query to this shard.
 *         *  Handle Database when it goes down:
 *              *  Availability is more important than consistency. Maintain multiple replicas of each shard, when update goes to this shard, 
 *                 it will also goes to the replicas. If a replica goes down, read/writes will continue on other replicas and serve queries
 *              *  This issue occurs when replicas comes back:
 *                   Option 1: If freq of this replica going down is lower, or we have much higher number of replicas, the replicas which comes back up
 *                      read whole data from one of the older working replica, while keeping the new incoming writes in a queue
 *                   Option 2: There is a queue with every server containing changelog or exact write query being sent to them.
 *                             Replica can request changelog from other replicas since a particular timestamp, use them to update Trie.
 *                                                      
 *                   
*/