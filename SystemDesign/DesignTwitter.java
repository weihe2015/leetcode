/** 
 *   Design a Twitter like System
 *   * Features:
 *       *  We are looking at posting tweets, following people and favoriting tweets. A user should be able to see feed of tweets of his/her followers
 *       *  Support replies to tweets / grouping tweets by conversation?
 *          *  Don't need it for now
 *       *  Privacy control around each tweet?
 *          *  Not required for now. All tweets are public
 *       *  Trending tweets or support localization and personalization?
 *          *  Not for now
 *       *  Direct Messaging? 
 *          *  Not for now.
 *       *  Mentions/Tagging?
 *          *  Not for now.
 *       *  Notification System?
 *          *  Not for now
 *   * Estimation:
 *       *  Number of users and traffic:
 *             *  Twitter has around 500 millions tweets per day with 100 millions daily active users
 *       *  Number of followers of each user:
 *             *  Each user has average 200 followers, with certain hot users having millions of followers
 *       *  Number of times that a tweet is favorited:
 *             *  Assume each tweet is favorited twice. There are certain tweets which might be favorited by millions of people
 *       *  Assuming network of users, number of user to follower edge would exist:
 *             *  100 millions of daily active user, 200 followers, 100 Million * 200 = 20 Billion edges
 *   *  Design Goal:
 *       *  Consistency, Availability, and Latency.
 *       *  Latency:
 *           * Low Latency is needed
 *       *  Consistency vs availability:
 *           *  Availability >> consistency, because it does not matter if user misses a tweet or two
 *   *  Skeleton of Design:
 *       *  API Design:
 *           *  Major operations:
 *                *  Posting new tweets
 *                *  Following a user
 *                *  Favoriting a tweet
 *                *  Get the feed for user 
 *           *  Data we need with each tweet we fetch:
 *                *  Tweet content
 *                *  Tweet creator
 *                *  Timestamp when this tweet was created
 *                *  Number of favorites for this tweets  
 *           *  Would we need all user profiles of users who have favorited a tweet?
 *                *  Fetch top 2 people on the list, 
 *                *  We would show every tweet as 200 favorites which on hover shows Favorited by X, Y and 198 others
 *           *  Number of tweets we fetch at a time:
 *                *  Only a certain number of tweets will be in the viewport. A page of tweets. For a user, we want to fetch a page of tweets at a time
 *                *  Page size is different across different situations, like mobile app and web browsers
 *           *  Posting new Tweets:
 *               *  addTweet(userId, tweetContent, timestamp)
 *           *  Following a user:
 *               *  followUser(userId, toFollowUserId)
 *           *  Favorite a tweet:
 *               *  favoriteTweet(userId, tweetId)
 *           *  TweetResult getUserFeed(user, pageNumber, pageSize, lastUpdatedStamp)
 *              TweetResult = {
 *                  List<Tweet> tweets
 *                  boolean isDeltaUpdate
 *              }
 *              Tweet = {
 *                  userId,
 *                  content,
 *                  timestamp
 *                  numFavorites
 *                  sampleFavoriteNames
 *              }
 * 
 *      *  Write Query look like:
 *         *  Client (mobile app, Browser, etc) which calls addTweet(userId, tweetContent, timestamp) -> application -> database
 *      *  Read Query:
 *         *  Client (mobile app, Browser, etc) calls getUserFeed, -> application -> database
 *   *  Deep Dive:
 *      *  Application layer fault tolerance:
 *           *  Solution: Have multiple application server, which do not store and all of them behave the exact same way when up 
 *                          Have load balancer to track the set of application servers which are active
 *              Multiple application server machines along with load balancer is the way to go.
 *      *  Database layer:
 *           *  Data to store:
 *              *  For every tweet, we need to store content, timestamp and ownerID
 *              *  For every user, we need to store some personal information
 *              *  We need to store user1 -> user2 follower relations
 *              *  We need to store all userIds against a tweet of users who have favorited the tweet
 *           *  RDBMS vs NoSQL
 *              *  relations:
 *                 *  user -> followers
 *                 *  tweets -> favorited by users
 *              *  Size of data:
 *                 *  500 billions per day
 *                 *  Max size of tweet: 140 chars + 1 byte for timestamp + 1 byte for userId = 142 bytes
 *                 *  Provision for 5 years: 500 millions * 142 bytes * 365 * 5 = 129.5 TB
 *              *  Size of user:
 *                 *  Assume total of 1 billion users, each user has 200 followers, => 200 Billions connections.
 *                      => 200 billions * 2 bytes (size of 2 userId) => 400 GB
 *              *  Size of tweet to favorites relation:
 *                 *  Avg num of favorites per tweet: 2
 *                 *  Total num of tweets daily: 500 millions 
 *                 *  2 bytes for userIds, 1 bytes for tweetId
 *                 *  Provision for 5 years: 500 millions * (2 + 1) * 365 * 5 = 2.7TB
 *              *  Total size: 129.5 TB + 0.4 TB + 2.7TB = 132.6TB, cannot fit in one machine
 *       *  Important of technology maturity:
 *           *  SQL DBs like MySQL have been around for a long time and hence to be very stable.
 *           *  We will choose MySQL 
 *       *  Database Schema:
 *           *  User Table:   Id (primary),  username (unique), firstName, lastName, password, createdAt, lastUpdatedAt, DateOfBirth, description
 *           *  Tweets Table:  Id (primary),  content (content), dateOfCreated, userAuthorId
 *           *  Connection Table: userId, followedUserId, dateOfCreation(created at)
 *           *  Favorites Table: userId, tweetId, dateOfCreation
 *       *  Get feed of user:
 *           *  Require us to quickly lookup the userIds a user follows, get their top tweets and for each tweets, get users who have favorites the tweet
 *           *  Index:
 *              *  An index on followerId in connections to quickly lookup the userIds a user follows
 *              *  An index on userId, createdAt in tweets to get the top tweets for a user (where user_id = x sort by createdAt DESC)
 *              *  An index on tweetId in favorites
 *       *  Data sharding:
 *          *  Approach 1:
 *              *  users: part of table with userIds belong to the shard
 *              *  tweets: part of table with authorUserIds belong to shard
 *              *  connections: All entries where followerId belongs to current shard
 *              *  favorites: All entries where tweetId belongs to the tweets table in this shard
 *              *  Pros:
 *                   *  Equal load distribution
 *                   *  All write queries are simple and rely on just one shard
 *              *  Cons:
 *                   *  Getting the top tweet for each of these userIds would require different shards   
 *          *  Approach 2:
 *              *  Shard on recently
 *                 *  Most of the time we only work on the most recent tweets. New tweets are requested most frequently
 *                 *  Users: all users resides in one shard separately
 *                 *  tweets: This table is shard across shard by recency. When shard is full, we create a new shard and assign incoming new tweets to it
 *                 *  connections: All table reides in the shard with users
 *                 *  favorites: Stored with the tweets in their shard
 *                 *  Pros:
 *                     *  Feteching user feeds requires just query 2 shard (users and tweets, low latency)
 *                 *  Cons:
 *                     *  Load imbalance: The most recent tweet shard will be handling almost all of the traffic while the other shards will remain idle.
 *                     *  Huge maintenance overhead: Every time we need to create new shard, allocate new machines, setup replication and make things switch almost instantly so that no downtime is induced.
 *  
 *    
 *  
*/