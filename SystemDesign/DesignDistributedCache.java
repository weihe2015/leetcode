/**
Link: https://www.youtube.com/watch?v=iuqZvajTOyA


Stage 1. Clarification Questions: Be clear on what functional pieces of the problem we are going to focus on
                            What features of the system we need to design

    Problem Statement:
        web service backed by a data store. Client makes a call to web application, which in turns makes a call to the data store, there may be several issues with this step.
            Issue 1: Calls to data store may take a long time to execute / utilize lots of system resources.
            -- If datastore is down or slow, our web application may still process requests as usual, at least for some period of time.
        Client requests come, first check the cache and retrieve information from memory. If only if data is unavailable or stale, we then make a call to datastore.

    Functionality Requirement: (a set of operations system will support)
        -- put(key, value)
        -- get(key)

    Non-functionality Requirement:
        -- We want to design scalable, highly available and fast cache.
        -- High scalability (scales out easily together with increasing number of requests and data)
            * Ensure our cache can handle increased number of put and get requests.
        -- High Availablity (survives hardware/network failure)
            * Ensure that data in the cache is not lost during hardware failure. 
            * Minimize number of cache misses and a result number of calls to database.
        -- High Performance: (fast puts and fast gets) (Most important, No.1 priority)
            * Whole point of the cache is to be fast, as it is called on every request. 

        * Tips: think about the following 3 requirements first:
            -- Scalability, availablity and performance. If data persistent is important, then think of durability as well.
            -- Availability may be replaced with consistency.
            —- Provide as many positive data points as possible.
            -- Should start approaching any design problem with some small and simple steps. Evolve your solution with every next step. Demonstrate progress, ability to deal with ambiguity and simplify things.

    * Start with local cache:
        -- Later see how distributed cache design evolves from the solution for the local cache.
        -- Start with a single server and need to implement a basic in-memory data store.
        -- hashtable data structure
        -- Evict stale elements: (Replacement Policy)
            * LRU: Least recently used.
                -- HashMap + Double Linked List (Item at the head of list is most recently used and tail is the least recently used.)

    * Start making it distributed: (Partition data into shards, put each shard onto its own server)
        -- Dedicated cache cluster:
            -- Let each cache host has its own LRU cache to store only chunk of data, called shard.
            -- cache host is different from service host.
            -- Service hosts knows all the shards. Forward put/get request to particular shard.
            -- Pros:
                * Islation of cache resource from service resource. Both cache and service do not share memory and CPU. Can scale on their own.
                * Can be used by multiple services.
                * Utilize same cluster across several microservices our team owns.
                * Flexibility in choosing hardware. With lot of memory and high network bandwidth.
        -- Co-located cache:
            -- Let LRU cache lives in service host. Run cache as separate process on a service host. Data is split into shards.
            -- when service needs to make a put/get request to the cache, it picks the shards that stores data and makes a call.
            -- Pros:
                * No extra hardware and operation cost.
                * Both service and cache can bbe scale out at the same time.

    * How do cache client decide which cache shard to call?
        -- Naive solution: MOD function
            -- Cache Host Number = Hash(key) MOD (number of cache hosts)
            -- Cons:
                * Choose different cache host if a machine is died or new machines are added.
                * High percentage of cache miss.
        -- Consistent Hashing:
            -- Based on mapping each object to a point on a circle.
            -- For list of hosts, calculate a hash for each host based on a host identifier. (Ex: IP address or name)
            -- Assign a list of hash ranges each cache host owns. Each host will own all the cache item that live between this host and the nearest counter-clockwise neighbor.
            -- When find the cache host, calculate the hash of item, look clockwise for the cache host.
            -- Pros:
                * Minimize a number of keys we need to re-hash when new hosts are added or host is removed from cache cluster.

    * Who is responsible to run hash calculation and routing requests to the selected cache host?
        -- Cache clients know about all cache servers.
        -- All cache clients should have the same list of servers. Otherwise, different clients will have their own view of the consistent hashing circle. Same kye may be routed to different cache hosts.
        -- Client stores list of cache hosts in sorted order. Binary search can be used to find a cache server that owns the key. (logN time to find cache server) 
        -- Cache Client uses TCP or UDP protocol to talk to servers.
        -- If server is unavailable, client proceeds as though it was a cache miss. 

    * How list of cache servers is created, maintained and shard among cache client.
        -- Option 1:
            -- Store a list of cache hosts in a file and deploy this file to service using some continuous deployment pipeline.
            -- Use configuration management tool such as chef and puppet to deploy file to every service host.
            -- Cons:
                * Simplest option, but not flexible. Every time list changes, we need to make a code change and deploy it out to every service host.

        -- Option 2:
            -- Put the file on the share storage (Ex: S3 storage service and make service hosts poll for the file periodically.
            -- Introduce a daemon process that runs on each service host and poll data from storage every 1 min or etc.
            -- Cons:
                -- Still need to maintain the file manually.
                -- Cannot monitor cache server health and if something bad happens to cache server, all service hosts are notify and stop sending any requests to unavailable cache server.

        -- Option 3: Configuration Service (Ex: ZooKeeper)
            -- Purpose:
                * Discover cache hosts and monitor their health.
                * Each cache server registers itself with configuration service and sends heartbeats to it periodically.
                * Cache server is alive as long as configuration service receives heart beats.
            -- Cache client grap list of registered cache servers from configuration service.
            -- Cons:
                -- Hardest from implementation, require extra operational higher cost.
            -- Pros:
                -- Fully automate the list maintenance.

    * Hot data:
        -- Common problem for data sharding is that some of them may become hot, some shards process much more requests than their peers, result in bottle neck.
        -- Adding more cache server may not be very effective.

    * Archieve High Availablity:
        -- Data will lost if some shard dies or become unavailable due to network partition, all cache data is lost and results to cache miss, until keys are rehashed.
        -- Data replication:
            * Two catagories of data replication protocols:
                * Set of probabilisitic protocols 
                    * Ex: gossip, epidemic broadcast trees, bimodal multicast.
                    * Favor to eventual consistency
                * Consensus protocols 
                    * Ex: 2 or 3 phase commit, Paxos Raft
                    * Favor to strong consistency

            * Master slave replication.
                * Designate a master cache server and several read replicas.
                * Followers try to be an exact copy of the master.
                * Replicas live in different data centers, so that cache data is still available when one data center is down.
                * Put requests handled by master node
                * Get requests handled by both master and replicated nodes. 
                * How leader is elected:
                    * Implement leader election in the cache cluster.
                * Configuration service is responsible for monitoring of both leaders and followers and failover.
                    * If leader is died or become unavailable, configuration service can promote one follower to leader.
                * Configuration service is a distribbuted service by nature.
                    * Consists of odd number of nodes, (archieve quorum easier)
                    * Nodes located on machine that failed independently, --> configuration service remain available in case of network partitions and all nodes talk to each other using TCP protocol.
                * ZooKeeper / Redis Sentinel

            * Hot data:
                * Scale out by adding more read replicas

            * Data replication is asynchronously, so that we have a better performance.
                -- Don't want to wait until leaders server replicates data to all followers.
                -- Data is lost if leader server failed before this data was replicated by any of the followers.
                -- Acceptable behavior in many real-life use cases, when we deal with cache.
                -- Avaiablibilty >> Consistency. Needs to be fast.
                -- If data loses in some rare scenairos, not a big deal.
                -- Such cache miss is expected.

    * Consistency:
        -- Distributed Cache service favors performance and availablity over consistency.
        -- Data inconsistency:
            * Replicate data asynchorously. reads with same key may have different results.
            * Cache clients have different cache server lists, may write data to somewhere else that no other clients can read.
        -- Strong consistency will increase latency and overall complexity of the system
        -- Discuss the tradeoff of the system.

    * Data expiration:
        -- If cache is not full, some items may sit there for a long time, such item may become stale.
        -- Introduce time-to-live metadata for cache entry.
        -- Two common approaches how expired items are cleaned up from cache.
            * Passively expire an item:
                -- When some clients tries to access it, the item found to be expired.
            * Actively expire:
                -- Create a maintainance thread that runs at a regular period of time, remove expired items.
                -- Cannot iterate over all cache items, may have 1 Billion items in cache.
                -- Some probabilisitic algorithms are used, when several random items are tested with every run.

    * Local Cache:
        -- Construct local cache with cache client.
        -- Can use LRU cache or 3rd party implementations -- Guava cache.

    * Security:
        -- Optimized for maximum performance as well as simplicity, not optimized for security.
        -- Not expose to internet, host them in trusted environment.
        -- Firewall, ensure only approved cache clents can access the cache servers.
        -- Encrypt data before storing in cache, decrypt it on the way out.
            -- Watch out for performance implications.

    * Monitoring and Logging:
        -- Performance Degradation 
        -- Number of faults whiling calling the cache
        -- Latency
        -- Number of hits and misses
        -- CPU and memory utilization on cache hosts
        -- Network I/O
        -- May capture the details of every requests to the cache.
            -- Who and when acceess the cache, what was the key and return status code.

    * Cache client:
        -- Introduce proxy between cache clients and cache servers.
            -- Will be responsible for picking a cache shard.
            -- Ex: Twemproxy by twitter: https://blog.twitter.com/developer/en_us/a/2012/twemproxy.html
                -- Light-weight proxy for the memcached protocol.
                -- Purpose: reduce open connections to our cache servers
                -- Bound the number of connections on the cache servers by deploying it as local proxy on each of the front-ends
                -- Pros:
                    * Maintain persistent server connections
                    * Keep connection count on cache servers low
                    * Pipeline requests and response
                    * Use multiple server pools simultaneously
                    * Proxy the complete memcache ASCII protocol
                    * Easily configure server pools using a YAML file
                    * Use multiple hashing modes, including consistent hashing
                    * Increase observability using stats expose on a monitoring port

        -- Or make cache servers to pick up the shards:
            -- Cache clients sends requests to random cache server, cache server applies consistent hashing or other partitioning algorithm and redirects requests to the shard that stores data
            -- Utilized by Redis Cluster.

    * Consistent hashing:
        -- Two major flaws:
            -- Domino effect:
                -- It appears when cache server dies. All of the loads are transferred to next server.
                -- This loads may overload the next server, and then this server would fail, causing a chain reaction of failures.
            -- The fact that cache servers do not split the circle evenly
                -- Some servers may reside close to each other and some may be far apart, causing uneven distribution of keys among the cache servers.
                -- Solution: 
                    * add each server on the circle multiple times.
                    * Use Jump Hash Algorithm (Paper published by Google by 2014)
                    * Or use Proportional algorithm used by Yahoo Video Platform.
*/