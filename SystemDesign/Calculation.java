/**

    * Bytes:
        * Bytes 1 bytes
        * KB: 2^10 bytes =~ 1024 10^3 bytes
        * MB: 2^20 bytes =~ 10^6
        * GB: 2^30 bytes =~ 10^9
        * TB: 2^40 bytes =~ 10^12
        * PB: 2^50 bytes =~ 10^15

    * Disk
        * Capacity: 6 TB
        * Write Speed: 200 MB/s
        * (6 * 1024 * 1024 / (200 * 3600)) = 9 hours to fill up

    * Size of Facebook Social Network: 10 PB
    * Peek of QPS of TAO: 1 billions requests / s
    * MySQL: 40 millions I/O at peek
        * Single MySQL: QPS: 50K queries / s

*/