

Spring Cloud 2.3.4

Spring Build Docker Images:
    * /mvnw spring-boot:build-image
    * https://spring.io/blog/2020/01/27/creating-docker-images-with-spring-boot-2-3-0-m1

Spring.Actuator
    * Monitoring apps

Netflix Eureka Server:
    * Service register
    * spring.application.name=xx


Distributed Tracing:

    how message flow from one node to another

    * Zipkin Server:
        run as docker container
        * Transmit as each message flows through the system
        * Sending each hop in the journey of a message from one http request to another, asynchronously
        * Emitting
        * Assembing a call trace
        * Show all details of call trace,
            * http method
            * http path
            * mvc.controller.class
            * mvc.controller.method
            * Client address
    * Zipkin Client
        * Zipkin installation, and Spring Cloud Sleuth Zipkin
    * Spring Cloud Sleuth:
        * provides Spring Boot auto-configuration for distributed tracing
        * inspired by dapper, which is the distributed tracing tool at Google
        * To trace difference services
        * Tell Sleuth how often it should trace something, how often it should sample something
            * By default it is 0.1, 1/10 request will be sampled

Commands:
    * UAO: import zip project to intellij
        import project into idea command
        $ curl -L "https://gist.githubusercontent.com/chrisdarroch/7018927/raw/9a6d663fd7a52aa76a943fe8a9bc6091ad06b18d/idea" -o /usr/local/bin/idea
        $ chmod +x /usr/local/bin/idea

        unzip and open project command
        $ curl -L "https://gist.githubusercontent.com/sgyyz/adfa4f05af3d81cf0e17e19cf7044c85/raw/b6b9e871d5a4f5435a09d00b0a52e3db0b90699a/uao.sh" -o /usr/local/bin/uao.sh
        $ chmod +x /usr/local/bin/uao.sh
        $ ln -sf /usr/local/bin/uao.sh /usr/local/bin/uao