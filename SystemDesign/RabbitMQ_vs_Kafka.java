/**
https://www.youtube.com/watch?v=GMmRtSFQ5Z0
https://www.youtube.com/watch?v=7Faly8jORIw

    * RabbitMQ:
        * Publish/Subscribe platform, Request Response & Point-to-Point Patterns
        * General purpose message broker, based around message queues, designed with a smart broker / passive consumer model
        * Various language of client libraries.
        * Bindings:
            * Set of rules how messages should flow
        * Smart Broker Model
            * Route traffic across different queue
        * Can be synchronous / asynchronous
        * Push based approach
        * Prioritize Messages
            * Once message is consumed, it is gone.
        * Decoupled consumer queues
            * Can have two different events pushed to one queue based on consumer pattern
            * Not need to tell producer what events you consumes
            * Consumers independently bring their own queue that binds to exchanges
        * quorum queue:
            * Durable, replicated FIFO queue based on the Raft consensus algorithm. It is available as of 3.8.0
        * Acknowledgement:
            * Send ack to producer on message receive to ensure message delivered to broker.
            * If failed, message resend from producers
        * Consumer:
            * Maintain persistent TCP connection with RabbitMQ broker, declare the queue that they used.
            * Consumer sends back ack to broker to confirm consuming the message
            * Message will be removed once consumed.
            * Consumer overwhelm:
                * Set pre-fetch value, number of unacknowledge messages that consumers can have at any one time
        * Exchange:
            * Direct exchange:
                * when message routing key = queue binding key
            * Topic exchange:
                * wildcard matches between routing key and queue binding key
            * Fan out exchange:
                * all queues regardless of routing keys or pattern matching
        * High availbility:
            * Clustering:
                * Three or many brokers -> conceptual hole broker
                * message reside on one node but visible and reachable from all the nodes
        * CAP:
            * Availability + Partition Tolerance:
                * Auto heal partition handling strategy
                * Both side of partitions will continue functioning, but data is inconsistent
                * Smaller side of the partition will be reset after network failure is resolved
            * Consistency + Partition Tolerance:
                * Pause minority:
                * Smaller partition will stop read and write
        * When to use:
            * No event replay
            * No clear picture of End to End Architecture
            * Po producer changes for consumer additions
            * Language agnostic microservice architecture
            * Easy to scale bby adding/removing competing consumers on a single queue
            * Can be configured for consistency, high availability, low latency, high throughput
            * Support strict ordering
            * Cluster rolling upgrades via feature flags
            * Wide use cases: e.g: event driven microservices, RPC, ETL, enterprise message bus, pub-sub messaging, real-time analytics
        * Limitation:
            * Queues are single threaded
            * Complex with more brokers
                * Operational complexity in resolving network partitions
            * Scaling broker:
                * > 3 becomes complicated and can have negative performance impacts
            * No events replay
            * No native streaming support
            * Not natively support stateful streaming use cases

    * Kafka:
        * Publish/Subscribe streaming platform
        * Distributed & partitioned commit log with messaging semantics
        * Smart Consumer Model:
            * Leverage the consumer to decide what intellegent you want to make
        * Durable message store
            * Streaming log
        * Pull based approach
        * Order/Retain/Guarantee Messages
            * Reconsume the message from broker
            * Ordering guarentee only within one partition
        * Coupled consumer partition/groups
            * Consumers subscribes to the partitions, or filter out unnecessary events in the consumer
            * Get complicated when events from different topics consumes in same application
                * But it is simplier in RabbitMQ
        * Join:
            * Joining multiple streams or streams and tables to enrich the data
        * Log & Real time:
            * Sequential I/O as opposed to random
                * benefit for disk performance
            * Extremely smart utilization of OS page cache
                * achieve read and writes without disk IOPS in call path
            * Zero copy send files
                * Kernel copies the data directly from the disk file to the socket, without going through the application
        * Controller:
            * One of the brokers is nominated as a controller
            * Zookeeper maintains who is the controller and which broker is a leader of the partition
            * Feature:
                * Monitoring other brokers
                * Broker shutdown
                * Election of partition leaders
                * Tell brokers about partition leaders
        * Topic and Partition Basics:
            * Topic is divided in partitions, each partitions is essentially a log file
            * Partitions can have relicas for High Availablity
            * One of replicas is chosen as a Leader
            * All reads and writes happen only on leader
            * Publisher can only append to partition
            * Order guarantees only in a partition
        * When to use:
            * event replay
            * Streaming
            * High throughput
            * Massive scale
            * Ability to plug in n consumer groups on a topic
            * Support strict ordering within a partition
            * replace complex data architecture
            * Wider use cases: pub/sub messaging, events driven microservices, logs store, streaming, event sourcing, enterprice data pipelines.
        * Limitation
            * Operational complexity:
                * scale in production
            * Storage overheads (Storage cost)
                * Everything is log based.
                * Not only topics user defines for application requirement, also the topics that Kafka creates internally to manage its own state. Fault tolerant mechanism
                * Peak scenario
            * Streaming API for specific language (JVM only)
            * Ordering per partition
                * Create ordering across events or partition, leverage join, not directly do it out of box
            * Need producer coordination with consumer for partition increases
            * Careful coordination needed between teams writing consumer groups and producers
                * Topics in Kafka are partitions
                * Plan the partition accordingly
            * No out of bbox management & monitoring console
            * Streaming API support restrictly mainly to Java
            * No out of box solution for upgrade


*/