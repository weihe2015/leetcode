/**

Design the Instagram:

https://www.youtube.com/watch?v=VJpfO6KdyWE

    * Requirement:
        * Upload images from a mobile client (native app or mobile web)
        * Users follow other users
        * Generate a news feed of images (API to request the feed)
        * Scale: 10 million users
        * Concern about other constraints like network bandwidth, storage on a device
        * Expected latency of getting images
            * How often should they be updated
            * Include people you follow or some other explore future

    * Scale:
        * 10 million users monthly basis
        * 2 photos per months
        * Each photo 5 MB, include metadata, location, created time etc
        * 10 ^ 7 * 5 MB * 2 = 10^14 Byte ~= 100 TB per month
        * 1.2 PB per year
        * How much traffice we are going to have, what storage requirement I'm going to look like, what sort of system should we choose to support those
        * Different ways to store those type of data

    * API / Data model:
        * Data model: 
            * fundamental relationship type of data problem
            * Users to users in a many to many way, one user can follow many users
            * Photos to user, many to one relationship
            * Relational database, sql database, mysql database Postgres
            * Whether data is inherently relational, whether would benefit from doing relational queries
                * User table: 
                    * id, primary key int, serial
                    * name: string
                    * email address: string
                    * other attributes:
                * Photo table: 
                    * id, primary key int, serial
                    * user_id: foreign id, reference to user.id
                    * description: string
                    * metadata: string
                    * path/url: string, distributed file system, store, replicated image files
                * Followers table:
                    * user1: foreign key referencing user.id
                    * user2: foreign key referencing user.id 
        * Distributed file system:
            * Like S3, store and replicate our data in a reliable way
            * Separated reliable place to upload images and reference them will be accessible and fast

    * High level architecture:
        * Application:
            * More people reading their feed than people uploading photos
            * More read than write, do both operations efficiently
            * Read heavy system
            * replicates of database, to efficiently read, achieve higher scalability of the system
        * Split app server to read/write
            * May have different patterns or different requirements, different types of caching
        * Cache Redis: (Distribbuted Cache System)
            * return frequently accessed data
            * Write through / write back policy:
                * Evict strategy:
                * Write through cache：
                        write completes for both write to cache and write to database
                        write latency is higher
                * Write around cache
                        write directly goes to DB, cache system reads info from DB in case of a miss
                        ensure lower write load to cache and faster writes
                * Write back cache
                        write completes once write to cache,
                        write to database will happen async
                * update our cache at the same time

*/