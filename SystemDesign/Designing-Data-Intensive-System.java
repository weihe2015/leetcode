/**

    * Chapter 1: Reliaable, Scalable and Maintainable Application:
        * Components:
            * Database
            * Cache
            * Search index
            * Stream processing
                * Send a message to another process to be handled asynchronously
            * Batch processsing
                * Periodically crunch a large amount of accumulated data
        * Three focues in most software systems:
            * Reliability
                * Work correct in hardware or software faults
            * Saclability
                * deal with system grows: (data volumn, traffice volume, or complexity)
            * Maintainability
                * Be able to work on it productively

        * Describe load:
            * Ex: Twitter:
                * Post Tweet: A user can publish a new message to their followers (4.6k requests/sec on average, over 12k requests/sec at peak).
                * Home timeline: A user can view tweets posted by the people they follow (300k requests/sec).
                * 12000 writes avg
        * Describe Performance:
            * Average requests response time return in less than 200 ms

    * Chapter 2: Data Models and Query Language:
        * Normalization:
            * normalizing this data requires many-to-one relationships
        * NoSQL Document Database:
            * Joins are not needed for one-to-many tree structures, and support for joins is often weak
            * Use case: data in your application has a document-like structure, typically the entire tree is loaded at once
            * Limitation: 
                * cannot refer directly to a nested item within a document
                * poor support for joins in document databases
                * Does use many-to-many relationships, the document model becomes less appealing.
            * Schema:
                * There is an implicit schema, but it is not enforced by the database
                * schema-on-read
                    * The structure of the data is implicit, and only interpreted when the data is read
                    * dynamic (runtime) type checking
                * Many different types of objects, and it is not practical to put each type of object in its own table.
                * The structure of the data is determined by external systems over which you have no control and which may change at any time
            * Data Locality:
                * Advantage: need large parts of the document at the same time
            * Graph-Like Data Model:
                * Document model: 
                    * tree-structure data, one to many relationship
                    * No relationship between records
                * Graph document:
                    * Many to many relationship

    * Chapter 3: Storage and Retrieval:
        * Index:
            * An index is an additional structure that is derived from the primary data
            * Trade off: well-chosen indexes speed up read queries, but every index slows down writes
                * Require you—the application developer or database administrator—to choose indexes manually
        * B-Tree:
            * keep key-value pairs sorted by key, which allows efficient key- value lookups and range queries
            * break the database down into fixed-size blocks or pages, traditionally 4 KB in size, and read or write one page at a time
            * Resilient:
                * write-ahead log (WAL, redo log). Append only file.
                * Every B-tree modification must be written before it can be applied to the pages of the tree itself
            * Optimization:
                * Copy on write schema:
                    * A modified page is written to a different location, and a new version of the parent pages in the tree is created, pointing at the new location (Snapshot Isolation and Repeatable Read)
                * Lay out the tree so that leaf pages appear in sequential order on disk
                * Each leaf page may have references to its sibling pages to the left and right

    * Chapter 5: Replication
        * synchronous replication:
            * Advantage: follower is guaranteed to have an up-to-date copy of the data that is consistent with the leader
            * Disadvantage: if the synchronous follower doesn’t respond, the write cannot be processed. The leader must block all writes and wait until the synchronous replica is available again
            * Leader-based replication is configured to be completely asynchronous. In this case, if the leader fails and is not recoverable, any writes that have not yet been replicated to followers are lost
        * semi-synchronous:
            * one of the followers is synchronous, and the others are asynchronous.
            * If the synchronous follower becomes unavailable or slow, one of the asynchronous followers is made synchronous
        * Handling Node Outage:
            * Follower failure: Catch-up recovery:
                * On its local disk, each follower keeps a log of the data changes it has received from the leader
            * Leader failure: Failover:
                1. Determining that the leader has failed
                2. Choosing a new leader
                3. Reconfiguring the system to use the new leader
                * Trade off:
                    * asynchronous replication, new leader not received all writes from old leader.
                    * Solution: unreplicated writes to simply be discarded -> violate clients’ durability expectations
        * Implementation of Replication Logs
            * Statement-based replication
            * Write-ahead log (WAL) shipping (append-only sequence of bytes)
                * Used in PostgreSQL and Oracle
                * Disadvantage: log describes the data on a very low level, not possible to run different versions of the database software on the leader and the followers.
            * Logical (row-based) log replication
                * A logical log for a relational database is usually a sequence of records describing writes to database tables at the granularity of a row
                * A transaction that modifies several rows generates several such log records, followed by a record indicating that the transaction was committed. MySQL’s binlog uses this approach
            * Trigger-based replication
                * If you want to only replicate a subset of the data, or want to replicate from one kind of database to another, or if you need conflict resolution logic, then you may need to move replication up to the application layer.
                * Some tools, such as Oracle GoldenGate [19], can make data changes available to an application by reading the database log.
                * External process can then apply any necessary application logic and replicate the data change to another system. Databus for Oracle [20] and Bucardo for Postgres
                * More prone to bugs and limitations than the database’s built-in repli‐ cation. However, it can nevertheless be useful due to its flexibility
        * Problems with Replication Lag
            * Eventual Consistency
            * An application reads from an asynchronous follower, it may see out‐dated information if the follower has fallen behind
        * Reading Your Own Writes:
            * asynchronous replication:
                * read-after-write consistency
                * When reading something that the user may have modified, read it from the leader; otherwise, read it from a follower.
                * The client can remember the timestamp of its most recent write—then the sys‐ tem can ensure that the replica serving any reads for that user reflects updates at least until that timestamp
            * Another complication:
                * cross-device read-after-write consistency
        * Monotonic Reads:
            * An anomaly that can occur when reading from asynchronous followers is that it’s possible for a user to see things moving backward in time.
            * Make sure that each user always makes their reads from the same replica (different users can read from different replicas). For example, the replica can be chosen based on a hash of the user ID, rather than randomly.
        * Solutions for Replication Lag:
            * eventually consistent system
            * Database transactions
    
    * Multi-Leader Replication:
        * Multi-datacenter operation, have a leader in each datacenter.
            * Cons: Write-conflicts: the same data may be concurrently modified in two different datacenters, and those write conflicts must be resolved
        * Live Example:
            *  Clients with offline operation
            * Collaborative editing
        * Real Example:
            * CouchDB is designed for this mode of operation
        * Conflicts:
            * The simplest strategy for dealing with conflicts is to avoid them
        * Converging toward a consistent state:
            * single-leader: the last write determines the final value of the field.
            *  multi-leader: no defined ordering of writes
        * Solution:
            * last write wins
                * Generate a unique ID: (a timestamp, a long random number, a UUID, or a hash of the key and value), pick the write with the highest ID as the winner
            * Give each replica a unique ID, and let writes that originated at a higher- numbered replica always take precedence over writes that originated at a lower- numbered replica
            * Somehow merge the values together
            * Record the conflict in an explicit data structure that preserves all information, and write application code that resolves the conflict at some later time
    * Multi-Leader Replication Topologies
        * All-to-all: 
            * MySQL by default supports only a circular topology
            * To order these events correctly, a technique called version vectors can be used

    * Leaderless Replication:
        * Quorum Consistency
            * r and w as the minimum number of votes required for the read or write to be valid.
            * r and w are chosen to be a majority (more than n/2) of nodes
            * Dynamo-style databases are generally optimized for use cases that can tolerate eventual consistency

    
    * Chapter 6: Partitioning;
        * Term:
            * shard in MongoDB, Elasticsearch and SolrCloud
            * region in HBase
            * tabblet in Bigtable
            * vnode in Cassandra and Riask
            * vBucket in Couchbase
        * Main reason:
            * Scalability
        * Partitioning and Replication:
            * Each node may be the leader for some partitions and a follower for other partitions.
        * Partitioning of Key-Value Data
            * Goal: spread the data and the query load evenly across nodes
            * Skew: some partitions have more data or queries than others, partitioning is unfair
            * A partition with disproportionately high load is called a hot spot.
        * Partitioning by Key Range:
            * assign a continuous range of keys to each partition
                * Cons: The ranges of keys are not necessarily evenly spaced, because your data may not be evenly distributed.
        * Partitioning by Hash of Key:
            * hash function to determine the partition for a given key
            * uniformly distributed
            * assign each partition a range of hashes
            * every key whose hash falls within a partition’s range will be stored in that partition
            * Cons:
                * lose a nice property of key-range partitioning: the ability to do efficient range queries
                * Range queries on the primary key are not supported by Riak, Couchbase (NoSQL cloud database), or Voldemort
            * Range Search:
                * Cassandra:
                    * A table in Cassandra can be declared with a compound primary key consisting of several columns. 
                    * Only the first part of that key is hashed to determine the partition
                    * but the other columns are used as a concatenated index for sorting the data in Cassandra’s SSTables
        * Consistent Hashing:
            * Consisten: describes a particular approach to rebalancing
        * Skewed Workloads and Relieving Hot Spots:
            * Hot Spot: all requests being routed to the same partition, all reads and writes are for the same key, a celebrity user with millions of followers, 
            * Solution: application to reduce the skew
                * add a random number to the beginning or end of the key, 
                * split the writes to the key evenly across 100 different keys, allowing those keys to be distributed to different partitions.
                * it only makes sense to append the random number for the small number of hot keys
        * Partitioning and Secondary Indexes
            * Goal: A secondary index usually doesn’t identify a record uniquely but rather is a way of searching for occurrences of a particular value
            * two main approaches to partitioning a database with secondary indexes: 
                * document-based partitioning and term-based partitioning
        * Partitioning Secondary Indexes by Term:
            * Partitioning by the term itself can be useful for range scans
            * Partitioning on a hash of the term gives a more even distribution of load.
        * Dynamic Partitioning:
            * Reason: Reconfiguring the partition bound‐ aries manually would be very tedious.
            * key range–partitioned databases such as HBase and RethinkDB create partitions dynamically.
            * Pros: the number of partitions adapts to the total data volume
            * Cons: empty database, single partition:
                * Solution: HBase and MongoDB allow an initial set of partitions to be configured on an empty database, In the case of key-range partition‐ ing, pre-splitting requires that you already know what the key distribution is going to look like
        
        * Partitioning proportionally to nodes:
            * have a fixed number of partitions per node
            * Used by Cassandra and Ketama

        * Request Routing
            * service discovery
            * ZooKeeper:
                * Helix, relies on ZooKeeper
                * HBase, SolrCloud, and Kafka also use ZooKeeper to track partition assignment. 
                * MongoDB has a similar architecture, but it relies on its own config server implementation and mongos daemons as the routing tier.
            * gossip protocol:
                * Cassandra and Riak
                * Requests can be sent to any node, and that node forwards them to the appropriate node for the requested partition

    * Chapter 7: Transactions

    * Chapter 10: Batch Processing:
        * Use Case:
            * read a set of files as input and produce a new set of output files
            * Input is bounded
            * create search indexes, recom‐ mendation systems, analytics, and more
        * Services (online systems)
        * Batch processing systems (offline systems)
        * Stream processing systems (near-real-time systems)

        * Separation of logic and wiring
            * loose coupling, late binding, inversion of control 
            * Separating the input/output wiring from the program logic makes it easier to compose small tools into bigger systems.
        
        * MapReduce and Distributed Filesystems
            * MapReduce:
                * read and write files on a distributed filesystem (HDFS), Hadoop Distributed File System
                    * Ex: Amazon S3, Azure Blob Storage, and OpenStack Swift
                * A central server called the NameNode keeps track of which file blocks are stored on which machine

        * Where does it fit in?
            * closer to analytics
        * Handling skew (reduce-side joins)
            * celebrity, hot spots
            * Since a MapReduce job is only complete when all of its mappers and reducers have completed, any subsequent jobs must wait for the slowest reducer to complete before they can start
            * Solution:
                1. first runs a sampling job to determine which keys are hot (Pig). When performing the actual join, the mappers send any records relating to a hot key to one of several reducers, chosen at random
                    * Conventional: MapReduce, which chooses a reducer deterministically based on a hash of the key
                2. Shard join, using randomization to alleviate hot spots in a partitioned database. (Crunch)
                    * Add a random number to the beginning or end of the hash key
                3. Map-side join: 
                    * Requires hot keys to be specified explicitly in the table metadata, and it stores records related to those keys in separate files from the rest. 
                    * When performing a join on that table, it uses a mapside join for the hot keys
                    * (Hive skewed join optimization)
            * Two stage aggregation:
                1. First Stage: sends records to a random reducer, so that each reducer performs the grouping on a subset of records for the hot key and outputs a more compact aggregated value per key
                2. Second Stage: combines the values from all of the first-stage reducers into a single value per key.

            * Mapper-side join: (move aggregation / join to mapper)
                * Reducer-side join:
                    * Pros: not need to make any assumptions about the input data, mappers can prepare the data to be ready for joining
                    * Cons: all that sorting, copying to reducers, and merging of reducer inputs can be quite expensive
                * Mapper-side join Use case:
                    * a large dataset is joined with a small dataset
                    * Small dataset can be loaded into mapper's memory
                * Broadcast hash join:
                    * Each mapper for a partition of the large input reads the entirety of the small input
                * Read-only index:
                    * store the small join input in a read-only index on the local disk, The frequently used parts of this index will remain in the operating system’s page cache, so this approach can provide random-access lookups almost as fast as an in-memory hash table, but without actually requiring the dataset to fit in memory.
                * Partitioned hash joins:
                    * Partitioned based on the last decimal digit of the user ID
                    * Sufficient for each mapper to only read one partition from each of the input datasets
                    * Each mapper can load a smaller amount of data into its hash table
                    * Only works both of the join’s inputs have the same number of partitions, with records assigned to partitions based on the same key and the same hash function
            
            * Mapper-side merge join:
                * not only partitioned in the same way, but also sorted based on the same key

        * The Output of Batch Workflows
            * build indexes for its search engine:
                * querying a search index by keyword is a read-only operation, these index files are immutable once they have been created.

        * Full text search:
            * full-text search index such as Lucene
            * It is a file (the term dictionary) in which you can efficiently look up a particular keyword and find the list of all the document IDs con‐ taining that keyword 
            * Requires various additional data, in order to rank search results by relevance, correct misspellings, resolve synonyms
            * If you need to perform a full-text search over a fixed set of documents, then a batch process is a very effective way of building the indexes: the mappers partition the set of documents as needed, each reducer builds the index for its partition, and the index files are written to the distributed filesystem

        * Materialization of Intermediate State
            * Every MapReduce job is independent from every other job
            * The process of writing out this intermediate state to files is called materialization
            * Cons:
                * A MapReduce job can only start when all tasks in the preceding jobs have completed
                * Mappers are often redundant: they just read back the same file that was just writ‐ ten by a reducer, and prepare it for the next stage of partitioning and sorting
            * Data flow engines:
                * Spark, Flink
                * they handle an entire workflow as one job, rather than breaking it up into independent sub jobs.
            * Fault Tolerance:
                * Spark, Flink, and Tez avoid writing intermediate state to HDFS, so they take a differ‐ ent approach to tolerating faults: if a machine fails and the intermediate state on that machine is lost, it is recomputed from other data that is still available (a prior inter‐ mediary stage if possible, or otherwise the original input data, which is normally on HDFS)
                * To enable this recomputation, the framework must keep track of how a given piece of data was computed—which input partitions it used, and which operators were applied to it. Spark uses the resilient distributed dataset (RDD) abstraction for track‐ ing the ancestry of data, while Flink checkpoints operator state, allowing it to resume running an operator that ran into a fault during its execution 

    * Chapter 11: Stream Processing:
        * Transmitting Event Streams
            * Record: an immutable object containing details, encoded as text, JSON, or binary form
            * Group as topic or stream
        * Messaging System:
            * Publish/subscribe model
            * Q: if the producers send messages faster than the consumers can process them?
                * A: 3 options. 
                    * Drop message
                    * Buffer messages in a queue
                    * Apply backpressure (Flow control) i.e. blocking producer from sending more messages
            * Message brokers
                * Producers write messages to the broker, and consumers receive them by reading them from the broker
                * Some message brokers only keep messages in memory, while others (depending on configuration) write them to disk so that they are not lost in case of a broker crash.
            * Message brokers compared to databases
                * Most message brokers automatically delete a message when it has been successfully delivered to its consumers.
                * Overall throughput may degrade if broker needs to buffer a lot of messages.
                * Ex: RabbitMQ, Azure Service Bus, and Google Cloud Pub/Sub
            * Multiple consumers
                * Load balancing
                * Fan-out: Each message is delivered to all of the consumers.
                    * Fan-out allows several inde‐ pendent consumers to each “tune in” to the same broadcast of messages, without affecting each other
            * Acknowledgments and redelivery:
                * A client must explicitly tell the broker when it has finished processing a message so that the broker can remove it from the queue.
                * If the connection to a client is closed or times out without the broker receiving an acknowledgment, it assumes that the message was not processed, and therefore it delivers the message again to another consumer.
                * Ordering:
                    * To avoid this issue, you can use a separate queue per consumer
                    * Reordering is not a problem if messages are completely independent of each other
            * Partitioned Logs 
                * Using logs for message storage
                    * Log can be partitioned
                    * Within each partition, the broker assigns a monotonically increasing sequence num‐ ber, or offset, to every message 
                    * Such a sequence number makes sense because a partition is append-only, so the messages within a partition are totally ordered. There is no ordering guarantee across different partitions.
                    * If a consumer node fails, another node in the consumer group is assigned the failed consumer’s partitions, and it starts consuming messages at the last recorded offset.
                    * Apache Kafka, Amazon Kinesis Streams, and Twitter’s Distributed Log are log-based message brokers that work like this.
                * Disk space usage:
                    * The log implements a bounded-size buffer that dis‐ cards old messages when it gets full
            * Log Compaction:
                * The storage engine periodically looks for log records with the same key, throws away any duplicates, and keeps only the most recent update for each key
                * This compaction and merging process runs in the background.
        * Stream-stream join (window join)
            * To implement this type of join, a stream processor needs to maintain state: for exam‐ ple, all the events that occurred in the last hour, indexed by session ID.
            * Whenever a search event or click event occurs, it is added to the appropriate index, and the stream processor also checks the other index to see if another event for the same ses‐ sion ID has already arrived.
        * Table-table join (materialized view maintenance)
            * A timeline cache: a kind of per-user “inbox” to which tweets are written as they are sent
            * When user u sends a new tweet, it is added to the timeline of every user who is following u.
            * When a user deletes a tweet, it is removed from all users’ timelines.
            * When user u1 starts following user u2, recent tweets by u2 are added to u1’s timeline.
            * When user u1 unfollows user u2, tweets by u2 are removed from u1’s timeline.
            * To implement this cache maintenance in a stream processor, you need streams of events for tweets (sending and deleting) and for follow relationships (following and unfollowing). The stream process needs to maintain a database containing the set of followers for each user so that it knows which timelines need to be updated when a new tweet arrives
        * Fault Tolerance:
            * Microbatching and checkpointing
                * To break the stream into small blocks, and treat each block like a min-iature batch process. This approach is called microbatching, and it is used in Spark Streaming
                * A variant approach, used in Apache Flink, is to periodically generate rolling check‐ points of state and write them to durable storage. The checkpoints are triggered by barriers in the message stream, similar to the boundaries between microbatches, but without forcing a particular window size.
            * Atomic commit revisited:
                * context of distributed transactions and two-phase commit.
            * Idempotence
                * To discard the partial output of any failed tasks so that they can be safely retried without taking effect twice
                * When consuming messages from Kafka, every message has a persistent, monotonically increasing offset. When writing a value to an external database, you can include the offset of the message that triggered the last write with the value.
                * Restarting a failed task must replay the same messages in the same order (a log-based message broker does this), the process‐ ing must be deterministic, and no other node may concurrently update the same value.
            * Rebuilding state after a failure:
                * Any stream process that requires state—for example, any windowed aggregations (such as counters, averages, and histograms) and any tables and indexes used for joins—must ensure that this state can be recovered after a failure.
                * One option is to keep the state in a remote datastore and replicate it
                    * Cons: Query a remote database for each individual message can be slow
                * Keep state local to the stream processor, and replicate it periodically
                * When the stream processor is recovering from a failure, the new task can read the replicated state and resume processing without data loss.
                * Ex:
                    * Flink periodically captures snapshots of operator state and writes them to durable storage such as HDFS
                    * Samza and Kafka Streams replicate state changes by sending them to a dedicated Kafka topic with log compaction, similar to change data capture
                    * VoltDB replicates state by redundantly processing each input message on several nodes
                * In some cases, it may not even be necessary to replicate the state, because it can be rebuilt from the input streams.

*/