/**

1. Clarification Questions: Be clear on what functional pieces of the problem we are going to focus on
                            What features of the system we need to design
    1.1 Users/Customers: how the system will be used?
                         who will use the system?
                         Ex: Data is retrieved not often. Or data is retrieved from the system with a very high pace?
    1.2 Scale (read and write), how our system will handle a growing amount of data
    1.3 Performancee, how fast our system must be
    1.4 Cost: budget constraints
*/