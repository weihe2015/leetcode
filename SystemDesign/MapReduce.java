/**
Map Reduce (Hive) Phases:

1. Input
2. Input Splits:
    -- Input data divided into fixed sizee pieces, that can be consumed by single map.
3. Mapping, each word has (word, frequency) 
    -- Each split is passed to a mapping function to produce output values.
4. Shuffling: 
    -- Consolidate the relevant records from Mapping phase output.
    -- Same words are clubed together with their respective frequency.
5. Reducer:
    -- Output value from the shuffling phase are aggregated. Combines values from Shuffling phase and returns a single output value.

Data Skew: https://blog.csdn.net/WYpersist/article/details/79797075
    -- Happens on two sides:
        -- Map phase:
            -- too many keys with smaller size of data, causing uneven distribution in mapping
        -- Reduce phase:
            -- the data volumn of some output keys over size of other output keys.
    -- Two types
        -- Uneven Data distribution skew: The amount of data in some areas is much larger than other areas
        -- Data frequency skew: frequency: some data's frequency will be much larger than others.

    -- How to find the skew problem:
        -- Calculate the skewness value: Skewness for the normal distribution equals zero.

    -- Solution:
        -- Repartitioning:
            -- By default, all values for a particular key goes to same mapper, if some keys have overcrowded values quality, we can divide it into more than one mapper
            -- Custom partition strategy could be set
            -- Can be implemented between processing phases
        -- Modify key:
            -- Write a custom partitioner to redesign the key so that they data can be distributed more evenly, ex: change hash function
            -- Add extra layer of hash function on key.
        -- Add a combiner function in map:
            -- Cons: not effective when data skew keys are distributed in different mapper.
            -- Cons: not useful when in average calculation.
        -- Join:
            -- Perform Join operation in Mapping phase to avoid stuck on reduce phase.
        -- Count distinct:
            -- First group by, sum(), then count distinct.
        -- Preprocess data:
            -- Filter out some invalid data.
        -- Big table joins smaller table: (map join)
            -- let smaller table store in memory, and perform reduce in Mappping Phase
            -- 使用mapjoin的方式，mapjoin的原理是把小表存入每一个map节点的内存中，顺序扫描另一张表在map端完成join，避免因分发key不均匀而导致的数据倾斜
        -- Null key:
            -- add a random keys like UUID to empty key, distributed them to different reducer.

    -- 解决方案:
        1. 调优参数:
            * set hive.map.aggr=true
                -- 在map中会做部分聚集操作，效率更高但需要更多的内存。
            * set hive.groupby.skewindata=true
                -- 生成的查询计划会有两个MRJob。第一个MRJob 中，Map的输出结果集合会随机分布到Reduce中，每个Reduce做部分聚合操作，并输出结果，这样处理的结果是相同的GroupBy Key有可能被分发到不同的Reduce中，从而达到负载均衡的目的；第二个MRJob再根据预处理的数据结果按照GroupBy Key分布到Reduce中（这个过程可以保证相同的GroupBy Key被分布到同一个Reduce中），最后完成最终的聚合操作。
        2. 在 key 上面做文章，在 map 阶段将造成倾斜的key 先分成多组
            * 例如 aaa 这个 key,map 时随机在 aaa 后面加上 1,2,3,4 这四个数字之一，把 key 先分成四组，先进行一次运算，之后再恢复 key 进行最终运算。
        3. 能先进行 group 操作的时候先进行 group 操作，把 key 先进行一次 reduce,之后再进行 count 或者 distinct count 操作。
        4. join 操作中，使用 map join 在 map 端就先进行 join ，免得到reduce 时卡住。

        1. 增加reduce 的jvm内存
        2. 增加reduce 个数
        3. customer partition
        4. 其他优化的讨论.
        5. reduce sort merge排序算法的讨论
        6. 正在实现中的hive skewed join.
        7. pipeline
        8. distinct
        9. index 尤其是bitmap index

*/