/**
Link: https://www.youtube.com/watch?v=bUHFg8CZFws

Stage 1. Clarification Questions: Be clear on what functional pieces of the problem we are going to focus on
                            What features of the system we need to design
    1.1 Users/Customers:  who will use the system?
                            Q1. is this all youtube viewers who will see the total views count for a video?
                            Q2: Per hour statistics available to a video owner only?
                            Q3: Or total count will be used by some ML model to generate recommendations?
                          how the system will be used?
                            Q1: data is not retrieved often: Data may be used by marketing department to generate some monthly report?
                            Q2: Data is retrieve in high pace.

                          Ex: Data is retrieved not often. Or data is retrieved from the system with a very high pace?
    1.2 Scale (read and write), how our system will handle a growing amount of data,
                                how much data is coming to the system,
                                how much data is retrived from the system
                            Q1: How many read queries per second the system needs to process
                            Q2: How much data is queried per request?
                            Q3: How many video views per second are processed by the system?
                            Q4: Can there be spikes in traffic? Should we deal with sudden traffice spikes and how big they may be
                                Or we can assume some reasonable values.

    1.3 Performance, how fast our system must be
                            Q1. What is expected write-to-read data delay?
                                 Ex: count views serveral hours later,
                                     then batch processing and stream processing designs can be considered.
                                 Ex: But time matters, we need to count views in real time.
                            Q2: How fast data must be retrieved from the system?
                                 Ex: response time must be as small as possible, -> count views when we write data,
                                     and we should do minimal or no counting when we read data. Data must already be aggregated.

    1.4 Cost: budget constraints: Evaluate technology stack
                            Q1. Should the design minimize the cost of development?
                                Open source framework
                            Q2: Should the design minimize the cost of maintanencemance?
                                public cloud services

    Functional/Non-funcitonal requirements:
        Functional requirements: System behaviors: more APIs, this is what our system will do.
                                 - System Behavior, a set of operations that system will support.
        Non-functional requirements: System qualities: fast, fault-tolerant, secure.

    Write down Functional Requirements: API: (Write down on board)
        1. The system has to count video view events. API: countViewEvent(videoId)
           Broader set of events:                     API: countEvent(videoId, eventType) "view/like/share"
           Count/Sum/Average                          API: processEvent(videoId, eventType, function) "sum/average/count"
           Process events in single batchs:           API: processEvents(listOfEvents)

        2. The system has to return video views counts for a time period.
                                                       API: getViewsCount(videoId, startTime, endTime)
           Broader set of events:                      API: getCount(videoId, eventType, startTime, endTime)
           Statistics:                                 API: getStats(videoId, eventType, function, startTime, endTime)

    Non-functional requirements: big scale & as fast as possible. (Write down on board)
         Top requirments: scalability, performance and availability
         * Scalable: (tens of thousands of video views per seconds)
         * Highly performance: (few tens of milliseconds to return total views count for a video)
         * Highly Available: (survives hardware/network failures, not single point of failure), to be shown to users all the time.
         * Consistency
         * Durablility
         * Cost (hardware, developement, maintenance)

         CAP theorem: I should choose between Availability and Consistency. Which way should I go?


Stage 2. High-level architecture, (Interviewers may ask any componenet we outlines in the high-level architecture)
         1. Start with something simple
         webservices, database, browser, users
         Move forward one step at a time.

         1. Data Model. what data should we store and how we will do it.
            1.1 Individual Events (every clicks)
                 videoId, timeStamp, deviceType, OS, etc
                 Pros: Fast writes,
                       can slice and dice data however we need,
                       can recalculate numbers needed
                 Cons: Slow reads
                       Costly for a large scale, (many eventTypes) (Youtube generates billions of view events per day)

            1.2 Aggregate data (per minute) in real time
                 videoId, timeInterval/startTimeStamp, count
                 Pros: read is fast,
                       don't need to calculate individual event,
                       decision making in real time.
                 Cons: Can query only the way data was aggregated.
                       Filter or aggregate data different is very limited.
                       Implement aggregation pipeline, pre-aggregate data in memory before storing it in DB.
                       Hard or even impossible to fix errors.

            Need Interviewer to help us make a decision.

            Expected data delay: time between when event happened and when it as processed.
                               1. (Stream data processing). No more than serveral minutes, must aggregate data on the fly
                               2. (Batch data processing) Several hours is ok, store raw events and process them in background.

                    Combine both approach:
                        1. Store raw events, but pure old events that serveral days or weeks old
                        2. Calculate the stats in real time, will be available to user right away.

                        Fast read, ability to aggregate data differently, re-calculate stats if there is any error.
                        Trade off: the system becomes more complex and expensive.

                    Interviewer choose to focus on real time aggregation option:

        2. Where we store data?

           SQL and NoSQL databases can scale and perform well, evaluate both types.
               - Does it require ACID transaction?
               - Does it run complex dynamic queries?
               - Does it use this storage for analytics or data warehousing?
               Recall non-functional requirements
               Evaluate databases against these requirments.
                    Q:
                      - How to scale writes
                      - How to scale reads
                      - How to make both writes and read fast?
                      - How not to lose data in case hardware faults and network partitions
                      - How to achieve strong consistenncy? What are the tradeoffs?
                      - How to recover data in case of an outage
                      - How to ensure data security?
                      - How to make it extensible for data model changes in the future?
                      - Where to run (cloud vs on-premises data centers)
                      - How much money will it all cost?

               * Database solution should scale both reads and writes
               * Database solution should be fast and guarantee high availibility.
               * We should be able to achieve required level of data consistency.
               * We should understand how to recover data, achieve security, apply future data model changes.

               SQL:
                   One DB in a single machine, then split them to different machines.
                      Sharding or horizontal partitioning.
                      Each shard stores a subset of all the data
                      Have a cluster proxy that knows all database machines well. route traffic to the correct shard.
                         No need to known each and every DB machine anymore.
                         Client Proxy needs to know when some shard dies / becomes unavailable due to network failure.
                         Client Proxy needs to know when new shard has been added to the database.

                         Introduce configuration service: (Zookeeper),
                              maintain health check connnections to all shards.
                              It always knows what database machine is available

                      Add shard proxy in front of each DB machine:
                          * cache query results
                          * monitor database instance
                          * publish metrics
                          * terminate queries that takes too long to return

                      Address Availablity: replicate data:
                         * Each existing shard is the master shard or leader,
                           Reads can go to master or replica
                           Writes goes through master shard.

                         * Put replicas to a data center different from their master shard, in case datacenter is failed.
                         * Data can be replicated synchrously or asynchrously to a corresponding read replica.

                        MySQL:
                            * Efficient on I/O optimization when dealing with disk, tricks like innodb, change buffering, efficient for actual storage of data
                            * Built-in compression, reduce data size to reduce servers we need
                            * Transaction, mutation in single transaction
                            * Seconardary index
                            * Range search


                NoSQL: Apache Cassandra Database:
                       * In NoSQL, we split data into trunks, also known as nodes.
                       * No leaders/followers, each shard is equal.
                       * No need to have configuration service to monitor health of each shard.
                       * Allow shards to talk to each other and exchange information between them.
                       * To reduce network load, no need each shard to talk to every other shard.
                          * (Gossip Protocol)
                          * Ex: every second shard may exchange information with a few other shards, no more than 3.
                          * Every nodes knows about each other. No need cluster proxy to route requests to a particular shards.
                             * client can call any node in the cluster and node itself will decide where to forward this request.
                       * Use round robin to choose intial node to talk to.
                       * Or choose the node that is closest in terms of network distance.
                       * Use consistent hashing to pick which node it should store the data.
                       * (Quorum writes) Wait for all responses from replicas may be too slow. Consider write success when majority replicas have commit the changes.
                       * (Quorum reads): defines minimum num of nodes that have to agree on the response.
                                         * Use version number to determine staleness of data.
                       * High availablity: Store replicated data across multiple data centers to ensure high avilablity.


                We choose high availablity over consistency: eventual consistency.
                    * We prefer to show stale data than no data at all.
                    * Synchronously data replicationn is slow, we usually replicate data asynchronously.


        3. How we store data?
           * Relational database: start with defining nouns, convert nouns into table and use foreign keys to reference related data.
                * data is normalized, we minimize data duplication across different tables.
                * Normalization is good for relational databases.

                     Video                                   Views                           Channel

                     Video_Info                           Video_Stats                         ChannelInfo
            VideoId, Name, ... Channel ID         VideoId    Timestamps   Count      ChannelId   Name  ...


           * No SQL database: thinks in terms of queries will be executed in the system we design.
                -- Denormalization is perfectly normal.
                -- Store everything required for the report together.

                Video Id, Channel Name, Video Name, 15:00, 16:00, 17:00 ...   
                  A          xxxx         xxxx       2        3    4

              * 4 Types of NoSQL database:
                 - Column    Apache Cassandra / Amazonn DynamoDB 
                 - Document
                 - Key-value 
                 - graph

                Choose Apache Cassandra it is fault-tolerant, scalable,
                        both read/write throughput increases linearly as new machines are added.
                        Supports multi datacenter replications and works well with time-series data.
                        Wide column databases that supports asynchronous masterless replication.

                Need to know advantages and disadvantages of those and when to use what.
                     * Cassandra: Wide column databases that supports asynchronous masterless replication.
                     * MongoDB: a documented oriented database, use leader-based replication.
                     * HBase: column-orientated datastore, has master based architecture.

        4. Processing Services:
            * Start with requirements:
               - How to scale
               - How to achieve high throughput
               - How not to lose data when processinng node crashes, or when database becomes unavailable.
               - How to make data processing scalable, reliable and fast?
                    * Scalable = Partitioning
                    * Reliable = Replication and checkpointing
                    * Fast = In memory, minimize disks reads

            * Data aggregation basic:
               * Should we pre-aggregate data in the processing service?
               * Should we accumulate data in the processing service memory for some period of time?
                 let say a few seconds.
                 Add accumulated value to the database counter.
               * Choose to increment in memory counter and every serval seconds in-memory counter sends to database to calculate the final count.

            * Push vs Pull?
                * Push: some other service sends event synchrously to the processing service
                * Pull: processingn services pulls events from some temporary storage.
                    - Provides a better fault-tolerance support and easier to scale.
                    - Pull events from stroages. Events generated by users are stored in that temprorary storage first.
                    - Checkpointing: Queue: Events are consumes sequentially
                                     Event idx stores in persistent storage.
                                     Update idx only after all events have been processed.
                          Stream data processing.
                    - Partitioning: Instead of putting all events into a single queue,
                                    have several queues.


            * Processing Service detailed design:
               * Read events from partition queues, accumulate counters in memory and flush them to database periodically.

               * Partition consumer, single thread component,
                 - read events from event stream
                 - establishes and maintainns TCP connection with the partition to fetch data
                 - infinite loop that polls data from the partition
                 - deserializes the event after reading it. (Convert byte array to actual object)
                 - Multi-threaded access:
                    - Cons: checkpointing becomes more complicated
                    - Hard to preserve order of events if needed.
                 - eliminate duplicate events. Avoid double counting.
                    - Use distribubted cache that stores unique events identifiers for 10 mins

               * Aggregator: (In-Memory Counting)
                  -- like hash table. accumulate data for some period of time.
                  -- Periodically, stop writing to the current hash table and create a new one.

               * Internal Queue: used in multi-threading, processing takes time.
                                 decouple consumption and processing.

               * Database writer, take a message from internal queue and write to database.
                    * Option 1: Dead-letter Queue, 
                     -- A queue to which messages are sent if they cannot be routed to their correct destination.
                     -- protect ourselves from database performance or availibility issues.
                     -- Preserve data in case of downstream services degradation.

                    * Option2: Save in-memory data into persistent state storage. reloads its state into memory when started.

        ** Data Ingestion Path


        5. Ingestion path components:

           * Partitioner Service Client
                - Blocking v.s non-blocking IO
                    - non-Blocking:
                        - Single Thread to handle multiple concurrent connections
                        - Piling up requests in queue is far less expensive than piling up threads
                        - increase complexity of operations
                        - Blocking system is easy to debug

                - Bufferring and batching:
                    - Batching: put events into buffer, combine multiple events into single requests
                        - Pros:
                            - Increase throughput,
                            - Help to save on cost, request compression is more effective.
                        - Cons:
                            - Increase complexity

                - Timeout:
                    - Defines how much time a client is willing to wait for a response.
                    - Connection timeout
                        - How much time client to wait for establishing a connection.
                    - Request Timeout
                        - When request processing takes too much time.
                            - Latency percentiles, measure last 1% of the slowest requests in the system.
                        - Retry request with increased waiting time until the maximum backoff time, with add randomness

                    - Circuit Breaker pattern:
                        - Stop a client from repeatly trying to execute an operation.
                        - Stop request if error threshold is exceeded, but call downstream service
                        - Cons:
                            - Make system more complex to test.
                            - Hard to set proper error threshold and timers.
           
           * Load Balancer
                - Distributed data traffic between multiple servers.
                - Algorithm:
                    - Round Robin Algorithm
                        - Distributes requests in order across the list of servers.
                    - Less connections Algorithm:
                        - send requests to the server with the lowest number of active connections.
                    - Least Response time algorithm:
                        - Send requests to the server with the fastest response time.
                    - Hash-base algorithm:
                        - Distributed based on a key we defined

                    - DNS: domain name -> IP address
                - Availability:
                    - Primary and secondary nodes. Lives in differen data center, in case one data center goes down.

           * Partitioner Service and Partitions

    6. Tech Stack:
        - Client side:
            - Netty: high-performance non-blocking IO framework for developing network applications
            - Hystrix from Netflix
            - Polly:
                - Implementation of timeouts, retries, circuit breaker pattern
        
        - Load Balancing:
            - Netscaler: hardware load balancer.
            - NGINX: software load balancer
            - AWS ELS: elastic load balancer

        - Messaging Systems:
            - Apache Kafka
            - Amazon Kinesis

        - Data processing, stream processing framework:
            - Apache Spark or Flink
            - AWS Kinesis Data Analytics

        - Storage:
            - Apache Cassandra
            - Apache HBase database: storing time-series data.
                - Wide column databases.
            - Apache Hadoop: store raw event:
            - AWS Redshift: cloud data warehouse
            - AWS S3: roll up data and archive it

            - Vitess: database solution for scaling and managing large clusters of MySQL instances.
            - Redis:
            - RabbitMQ: message broker: temporarily queue undelivered messages.
            - AWS SQS: public cloud alternative.
            - RockDB: high performance embedded database for key-value pairs
            - Apache Zookeeper, distributed configuration service, leader election, manager service discovery.
            - Netflix Eureka: webservice for service discovery.
            - AWS cloudwatch: monitor solutions

        Knows and understand the tradeoffs.

    7. Identify bottle neck:
        - Test it under heavy load. 
            - Performance testing, load testing, measure behavior of a system under a specific expected load.
                - Indeed scalable, can handle the load we expected,
            - Stress testing: test beyond normal operational capacity.
                - Identify the breakinng component of the system. Memory, CPU, network, disk IO
            - Soak testing: test a system with a typical production laod for an extended period of time.
                - Find leaks in resources, memory leaks

    8. Monitor System health:
        - Metrics, dashboard, alerts,
            - Metrics: a variable that we measure, error count.
            - Dashboard: summary views of services core metrics. 
            - Alert, notification sent to service owners.
        - Golden signals of monitoring:
            - Latency, traffic, errors, saturation.
    
    9. How to make sure system produces accurate results:
        - Build an audit system
            - Weak audit system:
                - Continuously running end to end testing, once a minute, validate the returned result.
                - Not 100% reliable.
            - Strong audit system:
                - Store raw event in hadoop and use MapReduce to count events, compare results of both systems.


    Functional requirements (API) 
        - Write donnw verbs
        - Define input parameters and return values
        - Make several iterations.

    -> (Non-functional requiremments, qualities) 
        - Scalability
        - Availability
        - Performance
        ... 
        - Consistency
        - Maintainability
        - Cost
    
    -> (High-level design) 
        - How data gets into system
        - How data gets out
        - How data store, where data stores
        - What components to focus on next.
    
    -> (Detailed Design) 
        - Start with data
            - How data storage, transfer, process
            - Use fundamental concepts of system design.
        - Apply relevant technologies
    
    -> (Bottlenecks / Tradeoffs)
        - Listen to interviewers,
        - What the bottlenecks are
        - Know the tradeoffs