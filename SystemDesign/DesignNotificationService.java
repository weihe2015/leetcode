/**

    Design Notification Service
    https://www.youtube.com/watch?v=bBTPZ9NdSk8

    * Background:
        * Message need to be sent in a reaction to some event
            * Ex1: credit card transaction amount exceeded a limit
            * Ex2: service monitoring system encountered a large amount of faults produced by API 
        * A component called publisher which produces messages that need to be delivered to a group of other components called subscribers
            * Challenges:
                * Hard to scale when number of subscribers and message grows
                * Hard to extend supporting different types of subscribers.
            * Solution:
                * Introduce a new component called Notification Service
                    * Can register an arbitrary large number of publishers and subscribers
                    * Can corrdinates message delivery between them

    * Requirement:
        * Functional requirement:
            * Note: We want to define system behavior, or more specifically APIs, a set of operations the system will support
            * createTopic(topicName)
            * publish(topicName, message)
            * subscribe(topicName, endPoint)
                * Topic: a bucket that stores messages from a publisher, all subscribers receive a copy of a message from the bucket.

        * Non-functional requirement:
            * Note: System qualities, scalability, highly avaiable, durable, highly performant? maintainability,
            * Scalable:
                * Supports an arbitrarily larget number of topics, publishers and subscribers
            * Highly Available:
                * Survives hardware/network failures, no single point of failure
            * Highly Performant:
                * Keep end-to-end latency as low as possible
            * Durable:
                * Message must not be lost, each subscriber must receive every message at least once.

            * Security
            * Operational cost

    * High-Level Architecture:
        * Load Balancer:
            * Ensure requests are equally distributed among requests processing servers.
        * FrontEnd service:
            * Does initial request processing
            * A lightweight webservice
            * Stateless service deployed across several data centers.
            * Action: (See Design Distributed Message Queue.java)
                * Request validation
                * Authentication / Authorization
                * TLS (SSL) termination
                * Server-side encryption
                * Caching
                * Rate Limiting Throttling
                * Request dispatching
                * Request deduplication
                * Usage data collection
            * Host:
                * Reverse Proxy:
                    * First component to reach for request
                    * Lightweight server
                    * Responsible for:
                        * SSL termination
                            * HTTPS are decrypted, passed further in unencrypted form.
                            * Encrypted response while sending them back to client
                        * Compression:
                            * Compress response before returning them back to clients.
                            * Reduce the amount of bandwidth required for data transfer
                        * Response Front-End Service:
                            * Response with 503 status code if Front-End service is unavailable or slow
                * Local cache:
                    * Minimize the num of calls to metadata service
                    * Ex: Google Guava, ehcache
                * Write logs:
                    * Service Health
                    * Exception happened in the service
                    * Emit metrics
                        * Key-value data that may later aggregate and used to monitor service health and gather statistics
                        * Num of requests, faults calls latency
                        * Write info that may be used for audit
                            * Log who and when made requests to a specific API in the system
                    * Front-end Service produces logs
                    * Other component agent will process the logs
                        * Responsible for data aggregation, transferring logs to other system for post processing and storage.
                    * Separation of responsibilities helps FrontEnd Service simpler, faster and more robust

        * Metadata Database:
            * Store information about topics and subscriptions
            * Store all information when createTopic and subscribe APIs are called.
        * Metadata Service:
            * Hide database behind another microservice Metadata service
            * Design principle: 
                * Provide access to the database through interface
                * Simplify maintainance and ability to make changes in the future.
            * Caching layer:
                * Between database and other components like FrontEnd server
                * Don't want to hit database with every message published to the system
                * Want to retrieve topic metadata from cache
            * Distributed Cache
                * Many reads, little writes
                * Millions of topics, all these information cannot be loaded into a memory on a single host.
                * Information about topic is divided between hosts in a cluster.
                * Cluster represents a consistent hashing ring.
                * Each Frontend Host calculates a hash (MD5 hash) using some key (Ex: combination of topic name and topic owner ID).
                * Base on the hash value, Front-end Host picks a cooresponding Metadata service host.
            * Approaches of how Front-end Hosts know which Metadata service to call:
                * Option 1: Central Registry, Configuration Service
                    * responsible for coordination
                    * Knows all the Metadata service hosts, and hosts constantly sends heartbeat to it.
                    * Every time we add/remove metadata service hosts, Configuration Services aware of the changes and re-maps hash key ranges.
                * Option 2: Goosip Protocol:
                    * Every frontend hosts contains information about all Metadata services hosts.
                    * This protocol is based on the way that epidemics spreads
                    * Implement this type of protocol with a form of random peer selection
        * Temporary Storage:
            * Store messages for some period of time.
            * Period will be short if subscribers are available and messages were successfully sent to all of them.
            * Or period can be longer (several days), so that message can be retrieve later if subscribers is not available.
            * Sonner we can deliver messages to subscribers is better, unless topic is configured to deliver with some delay.
                * Several options can be considered, try to lead the conversation between interviewers and you
                * Datatabase:
                    * Pros and Cons of SQL vs NoSQL
                    * Evaluate different NoSQL database type
                    * SQL:
                        * No need ACID transaction
                        * No need to run complex dynamic queries
                        * No plan to use this storage for analytics or data warehousing
                    * Need a database that can be easily scaled for both writes and reads
                    * NoSQL:
                        * Highly available, and tolerate network partitions.
                        * Message has limited size, we do not actually need Document Store
                        * No specific relationship between messages, => no Graph NoSQL
                        * Column or key-value databases type
                            * Column database: Apache Cassandra / Amazonn DynamoDB / HBase
                * Distributed memory caching?
                    * Choose an in-memory store that supports persistence, so that messages can live for several days before being dropped.
                    * Redis
                * Message queues:
                    * Specific names: Apache Kafka, Amazon SQS
                * Stream-processing platforms.
                    * Discuss pros and cons and compare this option with a distributed queue solution
                    * Specific names: Apache Kafka, Amazon Kinesis
        * Sender:
            * Retrieve messages from the message store and send them to subscribers
            * Call Metadata service to retrieve information about subscribers.
            * Ideas on which Sender service is built upon, can easily be applied to other distributed systems
                * Solution that involves data retrieval, processing, and sending results in a fan-out manner.

            * Components:
                * Message Retriever:
                    * A pool of threads where each threads tries to read data from the Temporary Storage
                    * Naive approach:
                        * Start a predefined number of message retrieval threads
                        * Problem:
                            * Some threads may be idle, may not be enough message to retrieve
                            * Or another extreme, all threads may quickly become occupied, the only way message retrieval would be adding more Sender hosts.
                    * Better approach:
                        * Use counting semaphore to restrict the number of read message threads.
                            * Semaphore maintain a set of permits.
                            * Adjust a num of these permits dynamically, based on the existing and desired read rate.
                        * Keep track of idle threads, adjust num of message retrieval threads dynamically.
                        * If too many idle threads, no new threads should be created.
                        * If all threads are busy, more threads in the pool cann start reading messages.
                        * Helpes to better scale the Sender Service, but protect Temporary Storage service from being constantly bombarded by Sender Service
                        * Sender Service can lower the rate of messages reads to help Temporary Storage service to recover fast.
                * MS client:
                    * Not pass HTTP endpoints or list of email address along with message:
                        * Several thousands of HTTP endpoints
                        * A long list of email address
                    * Not all key-value and column storagees can storee big messages, may require us to use document database instead.
                * Spread message:
                    * Question:
                        * Should we iterate a list of subscribers and make remomte call to each one of them?
                        * What should we do if message delivery failed to onee subscriber from the list?
                        * What if one subscriber is slow and delivery to other subscribers is impact due to this?
                    * Solution:
                        * Split message delivery into tasks
                        * Each task is reesponsible for delivery to a single subscriber
                        * Can Deliver all message in parallel and isolate any bad subscriber
                    * Two components:
                        * Task Creator:
                            * Responsible for creating and scheduling a single message delivery task.
                            * Use counting semaphore to keep track of available sender threads in the pool
                        * Task Executor:
                            * If have enough threads to process new created tasks, submit all tasks for processing
                            * If we don't have enough threads available, postpone or stop message processing and return the message back to the Temporary Storage. Different sender host may pickup this message.
                            * Better handle slow Sender service host issues.
                            * Task may delegate actual delivery to other microservice
                            * Responsible for sending emails or SMS messages.

        * Other important topics:
            * How to make sure notification will not be sent to users as spam?
                * Register subscribers
                * All subscribers need to confirm they agree to get notification from our service
            * Handle duplicate message
                * FrontEnd Service will deduplicate submissions.
                * Avoid duplicates while accepting messages from publishers.
                * Duplicate message may happen in delivering messages to subscribers.
                * Subscribers also become responsible for avoiding duplicates.
            * Retry of the delivery attempt
                * Retry is one of the options to guarantee at least once message delivery
                * Maximal allowed number of retries have been reached.
                * Store such messages in a system that can be monitored by subscribers then decide what to do with undelivered messages.
                * Be great if our notification system provides a way for subscribers to define retry, what to do in cases when a message cannot be delivered after retries limit is reached.
            * Message order:
                * Does system guarantee any specific message order? First come first deliver?
                    * No, it does not guarantee any specific order.
                    * Even if messages are published with some attribute preserves the order, sequence ID or timestamps, delivery of message does not honor this.
                    * Delivery tasks can be executed in any order.
            * Security:
                * Make sure only authenticated publishers can publish messages, only registered subscribers can receive them.
                * Encryption using SSL over HTTP helps to protect messages in transit
                * Encrypt messages while storing them
            * Monitoring:
                * For end-to-end customer experience
                * Give customers ability to track state of their topic
                    * Number of messages waiting for delivery
                    * Number of messages failed to deliver

    * Overview:
        * Scalable?
            * Yes, Every component is horizontally scalable
        * High availablity?
            * Yes, no single point of failure, each componennt is deployed across several data center
        * Highly performant system?
            * Yes
        * Durable System?
            * Yes, whatever Temporary Storage solution we choose, data will be stored in the redundant manner, when several copies of message is stored across several machines
            * Retry message for a period of time to make sure they are delivered to every subscriber at least once.

*/