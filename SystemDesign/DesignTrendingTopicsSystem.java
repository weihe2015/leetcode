/**

    Video: https://www.youtube.com/watch?v=onYBQR7qnWE

    * Build a system to track trending topics on twitter

    * Q1: Passive system to read live tweets or historical tweets or other system submitting metadata to this system?
        * Ans: Already have a tweets system that posts tweets, written to a database, no other cross connection with any other service. Up to us how to ingest and close to real time as possible. 
        * What tweets are being tweeted, endpoint, provide relevant and up to date content 

    * Q2: Are there any requirement on latency of the service?
        * Real time, Within a minute or less
        * No night processing, No Batch processing
        * Like trending topics in the last hour

    * Q3: Content?
        * A topic than the real tweets?
        * Trending hashtag?
        * Topic? 

    * Keyword extraction as a separated service -> a bunch of keywords
    * Main datatype is string

    * Q4: Any number of volume of data tweets that we are receiving?
        * Scale of twitter
            * Millions of tweets per hour
        * Can start from simplify at the beginning, 
        * Get into how you would scale it at some point

    * Conditional trending based on demographic geolocation. (Data model)
        * Input: geo location and other metadata
        * Output: trending list

    * Data estimation:
        * Each tweet: 5 keywords 
        * Millions of tweets per hour = 5 million keywords
        * 1 keyword => 10 character = 50 million characters
        * 1 char => 8 bytes = 400 million bytes = 400 * 10 ^ 6 = 400MB per hour
        * 1 day => 400 MB * 24 = 9600 MB = 9.6GB per day

    * Q5: How long should we store such keywords?
        * Ans: cold storage
        * Something treading, it should be in primary store. (hot storage)

    * Q6: read/write radio?
        * Heavily write
        * Read from other systems, list of trends on page, one trending list per session
        * Sensibly protect the service by caching the results
            * trending list now is the same as trending list 10 mins ago
            * Periodically publish new trending lists

    * Doable on memory and storage concern

    * Location as aggregation datapoint
        * More aggregation can be feasible. (user metadata, geolocation)
        * Country, zip code, political perference
        * Aggregate tweets by sliding time and then geolocation

    * API:
        1. submitKeywords(keywords, Medatadata, timestamp)
        2. getTreading(AggregateFilter, timestamp)

    * Components:
        * Distributed system: (Consistency, Data replication)
            * Separated instances of the service based on the location
            * Support global trends?
                * Option 1: Aggregate the aggregated data:
                    * Query all other local system, get their localized trending topics, compare the frequency
                * Option 2: Query the raw keywords submitted to local systems, do aggregation itself
                * Option 3: local system has background process to submit these keywords to global system
            * Regionalized service, geo-distributed system

        * Load balancer in front of application to receive keywords and evenly distributed
        * Caching Server to read trending service

    * How would processing and aggregation look like?
        * ==> TopKHeavyHitter System
*/