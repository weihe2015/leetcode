/**
Redis:
    solve the problem of heavily read of MySQL.
    cache of database.

    key-value

    Data structure:
        * String
        * Hash
        * List
        * Set
        * SortedSet
        * Bitmap

    cache expired & cache eviction

    * Strategy:
        * non-eviction 返回错误，不会删除任何键值
        * allkeys - lru 使用LRU算法删除最近最少使用的键值
        * volatile - lru 使用LRU算法从设置了过期时间的键集合中删除最近最少使用的键值
        * all keys - random 从所有key随机删除
        * volatile - random 从设置了过期时间的键的集合中国呢随机删除
        * volatile - ttl 从设置了过期时间的键中删除时间最短的键
        * volatile - lfu 从设置了过期时间的键中删除使用频率最少的键
        * allkeys - lfu 从所有键中删除使用频率最少的键

    cache penetration: 缓存穿透
        查询数据库不存在的数据，cache 缓存和数据库都查询不了这个数据，但每次都会请求MySQL数据库

    Problem:
        Use a none existed ID to search, create lots of request and overstress the database.

    Solution:
        1. Set null to these keys to cache, return null directly and set expired time.
            For scenario where the number of keys of null data is limited, probability of key repeat request is high
        2. Bloom filter
            For scenario where keys of null data are different and probability of key repeat request is low.

    Cache avalanche: 缓存雪崩
        指在我们设置缓存时采用了相同的过期时间，导致缓存在某一时刻同时失效，请求全部转发到database，database瞬时压力过重雪崩

    Solution:
        * Use Redis Cluster to make Redis to be high available
        * In the progress:
            Use ehcache local cache + Hystrix limiting requests
        * origin expired time + random limit time. 我们可以在原有的失效时间基础上增加一个随机值，比如1-5分钟随机，这样每一个缓存的过期时间的重复率就会降低，就很难引发集体失效的事件。

    Cache breakdown: 缓存击穿 Hotspot data set is invalid: 对于一些热点的数据来说，当缓存失效以后会存在大量的请求过来，然后打到数据库去，从而可能导致数据库崩溃的情况。

    Solution:
        * Set different expired time
        * Use mutex lock: 简单地来说，就是在缓存失效的时候（判断拿出来的值为空），不是立即去load db，而是先使用缓存工具的某些带成功操作返回值的操作（比如Redis的SETNX或者Memcache的ADD）去set一个mutex key，当操作返回成功时，再进行load db的操作并回设缓存；否则，就重试整个get缓存的方法。

https://alibaba-cloud.medium.com/redis-vs-memcached-in-memory-data-storage-systems-3395279b0941

    Data persistent:
        RDB: Redis Database file: storage of the snapshot of the current data into a data file for persistence
            Redis utilizes the copy on write mechanism of the fork command. Upon creation of a snapshot, the current process forks a subprocess that makes all the data cyclic and writes them into the RDB file.
            Upon the generation of a new RDB file, the Redis-generated subprocess will first write the data into a temporary file, and then rename the temporary file into a RDB file through the atomic rename system call, so that the RDB file is always available whenever Redis suffers a fault.

        AOF: Append Only File, an appended log file. Every command for changing data will generate a log
            Redis provides another feature — AOF rewrite. The function of AOF rewrite is to re-generate an AOF file

        For general business requirements, we suggest you use RDB for persistence because the RDB overhead is much lower than that of AOF logs. For applications that cannot stand the risk of any data loss, we recommend you use AOF logs.
*/

public String get(key) {
    String value = redis.get(key);
    if (value == null) {
        //设置3min的超时，防止del操作失败的时候，下次缓存过期一直不能load db
        if (redis.setnx(key_mutex, 1, 3 * 60) == 1) {
            value = db.get(key);
            redis.set(key, value, expire_secs);
            redis.del(key_mutex);
        } else {
            //这个时候代表同时候的其他线程已经load db并回设到缓存了，这时候重试
            sleep(50);
            get(key);
        }
    }
    return value;
}

/**
    2. 提前使用互斥锁 (mutex key):
       Set a timeout field in value. 当从cache读取到timeout1发现它已经过期时候，马上延长timeout1并重新设置到cache。然后再从数据库加载数据并设置到cache中。
*/
public String get(key) {
    String v = memcache.get(key);
    if (v == null) {
        if (memcache.add(key_mutex, 3 * 60 * 1000)) {
            value = db.get(key);
            memcache.set(key, value);
            memcache.delete(key_mutex);
        } else {
            sleep(50);
            retry();
        }
    } else {
        if (v.timeout <= now()) {
            if (memcache.add(key_mutex, 3 * 60 * 1000)) {
                // extend the timeout for other threads
                v.timeout += 3 * 60 * 1000;
                memcache.set(key, v, KEY_TIMEOUT * 2);

                // load the latest value from db
                v = db.get(key);
                v.timeout = KEY_TIMEOUT;
                memcache.set(key, value, KEY_TIMEOUT * 2);
                memcache.delete(key_mutex);
            } else {
                sleep(50);
                retry();
            }
        }
    }
}