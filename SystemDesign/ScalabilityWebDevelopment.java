/**
https://youtu.be/-W9F__D3oY4

Horizontal scaling:
    Different from Vertical scaling, SSD, more RAM
    Many cheaper machines,

    DNS, round robin, url -> IP address, ser1, ser2, ser3...
    DNS server: BIND: use A record.
    problem: ser1, heavy load

    RAID:
        RAID0, RAID1, RAID5, RAID6, RAID10, assume multiple hard drives

        RAID0: two hard drives identical size, stripe, write data part1 to 1 disk, part2 to 2nd disk etc

        RAID1: write data to two disks at the same time.

        RAID10: 4 drives, both striping and write data to 2 disks at the same time

        RAID5: 3,4,5 drives, only 1 for redundency

        RAID6: any 2 disks can die

    read heavy vs write heavy

    active active load balancer:
        2 load balancer, send heart beats to each other. one die, other one can take the traffic and redict to servers
    active passive load balancer:
        2 load balancers, send heart beats to each other. one die, other one will take over its role.
*/

