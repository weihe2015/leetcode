/**
    * https://www.youtube.com/watch?v=DXTHb9TqJOs

    * RabbitMQ:
        * Push notification, stateful connection. (channel, stream)
        * Push result back to client immediately, eliminate the latency of waiting.
            * show progress bar etc to have better user experience.

    * Queue:
        * Undeterminisitic: not sure how long the job will be last for.

    * Request Response Pattern:
        * GET request over HTTP, asynchronous
        * Get back the content.
        * chain of waiting, if one thing is broken, whole thing will be broken.
        * Pros:
            * Elegent and simple.
            * Stateless (HTTP)
            * Scalable in receiver end.
        * Cons:
            * Bad for multiple receivers.
            * High coupling.
                * Want software to have social anxiety
                * Not want software to talk to each other.
            * Client/Server have to be running.
            * Chaining, circuit breaking.
                * Need timeout correctly.
                * Retry mechanism, make the system complex


    * Publish-Subscribe Pattern:
        * Message Queue, topic, channels.
        * Decouple services, use topic/channels
        * Pros:
            * Scales with multiple unique receivers
            * Great for microservices
            * Loose Coupling.
            * Change one place without breaking any other service.
            * Works while clients not running.
        * Cons:
            * Message delivery issues (Two general problem)
                * How to know message is published?
                * How to ensure content only published once?
                * Subscriber: How to know the message has been consumed or not.
            * Complex
                * Slow client to keep track of.
            * Pulling method:
                * Does the channel have the message?
            * Solution:
                * non-pulling, Kafka uses it, Make a request and block yourself there.
            * Network saturation.
                * Network can fail and lead to retry situation.
*/