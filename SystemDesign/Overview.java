/*
首先对系统面试一点概念都没有的小白去学习下这门课程： https://www.educative.io/courses/grokking-the-system-design-interview
这个是FAANG 和 Microsoft 的Hiring manager 写的一本系统设计教程。文章开门见山，先告诉你系统设计面试的套路，然后直接代入高频面试题，最后把常用概念梳理并且分析了每种技术的tradeoff。非常时候小白学习。如果不想付费去学，可以看这个GitHub：https://github.com/sharanyaa/grok_sdi_educative , 看pdf版本，比较原网页，里面会缺少一些图。不过大致相同，不影响学习。

在学习高频题过程中：可以结合Youtube视频经行学习。例如在学习How to design twitter时，除了看书上的内容，我还看了视频 https://www.youtube.com/watch?v=KmAyPUv9gOY , 可以看到书上说的很有用，可以应对面试，视频里指出了书上基于data sharding 的不足支持，提出了 fang out redis cache 的全新思路，这两种方法都有可以借鉴的地方， 除了相互照应帮助理解，还可以取其所长。可以通过这个例子学到更多的东西。

面试的套路：
1. 分析要求design的这个东西是什么（twitter是什么?）
2. 分析产品的功能（twitter 有user time line, 有home time line, 可以follow 其他人，可以点赞评论）
3. 分析产品的非功能需求（Reliability，Availability，Performance，Consistency）
4. 大致估算产品的使用数据（DAU, storage, bandwidth, read heavy or write heavy?）
5. 给出产品的大方向设计图  （在没有满足非功能需求，优化前的设计）
6. 给出API，参数和 response schema (增删改查)
7. 设计数据库 （ER 图）
8. 提出系统的潜在的问题和优化方案， 给出每种解决办法的trade off(Data sharding ? Multi layer Cache? Distributed Storage? Master - Slave pattern ?  Load balancing?)
7. 在面试管的允许下对其他非关键功能展开讨论（例如 twitter 里怎么search）

在学习方法上，请大家不要直接看答案，尽量按照套路先自己演练一遍，把高频题当作真题来做。在做出自己的分析后，再去看书上的答案和其他视频，这样可以了解自己疏漏的地方，学习自己没用想到的地方或者答案中考虑的跟好的地方，这样区别于死记硬背，就会长进很快。
*/