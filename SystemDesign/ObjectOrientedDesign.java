/**
SOLID:
    * S: Single Responsibility Principle
        -- Software Entities should be open for extension, but close for modification
    * O: Open Close Principle
        -- A class/metho should have a single responsibility, where a responsibility is nothing but a reason to change.
    * L: Likov Substitution Principle
        -- If S is a subtype of T, then objects of type T may be replaced with objects with type S without altering any of the desirable properties of the program.
    * I: Interface Segregation Principle:
        -- Many client specific interfaces are better than one general purpose interface.
        -- Client should not depends on the interfaces that they don't need
        -- Methods in interfaces should be less
    * D: Dependenncy Inversion Principle
        -- Depend upon Abstractions. Do not depend upon concretions
        -- Abstractions should not depend upon details. Details should depend upon abstraction
        -- High-level modules should not be depend on low level mmodules. Both should depend on abstractions.

https://www.oodesign.com/

Creation Patterns:
    * Singleton 
        - Ensure that only one instance of a class is created 
        - Provide a global access point to the object
        - Ex:
            -- Logger Class
            -- Configuration Class
            -- Accessing resources in shared mode
            -- Database Driver: for a given class, there can only be one instance
            -- List of all the configuration settings
            -- current state of app 

    * Factory:
        - Creates objects without exposing the instantiation logic to the client
        - Refers to the newly created object through a common interface
        - When:
            -- A framework delegate the creation of objects dervied from a common superclass to the factory.
    * Factory method:
        -- Defines an interface for creating objects, but let subclasses to decide which class to instantiate
        -- Refers to the newly created object through a common interface

    * Abstract Factory:
        -- Offers the interface for creating a family of related objects, without explicitly specifying their classes.
        -- Difference with Factory: 
            -- set of many (Abstract Factory) vs one product (Factory Method)
            -- Factory method depends on inheritance to decide which product to be created.
            -- Abstract Factory has separate class dedicate to create a family of related/dependent products

    * Builder:
        -- Defines an instance for creating an object but letting subclasses decide which class to instantiate and Allows a finer control over the construction process.
    
    * Prototype:
        -- Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.

    * Object Pool:
        -- reuses and shares objects that are expensive to create.
        -- When:
            -- we'll use an object pool whenever there are several clients who needs the same stateless resource which is expensive to create.
        -- Ex:
            -- Database Connections
            -- Remote Objects

Behavioral Design Pattern:
    * Chain of Responsiblity: 
        -- It avoids attaching the sender of a request to its receiver, giving this way other objects the possibility of handling the request too. The objects become parts of a chain and the request is sent from one object to another across the chain until one of the objects will handle it.

    * Command: 
        -- Encapsulate a request in an object, Allows the parameterization of clients with different requests and Allows saving the requests in a queue.

    * Interpreter: 
        -- Given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language / Map a domain to a language, the language to a grammar, and the grammar to a hierarchical object-oriented design

    * Iterator: 
        -- Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

    * Mediator: 
        -- Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.

    * Observer: 
        -- Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.
        -- Pub/Sub, Message Queue, 
        -- Pros: loose coupling between the publisher that is creating events, the subscribers are listening to
        -- Cons: Overboard, nasty event loops, hard to debug
            -- Use specific message bus

    * Strategy: 
        -- Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.
        -- Ex:
            customer selection strategy | nofitication strategy
        -- decent default strategy

    * Visitor:
        -- Represents an operation to be performed on the elements of an object structure / Visitor lets you define a new operation without changing the classes of the elements on which it operates.

    * Null Object:
        -- Provide an object as a surrogate for the lack of an object of a given type. / The Null Object Pattern provides intelligent do nothing behavior, hiding the details from its collaborators.

Structural Design Patterns:
    * Adapter:
        -- Convert the interface of a class into another interface clients expect. / Adapter lets classes work together, that could not otherwise because of incompatible interfaces.

    * Bridge/Adapter:
        -- Compose objects into tree structures to represent part-whole hierarchies. / Composite lets clients treat individual objects and compositions of objects uniformly.
        -- Indent:
            The intent of this pattern is to decouple abstraction from implementation so that the two can vary independently.

    * Facade:
        -- Front of building, hide all mechanics of the bbuilding inside the insulation.
        -- Compiler: Stream, Scanner, Token, Parser
        -- Cons: 
            -- Leaky Paradigm, leaky abstraction, oversimplified to use
            -- Over verticalization, too specific to single use case but not general enough

    * Facade/Composite: 
        -- Compose objects into tree structures to represent part-whole hierarchies. / Composite lets clients treat individual objects and compositions of objects uniformly.s

    * Decorator:
        -- add additional responsibilities dynamically to an object.

https://www.youtube.com/watch?v=FLmBqI3IKMA


*/