/**

    * Large-Scale Low-Latency Storage for the Social Network - Data@Scale
        * https://www.youtube.com/watch?v=5RfFhMwRAic
        * Paper: https://www.usenix.org/system/files/conference/atc13/atc13-bronson.pdf
        * Remote Region:
            * Front-end client send requests to TAO follower, which forwards requests to TAO Leader.
            * TAO leader will bundle several requests, compress requests into one, use RPC to call master TAO leader, which writes to local database.
            * Asynchronous MySQL replication
            * Pros:
                * Multiple requests in single rpc, hide latency across countries or regions because we are only doing once.
                * Cache consistency model
                    * Write through model (data is written to the cache and the database location at the same time)
                    * Data be in the cache until it successfully writes to the database.
                        * Pros: low read latency
                        * Cons: high write latency, write to two place every time
                        * Good for: write and then re-read data frequently
                            * Write-around: data is written only to the database without writing to the cache
                                * Pros:
                                    * not flooding the cache with data that may not subsequently be re-read.
                                * Cons:
                                    * Reading recently written data will result in a cache miss
                            * Write-back: data is written to the cache and async written to the database in the background
                                * Pros:
                                    * Low latency and high throughput for write-intensive applications.
                                * Cons:
                                    * There is data availability risk because the cache could fail, data could be lost
                                * Good for: mixed workloads as both read and write I/O have similar response time levels
                    * Front-end cluster can read the data immediately
                    * Read-after write consistency
                    * Sticky user to cluster
                    * Eventual cache consistency
        * Wormhole:   MySQL -> BinLog -> WormHole --> App
            * Publish/Subscribe System
                * Subscribe events from database stream
                    * Ex: to see when this association is updated.
                    * MySQL writes to binLog
                    * Wormhole tailes the binlog, send the updates to consumer applications
            * Consumers:
                * Cache Invalidation
                * Indexing:
                    * Graph Search
                    * Secondary index service
                * ETL to data warehouse
        * Conclusion:
            * Social graph data needs worldwide realtime access
            * Distributed caching can hide latency issues
                * But create consistency issue
            * TAO / MySQL / Wormhole is our solution
            * Each datacenter contains a full copy of the data of Facebook. No partition across regions.

    * Turning Caches into Distributed Systems with mcrouter - Data@Scale
        * https://www.youtube.com/watch?v=e9lTgFO-ZXw
        * Paper: https://www.usenix.org/system/files/conference/nsdi13/nsdi13-final170_update.pdf

        * Memcache:
            * Distributed Caching System
        * Memcached:
            * Standalone server software
            * LRU eviction
            * Network attached hash-map
            * Primary operations:
                * Get, Set, Delete
        * Usage Models:
            * Demand-filled, look-aside cache
                * Cache miss -> query to database -> get result from DB -> set result to Memcache
            * Non-durable store
            * Fast published store
                * Lookup regularly published values
                * Often replicated for redundancy
        * Use Case:
            * Cache results of complex RPC calls
                * Stored for period of time for reuse
                * Often using TTL expiration to enforce maximum staleness
                    * Memcache will expire this data after this TTL time.
            * Example:
                * Timeline aggregation
            * Query cache for expensive TAO/DB queries
                * Birthday index (materialized index of data that is expensive to fetch)
                    * wide fan-out query: once per day for each user when they log on the site
                * Typeahead bootstrap data
            * Replicated cache for extremely hot TAO/DB data
                * Chat visibility
                * Account confirmation status.
            * Write-heavy, non-durable storage:
                * For data that is nice-to-have, but not necessary for correct operation
                * Might bee a performance or quality optimization
                    * News Feed, 5 mins later, cache result, heavy calculation
                    * Light weight call like new-news feed in last 10 mins
            * Example:
                * Complex calculation analytics 
        * What we want:
            * Server never failes
            * Network never flaps
            * Limitless memory
            * offers infinite read throughput
            * All clients see uniformly low latency

        * mcrouter:
            * Feels as simple as a single memcached instance
            * Operates as a robust distributed system
                * Geo-distributed for low latency
                * Replicas scale reads
                * TB of memory
                * Failover
            * Chronicles scaling Memcache to
                * Thousands of instances
                * Dozens of clusters
                * Several regions
            * Issues encountered:
                * Growing working set
                    * Problem:
                        * Active data does not fit on one memcached server anymore
                    * Solution:
                        * Shard data across many memcached servers
                * High read rates
                    * Problem:
                        * One memcached server cannot handle read rate (15k packets per second)
                        * Only a small set of data is this popular
                        * Main limitation of box is packet per second
                        * Splitting across additional shards doesn't increase throughput
                            * This is known as the multiget hole
                    * Solution:
                        * Replicate the data and read from only one replica
                * Heterogeneous workloads
                    * Problem: 
                        * Keys compete for memory space
                        * LRU is not aware of expensive of miss
                        * Complex data can be evicted in favor or simple data
                        * More frequently requested simple data can dominate the cache
                    * Solution:
                        * Divide different workloads into pools
                        * Use prefix in the key
                        * Some of them can be replicated. Some of them can be sharded. Coexist in the same setup.
                * Hardware and transient network failures
                    * Problem: 
                        * Networks and server failed.
                        * In a large distributed system, something is always broken
                        * Clients are simpler if they don't worry about failure
                    * Solution:
                        * Failover requests to another location
                        * Used when timeout or server error message received.
                        * Replicated Pools:
                            * forward requests to another replica
                        * Sharded Pools
                            * Forward request to neighboring clusters, replicas of each other
                            * Multiple regions, each region has multiple clusters. Inside cluster, we have memcache pools, replicas of each other, next higher level.
                            * Go to local replica or replica of another clusters
                * Transient failures:
                    * Problem: 
                        * Server may come back online with stale data.
                    * Solution:
                        * Replay delete for consistency
                        * Encounter cache miss, let request go to DB for new data.
                * Inter-cluster inconsistencies
                    * Problem: 
                        * When data is changed, all copies of the key must be deleted.
                    * Solution:
                        * Broadcast deletes to all clusters
                        * Could be involved with multi mcrouters
                * Planned maintenance and datacenter issues
                    * Problem:
                        * Empty cache server
                        * New clusters & reset servers start with no data.
                    * Solution:
                        * Cold cache warmup
                        * Get from a cold pool -> miss
                        * Get from a warm pool on miss
                        * Add back to cold pool
            * Client and Server Responsibilties
                * Client:
                    * Read data from store on MC miss
                    * Delete proper keys on update of store
                    * Replicated pools
                    * Cold cache warmup
                    * Failover
                    * Replay
                * Server
                    * Cache data
                    * Be fast
            * With mcrouters:
                * Client:
                    * Read data from store on MC miss
                    * Delete proper keys on update of store
                * mcrouter:
                    * Replicated pools
                    * Cold cache warmup
                    * Failover
                    * Replay
                * Server
                    * Cache data
                    * Be fast
        * Interfaces:
            * Memcache ASCII protocol
                * Standalone for easy adoption
                * Anything that uses memcache can use mcrouter
            * Embeddable for latency-sensitive applications
                * Multi-threaded & Asynchronous
        * Scales:
            * Over 4 billion operations per second
            * Used for memcache and TAO workloads

    * Dynamo Paper https://www.youtube.com/watch?v=hMt9yFp0JKM
        * CAP:
            * Partition + (Latency or Consistency)
        * Hint handoff:
            * If A is temporarily not reachable during a WRITE operation, then the replica that would have lived on A will be sent to D to maintain the desired availability and durability guarantees. The replica sent to D will have a hint in its metadata which tells who the intended recipient was (in our case, it is A).
            * The hinted replicas are stored in a separate local database which is scanned periodically to detect if A has recovered. Upon detection, D will send the replica to A and may delete the object from its local store without decreasing the total number of replicas. Using hinted handoff, DynamoDB ensures that READ and WRITE are successful even during temporary node or network failures.
            * https://cloudacademy.com/blog/how-to-handle-failures-in-dynamodb-an-inside-look-into-nosql-part-6/
        * Cassandra:
            * Cannot reach consistency level unless the replicas are up.
            * Allow user to specify consistency level
        * Conflict:
            * Key value: replace the entire value
            * columnFamily: Replace individual columns
                * DynamoDB and Cassandra are more like columnFamily DB.
        * Alternatives to Last Write Win:
            * vector clock:
            * Immutable data modelling, common in Cassandra
            * Event sourcing

*/