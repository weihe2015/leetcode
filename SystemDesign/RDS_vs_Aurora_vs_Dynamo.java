/**
    https://www.youtube.com/watch?v=crHwekf0gTA
    * RDS:
        * Relational Database Service:
            * Simplifies using databases by handling hardware, provisioning, setup, backups, patching, storage, autoscaling and more.
            * Offers many database engines includign MySQL, Postgres, SQL Server, Oracle and MariaDB.
            * Very similar to running a MySQL Database on EC2
                * Handle for you like deploy to subnet, security group, patching, auto scaling, incremental version upgrade.

    * Aurora:
        * Database engine option optimized for the cloud.
            * specific database engine, own underlying database implementation.
            * not postgre, not mysql
        * Compatible with both MySQL and Postgres
            * Option to choose, use mysql compatibility or postgres
        * Innovation:
            * Innovative Shared Storage Architecture Decouples Storage from Compute
            * Decouple compute and storage into two machines -> much higher performance throughput.
        * Additional features:
            * 5X performance
            * More Read replicas with autoscaling
            * Higher availablility
            * Automatic backups (No performance Impact)
            * Serverless

    * Dynamo DB:
        * NoSQL database Engine
        * consistent performance
        * NoSQL -> optimized for key value lookups that does,
        * No aggregations and grouping, that is not something that dynamodb is well suited for.
        * Need to do grouping on application layer.
        * Great for high performance workloads with predicatable performance.
        * It can scale elastically to very large databases like terebytes of size.
        * Global tables, automatic backups.
        * Support transactions
            * All success or all failed.
            * Be aware of how many items you can update per transaction.

    * The Four Quadrants
        * Structures
        * Storage -> limitation of scalability, how application behave when the size of your data grows to very high levels
            * How application scale out when it requires both higher throughput and higher storage
        * Access
        * Scale

    * Aurora:
        * Structures: traditional SQL way
            * Table
                * primary key, foreign key, attributes
            * Relationship:
                * Build relationship between tables.
            * Schema:
                * Define configuration like the table schema, constraints on different column types.
        * Storage:
            * Decoupled Compute and Storage
            * partitioning data
            * have copies of data on independent machines
            * Multi AZ Replication
                * Asynchronously replicate that to different availability zone.
            * Distributed Storage System.
        * Scale:
            * How do we actually achieve high throughput
            * Horizontal scaling (Add more machines and split up the work amongst all those machines)
            * Cluster endpoint: -> master node for write and deletion
            * Reader Endpoint: -> one reader endpoint to direct traffic to any actual reader nodes. (like load balancer)
            * Simplfy the scaling complexity.
        * Access:
            * through traditional database connection + raw SQL.
            * Object Relational Mapper (ORM)
                * Object or Entity you interact with in programming language
                * Java -> Hibernate
            * Data API
                * Allows you write and submit queries using rest endpoint
                    * Write a SQL statement, feed it to an SDK which creates a REST request to an endpoint in Aurora and Aurora will run that SQL query against your database.

     * DynamoDB:
        * Structure:
            * Tables such as collections of items, items are the rows in DB.
            * Primary Key consists of two items: partition key and sort key
            * GSI: Global Secondary Index.
            * Raw attribute fields
        * Storage
            * Hashing function -> decide which partition the data is going to be located.
        * Scale
            * Essentially infinite scalling
            * Add more / Split Partitions
        * Access
            * AWS SDK
            * Language Specific ORM. (Java, DynamoDB mapper, abstract some of the complexity, an interface to perform a lot of very typical operations.)

    * Mental Model and Usage Problem Example.
        * RDS Aurora: Start with your Data Relationships
            * What is the problem set I am working on. Define a schema that respects that relationship
        * DynamoDB: Start with your access pattern.
            * Not how my data is structured, but how do I want to access my data.
            * Is it by specific key or range between dates
            * Different mental models when you are comparing these two things.

    * When to use what:
        * Aurora:
            * When you want to keep the door open for flexible access pattern
            * When you want to perform bulky, relational queries, like grouping and aggregations.
            * When you want to enforce schema constraints.
            * When you want to use a ubiquitous access language (SQL)

        * Dynamo:
            * When you have a predictable access pattern,
                * want to achieve high performance on.
            * Search key is known in advance.
                * Need to know what key you are looking for.
            * When you are willing to sacrifice flexibility for ultra fast and consistent performance.
*/