/**

    * Resiliency Patterns:
        * Bulkhead Pattern:
            * Links:
                * https://docs.microsoft.com/en-us/azure/architecture/patterns/bulkhead
                * https://www.youtube.com/watch?v=Kh3HxWk8YF4
            * A type of application design that is tolerant of failure
            * In this architecutre, elements of an application are isolated into pools so that if one fails, the others will continue to function
                * Separated thread pools
                * Separated limits for max threads
            * Partition service instances into different groups, based on consumer load and availability requirements.
                -- Isolate failures, allows to sustain service functionality for some consumers
            * Partition resources:
                -- Ensure resources used to call one service don't affect the resource used to call another services.
                -- Use different connection pool
            * Issues and consideration:
                -- Consider the level of isolation offered by the technology as well as the overhead in terms of cost, performance and manageability.
                -- Consider using processes, thread pools, and semaphores.
                -- Consider deploying them into separate virtual machines, containers, or processes
                -- Consider combining bulkheads with retry, circuit breaker, and throttling patterns to provide more sophisticated fault handling.
                -- Determine the level of granularity for the bulkheads
            * Spring Cloud Hystrix:
                -- @HystrixCommnad, threadPoolKey
                    * ThreadPoolProperties: @HystrixProperty(name = "coreSize", value=20)
                    * ThreadPoolProperties: @HystrixProperty(name = "maxQueueSize", value=10)


        * Circuit Breaker Pattern:
            * Prevent a client from continuing to call a service that is failing or experiencing performance issues.
            * Circuit breaker will cut the call, allowing it to fail fast.
            * A circuit breaker acts as a proxy for operations that might fail. 
            * The proxy should monitor the number of recent failures that have occurred, and use this information to decide whether to allow the operation to proceed, or simply return an exception immediately.
            * States:
                * Closed:
                    -- It's automatically reset at periodic intervals.
                * Open:
                * Half-Open
            * Allows the service to continue to operate, prevents the failure from cascading to other systems, and provides the failing service time to recover.
            * Provides stability while the system recovers from a failure and minimizes the impact on performance.

        * Client side Load Balancing Pattern:
            * Link: https://medium.com/capital-one-tech/resiliency-patterns-at-the-edge-capital-one-a5b4d41d477e
            * Compare with server side load balancing, easily to scales, handles update efficiently, elimiates single point of failure.
            * Discovery service
                * Ex: Netflix Eureke / HashiCorp Consul
                * Discovery server cache location and health status of service instance
            * Recovery:
                * Remove unhealthy instance
        


*/