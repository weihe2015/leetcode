## Quick Start:

#### Etcd:
- Key value store for the cluster. When an object is created, that object's state is stored here.

- It acts as the reference for the cluster state. If the cluster differs from what is indicates here, the cluster is changed to match.

#### Pods:
- Smallest unit of Kubernates and can be either a single container or a group of containers.
- All Container in a pod have the same IP address
- All pods in a cluster have unique IPs in the same IP space
- All containers can communicate with all other containers without NAT
- All nodes can communicate with all containers (and vice versa) without NAT

#### API Server:
- Front end for the kubernetes control plane. All API calls are sent to this server, and the server sends commands to the other services

#### Scheduler:
- When a new pod is created, the scheduler determines which node the pod will run on.
- This decision is based on many factors, including hardware, workloads, affinity etc.

#### Controller Manager:
- Node Controller:
  - Responsible for noticing and responding when nodes go down
- Replication Controller:
  - Responsible for maintaining the correct number of pods for every replication controller object in the system
- Endpoints Controller:
  - Populates the endpoints object (i.e. joins services and pods)
- Service Account & Token Controllers:
  - Create default accounts and API access tokens for new namespaces

#### Proxy:
- This runs on the nodes and provides network connectivity for services on the nodes that connect to the pods. Services must be defined via the API in order to configure the proxy.

#### Kubelet:
- This is the primary node agent that runs on each node. It uses a PodSpec, a provided object that monitor the pods on its node. The kubelet checks the state of its pods and ensures that they match the spec.

#### Container Runtime:
- This is the container manager. It can be any container runtime that is compliant with the Open Container Initiative (such as Docker). When Kubernates needs to instantiate a container inside a pod, it interfaces with the container runtime to build the correct type of container.


#### DNS:
- All services that are defined in the cluster get a DNS record. This is true for the DNS service as well.
- Pods search DNS relative to their own namespace
- The DNS server schedules a DNS pod on the cluster and configures the kubelets to set the containrs to use the cluster's DNS service
- PodSepc DNS policies determine the way that a container uses DNS. Options include Default, ClusterFirst or None.


#### Install Docker and Kubernetes on All Servers:
1. Once we logged in, we need to elevate privileges using `sudo`:
   1. `sudo su`
2. Disable SELinux:
   1. `setenforce 0`
   2. `sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux`
3. Enable the `br_netfilter` module for cluster communication:
   1. `modprobe br_netfilter`
   2. `echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables`
4. Ensure that the Docker dependencies are satisfied:
   1. `yum install -y yum-utils device-mapper-persistent-data lvm2`
5. Add the Docker repo and install Docker:
   1. `yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo`
   2. `yum install -y docker-ce`
6. Set the cgroup driver for Docker to systemd, reload systemd, then enable and start Docker:
   1. `sed -i '/^ExecStart/ s/$/ --exec-opt native.cgroupdriver=systemd/' /usr/lib/systemd/system/docker.service`
   2. `systemctl daemon-reload`
   3. `systemctl enable docker --now`
7. Add the Kubernetes repo:
```
cat << EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
  https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```
8. Install Kubernetes:
   1. `yum install -y kubelet kubeadm kubectl`
9. Enable the `kubelet` service. The `kubelet` service will fail to start until the cluster is initialized, this is expected:
   1. `systemctl enable kubelet`
10. (Master only) Initialize the cluster using the IP range for Flannel:
    1. kubeadm init --pod-network-cidr=10.244.0.0/16
11. (Master only) Copy the `kubeadmn join` command that is in the output. We will need this later.
12. (Master only) Exit `sudo`, copy the `admin.conf` to your home directory, and take ownership.
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
13. (Master only) Deploy Flannel:
    1. `kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml`
14. (Master only) Check the cluster state:
    1. `kubectl get pods --all-namespaces`
15. (Nodes only): Run the `join` command that you copied earlier, this requires running the command prefaced with sudo on the nodes (if we hadn't run `sudo su` to begin with). Then we'll check the nodes from the master.
    1. `kubectl get nodes`