/**
 * Design a cache like Redis/Memcached:
    * Features:
        * Save value in cache, fetch value from cache, cache miss -> fetch from database, cache hit -> return to user
        * Size: a few TB: 2^40 bytes
        * Evict strategy:
            * Write through cache：
                    write completes for both write to cache and write to database
                    write latency is higher
            * Write around cache
                    write directly goes to DB, cache system reads info from DB in case of a miss
                    ensure lower write load to cache and faster writes
            * Write back cache
                    write completes once write to cache,
                      write to database will happen async
        * Type of cache:
            * LRU cache: Least recently used cache
    * Estimate:
        * Total size: 30 TB, 30 * 2^40 bytes
        * QPS: Query per second: 10M QPS estimated
        * Num of machines: prod level cache machine: 72G/144G RAM per computer, 30TB / 72G ~= 420 machines 
        *                  If QPS is higher, we need more machines
    * Design Goal:
        * CAP Theorm: Consistency, Availability and Partition Tolerance
            * Consistency? No
            * Availability? Yes, always have cache be available
            * Partition Tolerance? Yes
        * Latency:
            * Latency is important
    * Deep dive into the parts that interviewer is interested in:
        * Single thread: HashMap.
            * Least Recently Cache: HashMap with Double Linked List, Tail of the list will be the least recently used item.
                      HashMap's key will be cache key, value will be DoubleLinkedListNode, 
                      the actual cached value will be stored in Node
        * Multiple threads:
            * ConcurrentHashMap
            * read & write lock, have lock on each single row, so read row i will not affect write row j if i != j
        * Sharding:
            * 10 M QPS, 420 machines => 23000 QPS per machine
            * For each computer, 72G RAM, shard
            * CPU time: assume 4 cores, 1 query => 1 second, 1 query => 4/23000 = 174 ns = 174 * 10^(-6) >> 0.5 ns for CPU L1 cache.
               https://gist.github.com/jboner/2841832 
            * What if shard among 16GB computers:
                30 TB = 30 * 1000 GB / 16GB = 1875 num of shards, 1 shard can have multiple computers, which has the same data
                total 10M query, one shard will be 10 M /1875 = 5000 QPS, 
            * Option 1: 30TB data, 72GB RAM, => 420 machines, => 10M / 420 => 23000 QPS, not good
            * Option 2: 30TB data, 16GB RAM，=> 1875 shards => 10M / 1875 => 5500 QPS, good
        * Shard failure:
               1. If 1 shard has multiple computers, read query can go to other computer in the same shard.
                     But it will increase the complications
               2. But for simplicity, we stick to 1 shard for 1 computer, if occasional high latency is not a big issue
               3. Multiple servers with same data:
                      May have case that data is not sync between servers
                      solutions: 
                            1. Master-slaves: when master is down, one of slaves will be selected to be master
                            2. For eventually consistency: one master for write, many slaves for read
    * Example:     Scale Memcache at Facebook
                https://pdos.csail.mit.edu/6.824/papers/memcache-fb.pdf


* 
 */


