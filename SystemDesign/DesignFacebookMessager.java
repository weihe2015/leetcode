/** 
 *  Design a Facebook Messenger:
 *      * Feature: 
 *          * Scale: 10 Billions of messages per day, around 300 Millions users
 *          * Support one to one conversation or group conversation?
 *             *  Assume only one to one conversion, we will extend group conversation later on
 *          * Support attachement?
 *             *  Not now, only plain text messaging system
 *          *  Reasonable size limit of a message:
 *             *  Max size of each message is 64KB, and reject others
 *          *  Notification System for new message received:
 *             *  Will discuss later if have time.
 *      * Estimation:
 *          *  Note: Important number for this section is the QPS and the data which system will be required to handle
 *          *  Amount of data per day:
 *               *  Assume avg size of each message is 160 chars, 10 Billions messages sent per day:
 *               *  10 Billions * 160 = 1600GB = 1.6 TB
 *          * Expected storage size:
 *               *  1.6TB per day, provision for 10 years: 1.6 TB * 365 * 10 = 6 Petabytes
 *      * Design Goal:
 *          *  Consistency, availability, and Latency
 *          *  Latency: Low Latency. 
 *          *  Consistency vs availability.
 *              * Consistency is important. 
 *              * availability is good to have. Consistency >> availability.
 *      * Skeleton of Design:
 *          *  Operations that need to support:
 *             *  Send message to another person
 *             *  For a user, fetch all most recently conversations
 *             *  For every conversation, fetch the most recently messages.
 *          * Send API:
 *             * sendMessages(senderId, recepientId, messageContent, clientMessageId, conversationId)
 *                *  generate a random timestamp based ID for message, whici can be use to de-duplicate same messages being sent repeatedly. 
 *                   Ordering of messages should be maintained.
 *                   due to delays, messages are sent out of order.
 *                   Record the timestamp the first time the requests hits the server
 *          * Fetch all user's latest conversation API:
 *             *  Purpose: Show a page of conversations/threads
 *             *  PageSize is not constant across different situations, it based on client's screen size and resolution
 *             *  Delta Fetch: Need to support fetching only the updates when the lastFetchedTimeStamp is closer to currentTimeStamp
 *             ConversationResult fetchConversation(userId, pageNumber, pageSize, lastUpdatedTimestamp)
 *             ConversationResult = {
 *                 List<Conversation> conversations
 *                 boolean isDeltaUpdate
 *             }
 *             Conversation = {
 *                ConversationId,
 *                participants,
 *                snippet
 *                lastUpdatedTimestamp,
 *                joinedTimestamp
 *             }
 *             
 *         *  Fetch most recently messages in a conversation API:
 *            MessageResult fetchMessages(conversationId, pageNumber, pageSize, lastUpdatedTimestamp)
 *            MessageResult {
 *              List<Message> messages;
 *              boolean isDeltaUpdate;
 *            } 
 *            Message {
 *              messageId,
 *              senderId,
 *              messageContent,
 *              sentTimestamp
 *            }
 *         *  Send Message:   
 *                 SendMessage(senderId, recepientId, messageContent, clientMessageId, conversationId)
 *                 clientMessageId: generate a random timestamp based ID for message.
 *         *  Write Query:
 *               Client(Mobile, web browser, etc), which calls sendMessage(senderId, recepientId, messageContent, clientMessageId, conversationId)
 *               Application server:
 *                     1. Interprets the API call
 *                     2. Put in the serverTimestamp
 *                     3. Figure out which conversation this message should be append to based on conversationId
 *                     4. Figure out the last recent message exists with the same clientMessageId
 *                     5. Store the message with conversation
 *        *  Read Query:
 *               Client(Mobile, web browser etc) which calls fetchConversation(userId, pageNumber, pageSize, lastUpdatedTimestamp)
 *               Application server:
 *                     1. Interprets the API call
 *                     2. Query the list of conversations from database
 *                     3. Return to client
 *     * Deep dive:
 *        *  Handle the case where application server dies:
 *             *  Have multiple application servers, which don't store any data, and all of them behave the exact same way when up.
 *        *  Which server should client talks to? 
 *             * Use Load balancer, set of machines tracks the set of application servers which are active or down.
 *        *  Multiple application server machines along with the load balancer
 *        *  Database Layer:
 *              *  We have relations as following:
 *                 *  user -> messages
 *                 *  user -> conversations
 *                 *  conversation -> messages
 *              * So we use RDBMS, SQL, NoSQL store everything in a denormalized fashion
 *        *  Amount of data we need to store:
 *           *   We provisioned that there is 6 Petabytes data in 10 years, not possible to store in one machine
 *           *   We need SQL with sharding
 *        *  Read-write pattern:
 *           *   Messaging is going to be write heavy. It writes once and consumed by other paticipant once
 *           *   For write heavy system with a lot of data, RDBMS usually don't perform well. 
 *                * Every write is not just an append to a table but also an update to multiple index, 
 *                * which might require locking and hence might interfere with reads and other writes
 *                * NoSQL DBs like HBase where writes are cheaper.
 *        *  How to store data? Data schema:
 *           *  Assume we are using HBase for this problem, 
 *           *  Major operations:
 *              *  For a user, append a message to a conversation
 *              *  Fetch timestamp ordered conversation for a user (Most recently)
 *              *  Fetch most recent messages in a conversation for a user (Most recently)
 *           *  Have userId as the row ID: Data is sharded based on users
 *           *  Recent conversations: WE store conversationId, timestamp mapping in the same row.
 *           *  Recent messages in a conversation: If key in the same index is conversationID_timpstamp, we can use prefix search of conversationID
 *                 and use the most recent messages based on timestamp in the key. (Assuming the data is stored stored with the key)
 *        *  Database sharding:
 *        *  Reliable and consistent database system:
 *        *  How to increase efficiency of the system?
 *            *  Caching: Users are assigned to exactly one server for caching. 
 *            *  To ensure consistency, all writes for that user should be directed through this server and 
 *            *  this server updates its cache when the writes is successful on DB before confirming success.
 * 
 * 
*/          