/** 
 * Design URL Shortener
       https://www.interviewbit.com/problems/design-url-shortener/
 *     * Features:
 *         * Shorten: Take an url and return a much shorter URL:
 *         * redirection: Take the shorted URL and redirect to original URL
 *         * Customize URL: Allow user to create custom shortened URL
 *         * Analytics: Usage statistics for site owner, how many people click the shorted URL last day, 3 days or last week
 *     * Estimation:
 *         *  Assume 100 million new URLs are added each months
 *         *  Assume average life time of shortened URL is 2 week and 20% website creates 80% traffic, we will receive around 1 billion queries per months.
 *               1 billion = 100 million * 2 * (1 + 4) 
 *         *  1 Billion per month, 30 days 24 hours 60 mins 60 s = 1 billion / 30 * 24 * 60 * 60 ~ 400 Query per second
 *            * 360 querys for read and 40 querys for write
 *         *  Number of URLs in next 5 years:
 *            100 million per month, 100 * 12 * 5 = 6 billion.
 *         *  Min length of shorted url to represent 6 billions URLs:
 *            [A-Z][a-z][0-9], 26+26+10 = 62, x^62 > 6*10^9, x ~= 6
 *            (1 char = 1 byte) 6 chars for shortened URL, 6 * 6 billions URL = 36 billions chars => 36 GB
 *            average length of long URL 500 characters , Maximum URL length is 2,083 characters
 *            500 * 6 billion = 3 TB original URLs
 *                 URL Length: Most webservers have a limit of 8192 bytes (8KB), The limit is in MSIE and Safari about 2KB, in Opera about 4KB and in Firefox about 8KB.
 *         *  Data read/write per second:
 *               read: 360 querys * (500 + 6) = 180000 = 180KB 
 *               write: 40 * (500 + 6) ~= 20000 = 20KB
 *      * Design Goal:
 *         *  Latency, consistency, and availability:
 *         *  Latency: Low Latency is important to us
 *         *  Consistency vs availability: CAP theorem, we can only choose one of them
 *              We choose consistency
 *         *  URL is shorter than its competitor
 *      * Skeleton:
 *         *  Define our APIs:
 *              1. shorteningAPI(String url) {store the original url in url_mapping and return hash(url)}
 *              2. redirectingAPI(String hash) { redirect user to url_mapping[hash] }
 *         *  Write Components:
 *               Client (Mobile app/Browser) call shorteningAPI, => application => store in Database
 *               Application return hash to client
 *         *  Read Components:
 *               Client (Mobile app/Browser) call redirectingAPI => application get original URL from database
 *               Application return URL and redirect user to that URL.
 *       * Deep dive:
 *            * The way to compute hash:
 *                * If user shortened the same URL, two separated shorted URL will be generated for analytics reason.
 *            * Application layer fault tolerance:
 *                * Have multiple servers, which don't store any data, and all of them behave the exact same way when up.
 *            * Load Balancer:
 *                * A set of machines which tracks the set of application servers which are active. Client can send request to any of the load balancers
 *                  who forward request to one of the working application randomly
 *            * Data to store:
 *                * Hash => URL mapping, no relationship between objects
 *                * The time that this shorten URL is created
 *            * RDBMS or NoSQL:
 *                * Are join required? If yes => RDBMS, If no, NoSQL. NoSQL is inefficient for joins
 *                  If size of data is small to fit in one computer, we choose to use SQL, great performance, fit in memory.
 *                * Current situation: 
 *                  100 millions new URLs per month, avg size of URL: 500 bytes, provisioning: 5 years.
 *                  100 million * 12 * 500 * 5 = 3 TB
 *                * Need sharding data:
 *            * Database schema:
 *                * Key -> hash, value -> original URL
 *            * Sharding data:
 *                * Look back to Sharing database: We will use consistent hashing for data distribing to servers
 *            * The way to handle failure:
 *                * NoSQL has single machine failure handling built-in, will have availability issue, for maintaining consistency.
 *            * Improve Write Query:
 *                * Use Bloom Filter to check if a hash already exists in database:
 *                    * Bloom Filter: It is a space-efficient probabilistic data structure to test whether an element is existed in the set. 
 *                      * It uses multiple hash function, and separated the key evenly in the set.
 *                      * There may be false positive results: the filter may tell you the value is existed in the set but actually it is not.
 *            * Reduce Read Queries:
 *                * Caching the read queries, LRU cache. Redis/Memcache
 *                  Redis vs Memcache: 
 *                       1. Both have great read/write speed.
 *                       2. Memory usage: Redis is better.
 *                            * Memcache: Specify the cache size and quickly grows a little more than this size, never really a way to reclaim any of that space
 *                            * Redis: Setting a max size. Redis will never use more than that, and will give you back the memory which is no longer using.
 *                            
 *                       3. Value size:
 *                            * Memcache: limited to 1MB, data will lost when restart memcache
 *                            * Redis: Up to 512 MB, persistent configuration, data can keep when restart Redis
 *                       4. Other:
 *                            * Redis: More than a cache, in memory data structure
 *           *  The way to shard data if you are working with SQL DB:
 *               * Consistent Hashing, don't need to rehash all data again when new server is added.
 *           *  The way to handle machine down in case of SQL DB:
 *               * For every shard, we need to have more than one server, Master - slaves
 *                  Master -> all write requests and update itself and slaves
 *                  Slaves -> all read requests. 
 *               
 * 
 *           
 *               
*/