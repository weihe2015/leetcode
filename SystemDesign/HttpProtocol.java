/**
https://www.youtube.com/watch?v=IvsANO0qZEg

Different tools for different jobs:
For every API project, there is goinng to be different requirements, different needs
Start with a very use case based style, try a few small use cases with a particular style and see if it works and solves your problem.
If it does, try expanding the use cases and see if it fits more use cases. If it doesn't, try something else.
The goal should always be understand what patterns fit my particular use cases best.

    * REST
        * Representational state transfer
        * Architecture Style
        * Multiple endpoints
        * Actions: https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
            * GET:
                -- Representation of the specified resource. Requests using GET should only retrieve data.
            * POST
                -- Submit an entity to the specified resource, often causing a change in state or side effects on the server.
            * PUT
                -- Replaces all current representations of the target resource with the request payload.
            * PATCH
                -- Apply partial modifications to a resource. 
            * TRACE:
                -- Performs a message loop-back test along the path to the target resource.
            * OPTION:
                -- Describe the communication options for the target resource.
        * Model resources:
            -- Instead modeling functions as endpoints, we model resources and collections of resources, relationships between resources.
            -- decoupling clients from servers.
            -- Fundamental unit: resources
            -- Intention of modeling resources and linkns between resources..
        * Pros:
            -- Decouple client and server
            -- API can evolve over time
            -- Reuses HTTP
                -- HTTP verbs
                -- HTTP caching semantics
                -- HTTP content negotiation
        * Cons:
            -- Overfetching/Underfetching data (Big payloads and chattiness)
            -- No single spec

        * Tradeoffs:
            -- Trade off some great abstraction and great modeling of your API as resources for the fact that it tends to be heavier on wire and chattier, depends on whether working on mobile or not.

        * Use Case: Management API:
            * Focus on objects or resources
            * Many varied clients
            * Discoverability and Documentation
            * Fits object model or resource model
            * JSON-API

    * GRPC: Remote Procedure Call
        * Call a function on another server
            -- Fundamental unit: function
            -- Take function already exists in the system.
            -- Put on HTTP
            -- Command oriented or action oriented, invoke function on the server
        * Cutting edge of RPC:
            -- Apache Thrift, gRPC, Twirp
            -- High performance low overhead messaging
            -- Don't want network overhead
            -- Lightweight payloads
        * Cons:
            -- High coupling to the underlying system
            -- Don't have abstraction between functions in the system and external API.
            -- Leak implementation details about system.
            -- Side effects to call particular endpoint
            -- Low discoverability (How do I know what to call in your API, how do I know how to start)
                -- Not generally a way to introspect the API or send a request to understand based on API requests.
            -- Function explosion:
                -- Hard to understand
                -- Counter:
                    -- Have a better abstraction between the underlying system and the API.
                    -- Decouple the system to have a stronger layer of abstraction
        * Tradeoffs: 
            -- High performance, Tight coupling, not great for a strong external API or an API service

        * Use Case:
            -- Action-oriented
            -- Simple interactions
            -- Slack Bot, command focused
                -- Join channel, leave a channel,
                -- RPC like style
            -- Internal microservices
                -- High message rate, 
                -- High message performance
                -- High network performance
                -- Don't want to spend lots of time transmitting a ton of metadata like REST api
            -- gRPC:
                -- HTTP 2 under the hood, optimize the network layer. make that as efficient as possible.
                -- REST could be the option if your gola is not as much high network performance, but stable API contract

        * Procedure:
            * Client calls stub function (Push params onto stack)
            * Stub parameters to a network message
            * OS sends a network message to the Server
            * Server OS receives message, sends it up to stub
            * Server stub unmarshals params, calls server function
            * Server function runs, returns a value
            * Server stub marshals the return value, sends msg
            * Server OS sends the reply back across the network
            * Client OS receives the reply and passes up to stub
            * Client stub unmarshals return value, returns to client

    * GraphQL:
        * Framework/Query Language
        * Fundamental unit: query
        * Everything is strongly typed.
        * Get everything in one call, spotty connection, instead of making tons of connections
        
        * Based on Query, data response can be changed.
        * Initial purpose: reduce load of querying so much data, provide performance improvement on data.
        * Single endpoint do different things
        * Only change is the query, just change the request information.
        * Action:
            * POST
        * Pros:
            -- Low network overhead
            -- Typed schema
            -- Fits graph-like data very well
        * Cons:
            -- Complexity
            -- Caching
            -- Versioning
            -- Sill early
        * No overfetching data:
            * Data response is based on query input
        * Server Side Filtering (Resolvers)
            * Filtering on the server side based on query input
        
        * Use Case:
            * Heavily used on mobile clients
            * Data is graphic-like
            * Optimize for high latency
            * Payload optimization


                       Coupling       Chattiness      Client Complexity     Cognitive Complexity     Caching    Discoverablity    Versioning
    RPC Functions       High             Medium            Low                    Low                 Custom         Bad             Hard
 
    Rest Resources      Low              High              Low                    Low                 HTTP           Good            Easy

    GraphQL             Medium           Low               High                   High                Custom         Good             ???
 

Learn More:

* Youtube: Les Hazlewood on REST API design
* Youtube: Eric Baer on GraphQL
* Phil Sturgeon's blog: https://philsturgeon.uk
* https://apisyouwonnthate.com


*/