/*
Given a binary tree, flatten it to a linked list in-place.

For example,
Given

         1
        / \
       2   5
      / \   \
     3   4   6
The flattened tree should look like:
   1
    \
     2
      \
       3
        \
         4
          \
           5
            \
             6
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {

    // recursive
    /**
     * Basic Idea, recursively goes to the right most node, mark currNode as prevNode
     *    After going back from left recursive call, mark currNode.left = null and currNode.right = prevNode

    先序遍历的顺序是 1 2 3 4 5 6。
    遍历到 2，把 1 的右指针指向 2。1 -> 2 3 4 5 6。
    遍历到 3，把 2 的右指针指向 3。1 -> 2 -> 3 4 5 6。
    原因就是我们把 1 的右指针指向 2，那么 1 的原本的右孩子就丢失了，也就是 5 就找不到了。

    解决方法的话，我们可以逆过来进行。
    我们依次遍历 6 5 4 3 2 1，然后每遍历一个节点就将当前节点的右指针更新为上一个节点。
    遍历到 5，把 5 的右指针指向 6。6 <- 5 4 3 2 1。
    遍历到 4，把 4 的右指针指向 5。6 <- 5 <- 4 3 2 1。
    */
    private TreeNode prev = null;
    public void flatten(TreeNode root) {
        helper(root);
    }

    private void helper(TreeNode node) {
        if (node == null) {
            return;
        }
        helper(node.right);
        helper(node.left);
        node.left = null;
        node.right = prev;
        prev = node;
    }

    /**
    
    1
   / \
  2   5
 / \   \
3   4   6

//将 1 的左子树插入到右子树的地方
    1
     \
      2         5
     / \         \
    3   4         6        
//将原来的右子树接到左子树的最右边节点
    1
     \
      2          
     / \          
    3   4  
         \
          5
           \
            6
            
 //将 2 的左子树插入到右子树的地方
    1
     \
      2          
       \          
        3       4  
                 \
                  5
                   \
                    6   
        
 //将原来的右子树接到左子树的最右边节点
    1
     \
      2          
       \          
        3      
         \
          4  
           \
            5
             \
              6 
    需要找出左子树最右边的节点以便把右子树接过来。
    */
    // in place
    public void flatten(TreeNode root) {
        while (root != null) {
            if (root.left != null) {
                // 找左子树最右边的节点
                TreeNode curr = root.left;
                while (curr.right != null) {
                    curr = curr.right;
                }
                //将原来的右子树接到左子树的最右边节点
                curr.right = root.right;
                // 将左子树插入到右子树的地方
                root.right = root.left;
                root.left = null;
            }
            //左子树为 null，直接考虑下一个节点
            root = root.right;
        }
    }

    // divide and conquer
   public void flatten(TreeNode root){
        if (root == null) {
            return;
        }
        helper(root);
    }
    public TreeNode helper(TreeNode root){
        if (root == null || (root.left == null && root.right == null)) {
            return root;
        }

        if (root.left == null) {
            return helper(root.right);
        }

        if (root.right == null) {
            root.right = root.left;
            root.left = null;
            return helper(root.right);
        }
        // Divide
        TreeNode leftlastNode = helper(root.left);
        TreeNode rightlastNode = helper(root.right);

        // Conquer
        leftlastNode.right = root.right;
        root.right = root.left;
        root.left = null;
        return rightlastNode;
    }
}