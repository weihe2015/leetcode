/**
Evaluate the value of an arithmetic expression in Reverse Polish Notation.

Valid operators are +, -, *, /. Each operand may be an integer or another expression.

Some examples:
  ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
  ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
*/ 
public class Solution {
    public int evalRPN(String[] tokens) {
        int res = 0;
        String[] opers = {"+","-","*","/"};
        List<String> operatorList = Arrays.asList(opers);
        Stack<Integer> stack = new Stack<Integer>();
        for (String token : tokens) {
            if (operatorList.contains(token)) {
                int num2 = stack.pop();
                int num1 = stack.pop();
                if (token.equals("+")) {
                    res = num1 + num2;
                }
                else if (token.equals("-")) {
                    res = num1 - num2;
                }
                else if (token.equals("*")) {
                    res = num1 * num2;
                }
                else if (token.equals("/")) {
                    res = num1 / num2;
                }
                stack.push(res);
            }
            else {
                stack.push(Integer.parseInt(token));
            }
        }
        return stack.pop();
    }
}