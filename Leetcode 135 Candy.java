/**
There are N children standing in a line. Each child is assigned a rating value.

You are giving candies to these children subjected to the following requirements:

Each child must have at least one candy.
Children with a higher rating get more candies than their neighbors.
What is the minimum candies you must give?

Example 1:

Input: [1,0,2]
Output: 5
Explanation: You can allocate to the first, second and third child with 2, 1, 2 candies respectively.
Example 2:

Input: [1,2,2]
Output: 4
Explanation: You can allocate to the first, second and third child with 1, 2, 1 candies respectively.
             The third child gets 1 candy because it satisfies the above two conditions.
*/
public class Solution {
    /** 1, 0, 2
       init: 1, 1, 1
       -> 1, 1, 2
       <- 2, 1, 2
    */
    /**
    1. 首先每个人分一个糖果
    2. 从左向右历遍，如果右边的小朋友的等级比左边高，那么右边的小朋友的糖果数加1
    3. 从右向左历遍，如果左边的小朋友的等级比右边高，那么左边的小朋友的糖果就是自己原来的，或者是右边的小朋友的糖果数加1
    4. 把所有小朋友的糖果数加起来就是答案。
    */
    public int candy(int[] ratings) {
        int n = ratings.length;
        int[] nums = new int[n];
        Arrays.fill(nums, 1);
        for (int i = 1; i < n; i++) {
            if (ratings[i-1] < ratings[i]) {
                nums[i] = nums[i-1] + 1; 
            }
        }
        for (int i = n-1; i > 0; i--) {
            if (ratings[i-1] > ratings[i]) {
                nums[i-1] = Math.max(nums[i-1], nums[i]+1);
            }
        }
        int count = 0;
        for (int num : nums) {
            count += num;
        }
        return count;
    }
}