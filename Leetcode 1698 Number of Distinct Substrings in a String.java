/**
Given a string s, return the number of distinct substrings of s.

A substring of a string is obtained by deleting any number of characters (possibly zero) from the front of the string and any number (possibly zero) from the back of the string.

Example 1:

Input: s = "aabbaba"
Output: 21
Explanation: The set of distinct strings is ["a","b","aa","bb","ab","ba","aab","abb","bba","aba","aabb","abba","bbab","baba","aabba","abbab","bbaba","aabbab","abbaba","aabbaba"]
Example 2:

Input: s = "abcdefg"
Output: 28

Constraints:

1 <= s.length <= 500
s consists of lowercase English letters.

*/
public class Solution {

    // Running Time Complexity: O(N^2), Space Complexity: O(N)
    public int numOfDistincSubstrings(String s) {
        Set<String> set = new HashSet<>();
        StringBuffer sb = new StringBuffer();
        int n = s.length();
        for (int i = 0; i < n; i++) {
            sb.setLength(0);
            for (int j = i; j < n; j++) {
                sb.append(s.charAt(j));
                set.add(sb.toString());
            }
        }
        return set.size();
    }
}