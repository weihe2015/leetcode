/**
Given a non-empty array of integers, return the k most frequent elements.

Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
Example 2:

Input: nums = [1], k = 1
Output: [1]
Note:

You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
*/

public class Solution {
    // Running Time Complexity: O(N + N * logN + K) = O(NlogN), Space Complexity: O(N)
    public List<Integer> topKFrequent(int[] nums, int k) {
        List<Integer> result = new ArrayList<Integer>();
        if (nums == null || nums.length == 0 || k <= 0) {
            return result;
        }
        // key -> value, value -> frequency
        Map<Integer, FrequencyItem> map = new HashMap<>();
        for (int num : nums) {
            if (!map.containsKey(num)) {
                map.put(num, new FrequencyItem(num, 1));
            }
            else {
                FrequencyItem freqItem = map.get(num);
                freqItem.freq++;
                map.put(num, freqItem);
            }
        }
        // Max Heap so that we have max frequent items on the top
        Queue<FrequencyItem> pq = new PriorityQueue<FrequencyItem>((f1, f2) -> f2.freq - f1.freq);
        for (int key : map.keySet()) {
            pq.offer(map.get(key));
        }
        
        while (k > 0) {
            FrequencyItem freqItem = pq.poll();
            result.add(freqItem.val);
            k--;
        }
        return result;
    }
    
    class FrequencyItem {
        int val;
        int freq;
        public FrequencyItem(int val, int freq) {
            this.val = val;
            this.freq = freq;
        } 
    }

    //O(N + (N-k)lg k) time, O(n) space
    public List<Integer> topKFrequent(int[] nums, int k) {
        List<Integer> result = new ArrayList<Integer>();
        if (nums == null || nums.length == 0 || k <= 0) {
            return result;
        }
        // key -> value, value -> frequency
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (!map.containsKey(num)) {
                map.put(num, 1);
            }
            else {
                int freq = map.get(num);
                freq++;
                map.put(num, freq);
            }
        }
        // Min Heap so that the least frequent one will be removed first.
        Queue<FrequencyItem> pq = new PriorityQueue<FrequencyItem>((f1, f2) -> f1.freq - f2.freq);
        for (int key : map.keySet()) {
            pq.offer(new FrequencyItem(key, map.get(key)));
            if (pq.size() > k) {
                pq.poll();
            }
        }
        
        while (!pq.isEmpty()) {
            FrequencyItem freqItem = pq.poll();
            result.add(freqItem.val);
        }
        return result;
    }

    // solution 2 use bucket sort O(n) time, O(n) space
    public List<Integer> topKFrequent(int[] nums, int k) {
        // Calculate the frequency for each key
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int num : nums) {
            if (!map.containsKey(num)) {
                map.put(num, 1);
            }
            else {
                int freq = map.get(num);
                freq++;
                map.put(num, freq);
            }
        }
        // Set a list of buckets with size of n+1, where n is the length of nums.
        // so that if one item has 5 freq, buckets[5] = [item]
        int n = nums.length;
        List<List<Integer>> buckets = new ArrayList<>();
        for (int i = 0; i <= n; i++) {
            buckets.add(new ArrayList<Integer>());
        }
        
        for (int key : map.keySet()) {
            int freq = map.get(key);
            buckets.get(freq).add(key);
        }
        
        List<Integer> result = new ArrayList<Integer>();
        for (int i = n; i >= 0; i--) {
            if (result.size() >= k) {
                break;
            }
            List<Integer> list = buckets.get(i);
            if (!list.isEmpty()) {
                for (int num : list) {
                    result.add(num);
                }
            }
        }
        return result;
    }

}