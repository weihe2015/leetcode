/**
Given an array of strings products and a string searchWord. We want to design a system that suggests at most three product names from products after each character of searchWord is typed. Suggested products should have common prefix with the searchWord. If there are more than three products with a common prefix return the three lexicographically minimums products.

Return list of lists of the suggested products after each character of searchWord is typed. 


Example 1:

Input: products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord = "mouse"
Output: [
["mobile","moneypot","monitor"],
["mobile","moneypot","monitor"],
["mouse","mousepad"],
["mouse","mousepad"],
["mouse","mousepad"]
]
Explanation: products sorted lexicographically = ["mobile","moneypot","monitor","mouse","mousepad"]
After typing m and mo all products match and we show user ["mobile","moneypot","monitor"]
After typing mou, mous and mouse the system suggests ["mouse","mousepad"]
Example 2:

Input: products = ["havana"], searchWord = "havana"
Output: [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
Example 3:

Input: products = ["bags","baggage","banner","box","cloths"], searchWord = "bags"
Output: [["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]
Example 4:

Input: products = ["havana"], searchWord = "tatiana"
Output: [[],[],[],[],[],[],[]]
*/

public class Solution {
    // https://leetcode-cn.com/problems/search-suggestions-system/solution/java-jian-dan-qing-xi-zi-dian-shu-by-fforkboat/
    class TrieNode {
        TrieNode[] children;
        List<String> words;
        public TrieNode() {
            this.children = new TrieNode[26];
            this.words = new ArrayList<>();
        }
    }
    
    class Trie {
        TrieNode root;
        
        public Trie() {
            this.root = new TrieNode();
        }
        
        public void insertWords(String[] words) {
            for (String word : words) {
                TrieNode curr = root;
                for (char c : word.toCharArray()) {
                    if (curr.children[c-'a'] == null) {
                        curr.children[c-'a'] = new TrieNode();
                    }
                    curr = curr.children[c-'a'];
                    if (curr.words.size() < 3) {
                        curr.words.add(word);
                    }
                }
            }
        }
        
        public void searchWord(String searchWord, List<List<String>> res) {
            TrieNode curr = root;
            for (char c : searchWord.toCharArray()) {
                if (curr == null || (curr = curr.children[c-'a']) == null) {
                    res.add(new ArrayList<>());
                }
                else {
                    res.add(curr.words);
                }
            }
        }
    }
    
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        // 先对products排序，这样一来，遍历products时先遇到的是字典序更小的product，就无需对每个节点的words列表进行排序了
        Arrays.sort(products);
        
        Trie trie = new Trie();
        trie.insertWords(products);
        
        List<List<String>> res = new ArrayList<>();
        trie.searchWord(searchWord, res);
        return res;
    }
}