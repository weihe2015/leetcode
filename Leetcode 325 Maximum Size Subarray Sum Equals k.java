/*
Given an array nums and a target value k, find the maximum length of a subarray that sums to k. If there isnt one, return 0 instead.

Example 1:
Given nums = [1, -1, 5, -2, 3], k = 3,
return 4. (because the subarray [1, -1, 5, -2] sums to 3 and is the longest)

Example 2:
Given nums = [-2, -1, 2, 1], k = 1,
return 2. (because the subarray [-1, 2] sums to 1 and is the longest)

Follow Up:
Can you do it in O(n) time?
*/
public class solution {
     // SUM_i - SUM_j = k, which means elements between i and j are the subarray whose sum is equals to k.
	 public int maxSubArrayLen(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int sum_i = 0, max = 0;
        // key -> sum from 0 .. i, value -> i:
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, n = nums.length; i < n; i++) {
            sum_i += nums[i];
            // SUM_i - SUM_j = k
            int sum_j = sum_i - k;
            // This code is for counting first i+1 subarray if its sum is equals to k
            // Ex: {1, -1, 5, -2, 3}, first 4 items sum is 3, so set max = 4
            if (sum_i == k) {
            	max = i + 1;
            }
            if (map.containsKey(sum_j)) {
                int j = map.get(sum_j);
                max = Math.max(max, i-j);
            }
            // keep only 1st duplicate as we want first index as left as possible
            if (!map.containsKey(sum_i)) {
                map.put(sum_i, i);
            }
        }
        return max;
    }
    
    public int maxSubArrayLen(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int sum_i = 0, max = Integer.MIN_VALUE;
        Map<Integer, Integer> map = new HashMap<>();
        // This code is for counting first i+1 subarray if its sum is equals to k
        // Ex: {1, -1, 5, -2, 3}, first 4 items sum is 3, so set max = 4
        map.put(0, -1);
        for (int i = 0, n = nums.length; i < n; i++) {
            sum_i += nums[i];

            // SUM_i - SUM_j = k
            int sum_j = sum_i - k;
            
            if (map.containsKey(sum_j)) {
                int j = map.get(sum_j);
                max = Math.max(max, i-j);
            }
            // keep only 1st duplicate as we want first index as left as possible
            if (!map.containsKey(sum_i)) {
                map.put(sum_i, i);
            }
            
        }
        return max == Integer.MIN_VALUE ? 0 : max;
    }

	// naive solution O(n^2)
	public int maxSubArrayLen2(int[] nums, int k) {
		int max = 0;
		for(int i = 0, n = nums.length; i < nax; i++){
			int sum = 0;
			for(int j = i; j < n; j++){
				sum += nums[j];
				if(sum == k){
					max = Math.max(max, j-i+1);
				}
			}
		}
		return max;
	}
}