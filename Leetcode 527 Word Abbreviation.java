/**
Given an array of n distinct non-empty strings, you need to generate minimal possible abbreviations for every word following rules below.

Begin with the first character and then the number of characters abbreviated, which followed by the last character.
If there are any conflict, that is more than one words share the same abbreviation, a longer prefix is used instead of only the first character until making the map from word to abbreviation become unique. In other words, a final abbreviation cannot map to more than one original words.
If the abbreviation doesn't make the word shorter, then keep it as original.
Example:
Input: ["like", "god", "internal", "me", "internet", "interval", "intension", "face", "intrusion"]
Output: ["l2e","god","internal","me","i6t","interval","inte4n","f2e","intr4n"]
Note:
Both n and the length of each word will not exceed 400.
The length of each word is greater than 1.
The words consist of lowercase English letters only.
The return answers should be in the same order as the original array.
*/
public class Solution {
    /**
     * @param dict: an array of n distinct non-empty strings
     * @return: an array of minimal possible abbreviations for every word
     */
     // Running Time Complexity: O(N^2), Space Complexity: O(N)
    public String[] wordsAbbreviation(String[] dict) {
        int n = dict.length;
        String[] result = new String[n];
        // An array to record the length of prefix we use on each word:
        // Ex: k = 1: word -> w2d
        // k = 2: word -> wo1d
        int[] prefixNum = new int[n];
        // Generate Abbreivate Words:
        for (int i = 0; i < n; i++) {
            result[i] = abbreviateWord(dict[i], 1);
            prefixNum[i] = 1;
        }
        
        for (int i = 0; i < n; i++) {
            Set<Integer> set = new HashSet<Integer>();;
            do {
                set.clear();
                for (int j = i+1; j < n; j++) {
                    if (result[j].equals(result[i])) {
                        set.add(j);
                    }
                }
                if (set.isEmpty()) {
                    break;
                }
                else {
                    set.add(i);
                    // increase the prefixNum and generate the new abbreivated word
                    for (int num : set) {
                        prefixNum[num]++;
                        result[num] = abbreviatedWord(dict[num], prefixNum[num]);
                    }
                }
            } while (!set.isEmpty());  
        }
        return result;
    }
    
    private String abbreviatedWord(String word, int k) {
        StringBuffer sb = new StringBuffer();
        sb.append(word.substring(0,k));
        sb.append(word.length() - k - 1);
        sb.append(word.charAt(word.length() - 1));
        if (sb.length() < word.length()) {
            return sb.toString();
        }
        else {
            return word;
        }
    }
}