/**
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most two transactions.

Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).

Example 1:

Input: [3,3,5,0,0,3,1,4]
Output: 6
Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
             Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.
Example 2:

Input: [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
             Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
             engaging multiple transactions at the same time. You must sell before buying again.
Example 3:

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.
*/

public class Solution {

    /**
    k = 2 和前面题目的情况稍微不同，因为上面的情况都和 k 的关系不太大。
    要么 k 是正无穷，状态转移和 k 没关系了；要么 k = 1，跟 k = 0 这个 base case 挨得近，最后也没有存在感。
    这道题 k = 2 和后面要讲的 k 是任意正整数的情况中，对 k 的处理就凸显出来了。

    原始的状态转移方程:
    dp[i][k][0] = Math.max(dp[i-1][k][0], dp[i-1][k][1] + prices[i]);
    dp[i][k][1] = Math.max(dp[i-1][k][1], dp[i-1][k-1][0] - prices[i]);
    
    */
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int K = 2;
        int[][][] dp = new int[n][K+1][2];
        for (int i = 0; i < n; i++) {
            for (int k = K; k >= 1; k--) {
                if (i == 0) {
                    dp[i][k][0] = 0;
                    dp[i][k][1] = -prices[i];
                    continue;
                }
                dp[i][k][0] = Math.max(dp[i-1][k][0], dp[i-1][k][1] + prices[i]);
                dp[i][k][1] = Math.max(dp[i-1][k][1], dp[i-1][k-1][0] - prices[i]);
            }
        }
        return dp[n-1][K][0];
    }

    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int K = 2;
        int n = prices.length;
        int dp_i_1_0 = 0;
        int dp_i_1_1 = Integer.MIN_VALUE;
        int dp_i_2_0 = 0;
        int dp_i_2_1 = Integer.MIN_VALUE;
        
        for (int i = 0; i < n; i++) {
            dp_i_2_0 = Math.max(dp_i_2_0, dp_i_2_1 + prices[i]);
            dp_i_2_1 = Math.max(dp_i_2_1, dp_i_1_0 - prices[i]);
            dp_i_1_0 = Math.max(dp_i_1_0, dp_i_1_1 + prices[i]);
            dp_i_1_1 = Math.max(dp_i_1_1, -prices[i]);
        }
        return dp_i_2_0;
    }

    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int maxProfit(int[] prices) {
        int hold1 = Integer.MIN_VALUE, hold2 = Integer.MIN_VALUE;
        int release1 = 0, release2 = 0;
        for (int price : prices) {
            release2 = Math.max(release2, hold2 + price); // sold 2nd
            hold2 = Math.max(hold2, release1 - price);    // buy 2nd
            release1 = Math.max(release1, hold1 + price); // sold 1st
            hold1 = Math.max(hold1, -price);              // buy 1st
        }
        return release2;
    }

    // Running Time Complexity: O(K * N^2)
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int k = 2, n = prices.length, maxProfit = 0;
        int[][] dp = new int[k+1][n];
        // kth transaction of previous day (dp[i][j-1]), vs:
        // k-1th transaction of previous day (dp[i-1][j-1]) and make transaction today:
        // dp[i][j] = Math.max(dp[i][j-1], dp[i-1][j-1] + (prices[j] - currMin))
        for (int i = 1; i <= 2; i++) {
            for (int j = 1; j < n; j++) {
                // complete transaction on day j, the best you can get by selling at day j and buying it on m day.
                // prevMax = Math.max(prevMax, prices[j] - prices[m] + dp[i-1][m]);
                int prevMax = 0;
                for (int m = 0; m < j; m++) {
                    prevMax = Math.max(prevMax, prices[j] - prices[m] + dp[i-1][m]);
                }
                dp[i][j] = Math.max(dp[i][j-1], prevMax);
            }
        }
        return dp[k][n-1];
    }
    // dp solution
    /*
        dp[i][j] max profit until price[j]
        dp[0][j] = 0, 0 transaction
        dp[i][0] = 0, one price data, no profit
        dp[i][j] = max(dp[i][j-1],price[j] - prices[m] + dp[i-1][m]) 0 <= m <= j-1
    */
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int k = 2;
        int n = prices.length;
        int[][] dp = new int[k+1][n];
        // kth transaction of previous day (dp[i][j-1]), did not make transaction vs:
        // k-1th transaction of previous day (dp[i-1][j-1]) and make transaction today:
        // dp[i][j] = Math.max(dp[i][j-1], dp[i-1][j-1] + (prices[j] - currMin))
        for (int i = 1; i <= k; i++) {
            // buy stock at prices[0]:
            int maxDiff = -prices[0];
            for (int j = 1; j < n; j++) {
                // did not make transaction on day j vs sell stock at day j:
                dp[i][j] = Math.max(dp[i][j-1], prices[j] + maxDiff);
                // dp[i-1][j] - prices[j]: profit of k-1th transaction of current day and buy stock at prices[j]
                // vs: previous profit of prices[j]
                maxDiff = Math.max(maxDiff, dp[i-1][j] - prices[j]);
            }
        }
        return dp[k][n-1];
    }

    // Scan the array twice, first time, scan front to the end, mark maxProfit of each day and save it into an array.
    // second time, scan from end to back:
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int[] leftProfit = new int[n];
        int maxLeftProfit = 0, currMin = prices[0];
        for (int i = 1; i < n; i++) {
            // not make profit:
            if (prices[i] < currMin) {
                currMin = prices[i];
            }
            // make profit:
            else {
                maxLeftProfit = Math.max(maxLeftProfit, prices[i] - currMin);
            }
            leftProfit[i] = maxLeftProfit;
        }
        int maxProfit = leftProfit[n-1];
        int currMax = prices[n-1], maxRightProfit = 0;
        for (int i = n-2; i >= 1; i--) {
            // does not make profit:
            if (prices[i] > currMax) {
                currMax = prices[i];
            }
            else {
                maxRightProfit = Math.max(maxRightProfit, currMax - prices[i]);
            }
            maxProfit = Math.max(maxProfit, leftProfit[i] + maxRightProfit);
        }
        return maxProfit;
    }
}