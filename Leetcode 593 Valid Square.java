/**
Given the coordinates of four points in 2D space, return whether the four points could construct a square.

The coordinate (x,y) of a point is represented by an integer array with two integers.

Example:

Input: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,1]
Output: True


Note:

All the input integers are in the range [-10000, 10000].
A valid square has four equal sides with positive length and four equal angles (90-degree angles).
Input points have no order.
*/
public class Solution {

    class Point {
        int x;
        int y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        Point pp1 = new Point(p1[0], p1[1]);
        Point pp2 = new Point(p2[0], p2[1]);
        Point pp3 = new Point(p3[0], p3[1]);
        Point pp4 = new Point(p4[0], p4[1]);

        List<Point> points = Arrays.asList(pp1, pp2, pp3, pp4);
        Set<String> set = new HashSet<>();
        for (Point p : points) {
            String key = convertPointToKey(p);
            if (!set.contains(key)) {
                set.add(key);
            }
        }
        if (set.size() != 4) {
            return false;
        }

        Point p = forthPointFormSquare(pp1, pp2, pp3);
        return p != null && p.x == pp4.x && p.y == pp4.y;
    }

    private String convertPointToKey(Point point) {
        StringBuffer sb = new StringBuffer();
        sb.append(point.x);
        sb.append("-");
        sb.append(point.y);
        return sb.toString();
    }

    private Point forthPointFormSquare(Point p1, Point p2, Point p3) {
        // p1 is the corner point:
        int res = (p2.x - p1.x) * (p3.x - p1.x) + (p2.y - p1.y) * (p3.y - p1.y);
        if (res == 0 && getDistance(p3, p1) == getDistance(p2, p1)) {
            // validate if vector P3_1 is the same length of P2_1
            return createNewPoint(p1, p2, p3);
        }
        // p2 is the corner point:
        res = (p1.x - p2.x) * (p3.x - p2.x) + (p1.y - p2.y) * (p3.y - p2.y);
        if (res == 0 && getDistance(p3, p2) == getDistance(p1, p2)) {
            // validate if vector P3_1 is the same length of P2_1
            return createNewPoint(p2, p1, p3);
        }
        // p3 is the corner point
        res = (p1.x - p3.x) * (p2.x - p3.x) + (p1.y - p3.y) * (p2.y - p3.y);
        if (res == 0 && getDistance(p1, p3) == getDistance(p2, p3)) {
            // validate if vector P3_1 is the same length of P2_1
            return createNewPoint(p3, p1, p2);
        }
        return null;
    }

    private int getDistance(Point p1, Point p2) {
        int x = p1.x - p2.x;
        int y = p1.y - p2.y;
        return x * x + y * y;
    }

    // P1 is the corner point:
    private Point createNewPoint(Point p1, Point p2, Point p3) {
        int x = (p2.x - p1.x) + (p3.x - p1.x) + p1.x;
        int y = (p2.y - p1.y) + (p3.y - p1.y) + p1.y;
        return new Point(x, y);
    }
}

// Solution 2:
public class Solution {
    public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        List<Integer> distances = new ArrayList<>();
        distances.add(getDistance(p1, p2));
        distances.add(getDistance(p1, p3));
        distances.add(getDistance(p1, p4));
        distances.add(getDistance(p2, p3));
        distances.add(getDistance(p2, p4));
        distances.add(getDistance(p3, p4));

        int side = distances.get(0);
        int diag = 0;
        // key -> distance, val -> freq
        Map<Integer, Integer> map = new HashMap<>();
        for (int distance : distances) {
            side = Math.min(side, distance);
            diag = Math.max(diag, distance);
            map.put(distance, map.getOrDefault(distance, 0) + 1);
        }
        return map.keySet().size() == 2 && map.get(side) == 4 && map.get(diag) == 2;
    }

    private int getDistance(int[] p, int[] q) {
        int x = p[0] - q[0];
        int y = p[1] - q[1];
        return x * x + y * y;
    }
}
