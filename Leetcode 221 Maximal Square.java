/**
Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

Example:

Input: 

1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0

Output: 4
*/

public class Solution {
    
    /**
    dp[i][j]: maximum square size you can achieve at matrix[i-1][j-1]
    dp[i][j] min of {
              dp[i-1][j]
              dp[i-1][j-1]
              dp[i][j-1]
             } + 1;
    dp[i][j] can only be 2 only when dp[i-1][j], dp[i][j-1], dp[i-1][j-1] are all 1
    Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    */
    public int maximalSquare(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[][] dp = new int[m+1][n+1];
        int size = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if current number is 1, find the min of dp[i-1][j], dp[i][j-1], dp[i-1][j-1]
                if (matrix[i-1][j-1] == '1') {
                    int minVal = Math.min(dp[i-1][j], dp[i][j-1]);
                    dp[i][j] = Math.min(dp[i-1][j-1], minVal) + 1;
                    size = Math.max(size, dp[i][j]);
                }
            }
        }
        return size * size;
}

    // Running Time Complexity: O(m*n), Space Complexity: O(n)
    /**
    use variable lastLeftItem to keep track or dp[i-1][j-1] item, so that we can use an dp[n+1] array to store the state
    */
    public int maximalSquare(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[] dp = new int[n+1];
        int prevTopLeftItem = 0, size = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                int prevLeftItem = dp[j];
                if (matrix[i-1][j-1] == '1') {
                    int minVal = Math.min(dp[j], dp[j-1]);
                    dp[j] = Math.min(prevTopLeftItem, minVal) + 1;
                    size = Math.max(size, dp[j]);
                }
                else {
                    dp[j] = 0;
                }
                prevTopLeftItem = prevLeftItem;
            }
        }
        return size * size;
    }

    // Solution 3:
    // DFS: Running Time Complexity: O(m * n), Space Complexity: O(L) largest square size in the matrix
    public int maximalSquare(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int size = 0;
        for (int i = 0; i < m - size; i++) {
            for (int j = 0; j < n - size; j++) {
                int maxVal = maximalSquare(matrix, i, j, m, n, size);
                size = Math.max(size, maxVal);
            }
        }
        return size * size;
    }
    
    private int maximalSquare(char[][] matrix, int i, int j, int m, int n, int size) {
        if (i + size >= m || j + size >= n) {
            return 0;
        }
        if (matrix[i][j] == '0') {
            return 0;
        }
        for (int row = i; row <= i + size; row++) {
            for (int col = j; col <= j + size; col++) {
                if (matrix[row][col] == '0') {
                    return 0;
                }
            }
        }
        int maxVal = maximalSquare(matrix, i, j, m, n, size+1);
        return Math.max(size + 1, maxVal);
    }
}