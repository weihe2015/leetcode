/**
Given an integer array nums, find three numbers whose product is maximum and return the maximum product.

Example 1:

Input: nums = [1,2,3]
Output: 6
Example 2:

Input: nums = [1,2,3,4]
Output: 24
Example 3:

Input: nums = [-1,-2,-3]
Output: -6

*/
class Solution {
    /**
    遍历一次，找到最小的两个数和最大的三个数即可
    最小与两个最大乘积 和 三个最大乘积
    只需要比较 最小与两个最大乘积 和 三个最大乘积
    * 3个最大数乘积: 
        -- 0负、3个及以上非负: max = 3非负乘积
        -- 1负、2个非负: max = 1负2个非负乘积
        -- 1负、3个及以上非负: max = 3非负乘积
    * 2个最小1个最大乘积: 
        -- 2负、1~2非负: max = 2负1非负乘积
    * 3个最大数乘积、2个最小1个最大乘积:
        -- 2负、3个及以上非负: max = 3非负乘积、2负1非负乘积
    * 3个最大数乘积: 
        -- 3负、0非负: max = 3负乘积
    * 3个最大数乘积、2个最小1个最大乘积: 
        -- 3负、1~2非负: max = 2负1非负乘积
    * 3个最大数乘积、2个最小1个最大乘积: 
        -- 3负、3个及以上非负: max = 3非负乘积、2负1非负乘积

以上10中情况全部都能被包含，所以只需要算出2个最小和三个最大即可

https://leetcode-cn.com/problems/maximum-product-of-three-numbers/solution/628san-ge-shu-de-zui-da-cheng-ji-by-ac_f-mnhw/
    */
    public int maximumProduct(int[] nums) {
        int n = nums.length;
        if (n < 3) {
            return 0;
        }
        int min1 = Integer.MAX_VALUE;
        int min2 = Integer.MAX_VALUE;
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        int max3 = Integer.MIN_VALUE;
        
        for (int num : nums) {
            if (num < min1) {
                min2 = min1;
                min1 = num;
            }
            else if (num < min2) {
                min2 = num;
            }
            
            if (num > max1) {
                max3 = max2;
                max2 = max1;
                max1 = num;
            }
            else if (num > max2) {
                max3 = max2;
                max2 = num;
            }
            else if (num > max3) {
                max3 = num;
            }
        }
        return Math.max(min1 * min2 * max1, max1 * max2 * max3);
    }
}