/**
Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.

Example 1:

Input: [[1,1],[2,2],[3,3]]
Output: 3
Explanation:
^
|
|        o
|     o
|  o  
+------------->
0  1  2  3  4
Example 2:

Input: [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]]
Output: 4
Explanation:
^
|
|  o
|     o        o
|        o
|  o        o
+------------------->
0  1  2  3  4  5  6
*/
/**
 * Definition for a point.
 * class Point {
 *     int x;
 *     int y;
 *     Point() { x = 0; y = 0; }
 *     Point(int a, int b) { x = a; y = b; }
 * }
 */
public class Solution {
    /**
    经过某个点的直线，哪条直线上的点最多。
    当确定一个点后，平面上的其他点都和这个点可以求出一个斜率，斜率相同的点就意味着在同一条直线上。

    所以我们可以用 HashMap 去计数，斜率作为 key，然后遍历平面上的其他点，相同的 key 意味着在同一条直线上。

    上边的思想解决了「经过某个点的直线，哪条直线上的点最多」的问题。接下来只需要换一个点，然后用同样的方法考虑完所有的点即可。

    当然还有一个问题就是斜率是小数，怎么办。

    之前提到过了，我们用分数去表示，求分子分母的最大公约数，然后约分，最后将 「分子 + "@" + "分母"」作为 key 即可。

    最后还有一个细节就是，当确定某个点的时候，平面内如果有和这个重叠的点，如果按照正常的算法约分的话，会出现除 0 的情况，
    所以我们需要单独用一个变量记录重复点的个数，而重复点一定是过当前点的直线的。

    */
    /**
     * 思路：对于每个点p1，计算他之后的每一个点与p1的斜率dx和dy. 
     * 如果dx = 0 && dy == 0, 那么相同的点++
     * 局部最优解就是 maxPoint = Math.max(maxPoint, map.get(dx).get(dy))
     * 全局最优解就是 max = Math.max(max, maxPoint + samePoint);
     */
    public int maxPoints(Point[] points) {
        if (points == null) {
            return 0;
        }
        int n = points.length;
        if (n <= 2) {
            return n;
        }
        int max = 0;
        // Key -> dx, value -> map of (dy, freq)
        Map<Integer, Map<Integer, Integer>> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            Point p1 = points[i];
            int samePoints = 1;
            int maxPoints = 0;
            // 每次都要重置map是因为对于每一个点来说，之前计算的斜率和结果都不相同，不能使用。
            map.clear();
            for (int j = i+1; j < n; j++) {
                Point p2 = points[j];
                int dx = p1.x - p2.x;
                int dy = p1.y - p2.y;
                if (dx == 0 && dy == 0) {
                    samePoints++;
                }
                else {
                    // 化简斜率:, 用dx和dy的最大公约数来化简
                    // d 不可能等于零，因为p1和p2不相等，dx != 0 || dy != 0
                    int d = gcd(dx, dy);
                    dx /= d;
                    dy /= d;

                    Map<Integer, Integer> subMap = map.getOrDefault(dx, new HashMap<>());
                    int freq = subMap.getOrDefault(dy, 0) + 1;
                    subMap.put(dy, freq);
                    map.put(dx, subMap);

                    maxPoints = Math.max(maxPoints, freq);
                }
            }
            max = Math.max(max, samePoints + maxPoints);
        }
        return max;
    }
    
    private int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }
}