/**
Given a nested list of integers, return the sum of all integers in the list weighted by their depth.

Each element is either an integer, or a list -- whose elements may also be integers or other lists.

Different from the previous question where weight is increasing from root to leaf, now the weight is defined from bottom up. i.e., the leaf level integers have weight 1, and the root level integers have the largest weight.

Example 1:

Input: [[1,1],2,[1,1]]
Output: 8
Explanation: Four 1's at depth 1, one 2 at depth 2.
Example 2:

Input: [1,[4,[6]]]
Output: 17
Explanation: One 1 at depth 3, one 4 at depth 2, and one 6 at depth 1; 1*3 + 4*2 + 6*1 = 17.
*/
public class Solution {
    // Running Time Complexity: O(N), Space Complexity: O(N),
    // Iterate the nestedInteger list twice
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int depth = calculateDepthOfNestedList(nestedList);
        int sum = 0;
        Queue<NestedInteger> queue = new LinkedList<>();
        queue.addAll(nestedList);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (ni.isInteger()) {
                    sum += ni.getInteger() * depth;
                }
                else {
                    queue.addAll(ni.getList());
                }
            }
            depth--;
        }
        return sum;
    }

    private int calculateDepthOfNestedList(List<NestedInteger> nestedList) {
        Queue<NestedInteger> queue = new LinkedList<>();
        queue.addAll(nestedList);
        int depth = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (!ni.isInteger()) {
                    queue.addAll(ni.getList());
                }
            }
            depth++;
        }
        return depth;
    }

    // Best Solution: Use two int variables
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int weighted = 0, unweighted = 0;
        Queue<NestedInteger> queue = new LinkedList<>();
        queue.addAll(nestedList);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (ni.isInteger()) {
                    unweighted += ni.getInteger();
                }
                else {
                    queue.addAll(ni.getList());
                }
            }
            weighted += unweighted;
        }
        return weighted;
    }
}