/*
Given an array of meeting time intervals consisting of start and
end times [[s1,e1],[s2,e2],...] (si < ei), determine if a person could attend all meetings.

Example 1:

Input: [[0,30],[5,10],[15,20]]
Output: false
Example 2:

Input: [[7,10],[2,4]]
Output: true
*/
public class Solution {
    class Interval {
        int start;
        int end;
        public Interval() {
           this.start = 0;
           this.end = 0;
        }
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

	// Solution 1: Running Time Complexity: O(nlogn), Space Complexity: O(1)
    public boolean canAttendMeetings(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return true;
        }
        // Sort List of Intervals by start time
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        int end = -1;
        for (Interval interval : intervals) {
            if (end == -1 || end <= interval.start) {
                end = interval.end;
            }
            else if (end > interval.start) {
                return false;
            }
        }
        return true;
    }

	// Solution 2
	// O(nlogn) time, O(n) space, but faster
    public boolean canAttendMeetings(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return true;
        }
        int n = intervals.size();
        int[] starts = new int[n], ends = new int[n];
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        Arrays.sort(starts);
        Arrays.sort(ends);
        for (int i = 0; i < n-1; i++) {
            if (ends[i] > starts[i+1]) {
                return false;
            }
        }
        return true;
    }
}