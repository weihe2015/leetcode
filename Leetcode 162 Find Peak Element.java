/**
A peak element is an element that is greater than its neighbors.

Given an input array where num[i] ≠ num[i+1], find a peak element and return its index.

The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.

You may imagine that num[-1] = num[n] = -∞.

For example, in array [1, 2, 3, 1], 3 is a peak element and your function should return the index number 2.
*/
public class Solution {
    // binary search iteration
    // Running Time Complexity: O(logN)
    public int findPeakElement(int[] nums) {
        int low = 0, high = nums.length-1;
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            // when mid == nums.length - 1, it means the whole array is accending.
            if (mid == nums.length - 1) {
                res = mid;
                break;
            }
            if (nums[mid] < nums[mid+1]) {
                low = mid + 1;
            }
            // We cannot stop here because it may have a peak element on the left
            // Ex: [3,2,1]
            else if (nums[mid] > nums[mid+1]) {
                res = mid;
                high = mid - 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }
    // sequential search
    // Bruth Force
    // Running Time Complexity: O(N)
    public int findPeakElement(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        // Bruth Force:
        for (int i = 0, max = nums.length; i < max-1; i++) {
            if (nums[i] > nums[i+1]) {
                return i;
            }
        }
        return nums.length-1;
    }


    // binary search iterative
    public int findPeakElement2(int[] nums) {
        return helper(nums,0,nums.length-1);
    }

    public int helper(int[] nums, int lo, int hi){
        if(lo == hi)
            return lo;
        else{
            int mid1 = (lo + hi) / 2;
            int mid2 = mid1+1;
            if(nums[mid1] < nums[mid2])
                return helper(nums,mid2,hi);
            else
                return helper(nums,lo,mid1);
        }
    }
}