Write a program to find the n-th ugly number.

Ugly numbers are positive numbers whose prime factors only include 2, 3, 5. For example, 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 is the sequence of the first 10 ugly numbers.

Note that 1 is typically treated as an ugly number.

public class Solution {
    public int nthUglyNumber(int n) {
        int i2 = 0, i3 = 0, i5 = 0;
        int[] r = new int[n];
        r[0] = 1;
        for(int i = 1; i < n; i++){
            r[i] = Math.min(r[i2]*2,Math.min(r[i3]*3,r[i5]*5));
            if(r[i]%2 == 0) i2++;
            if(r[i]%3 == 0) i3++;
            if(r[i]%5 == 0) i5++;
        }
        return r[n-1];
    }
}