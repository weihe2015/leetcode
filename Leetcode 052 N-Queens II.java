/*
Follow up for N-Queens problem.

Now, instead outputting board configurations, return the total number of distinct solutions.
*/
public class Solution {
    // Solution 1:
    public int totalNQueens(int n) {
        boolean[] cols = new boolean[n];     // columns   |
        boolean[] d1 = new boolean[2 * n];   // diagonals \
        boolean[] d2 = new boolean[2 * n];   // diagonals /
        int[] res = new int[1];
        totalNQueens(cols, d1, d2, 0, n, res);
        return res[0];
    }
    
    private void totalNQueens(boolean[] cols, boolean[] d1, boolean[] d2, int row, int n, int[] res) {
        if (row == n) {
            res[0]++;
            return;
        }
        for (int col = 0; col < n; col++) {
            int id1 = col - row + n;
            int id2 = col + row;
            if (cols[col] || d1[id1] || d2[id2]) {
                continue;
            }
            cols[col] = true;
            d1[id1] = true;
            d2[id2] = true;
            totalNQueens(cols, d1, d2, row+1, n, res);
            cols[col] = false;
            d1[id1] = false;
            d2[id2] = false;
        }
    }

    // Solution 2:
    int result = 0;
    public int totalNQueens(int n) {
        boolean[] column = new boolean[n];
        boolean[] dia45 = new boolean[2 * n - 1];
        boolean[] dia135 = new boolean[2 * n - 1];
        helper(0, n, column, dia45, dia135);
        return result;
    }
    private void helper(int row, int n, boolean[] column, boolean[] dia45, boolean[] dia135) {
        if (row == n) {
            result++;
            return;
        }
        for (int col = 0; col < n; col++) {
            if (!column[col] && !dia45[col + row] && !dia135[n - 1- row + col]) {
                column[col] = dia45[col + row] = dia135[n - 1- row + col] = true;
                helper(row + 1, n, column, dia45, dia135);
                column[col] = dia45[col + row] = dia135[n - 1- row + col] = false;
            }
        }
    }
}