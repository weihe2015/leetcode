/**
Given two words (beginWord and endWord), and a dictionary's word list, find the length of shortest transformation sequence from beginWord to endWord, such that:

Only one letter can be changed at a time.
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
Note:

Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
Example 1:

Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output: 5

Explanation: As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
return its length 5.
Example 2:

Input:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]

Output: 0

Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
*/
/**
Basically I keep two sets of words, one set reached that represents the borders that have been reached with "distance" steps;
another set wordDict that has not been reached. In the while loop, for each word in the reached set,
I give all variations and check if it matches anything from wordDict, if it has a match, I add that word into toAdd set,
which will be my "reached" set in the next loop,
and remove the word from wordDict because I already reached it in this step.
And at the end of while loop,
I check the size of toAdd, which means that if I can't reach any new String from wordDict,
I won't be able to reach the endWord, then just return 0.
Finally if the endWord is in reached set, I return the current steps "distance".

The idea is that reached always contain only the ones we just reached in the last step,
and wordDict always contain the ones that haven't been reached. This is pretty much what Dijkstra's algorithm does,
or you can see this as some variation of BFS.
*/
public class Solution {
    // Running Time Complexity: BFS: O(26 * l * N)
    // where N = num of words in wordDict, l = word.length()
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordDict = new HashSet<>(wordList);
        if (!wordDict.contains(endWord)) {
            return 0;
        }
        Queue<String> queue = new LinkedList<>();
        queue.offer(beginWord);
        int level = 1;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                String currWord = queue.poll();
                char[] cList = currWord.toCharArray();
                for (int j = 0, n = cList.length; j < n; j++) {
                    char originChar = cList[j];
                    for (char c = 'a'; c <= 'z'; c++) {
                        if (originChar == c) {
                            continue;
                        }
                        cList[j] = c;
                        String newWord = String.valueOf(cList);
                        if (newWord.equals(endWord)) {
                            return level+1;
                        }
                        if (wordDict.contains(newWord)) {
                            queue.offer(newWord);
                            wordDict.remove(newWord);
                        }
                    }
                    cList[j] = originChar;
                }
            }
            level++;
        }
        return 0;
    }

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordDict = new HashSet<String>(wordList);
        if (!wordDict.contains(endWord)) {
            return 0;
        }
        Set<String> reached = new HashSet<>();
        Set<String> neighbors = new HashSet<String>();
        reached.add(beginWord);
        int step = 1;
        while (!reached.contains(endWord)) {
            for (String str : reached) {
                char[] cList = str.toCharArray();
                for (int i = 0, n = cList.length; i < n; i++) {
                    char oldChar = cList[i];
                    for (char ch = 'a'; ch <= 'z'; ch++) {
                        if (ch == oldChar) {
                            continue;
                        }
                        cList[i] = ch;
                        String newWord = new String(cList);
                        if (newWord.equals(endWord)) {
                            return step+1;
                        }
                        if (wordDict.contains(newWord)) {
                            neighbors.add(newWord);
                            // Remove this word to avoid cyclic if there is a cycle in wordDict
                            wordDict.remove(newWord);
                        }
                    }
                    cList[i] = oldChar;
                }
            }
            step++;
            // No new word can be reached from reached set in wordDict
            if (neighbors.isEmpty()) {
                return 0;
            }
            // Both reached and neighbors are pointing to same reference. So cannot neighbors.clear(). It will clear both variable.
            reached.addAll(neighbors);
            // matching the one with reached.addAll(neighbors);
            // Both reached and neighbors are pointing to same reference. So cannot neighbors.clear(). It will clear both variable.
            neighbors.clear();
        }
        return 0;
    }

    // Solution 2:
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordDict = new HashSet<String>(wordList);
        if (!wordDict.contains(endWord)) {
            return 0;
        }
        Set<String> beginSet = new HashSet<>();
        Set<String> endSet = new HashSet<>();
        Set<String> neighbors = new HashSet<>();
        beginSet.add(beginWord);
        endSet.add(endWord);
        int step = 1;
        while (!beginSet.isEmpty() && !endSet.isEmpty()) {
            if (beginSet.size() > endSet.size()) {
                Set<String> tmp = beginSet;
                beginSet = endSet;
                endSet = tmp;
            }
            for (String str : beginSet) {
                char[] cList = str.toCharArray();
                for (int i = 0, n = cList.length; i < n; i++) {
                    char oldChar = cList[i];
                    for (char ch = 'a'; ch <= 'z'; ch++) {
                        if (ch == oldChar) {
                            continue;
                        }
                        cList[i] = ch;
                        String newWord = new String(cList);
                        if (endSet.contains(newWord)) {
                            return step+1;
                        }
                        if (wordDict.contains(newWord)) {
                            neighbors.add(newWord);
                            wordDict.remove(newWord);
                        }
                    }
                    cList[i] = oldChar;
                }
            }
            step++;
            if (neighbors.isEmpty()) {
                break;
            }
            beginSet.addAll(neighbors);
            neighbors.clear();
        }
        return 0;
    }
}