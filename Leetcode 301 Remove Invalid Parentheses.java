/**
Remove the minimum number of invalid parentheses in order to make the input string valid. Return all possible results.

Note: The input string may contain letters other than the parentheses ( and ).

Example 1:

Input: "()())()"
Output: ["()()()", "(())()"]
Example 2:

Input: "(a)())()"
Output: ["(a)()()", "(a())()"]
Example 3:

Input: ")("
Output: [""]
*/
public class Solution {
    // Solution 1: find extra '(' or ')' to delete: 
    // Running Time Complexity: O(2^(l+r)), Space Complexity: O((l+r)^2) ~ O(N^2)
    /** 
     * Idea: 1. Compute the min number of '(' and ')' to remove unbalanced '(' and ')'
     *    2. Try all possible ways to remove ')' and '('. Remove ')' to make prefix valid.
     *    3. When l == 0 and r == 0 and removed string is valid, we add it into result set.
    */
    public List<String> removeInvalidParentheses(String s) {
        List<String> result = new ArrayList<String>();
        // Compute the min number of '(' and ')' to remove unbalanced '(' and ')'
        int l = 0, r = 0;
        for (char c : s.toCharArray()) {
            if (c == '(') {
                l++;
            }
            else if (c == ')') {
                if (l > 0) {
                    l--;
                }
                else {
                    r++;
                }
            }
        }
        // convert string to arrayList:
        // String is immutable in Java, we need to use other data structure to make it mutable.
        List<Character> list = convertStringToList(s);
        dfs(result, l, r, 0, list);
        return result;
    }
    
    private void dfs(List<String> result, int l, int r, int start, List<Character> list) {
        if (l == 0 && r == 0 && isValid(list)) {
            result.add(convertListToString(list));
            return;
        }
        for (int i = start, n = list.size(); i < n; i++) {
            char c = list.get(i);
            // we only remove the last parthesis if there are consecutives ones,
            // to avoid duplicates
            if (i > start && c == list.get(i-1)) {
                continue;
            }
            if (c == '(' || c == ')') {
                list.remove(i);
                if (r > 0) {
                    dfs(result, l, r-1, i, list);
                }
                else if (l > 0) {
                    dfs(result, l-1, r, i, list);
                }
                list.add(i, c);
            }
        }
    }
    
    private boolean isValid(List<Character> list) {
        int count = 0;
        for (char c : list) {
            if (c == '(') {
                count++;
            }
            else if (c == ')') {
                count--;
            }
            // If count < 0, it means in prefix string, num of ) is greater than (
            if (count < 0) {
                return false;
            }
        }
        return count == 0;
    }
    
    private List<Character> convertStringToList(String s) {
        List<Character> list = new ArrayList<Character>();
        for (char c : s.toCharArray()) {
            list.add(c);
        }
        return list;
    }
    
    private String convertListToString(List<Character> list) {
        StringBuffer sb = new StringBuffer();
        for (char c : list) {
            sb.append(c);
        }
        return sb.toString();
    }

    // Solution 2: Append characters:
    public List<String> removeInvalidParentheses(String s) {
        Set<String> result = new HashSet<String>();
        int l = 0, r = 0, n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                l++;
            }
            else if (c == ')') {
                if (l != 0) {
                    l--;
                }
                else {
                    r++;
                }
            }
        }
        dfs(result, s, n, l, r, 0, 0, new StringBuffer());
        return new ArrayList<String>(result);
    }
    /**
    * For either '(' or ')', there are two options, add it or not add it.
    * We use l and r to record which side of parthensis is extra, and use 'open' to keep track of number of parenthesis
    */
    private void dfs(Set<String> result, String s, int n, int l, int r, int level, int open, StringBuffer sb) {
        if (level == n && l == 0 && r == 0 && open == 0) {
            result.add(sb.toString());
            return;
        }
        if (level == n || l < 0 || r < 0 || open < 0) {
            return;
        }
        char c = s.charAt(level);
        int prevLen = sb.length();
        if (c == '(') {
            // not use (
            dfs(result, s, n, l-1, r, level+1, open, sb);
            sb.append(c);
            // use (
            dfs(result, s, n, l, r, level+1, open+1, sb);
        }
        else if (c == ')') {
            // not use )
            dfs(result, s, n, l, r-1, level+1, open, sb);
            sb.append(c);
            // use (
            dfs(result, s, n, l, r, level+1, open-1, sb);
        }
        else {
            sb.append(c);
            dfs(result, s, n, l, r, level+1, open, sb);
        }
        sb.setLength(prevLen);
    }

    // Return only one result:
    /** 
     * "(a)()" -> "(a)()"
        "((bc)" -> "(bc)".
        ")))a((" -> "a"
        "(a(b)" ->"(ab)" or "a(b)"
     */
    public String removeInvalidParentheses(String s) { 
        int l = 0, r = 0;
        StringBuffer sb = new StringBuffer();
        StringBuffer result = new StringBuffer();
        // remove invalid ')'
        for (char c : s.toCharArray()) {
            if (c == '(') {
                l++;
                sb.append(c);
            }
            else if (c == ')') {
                if (l > 0) {
                    l--;
                    sb.append(c);
                }
            }
            else {
                sb.append(c);
            }
        }

        // remove invalid '('
        for (int i = sb.length() - 1; i >= 0; i--) {
            char c = sb.charAt(i);
            if (c == ')') {
                r++;
                result.append(c);
            }
            else if (c == '(') {
                if (r > 0) {
                    r--;
                    result.append(c);
                }
            }
            else {
                result.append(c);
            }
        }
        return result.reverse().toString();
    }

    @Test
    public void removeInvalidParentheseTest() throws Exception {
        String s = "(a)()";
        assertEquals("(a)()", removeInvalidParentheses(s));
        s = "((bc)";
        assertEquals("(bc)", removeInvalidParentheses(s));
        s = ")))a((";
        assertEquals("a", removeInvalidParentheses(s));
        s = "(a(b)"
        assertEquals("(ab)", removeInvalidParentheses(s));
    }

    public String removeInvalidParentheses(String s) {
        int l = 0, r = 0;
        // remove invalid ')':
        StringBuffer sb = new StringBuffer();
        for (char c : s.toCharArray()) {
            if (c == '(') {
                l++;
                sb.append(c);
            }
            else if (c == ')') {
                if (l > 0) {
                    l--;
                    sb.append(c);
                }
            }
            else {
                sb.append(c);
            }
        }
        StringBuffer result = new StringBuffer();
        // remove invalid '(':
        for (int i = sb.length() - 1; i >= 0; i--) {
            char c = sb.charAt(i);
            if (c == ')') {
                r++;
                result.append(c);
            }
            else if (c == '(') {
                if (r > 0) {
                    r--;
                    result.append(c);
                }
            }
            else {
                result.append(c);
            }
        }
        return result.reverse().toString();
    }

}