/*
Clone an undirected graph. Each node in the graph contains a label and a list of its neighbors.

OJs undirected graph serialization:
Nodes are labeled uniquely.

We use # as a separator for each node, and , as a separator for node label and each neighbor of the node.
As an example, consider the serialized graph {0,1,2#1,2#2,2}.

The graph has a total of three nodes, and therefore contains three parts as separated by #.

First node is labeled as 0. Connect node 0 to both nodes 1 and 2.
Second node is labeled as 1. Connect node 1 to node 2.
Third node is labeled as 2. Connect node 2 to node 2 (itself), thus forming a self-cycle.
Visually, the graph looks like the following:

       1
      / \
     /   \
    0 --- 2
         / \
         \_/
*/
/*
// Definition for a Node.
class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {
        val = 0;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val) {
        val = _val;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val, ArrayList<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
}
*/
public class Solution {
    // Solution 1: DFS:
    private Map<Integer, Node> map = new HashMap<>();

    public Node cloneGraph(Node node) {
        return cloneNode(node);
    }

    private Node cloneNode(Node node) {
        if (node == null) {
            return null;
        }
        if (map.containsKey(node.val)) {
            return map.get(node.val);
        }

        Node clonedNode = new Node(node.val);
        map.put(node.val, clonedNode);

        for (Node neighbor : node.neighbors) {
            Node clonedNeighbor = cloneNode(neighbor);
            clonedNode.neighbors.add(clonedNeighbor);
        }
        return clonedNode;
    }

    // Solution: BFS:
    public Node cloneGraph(Node node) {
        if (node == null) {
            return node;
        }
        Node clonedNode = new Node(node.val);
        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);

        // key -> node's val, value -> cloned Node
        Map<Integer, Node> map = new HashMap<>();
        map.put(node.val, clonedNode);

        while (!queue.isEmpty()) {
            Node curr = queue.poll();
            // handle the neighbors:
            for (Node neighbor : curr.neighbors) {
                if (!map.containsKey(neighbor.val)) {
                    // clone the neighbor:
                    Node clonedNeighbor = new Node(neighbor.val);
                    map.put(neighbor.val, clonedNeighbor);
                    queue.add(neighbor);
                }
                map.get(curr.val).neighbors.add(map.get(neighbor.va;));
            }
        }
        return clonedNode;
    }
}