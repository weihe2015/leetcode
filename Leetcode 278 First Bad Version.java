/*
You are a product manager and currently leading a team to develop a new product.
Unfortunately, the latest version of your product fails the quality check.
Since each version is developed based on the previous version, all the versions after a bad version are also bad.

Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.

You are given an API bool isBadVersion(version)
which will return whether version is bad. Implement a function to find the first bad version.
You should minimize the number of calls to the API.
*/
/* The isBadVersion API is defined in the parent class VersionControl.
      boolean isBadVersion(int version); */

public class Solution extends VersionControl {
    // Find the left boundary of target
    // Running Time Complexity: O(logN), Space Complexity: O(1)
    public int firstBadVersion(int n) {
        int low = 1, high = n,
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (isBadVersion(mid)) {
                res = mid;
                // bias to left because we want to find the first bad version, which is on the left
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return res;
    }

    public int firstBadVersion(int n) {
        if(n == 1)
            return 1;
        int start = 1;
        int end = n;
        while(start < end){
            int mid = start + (end - start) / 2;
            if(isBadVersion(mid)){
                end = mid;
            }
            else{
                start = mid+1;
            }
        }
        return end;
    }
    public int firstBadVersion(int n) {
        int l = 1, r = n;
        while(l < r){
            int m = l + (r - l)/2;
            if(isBadVersion(m))
                r = m;
            else
                l = m + 1;
        }
        // could also use return r; they are the same
        return l;
    }

    // No need to consider the boundary.
    public int firstBadVersion(int n) {
        int low = 0, high = n;
        while (low + 1 < high) {
            int mid = low + (high - low)/2;
            if (isBadVersion(mid)) {
                high = mid;
            }
            else {
                low = mid;
            }
        }
        if (isBadVersion(low)) {
            return low;
        }
        else if (isBadVersion(high)) {
            return high;
        }
        else {
            return -1;
        }
    }

    public int firstBadVersion(int n) {
        if (n < 1) {
            return -1;
        }
        int low = 1, high = n;
        while (low < high) {
            int mid = low + (high - low)/2;
            if (isBadVersion(mid)) {
                high = mid;
            }
            else {
                low = mid + 1;
            }
        }
        // if there is no bad version, we need to verify it,
        // otherwise, if we have assumption that we have bad version, just return low.
        if (isBadVersion(low)) {
            return low;
        }
        else {
            return -1;
        }
    }
}