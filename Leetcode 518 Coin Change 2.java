/**
You are given coins of different denominations and a total amount of money. Write a function to compute the number of combinations that make up that amount. You may assume that you have infinite number of each kind of coin.

Note: You can assume that

0 <= amount <= 5000
1 <= coin <= 5000
the number of coins is less than 500
the answer is guaranteed to fit into signed 32-bit integer

Example 1:

Input: amount = 5, coins = [1, 2, 5]
Output: 4
Explanation: there are four ways to make up the amount:
5=5
5=2+2+1
5=2+1+1+1
5=1+1+1+1+1

Example 2:

Input: amount = 3, coins = [2]
Output: 0
Explanation: the amount of 3 cannot be made up just with coins of 2.

Example 3:

Input: amount = 10, coins = [10]
Output: 1
*/
public class Solution {
    // https://labuladong.gitbook.io/algo/dong-tai-gui-hua-xi-lie/bei-bao-ling-qian
    // 有重复背包 == 完全背包
    // Running Time Complexity: O(n*m), Space Complexity: O(m * n)
    public int change(int amount, int[] coins) {
        if (amount == 0) {
            return 1;
        }
        if (coins == null || coins.length == 0) {
            return 0;
        }
        int n = coins.length;
        int m = amount;
        // 若只使用前 i 个物品，当背包容量为 j 时，有 dp[i][j] 种方法可以装满背包。
        // dp[i][j]
        int[][] dp = new int[n+1][m+1];
        // dp[0][x]: 不使用任何一个硬币，就无法凑出任何金额
        // dp[x][0]: 凑出的目标金额为0，就只有1种方法
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }

        for (int i = 1; i <= n; i++) {
            int currItem = coins[i-1];
            for (int j = 1; j <= amount; j++) {
                if (j >= currItem) {
                    // dp[i-1][j]: 只使用前i-1个物品 凑出背包容量j 的方法数量
                    // dp[i][j-currItem]: 使用前i个物品，凑出j-currItem金额的方法数量
                    // Ex: 你想用面值为2的硬币凑出金额5，那么如果你知道了凑出金额 3 的方法，再加上一枚面额为 2 的硬币，不就可以凑出5了嘛
                    dp[i][j] = dp[i-1][j] + dp[i][j-currItem];
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }

    /**
    我们通过观察可以发现，dp 数组的转移只和 dp[i][..] 和 dp[i-1][..] 有关，
    所以可以压缩状态，进一步降低算法的空间复杂度
    */
    // Running Time Complexity: O(n*m), Space Complexity: O(m)
    public int change(int amount, int[] coins) {
        int n = coins.length;
        int m = amount;
        int[] dp = new int[m+1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            int currItem = coins[i-1];
            // we cannot reverse the j order like for (int j = m; j >= 1; j--) { }
            // which is incorrect.
            // 完全背包问题，就是一个物品可以多次使用，就必须要 for (int j = 1; j <= m; j++) { }
            for (int j = 1; j <= m; j++) {
                if (j >= currItem) {
                    dp[j] = dp[j] + dp[j-currItem];
                }
            }
        }
        return dp[m];
    }
}