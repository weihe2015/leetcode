/**
Implement a basic calculator to evaluate a simple expression string.

The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

The expression string contains only non-negative integers, +, -, *, / operators , open ( and closing parentheses ) and empty spaces . The integer division should truncate toward zero.

You may assume that the given expression is always valid. All intermediate results will be in the range of [-2147483648, 2147483647].

Some examples:

"1 + 1" = 2
" 6-4 / 2 " = 4
"2*(5+5*2)/3+(6/2+8)" = 21
"(2+6* 3+5- (3*14/7+2)*5)+3"=-12
*/
public class Solution {
	// Running Time Complexity: O(n), Space Complexity: O(n)
	/**
	 * Basic Idea:
	 * The same as Leetcode 227, Basic Calculator II.
	 * 	Use result to store previous num.
	 * 		when previous sign == '+' or '-', set previousNum = num or -num, and add it to result.
	 *  	when previous sign == '*' or '/', substract previousNum from res, 
	 * 			set previousNum = previousNum * num or previousNum = previousNum / num; and add it to result.
	 *  For open parentheses:
	 * 	    Save previousSign and result into stack. Reset num, previous sign and num to initial state.
	 *  For close parenthese:
	 * 	    1: finish calculate the expression inside this parenthesis by applying the same method as step 1:
	 *      2: set num as this result
	 *      3: Set previous sign from stack and previous num & result from stack
	 * 
	 *  In the end, in order to handle the last number: apply the same method as step 1 to get the result.
	*/
	public int calculate(String s) {
		Stack<Object> stack = new Stack<>();
		int result = 0;
		int num = 0;
		int prevNum = 0;
		char prevSign = '+';
		for (char c : s.toCharArray()) {
			if (Character.isDigit(c)) {
				num = 10 * num + (c - '0');
			}
			else if (c == ' ') {
				continue;
			}
			else if (c == '(') {
				stack.push(result);
				stack.push(prevSign);
				prevSign = '+';
				result = 0;
				num = 0;
			}
			else if (c == ')') {
				int[] arr = handleOperators(result, prevNum, num, prevSign);
                result = arr[0]; 
				prevNum = arr[1];
				// set num as the result of this expression inside the parentheses
				num = result;
				// get the prevSign from stack:
				prevSign = (Character) stack.pop();
				// get result from stack and set prevNum the same as result.
				result = (Integer) stack.pop();
				prevNum = result;
			}
			else {
				int[] arr = handleOperators(result, prevNum, num, prevSign);
				result = arr[0];
				prevNum = arr[1];
				prevSign = c;
				num = 0;
			}
		}
		// handle last number:
		if (num != 0) {
			int[] arr = handleOperators(result, prevNum, num, prevSign);
			result = arr[0];
			prevNum = arr[1];
		}
		return result;
	}
	
	private int[] handleOperators(int result, int prevNum, int num, char prevSign) {
		int[] arr = new int[2];
		if (prevSign == '+') {
			prevNum = num;
		}
		else if (prevSign == '-') {
			prevNum = -1 * num;
		}
		else if (prevSign == '*') {
			result -= prevNum;
			prevNum = prevNum * num;
		}
		else if (prevSign == '/') {
			result -= prevNum;
			prevNum = prevNum / num;
		}
		result += prevNum;
		arr[0] = result;
		arr[1] = prevNum;
		return arr;
	}
    
    @Test
	public void BasicCalculatorIIITest1() throws Exception {
		BasicCalculatorIII sol = new BasicCalculatorIII();
		String s = "2*(5+5*2)/3+(6/2+8)";
		int res = sol.calculate(s);
		Assert.assertEquals(21, res);
		s = "(2+6* 3+5- (3*14/7+2)*5)+3";
		res = sol.calculate(s);
		Assert.assertEquals(-12, res);
	}
}