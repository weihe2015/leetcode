/**
Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

For example,
Given the following matrix:

[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]

You should return [1,2,3,6,9,8,7,4,5].
*/
public class Solution {
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> result = new ArrayList<Integer>();
        int m = matrix.length;
        int n = matrix[0].length;
        
        if (m == 0 || n == 0) {
            return res;
        }
        int colBegin = 0;
        int colEnd = n-1;
        
        int rowBegin = 0;
        int rowEnd = m-1;

        while (rowBegin <= rowEnd && colBegin <= colEnd) {
            // traverse right:
            for (int i = colBegin; i <= colEnd; ++i) {
                result.add(matrix[rowBegin][i]);
            }
            ++rowBegin;

            // traverse down:
            for (int i = rowBegin; i <= rowEnd; ++i) {
                result.add(matrix[i][colEnd]);
            }
            --colEnd;

            // traverse left:
            // add condition to handle case: [[1,2,3]], when rowBegin > rowEnd
            if (rowStart > rowEnd) {
                break;
            }
            for (int i = colEnd; i >= colBegin; --i) {
                result.add(matrix[rowEnd][i]);
            }
            --rowEnd;

            // traverse up:
            // add condition to handle case: [[1], [2],[3]], when colBegin > colEnd
            if (colStart > colEnd) {
                break;
            }
            for (int i = rowEnd; i >= rowBegin; --i) {
                result.add(matrix[i][colBegin]);
            }
            ++colBegin;
        }
        return result;
    }
}