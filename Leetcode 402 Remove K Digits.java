/**
Given a non-negative integer num represented as a string, remove k digits from the number so that the new number is the smallest possible.

Note:
The length of num is less than 10002 and will be ≥ k.
The given num does not contain any leading zero.
Example 1:

Input: num = "1432219", k = 3
Output: "1219"
Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219 which is the smallest.
Example 2:

Input: num = "10200", k = 1
Output: "200"
Explanation: Remove the leading 1 and the number is 200. Note that the output must not contain leading zeroes.
Example 3:

Input: num = "10", k = 2
Output: "0"
Explanation: Remove all the digits from the number and it is left with nothing which is 0.
*/
public class Solution {
    public String removeKdigits(String num, int k) {
        // monotonous increasing stack
        Deque<Character> stack = new ArrayDeque<Character>();
        for (char c : num.toCharArray()) {
            // While this character is greater than the the head of stack, pop it
            while (k > 0 && !stack.isEmpty() && stack.peekFirst() > c) {
                stack.pollFirst();
                k--;
            }
            stack.offerFirst(c);
        }
        // Perform additional removes from monotonous increasing stack:
        while (k > 0 && !stack.isEmpty()) {
            stack.pollFirst();
            k--;
        }
        // Append number into StringBuffer, and remove leading zeros:
        StringBuffer sb = new StringBuffer();
        while (!stack.isEmpty()) {
        	sb.append(stack.pop());
        }
        String reverse = sb.reverse().toString();
        sb.setLength(0);
        // remove leading zeros:
        int i = 0, n = reverse.length();
        while (i < n && reverse.charAt(i) == '0') {
        	i++;
        }
        sb = new StringBuffer(reverse.substring(i));
        return sb.length() == 0 ? "0" : sb.toString();
    }
}