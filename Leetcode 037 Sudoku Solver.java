/**
 * Write a program to solve a Sudoku puzzle by filling the empty cells.
A sudoku solution must satisfy all of the following rules:

Each of the digits 1-9 must occur exactly once in each row.
Each of the digits 1-9 must occur exactly once in each column.
Each of the the digits 1-9 must occur exactly once in each of the 9 3x3 sub-boxes of the grid.
Empty cells are indicated by the character '.'.

Note:

The given board contain only digits 1-9 and the character '.'.
You may assume that the given Sudoku puzzle will have a single unique solution.
The given board size is always 9x9.
 *
*/
public class Solution {
    private static final int ONE = 1;
    private static final int THREE = 3;
    private static final int NINE = 9;
    public void solveSudoku(char[][] board) {
        if (board == null || board.length != NINE || board[0].length != NINE) {
            return;
        }
        solve(board);
    }

    private boolean solve(char[][] board) {
        for (int i = 0; i < NINE; i++) {
            for (int j = 0; j < NINE; j++) {
                if (board[i][j] == '.') {
                    // try each number from 1 to 9:
                    for (int num = ONE; num <= NINE; num++) {
                        char c = Character.forDigit(num, 10);
                        if (isValid(board, i, j, c)) {
                            board[i][j] = c;
                            if (solve(board)) {
                                return true;
                            }
                            else {
                                board[i][j] = '.';
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isValid(char[][] board, int i, int j, char c) {
        // Check each row:
        for (int row = 0; row < NINE; row++) {
            if (board[row][j] == c) {
                return false;
            }
        }

        // check each column:
        for (int col = 0; col < NINE; col++) {
            if (board[i][col] == c) {
                return false;
            }
        }

        // check each 3*3 matrix:
        int row = i/3, col = j/3;
        int xIdx = row*3, yIdx = col*3;
        for (int x = xIdx; x < xIdx+THREE; x++) {
            for (int y = yIdx; y < yIdx+THREE; y++) {
                if (board[x][y] == num) {
                    return false;
                }
            }
        }
        return true;
    }
}