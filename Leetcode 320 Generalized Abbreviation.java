/**
Write a function to generate the generalized abbreviations of a word.

Example:
Given word = "word", return the following list (order does not matter):
["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
*/
public class Solution {
    // Running Time Complexity: O(2^N), Space Complexity: O(N)
    public List<String> generateAbbreviations(String word) {
        List<String> result = new ArrayList<String>();
        if (word == null || word.length() == 0) {
            return result;
        }
        generateAbbreviations(result, word, new StringBuffer(), 0, 0);
        return result;
    }
    
    private void generateAbbreviations(List<String> result, String word, StringBuffer sb, int count, int level) {
        if (level == word.length()) {
            if (count > 0) {
                sb.append(count);
            }
            result.add(sb.toString());
            return;
        }
        int prevLen = sb.length();
        generateAbbreviations(result, word, sb, count+1, level+1);
        sb.setLength(prevLen);

        if (count > 0) {
            sb.append(count);
        }
        sb.append(word.charAt(level));
        generateAbbreviations(result, word, sb, 0, level + 1);
        sb.setLength(prevLen);
    }
}