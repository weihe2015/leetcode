/*
Median is the middle value in an ordered integer list.
If the size of the list is even, there is no middle value.
So the median is the mean of the two middle value.

Examples:
[2,3,4] , the median is 3

[2,3], the median is (2 + 3) / 2 = 2.5

Design a data structure that supports the following two operations:

void addNum(int num) - Add a integer number from the data stream to the data structure.
double findMedian() - Return the median of all elements so far.
For example:

add(1)
add(2)
findMedian() -> 1.5
add(3)
findMedian() -> 2
*/

class MedianFinder {
    // Similar to solution Leetcode 480: Slidign Window Median:
    // https://leetcode.com/problems/sliding-window-median
    private Queue<Integer> minQueue;
    private Queue<Integer> maxQueue;

    /** initialize your data structure here. */
    public MedianFinder() {
        minQueue = new PriorityQueue<>();
        maxQueue = new PriorityQueue<>(Collections.reverseOrder());
    }

    public void addNum(int num) {
        if (minQueue.isEmpty()) {
            minQueue.add(num);
            return;
        }
        else if (minQueue.peek() <= num) {
            minQueue.offer(num);
        }
        else {
            maxQueue.offer(num);
        }
        rebalance();
    }

    private void rebalance() {
        if (minQueue.size() - maxQueue.size() > 1) {
            maxQueue.offer(minQueue.poll());
        }
        else if (maxQueue.size() > minQueue.size()) {
            minQueue.offer(maxQueue.poll());
        }
    }

    public double findMedian() {
        if (minQueue.size() == maxQueue.size()) {
            return ((double) minQueue.peek() + (double) maxQueue.peek()) / 2.0;
        }
        else {
            return (double) minQueue.peek();
        }
    }
}

// Solution 2:
class MedianFinder {

    private List<Integer> nums;
    /** initialize your data structure here. */
    public MedianFinder() {
        this.nums = new ArrayList<>();
    }
    
    public void addNum(int num) {
        this.nums.add(num);
    }
    
    // Follow up: getMedium, Solution 1: use quick select: quickly finds the k-th smallest element of an unsorted array of n elements.
    // Quick select time complexity: O(N), with worst case of O(N^2), increasing order of element.
    // pivot is always the largest element and k is always 1
    public double findMedian() {
        int size = nums.size();
        if (size % 2 == 1) {
            // use quick select to find the kth smallest item in the ArrayList:
            int k = size / 2;
            return kthSmallest(0, size-1, k);
        }
        else {
            int k = size / 2 - 1;
            int k1 = size / 2;
            int num1 = kthSmallest(0, size-1, k);
            int num2 = kthSmallest(0, size-1, k1);
            return (num1 + num2) / 2.0;
        }
    }

    private int kthSmallest(int low, int high, int k) {
        int idx = partition(low, high);
        if (idx == k) {
            return nums.get(idx);
        }
        else if (idx < k) {
            return kthSmallest(idx+1, high, k);
        }
        else {
            return kthSmallest(low, idx-1, k);
        }
    }
    
    private int partition(int low, int high) {
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (nums.get(j) <= nums.get(pivot)) {
                swap(i, j);
                i++;
            }
        }
        swap(i, pivot);
        return i;
    }
    
    private void swap(int i, int j) {
        int temp = nums.get(i);
        nums.set(i, nums.get(j));
        nums.set(j, temp);
    }
    
}

// Your MedianFinder object will be instantiated and called as such:
// MedianFinder mf = new MedianFinder();
// mf.addNum(1);
// mf.findMedian();