/**
Given a directed acyclic graph of N nodes. Find all possible paths from node 0 to node N-1, and return them in any order.

The graph is given as follows:  the nodes are 0, 1, ..., graph.length - 1.  graph[i] is a list of all nodes j for which the edge (i, j) exists.

Input: graph = [[1,2],[3],[3],[]]
Output: [[0,1,3],[0,2,3]]
Explanation: There are two paths: 0 -> 1 -> 3 and 0 -> 2 -> 3.

Input: graph = [[4,3,1],[3,2,4],[3],[4],[]]
Output: [[0,4],[0,3,4],[0,1,3,4],[0,1,2,3,4],[0,1,4]]
Example 3:

Input: graph = [[1],[]]
Output: [[0,1]]
Example 4:

Input: graph = [[1,2,3],[2],[3],[]]
Output: [[0,1,2,3],[0,2,3],[0,3]]
Example 5:

Input: graph = [[1,3],[2],[3],[]]
Output: [[0,1,2,3],[0,3]]

*/
public class Solution {
    public List<List<Integer>> allPathsSourceTarget(int[][] graph) {
        List<List<Integer>> res = new ArrayList<>();
        if (graph == null || graph.length == 0) {
            return res;
        }

        int n = graph.length;
        boolean[] visited = new boolean[n];
        dfs(graph, 0, new ArrayList<>(), res, visited);
        return res;
    }

    private void dfs(int[][] graph, int from, List<Integer> path, List<List<Integer>> paths, boolean[] visited) {
        if (visited[from]) {
            return;
        }
        visited[from] = true;
        path.add(from);

        if (from == graph.length-1) {
            paths.add(new ArrayList<>(path));
        }
        else {
            for (int neighbor : graph[from]) {
                dfs(graph, neighbor, path, paths, visited);
            }
        }

        visited[from] = false;
        path.remove(path.size()-1);
    }
}