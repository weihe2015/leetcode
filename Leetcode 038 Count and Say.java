/*
The count-and-say sequence is the sequence of integers with the first five terms as following:

1.     1
2.     11
3.     21
4.     1211
5.     111221
1 is read off as "one 1" or 11.
11 is read off as "two 1s" or 21.
21 is read off as "one 2, then one 1" or 1211.
Given an integer n, generate the nth term of the count-and-say sequence.

Note: Each term of the sequence of integers will be represented as a string.

Example 1:

Input: 1
Output: "1"
Example 2:

Input: 4
Output: "1211"
*/

public class Solution {
    // Running Time Complexity: O(n * m), where m is the last prev String length.
    public String countAndSay(int n) {
        StringBuffer sb = new StringBuffer();
        sb.append("1");
        // iterate n-1 times:
        for (int i = 1; i <= n-1; i++) {
            String prev = sb.toString();
            // reset stringBuffer for each iteration:
            sb.setLength(0);
            int count = 1;
            char say = prev.charAt(0);
            // iterate previous string to get the count and say:
            for (int j = 1, m = prev.length(); j < m; j++) {
                char curr = prev.charAt(j);
                if (curr == say) {
                    count++;
                }
                else {
                    sb.append(count);
                    sb.append(say);
                    // reset count and say
                    say = curr;
                    count = 1;
                }
            }
            // get last count and say:
            sb.append(count);
            sb.append(say);
        }
        return sb.toString();
    }
}