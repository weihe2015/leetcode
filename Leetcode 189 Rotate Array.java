/**

Given an array, rotate the array to the right by k steps, where k is non-negative.

Example 1:

Input: [1,2,3,4,5,6,7] and k = 3
Output: [5,6,7,1,2,3,4]
Explanation:
rotate 1 steps to the right: [7,1,2,3,4,5,6]
rotate 2 steps to the right: [6,7,1,2,3,4,5]
rotate 3 steps to the right: [5,6,7,1,2,3,4]
Example 2:

Input: [-1,-100,3,99] and k = 2
Output: [3,99,-1,-100]
Explanation:
rotate 1 steps to the right: [99,-1,-100,3]
rotate 2 steps to the right: [3,99,-1,-100]
Note:

Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
Could you do it in-place with O(1) extra space?
*/
public class Solution {
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public void rotate(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k <= 0) {
            return;
        }
        // use % to avoid the case of k > n
        k %= n;
        int n = nums.length;
        int[] temp = new int[n];
        for (int i = 0; i < n; i++) {
            int idx = (i + k) % n;
            temp[idx] = nums[i];
        }
        // copy temp back to nums:
        for (int i = 0; i < n; i++) {
            nums[i] = temp[i];
        }
    }

    // O(n) time, O(n) space
    public void rotate(int[] nums, int k) {
        int n = nums.length;
        k = k % n;
        int[] temp = new int[n];
        for(int i = 0; i < k; i++){
            temp[i] = nums[n-k+i];
        }
        for(int i = 0; i < n-k; i++){
            temp[k+i] = nums[i];
        }
        System.arraycopy(temp, 0, nums, 0, n);
    }

    // Time Complexity: O(N), Space Complexity: O(1)
    public void rotate(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k <= 0) {
            return;
        }
        int n = nums.length;
        k %= n;
        // reverse first n-k number
        reverse(nums, 0, n-k-1);
        // reverse last k number
        reverse(nums, n-k, n-1);
        // reverse all number
        reverse(nums, 0, n-1);
    }

    private void reverse(int[] nums, int i, int j) {
        while (i < j) {
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}