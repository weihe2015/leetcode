/**
Given a 2d grid map of '1's (land) and '0's (water),
count the number of islands. An island is surrounded by water and
is formed by connecting adjacent lands horizontally or vertically.
You may assume all four edges of the grid are all surrounded by water.

Example 1:

Input:
11110
11010
11000
00000

Output: 1
Example 2:

Input:
11000
11000
00100
00011

Output: 3
*/
// https://leetcode.com/problems/number-of-islands
public class Solution {
    /*
    The algorithm works as follow:

    1.Scan each cell in the grid.
    2.If the cell value is '1' explore that island.
    3.Mark the explored island cells with 'x'.
    4.Once finished exploring that island, increment islands counter.
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    res++;
                    numIslands(grid, i, j, m, n);
                }
            }
        }
        return res;
    }

    private void numIslands(char[][] grid, int i, int j, int m, int n) {
        if (i < 0 || i >= m || j < 0 || j >= n) {
            return;
        }
        if (grid[i][j] != '1') {
            return;
        }
        grid[i][j] = 'X';
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            numIslands(grid, x, y, m, n);
        }
    }

    // Without modifying input grid, use boolean[][] as visited marker
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int res = 0;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1' && !visited[i][j]) {
                    res++;
                    numIslands(grid, visited, i, j, m, n);
                }
            }
        }
        return res;
    }

    private void numIslands(char[][] grid, boolean[][] visited, int i, int j, int m, int n) {
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] != '1' || visited[i][j]) {
            return;
        }
        visited[i][j] = true;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            numIslands(grid, visited, x, y, m, n);
        }
    }

    // solution 3, union find
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numIslands(char[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int N = m * n;
        UnionFind uf = new UnionFind(N);
        // build graph:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    // current index of the id array:
                    int idx = i * n + j;
                    for (int[] dir : dirs) {
                        int x = i + dir[0];
                        int y = j + dir[1];
                        if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] != '1') {
                            continue;
                        }
                        int neighborIndex = x * n + y;
                        uf.union(idx, neighborIndex);
                    }
                }
            }
        }
        int count = 0;
        for (int i = 0; i < N; i++) {
            // count only the root with label 1
            if (i == uf.id[i] && grid[i/n][i%n] == '1') {
                count++;
            }
        }
        return count;
    }

    class UnionFind {
        private int[] id;
        private int[] size;

        public UnionFind(int N) {
            id = new int[N];
            size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }

        private int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        private void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
        }
    }
}