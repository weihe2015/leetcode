/*
Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.

According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes v and w as the lowest node in T that has both v and w as descendants (where we allow a node to be a descendant of itself).”

        _______3______
       /              \
    ___5__          ___1__
   /      \        /      \
   6      _2       0       8
         /  \
         7   4
For example, the lowest common ancestor (LCA) of nodes 5 and 1 is 3. Another example is LCA of nodes 5 and 4 is 5, since a node can be a descendant of itself according to the LCA definition.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive
    // https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-tree/solution/236-er-cha-shu-de-zui-jin-gong-gong-zu-xian-hou-xu/
    // Running Time Complexity: O(N), 最差情况下，需要递归遍历树的所有节点。
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // 当越过叶节点，则直接返回 null
        // 当 rootroot 等于 p,q ，则直接返回 root
        if (root == null || root == p || root == q) {
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        
        // 说明 root 的左 / 右子树中都不包含 p,q ，返回 null
        if (left == null && right == null) {
            return null;
        }
        // p,q 都不在 root 的左子树中
        else if (left == null) {
            return right;
        }
         // p,q 都不在 root 的右子树中
        else if (right == null) {
            return left;
        }
        // 说明 p,q 分列在 root 的异侧 (分别在 左 / 右子树)，因此 root 为最近公共祖先，返回 root;
        else {
            return root;
        }
    }

    // Iterative
    // solution 3, use parent node
    /**
    时间复杂度：O(N)，其中 N 是二叉树的节点数。二叉树的所有节点有且只会被访问一次，从 p 和 q 节点往上跳经过的祖先节点个数不会超过 N，因此总的时间复杂度为 O(N)。

    空间复杂度：O(N) ，其中 N 是二叉树的节点数。递归调用的栈深度取决于二叉树的高度，二叉树最坏情况下为一条链，此时高度为 NN，因此空间复杂度为 O(N)，
    哈希表存储每个节点的父节点也需要 O(N) 的空间复杂度，因此最后总的空间复杂度为 O(N)。
    */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        }
        // Key: childNode, Value: ParentNode
        Map<TreeNode, TreeNode> parentMap = new HashMap<>();
        parentMap.put(root, null);
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!parentMap.containsKey(p) || !parentMap.containsKey(q)) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                if (currNode.left != null) {
                    parentMap.put(currNode.left, currNode);
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    parentMap.put(currNode.right, currNode);
                    queue.offer(currNode.right);
                }
            }
        }
        // find all ancestors of node p:
        Set<TreeNode> ancestor = new HashSet<>();
        TreeNode curr = p;
        while (curr != null) {
            ancestor.add(curr);
            curr = parentMap.get(curr);
        }

        // Get all ancestor of node q until it reaches the one of node p:
        curr = q;
        while (!ancestor.contains(curr)) {
            curr = parentMap.get(curr);
        }
        return curr;
    }
}