/**
Count the number of prime numbers less than a non-negative number, n.



Example 1:

Input: n = 10
Output: 4
Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
Example 2:

Input: n = 0
Output: 0
Example 3:

Input: n = 1
Output: 0
*/
public class Solution {
    // Solution 1: TLE O(N^2)
    public int countPrimes(int n) {
        List<Integer> nums = new ArrayList<>();
        for (int i = 2; i < n; i++) {
            if (isPrime(i)) {
                nums.add(i);
            }
        }
        return nums.size();
    }
    private boolean isPrime(int num) {
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    // Solution 2:
    public int countPrimes(int n) {
        boolean[] primes = new boolean[n];
        // assume all numbers starting from 2 are prime:
        for (int i = 2; i < n; i++) {
            primes[i] = true;
        }

        for (int i = 2; i*i < n; i++) {
            if (primes[i]) {
                for (int j = 2*i; j < n; j += i) {
                    primes[j] = false;
                }
            }
        }
        int cnt = 0;
        for (int i = 2; i < n; i++) {
            cnt += primes[i] ? 1 : 0;
        }
        return cnt;
    }
}