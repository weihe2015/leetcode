/**
An abbreviation of a word follows the form <first letter><number><last letter>. Below are some examples of word abbreviations:

a) it                      --> it    (no abbreviation)

     1
b) d|o|g                   --> d1g

              1    1  1
     1---5----0----5--8
c) i|nternationalizatio|n  --> i18n

              1
     1---5----0
d) l|ocalizatio|n          --> l10n
Assume you have a dictionary and given a word, find whether its abbreviation is unique in the dictionary. A words abbreviation is unique if no other word from the dictionary has the same abbreviation.

Example: 
Given dictionary = [ "deer", "door", "cake", "card" ]

isUnique("dear") -> false
isUnique("cart") -> true
isUnique("cane") -> false
isUnique("make") -> true

1. dictionary = {"dear"},  isUnique("door") -> false
2. dictionary = {"door", "door"}, isUnique("door") -> true
3. dictionary = {"dear", "door"}, isUnique("door") -> false
*/
public class ValidWordAbbr {
    
    /*
    * @param dictionary: a list of words
    */
    // Key -> abbreviated word, value: list of word that matches this abbreviated word.
    Map<String, Set<String>> map = new HashMap<>();
    // Solution 1: Running Time Complexity: O(N), Space Complexity: O(N * L)
    public ValidWordAbbr(String[] dictionary) {
        for (String word : dictionary) {
            String newWord = abbreviateWord(word);
            Set<String> subset = null;
            if (!map.containsKey(newWord)) {
                subset = new HashSet<String>();
            }
            else {
                subset = map.get(newWord);
            }
            if (!subset.contains(word)) {
                subset.add(word);
            }
            map.put(newWord, subset);
        }
    }

    /*
     * @param word: a string
     * @return: true if its abbreviation is unique or false
     */
    // Solution 1: Running Time Complexity: O(1) 
    public boolean isUnique(String word) {
        String newWord = abbreviateWord(word);
        if (!map.containsKey(newWord) || (map.get(newWord).contains(word) && map.get(newWord).size() == 1)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private String abbreviateWord(String word) {
        if (word.length() <= 2) {
            return word;
        }
        StringBuffer sb = new StringBuffer();
        char[] cList = word.toCharArray();
        sb.append(cList[0]);
        sb.append(cList.length - 2);
        sb.append(cList[cList.length - 1]);
        return sb.toString();
    }

    // Solution 2:
    // 如果该缩写形式应经存在，如果映射的单词不是当前单词，我们将映射单词改为空字符串
    /*
    * @param dictionary: a list of words
    */
    Map<String, String> map = new HashMap<>();
    public ValidWordAbbr(String[] dictionary) {
        for (String word : dictionary) {
            String newWord = abbreviateWord(word);
            if (map.containsKey(newWord) && !map.get(newWord).equals(word)) {
                map.put(newWord, "");
            }
            else { 
                map.put(newWord, word);
            }
        }
    }

    /*
     * @param word: a string
     * @return: true if its abbreviation is unique or false
     */
    public boolean isUnique(String word) {
        String newWord = abbreviateWord(word);
        return !map.containsKey(newWord) || map.get(newWord).equals(word);
    }
}
