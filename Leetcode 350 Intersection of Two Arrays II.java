/**
Given two arrays, write a function to compute their intersection.

Example 1:

Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2,2]
Example 2:

Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [4,9]
Note:

Each element in the result should appear as many times as it shows in both arrays.
The result can be in any order.
Follow up:

What if the given array is already sorted? How would you optimize your algorithm?
What if nums1's size is small compared to nums2's size? Which algorithm is better?
What if elements of nums2 are stored on disk, and the memory is limited
such that you cannot load all elements into the memory at once?
*/
public class Solution {
	// O(nlogn) time O(1) space
    public int[] intersect(int[] nums1, int[] nums2) {
        List<Integer> list = new ArrayList<>();
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i = 0, j = 0, m = nums1.length, n = nums2.length;
        while (i < m && j < n) {
            if (nums1[i] == nums2[j]) {
                list.add(nums1[i]);
                i++;
                j++;
            }
            else if (nums1[i] < nums2[j]) {
                i++;
            }
            else {
                j++;
            }
        }
        i = 0;
        int[] res = new int[list.size()];
        for (int num : list) {
            res[i++] = num;
        }
        return res;
    }

    // O(n) time, O(n) space
    public int[] intersect(int[] nums1, int[] nums2) {
        // key -> num, val -> freq
        Map<Integer, Integer> map = new LinkedHashMap<>();
        for (int num : nums1) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        List<Integer> list = new ArrayList<>();
        for (int num : nums2) {
            if (map.containsKey(num)) {
                int freq = map.get(num);
                if (freq >= 1) {
                    list.add(num);
                    map.put(num, map.get(num)-1);
                }

            }
        }
        int i = 0;
        int[] res = new int[list.size()];
        for (int num : list) {
            res[i++] = num;
        }
        return res;
    }

	/**
	Follow up:
	1. What if the given array is already sorted? How would you optimize your algorithm?
		We will use two pointer solutions
	2. What if nums1's size is small compared to nums2's size? Which algorithm is better?
		Depends on space requirement. If no space requirement, hashmap solution is better
		If space is required, then we will choose two pointers solution
	3. What if elements of nums2 are stored on disk, and the memory is limited such that
		you cannot load all elements into the memory at once?
		We will perform external merged sort, by reading two small pieces from disk one time,
		sort them in memory and write them back to disk.

	*/
}