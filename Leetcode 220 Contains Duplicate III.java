Given an array of integers, find out whether there are two distinct indices i and j in the array such that the difference between nums[i] and nums[j] is at most t and the difference between i and j is at most k.

public class Solution {
    // TreeSet solution, O(n*logk) time 
    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if(nums == null || nums.length == 0 || k < 0)
            return false;
        TreeSet<Long> tree = new TreeSet<Long>();
        for(int i = 0; i < nums.length; i++){
            Long floor = tree.floor((long)nums[i]+t);
            if(floor != null && floor + t >= nums[i])
                return true;
            tree.add((long)nums[i]);
            if(i >= k)
                tree.remove((long)nums[i-k]);
        }
        return false;
    }

    // naive solution O(n^2)
    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if(nums.length <= 1 || nums == null)
            return false;
        for(int i = 0; i < nums.length; i++){
            for(int j = i + 1; j < nums.length && j <= i + k; j++){
                if(Math.abs(nums[i] - nums[j]) <= t)
                    return true;
            }
        }
        return false;
    }
}