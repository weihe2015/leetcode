/**
There are n servers numbered from 0 to n-1 connected by undirected server-to-server connections forming a network where connections[i] = [a, b] represents a connection between servers a and b. 
Any server can reach any other server directly or indirectly through the network.

A critical connection is a connection that, if removed, will make some server unable to reach some other server.

Return all critical connections in the network in any order.

Input: n = 4, connections = [[0,1],[1,2],[2,0],[1,3]]
Output: [[1,3]]
Explanation: [[3,1]] is also accepted.

Constraints:

1 <= n <= 10^5
n-1 <= connections.length <= 10^5
connections[i][0] != connections[i][1]
There are no repeated connections.
*/
// https://leetcode.com/discuss/interview-question/436073/
// https://leetcode.com/problems/critical-connections-in-a-network/
public class Solution {
    
    private int id = 0;
    private int[] lowLinkVals;
    private int[] ids;
    private boolean[] visited;
    private Map<Integer, Set<Integer>> graphMap;
    
    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        lowLinkVals = new int[n];
        ids = new int[n];
        visited = new boolean[n];
        graphMap = new HashMap<>();
        
        for (List<Integer> connection : connections) {
            int from = connection.get(0);
            int to = connection.get(1);
            addEdge(from, to);
            addEdge(to, from);
        }
        
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            dfs(i, i, -1, res);
        }
        return res;
    }
    
    private void addEdge(int from, int to) {
        Set<Integer> set = graphMap.getOrDefault(from, new HashSet<>());
        set.add(to);
        graphMap.put(from, set);
    }
    
    private void dfs(int root, int from, int parent, List<List<Integer>> res) {
        visited[from] = true;
        id++;
        lowLinkVals[from] = id;
        ids[from] = id;
        
        if (!graphMap.containsKey(from)) {
            return;
        }
        Set<Integer> neighbors = graphMap.get(from);
        for (int to : neighbors) {
            if (to == parent) {
                continue;
            }
            if (visited[to]) {
                lowLinkVals[from] = Math.min(lowLinkVals[from], ids[to]);
            }
            else {
                dfs(root, to, from, res);
                lowLinkVals[from] = Math.min(lowLinkVals[from], lowLinkVals[to]);
                // Articulation point found via bridge:
                if (ids[from] < lowLinkVals[to]) {
                    res.add(Arrays.asList(from, to));
                }
            }
        }
    }

    // Solution 2:
    private int T;
    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        Map<Integer, List<Integer>> graphMap = new HashMap<>();
        for (int i = 0; i < n; i++) {
            graphMap.put(i, new ArrayList<>());
        }
    
        for (List<Integer> connection : connections) {
            int from = connection.get(0);
            int to = connection.get(1);
            addEdge(graphMap, from, to);
            addEdge(graphMap, to, from);
        }
        this.T = 1;
        int[] timeStamp = new int[n];
        List<List<Integer>> res = new ArrayList<>();
        dfs(res, graphMap, timeStamp, 0, -1);
        return res;
    }
    
    private int dfs(List<List<Integer>> res, Map<Integer, List<Integer>> graphMap, int[] timeStamp, int from, int parent) {
        if (timeStamp[from] > 0) {
            return timeStamp[from];
        }
        timeStamp[from] = T++;
        
        int minTime = Integer.MAX_VALUE;
        List<Integer> neighbors = graphMap.get(from);
        for (int to : neighbors) {
            if (to == parent) {
                continue;
            }
            int nextTime = dfs(res, graphMap, timeStamp, to, from);
            minTime = Math.min(minTime, nextTime);
        }
        if (minTime >= timeStamp[from] && parent != -1) {
            res.add(Arrays.asList(from, parent));
        }
        return Math.min(minTime, timeStamp[from]);
    }
    
    private void addEdge(Map<Integer, List<Integer>> graphMap, int from, int to) {
        List<Integer> list = graphMap.get(from);
        list.add(to);
        graphMap.put(from, list);
    }
}