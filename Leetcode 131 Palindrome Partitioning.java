/*
Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

For example, given s = "aab",
Return

[["aa","b"],["a","a","b"]]
*/
public class Solution {
    public List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return result;
        }
        partition(result, s, 0, new ArrayList<String>());
        return result;
    }

    private void partition(List<List<String>> result, String s, int index, List<String> list) {
        if (index == s.length() && !list.isEmpty()) {
            result.add(new ArrayList<>(list));
            return;
        }
        for (int i = index, max = s.length(); i < max; i++) {
            String str = s.substring(index, i+1);
            if (!isPalindrome(str)) {
                continue;
            }
            list.add(str);
            partition(result, s, i+1, list);
            list.remove(list.size()-1);
        }
    }

    private boolean isPalindrome(String s) {
        int i = 0, j = s.length()-1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++; j--;
        }
        return true;
    }
}