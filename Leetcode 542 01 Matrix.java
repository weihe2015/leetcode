/**
Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.

The distance between two adjacent cells is 1.
Example 1: 
Input:

0 0 0
0 1 0
0 0 0
Output:
0 0 0
0 1 0
0 0 0
Example 2: 
Input:

0 0 0
0 1 0
1 1 1
Output:
0 0 0
0 1 0
1 2 1
Note:
The number of elements of the given matrix will not exceed 10,000.
There are at least one 0 in the given matrix.
The cells are adjacent in only four directions: up, down, left and right.
*/
public class Solution {
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int[][] updateMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return matrix;
        }
        int m = matrix.length, n = matrix[0].length;
        Queue<Point> queue = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    queue.offer(new Point(i, j));
                }
                else {
                    matrix[i][j] = Integer.MAX_VALUE;
                }
            }
        }
        while (!queue.isEmpty()) {
            int[] curr = queue.poll();
            int i = curr[0];
            int j = curr[1];
            for (int[] dir : dirs) {
                int x = i + dir[0];
                int y = j + dir[1];
                // If newly calculated distance >= current distance, then we don't need to explore that cell again.
                if (x < 0 || x >= m || y < 0 || y >= n || matrix[x][y] <= matrix[i][j] + 1) {
                    continue;
                }
                matrix[x][y] = matrix[i][j] + 1;
                queue.offer(new int[]{x, y});
            }
        }
        return matrix;
    }
}