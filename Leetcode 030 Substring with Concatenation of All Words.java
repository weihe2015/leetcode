/**
You are given a string, s, and a list of words, words, that are all of the same length. 
Find all starting indices of substring(s) in s that is a concatenation of each word in words exactly once and without any intervening characters.

Example 1:

Input:
  s = "barfoothefoobarman",
  words = ["foo","bar"]
Output: [0,9]
Explanation: Substrings starting at index 0 and 9 are "barfoo" and "foobar" respectively.
The output order does not matter, returning [9,0] is fine too.
Example 2:

Input:
  s = "wordgoodbestgoodword",
  words = ["word","best"]
Output: []
*/

public class Solution {
    // O(KN)
    public List<Integer> findSubstring(String s, String[] words) {
        List<Integer> result = new ArrayList<Integer>();
        if (s == null || words == null || words.length == 0) {
            return result;
        }
        int wsLen = words.length, wLen = words[0].length(), sLen = s.length();
        // Condition: list of words, words, that are all of the same length
        // If string s length() is less than the total length of words, 
        // then it does not contains substring containing all word in words.
        if (sLen < wLen * wsLen) {
            return result;
        }
        // Use HashMap to store occurance of each word
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            if (map.containsKey(word)) {
                map.put(word, map.get(word)+1);
            }
            else {
                map.put(word, 1);
            }
        }
        // iterate all character in words[0]
        for (int i = 0; i < wLen; i++) {
            int l = i, r = i, count = 0;
            Map<String, Integer> currMap = new HashMap<>();
            while (r <= sLen - wLen) {
                // substring starts from r, with len wLen:
                String str = s.substring(r, r+wLen);
                if (map.containsKey(str)) {
                    // Save substr into currMap:
                    if (currMap.containsKey(str)) {
                        currMap.put(str, currMap.get(str)+1);
                    }
                    else {
                        currMap.put(str, 1);
                    }
                    // handle multiple same words in substring subStr
                    while (currMap.get(str) > map.get(str)) {
                        String tmp = s.substring(l, l+wLen);
                        currMap.put(tmp, currMap.get(tmp) - 1);
                        count--;
                        l += wLen;
                    }
                    count++;
                    if (count == wsLen) {
                        result.add(l);
                    }
                    
                }
                else {
                    currMap = new HashMap<>();
                    count = 0;
                    l = r + wLen;
                }
                r += wLen;
            }
        }
        return result;
    }
}