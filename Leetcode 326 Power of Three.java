/**
Given an integer n, return true if it is a power of three. Otherwise, return false.

An integer n is a power of three, if there exists an integer x such that n == 3x.

Example 1:

Input: n = 27
Output: true
Example 2:

Input: n = 0
Output: false
Example 3:

Input: n = 9
Output: true
Example 4:

Input: n = 45
Output: false
 
Constraints:

-2^31 <= n <= 2^31 - 1

Follow up:
Could you do it without using any loop / recursion?
*/
public class Solution {
    // use loop
    // n = 4: false
    // n = 1: true
    // n = 3 => 1 true
    public boolean isPowerOfThree(int n) {
        if (n == 0) {
            return false;
        }
        while (n % 3 == 0) {
            n /= 3;
        }
        return n == 1;
    }

    // use Math
	public boolean isPowerOfThree2(int n) {
		int num = (int) Math.pow(3,19);
	    // 1162261467 is 3^19,  3^20 is bigger than int  
	    return n > 0 && num % n == 0;
	}
}