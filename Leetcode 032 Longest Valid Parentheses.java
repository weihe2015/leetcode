/**
 * Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.
Example 1:

Input: "(()"
Output: 2
Explanation: The longest valid parentheses substring is "()"
Example 2:

Input: ")()())"
Output: 4
Explanation: The longest valid parentheses substring is "()()"
 * */
public class Solution {
    // Naive Solution: For all substring, check if it is valid parthesis, if it is, update maxLen:
    // running Time Complexity: O(N^3)
    public int longestValidParentheses(String s) {
        int maxLen = 0;
        for (int i = 0, n = s.length(); i < n; i++) {
            for (int j = i; j < n; j++) {
                String substr = s.substring(i, j+1);
                if (isValid(substr)) {
                    maxLen = Math.max(maxLen, j-i+1);
                }
            }
        }
        return maxLen;
    }
    
    private boolean isValid(String s) {
        int count = 0;
        for (char c : s.toCharArray()) {
            if (c == '(') {
                count++;
            }
            else if (c == ')') {
                count--;
            }
            if (count < 0) {
                return false;
            }
        }
        return count == 0;
    }

    // Solution 2: Running Time Complexity: O(N), Space Complexity: O(N)
    /** 
     * Idea: 1. Create an empty stack and push -1 to it, to count substring(0..i)
     *       2. Initialize result as 0
     *       3. If the character is '(', push its index into stack
     *       4. if the character is ')', pop the item from stack.
     *         4.1 After popping the item, if the stack is not empty, 
     *             calculate the length of current index and the index on the top of stack, which is the beginning of the valid substring
     *         4.2 After popping the item, if the stack is empty, push current index as start index of the substring.
    */
    public int longestValidParentheses(String s) {
        Deque<Integer> stack = new ArrayDeque<Integer>();
        stack.push(-1);
        int maxLen = 0;
        for (int i = 0, n = s.length(); i < n; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                stack.push(i);
            }
            else if (c == ')') {
                stack.pop();
                // If the stack is not empty, it means the prefix string is still valid parthesis, keep counting.
                if (!stack.isEmpty()) {
                    int currLen = (i - stack.peek());
                    maxLen = Math.max(maxLen, currLen);
                }
                // If stack is empty(), it means the prefix string is not valid parthesis substring, reset the pointer.
                // push last right parthesis to stack.
                else {
                    stack.push(i);
                }
            }
        }
        return maxLen;
    }

    // Return the longest valid parenthese:
    public String longestValidParentheses(String s) {
        Stack<Integer> stack = new Stack<>();
        stack.push(-1);
        int start = -1;
        int end = -1;
        for (int i = 0, n = s.length(); i < n; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                stack.push(i);
            }
            else {
                stack.pop();
                if (!stack.isEmpty()) {
                    if (start == -1 || end - start < i - stack.peek()) {
                        start = stack.peek() + 1;
                        end = i + 1;
                    }
                }
                else {
                    stack.push(i);
                }
            }
        }
        if (start == -1) {
            return "";
        }
        else {
            return s.substring(start, end);
        }
    }

    // Solution 3: Running Time Complexity: O(N), Space Complexity: O(1)
    public int longestValidParentheses(String s) {
        int maxLen = 0;
        int start = -1;
        int depth = 0;
        // scan from left to right:
        for (int i = 0, n = s.length(); i < n; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                depth++;
            }
            else if (c == ')') {
                depth--;
                if (depth == 0) {
                    maxLen = Math.max(maxLen, i-start);
                }
                else if (depth < 0) {
                    start = i;
                    depth = 0;
                }
            }
        }
        
        depth = 0;
        start = s.length();
        // Scan from right to left:
        for (int i = s.length() - 1; i >= 0; i--) {
            char c = s.charAt(i);
            if (c == ')') {
                depth++;
            }
            else if (c == '(') {
                depth--;
                if (depth == 0) {
                    maxLen = Math.max(maxLen, start - i);
                }
                else if (depth < 0) {
                    depth = 0;
                    start = i;
                }
            }
        }
        return maxLen;
    }

    // Return the longest valid parenthese:
    public int longestValidParentheses(String s) {
        int n = s.length();
        int l = -1;
        int r = -1;
        int start = -1;
        
        int depth = 0;
        // scan from left to right, skip case of additional ). For additional (, it will be handled next part.
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                depth++;
            }
            else {
                depth--;
                if (depth == 0) {
                    if (l == -1 || r - l < i - start) {
                        l = start + 1;
                        r = i + 1;
                    }
                }
                // addition ), reset the start place to i:
                else if (depth < 0) {
                    start = i;
                    depth = 0;
                }
            }
        }
        
        depth = 0;
        int end = n;
        // Scan from right to left, skip case of additional (.
        for (int i = n-1; i >= 0; i--) {
            char c = s.charAt(i);
            if (c == ')') {
                depth++;
            }
            else {
                depth--;
                if (depth == 0) {
                    if (l == -1 || r - l < end - i) {
                        r = end;
                        l = i;
                    }
                }
                // additional (, reset the end place to i;
                else if (depth < 0) {
                    end = i;
                    depth = 0;
                }
            }
        }
        String res;
        if (l == -1) {
            res = "";
        }
        else {
            res = s.substring(l, r);
        }
        return res.length();
    }
}