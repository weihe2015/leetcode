/**
    Given a sorted array, 
    remove the duplicates in place such that each element appear only once and return the new length.
    Do not allocate extra space for another array, you must do this in place with constant memory.

    For example,
    Given input array nums = [1,1,2],
    Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively. 
    It doesnt matter what you leave beyond the new length.
*/
public class Solution {
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int idx = 1;
        for (int i = 1, max = nums.length; i < max; i++) {
            if (nums[i] != nums[i-1]) {
                nums[idx] = nums[i];
                idx++;
            }
        }
        return idx;
    }
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        if (nums.length <= 1) {
            return nums.length;
        }
        int i = 0, j = 1, idx = 1, n = nums.length;
        while (j < n) {
            if (nums[i] != nums[j]) {
                nums[idx] = nums[j];
                i = j;
                idx++;
            }
            j++;
        }
        return idx;
    }
}