/**
In English, we have a concept called root, which can be followed by some other words to form another longer word - let's call this word successor. For example, the root an, followed by other, which can form another word another.

Now, given a dictionary consisting of many roots and a sentence. You need to replace all the successor in the sentence with the root forming it. If a successor has many roots can form it, replace it with the root with the shortest length.

You need to output the sentence after the replacement.

Example 1:

Input: dict = ["cat", "bat", "rat"]
sentence = "the cattle was rattled by the battery"
Output: "the cat was rat by the bat"
*/

class Solution {
    public String replaceWords(List<String> dict, String sentence) {
        if (dict == null || dict.isEmpty()) {
            return "";
        }
        TrieNode root = new TrieNode();
        for (String word : dict) {
            addWord(root, word);
        }
        StringBuffer sb = new StringBuffer();
        String[] strs = sentence.split(" ");
        for (String word : strs) {
            searchWord(sb, root, word);
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }
    
    private void searchWord(StringBuffer sb, TrieNode curr, String word) {
        for (char c : word.toCharArray()) {
            // found:
            if (curr.isWord) {
                sb.append(curr.word).append(" ");
                return;
            }
            // not found:
            if (curr.next[c-'a'] == null) {
                sb.append(word).append(" ");
                return;
            }
            curr = curr.next[c-'a'];
        }
        if (curr.isWord) {
            sb.append(curr.word).append(" ");
        }
        else {
            sb.append(word).append(" ");
        }
    }
    
    private void addWord(TrieNode curr, String word) {
        for (char c : word.toCharArray()) {
            if (curr.next[c-'a'] == null) {
                curr.next[c-'a'] = new TrieNode();
            }
            curr = curr.next[c-'a'];
        }
        curr.isWord = true;
        curr.word = word;
    }
    
    class TrieNode {
        boolean isWord;
        String word;
        TrieNode[] next;
        public TrieNode() {
            this.isWord = false;
            this.word = "";
            this.next = new TrieNode[26];
        }
    }
}