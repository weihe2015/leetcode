/*
Given a sorted integer array where the range of elements are
[lower, upper] inclusive, return its missing ranges.

For example, given [0, 1, 3, 50, 75], lower = 0 and upper = 99,
return ["2", "4->49", "51->74", "76->99"].
*/
public class Solution {

	public List<String> findMissingRanges(int[] nums, int lo, int hi) {
	  	List<String> res = new ArrayList<>();

	  	// the start missing range we need to find
		int start = lo;
		for (int num : nums) {
			if (num == start) {
				start++;
			}
			else if (num > start) {
				int end = num-1;
				res.add(getRange(start, end));
				// to avoid case like [0, 2147483647], that num+1 is overflow.
				if (num == hi) {
					return res;
				}
				start = num+1;
			}
		}
		if (start <= hi) {
			res.add(getRange(start, hi));
		}
		return res;
	}

	public String getRange(int n1, int n2) {
		return (n1 == n2) ? String.valueOf(n1) : String.format("%d->%d", n1, n2);
	}
}