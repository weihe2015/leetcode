/*
Given an array A of 0s and 1s, we may change up to K values from 0 to 1.

Return the length of the longest (contiguous) subarray that contains only 1s.

Example 1:

Input: A = [1,1,1,0,0,0,1,1,1,1,0], K = 2
Output: 6
Explanation:
[1,1,1,0,0,1,1,1,1,1,1]
Bolded numbers were flipped from 0 to 1.  The longest subarray is underlined.
Example 2:

Input: A = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], K = 3
Output: 10
Explanation:
[0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
Bolded numbers were flipped from 0 to 1.  The longest subarray is underlined.
*/
public class Solution {
    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int longestOnes(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length, l = 0, r = 0, maxLen = 0;
        boolean canFlipZeroToOne = true;
        int[] count = {0,0};
        while (r < n) {
            if (canFlipZeroToOne) {
                int num = nums[r];
                count[num]++;
                if (count[0] == k+1) {
                    canFlipZeroToOne = false;
                }
                r++;
            }
            while (!canFlipZeroToOne) {
                int num = nums[l];
                count[num]--;
                if (count[0] == k) {
                    canFlipZeroToOne = true;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // Solution 2:
    public int longestOnes(int[] A, int K) {
        int n = A.length;
        int l = 0;
        int r = 0;
        int maxLen = 0;
        Queue<Integer> queue = new LinkedList<>();
        
        while (r < n) {
            if (A[r] == 0) {
                queue.offer(r);
            }
            if (queue.size() > K) {
                l = queue.poll() + 1;
            }
            r++;
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }
}