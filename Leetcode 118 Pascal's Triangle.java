/**
For example, given numRows = 5,
Return

[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
*/
public class Solution {
    // Running Time Complexity: O(N^2), Space Complexity: O(1)
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        if (numRows <= 0) {
            return result;
        }
        // let i and j starts from 1 will be easier.
        for (int i = 1; i <= numRows; i++) {
            List<Integer> row = new ArrayList<>();
            for (int j = 1; j <= i; j++) {
                if (j == 1 || j == i) {
                    row.add(1);
                }
                else {
                    int sum = result.get(i-2).get(j-2) + result.get(i-2).get(j-1);
                    row.add(sum);
                }
            }
            result.add(row);
        }
        return result;
    }
}