/**
Given an encoded string, return its decoded string.

The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times.
Note that k is guaranteed to be a positive integer.

You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.

Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k.
For example, there won't be input like 3a or 2[4].

Example 1:

Input: s = "3[a]2[bc]"
Output: "aaabcbc"
Example 2:

Input: s = "3[a2[c]]"
Output: "accaccacc"
Example 3:

Input: s = "2[abc]3[cd]ef"
Output: "abcabccdcdcdef"
Example 4:

Input: s = "abc3[cd]xyz"
Output: "abccdcdcdxyz"
*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {

    private static final Pattern BRACKET_PATTERN = Pattern.compile("([0-9]+)\\[([A-Za-z]+)\\]", Pattern.DOTALL);

    public String decodeString(String s) {
        Matcher matcher = BRACKET_PATTERN.matcher(s);
        while (matcher.find()) {
            StringBuffer sb = new StringBuffer();
            int pos = 0;
            while (matcher.find(pos)) {
                if (matcher.start() > pos) {
                    String content = s.substring(pos, matcher.start());
                    sb.append(content);
                }
                int num = Integer.parseInt(matcher.group(1));
                String base = matcher.group(2);
                sb.append(rebuildNumString(num, base));
                pos = matcher.end();
            }
            if (pos < s.length()) {
                String content = s.substring(pos);
                sb.append(content);
            }
            s = sb.toString();
            matcher = BRACKET_PATTERN.matcher(s);
        }
        return s;
    }

    private String rebuildNumString(int num, String base) {
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= num; i++) {
            sb.append(base);
        }
        return sb.toString();
    }
}