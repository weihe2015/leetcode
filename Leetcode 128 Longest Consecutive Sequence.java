/**
Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

For example,
Given [100, 4, 200, 1, 3, 2],
The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.

Your algorithm should run in O(n) complexity.
*/
public class Solution {
    // Solution 1: Union Find Solution:
    public int longestConsecutive(int[] nums) {
        int n = nums.length;
        UnionFind unionFind = new UnionFind(n);
        // Key -> nums[i], value -> index i
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < n; i++) {
            if (map.containsKey(nums[i])) {
                continue;
            }
            map.put(nums[i],i);
            if (map.containsKey(nums[i]+1)) {
                unionFind.union(i, map.get(nums[i]+1));
            }
            if (map.containsKey(nums[i]-1)) {
                unionFind.union(i, map.get(nums[i]-1));
            }
        }
        return unionFind.getMaxConnectedNumber();
    }
    
    class UnionFind {
        int[] id;
        int[] size;
        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }
        
        public int find(int idx) {
            while (idx != id[idx]) {
                id[idx] = id[id[idx]];
                idx = id[idx];
            }
            return idx;
        }
        
        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
        }
        
        public int getMaxConnectedNumber() {
            int maxResult = 0;
            for (int num : size) {
                maxResult = Math.max(maxResult, num);
            }
            return maxResult;
        }
    }

    // Solution 2: Sort Array and scan the whole array:
    public int longestConsecutive(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        Arrays.sort(nums);
        int maxResult = 0, count = 1;
        for (int i = 0; i < n-1; i++) {
            if (nums[i] == nums[i+1] - 1) {
                count++;
            }
            else if (nums[i] != nums[i+1]) {
                maxResult = Math.max(maxResult, count);
                count = 1;
            }
        }
        maxResult = Math.max(maxResult, count);
        return maxResult;
    }

    /**
    See if n - 1 and n + 1 exist in the map, 
    and if so, it means there is an existing sequence next to n. 
    Variables left and right will be the length of those two sequences, 
    while 0 means there is no sequence and n will be the boundary point later. 
    Store (left + right + 1) as the associated value to key n into the map.
    Use left and right to locate the other end of the sequences to the left and right of n respectively, 
    and replace the value with the new length.
    */
    public int longestConsecutive(int[] nums) {
        int maxResult = 0;
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int num : nums) {
            if (!map.containsKey(num)) {
                int left = 0;
                if (map.containsKey(num-1)) {
                    left = map.get(num-1);
                }
                int right = 0;
                if (map.containsKey(num+1)) {
                    right = map.get(num+1);
                }
                int len = left + right + 1;
                maxResult = Math.max(maxResult, len);
                map.put(num, len);
                map.put(num-left, len);
                map.put(num+right, len);
            }
        }
        return maxResult;
    }
}