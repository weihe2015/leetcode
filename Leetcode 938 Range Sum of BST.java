/**
Given the root node of a binary search tree, 
return the sum of values of all nodes with a value in the range [low, high].

Input: root = [10,5,15,3,7,null,18], low = 7, high = 15
Output: 32

Input: root = [10,5,15,3,7,13,18,1,null,6], low = 6, high = 10
Output: 23

Constraints:

The number of nodes in the tree is in the range [1, 2 * 104].
1 <= Node.val <= 105
1 <= low <= high <= 105
All Node.val are unique.

*/
public class Solution {
    // Iterative
    public int rangeSumBST(TreeNode root, int low, int high) {
        int total = 0;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = root;
        while (!stack.isEmpty() || curr != null) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if (low <= curr.val && curr.val <= high) {
                total += curr.val;
            }
            else if (curr.val > high) {
                break;
            }
            curr = curr.right;
        }
        return total;
    }

    // Recursive
    private int total;
    public int rangeSumBST(TreeNode root, int low, int high) {
        total = 0;
        helper(root, low, high);
        return total;
    }
    
    private void helper(TreeNode node, int low, int high) {
        if (node == null) {
            return;
        }
        helper(node.left, low, high);
        if (low <= node.val && node.val <= high) {
            total += node.val;
        }
        helper(node.right, low, high);
    }
}