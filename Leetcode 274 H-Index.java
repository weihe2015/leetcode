/**
Given an array of citations (each citation is a non-negative integer) of a researcher, write a function to compute the researcher's h-index.

According to the definition of h-index on Wikipedia: "A scientist has index h if h of his/her N papers have at least h citations each,
and the other N − h papers have no more than h citations each."

Example:

Input: citations = [3,0,6,1,5]
Output: 3
Explanation: [3,0,6,1,5] means the researcher has 5 papers in total and each of them had
             received 3, 0, 6, 1, 5 citations respectively.
             Since the researcher has 3 papers with at least 3 citations each and the remaining
             two with no more than 3 citations each, her h-index is 3.
Note: If there are several possible values for h, the maximum one is taken as the h-index.
*/
public class Solution {
    // Running Time Complexity: O(NlogN), Space Complexity: O(1)
    public int hIndex(int[] citations) {
        // sort array in descending order
        Arrays.sort(citations);
        reverse(citations);
        int n = citations.length;
        // 从前往后查找排序后的列表，直到某篇论文的序号大于该论文被引次数。
        for (int i = 0; i < n; i++) {
            if (i >= citations[i]) {
                return i;
            }
        }
        return n;
    }

    private void reverse(int[] nums) {
        int i = 0, j = nums.length-1;
        while (i < j) {
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // Solution 2: Use Bucket Sort
    /**
    https://leetcode.com/problems/h-index/discuss/70768/Java-bucket-sort-O(n)-solution-with-detail-explanation
    First, you may ask why bucket sort. Well, the h-index is defined as the number of papers with reference greater than the number.
    So assume n is the total number of papers, if we have n+1 buckets, number from 0 to n,
    then for any paper with reference corresponding to the index of the bucket, we increment the count for that bucket.

    Then we iterate from the back to the front of the buckets, whenever the total count exceeds the index of the bucket,
    meaning that we have the index number of papers that have reference greater than or equal to the index.
    Which will be our h-index result.

    The reason to scan from the end of the array is that we are looking for the greatest h-index.
    */
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int hIndex(int[] citations) {
        int n = citations.length;
        int[] buckets = new int[n+1];
        for (int citation : citations) {
            if (citation > n) {
                buckets[n]++;
            }
            else {
                buckets[citation]++;
            }
        }
        int total = 0;
        for (int max = buckets.length, i = max-1; i >= 0; i--) {
            total += buckets[i];
            if (total >= i) {
                return i;
            }
        }
        return -1;
    }
}