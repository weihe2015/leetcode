/**
Given a collection of integers that might contain duplicates, nums, return all possible subsets.

Note:
Elements in a subset must be in non-descending order.
The solution set must not contain duplicate subsets.
For example,
If nums = [1,2,2], a solution is:

[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]
*/
public class Solution {
    // Iterative
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        int n = nums.length;
        if(n == 0)
            return result;
        Arrays.sort(nums);
        result.add(new ArrayList<Integer>());
        for(int i = 0; i < n; i++){
            int dupCount = 0;
            while( i + 1 < n && nums[i] == nums[i+1] ){
                dupCount++;
                i++;
            }
            int size = result.size();
            for(int j = 0; j < size; j++){
                List<Integer> list = new ArrayList<Integer>(result.get(j));
                for(int k = 0; k <= dupCount; k++){
                    list.add(nums[i]);
                    // It will need to create a new arraylist for subList because it not, 
                    // it will change the one inside result list for subList.add(nums[i])
                    result.add(new ArrayList<Integer>(list));
                }
            }
        }
        return result;
    }

    // backtracking Running Time Complexity: O(2^N)
     public List<List<Integer>> subsetsWithDup2(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        int n = nums.length;
        if(n == 0)
            return result;
        Arrays.sort(nums);
        generateSubSetsWithDuplites(result,new ArrayList<Integer>(),nums,0);
        return result;
     }
     
     public void generateSubSetsWithDuplites(List<List<Integer>> result, List<Integer> list, int[] nums, int level){
         result.add(new ArrayList<Integer>(list));
         for(int i = level; i < nums.length; i++){
            if(i > level && nums[i-1] == nums[i]) {
                continue;
            }
            list.add(nums[i]);
            subset(result,list,nums,i+1);
            list.remove(list.size()-1);
         }
     }
}