/**
Given a square grid of integers arr, a falling path with non-zero shifts is a choice of exactly one element from each row of arr, such that no two elements chosen in adjacent rows are in the same column.

Return the minimum sum of a falling path with non-zero shifts.

Example 1:

Input: arr = [[1,2,3],[4,5,6],[7,8,9]]
Output: 13
Explanation: 
The possible falling paths are:
[1,5,9], [1,5,7], [1,6,7], [1,6,8],
[2,4,8], [2,4,9], [2,6,7], [2,6,8],
[3,4,8], [3,4,9], [3,5,7], [3,5,9]
The falling path with the smallest sum is [1,5,7], so the answer is 13.
*/
public class Solution {
    // Running Time Complexity: O(N^2)
    // https://leetcode-cn.com/problems/minimum-falling-path-sum-ii/solution/dong-tai-gui-hua-on2-suan-fa-by-npe_tle/
    public int minFallingPathSum(int[][] arr) {
        int n = arr.length;
        
        int[][] dp = new int[n][n];
        
        // 第一行每个元素的路径最小值就是数组元素本身
        for (int j = 0; j < n; j++) {
            dp[0][j] = arr[0][j];
        }
        
        // 记录上一行的每个位置除自己外的最小值
        int[] preRowMinArr = getPreviousRowMinArray(dp, 0, n);
        
        // 保存两个最小两个值
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < n; j++) {
                dp[i][j] = preRowMinArr[j] + arr[i][j];
            }
            preRowMinArr = getPreviousRowMinArray(dp, i, n);
        }
        
        int minVal = Integer.MAX_VALUE;
        for (int j = 0; j < n; j++) {
            minVal = Math.min(minVal, dp[n-1][j]);
        }
        return minVal;
    }
    
    private int[] getPreviousRowMinArray(int[][] dp, int i, int n) {
        // 除自己之外的最小值
        int[] minArr = new int[n];
        
        // 先求最小的两个数:
        int minIdx1 = -1;
        int minVal1 = Integer.MAX_VALUE;
        int minVal2 = Integer.MAX_VALUE;

        for (int j = 0; j < n; j++) {
            if (dp[i][j] < minVal1) {
                minVal2 = minVal1;
                minVal1 = dp[i][j];

                minIdx1 = j;
            }
            else if (minVal1 <= dp[i][j] && dp[i][j] < minVal2) {
                minVal2 = dp[i][j];
                minIdx2 = j;
            }
        }
        
        for (int j = 0; j < n; j++) {
            if (j == minIdx1) {
                minArr[j] = minVal2;
            }
            else {
                minArr[j] = minVal1;
            }
        }
        return minArr;
    }
}