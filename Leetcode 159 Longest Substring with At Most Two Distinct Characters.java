/*
Given a string s , find the length of the longest substring t  that contains at most 2 distinct characters.

Example 1:

Input: "eceba"
Output: 3
Explanation: t is "ece" which its length is 3.
Example 2:

Input: "ccaabbb"
Output: 5
Explanation: t is "aabbb" which its length is 5.
*/

public class solution {
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        int k = 2;
        int[] map = new int[256];
        // count: number of distinct characters 
        int count = 0, l = 0, r = 0, n = s.length(), maxLen = 0;
        while (r < n) {
            // while the window [l,r] satisfy the requirement: we continue expanding the r pointer:
            if (count <= k) {
                char rc = s.charAt(r);
                // We meet a new character, increment the count
                if (map[rc] == 0) {
                    count++;
                }
                map[rc]++;
                r++;
            }
            // while the window [l,r] does not satisfy the requirement, 
            // we shrink the window by moving l pointer to the right:
            while (count > k) {
                char lc = s.charAt(l);
                // At this time, after removing char lc, there is no more char lc in substring(l, r)
                // So we decrement count by 1
                if (map[lc] == 1) {
                    count--;
                }
                map[lc]--;
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

	public int lengthOfLongestSubstringTwoDistinct(String s) {
        int i = 0, j = -1;
        int maxLen = 0;
        for (int k = 1; k < s.length(); k++) {
            if (s.charAt(k) == s.charAt(k-1)) continue;
            if (j > -1 && s.charAt(k) != s.charAt(j)) {
                maxLen = Math.max(maxLen, k - i);
                i = j + 1;
            }
            j = k - 1;
        }
        return maxLen > (s.length() - i) ? maxLen : s.length() - i;
    }

    @Test
	public void LongestSubstringWithAtMostTwoDistinctCharactersTest1() throws Exception {
		String s1 = "eceba";
		Assert.assertEquals(3, lengthOfLongestSubstringTwoDistinct(s1));
		
		String s2 = "ccaabbb";
		Assert.assertEquals(5, lengthOfLongestSubstringTwoDistinct(s2));
	}
}