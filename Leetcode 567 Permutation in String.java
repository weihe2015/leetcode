/**
Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1.
In other words, one of the first string's permutations is the substring of the second string.
Example 1:
Input:s1 = "ab" s2 = "eidbaooo"
Output:True
Explanation: s2 contains one permutation of s1 ("ba").
Example 2:
Input:s1= "ab" s2 = "eidboaoo"
Output: False
*/
public class Solution {
    // Running Time Complexity: O(n), where n = s2.length();, Space Complexity: O(1)
    public boolean checkInclusion(String s1, String s2) {
        if (s2 == null || s2.length() == 0) {
            return false;
        }
        int[] map = new int[256];
        for (char c : s1.toCharArray()) {
            map[c]++;
        }
        int l = 0, r = 0, n = s2.length(), count = s1.length();
        while (r < n) {
            if (count > 0) {
                char rc = s2.charAt(r);
                map[rc]--;
                if (map[rc] >= 0) {
                    count--;
                }
                r++;
            }
            while (count == 0) {
                char lc = s2.charAt(l);
                map[lc]++;
                if (map[lc] == 1) {
                    count++;
                    if (r - l == s1.length()) {
                        return true;
                    }
                }
                l++;
            }
        }
        return false;
    }
}