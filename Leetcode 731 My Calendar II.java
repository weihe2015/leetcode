/**
Implement a MyCalendarTwo class to store your events. A new event can be added if adding the event will not cause a triple booking.

Your class will have one method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

A triple booking happens when three events have some non-empty intersection (ie., there is some time that is common to all 3 events.)

For each call to the method MyCalendar.book, return true if the event can be added to the calendar successfully without causing a triple booking. Otherwise, return false and do not add the event to the calendar.

Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
Example 1:

MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(50, 60); // returns true
MyCalendar.book(10, 40); // returns true
MyCalendar.book(5, 15); // returns false
MyCalendar.book(5, 10); // returns true
MyCalendar.book(25, 55); // returns true
Explanation: 
The first two events can be booked.  The third event can be double booked.
The fourth event (5, 15) can't be booked, because it would result in a triple booking.
The fifth event (5, 10) can be booked, as it does not use time 10 which is already double booked.
The sixth event (25, 55) can be booked, as the time in [25, 40) will be double booked with the third event;
the time [40, 50) will be single booked, and the time [50, 55) will be double booked with the second event.
 
Note:

The number of calls to MyCalendar.book per test case will be at most 1000.
In calls to MyCalendar.book(start, end), start and end are integers in the range [0, 10^9].
*/
public class MyCalendarTwo {

    /**
    考虑使用Map，只存储每个时间片的起始和终止位置，以及其对应的占用次数，以达成节省内存的目的：Map的key为时间点，
    key对应的value为从此时间点开始向后，直到下一个key为止的所有时间点上的占用次数
    
    1. 使用一个TreeMap维护时间片的占用次数，例如TreeMap(0=>0, 5=>1, 15=>0)就代表[5,15)区间内的占用次数为1
    2. 当有book请求时，找到小于等于start的第一个key对应的value，即为start时间点上目前的占用次数 freq
       如果oc==2，很明显这个book是不成功的；如oc<2，则向start时间点上插入value=oc+1
    3. 遍历(start, end)区间内的所有key，依次更新其对应的value为value+1，如果范围内某个value==2，那么book也是失败的, 需要将第2和第3步执行过的所有插入操作回滚
    4. 最后，向end时间点上插入value，值为第3步中最后一个遍历到的value-1，此时应注意，如果TreeMap中end时间点上已经有值，说明已经有其他时间片在end处结束，此时不需要更新end处的value

    */
    // Running Time Complexity: 最佳时间复杂度可达到O(NlogN)O(NlogN)（每次在TreeMap中查找的复杂度是O(logN)O(logN)），效率高于官方题解
    private TreeMap<Integer, Integer> map;
    
    public MyCalendarTwo() {
        this.map = new TreeMap<>();
        map.put(0, 0);
    }
    
    public boolean book(int start, int end) {
        int k = 2;
        Map.Entry<Integer, Integer> entry = map.floorEntry(start);
        if (entry.getValue() >= k) {
            return false;
        }
        Map<Integer, Integer> tmpMap = new HashMap<>();
        int newFreq = entry.getValue() + 1;
        tmpMap.put(start, newFreq);
        
        //遍历并更新(start,end)范围内的key
        entry = map.ceilingEntry(start + 1);
        while (entry != null && entry.getKey() < end) {
            if (entry.getValue() >= 2) {
                return false;
            }
            newFreq = entry.getValue() + 1;
            tmpMap.put(entry.getKey(), newFreq);
            entry = map.ceilingEntry(entry.getKey() + 1);
        }
        
        // 如果end处无entry，插入end对应的value为k-1，即上一步遍历到的最后一个key对应的value-1
        if (entry == null || entry.getKey() != end) {
            tmpMap.put(end, newFreq-1);
        }
        map.putAll(tmpMap);
        return true;
    }
}

/**
 * Your MyCalendarTwo object will be instantiated and called as such:
 * MyCalendarTwo obj = new MyCalendarTwo();
 * boolean param_1 = obj.book(start,end);
 */