/**
On a staircase, the i-th step has some non-negative cost cost[i] assigned (0 indexed).

Once you pay the cost, you can either climb one or two steps. You need to find minimum cost to reach the top of the floor, 
and you can either start from the step with index 0, or the step with index 1.

Example 1:
Input: cost = [10, 15, 20]
Output: 15
Explanation: Cheapest is start on cost[1], pay that cost and go to the top.
Example 2:
Input: cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
Output: 6
Explanation: Cheapest is start on cost[0], and only step on 1s, skipping cost[3].
Note:
cost will have a length in the range [2, 1000].
Every cost[i] will be an integer in the range [0, 999].
*/
public class Solution {
    // Running Time complexity: O(n), Space Complexity: O(n)
    public int minCostClimbingStairs(int[] cost) {
        if (cost == null || cost.length == 0) {
            return 0;
        }
        int n = cost.length;
        int[] dp = new int[n];
        dp[0] = cost[0];
        dp[1] = cost[1];
        for (int i = 2; i < n; i++) {
            dp[i] = cost[i] + Math.min(dp[i-1], dp[i-2]);
        }
        return Math.min(dp[n-2], dp[n-1]);
    }

    // Running Time complexity: O(n), Space Complexity: O(1)
    public int minCostClimbingStairs(int[] cost) {
        if (cost == null || cost.length == 0) {
            return 0;
        }
        int n = cost.length;
        int minCost1 = cost[0]; // 2 steps away from current step.
        int minCost2 = cost[1]; // 1 steps away from current step.
        int minCost = 0;
        for (int i = 2; i < n; i++) {
            minCost = Math.min(minCost1, minCost2) + cost[i];
            // previous one step of current step = 2 steps from next step.
            minCost1 = minCost2;
            // current cost of this step = one step from next step
            minCost2 = minCost;
        }
        return Math.min(minCost1, minCost2);
    }
}