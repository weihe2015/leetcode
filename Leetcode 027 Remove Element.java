/**
    Given input array nums = [3,2,2,3], val = 3
    Your function should return length = 2, with the first two elements of nums being 2.

    Example 1:
    Given nums = [3,2,2,3], val = 3,
    Your function should return length = 2, with the first two elements of nums being 2.
    It doesn't matter what you leave beyond the returned length.
    Example 2:
    Given nums = [0,1,2,2,3,0,4,2], val = 2,
    Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.
    Note that the order of those five elements can be arbitrary.
    It doesn't matter what values are set beyond the returned length.
*/

public class Solution {
	// Running time: O(n), Space Complexity: O(1)
    public int removeElement(int[] nums, int val) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int idx = 0;
        for (int i = 0, max = nums.length; i < max; i++) {
            if (nums[i] != val) {
                nums[idx] = nums[i];
                idx++;
            }
        }
        return idx;
    }

    // Use additional space:
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int removeElement(int[] nums, int val) {
        // not in place:
        if (nums == null || nums.length == 0) {
            return 0;
        }
        List<Integer> list = new ArrayList<>();
        for (int num : nums) {
            if (num != val) {
                list.add(num);
            }
        }
        int max = list.size();
        for (int i = 0; i < max; i++) {
            nums[i] = list.get(i);
        }
        return max;
    }
}