/**
Given a string s of '(' , ')' and lowercase English characters. 

Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) 
so that the resulting parentheses string is valid and return any valid string.

Formally, a parentheses string is valid if and only if:

It is the empty string, contains only lowercase characters, or
It can be written as AB (A concatenated with B), where A and B are valid strings, or
It can be written as (A), where A is a valid string.
 
Example 1:

Input: s = "lee(t(c)o)de)"
Output: "lee(t(c)o)de"
Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.
Example 2:

Input: s = "a)b(c)d"
Output: "ab(c)d"
Example 3:

Input: s = "))(("
Output: ""
Explanation: An empty string is also valid.
Example 4:

Input: s = "(a(b(c)d)"
Output: "a(b(c)d)"

Constraints:

1 <= s.length <= 10^5
s[i] is one of  '(' , ')' and lowercase English letters.
*/
public class Solution {
    /**
    遇到一对括号就弹出栈；否则将括号加入；
    记录多余的括号索引位置，然后从后往前删除多余的括号位置
    */
    public String minRemoveToMakeValid(String s) {
        Stack<Integer> stack = new Stack<>();
        StringBuffer sb = new StringBuffer(s);
        
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c == ')' && !stack.isEmpty() && s.charAt(stack.peek()) == '(') {
                stack.pop();
            }
            else if (c == '(' || c == ')') {
                stack.push(i);
            }
        }
        
        while (!stack.isEmpty()) {
            int idx = stack.pop();
            sb.deleteCharAt(idx);
        }
        return sb.toString();
    }

    // Solution 2: use array to record valid positions
    public String minRemoveToMakeValid(String s) {
        int n = s.length();
        Stack<Integer> stack = new Stack<>();
        boolean[] valid = new boolean[n];
        Arrays.fill(valid, true);

        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                stack.push(i);
                valid[i] = false;
            }
            else if (c == ')') {
                if (stack.isEmpty()) {
                    valid[i] = false;
                }
                else {
                    int openIdx = stack.pop();
                    valid[openIdx] = true;
                }
            }
        }
        
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < n; i++) {
            if (valid[i]) {
                sb.append(s.charAt(i));
            }
        }
        return sb.toString();
    }
}