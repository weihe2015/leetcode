/**
 * Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
For example, given n = 3, a solution set is:
["((()))","(()())","(())()","()(())","()()()"]
 * 
*/
public class Solution {
    public List<String> generateParenthesis(int n) {
        List<String> res = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        generateParenthesis(res, 0, 0, n, sb);
        return res;
    }
    
    private void generateParenthesis(List<String> res, int open, int close, int n, StringBuffer sb) {
        if (sb.length() == n * 2) {
            res.add(sb.toString());
            return;
        }
        if (open < n) {
            sb.append('(');
            generateParenthesis(res, open+1, close, n, sb);
            sb.setLength(sb.length()-1);
        }
        if (open > close) {
            sb.append(')');
            generateParenthesis(res, open, close+1, n, sb);
            sb.setLength(sb.length()-1);
        }
    }

    // use StringBuffer: Time Complexity -  n * Catalan number (n) ~ O(4n)， Space Complexity O(n^2)
    public List<String> generateParenthesis(int n) {
        List<String> res = new ArrayList<>();
        dfs(res, n, n, new StringBuffer());
        return res;
    }
    
    private void dfs(List<String> res, int open, int close, StringBuffer sb) {
        if (open == 0 && close == 0) {
            res.add(sb.toString());
            return;
        }
        if (open > 0) {
            sb.append('(');
            dfs(res, open-1, close, sb);
            sb.setLength(sb.length()-1);
        }
        if (open < close) {
            sb.append(')');
            dfs(res, open, close-1, sb);
            sb.setLength(sb.length()-1);
        }
    }
}