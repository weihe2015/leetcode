/**
Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.

'.' Matches any single character.
'*' Matches zero or more of the preceding element.
The matching should cover the entire input string (not partial).

Note:

s could be empty and contains only lowercase letters a-z.
p could be empty and contains only lowercase letters a-z, and characters like . or *.
Example 1:

Input:
s = "aa"
p = "a"
Output: false
Explanation: "a" does not match the entire string "aa".
Example 2:

Input:
s = "aa"
p = "a*"
Output: true
Explanation: '*' means zero or more of the precedeng element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
Example 3:

Input:
s = "ab"
p = ".*"
Output: true
Explanation: ".*" means "zero or more (*) of any character (.)".
Example 4:

Input:
s = "aab"
p = "c*a*b"
Output: true
Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore it matches "aab".
Example 5:

Input:
s = "mississippi"
p = "mis*is*p*."
Output: false
*/

dp[i][j] = s[0...i-1] & p[0...j-1]
dp[i-1][j-1] prev
*:
dp[i][j-2] match 0
dp[i-1][j] match 1 or more

// https://leetcode.com/problems/regular-expression-matching
public class Solution {
    // dp solution
    /* dp[i][j] means s.substring(0,i), p.substring(0,j) is match.
    Initialization:
       for regular expression string p, check if there is an empty string match like a*b*
          for each j = 1 ... n
             if p.charAt(j-1) == '*' // treat a* as empty match.
                dp[0][j] = dp[0][j-2]
    Recurrence Relation:
        for each i = 1 ... m
           for each j = 1 ... n
                if s.charAt(i-1) == p.charAt(j-1)
                   dp[i][j] = dp[i-1][j-1];
                if p.charAt(j-1) == '.'
                   dp[i][j] = dp[i-1][j-1];
                if p.charAt(j-1) == '*'
                      if s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.':
                         dp[i][j] = dp[i-1][j] // in this case: a* count as multiple a
                                               // dp[i-1][j]: match previous character with p.substring(0,j) regex
                                               // Ex: s: aab, p: a*b, j = 2, i = 2
                                 or dp[i][j-1] // in this case: a* count as single a
                                 or dp[i][j-2] // in this case: a* count as empty
                      if p.charAt(j-2) != '.' && s.charAt(i-1) != p.charAt(j-2):
                         in this case: a* or .* count as empty:
                         dp[i][j] = dp[i][j-2]

    */
    // Running Time Complexity: O(m*n), Space Complexity: O(m*n)
    public boolean isMatch(String s, String p) {
        int m = s.length();
        int n = p.length();
        boolean[][] dp = new boolean[m+1][n+1];
        // empty regex matches empty string:
        dp[0][0] = true;
        // scan the regex string and initialize the * match with 2 characters before:
        // s = "", p = a*  ba* = b
        // s = "", p = a?  ba? = b
        for (int j = 1; j <= n; j++) {
            // j > 1 in case index out of bound
            if (j > 1 && p.charAt(j-1) == '*') {
                dp[0][j] = dp[0][j-2];
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if two characters from s and p at same index are matching,
                // or current regex character is .
                if (s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) == '.') {
                    dp[i][j] = dp[i-1][j-1];
                }
                // if current regex character is *:
                else if (j > 1 && p.charAt(j-1) == '*') {
                    // assume regex string is always valid, no * at the beginning:
                    // the character before * is a .
                    // or current character from s matches character before *:
                    if (s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.') {
                        // dp[i-1][j]: match previous character with p.substring(0,j) regex
                        // dp[i][j-1]: a* or .* matches empty string
                        dp[i][j] = dp[i-1][j] || dp[i][j-2];
                        //dp[i][j] = dp[i-1][j] || dp[i][j-1] || dp[i][j-2];
                    }
                    // in this case: a* or .* count as empty:
                    else {
                        dp[i][j] = dp[i][j-2];
                    }
                }
                else {
                    dp[i][j] = false;
                }
            }
        }
        return dp[m][n];
    }

    // Follow up: add + and ?
    /* dp[i][j] means s.substring(0,i), p.substring(0,j) is match.
    Initialization:
       for regular expression string p, check if there is an empty string match like a*b*
          for each j = 1 ... n
             if p.charAt(j-1) == '*' // treat a* as empty match.
                dp[0][j] = dp[0][j-2]
    Recurrence Relation:
        for each i = 1 ... m
           for each j = 1 ... n
                if s.charAt(i-1) == p.charAt(j-1)
                   dp[i][j] = dp[i-1][j-1];
                if p.charAt(j-1) == '.'
                   dp[i][j] = dp[i-1][j-1];
                else if p.charAt(j-1) == '*'
                      if s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.' :
                         dp[i][j] = dp[i-1][j] // in this case: a* count as multiple a
                                               // dp[i-1][j]: match previous character with p.substring(0,j) regex
                                 or dp[i][j-1] // in this case: a* count as single a
                                 or dp[i][j-2] // in this case: a* count as empty
                      if p.charAt(j-2) != '.' && s.charAt(i-1) != p.charAt(j-2):
                         in this case: a* or .* count as empty:
                         dp[i][j] = dp[i][j-2]
                else if p.charAt(j-1) == '+'
                      if s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.' :
                         dp[i][j] = dp[i-1][j] // in this case: a+ count as multiple a
                                 or dp[i][j-1] // in this case: a+ count as single a
                      else
                         dp[i][j] = false;
                else if p.charAt(j-1) == '?'
                      if s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.' :
                         dp[i][j] = dp[i][j-2] // in this case: a? count as empty
                                 or dp[i][j-1] // in this case: a? count as single a
                      else
                         in this case: a? count as empty:
                         dp[i][j] = dp[i][j-2]
                else
                    dp[i][j] = false

    */

    // follow up: add + and ?
    // Running Time Complexity: O(m*n), Space Complexity: O(m*n)
    public boolean isMatch(String s, String p) {
        int m = s.length();
        int n = p.length();
        boolean[][] dp = new boolean[m+1][n+1];
        // empty regex matches empty string:
        dp[0][0] = true;
        // scan the regex string and initialize the * match with 2 characters before:
        // s = "", p = a*  ba* = b
        // s = "", p = a?  ba? = b
        for (int j = 1; j <= n; j++) {
            if (j > 1 && (p.charAt(j-1) == '*' || p.charAt(j-1) == '?')) {
                dp[0][j] = dp[0][j-2];
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if two characters from s and p at same index are matching,
                // or current regex character is .
                if (s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) == '.') {
                    dp[i][j] = dp[i-1][j-1];
                }
                // if current regex character is *:
                else if (j > 1 && p.charAt(j-1) == '*') {
                    // assume regex string is always valid, no * at the beginning:
                    // the character before * is a .
                    // or current character from s matches character before *:
                    if (s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.') {
                        // dp[i-1][j]: match previous character with p.substring(0,j) regex
                        dp[i][j] = dp[i-1][j] || dp[i][j-1] || dp[i][j-2];
                    }
                    // in this case: a* or .* count as empty:
                    else {
                        dp[i][j] = dp[i][j-2];
                    }
                }
                else if (p.charAt(j-1) == '+') {
                    // in this case: a+ count as multiple a or single a:
                    if (s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.') {
                        dp[i][j] = dp[i-1][j] || dp[i][j-1];
                    }
                    else {
                        dp[i][j] = false;
                    }
                }
                else if (p.charAt(j-1) == '?') {
                    if (s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.') {
                        dp[i][j] = dp[i][j-1] || dp[i][j-2];
                    }
                    // in this case: a? or .? count as empty:
                    else {
                        dp[i][j] = dp[i][j-2];
                    }
                }
                else {
                    dp[i][j] = false;
                }
            }
        }
        return dp[m][n];
    }

    /** 
        Recursive Solution:
        Running Time Complexity: O(m*n)
     */
    public boolean isMatch(String s, String p) {
        int m = s.length();
        int n = p.length();
        return isMatch(s, p, 0, 0, m, n);
    }

    public boolean isMatch(String s, String p, int i, int j, int m, int n) {
        if (i == m && j == n) {
            return true;
        }
        if (j == n) {
            return false;
        }
        if (j < n-1 && p.charAt(j+1) == '*') {
            // first or condition, treat a* as empty match
            // second or condition, treat a* as multiple match. 
            return isMatch(s, p, i, j+2, m, n) || isMatchCharacter(s, p, i, j, m) && isMatch(s, p, i+1, j, m, n);
        }
        else {
            // If current character is match, we will check if next character is match or not.
            return isMatchCharacter(s, p, i, j, m) && isMatch(s, p, i+1, j+1, m, n);
        }
    }

    private boolean isMatchCharacter(String s, String p, int i, int j, int m) {
        return i < m && (s.charAt(i) == p.charAt(j) || p.charAt(j) == '.');
    }

    // Another Solution without using DP:
    public boolean isMatch(String s, String p) {
        for (int i = 0; i < p.length(); s = s.substring(1)) {
            char c = p.charAt(i);
            if (i + 1 >= p.length() || p.charAt(i + 1) != '*') {
                i++;
            }
                
            else if (isMatch(s, p.substring(i + 2))) {
                return true;
            } 
            if (s.isEmpty() || (c != '.' && c != s.charAt(0))) {
                return false;
            }
        }
        return s.isEmpty();
    }
}