Given a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list.

For example,
Given 1->2->3->3->4->4->5, return 1->2->5.
Given 1->1->1->2->3, return 2->3.

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode newHead = new ListNode(0), prev = newHead, curr = head;
        newHead.next = head;
        boolean found = false;
        while (curr != null && curr.next != null) {
            if (curr.val == curr.next.val) {
                found = true;
            }
            else {
                if (found) {
                    prev.next = curr.next;
                    found = false;
                }
                else {
                    prev = prev.next;
                }
            }
            curr = curr.next;
        }
        // handle [1,1,1] cases
        if (found && curr != null) {
            prev.next = curr.next;
        }
        return newHead.next;
    }

    public ListNode deleteDuplicates(ListNode head) {
        ListNode newHead = new ListNode(0), prev = newHead, curr = head;
        newHead.next = head;
        while (curr != null && curr.next != null) {
            while (curr.next != null && curr.val == curr.next.val) {
                curr = curr.next;
            }
            // If prev next node is not curr, which means it has duplicates, set prev.next = curr.next
            // Else, move prev one step forward
            if (prev.next == curr) {
                prev = prev.next;
            }
            else {
                prev.next = curr.next;
            }
            curr = curr.next;
        }
        return newHead.next;
    }
}