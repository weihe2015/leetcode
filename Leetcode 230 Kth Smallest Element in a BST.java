/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {

    // Recursive: Use Inorder Traversal:
    public int kthSmallest(TreeNode root, int k) {
        List<Integer> list = new ArrayList<Integer>();
        inorderTraversal(list, root);
        return list.get(k-1);
    }

    private void inorderTraversal(List<Integer>list, TreeNode node) {
        if (node == null) {
            return;
        }
        inorderTraversal(list, node.left);
        list.add(node.val);
        inorderTraversal(list, node.right);
    }
    // Second solution for Recursive In order Traversal.
    private int count = 0, num = 0;
    public int kthSmallest(TreeNode root, int k) {
        List<Integer> list = new ArrayList<Integer>();
        inorderTraversal(root, k);
        return num;
    }

    private void inorderTraversal(TreeNode node, int k) {
        if (node == null || count >= k) {
            return;
        }
        inorderTraversal(node.left, k);
        count++;
        if (count == k) {
            num = node.val;
            return;
        }
        inorderTraversal(node.right, k);
    }

    // Iterative: Use Inorder Traversal
    public int kthSmallest(TreeNode root, int k) {
        int res = 0;
        if (root == null) {
            return res;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode node = root;
        int num = 1;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode curr = stack.pop();
            if (num == k) {
                res = curr.val;
                break;
            }
            num++;
            node = curr.right;
        }
        return res;
    }

    // Follow up:
    // What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently?
    // How would you optimize the kthSmallest routine?
    /**
     * Answer: add rank field inside the TreeNode class, which represents number of nodes smaller than this node.
     *   Update this rank field when we construct the tree.
     * Then we can use search method in BST to find this kth smallest node, O(h) time
    */
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        int rank;
        public TreeNode(int x) {
            val = x;
        }
    }

    public int kthSmallest(TreeNode root, int k) {
        if (root == null) {
            return;
        }
        TreeNode curr = root;
        while (curr != null) {
            if (curr.rank + 1 == k) {
                return curr;
            }
            else if (curr.rank + 1 < k) {
                curr = curr.right;
            }
            else {
                curr = curr.left;
            }
        }
    }
}