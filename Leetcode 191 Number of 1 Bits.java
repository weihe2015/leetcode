Write a function that takes an unsigned integer and returns the number of ’1' bits it has (also known as the Hamming weight).

For example, the 32-bit integer ’11' has binary representation 00000000000000000000000000001011, so the function should return 3.

public class Solution {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int num = 0;
        String binary = Integer.toBinaryString(n);
        for(char c : binary.toCharArray()){
            if(c == '1')
                num++;
        }
        return num;
    }
    public int hammingWeight(int n) {
        // The unsigned right shift operator ">>>" shifts a zero into the leftmost position, 
        // while the leftmost position after ">>" depends on sign extension.
        int num = 0;
        while(n != 0){
            num += n & 1;
            n >>>= 1;
        }
        return num;
    }
}