    /**
    Leetcode 269 Alien Dictionary:
    https://leetcode.com/problems/alien-dictionary
    https://www.lintcode.com/problem/alien-dictionary/description
    There is a new alien language which uses the latin alphabet. 
    However, the order among letters are unknown to you. 
    You receive a list of non-empty words from the dictionary, 
    where words are sorted lexicographically by the rules of this new language. 
    Derive the order of letters in this language.

    Example 1:

    Input:
    [
    "wrt",
    "wrf",
    "er",
    "ett",
    "rftt"
    ]

    Output: "wertf"
    Example 2:

    Input:
    [
    "z",
    "x"
    ]

    Output: "zx"
    Example 3:

    Input:
    [
    "z",
    "x",
    "z"
    ] 

    Output: "" 
    Explanation: The order is invalid, so return "".
*/
public class Solution {
    /**
     * @param words: a list of words
     * @return: a string which is correct order
     */
    /**
    https://www.cnblogs.com/grandyang/p/5250200.html

    Directed Graph Traversal: 
    t->f
    w->e
    r->t
    e->r

    These are edges in the directed graph. Topological Sorting, indegree = 0 first, BFS
    
    根据之前讲解，需用 TreeSet 来保存这些 pair，还需要一个 HashSet 来保存所有出现过的字母，需要一个一维数组 in 来保存每个字母的入度，另外还要一个 queue 来辅助拓扑遍历，我们先遍历单词集，把所有字母先存入 HashSet，然后我们每两个相邻的单词比较，找出顺序 pair，然后根据这些 pair 来赋度，把 HashSet 中入度为0的字母都排入 queue 中，然后开始遍历，如果字母在 TreeSet 中存在，则将其 pair 中对应的字母的入度减1，若此时入度减为0了，则将对应的字母排入 queue 中并且加入结果 res 中，直到遍历完成，看结果 res 和 ch 中的元素个数是否相同，若不相同则说明可能有环存在，返回空字符串
    */
    public String alienOrder(String[] words) {
        // build graph
        int[] indegree = new int[26];

        Map<Character, Set<Character>> graphMap = new HashMap<>();
        for (String word : words) {
            for (char c : word.toCharArray()) {
                if (!graphMap.containsKey(c)) {
                    graphMap.put(c, new HashSet<>());
                }
            }
        }
    
        for (int i = 1, n = words.length; i < n; i++) {
            String word1 = words[i-1];
            String word2 = words[i];
            int minLen = Math.min(word1.length(), word2.length());
            
            for (int j = 0; j < minLen; j++) {
                char c1 = word1.charAt(j);
                char c2 = word2.charAt(j);
                if (c1 != c2) {
                    if (!graphMap.get(c1).contains(c2)) {
                        indegree[c2-'a']++;
                        graphMap.get(c1).add(c2);
                    }
                    break;
                }
            }
        }

        StringBuffer sb = new StringBuffer();
        Queue<Character> queue = new LinkedList<>();
        for (char c : graphMap.keySet()) {
            if (indegree[c-'a'] == 0) {
                queue.offer(c);
            }
        }

        while (!queue.isEmpty()) {
            char c = queue.poll();
            sb.append(c);
            if (!graphMap.containsKey(c)) {
                continue;
            }
            Set<Character> neighbors = graphMap.get(c);
            for (char neighbor : neighbors) {
                indegree[neighbor-'a']--;
                if (indegree[neighbor-'a'] == 0) {
                    queue.offer(neighbor);
                }
            }
        }
        return sb.length() == graphMap.size() ? sb.toString() : "";
    }
}