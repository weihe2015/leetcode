/**
Given a 2D board and a word, find if the word exists in the grid.

The word can be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once.

For example,
Given board =

[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]
word = "ABCCED", -> returns true,
word = "SEE", -> returns true,
word = "ABCB", -> returns false.
*/
public class Solution {
    // Running Time Complexity: O(m*n * 4^k), Space Complexity: O(m*n)
    // where k is the length of the string; mn for for loop and for the dfs method its 4^k.
    // Since the dfs method goes only as deep as the word length
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public boolean exist(char[][] board, String word) {
        if (board == null || board.length == 0) {
            return false;
        }
        int m = board.length, n = board[0].length, idx = 0;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (exist(board, word, i, j, m, n, visited, idx)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean exist(char[][] board, String word, int i, int j, int m, int n, boolean[][] visited, int idx) {
        if (index == word.length()) {
            return true;
        }
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j] || board[i][j] != word.charAt(idx)) {
            return false;
        }
        visited[i][j] = true;
        // scan four directions: visited its four direction, return true when one is true.
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            // only one true is enough
            if (exist(board, word, x, y, m, n, visited, idx+1)) {
                return true;
            }
        }
        // if four directions are false, then return false.
        visited[i][j] = false;
        return false;
    }

    // no additional space // Running Time Complexity: O(m*n * 4^k), Space Complexity: O(1)
    private static int[][] dirs2 = {{0,1},{0,-1},{1,0},{-1,0}};
    public boolean exist2(char[][] board, String word) {
        if (board == null || board.length == 0) {
            return false;
        }
        int m = board.length, n = board[0].length;
        char c = word.charAt(0);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == c && exist(board, word, i, j, m, n, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean exist(char[][] board, String word, int i, int j, int m, int n, int index) {
        if (index == word.length()) {
            return true;
        }
        char c = word.charAt(index);
        if (i < 0 || i >= m || j < 0 || j >= n || c != board[i][j]) {
            return false;
        }
        // mark the curr char in case going back
        board[i][j] = '*';
        // search for 4 directions
        for (int[] dir : dirs2) {
            int x = i + dir[0];
            int y = j + dir[1];
            boolean result = exist(board, word, x, y, m, n, index+1);
            // if either one path is success, then true
            if (result) {
                board[i][j] = c;
                return true;
            }
        }
        // If not found, meaning no path containing such word,
        // change the board character for next search
        board[i][j] = c;
        return false;
    }
}