/**
You are given an n x n 2D matrix representing an image.

Rotate the image by 90 degrees (clockwise).

*/
public class Solution {
    // Running Time Complexity: O(m*n), Space Complexity: O(1)
    /** 
    1,2,3     1,4,7     7,4,1
    4,5,6  => 2,5,8  => 8,5,2
    7,8,9     3,6,9     9,6,3
    
    */
    public void rotate(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return;
        }
        int m = matrix.length, n = matrix[0].length;
        // flip the matrix
        for (int i = 0; i < m; i++) {
            for (int j = i+1; j < n; j++) {
                swap(matrix, i, j);
            }
        }
        // reverse the row
        for (int i = 0; i < m; i++) {
            reverse(matrix[i], 0, n-1);
        }
    }

    private void swap(int[][] matrix, int i, int j) {
        int temp = matrix[i][j];
        matrix[i][j] = matrix[j][i];
        matrix[j][i] = temp;
    }
    
    private void reverse(int[] nums, int i, int j) {
        while (i < j) {
            int temp = nums[i];
            nums[i] = nums[j];
            nums[j] = temp;
            i++; 
            j--;
        }
    }

    // Solution 2:
    /** 
    1  2  3      1  4  7      7  4  1     
    4  5  6  =>  2  5  8  =>  8  5  2
    7  8  9      3  6  9      9  6  3 
    *//
    public void rotate(int[][] matrix) {
        int m = matrix.length;
        if(m == 0)
            return;
        int n = matrix[0].length;
        // swap symmetry
        for(int i = 0; i < m; i++){
            for(int j = i; j < n; j++){
                swap(matrix,i,j);
            }
        }
        // swap horizontally
        for(int i = 0; i < m; i++){
            for(int j = 0; j < m/2; j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[i][matrix.length-j-1];
                matrix[i][matrix.length-j-1] = temp;
            }
        }
    }
    
    public void swap(int[][] matrix, int i, int j){
        int temp = matrix[i][j];
        matrix[i][j] = matrix[j][i];
        matrix[j][i] = temp;
    }


   /* 
    * first reverse up to down, then swap the symmetry 
    * 1 2 3     7 8 9     7 4 1
    * 4 5 6  => 4 5 6  => 8 5 2
    * 7 8 9     1 2 3     9 6 3
    */
    public void rotate2(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return;
        }
        int m = matrix.length, n = matrix[0].length;
        // reverse the row from top to bottom
        for (int i = 0; i < m/2; ++i){
            int[] array = matrix[i];
            matrix[i] = matrix[m-1-i];
            matrix[m-1-i] = array;
        }
        
        // swap symmetry
        for (int i = 0; i < m; ++i){
            for (int j = i+1; j < n; ++j){
                swap(matrix, i, j);
            }
        }
    }
}