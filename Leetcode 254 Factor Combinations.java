/**
Numbers can be regarded as product of its factors. For example,

8 = 2 x 2 x 2;
  = 2 x 4.
Write a function that takes an integer n and return all possible combinations of its factors.

Note: 
Each combinations factors must be sorted ascending, for example: The factors of 2 and 6 is [2, 6], not [6, 2].
You may assume that n is always positive.
Factors should be greater than 1 and less than n.
Examples: 
input: 1
output: 
[]
input: 37
output: 
[]
input: 12
output:
[[2, 6],[2, 2, 3],[3, 4]]
input: 32
output:
[[2, 16],[2, 2, 8],[2, 2, 2, 4],[2, 2, 2, 2, 2],[2, 4, 4],[4, 8]]
*/
public class Solution {
    // Brute Force:
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> res = new ArrayList<>();
        if (n <= 1) {
            return res;
        }
        List<Integer> list = new ArrayList<>();
        getFactors(res, 2, n, 1, list);
        return res;
    }
    
    private void getFactors(List<List<Integer>> res, int idx, int n, int product, List<Integer> list) {
        if (product > n) {
            return;
        }
        if (product == n && list.size() > 1) {
            res.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = idx; i <= n; i++) {
            if (n % i != 0) {
                continue;
            }
            list.add(i);
            product *= i;
            getFactors(res, i, n, product, list);
            product /= i;
            list.remove(list.size()-1);
        }
    }

    // Solution 2:
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> res = new ArrayList<>();
        if (n <= 1) {
            return res;
        }
        List<Integer> list = new ArrayList<>();
        getFactors(res, 2, n, list);
        return res;
    }
    
    private void getFactors(List<List<Integer>> res, int idx, int n, List<Integer> list) {
        if (n == 1 && list.size() > 1) {
            res.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = idx; i <= n; i++) {
            if (n % i != 0) {
                continue;
            }
            list.add(i);
            getFactors(res, i, n/i, list);
            list.remove(list.size()-1);
        }
    }
}
