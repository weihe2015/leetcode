/*
Find the kth largest element in an unsorted array. Note that it is the kth largest element in the sorted order, not the kth distinct element.

Example 1:

Input: [3,2,1,5,6,4] and k = 2
Output: 5
Example 2:

Input: [3,2,3,1,2,4,5,5,6] and k = 4
Output: 4
Note: 
You may assume k is always valid, 1 ≤ k ≤ array's length.
*/

public class Solution {
    // Use quickSort to sort the array and find out the answer:
    // Running Time Complexity: O(NlogN), space complexity: O(1)
    public int findKthLargest(int[] nums, int k) {
        quickSort(nums, nums.length);
        return nums[nums.length - k];
    }
    
    private void quickSort(int[] nums, int N) {
        quickSort(nums, 0, N-1);
    }
    
    private void quickSort(int[] nums, int low, int high) {
        if (low >= high) {
            return;
        }
        int pivot = partition(nums, low, high);
        quickSort(nums, low, pivot-1);
        quickSort(nums, pivot+1, high);
    }
    // choose last element as pivot point:
    private int partition(int[] nums, int low, int high) {
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (nums[j] <= nums[pivot]) {
                swap(nums, i, j);
                i++;
            }
        }
        swap(nums, i, pivot);
        return i;
    }
    
    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // Solution 2: Use Priority Queue:
    // Running Time Complexity: O(NlogN), Space Complexity: O(N)
    public int findKthLargest(int[] nums, int k) {
        // max heap
        Queue<Integer> q = new PriorityQueue<>((n1, n2) -> n2 - n1);
        for (int num : nums) {
            q.offer(num);
        }
        while (k >= 2) {
            q.poll();
            k--;
        }
        return q.poll();
    }

    // Solution 3: Use Heapify Max Heap:
    // Running Time Complexity: O(NlogN + k * logK), Space Complexity: O(1)
    public int findKthLargest(int[] nums, int k) {
        int N = nums.length;
        // O(NlogN) to build a heap:
        // https://www.geeksforgeeks.org/time-complexity-of-building-a-heap/
        for (int i = N/2; i >= 0; i--) {
            heapify(nums, N, i);
        }
        for (int i = N-1; i > N-k; i--) {
            swap(nums, 0, i);
            heapify(nums, i, 0);
        }
        return nums[0];
    }
    
    private void heapify(int[] nums, int N, int i) {
        while (i < N) {
            int max = i;
            int left = 2 * i + 1;
            int right = 2 * i + 2;
            if (left < N && nums[left] > nums[max]) {
                max = left;
            }
            if (right < N && nums[right] > nums[max]) {
                max = right;
            }
            if (max == i) {
                break;
            }
            swap(nums, max, i);
            i = max;
        }
    }
    
    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

	//O(nlogn) time, O(1) time
    public int findKthLargest(int[] nums, int k) {
        Arrays.sort(nums);
        return nums[nums.length-k];
    }

    // Running Time Complexity: O(N), with worse case O(N^2) when you always pick the largest element as pivot point, when the array is sorted.
    public int findKthLargest(int[] nums, int k) {
        int n = nums.length;
        return findKthSmallest(nums, 0, n-1, n-k);
    }
    
    private int findKthSmallest(int[] nums, int low, int high, int k) {
        int idx = partition(nums, low, high);
        if (idx == k) {
            return nums[idx];
        }
        else if (idx < k) {
            return findKthSmallest(nums, idx+1, high, k);
        }
        else {
            return findKthSmallest(nums, low, idx-1, k);
        }
    }
    
    private int partition(int[] nums, int low, int high) {
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (nums[j] <= nums[pivot]) {
                swap(nums, i, j);
                i++;
            }
        }
        swap(nums, i, pivot);
        return i;
    }
    
    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // Running Time Complexity: O(N), with worse case O(N^2) when you always pick the largest element as pivot point, when the array is sorted.
    // ref: http://web.uvic.ca/~nmehta/csc226_fall2018/lecture2.pdf, page 29    
    public int findKthLargest(int[] nums, int k) {
        // quicksort O(n^2) worse case running time
        // without randomized;
        shuffle(nums);
        int n = nums.length;
        k = n-k;
        
        int low = 0;
        int high = n-1;
        while (low < high) {
            int idx = partition(nums, low, high);
            if (idx == k) {
                return nums[idx];
            }
            else if (idx < k) {
                low = idx + 1;
            }
            else {
                high = idx - 1;
            }
        }
        return nums[k];
    }
    
    private int partition(int[] nums, int low, int high) {
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (nums[j] <= nums[pivot]) {
                swap(nums, i, j);
                i++;
            }
        }
        swap(nums, i, pivot);
        return i;
    }
    
    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
    
    private void shuffle(int[] nums){
        Random random = new Random();
        for(int i = 0; i < nums.length; i++){
            int r = random.nextInt(i+1);
            swap(nums, i, r);
        }
    }
}