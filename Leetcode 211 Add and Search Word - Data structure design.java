/*
Design a data structure that supports the following two operations:

void addWord(word)
bool search(word)
search(word) can search a literal word or a regular expression string containing only letters a-z or .. A . means it can represent any one letter.

For example:

addWord("bad")
addWord("dad")
addWord("mad")
search("pad") -> false
search("bad") -> true
search(".ad") -> true
search("b..") -> true
*/
public class WordDictionary {
    public class TrieNode{
        public TrieNode[] next = new TrieNode[26];
        public boolean isWord = false;
        public TrieNode(){}
    }
    
    private TrieNode root;
    
    public WordDictionary(){
        root = new TrieNode();
    }
    
    // Adds a word into the data structure.
    public void addWord(String word) {
        TrieNode curr = root;
        for(char c : word.toCharArray()){
            if(curr.next[c-'a'] == null)
                curr.next[c-'a'] = new TrieNode();
            curr = curr.next[c-'a'];
        }
        curr.isWord = true;
    }

    // Returns if the word is in the data structure. A word could
    // contain the dot character '.' to represent any one letter.
    public boolean search(String word) {
        return search(root,word,0);
    }
    
    public boolean search(TrieNode curr, String s, int index){
        if(index >= s.length()){
            return curr.isWord;
        }
        char c = s.charAt(index);
        if(c == '.'){
            for(TrieNode node : curr.next){
                if(node != null && search(node,s,index+1)){
                    return true;
                }
            }
            return false;
        }
        else{
            TrieNode node = curr.next[c-'a'];
            return (node != null && search(node,s,index+1));
        }
    }
}

class WordDictionary {

    private TrieNode root;
    /** Initialize your data structure here. */
    public WordDictionary() {
        root = new TrieNode();
    }
    
    /** Adds a word into the data structure. */
    public void addWord(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.containsKey(c)) {
                curr.setNextTrieNode(c);
            }
            curr = curr.getNextTrieNode(c);
        }
        curr.setIsWord(true);
    }
    
    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {
        return search(word, 0, root);
    }
    
    private boolean search(String word, int index, TrieNode node) {
        if (index == word.length()) {
            return node.getIsWord();
        }
        char c = word.charAt(index);
        if (c == '.') {
            Set<Character> keys = node.getAllKeys();
            for (char cc : keys) {
                TrieNode nextNode = node.getNextTrieNode(cc);
                if (search(word, index+1, nextNode)) {
                    return true;
                }
            }
            return false;
        }
        else {
            if (!node.containsKey(c)) {
                return false;
            }
            TrieNode nextNode = node.getNextTrieNode(c);
            return search(word, index+1, nextNode);
        }
    }
    
    class TrieNode {
        public boolean isWord;
        public Map<Character, TrieNode> childrenMap;
        public TrieNode() {
            this.isWord = false;
            childrenMap = new HashMap<Character, TrieNode>();
        }
        
        public void setIsWord(boolean isWord) {
            this.isWord = isWord;
        }
        
        public boolean getIsWord() {
            return this.isWord;
        }
        
        public void setNextTrieNode(char c) {
            this.childrenMap.put(c, new TrieNode());
        }
        
        public TrieNode getNextTrieNode(char c) {
            return this.childrenMap.get(c);
        }
        
        public boolean containsKey(char c) {
            return this.childrenMap.containsKey(c);
        }

        public Set<Character> getAllKeys() {
            return this.childrenMap.keySet();
        }
    }
}

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary obj = new WordDictionary();
 * obj.addWord(word);
 * boolean param_2 = obj.search(word);
 */

// Your WordDictionary object will be instantiated and called as such:
// WordDictionary wordDictionary = new WordDictionary();
// wordDictionary.addWord("word");
// wordDictionary.search("pattern");