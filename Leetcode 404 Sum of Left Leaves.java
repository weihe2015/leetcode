Find the sum of all left leaves in a given binary tree.

Example:

    3
   / \
  9  20
    /  \
   15   7

There are two left leaves in the binary tree, with values 9 and 15 respectively. Return 24.

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Iterative BFS
    public int sumOfLeftLeaves(TreeNode root) {
        int sum = 0;
        if(root == null)
            return 0;
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        while(q.size() > 0){
            int size = q.size();
            for(int i = 0; i < size; i++){
                TreeNode curr = q.poll();
                if(curr.left != null){
                    if(isLeaf(curr.left))
                        sum += curr.left.val;
                    else
                        q.offer(curr.left);
                }
                if(curr.right != null){
                    q.offer(curr.right);
                }
            }
        }
        return sum;
    }
    
    // Iterative BFS Stack
    public int sumOfLeftLeaves(TreeNode root) {
        if(root == null)
            return 0;
        Stack<TreeNode> s = new Stack<TreeNode>();
        s.push(root);
        int sum = 0;
        while(!s.isEmpty()){
            TreeNode curr = s.pop();
            if(curr.left != null){
                if(isLeaf(curr.left))
                    sum += curr.left.val;
                else
                    s.push(curr.left);
            }
            if(curr.right != null)
                s.push(curr.right);
        }
        return sum;
    }

    private boolean isLeaf(TreeNode node){
        return node.left == null && node.right == null;
    }
    
    // recursive Level order traversal, Queue
    public int sumOfLeftLeaves(TreeNode root) {
        if(root == null)
            return 0;
        int sum = 0;
        if(root.left != null){
            if(isLeaf(root.left))
                sum += root.left.val;
            else
                sum += sumOfLeftLeaves(root.left);
        }
        sum += sumOfLeftLeaves(root.right);
        return sum;
    }


}