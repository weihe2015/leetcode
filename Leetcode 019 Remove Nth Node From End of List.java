/**
 * Given a linked list, remove the n-th node from the end of list and return its head.
Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:

Given n will always be valid.

Follow up:

Could you do this in one pass?
 * 
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    // One pass solution:
    /* trick: Assume total length of linked list is k
     1. First move curr pointer from header for n steps.
     2. If curr == null, then it means it will remove header
     3. Else: Move the prev pointer from header until curr becomes the last element:
        3.1 It moves prev pointer (k - n - 1) steps:
    */
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if (head == null) {
            return null;
        }
        // move curr for n steps:
        ListNode curr = head;
        int i = 0;
        while (curr != null && i < n) {
            curr = curr.next;
            i++;
        }
        if (curr == null) {
            return head.next;
        }
        ListNode prev = head;
        // let curr ptr to reach the last element:
        while (curr.next != null) {
            curr = curr.next;
            prev = prev.next;
        }
        prev.next = prev.next.next;
        return head;
    }

    // two pass solution
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode curr = head;
        // Index of previous Node of deleted Node:
        int idx = countLinkedListLength(curr) - n;
        // delete header
        if (idx == 0) {
            head = head.next;
        }
        else {
            // move until the node before the deleted node
            idx--;
            curr = head;
            while (idx > 0) {
                curr = curr.next;
                idx--;
            }
            curr.next = curr.next.next;
        }
        return head;
    }
    
    private int countLinkedListLength(ListNode head) {
        int count = 0;
        while (head != null) {
            count++;
            head = head.next;
        }
        return count;
    }
}