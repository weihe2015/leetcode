/**
A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

How many possible unique paths are there?

Note: m and n will be at most 100.

Example 1:

Input: m = 3, n = 2
Output: 3
Explanation:
From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
1. Right -> Right -> Down
2. Right -> Down -> Right
3. Down -> Right -> Right
Example 2:

Input: m = 7, n = 3
Output: 28
*/

public class Solution {
    // dp solution
    // Running Time Complexity: O(m * n), Space Complexity: O(m * n)
    public int uniquePaths(int m, int n) {
        if (m < 0 || n < 0) {
            return 0;
        }
        // sub problem: dp[i][j] = dp[i-1][j] + dp[i][j-1]
        // result: dp[m-1][n-1] since we starts at dp[0][0]
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // all boundary can only have one way to go.
                if (i == 0 || j == 0) {
                    dp[i][j] = 1;
                }
                // else, use sub problem to form current position.
                else {
                    dp[i][j] = dp[i-1][j] + dp[i][j-1];
                }
            }
        }
        return dp[m-1][n-1];
    }

    // dp solution, Time complexity: O(m*n) space complexity: O(min(m,n))
    // Running Time Complexity: O(m * n), Space Complexity: O(m)
    public int uniquePaths(int m, int n) {
        if (m < 0 || n < 0) {
            return 0;
        }
        int[] dp = new int[n];
        // initial array
        Arrays.fill(dp, 1);
        for (int i = 1; i < m; i++) {
        	for (int j = 1; j < n; j++) {
                // dp[i][j] = dp[i-1][j] + dp[i][j-1]
                // dp[i-1][j]: last dp[j]
                // dp[i][j-1]: dp[j-1]
        		dp[j] = dp[j] + dp[j-1];
        	}
        }
        return dp[n-1];
    }

/* solution 2
    (m+n-2) choose (n-1) formula
    combination
    m = 6, n = 4  8C3 = 8 * 7 * 6
                        3 * 2 * 1
    i = 1..n-1
    6 = m + i - 1, in case not overflow
    */
    public int uniquePaths(int m, int n) {
        // Math solution:
        // Total steps that we need to have:
        int N = m + n - 2;
        // Total steps that we need to go down:
        int k = m - 1;
        // number of steps
        double result = 1;

        // Formula: kCN = N!/k!(N-k)!
        for (int i = 1; i <= k; ++i) {
            result = result * (N - k + i) / i;
        }
        return (int)result;
    }
}