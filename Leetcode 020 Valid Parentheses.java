public class Solution {
    //"()" and "()[]{}" are all valid but "(]" and "([)]" are not
    public boolean isValid(String s) {
        Map<Character, Character> map = new HashMap<>();
        map.put('(', ')');
        map.put('[', ']');
        map.put('{', '}');
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            // For open bracket:
            if (map.containsKey(c)) {
                stack.push(c);
            }
            else {
                if (stack.isEmpty()) {
                    return false;
                }
                char openChar = stack.pop();
                char closeChar = map.get(openChar);
                if (closeChar != c) {
                    return false;
                }
            }
        }
        // handle '('
        return stack.isEmpty();
    }

    private static final Pattern PARENS = Pattern.compile("\\(\\)|\\[\\]|\\{\\}");
    public boolean isValid(String s) {
        int length;
        do {
            length = s.length();
            s = PARENS.matcher(s).replaceAll("");
        } while (length != s.length());
        return s.isEmpty();
    }
}