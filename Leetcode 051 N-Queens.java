/**
The n-queens puzzle is the problem of placing n queens on an n×n chessboard such that no two queens attack each other.

Given an integer n, return all distinct solutions to the n-queens puzzle.

Each solution contains a distinct board configuration of the n-queens placement, where 'Q' and '.' both indicate a queen and an empty space respectively.
*/
public class Solution {
    // Running Time Complexity: O(N^4N). Use list to store current Queen position will reduce to O(N^2N)
    public List<List<String>> solveNQueens(int n) {
        List<List<String>> results = new ArrayList<>();
        if (n < 1) {
            return results;
        }
        char[][] board = new char[n][n];
        constructBoard(board, n);
        solveNQueens(results, board, 0, n);
        return results;
    }

    // scan each row:
    private void solveNQueens(List<List<String>> results, char[][] board, int colIndex, int n) {
        if (colIndex == n) {
            List<String> result = constructList(board, n);
            results.add(result);
            return;
        }
        // scan each row:
        for (int i = 0; i < n; i++) {
            // if this position is valid, put Queen on it.
            if (validate(board, i, colIndex, n)) {
                board[i][colIndex] = 'Q';
                solveNQueens(results, board, colIndex+1, n);
                board[i][colIndex] = '.';
            }
        }
    }

    private boolean validate(char[][] board, int i, int j, int n) {
        // validate column:
        for (int x = 0; x < n; x++) {
            if (board[x][j] == 'Q') {
                return false;
            }
        }

        // validate row:
        for (int y = 0; y < n; y++) {
            if (board[i][y] == 'Q') {
                return false;
            }
        }

        // validate diagonal
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < j; y++) {
                if (board[x][y] == 'Q' && Math.abs(x-i) == Math.abs(y-j)) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<String> constructList(char[][] board, int n) {
        List<String> list = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < n; i++) {
            for (char c : board[i]) {
                sb.append(c);
            }
            list.add(sb.toString());
            sb.setLength(0);
        }
        return list;
    }

    private void constructBoard(char[][] board, int n) {
        for (int i = 0; i < n; i++) {
            Arrays.fill(board[i], '.');
        }
    }

    // Solution 2:
    // Scan each column
    private void solveNQueens2(List<List<String>> results, char[][] board, int rowIndex, int n) {
        if (rowIndex == n) {
            List<String> result = constructResult(board, n);
            results.add(result);
            return;
        }
        // scan each column:
        for (int j = 0; j < n; j++) {
            if (validateQueen2(board, rowIndex, j, n)) {
                board[rowIndex][j] = 'Q';
                solveNQueens2(results, board, rowIndex+1, n);
                board[rowIndex][j] = '.';
            }
        }
    }

    private boolean validateQueen2(char[][] board, int x, int y, int n) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'Q' && (j == y || Math.abs(y-j) == Math.abs(x-i))) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<String> constructResult(char[][] board, int n) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            StringBuffer sb = new StringBuffer();
            for (int j = 0; j < n; j++) {
                sb.append(board[i][j]);
            }
            result.add(sb.toString());
        }
        return result;
    }

    private char[][] initializeBoard(int n) {
        char[][] board = new char[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] = '.';
            }
        }
        return board;
    }

    // Solution 3: Store each Queen position as node in linkedlist, to improve validateQueen method
    class Node {
        private int x;
        private int y;
        public Node(int x, int y) {
            this.x = x;
            this.y = y;
        }
        public int getX() {
            return this.x;
        }

        public int getY() {
            return this.y;
        }
    }

    public List<List<String>> solveNQueens3(int n) {
        List<List<String>> results = new ArrayList<>();
        if (n < 1) {
            return results;
        }
        char[][] board = initializeBoard(n);
        List<Node> q = new LinkedList<>();
        solveNQueens(results, board, 0, n, q);
        return results;
    }

    private void solveNQueens(List<List<String>> results, char[][] board, int rowIndex, int n, List<Node> q) {
        if (rowIndex == n) {
            List<String> result = constructResult(board, n);
            results.add(result);
            return;
        }
        // scan each column:
        for (int j = 0; j < n; j++) {
            if (validateQueen(board, rowIndex, j, n, q)) {
                board[rowIndex][j] = 'Q';
                Node node = new Node(rowIndex, j);
                q.add(node);
                solveNQueens(results, board, rowIndex+1, n, q);
                board[rowIndex][j] = '.';
                q.remove(q.size()-1);
            }
        }
    }

    private boolean validateQueen(char[][] board, int i, int j, int n, List<Node> q) {
        for (Node node : q) {
            int x = node.getX();
            int y = node.getY();
            if (y == j || Math.abs(x-i) == Math.abs(y-j)) {
                return false;
            }
        }
        return true;
    }

    // Best Solution: Running Time Complexity: O(N^2), Space Complexity: O(N)
    public List<List<String>> solveNQueens4(int n) {
        List<List<String>> res=new ArrayList<List<String>>();
        String[] queens = new String[n];
        char[] initial = new char[n];
        Arrays.fill(initial,'.');
        Arrays.fill(queens,String.valueOf(Arrays.copyOf(initial,n)));
        int[] flag = new int[5*n-2];
        Arrays.fill(flag,1);
        slove(res,queens,flag,0,n);
        return res;
    }
    private void slove(List<List<String>> res,String[] queens,int[] flag,int row,int n){
        if(row==n){
            res.add(new ArrayList<String>(Arrays.asList(queens)));
            return;
        }
        for(int col=0;col!=n;col++){
            if(flag[col]==1 && flag[n+col+row]==1 && flag[4*n-2+col-row]==1){
                flag[col]=0;
                flag[n+col+row]=0;
                flag[4*n-2+col-row]=0;
                char[] chars=queens[row].toCharArray();

                chars[col]='Q';
                queens[row]=String.valueOf(chars);

                slove(res,queens,flag,row+1,n);

                chars=queens[row].toCharArray();
                chars[col]='.';
                queens[row]=String.valueOf(chars);

                flag[col]=1;
                flag[n+col+row]=1;
                flag[4*n-2+col-row]=1;
            }
        }
    }
}