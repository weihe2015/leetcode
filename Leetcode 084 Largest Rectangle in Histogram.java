/*
Given n non-negative integers representing the histograms bar height where the width of each bar is 1, 
find the area of largest rectangle in the histogram.
Above is a histogram where width of each bar is 1, given height = [2,1,5,6,2,3].
The largest rectangle is shown in the shaded area, which has area = 10 unit.
For example,
Given heights = [2,1,5,6,2,3],
return 10.
*/
public class Solution {
    // Running Time Complexity: O(n^2), Space Complexity: O(1)
    // Basic idea: for each bar, search its left hand side and right hand side until it finds the bard lower than itself.
    // Use these two indexes to calculate the rectangle area: height * (r-l+1) and update the maxArea
    public int largestRectangleArea(int[] heights) {
        int maxArea = 0;
        for (int i = 0, max = heights.length; i < max; i++) {
            int height = heights[i];
            // search left hand side of this bar until it finds the bar lower than itself:
            int l = i;
            for (int k = i-1; k >= 0; k--) {
                if (heights[k] >= height) {
                    l--;
                }
                else {
                    break;
                }
            }
            // search right hand side of this bar until it finds the bar lower than itself:
            int r = i;
            for (int k = i+1; k < max; k++) {
                if (heights[k] >= height) {
                    r++;
                }
                else {
                    break;
                }
            }
            int currArea = height * (r - l + 1);
            maxArea = Math.max(maxArea, currArea);
        }
        return maxArea;
    }

    // Better Solution:
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int largestRectangleArea(int[] heights) {
        int maxArea = 0, max = heights.length;
        Deque<Integer> s = new ArrayDeque<Integer>();
        int i = 0;
        while (i <= max) {
            int height = 0;
            // if left pointer does not reach to the end, get current height:
            if (i < max) {
                height = heights[i];
            }
            // if stack is empty or current height is higher than previous one, push index into the stack.
            if (s.isEmpty() || height >= heights[s.peek()]) {
                s.push(i);
                i++;
            }
            else {
                // Get the previous top height
                int topIdx = s.pop();
                // If stack is empty, then it means previous elements are greater than heights[topIdx]
                // Else, s.peek() is the starting point of histogram. width = i - (s.peek() + 1)
                int width = i;
                if (!s.isEmpty()) {
                    width = i - (s.peek() + 1);
                }
                int currArea = heights[topIdx] * width;
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }
}