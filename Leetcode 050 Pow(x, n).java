/**
Implement pow(x, n), which calculates x raised to the power n (xn).

Example 1:

Input: 2.00000, 10
Output: 1024.00000
Example 2:

Input: 2.10000, 3
Output: 9.26100
Example 3:

Input: 2.00000, -2
Output: 0.25000
Explanation: 2-2 = 1/22 = 1/4 = 0.25
Note:

-100.0 < x < 100.0
n is a 32-bit signed integer, within the range [−231, 231 − 1]
*/
public class Solution {
    // https://leetcode-cn.com/problems/powx-n/solution/powx-n-by-leetcode-solution/
    /**
    Iterative:

    x -> x^2 -> x^4 -> + x^9 -> + x^19 -> x^38 -> + x^77 
    这些步骤中，我们直接把上一次的结果进行平方, 这些+步骤中，我们把上一次的结果进行平方后，还要额外乘一个 x

    1. x^38 -> x^77 中额外乘了一个x
    2. x^9 -> x^19 中额外乘了一个x, 被平方了2次，因此在x^77 中贡献了 x^(2^2) = x^(4)
    3. x^4 -> x^9 中额外乘了一个x，被平方了3次，因此在x^77 中贡献了 x^(2^3) = x^(8)
    4. 最初的x 在之后的平方了6次，因此在x^77 中贡献了 x^(2^6) = x^(64)

    x * x^4 * x^8 * x^64 = x^77

    77 = (1001101)2 指数1，4，8，64代表数字二进制中的1

    x^5
    x -> x^2 ->+ x^5
    */
    // 因此递归的层数为 O(log n)
    public double myPow(double x, int n) {
        long N = (long) n;
        if (N < 0) {
            return 1.0 / quickPow(x, -N);
        }
        else {
            return quickPow(x, N);
        }
    }
    
    private double quickPow(double x, long n) {
        double res = 1.0;
        // 贡献的最初值:
        double x_contribute = x;
        // 在对 N 进行二进制拆分的同时计算答案
        while (n > 0) {
            if (n % 2 == 1) {
                // 如果 N 二进制表示的最低位为 1，那么需要计入贡献
                res *= x_contribute;
            }
            // 将贡献不断地平方
            x_contribute *= x_contribute;
            n /= 2;
        }
        return res;
    }


    /**
    Divide and Conquer: 
    Recursive:

    x^77:
    x -> x^2 -> x^4 -> x^9 -> x^19 -> x^38 -> x^77 

    1. 当我们要计算 x^n 时，我们可以递归地算出 y=x^[n/2], 其中[a] 是表示对a进行向下取整
    2. 根据递归计算的结果，如果n 为偶数，那么x^n = y^2; 如果n 为奇数，那么x^n = y^2 * x
    3. 递归的边界为 n =0，任意数的 0 次方均为 1。
    */
    // Time Complexity: O(logN), # of level stack, Space Complexity: O(logN)
    public double myPow(double x, int n) {
        long N = (long) n;
        if (N < 0) {
            return 1.0 / recursivePow(x, -N);
        }
        else {
            return recursivePow(x, N);
        }
    }
    
    private double recursivePow(double x, long n) {
        if (n == 0) {
            return 1.0;
        }
        double y = recursivePow(x, n/2);
        if (n % 2 == 0) {
            return y * y;
        }
        else {
            return y * y * x;
        }
    }
}