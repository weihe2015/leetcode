/**
Given a string s and a string t, check if s is subsequence of t.

You may assume that there is only lower case English letters in both s and t. t is potentially
a very long (length ~= 500,000) string, and s is a short string (<=100).

A subsequence of a string is a new string which is formed from the original string by deleting some (can be none) of the characters
without disturbing the relative positions of the remaining characters. (ie, "ace" is a subsequence of "abcde" while "aec" is not).

Example 1:
s = "abc", t = "ahbgdc"

Return true.

Example 2:
s = "axc", t = "ahbgdc"

Return false.

Follow up:
If there are lots of incoming S, say S1, S2, ... , Sk where k >= 1B,
and you want to check one by one to see if T has its subsequence. In this scenario, how would you change your code?
*/
public class Solution {
    // Two pointers solution:
    // Running time Complexity: O(m + n), Space Complexity: O(1)
    public boolean isSubsequence(String s, String t) {
        int i = 0;
        for (int j = 0, max = t.length(); j < max; j++) {
            if (i >= s.length()) {
                break;
            }
            if (s.charAt(i) == t.charAt(j)) {
                i++;
            }
        }
        return i == s.length();
    }

    // Brute Force Solution:
    public boolean isSubsequence(String s, String t) {
        Deque<Character> stack = new ArrayDeque<Character>();
        char[] cList = s.toCharArray();
        for (int n = cList.length, i = n-1; i >= 0; i--) {
            stack.push(cList[i]);
        }
        for (int i = 0, n = t.length(); i < n; i++) {
            char c = t.charAt(i);
            if (!stack.isEmpty() && c == stack.peek()) {
                stack.pop();
            }
        }
        return stack.isEmpty();
    }

    // Running Time Complexity: O(N), because we move j index every time we use string.indexOf
    public boolean isSubsequence(String s, String t) {
        int j = 0;
        for (int i = 0, n = s.length(); i < n; i++) {
            char c = s.charAt(i);
            // indexOf(String str, int fromIndex)
            // Returns the index within this string of the first occurrence of the specified substring,
            // starting at the specified index.
            j = t.indexOf(c, j);
            // If this character c is not in substring starting from j
            // then it is not a subsequence
            if (j < 0){
                return false;
            }
            j++;
        }
        return true;
    }
    /**
     * Follow up: Preprocess string t, into Map<Character, TreeSet<Integer>>, where TreeSet will store all indexes of character c:
    */
   public boolean[] isSubsequence(List<String> list, String t) {
       Map<Character, TreeSet<Integer>> map = new HashMap<>();
       for (int i = 0, n = t.length(); i < n; i++) {
           char c = t.charAt(i);
           if (!map.containsKey(c)) {
               map.put(c, new TreeSet<Integer>());
           }
           map.get(c).add(i);
       }
       int k = list.size();
       boolean[] res = new boolean[k];
       for (String s : list) {
           int j = 0;
           int idx = 0;
           for (int i = 0, n = s.length(); i < n; i++) {
               char c = s.charAt(i);
               if (!map.containsKey(c)) {
                   break;
               }
               Set<Integer> set = map.get(c);
               Integer nextIdx = set.ceiling(idx);
               if (nextIdx == null) {
                   break;
               }
               else {
                   idx = nextIdx;
               }
               if (i == n-1) {
                   res[j++] = true;
               }
           }
       }
       return res;
   }
}