/**

Implement strStr().

Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

Example 1:

Input: haystack = "hello", needle = "ll"
Output: 2
Example 2:

Input: haystack = "aaaaa", needle = "bba"
Output: -1
Clarification:

What should we return when needle is an empty string? This is a great question to ask during an interview.

For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().
*/
public class Solution {
    // Running Time Complexity: O((n-m) * m)
    // Space Complexity: O(1)
    public int strStr(String haystack, String needle) {
        if (haystack == null || needle == null) {
            return 0;
        }
        int m = haystack.length(), n = needle.length();
        for (int i = 0; i <= m-n; i++) {
            int j = 0;
            while (j < n) {
                if (haystack.charAt(i + j) != needle.charAt(j)) {
                    break;
                }
                j++;
            }
            if (j == n) {
                return i;
            }
        }
        return -1;
    }

    // Naive Solution:
    public int strStr(String haystack, String needle) {
        if (needle == null || needle.length() == 0) {
            return 0;
        }
        if (haystack == null || haystack.length() == 0 || needle.length() > haystack.length()) {
            return -1;
        }
        char[] hays = haystack.toCharArray();
        char[] needs = needle.toCharArray();
        
        for (int i = 0, n = hays.length; i < n; i++) {
            int idx = i;
            int j = 0, m = needs.length;
            while (idx < n && j < m) {
                char c1 = hays[idx];
                char c2 = needs[j];
                if (c1 == c2) {
                    idx++;
                }
                else {
                    break;
                }
                j++;
            }
            if (j == m) {
                return i;
            }
        }
        return -1;
    }
}