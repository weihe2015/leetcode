/**
A message containing letters from A-Z is being encoded to numbers using the following mapping way:

'A' -> 1
'B' -> 2
...
'Z' -> 26
Beyond that, now the encoded string can also contain the character '*', which can be treated as one of the numbers from 1 to 9.

Given the encoded message containing digits and the character '*', return the total number of ways to decode it.

Also, since the answer may be very large, you should return the output mod 109 + 7.

Example 1:
Input: "*"
Output: 9
Explanation: The encoded message can be decoded to the string: "A", "B", "C", "D", "E", "F", "G", "H", "I".
Example 2:
Input: "1*"
Output: 9 + 9 = 18
Note:
The length of the input string will fit in range [1, 105].
The input string will only contain the character '*' and digits '0' - '9'.
*/

public class Solution {
    public int numDecodings(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int n = s.length();
        long MOD = (long) (1e9 + 7);
        char[] cList = s.toCharArray();
        long[] dp = new long[n+1];
        dp[0] = 1;
        if (cList[0] == '*') {
            dp[1] = 9;
        }
        else {
            dp[1] = (cList[0] - '0') == 0 ? 0 : 1;
        }
        for (int i = 2; i <= n; i++) {
            char c1 = cList[i-1];
            char c2 = cList[i-2];
            if (c1 == '0') {
                if (c2 == '1' || c2 == '2') {
                    dp[i] = dp[i-2];
                }
                else if (c2 == '*') {
                    dp[i] = 2 * dp[i-2];
                }
                // 00
                else {
                    return 0;
                }
            }
            else if (c1 >= '1' && c1 <= '9') {
                dp[i] = dp[i-1];
                if (c2 == '1' || (c2 == '2' && c1 <= '6')) {
                    dp[i] = (dp[i] + dp[i-2]) % MOD;
                }
                else if (c2 == '*') {
                    if (c1 <= '6') {
                        dp[i] = (dp[i] + 2 * dp[i-2]) % MOD;
                    }
                    else {
                        dp[i] = (dp[i] + dp[i-2]) % MOD;
                    }
                }
            }
            // cList[i] == '*'
            else {
                dp[i] = (9 * dp[i-1]) % MOD;
                // 11 - 16:
                if (c2 == '1') {
                    dp[i] = (dp[i] + 9 * dp[i-2]) % MOD;
                }
                // 21 - 26:
                else if (c2 == '2'){
                    dp[i] = (dp[i] + 6 * dp[i-2]) % MOD;
                }
                else if (c2 == '*') {
                    dp[i] = (dp[i] + 15 * dp[i-2]) % MOD;
                }
            }
        }
        return (int)dp[n];
    }
}