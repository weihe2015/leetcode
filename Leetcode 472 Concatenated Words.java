/**
Given a list of words (without duplicates), please write a program that returns all concatenated words in the given list of words.
A concatenated word is defined as a string that is comprised entirely of at least two shorter words in the given array.

Example:
Input: ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]

Output: ["catsdogcats","dogcatsdog","ratcatdogcat"]

Explanation: "catsdogcats" can be concatenated by "cats", "dog" and "cats"; 
 "dogcatsdog" can be concatenated by "dog", "cats" and "dog"; 
"ratcatdogcat" can be concatenated by "rat", "cat", "dog" and "cat".
*/
public class Solution {
    // Trie Solution:
    // Running Time complexity: O(N * L), Space Complexity: O(N * L)
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        List<String> result = new ArrayList<String>();
        
        Trie trie = new Trie();
        trie.addWords(words);
        
        for (String word : words) {
            if (search(word, 0, 0, trie)) {
                result.add(word);
            }
        }
        return result;
    }
    
    private boolean search(String word, int idx, int count, Trie trie) {
        int n = word.length();
        if (idx == n) {
            return count > 1;
        }
        TrieNode curr = trie.root;
        for (int i = idx; i < n; i++) {
            char c = word.charAt(i);
            if (curr.children[c-'a'] == null) {
                return false;
            }
            curr = curr.children[c-'a'];
            if (curr.isWord && search(word, i+1, count+1, trie)) {
                return true;
            }
        }
        return false;
    }
    
    class Trie {
        TrieNode root;
        public Trie() {
            root = new TrieNode();
        }
        
        public void addWords(String[] words) {
            for (String word : words){
                addWord(word);
            }
        }
        
        public void addWord(String word) {
            TrieNode node = root;
            for (char c : word.toCharArray()) {
                if (node.next[c-'a'] == null) {
                    node.next[c-'a'] = new TrieNode();
                }
                node = node.next[c-'a'];
            }
            node.isWord = true;
        }
    }
     
    class TrieNode {
        boolean isWord;
        TrieNode[] next;
        public TrieNode() {
            next = new TrieNode[26];
        }
    }

    // Solution 2: Use Word Break: LTE: Time Limited Exceeded.
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        List<String> result = new ArrayList<String>();
        Arrays.sort(words, (s1, s2) -> s1.length() - s2.length());
        
        Set<String> wordDict = new HashSet<>();
        for (String word : words) {
            if (wordBreak(word, wordDict)) {
                result.add(word);
            }
            wordDict.add(word);
        }
        return result;
    }
    
    public boolean wordBreak(String s, Set<String> wordDict) {
        if (wordDict.isEmpty()) {
            return false;
        }
        int n = s.length();
        // dp[i]: substring(0, i) can be segmented with words in wordDict:
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for (int i = 0; i <= n; i++) {
            // If substring(0...i) cannot be segmented with words in wordDict,
            // there is no need to see if appended word in wordDict can be segmented.
            if (!dp[i]) {
                continue;
            }
            // Try each word and find substring(i, i+word.length()):
            // "applepenapple": word = pen, i = 5, dp[i] = true, substring(i, i+word.length()) = substring(5, 8) = pen:
            // so dp[8] = true;
            for (String word : wordDict) {
                int end = i + word.length();
                // If either appended word to substring(0,i) is longer than s, or we already found dp[end] == true
                if (end > n || dp[end]) {
                    continue;
                }
                // substr like "pen", s="applepenapple", ["apple","pen"]
                String substr = s.substring(i, end);
                if (substr.equals(word)) {
                    dp[end] = true;
                }
            }
        }
        return dp[n];
    }


    /**
    Follow up:
    Input: ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]
    Output:
    // Print out all the paths (subwords) that can be concatenated with the words in the list:
    [["cats", "dog", "cats", "catsdogcats"], ["dog", "cats", "dog", "dogcatsdog"], ["rat", "cat", "dog", "cat"]]
    */
    public List<List<String>> findAllConcatenatedWordsInADict(String[] words) {
        List<List<String>> res = new ArrayList<>();
        Arrays.sort(words, Comparator.comparing(w -> w.length()));
        Trie trie = new Trie();
        trie.addWords(words);

        for (String word : words) {
            List<String> list = new ArrayList<>();
            int n = word.length();
            if (search(word, 0, n, trie, list)) {
                list.add(word);
                res.add(list);
            }
        }

        return res;
    }

    public boolean search(String word, int idx, int n, Trie trie, List<String> list) {
        if (idx == n) {
            return list.size() > 1;
        }
        TrieNode curr = trie.root;
        for (int i = idx; i < n; i++) {
            char c = word.charAt(i);
            if (curr.children[c-'a'] == null) {
                return false;
            }
            curr = curr.children[c-'a'];
            if (curr.isWord) {
                list.add(curr.word);
                if (search(word, i+1, n, trie, list)) {
                    return true;
                }
                else {
                    list.remove(list.size() - 1);
                }
            }
        }
        return false;
    }

    class Trie {
        TrieNode root;
        public Trie() {
            this.root = new TrieNode();
        }

        public void addWords(String[] words) {
            for (String word : words) {
                TrieNode curr = root;
                for (char c : word.toCharArray()) {
                    if (curr.children[c-'a'] == null) {
                        curr.children[c-'a'] = new TrieNode();
                    }
                    curr = curr.children[c-'a'];
                }
                curr.isWord = true;
                curr.word = word;
            }
        }
    }

    class TrieNode {
        boolean isWord;
        String word;
        TrieNode[] children;
        public TrieNode() {
            this.children = new TrieNode[26];
        }
    }
}