/**
Given a string, find the length of the longest substring T that contains at most k distinct characters.

Example 1:

Input: s = "eceba", k = 2
Output: 3
Explanation: T is "ece" which its length is 3.
Example 2:

Input: s = "aa", k = 1
Output: 2
Explanation: T is "aa" which its length is 2.
*/

public class Solution {
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // write your code here
        if (s == null || s.length() == 0) {
            return 0;
        }
        int l = 0;
        int r = 0;
        int n = s.length();
        int maxLen = 0;
        int distinctCharCnt = 0;
        int[] charMap = new int[256];

        while (r < n) {
            if (distinctCharCnt <= k) {
                char rc = s.charAt(r);
                charMap[rc]++;
                if (charMap[rc] == 1) {
                    distinctCharCnt++;
                }
                r++;
            }
            while (distinctCharCnt > k) {
                char lc = s.charAt(l);
                charMap[lc]--;
                if (charMap[lc] == 0) {
                    distinctCharCnt--;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }
}