/*
Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.

Note that you cannot sell a stock before you buy one.

Example 1:

Input: [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
             Not 7-1 = 6, as selling price needs to be larger than buying price.
Example 2:

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.
*/
public class Solution {

    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int[][] dp = new int[n][2];
        
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                // rest
                dp[i][0] = 0;
                // buy in
                dp[i][1] = -prices[i];
                continue;
            }
            // 今天没有股票: max(昨天没有股票， 昨天有股票 + 卖了今天的股票)
            dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i]);
            // 今天有股票: max(昨天有股票, 昨天没有股票 + 买了今天的股票)
            dp[i][1] = Math.max(dp[i-1][1], -prices[i]);
        }
        return dp[n-1][0];
    }

    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        // first day, no transaction, 0: without stock, max profit = 0;
        int dp_i_0 = 0;
        // first day, 1 transaction, 1: with stock, max profit = buy stock = -prices[0]
        int dp_i_1 = -prices[0];
        for (int i = 1; i < n; i++) {
            dp_i_0 = Math.max(dp_i_0, dp_i_1 + prices[i]);
            dp_i_1 = Math.max(dp_i_1, -prices[i]);
        }
        return dp_i_0;
    }


    // Naive Solution:
    // Running Time Complexity: O(n^2), Space Complexity: O(1)
    public int maxProfit(int[] prices) {
        int profit = 0;
        for (int i = 0, n = prices.length; i < n-1; i++) {
            for (int j = i+1; j < n; j++) {
                int diff = prices[j] - prices[i];
                if (diff > 0) {
                    profit = Math.max(profit, diff);
                }
            }
        }
        return profit;
    }

    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int maxProfit(int[] prices) {
        int maxProfit = 0, currMin = Integer.MAX_VALUE;
        for (int price : prices) {
            currMin = Math.min(price, currMin);
            maxProfit = Math.max(maxProfit, price - currMin);
        }
        return maxProfit;
    }

    // Kadane's Algorithm
    public int maxProfit(int[] prices){
        int maxCur = 0, maxSoFar = 0;
        for (int i = 1; i < prices.length; i++) {
            maxCur = Math.max(0, maxCur += prices[i] - prices[i-1]);
            maxSoFar = Math.max(maxCur, maxSoFar);
        }
        return maxSoFar;
    }

    // DP solution
    public int maxProfit(int[] prices){
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        // dp[i]: the maxProfit of current position:
        int[] dp = new int[n];
        int curMin = prices[0];
        for (int i = 1; i < n; i++) {
            // Get the larger between previous day earning and current day profit (prices[i] - currMin)
            dp[i] = Math.max(dp[i-1], prices[i] - curMin);
            curMin = Math.min(curMin, prices[i]);
        }
        return dp[n-1];
    }
}