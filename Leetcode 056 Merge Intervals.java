/*
Given a collection of intervals, merge all overlapping intervals.

Example 1:

Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considerred overlapping.
*/
/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
public class Solution {

    // Running Time Complexity: O(NlogN)
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> result = new ArrayList<Interval>();
        if (intervals.isEmpty()) {
            return result;
        }
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        // Sort intervals by its start
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        /**
        Collections.sort(intervals, new Comparator<Interval>(){
            @Override
            public int compare(Interval i1, Interval i2){
                return Integer.compare(i1.start, i2.start);
            }
        });
        */
        int start = -1, end = -1;
        for (Interval interval : intervals) {
            if (start == -1 && end == -1) {
                start = interval.start;
                end = interval.end;
            }
            else {
                // Overlapping intervals, update end bound
                if (interval.start <= end) {
                    end = Math.max(end, interval.end);
                }
                // Disjoint intervals, add the previous one and reset bounds
                else {
                    result.add(new Interval(start, end));
                    start = interval.start;
                    end = interval.end;
                }
            }
        }
        // add last Interval
        result.add(new Interval(start, end));
        return result;
    }

    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> res = new ArrayList<>();
        if (intervals == null || intervals.isEmpty()) {
            return res;
        }
        // sort by starting time.
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        Interval prevInterval = null;
        for (Interval interval : intervals) {
            // add non-overlap interval:
            if (prevInterval == null || prevInterval.end < interval.start) {
                res.add(interval);
                prevInterval = interval;
            }
            else if (prevInterval.end <= interval.end) {
                prevInterval.end = interval.end;
            }
        }
        return res;
    }

    public int[][] merge(int[][] intervals) {
        // sort by starting time.
        Arrays.sort(intervals, Comparator.comparingInt(l -> l[0]));
        List<int[]> list = new ArrayList<>();
        int[] prev = new int[]{-1, -1};
        for (int[] curr : intervals) {
            // add non-overlap interval:
            if (prev[0] == -1 || prev[1] < curr[0]) {
                list.add(curr);
                prev = curr;
            }
            else if (prev[1] <= curr[1]) {
                prev[1] = curr[1];
                int n = list.size();
                list.get(n-1)[1] = curr[1];
            }
        }
        int n = list.size();
        // System.out.println(Arrays.deepToString(res));
        return list.toArray(new int[n][2]);
    }

    public List<Interval> merge(List<Interval> intervals) {
        int n = intervals.size();
        int[] starts = new int[n];
        int[] ends = new int[n];
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        // sort start & end
        Arrays.sort(starts);
        Arrays.sort(ends);

        List<Interval> result = new ArrayList<Interval>();
        // Iterate each start and end point.
        for (int i = 0, j = 0; i < n; i++) {
            // If i is last item or next item's start is larger than ends, add this into new Interval
            if (i == n-1 || starts[i+1] > ends[i]) {
                result.add(new Interval(starts[j], ends[i]));
                // Move j pointer to point to next interval
                j = i + 1;
            }
        }
        return result;
    }
}