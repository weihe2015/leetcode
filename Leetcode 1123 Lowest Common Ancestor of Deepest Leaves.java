/**
Given the root of a binary tree, return the lowest common ancestor of its deepest leaves.

Recall that:

The node of a binary tree is a leaf if and only if it has no children
The depth of the root of the tree is 0. 
If the depth of a node is d, the depth of each of its children is d + 1.

The lowest common ancestor of a set S of nodes, is the node A with the largest depth such that every node in S is in the subtree with root A.

Note: This question is the same as 865: https://leetcode.com/problems/smallest-subtree-with-all-the-deepest-nodes/

*/
public class Solution {
    /**
    如果左右子树高度相等，则当前结点为要找的结点。否则要找的结点在高度较大的子树中。自底向上的计算高度并返回寻找到的结点。
    */
    public TreeNode lcaDeepestLeaves(TreeNode root) {
        Object[] res = find(root, 0);
        return (TreeNode) res[0];
    }
    
    private Object[] find(TreeNode node, int depth) {
        if (node == null) {
            return new Object[]{node, depth};
        }
        Object[] left = find(node.left, depth+1);
        Object[] right = find(node.right, depth+1);
        
        int leftDepth = (int) left[1];
        int rightDepth = (int) right[1];
        if (leftDepth == rightDepth) {
            return new Object[]{node, leftDepth};
        }
        else if (leftDepth > rightDepth) {
            return left;
        }
        else {
            return right;
        }
    }

    /**
    如果当前节点是最深叶子节点的最近公共祖先，那么它的左右子树的高度一定是相等的，
    否则高度低的那个子树的叶子节点深度一定比另一个子树的叶子节点的深度小，
    因此不满足条件。所以只需要dfs遍历找到左右子树高度相等的根节点即出答案。
    */
    public TreeNode lcaDeepestLeaves(TreeNode root) {
        if (root == null) {
            return null;
        }
        int leftDepth = depth(root.left);
        int rightDepth = depth(root.right);
        
        if (leftDepth == rightDepth) {
            return root;
        }
        else if (leftDepth > rightDepth) {
            return lcaDeepestLeaves(root.left);
        }
        else {
            return lcaDeepestLeaves(root.right);
        }
    }
    
    private int depth(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int leftDepth = depth(node.left);
        int rightDepth = depth(node.right);
        return Math.max(leftDepth, rightDepth) + 1;
    }
}