/**
Given two strings s and t, determine if they are both one edit distance apart.

Note: 

There are 3 possiblities to satisify one edit distance apart:

Insert a character into s to get t
Delete a character from s to get t
Replace a character of s to get t
Example 1:

Input: s = "ab", t = "acb"
Output: true
Explanation: We can insert 'c' into s to get t.
Example 2:

Input: s = "cab", t = "ad"
Output: false
Explanation: We cannot get t from s by only one step.
Example 3:

Input: s = "1203", t = "1213"
Output: true
Explanation: We can replace '0' with '1' to get t.
*/

public class solution {
	/*
	 * There're 3 possibilities to satisfy one edit distance apart: 
	 * 
	 * 1) Replace 1 char:
	      s: a B c
	      t: a D c
	 * 2) Delete 1 char from s: 
	      s: a D  b c
	      t: a    b c
	 * 3) Delete 1 char from t
	      s: a   b c
	      t: a D b c
	 */

	/**
    1. 两个字符串的长度之差大于1，那么直接返回False
    2. 两个字符串的长度之差等于1，那么长的那个字符串去掉一个字符，剩下的应该和短的字符串相同
    3. 两个字符串的长度之差等于0，那么两个字符串对应位置的字符只能有一处不同。
    */
	// Running time Complexity: O(m+n), Space Complexity: O(1)
    public boolean isOneEditDistance(String s, String t) {
        // write your code here
        if (s.length() < t.length()) {
            char[] c1 = s.toCharArray();
            char[] c2 = t.toCharArray();
            char[] tmp = c1;
            c1 = c2;
            c2 = tmp;
            s = new String(c1);
            t = new String(c2);
        }
        int m = s.length(), n = t.length();
        // 两个字符串的长度之差大于1，那么直接返回False
        if (m - n > 1) {
            return false;
        }
        // 两个字符串的长度之差等于1，那么长的那个字符串去掉一个字符，剩下的应该和短的字符串相同
        else if (m - n == 1) {
            int[] cList = new int[256];
            for (char c : t.toCharArray()) {
                cList[c]++;
            }
            for (char c : s.toCharArray()) {
                cList[c]--;
            }
            int count = 0;
            for (int num : cList) {
                if (num != 0) {
                    count++;
                }
            }
            return count == 1;
        }
        // 两个字符串的长度之差等于0，那么两个字符串对应位置的字符只能有一处不同。
        else {
            int count = 0;
            for (int i = 0; i < m; i++) {
                if (s.charAt(i) != t.charAt(i)) {
                    count++;
                }
            }
            return count == 1;
        }
    }

	public boolean isOneEditDistance(String s, String t) {
	    for (int i = 0; i < Math.min(s.length(), t.length()); i++) { 
	        if (s.charAt(i) != t.charAt(i)) {
	            if (s.length() == t.length()) // s has the same length as t, so the only possibility is replacing one char in s and t
	                return s.substring(i + 1).equals(t.substring(i + 1));
	            else if (s.length() < t.length()) // t is longer than s, so the only possibility is deleting one char from t
	                return s.substring(i).equals(t.substring(i + 1));               
	            else // s is longer than t, so the only possibility is deleting one char from s
	                return t.substring(i).equals(s.substring(i + 1));
	        }
	    }       
	    //All previous chars are the same, the only possibility is deleting the end char in the longer one of s and t 
	    return Math.abs(s.length() - t.length()) == 1;        
	}
}