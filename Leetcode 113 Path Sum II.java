/*
Given a binary tree and a sum, find all root-to-leaf paths where each paths sum equals the given sum.

For example:
Given the below binary tree and sum = 22,
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \    / \
        7    2  5   1
return
[
   [5,4,11,2],
   [5,8,4,5]
]
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive:
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        List<Integer> list = new ArrayList<>();
        list.add(root.val);
        dfs(result, root, sum-root.val, list);
        return result;
    }

    private void dfs(List<List<Integer>> result, TreeNode node, int sum, List<Integer> list) {
        if (node.left == null && node.right == null && sum == 0) {
            result.add(new ArrayList<>(list));
            return;
        }
        if (node.left != null) {
            list.add(node.left.val);
            dfs(result, node.left, sum-node.left.val, list);
            list.remove(list.size()-1);
        }
        if (node.right != null) {
            list.add(node.right.val);
            dfs(result, node.right, sum-node.right.val, list);
            list.remove(list.size()-1);
        }
    }

    // Solution 2:
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        pathSum(result, root, sum, new ArrayList<Integer>());
        return result;
    }

    private void pathSum(List<List<Integer>> result, TreeNode node, int sum, List<Integer> list) {
        if (node == null) {
            return;
        }
        int diff = sum - node.val;
        if (node.left == null && node.right == null && diff == 0) {
            list.add(node.val);
            result.add(new ArrayList<Integer>(list));
            list.remove(list.size()-1);
            return;
        }
        list.add(node.val);
        pathSum(result, node.left, diff, list);
        pathSum(result, node.right, diff, list);
        list.remove(list.size()-1);
    }

    // Post order traversal
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        List<Integer> list = new ArrayList<Integer>();
        int tempSum = 0;
        TreeNode node = root, prevNode = null;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                tempSum += node.val;
                list.add(node.val);
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.peek();
            if (currNode.left == null && currNode.right == null && tempSum == sum) {
                result.add(new ArrayList<Integer>(list));
            }
            if (currNode.right != null && currNode.right != prevNode) {
                node = currNode.right;
            }
            else {
                currNode = stack.pop();
                prevNode = currNode;
                tempSum -= currNode.val;
                list.remove(list.size()-1);
            }
        }
        return result;
    }
}