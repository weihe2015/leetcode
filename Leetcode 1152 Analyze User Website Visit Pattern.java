/**
We are given some website visits: the user with name username[i] visited the website website[i] at time timestamp[i].

A 3-sequence is a list of websites of length 3 sorted in ascending order by the time of their visits. 
(The websites in a 3-sequence are not necessarily distinct.)

Find the 3-sequence visited by the largest number of users. If there is more than one solution, 
return the lexicographically smallest such 3-sequence.

Example 1:

Input: username = ["joe","joe","joe","james","james","james","james","mary","mary","mary"], timestamp = [1,2,3,4,5,6,7,8,9,10], website = ["home","about","career","home","cart","maps","home","home","about","career"]
Output: ["home","about","career"]
Explanation: 
The tuples in this example are:
["joe", 1, "home"]
["joe", 2, "about"]
["joe", 3, "career"]
["james", 4, "home"]
["james", 5, "cart"]
["james", 6, "maps"]
["james", 7, "home"]
["mary", 8, "home"]
["mary", 9, "about"]
["mary", 10, "career"]
The 3-sequence ("home", "about", "career") was visited at least once by 2 users.
The 3-sequence ("home", "cart", "maps") was visited at least once by 1 user.
The 3-sequence ("home", "cart", "home") was visited at least once by 1 user.
The 3-sequence ("home", "maps", "home") was visited at least once by 1 user.
The 3-sequence ("cart", "maps", "home") was visited at least once by 1 user.
*/
public class Solution {
    
    class Tuple {
        int timeStamp;
        String username;
        String website;
        public Tuple(int timeStamp, String username, String website) {
            this.timeStamp = timeStamp;
            this.username = username;
            this.website = website;
        }
    }

    public List<String> mostVisitedPattern(String[] usernames, int[] timestamps, String[] websites) {
        List<Tuple> tuples = new ArrayList<>();
        for (int i = 0, n = usernames.length; i < n; i++) {
            String username = usernames[i];
            int timeStamp = timestamps[i];
            String website = websites[i];
            tuples.add(new Tuple(timeStamp, username, website));
        }
        // sort them by time stamp;
        Collections.sort(tuples, Comparator.comparingInt(t -> t.timeStamp));

        // key -> username, value: List of websites they visited;
        Map<String, List<String>> visitedMap = new HashMap<>();
        for (Tuple tuple : tuples) {
            String username = tuple.username;
            String website = tuple.website;
            List<String> list = visitedMap.getOrDefault(username, new ArrayList<>());
            list.add(website);
            visitedMap.put(username, list);
        }

        // key -> combination, value: frequency:
        Map<String, Integer> freqCntMap = new HashMap<>();
        int maxFreq = -1;
        String maxSequence = "";
        for (String username : visitedMap.keySet()) {
            Set<String> combinations = getTupleCombination(visitedMap.get(username));
            for (String combination : combinations) {
                int cnt = freqCntMap.getOrDefault(combination, 0);
                cnt++;
                freqCntMap.put(combination, cnt);
                if (cnt > maxFreq) {
                    maxSequence = combination;
                    maxFreq = cnt;
                }
                else if (cnt == maxFreq && combination.compareTo(maxSequence) < 0) {
                    maxSequence = combination;
                }
            }
        }
        return Arrays.asList(maxSequence.split("_"));
    }

    private Set<String> getTupleCombination(List<String> websites) {
        Set<String> set = new HashSet<>();
        for (int i = 0, n = websites.size(); i < n-2; i++) {
            for (int j = i+1; j < n-1; j++) {
                for (int k = j+1; k < n; k++) {
                    StringBuffer sb = new StringBuffer();
                    sb.append(websites.get(i)).append("_");
                    sb.append(websites.get(j)).append("_");
                    sb.append(websites.get(k));
                    set.add(sb.toString());
                }
            }
        }
        return set;
    }
}
