/**
Find the length of the longest substring T of a given string (consists of lowercase letters only) 
such that every character in T appears no less than k times.
Example 1:

Input:
s = "aaabb", k = 3

Output:
3

The longest substring is "aaa", as 'a' is repeated 3 times.
Example 2:

Input:
s = "ababbc", k = 2

Output:
5

The longest substring is "ababb", as 'a' is repeated 2 times and 'b' is repeated 3 times.
*/
public class Solution {
   /** 
    * Idea: Condition: We only contains lowercase letter.
    * First we count the number of unique characters in string s:
    * For num from 1 to the number of unique characters in string s, 
    * we use sliding window to check maxLength of substring str from string s
    * which contains exactly num unique characters, and for each characters, it appears at least k times.
    * For example, when num = 3 and k = 5, we are going to find the longest substring str that contains exact 3 unique characters
    * and each repeating 5 times.
   */
    // Running Time Complexity: O(N + L * N) where L is the number of distinct characters in String s, N is the length of string s.
    // O(26 * N): since string s will only contiains lower case characters.
    // Space Complexity: O(1). We treact O(256) as constant O(1)
    public int longestSubstring(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        // Count unique characters in string s:
        Set<Character> set = new HashSet<Character>();
        for (char c : s.toCharArray()) {
            set.add(c);
        }
        int maxLen = 0, size = set.size();
        for (int num = 1; num <= size; num++) {
            int[] map = new int[256];
            int uniqueCount = 0, l = 0, r = 0, n = s.length(), noLessThanK = 0;
            while (r < n) {
                // The numbet of unique character in substring(l,r) is less than num, we continue to expand r:
                if (uniqueCount <= num) {
                    char rc = s.charAt(r);
                    if (map[rc] == 0) {
                        uniqueCount++;
                    }
                    map[rc]++;
                    if (map[rc] == k) {
                        noLessThanK++;
                    }
                    r++;
                }
                // while substring(l, r) has too many unique character, we shrink l pointer until uniqueCount == num
                while (uniqueCount > num) {
                    char lc = s.charAt(l);
                    if (map[lc] == k){
                        noLessThanK--;
                    }
                    if (map[lc] == 1) {
                        uniqueCount--;
                    }
                    map[lc]--;
                    l++;
                }
                // At this point, the unique characters in substring(l, r) is num:
                // If num Of Characters no less than K is the same as num, we record the length 
                if (uniqueCount == noLessThanK) {
                    maxLen = Math.max(maxLen, r-l);
                }
            }
        }
        return maxLen;
    }
}