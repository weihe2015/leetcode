For num = 5 you should return [0,1,1,2,1,2].

Follow up:

It is very easy to come up with a solution with run time O(n*sizeof(integer)). But can you do it in linear time O(n) /possibly in a single pass?
Space complexity should be O(n).

public class Solution {
    public int[] countBits(int num) {
        int[] result = new int[num+1];
        for(int i = 1; i <= num; i++){
            result[i] = result[i >> 1] + (i & 1);
        }
        return result;
    }
}