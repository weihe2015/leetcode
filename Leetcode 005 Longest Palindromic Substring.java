public class Solution {
    // Naive Solution: O(N^3)
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int max = Integer.MIN_VALUE, start = -1, end = -1;
        for (int i = 0, n = s.length(); i < n; i++) {
            for (int j = i; j < n; j++) {
                if (isPalindrome(s, i, j)) {
                    if (max < j-i+1) {
                        max = j-i+1;
                        start = i;
                        end = j+1;
                    }
                }
            }
        }
        if (start == -1) {
            return "";
        }
        return s.substring(start, end);
    }

    private boolean isPalindrome(String s, int l, int r) {
        if (l > r) {
            return false;
        }
        while (l < r) {
            if (s.charAt(l) != s.charAt(r)) {
                return false;
            }
            l++; r--;
        }
        return true;
    }

    // Dynamic Programming:
    // Running Time: O(n^2), Space Complexity: O(n^2)
    /**
    Basic idea: when substring length is less than 3, only compare s.charAt(l) == s.charAt(r)
        when substring length is >= 3, use s.charAt(l) == s.charAt(r) && dp[l+1][r-1]
         bdb is palindrome
        l              r
        a   b   d   b  a is palindome
           l+1      r-1
        dp[i][j] = isPalindrome(s.substring(i,j))
        ==> dp[i][j] = dp[i+1][j-1] && s.charAt(i) == s.charAt(j)
    */
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int n = s.length();
        // dp[i][j]: substring(i, j) is the palindrome
        boolean[][] dp = new boolean[n][n];
        int start = -1;
        int end = -1;
        int max = -1;
        for (int r = 0; r < n; r++) {
            for (int l = r; l >= 0; l--) {
                // If substring is at least 3 characters long, use previous step to calculate current step:
                if (r - l >= 3) {
                    dp[l][r] = s.charAt(l) == s.charAt(r) && dp[l+1][r-1];
                }
                else {
                    dp[l][r] = s.charAt(l) == s.charAt(r);
                }
                // If s.substring(i, j+1) is palindrome:
                int len = r - l + 1;
                if (dp[l][r] && max < len) {
                    max = len;
                    start = l;
                    end = r;
                }
            }
        }
        // no substring palindrome is found:
        if (start == -1) {
            return "";
        }
        return s.substring(start, end+1);
    }

    // Running Time Complexity: O(n^2), Space Compexity: O(1)
    /**
    Basic Idea: For each character in String s, expand it from its index to two sides,
                until this expanded substring is not palindrome.
                Record the starting point of each expanded substring
                There are two cases: odd length palindrome and even length palindrome.
                   odd length palindrome: l = i, r = i: expandPalindrome(s, i, i, n);
                   even length palindrome: l = i, r = i+1: expandPalindrome(s, i, i+1, n);
    */
    public String longestPalindrome(String s) {
        if (s == null || s.length() <= 1) {
            return s;
        }
        int start = -1, max = Integer.MIN_VALUE;
        for (int i = 0, n = s.length(); i < n-1; i++) {
            // Expand from index i, odd length case:
            int[] res1 = expandPalindrome(s, i, i, n);
            // Expand from index i and i+1, even length case:
            int[] res2 = expandPalindrome(s, i, i+1, n);

            // Compute the substring len to get the larger one, and set the starting pointer.
            int len1 = res1[1] - res1[0] + 1;
            int len2 = res2[1] - res2[0] + 1;
            int maxLen = Math.max(len1, len2);
            if (max < maxLen) {
                max = maxLen;
                if (len1 > len2) {
                    start = res1[0];
                }
                else {
                    start = res2[0];
                }
            }
        }
        // No substring palindrome is found:
        if (start == -1) {
            return "";
        }
        else {
            return s.substring(start, start+max);
        }
    }


    /**
    This is a function to expand string from index l to two sides
    until its substring(l, r+1) does not form a palindrome
    @param: String s, original String
    @param: int l, left pointer
    @param: int r, right pointer
    @param: int n, length of String s
    @return: int array which contains start and end pointer of longest palindrome string, which is expanded from index l.
    */
    private int[] expandPalindrome(String s, int l, int r, int n) {
        int[] res = new int[2];
        if (l > r) {
            return res;
        }
        while (l >= 0 && r < n) {
            if (s.charAt(l) != s.charAt(r)) {
                break;
            }
            else {
                res[0] = l;
                res[1] = r;
            }
            l--; r++;
        }
        return res;
    }

    // Solution 3:
    // Running Time Complexity: O(N^2), Space Complexity: O(1)
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int maxLen = -1;
        int start = -1, end = -1;
        int n = s.length();
        // babad odd length palindrome:
        for (int mid = 0; mid < n; mid++) {
            for (int i = 1; mid - i >= 0 && mid + i < n; i++) {
                if (s.charAt(mid-i) != s.charAt(mid+i)) {
                    break;
                }
                int len = 2 * i + 1;
                if (len > maxLen) {
                    maxLen = len;
                    start = mid-i;
                    end = mid+i;
                }
            }
        }
        // cbbd, even length palidrome:
        for (int mid = 0; mid < n-1; mid++) {
            for (int i = 1; mid - i + 1 >= 0 && mid + i < n; i++) {
                if (s.charAt(mid-i+1) != s.charAt(mid+i)) {
                    break;
                }
                int len = 2 * i;
                if (len > maxLen) {
                    maxLen = len;
                    start = mid-i+1;
                    end = mid+i;
                }
            }
        }
        if (start == -1) {
            return s.substring(0, 1);
        }
        return s.substring(start, end+1);
    }

    public String longestPalindrome(String s) {
        if(s == null || s.length() <= 1)
            return s;
        String result = "";
        int curr = 0;
        for(int i = 0; i < s.length(); i++){
            if(isPalindrome(s,i-curr-1,i)){
                result = s.substring(i-curr-1,i+1);
                curr = curr + 2;
            }
            else if(isPalindrome(s,i-curr,i)){
                result = s.substring(i-curr,i+1);
                curr = curr + 1;
            }
        }
        return result;
    }


}