/*
Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row below.

For example, given the following triangle
[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

Note:
Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the triangle.
*/

public class Solution {
    // Buttom up DP: 1 Dimension: Complexity: O(m * m), Space Complexity: O(m)
    public int minimumTotal(List<List<Integer>> triangle) {
        if (triangle == null || triangle.isEmpty()) {
            return 0;
        }
        int m = triangle.size();
        int[] dp = new int[m+1];
        for (int i = m-1; i >= 0; i--) {
            List<Integer> list = triangle.get(i);
            for (int j = 0, n = list.size(); j < n; j++) {
                dp[j] = Math.min(dp[j], dp[j+1]) + list.get(j);
            }
        }
        return dp[0];
    }

    // Running Time Complexity: O(m * m), Space Complexity: O(m * m)
    public int minimumTotal(List<List<Integer>> triangle) {
        if (triangle == null || triangle.isEmpty()) {
            return 0;
        }
        int m = triangle.size();
        int[][] dp = new int[m][m];
        dp[0][0] = triangle.get(0).get(0);
        for (int i = 1; i < m; i++) {
            List<Integer> list = triangle.get(i);
            for (int j = 0, n = list.size(); j < n; j++) {
                int l = Math.max(0, j-1);
                int r = Math.min(j, triangle.get(i-1).size()-1);
                dp[i][j] = Math.min(dp[i-1][l], dp[i-1][r]) + list.get(j);
            }
        }
        int minCost = Integer.MAX_VALUE;
        for (int k = 0; k < m; k++) {
            minCost = Math.min(minCost, dp[m-1][k]);
        }
        return minCost;
    }
}