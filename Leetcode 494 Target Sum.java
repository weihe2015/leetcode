/**
You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.

Find out how many ways to assign symbols to make sum of integers equal to target S.

Example 1:
Input: nums is [1, 1, 1, 1, 1], S is 3. 
Output: 5
Explanation: 

-1+1+1+1+1 = 3
+1-1+1+1+1 = 3
+1+1-1+1+1 = 3
+1+1+1-1+1 = 3
+1+1+1+1-1 = 3

There are 5 ways to assign symbols to make the sum of nums be target 3.
Note:
The length of the given array is positive and will not exceed 20.
The sum of elements in the given array will not exceed 1000.
Your output answer is guaranteed to be fitted in a 32-bit integer.
*/
public class Solution {
    // BackTracking Solution:
    public int findTargetSumWays(int[] nums, int S) {
        return findTargetSum(nums, S, 0);
    }
    
    private int findTargetSum(int[] nums, int sum, int level) {
        if (level == nums.length) {
            if (sum == 0) {
                return 1;
            }
            else {
                return 0;
            }
        }
        int addVal = findTargetSum(nums, sum + nums[level], level+1);
        int minusVal = findTargetSum(nums, sum - nums[level], level+1);
        return addVal + minusVal;
    }

    // backtracking with memorization:
    public int findTargetSumWays(int[] nums, int S) {
        return findTargetSum(nums, S, 0, new HashMap<String, Integer>());
    }
    
    private int findTargetSum(int[] nums, int sum, int level, Map<String, Integer> map) {
        String path = level + "->" + sum;
        if (map.containsKey(path)) {
            return map.get(path);
        }
        if (level == nums.length) {
            if (sum == 0) {
                return 1;
            }
            else {
                return 0;
            }
        }
        int addVal = findTargetSum(nums, sum + nums[level], level+1, map);
        int minusVal = findTargetSum(nums, sum - nums[level], level+1, map);
        int val = addVal + minusVal;
        map.put(path, val);
        return val;
    }

    // Solution 3:
    // https://leetcode.com/problems/target-sum/discuss/97335/Short-Java-DP-Solution-with-Explanation
    public int findTargetSumWays(int[] nums, int S) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum < S || nums.length == 0) {
            return 0;
        }
        int[] dp = new int[2*sum + 1];
        dp[0 + sum] = 1;
        for (int num : nums) {
            int[] next = new int[2*sum + 1];
            for (int k = 0; k < 2*sum+1; k++) {
                if (dp[k] != 0) {
                    next[k + num] += dp[k];
                    next[k - num] += dp[k];
                }
            }
            dp = next;
        }
        return dp[sum + S];
    }
    // Solution 4:
    // https://leetcode.com/problems/target-sum/discuss/152284/Accepted-JAVA-Solution-DP
    public int findTargetSumWays(int[] nums, int S) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum < S || nums.length == 0) {
            return 0;
        }
        int diff = sum - S;
        int[] dp = new int[diff + 1];
        dp[0] = 1;
        for (int num : nums) {
            for (int i = diff; i >= num * 2; i--) {
                dp[i] += dp[i - num*2];
            }
        }
        return dp[diff];
    }
}