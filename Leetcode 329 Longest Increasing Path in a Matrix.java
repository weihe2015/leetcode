/**
Given an integer matrix, find the length of the longest increasing path.

From each cell, you can either move to four directions: left, right, up or down. You may NOT move diagonally or move outside of the boundary (i.e. wrap-around is not allowed).

Example 1:

nums = [
  [9,9,4],
  [6,6,8],
  [2,1,1]
]
Return 4
The longest increasing path is [1, 2, 6, 9].

Example 2:

nums = [
  [3,4,5],
  [3,2,6],
  [2,2,1]
]
Return 4
The longest increasing path is [3, 4, 5, 6]. Moving diagonally is not allowed.

1. Do DFS from every cell
2. Compare every 4 direction and skip cells that are out of boundary or smaller
3. Get matrix max from every cells max
4. Use matrix[x][y] <= matrix[i][j] so we dont need a visited[m][n] array
5. The key is to cache the distance because its highly possible to revisit a cell
*/
public class Solution {
    // Bruth Force DFS without memorization:
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        boolean[][] visited = new boolean[m][n];
        int maxLen = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int len = dfs(matrix, visited, i, j, m, n, 0, Integer.MIN_VALUE);
                maxLen = Math.max(maxLen, len);
            }
        }
        return maxLen;
    }
    
    private int dfs(int[][] matrix, boolean[][] visited, int i, int j, int m, int n, int maxLen, int prevNum) {
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j] || matrix[i][j] <= prevNum) {
            return len;
        }
        visited[i][j] = true;
        maxLen++;
        int prevLen = maxLen;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            // for each direction, the currLen will be prevLen + dfs();
            // maxLen will be the max of four directions
            int currLen = prevLen + dfs(matrix, visited, x, y, m, n, 0, matrix[i][j]);
            maxLen = Math.max(maxLen, currLen);
        }
        visited[i][j] = false;
        return maxLen;
    }

    // Solution 2:
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        boolean[][] visited = new boolean[m][n];
        int maxLen = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int len = dfs(matrix, visited, i, j, m, n, Integer.MIN_VALUE);
                maxLen = Math.max(maxLen, len);
            }
        }
        return maxLen;
    }
    
    private int dfs(int[][] matrix, boolean[][] visited, int i, int j, int m, int n, int prevNum) {
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j] || matrix[i][j] <= prevNum) {
            return len;
        }
        visited[i][j] = true;
        int maxLen = 1;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            // for each direction, the currLen will be 1 + dfs();
            // maxLen will be the max of four directions
            int currLen = 1 + dfs(matrix, visited, x, y, m, n, 0, matrix[i][j]);
            maxLen = Math.max(maxLen, currLen);
        }
        visited[i][j] = false;
        return maxLen;
    }

    // Solution 2: DFS with memorization
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        // 记忆化搜索，省去很大的空间
        int[][] cached = new int[m][n];
        int maxLen = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int len = dfs(matrix, cached, i, j, m, n, Integer.MIN_VALUE);
                maxLen = Math.max(maxLen, len);
            }
        }
        return maxLen;
    }
    
    private int dfs(int[][] matrix, int[][] cached, int i, int j, int m, int n, int prevNum) {
        if (i < 0 || i >= m || j < 0 || j >= n || matrix[i][j] <= prevNum) {
            return 0;
        }
        if (cached[i][j] != 0) {
            return cached[i][j];
        }
        // 自身长度至少是1
        int maxLen = 1;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            int currLen = 1 + dfs(matrix, cached, x, y, m, n, matrix[i][j]);
            maxLen = Math.max(maxLen, currLen);
        }
        // 四个方向更新完之后，记得保存以这个点出发的最长路径，下次就不用算了
        cached[i][j] = maxLen;
        return maxLen;
    }
}