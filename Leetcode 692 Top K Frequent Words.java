/** 
Given a non-empty list of words, return the k most frequent elements.

Your answer should be sorted by frequency from highest to lowest. If two words have the same frequency, 
then the word with the lower alphabetical order comes first.

Example 1:
Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
Output: ["i", "love"]
Explanation: "i" and "love" are the two most frequent words.
    Note that "i" comes before "love" due to a lower alphabetical order.
Example 2:
Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
Output: ["the", "is", "sunny", "day"]
Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
    with the number of occurrence being 4, 3, 2 and 1 respectively.
*/
class Solution {

    // Running Time Complexity: O(NlogK), Space Complexity: O(N)
    public List<String> topKFrequent(String[] words, int k) {
        List<String> res = new ArrayList<>();
        if (words == null || words.length == 0) {
            return res;
        }
        Map<String, Integer> freqMap = new HashMap<>();
        for (String word : words) {
            freqMap.put(word, freqMap.getOrDefault(word, 0) + 1);
        }

        Queue<FreqWord> pq = new PriorityQueue<>(new Comparator<>(){
            @Override
            public int compare(FreqWord fw1, FreqWord fw2) {
                // If two words frequency the same, we want the lower alphabetical order.
                if (fw1.freq != fw2.freq) {
                    return Integer.compare(fw2.freq, fw1.freq);
                }
                else {
                    return fw1.word.compareTo(fw2.word);
                }
            }
        });
        
        for (String word : freqMap.keySet()) {
            int freq = freqMap.get(word);
            pq.add(new FreqWord(freq, word));
        }

        while (!pq.isEmpty() && res.size() < k) {
            res.add(pq.poll().word);
        }
        return res;
    }
    
    class FreqWord {
        int freq;
        String word;
        public FreqWord(int freq, String word) {
            this.freq = freq;
            this.word = word;
        }
    }
}