/*
The set [1,2,3,…,n] contains a total of n! unique permutations.

By listing and labeling all of the permutations in order,
We get the following sequence (ie, for n = 3):

"123"
"132"
"213"
"231"
"312"
"321"
Given n and k, return the kth permutation sequence.
*/
public class Solution {
    public String getPermutation(int n, int k) {
        // create an array for factorial lookup:
        // [1,1,2,6,24,...,n!]
        int sum = 1;
        int[] factorial = new int[n+1];
        factorial[0] = sum;
        for (int i = 1; i <= n; i++) {
            sum *= i;
            factorial[i] = sum;
        }
        
        // create a list of numbers:
        List<Integer> nums = new ArrayList<Integer>();
        for (int i = 1; i <= n; i++) {
            nums.add(i);
        }
        // subtract 1 because of things always starting at 0
        k--;
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= n; i++) {
            int idx = k / factorial[n-i];
            int num = nums.get(idx);
            sb.append(num);
            nums.remove(idx);
            k -= idx * factorial[n-i];
        }
        return sb.toString();
    }
}