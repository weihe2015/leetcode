/**
Given a binary array, find the maximum number of consecutive 1s in this array if you can flip at most one 0.

Example 1:
	Input:  nums = [1,0,1,1,0]
	Output:  4

	Explanation:
	Flip the first zero will get the the maximum number of consecutive 1s.
	After flipping, the maximum number of consecutive 1s is 4.

Example 2:
	Input: nums = [1,0,1,0,1]
	Output:  3

	Explanation:
	Flip each zero will get the the maximum number of consecutive 1s.
	After flipping, the maximum number of consecutive 1s is 3.

Note:
The input array will only contain 0 and 1.
The length of input array is a positive integer and will not exceed 10,000

Follow up:
What if the input numbers come in one by one as an infinite stream?
In other words, you can't store all numbers coming from the stream
as it's too large to hold in memory. Could you solve it efficiently?

Follow up:
上面那种方法对于follow up中的情况无法使用，因为nums[left]需要访问之前的数字。我们可以将遇到的0的位置全都保存下来，
这样我们需要移动left的时候就知道移到哪里了：
*/

public class Solution {
    /**
     * @param nums: a list of integer
     * @return: return a integer, denote the maximum number of consecutive 1s
     */
    // Running Time Complexity: O(N), Space Complexity: O(k)
    public int findMaxConsecutiveOnes(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int maxLen = 0, l = 0, r = 0, n = nums.length;
        int[] count = {0,0};
        boolean containsLessThanTwoZeros = true;
        while (r < n) {
            if (containsLessThanTwoZeros) {
                int num = nums[r];
                count[num]++;
                if (count[0] == 2) {
                    containsLessThanTwoZeros = false;
                }
                r++;
            }
            while (!containsLessThanTwoZeros) {
                int num = nums[l];
                count[num]--;
                if (count[0] == 1) {
                    containsLessThanTwoZeros = true;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // Follow up: You cannot use nums[l] or nums[r] to get the number,
    public int findMaxConsecutiveOnes(int[] nums) {
        int l = 0;
        int r = 0;
        int n = nums.length;
        int k = 1;
        int maxLen = 0;
        Queue<Integer> q = new LinkedList<>();
        while (r < n) {
            if (nums[r] == 0) {
                q.offer(r);
            }
            if (q.size() > k) {
                l = q.poll() + 1;
            }
            r++;
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }
}