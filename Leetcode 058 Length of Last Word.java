Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string.

If the last word does not exist, return 0.

Note: A word is defined as a character sequence consists of non-space characters only.

For example, 
Given s = "Hello World",
return 5.

public class Solution {
    // my solution
    public int lengthOfLastWord(String s) {
        String str = s.trim();
        int n = str.length();
        int count = 0;
        for(int i = n-1; i >= 0; i--){
            if(str.charAt(i) != ' ')
                count++;
            else
                break;
        }
        return count;
    }
	// solution 1
    public int lengthOfLastWord(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        String[] strs = s.split(" ");
        if (strs.length == 0) {
            return 0;
        }
        return strs[strs.length-1].length();
    }
    
    // solution 2
    public int lengthOfLastWord(String s) {
        int n = s.length(), lastLength=0;
    
        while(n > 0 && s.charAt(n-1)==' '){
            n--;
        }
    
        while(n > 0 && s.charAt(n-1)!=' '){
            lastLength++;
            n--;
        }
    
        return lastLength;
    }
}
