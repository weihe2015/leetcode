/*
Design and implement a TwoSum class. It should support the following operations: add and find.

add - Add the number to an internal data structure.
find - Find if there exists any pair of numbers which sum is equal to the value.

For example,
add(1); add(3); add(5);
find(4) -> true
find(7) -> false
*/
public class TwoSum {
    // Key -> number, value -> its occurence:
    // Running Time Complexity of add: O(1)
    // Running Time Complexity of find: O(N), if we have N distinct values. Space Complexity: O(1)
    Map<Integer, Integer> map;
    public TwoSum() {
        map = new HashMap<Integer, Integer>();
    }
    /**
     * @param number: An integer
     * @return: nothing
     */
    public void add(int number) {
        map.put(number, map.getOrDefault(number, 0) + 1);
    }

    /**
     * @param value: An integer
     * @return: Find if there exists any pair of numbers which sum is equal to the value.
     */
    public boolean find(int value) {
        for (int key : map.keySet()) {
            int diff = value - key;
            // Example: add(2), find(4), diff = 2,
            if (key != diff && map.containsKey(diff)) {
                return true;
            }
            // Example: add(2), add(2), find(4), diff = 2
            else if (key == diff && map.get(key) > 1) {
                return true;
            }
        }
        return false;
    }

    // Naive Solution:
    // List of nums as storage, and solution of two sum I.
    // Running Time Complexity of add: O(1)
    // Running Time Complexity of find: O(N), Space Complexity: O(N)
    private List<Integer> nums = new ArrayList<>();
    /**
     * @param number: An integer
     * @return: nothing
     */
    public void add(int number) {
        // write your code here
        nums.add(number);
    }

    /**
     * @param value: An integer
     * @return: Find if there exists any pair of numbers which sum is equal to the value.
     */
    public boolean find(int target) {
        // write your code here
        Set<Integer> map = new HashSet<>();
        for (int i = 0, max = nums.size(); i < max; i++) {
            int diff = target - nums.get(i);
            if (map.contains(diff)) {
                return true;
            }
            else {
                map.add(nums.get(i));
            }
        }
        return false;
    }
}