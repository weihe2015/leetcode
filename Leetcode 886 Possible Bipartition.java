/**
Given a set of N people (numbered 1, 2, ..., N), we would like to split everyone into two groups of any size.

Each person may dislike some other people, and they should not go into the same group.

Formally, if dislikes[i] = [a, b], it means it is not allowed to put the people numbered a and b into the same group.

Return true if and only if it is possible to split everyone into two groups in this way.

Example 1:

Input: N = 4, dislikes = [[1,2],[1,3],[2,4]]
Output: true
Explanation: group1 [1,4], group2 [2,3]
Example 2:

Input: N = 3, dislikes = [[1,2],[1,3],[2,3]]
Output: false
Example 3:

Input: N = 5, dislikes = [[1,2],[2,3],[3,4],[4,5],[1,5]]
Output: false

Note:
1 <= N <= 2000
0 <= dislikes.length <= 10000
1 <= dislikes[i][j] <= N
dislikes[i][0] < dislikes[i][1]
There does not exist i != j for which dislikes[i] == dislikes[j].
*/
public class Solution {
    public boolean possibleBipartition(int N, int[][] dislikes) {
        int[][] graph = new int[N][N];
        for (int[] dislike : dislikes) {
            int i = dislike[0] - 1;
            int j = dislike[1] - 1;
            graph[i][j] = 1;
            graph[j][i] = 1;
        }
        int[] visited = new int[N];
        for (int i = 0; i < N; i++) {
            if (visited[i] == 0) {
                visited[i] = 1;
                if (!isBipartiteDFS(graph, visited, i, N)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isBipartiteDFS(int[][] graph, int[] visited, int u, int N) {
        for (int v = 0; v < N; v++) {
            if (graph[u][v] == 1) {
                if (visited[v] == 0) {
                    visited[v] = visited[u] == 1 ? 2 : 1;
                    if (!isBipartiteDFS(graph, visited, v, N)) {
                        return false;
                    }
                }
                else if (visited[u] == visited[v]) {
                    return false;
                }
            }
        }
        return true;
    }

    // Adjacent List Solution:
    public boolean possibleBipartition(int N, int[][] dislikes) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            graph.add(new ArrayList<Integer>());
        }
        for (int[] dislike : dislikes) {
            int i = dislike[0] - 1;
            int j = dislike[1] - 1;
            graph.get(i).add(j);
            graph.get(j).add(i);
        }
        // 0 -> not visited, 1 -> black, 2 -> white
        int[] visited = new int[N];
        for (int i = 0; i < N; i++) {
            if (visited[i] == 0) {
                visited[i] = 1;
                if (!isBipartiteDFS(graph, visited, i)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isBipartiteDFS(List<List<Integer>> graph, int[] visited, int from) {
        for (int u : graph.get(from)) {
            if (visited[u] == 0) {
                visited[u] = visited[from] == 1 ? 2 : 1;
                if (!isBipartiteDFS(graph, visited, u)) {
                    return false;
                }
            }
            else if (visited[u] == visited[from]) {
                return false;
            }
        }
        return true;
    }

    // BFS:
    public boolean possibleBipartition(int N, int[][] dislikes) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            graph.add(new ArrayList<Integer>());
        }
        for (int[] dislike : dislikes) {
            int i = dislike[0] - 1;
            int j = dislike[1] - 1;
            graph.get(i).add(j);
            graph.get(j).add(i);
        }
        // 0 -> not visited, 1 -> black, 2 -> white
        int[] visited = new int[N];
        for (int i = 0; i < N; i++) {
            if (visited[i] == 0) {
                visited[i] = 1;
                if (!isBipartiteBFS(graph, visited, i)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isBipartiteBFS(List<List<Integer>> graph, int[] visited, int from) {
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.offer(from);
        while (!queue.isEmpty()) {
            int v = queue.poll();
            for (int u : graph.get(v)) {
                if (visited[u] == 0) {
                    visited[u] = visited[v] == 1 ? 2 : 1;
                    queue.offer(u);
                }
                else if (visited[u] == visited[v]) {
                    return false;
                }
            }
        }
        return true;
    }
}