/**
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

Count the number of distinct islands. An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.

Example 1:
11000
11000
00011
00011
Given the above grid map, return 1.
Example 2:
11011
10000
00001
11011
Given the above grid map, return 3.

Notice that:
11
1
and
 1
11
are considered different island shapes, because we do not consider reflection / rotation.
Note: The length of each dimension in the given grid does not exceed 50.
*/
public class Solution {
    // https://leetcode.jp/problemdetail.php?id=694
    // https://www.lintcode.com/problem/number-of-distinct-islands
    // https://leetcode.com/problems/number-of-distinct-islands
    /**
     * @param grid: a list of lists of integers
     * @return: return an integer, denote the number of distinct islands
     */
    /**
    可以通过相对位置坐标来判断，比如使用岛屿的最左上角的1当作基点
    encode 成String，用DFS的起始点来做相对坐标

    由于是有平移操作，使用的 trick 是利用相对位移保持不变的特性来确定同一种同一种岛屿形状的
    */
    // DFS:
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numberofDistinctIslands(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        
        boolean[][] visited = new boolean[m][n];
        Set<String> set = new HashSet<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0 || visited[i][j]) {
                    continue;
                }
                List<String> list = new ArrayList<>();
                dfs(grid, i, j, i, j, m, n, visited, list);
                StringBuffer sb = new StringBuffer();
                for (String s : list) {
                    sb.append(s);
                }
                set.add(sb.toString());
            }
        }
        return set.size();
    }
    
    private void dfs(int[][] grid, int i, int j, int i0, int j0, int m, int n, boolean[][] visited, List<String> list) {
        if (visited[i][j]) {
            return;
        }
        visited[i][j] = true;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] == 0 || visited[x][y]) {
                continue;
            }
            StringBuffer sb = new StringBuffer();
            sb.append(x-i0).append("_").append(y-j0);
            list.add(sb.toString());
            dfs(grid, x, y, i0, j0, m, n, visited, list);
        }
    }

    // BFS:
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numberofDistinctIslands(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        
        Set<String> set = new HashSet<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0 || visited[i][j]) {
                    continue;
                }
                Queue<int[]> queue = new LinkedList<>();
                queue.offer(new int[]{i, j});
                
                List<String> list = new ArrayList<>();
                visited[i][j] = true;
                while (!queue.isEmpty()) {
                    int[] curr = queue.poll();
                    int i0 = curr[0];
                    int j0 = curr[1];
                    
                    for (int[] dir : dirs) {
                        int x = i0 + dir[0];
                        int y = j0 + dir[1];
                        if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] == 0 || visited[x][y]) {
                            continue;
                        }
                        StringBuffer sb = new StringBuffer();
                        sb.append(x-i).append("_").append(y-j);
                        list.add(sb.toString());
                        visited[x][y] = true;
                        
                        queue.offer(new int[]{x, y});
                    }
                }
                StringBuffer sb = new StringBuffer();
                for (String s : list) {
                    sb.append(s);
                }
                set.add(sb.toString());
            }
        }
        return set.size();
    }
}