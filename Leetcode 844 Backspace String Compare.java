/**
Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace character.

Example 1:

Input: S = "ab#c", T = "ad#c"
Output: true
Explanation: Both S and T become "ac".
Example 2:

Input: S = "ab##", T = "c#d#"
Output: true
Explanation: Both S and T become "".
Example 3:

Input: S = "a##c", T = "#a#c"
Output: true
Explanation: Both S and T become "c".
Example 4:

Input: S = "a#c", T = "b"
Output: false
Explanation: S becomes "c" while T becomes "b".
Note:

1 <= S.length <= 200
1 <= T.length <= 200
S and T only contain lowercase letters and '#' characters.
Follow up:

Can you solve it in O(N) time and O(1) space?
*/
public class Solution {

    // Running Time Complexity: O(M + N), Space Complexity: O(M + N)
    public boolean backspaceCompare(String S, String T) {
        Deque<Character> stack1 = new ArrayDeque<Character>();
        Deque<Character> stack2 = new ArrayDeque<Character>();
        for (char c : S.toCharArray()) {
            if (c != '#') {
                stack1.push(c);
            }
            else if (!stack1.isEmpty()){
                stack1.pop();
            }
        }
        for (char c : T.toCharArray()) {
            if (c != '#') {
                stack2.push(c);
            }
            else if (!stack2.isEmpty()){
                stack2.pop();
            }
        }
        if (stack1.size() != stack2.size()) {
            return false;
        }
        while (!stack1.isEmpty() && !stack2.isEmpty()) {
            char c1 = stack1.pop();
            char c2 = stack2.pop();
            if (c1 != c2) {
                return false;
            }
        }
        return true;
    }

    // Two Pointer Solution:
    public boolean backspaceCompare(String S, String T) {
        int i = S.length() - 1, j = T.length() - 1, countS = 0, countT = 0;
        while (i >= 0 || j >= 0) {
            while (i >= 0 && (S.charAt(i) == '#' || countS > 0)) {
                if (S.charAt(i) == '#') {
                    countS++;
                }
                else {
                    countS--;
                }
                i--;
            }
            while (j >= 0 && (T.charAt(j) == '#' || countT > 0)) {
                if (T.charAt(j) == '#') {
                    countT++;
                }
                else {
                    countT--;
                }
                j--;
            }
            // If any of String S or T has deleted by #, i or j will = -1 
            if (i < 0 || j < 0) {
                return i == j;
            }
            if (S.charAt(i) != T.charAt(j)) {
                return false;
            }
            i--; j--;
        }
        return true;
    }
}