/**
Follow up for "Find Minimum in Rotated Sorted Array":
What if duplicates are allowed?

Would this affect the run-time complexity? How and why?
Suppose a sorted array is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.

The array may contain duplicates.
*/
public class Solution {
    // Running time O(n), in worst case: because it may be the case: [3,1,3,3,3,3,3,3]
    public int findMin(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        while (low < high) {
            int mid = low + (high - low)/2;
            if (nums[low] < nums[high]) {
                return nums[low];
            }
            // if min is in right subarray
            if (nums[mid] > nums[high]) {
                low = mid + 1;
            }
            // if min is in left subarray
            // ex: [3,1,3], cannot skip nums[mid] because it may be the min item.
            else if (nums[mid] < nums[high]) {
                high = mid;
            }
            else {
                high--;
            }
        }
        return nums[low];
    }
}