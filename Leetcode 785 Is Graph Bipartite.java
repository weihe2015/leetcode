public class Solution {
    // BFS: If not visited, mark the color as opposite of the parent and push it back to Queue
    // Else: If the node's color is the same as their parent, that means there is no way we can sperated graph into two part.
    // BFS:
    public boolean isBipartite(int[][] graph) {
        int n = graph.length;
        // 0 => not visited, 1 => black, 2 => white
        int[] visited = new int[n];
        for (int i = 0; i < n; i++) {
            if (visited[i] == 0) {
                visited[i] = 1;
                Queue<Integer> queue = new LinkedList<>();
                queue.offer(i);
                while (!queue.isEmpty()) {
                    int u = queue.poll();
                    for (int v : graph[u]) {
                        if (visited[v] == 0) {
                            queue.offer(v);
                            visited[v] = visited[u] == 1 ? 2 : 1;
                        }
                        else if (visited[u] == visited[v]) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    // DFS:
    public boolean isBipartite(int[][] graph) {
        int n = graph.length;
        // 0 => not visited, 1 => black 2 => white
        int[] visited = new int[n];
        for (int i = 0; i < n; i++) {
            if (visited[i] == 0) {
                visited[i] = 1;
                if (!isBipartiteDFS(graph, visited, i)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isBipartiteDFS(int[][] graph, int[] visited, int u) {
        for (int u : graph[u]) {
            if (visited[u] == 0) {
                visited[u] = visited[u] == 1 ? 2 : 1;
                if (!isBipartiteDFS(graph, visited, u)) {
                    return false;
                }
            }
            else if (visited[u] == visited[u]) {
                return false;
            }
        }
        return true;
    }
}