/*
Suppose a sorted array is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.

You may assume no duplicate exists in the array.
*/
/**
 * Three cases:
 * 1. The leftmost value is less than the rightmost value in the list: This means that the list is not rotated.
 *   ex: [1,2,4,5,6,9], low = 0, high = 6, mid = 3, 5 < 9
 * 2. The value in the middle of the list is > than the leftmost and rightmost values in the list.
 *  ex: [4,5,6,7,0,1,2], low = 0, high = 6, mid = 3, 7 > 2 => low = mid + 1
 * 3. The value in the middle of the list is < the leftmost and rightmost values in the list.
 *  ex: [5,6,7,0,1,2,4], low = 0, high = 6, mid = 3, 0 < 4 => high = mid;
 *
*/

public class Solution {
    public int findMin(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int n = nums.length;
        int low = 0;
        int high = n - 1;
        int minVal = Integer.MAX_VALUE;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            minVal = Math.min(minVal, nums[mid]);
            if (nums[mid] < nums[high]) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return minVal;
    }

    public int findMin(int[] nums) {
        if (nums.length == 0 || nums == null) {
            return 0;
        }
        int start = 0;
        int end = nums.length - 1;
        while (start < end) {
            int mid = (start + end) / 2;
            if (nums[mid] < nums[end]) {
                end = mid;
            }
            else {
                start = mid + 1;
            }
        }
        return nums[start];
    }

    // Naive Solution:
    // Running Time Complexity: O(N)
    public int findMin(int[] nums) {
        if (nums == null) {
            return -1;
        }
        int minVal = Integer.MAX_VALUE;
        for (int num : nums) {
            if (num < minVal) {
                minVal = num;
            }
        }
        return minVal;
    }

    // my solution
    public int findMin(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int n = nums.length;
        int low = 0;
        int high = n - 1;
        while (low < high) {
            int mid = low + (high - low)/2;
            if (nums[low] < nums[high]) {
                return nums[low];
            }
            if (nums[mid] > nums[high]) {
                low = mid + 1;
            }
            else {
                high = mid;
            }
        }
        // end with low == high
        return nums[low];
    }

    // No need to consider boundary
    public int findMin(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int low = 0, high = nums.length - 1;
        while (low + 1 < high) {
            int mid = low + (high - low)/2;
            if (nums[mid] < nums[high]) {
                high = mid;
            }
            else {
                low = mid;
            }
        }
        return Math.min(nums[low], nums[high]);
    }
}