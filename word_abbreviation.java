import java.util.*;

public class solution {

  public static void abbreviation(String[] wordList){
		// key -> l10n, value -> TrieNode root
		Map<String, TrieNode> map = new HashMap<>();
		List<String> result = new ArrayList<String>();
		// build trie
		for(String word : wordList){
			int n = word.length();
			String key = "" + word.charAt(0) + n + word.charAt(n-1) + "";
			if(!map.containsKey(map))
				map.put(key,new TrieNode());
			insert(word,map.get(key));
		}
		for(String word : wordList){
			int n = word.length();
			String key = "" + word.charAt(0) + n + word.charAt(n-1) + "";
			String compress = search(word,map.get(key));
			result.add(compress);
		}
	}

  public static String search(String word, TrieNode root) {
    TrieNode temp = root, node = root;
    int n = word.length();
    for (int i = 0; i < n; i++) {
      if (temp.wordCount > 1)
        node = temp;
      if (i == n - 1 && node == root) {
        String compressed = "" + word.charAt(0) + n + word.charAt(n - 1) + "";
        if (compressed.length() >= n)
          return word;
        else
          return compressed;
      }
      temp = temp.map.get(word.charAt(i));
    }
    temp = root;
    StringBuffer sb = new StringBuffer();
    for (char c : word.toCharArray()) {
      if (temp != node) {
        sb.append(c);
        temp = temp.map.get(c);
      } else {
        sb.append(c);
        break;
      }
    }

    String compressed = sb.toString() + n + word.charAt(n - 1) + "";
    if (compressed.length() >= word.length())
      return word;
    else
      return compressed;
  }

  public static void insert(String word, TrieNode root) {
    TrieNode node = root;
    for (char c : word.toCharArray()) {
      if (!node.map.containsKey(c))
        node.map.put(c, new TrieNode());
      node.wordCount++;
      node = node.map.get(c);
    }
  }
}

class Trie {
  char c;
  HashMap<Character, TrieNode> map = new HashMap<>();
  int wordCount = 0;

  public Trie() {
  }
}