/**
Given a 01 matrix M, find the longest line of consecutive one in the matrix. 
The line could be horizontal, vertical, diagonal or anti-diagonal.
Example:
Input:
[[0,1,1,0],
 [0,1,1,0],
 [0,0,0,1]]
Output: 3
Hint: The number of elements in the given matrix will not exceed 10,000.
*/
public class Solution {
    // Solution 1: scan horizontal, vertical and diagonal:
    /**
     * @param M: the 01 matrix
     * @return: the longest line of consecutive one in the matrix
     */
    public int longestLine(int[][] M) {
        // Write your code here
        if (M == null || M.length == 0) {
            return 0;
        }
        int m = M.length;
        int n = M[0].length;
        int res = 0;
        // check horizontal:
        for (int i = 0; i < m; i++) {
            int cnt = 0;
            for (int j = 0; j < n; j++) {
                if (M[i][j] == 1) {
                    cnt++;
                    res = Math.max(res, cnt);
                }
                else {
                    cnt = 0;
                }
            }
        }
        
        // check vertical:
        for (int j = 0; j < n; j++) {
            int cnt = 0;
            for (int i = 0; i < m; i++) {
                if (M[i][j] == 1) {
                    cnt++;
                    res = Math.max(res, cnt);
                }
                else {
                    cnt = 0;
                }
            }
        }
        
        // check diagonal and anti-diagonal:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (M[i][j] == 0) {
                    continue;
                }
                int x = i;
                int y = j;
                int count = 0;
                while (x < m && y < n && M[x][y] == 1) {
                    count++;
                    x += 1;
                    y += 1;
                }
                res = Math.max(res, count);
                
                x = i;
                y = j;
                count = 0;
                while (x >= 0 && y >= 0 && M[x][y] == 1) {
                    count++;
                    x--;
                    y--;
                }
                res = Math.max(res, count);
            }
        }
        return res;
    }

    // Solution 2 DFS:
    static private final int[][] dirs = {{1,0},{0,1},{-1,-1},{1,1}};
    public int longestLine(int[][] M) {
        // Write your code here
        if (M == null || M.length == 0) {
            return 0;
        }
        int m = M.length;
        int n = M[0].length;
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (M[i][j] == 0) {
                    continue;
                }
                for (int[] dir : dirs) {
                    int x = i;
                    int y = j;
                    int count = 0;
                    while (x >= 0 && x < m && y >= 0 && y < n && M[x][y] == 1) {
                        x += dir[0];
                        y += dir[1];
                        count++;
                    }
                    res = Math.max(res, count);
                }
            }
        }
        return res;
    }
}