/**
Given a sorted array of integers, find the starting and ending position of a given target value.

Your algorithm's runtime complexity must be in the order of O(log n).

If the target is not found in the array, return [-1, -1].

For example,
Given [5, 7, 7, 8, 8, 10] and target value 8,
return [3, 4].

Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
 *
*/

public class Solution {
    public int[] searchRange(int[] nums, int target) {
        int[] res = {-1, -1};
        if (nums == null || nums.length == 0) {
            return res;
        }
        int fstPos = binarySearchFirstPosition(nums, target);
        if (fstPos == -1) {
            return res;
        }
        res[0] = fstPos;
        res[1] = binarySearchLastPosition(nums, target);
        return res;
    }

    private int binarySearchFirstPosition(int[] nums, int target) {
        int low = 0, high = nums.length - 1, ans = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                ans = mid;
                // bias to the left, because we are finding the left bounder
                high = mid - 1;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return ans;
    }

    private int binarySearchLastPosition(int[] nums, int target) {
        // low can start from left bounder of first function, which may help the running time a little.
        // low = ans; high = nums.length - 1;
        int low = 0, high = nums.length - 1, ans = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                ans = mid;
                // bias to the right, because we are finding the right bounder
                low = mid + 1;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return ans;
    }
}