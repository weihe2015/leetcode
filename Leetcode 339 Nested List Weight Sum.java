/*
Given a nested list of integers, return the sum of all integers in the list weighted by their depth.

Each element is either an integer, or a list -- whose elements may also be integers or other lists.

Example 1:
Given the list [[1,1],2,[1,1]], return 10. (four 1s at depth 2, one 2 at depth 1)

Example 2:
Given the list [1,[4,[6]]], return 27. (one 1 at depth 1, one 4 at depth 2, and one 6 at depth 3; 1 + 4*2 + 6*3 = 27)
*/
public class Solution {
    // recursive: DFS
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int depthSum(List<NestedInteger> nestedList) {
        // 0: sum, 1: level:
        int[] res = {0,1};
        depthSum(nestedList, res);
        return res[0];
    }

    private void depthSum(List<NestedInteger> nestedList, int[] res) {
        for (NestedInteger ni : nestedList) {
            if (ni.isInteger()) {
                res[0] += ni.getInteger() * res[1];
            }
            else {
                res[1]++;
                depthSum(ni.getList(), res);
                res[1]--;
            }
        }
    }

    // Solution 2:
    public int depthSum(List<NestedInteger> nestedList) {
        int sum = 0;
        sum = depthSum(nestedList, 1);
        return sum;
    }

    private int depthSum(List<NestedInteger> nestedList, int depth) {
        if (nestedList == null || nestedList.isEmpty()) {
            return 0;
        }
        int sum = 0;
        for (NestedInteger ni : nestedList) {
            if (ni.isInteger()) {
                sum += ni.getInteger() * depth;
            }
            else {
                sum += depthSum(ni.getList(), depth+1);
            }
        }
        return sum;
    }

    // Iterative: level order traversal O(n), each node visit once
    public int depthSum(List<NestedInteger> nestedList) {
        int sum = 0;
        int level = 1;
        Queue<NestedInteger> queue = new LinkedList<>(nestedList);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (ni.isInteger()) {
                    sum += ni.getInteger() * level;
                }
                else {
                    queue.addAll(ni.getList());
                }
            }
            level++;
        }
        return sum;
    }
}