/*
Given a list of airline tickets represented by pairs of departure and arrival airports [from, to], 
reconstruct the itinerary in order. All of the tickets belong to a man who departs from JFK. 
Thus, the itinerary must begin with JFK.

Note:

If there are multiple valid itineraries, you should return the itinerary that has the smallest lexical order when read as a single string. For example, the itinerary ["JFK", "LGA"] has a smaller lexical order than ["JFK", "LGB"].
All airports are represented by three capital letters (IATA code).
You may assume all tickets form at least one valid itinerary.
One must use all the tickets once and only once.
Example 1:

Input: [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
Output: ["JFK", "MUC", "LHR", "SFO", "SJC"]
Example 2:

Input: [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
Output: ["JFK","ATL","JFK","SFO","ATL","SFO"]
Explanation: Another possible reconstruction is ["JFK","SFO","ATL","JFK","ATL","SFO"].
             But it is larger in lexical order.
*/

public class Solution {
    // https://www.youtube.com/watch?v=4udFSOWQpdg
    /**
    Treat the graph as Tree, sort the children by the laxigraphical order,
    Perform a DFS and record the children in stack.
    or looks like post order traversal

 NRT <-> JFK -> KUL

    ==>   JFK
       KUL    NRT
            JFK 

    */
    // Solution 1: DFS:
    public List<String> findItinerary(List<List<String>> tickets) {
        List<String> res = new ArrayList<>();
        if (tickets == null || tickets.isEmpty()) {
            return res;
        }

        Map<String, Queue<String>> map = new HashMap<>();
        for (List<String> list : tickets) {
            String from = list.get(0);
            String to = list.get(1);
            Queue<String> pq = map.getOrDefault(from, new PriorityQueue<String>());
            pq.offer(to);
            map.put(from, pq);
        }

        String start = "JFK";
        Stack<String> stack = new Stack<>();
        dfs(map, start, stack);

        while (!stack.isEmpty()) {
            res.add(stack.pop());
        }
        return res;
    }

    private void dfs(Map<String, Queue<String>> map, String from, Stack<String> stack) {
        Queue<String> queue = map.get(from);
        while (!queue.isEmpty()) {
            String neighbor = queue.poll();
            if (!map.containsKey(neighbor)) {
                stack.push(neighbor);
            }
            else {
                dfs(map, neighbor, stack);
            }
        }
        stack.push(from);
    }

    // Solution 2: BFS:
    public List<String> findItinerary(List<List<String>> tickets) {
        List<String> res = new LinkedList<>();
        if (tickets == null || tickets.isEmpty()) {
            return res;
        }

        Map<String, Queue<String>> map = new HashMap<>();
        for (List<String> list : tickets) {
            String from = list.get(0);
            String to = list.get(1);
            Queue<String> pq = map.getOrDefault(from, new PriorityQueue<>());
            pq.offer(to);
            map.put(from, pq);
        }

        String start = "JFK";
        Stack<String> stack = new Stack<>();
        stack.push(start);
        while (!stack.isEmpty()) {
            String from = stack.peek();
            if (!map.containsKey(from)) {
                stack.pop();
                res.add(0, from);
            }
            else {
                Queue<String> pq = map.get(from);
                String to = pq.poll();
                stack.push(to);
                if (pq.isEmpty()) {
                    map.remove(from);
                }
            }
        }
        return res;
    }

    // Solution 3:
    public List<String> findItinerary(String[][] tickets) {
        List<String> res = new ArrayList<>();
        if (tickets == null || tickets.length == 0) {
            return res;
        }

        Map<String, Queue<String>> ticketsMap = new HashMap<>();
        for (List<String> list : tickets) {
            String from = list.get(0);
            String to = list.get(1);
            Queue<String> pq = ticketsMap.getOrDefault(from, new PriorityQueue<>());
            pq.offer(to);
            ticketsMap.put(from, pq);
        }

        String curr = "JFK";
        Stack<String> drawBack = new Stack<>();
        for (int i = 0; i < tickets.length; i++){
            while (!ticketsMap.containsKey(curr) || ticketsMap.get(curr).isEmpty()){
                drawBack.push(curr);
                curr = result.remove(result.size()-1);
            }
            result.add(curr);
            curr = ticketsMap.get(curr).poll();
        }
        result.add(curr);
        while (!drawBack.isEmpty()) {
            result.add(drawBack.pop());
        }
        return res;
    }
}