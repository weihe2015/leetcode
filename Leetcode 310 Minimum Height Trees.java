/**
For a undirected graph with tree characteristics, we can choose any node as the root. The result graph is then a rooted tree. 
Among all possible rooted trees, those with minimum height are called minimum height trees (MHTs). 
Given such a graph, write a function to find all the MHTs and return a list of their root labels.

Format
The graph contains n nodes which are labeled from 0 to n - 1. 
You will be given the number n and a list of undirected edges (each edge is a pair of labels).

You can assume that no duplicate edges will appear in edges. 
Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.

Example 1:

Given n = 4, edges = [[1, 0], [1, 2], [1, 3]]

        0
        |
        1
       / \
      2   3
return [1]

Example 2:

Given n = 6, edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]]

     0  1  2
      \ | /
        3
        |
        4
        |
        5
return [3, 4]
*/
public class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> result = new ArrayList<Integer>();
        if (n <= 0) {
            return result;
        }
        if (n == 1) {
            result.add(0);
            return result;
        }
        // Build Graph:
        List<Set<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new HashSet<Integer>());
        }
        for (int[] edge : edges) {
            int from = edge[0];
            int to = edge[1];
            graph.get(from).add(to);
            graph.get(to).add(from);
        }
        Queue<Integer> q = new LinkedList<Integer>();
        // Add all leaves into a queue. A leaf meaning only has one vertex neighbor:
        for (int i = 0; i < n; i++) {
            if (graph.get(i).size() == 1) {
                q.offer(i);
            }
        }
        while (!q.isEmpty()) {
            int size = q.size();
            result.clear();
            for (int i = 1; i <= size; i++) {
                int u = q.poll();
                result.add(u);
                for (int v : graph.get(u)) {
                    // Remove obj u from list v
                    graph.get(v).remove(u);
                    if (graph.get(v).size() == 1) {
                        q.offer(v);
                    }
                }
            }
        }
        return result;
    }
    /**
    Tree in graph theory:
    1. A tree is an undirected graph where any two vertices are connected by exactly one path
    2. Any connected graph who has n nodes with n-1 edges is a tree
    3. The degree of a vertex of a graph is the number of edges incident to the vertex
    4. A leaf is a vertex of degree 1. An internal vertex is a vertex of degree at least 2
    5. A path graph is a tree with two or more vertices that is not branched at all.
    6. A tree is called a rooted tree if noe vertex has been designated the root
    7. The height of a rooted tree is the number of edges on the longest downward path between root and a leaf

    */
    /**
    基本思路是“逐层删去叶子节点，直到剩下根节点为止”
    有点类似于拓扑排序
    最终剩下的节点个数可能为1或者2
    */
    // BFS:
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> result = new ArrayList<Integer>();
        if (n == 0) {
            return result;
        }
        if (n == 1) {
            result.add(0);
            return result;
        }
        // Build Graph with Adjacent List:
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<Integer>());
        }
        int[] indegree = new int[n];
        for (int[] edge : edges) {
            int from = edge[0];
            int to = edge[1];
            graph.get(from).add(to);
            graph.get(to).add(from);
            indegree[from]++;
            indegree[to]++;
        }
        // Add all source vertex into Queue
        Queue<Integer> q = new LinkedList<Integer>();
        for (int i = 0; i < n; i++) {
            if (indegree[i] == 1) {
                q.offer(i);
            }
        }
        // BFS
        /**
        所有indegree = 1的vertex都是叶子，从叶子开始做BFS，历遍叶子的所有
        邻居，到最后的时候就是Minimum Height Trees的root，结果有一个或者两个vertex。
        */ 
        boolean[] visited = new boolean[n];
        while (!q.isEmpty()) {
            int size = q.size();
            // 舍去之前的结果，因为
            result.clear();
            for (int i = 1; i <= size; i++) {
                int u = q.poll();
                visited[u] = true;
                result.add(u);
                for (int v : graph.get(u)) {
                    indegree[v]--;
                    if (indegree[v] == 1 && !visited[v]) {
                        q.offer(v);
                    }
                }
            }
        }
        return result;
    }
}