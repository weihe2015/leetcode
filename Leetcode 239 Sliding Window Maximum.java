/*
Given an array nums, there is a sliding window of size k which is moving from the very left of the array to the very right.
You can only see the k numbers in the window.
Each time the sliding window moves right by one position. Return the max sliding window.

Example:

Input: nums = [1,3,-1,-3,5,3,6,7], and k = 3
Output: [3,3,5,5,6,7]
Explanation:

Window position                Max
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7
Note:
You may assume k is always valid, 1 ≤ k ≤ input array's size for non-empty array.

Follow up:
Could you solve it in linear time?
*/
public class Solution {
    // Brute Force Solution:
    // Running Time Complexity: O(N * K)
    public int[] maxSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        int[] res = new int[n-k+1];
        for (int i = 0, max = n-k; i <= max; i++) {
            int maxVal = Integer.MIN_VALUE;
            for (int j = i; j < i+k; j++) {
                maxVal = Math.max(maxVal, nums[j]);
            }
            res[i] = maxVal;
        }
        return res;
    }

    // Running Time Complexity: O(n * k), Space Complexity: O(1)
    public int[] maxSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        if (n == 0) {
            return new int[0];
        }
        int[] res = new int[n-k+1];
        // idx in res array:
        int idx = 0;
        int r = k-1;
        int maxVal = Integer.MIN_VALUE;
        
        while (r < n) {
            // initial window: get the maximum from this window.
            if (idx == 0) {
                maxVal = findMaxInWindow(nums, r, k);
            }
            else {
                // leftItem is the previous left item being excluded
                int leftItem = nums[r-k];
                // rightItem is the new right item being added into the window
                int rightItem = nums[r];
                // If the new item larger than previous maxVal, it is the new maxVal
                if (rightItem > maxVal) {
                    maxVal = rightItem;
                }
                // if the previous left Item being excluded is the maxVal, we need to find the new maxVal within this window.
                else if (leftItem == maxVal) {
                    maxVal = findMaxInWindow(nums, r, k);
                }
                else {
                    // maxVal is still in the window, nothing is needed to do.
                }
            }
            res[idx] = maxVal;
            idx++;
            r++;
        }
        return res;
    }

    private int findMaxInWindow(int[] nums, int r, int k) {
        int maxVal = Integer.MIN_VALUE;
        int idx = r-k+1;
        while (idx <= r) {
            maxVal = Math.max(maxVal, nums[idx]);
            idx++;
        }
        return maxVal;
    }

    // Solution 3:
    class MaxQueue {
        Deque<Integer> queue = new ArrayDeque<>();
        // Decreasing Monotonic queue, the max item always on the leftmost
        // push only takes O(K), where k is the size of the sliding window

        public void push(int num) {
            while (!queue.isEmpty() && queue.peekLast() < num) {
                queue.pollLast();
            }
            queue.offerLast(num);
        }

        public int getFront() {
            return queue.peekFirst();
        }

        public void popMaxIfExists(int num) {
            if (num != queue.peekFirst()) {
                return;
            }
            queue.pollFirst();
        }
    }
    // Use Monotonic Queue: Running Time Complexity: O(n)
    public int[] maxSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        int[] res = new int[n-k+1];
        int idx = 0;
        MaxQueue mq = new MaxQueue();
        for (int i = 0; i < n; i++) {
            // 1 to k-2 items
            int num = nums[i];
            mq.push(num);
            if (i >= k-1) {
                res[idx] = mq.getFront();
                int leftItem = nums[i-k+1];
                // In next iteration, the leftItem will be exclude.
                // If it is the maximum, we need to pop it from the deque.
                mq.popMaxIfExists(leftItem);
                idx++;
            }
        }
        return res;
    }

    // Solution 4:
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return new int[]{};
        }
        int l = 0, r = k-1, n = nums.length, maxVal = Integer.MIN_VALUE, maxIdx = -1;
        int[] res = new int[n-k+1];
        while (r < n) {
            // If maxValue is outside of window[l...r]:
            // Find the maxValue in window[l..r]
            if (maxIdx < l) {
                maxVal = Integer.MIN_VALUE;
                for (int i = l; i <= r; i++) {
                    if (maxVal < nums[i]) {
                        maxIdx = i;
                        maxVal = nums[i];
                    }
                }
            }
            else {
                // If the right most item is the largest:
                if (nums[r] > maxVal) {
                    maxVal = nums[r];
                    maxIdx = r;
                }
            }
            res[l] = maxVal;
            l++;
            r++;
        }
        return res;
    }
}