/**

In a 2D grid from (0, 0) to (N-1, N-1), every cell contains a 1, except those cells in the given list mines which are 0. What is the largest axis-aligned plus sign of 1s contained in the grid? Return the order of the plus sign. If there is none, return 0.

An "axis-aligned plus sign of 1s of order k" has some center grid[x][y] = 1 along with 4 arms of length k-1 going up, down, left, and right, and made of 1s. This is demonstrated in the diagrams below. Note that there could be 0s or 1s beyond the arms of the plus sign, only the relevant area of the plus sign is checked for 1s.

Examples of Axis-Aligned Plus Signs of Order k:

Order 1:
000
010
000

Order 2:
00000
00100
01110
00100
00000

Order 3:
0000000
0001000
0001000
0111110
0001000
0001000
0000000
Example 1:

Input: N = 5, mines = [[4, 2]]
Output: 2
Explanation:
11111
11111
11111
11111
11011
In the above grid, the largest plus sign can only be order 2.  One of them is marked in bold.
Example 2:

Input: N = 2, mines = []
Output: 1
Explanation:
There is no plus sign of order 2, but there is of order 1.
Example 3:

Input: N = 1, mines = [[0, 0]]
Output: 0
Explanation:
There is no plus sign, so return 0.
*/
public class Solution {
    // Brute Force Solution: 
    // Running Time Complexity: O(N^3)
    public int orderOfLargestPlusSign(int N, int[][] mines) {
        int[][] grid = new int[N][N];
        // Initialize the Grid:
        for (int i = 0; i < N; i++) {
            Arrays.fill(grid[i], 1);
        }
        // Fill in the 0 by given mines:
        for (int[] mine: mines) {
            grid[mine[0]][mine[1]] = 0;
        }
        // Iterate the grid:
        int maxResult = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                // If grid[i][j] == 1, set it as center point and expand 
                if (grid[i][j] == 1) {
                    int count = 1;
                    int dir = 1;
                    while (j - dir >= 0 && j + dir < N && i - dir >= 0 && i + dir < N) {
                        if (grid[i][j-dir] == 1 && grid[i][j+dir] == 1 && 
                                grid[i-dir][j] == 1 && grid[i+dir][j] == 1){
                            count++;
                            dir++;
                        }
                        else {
                            break;
                        }
                    }
                    maxResult = Math.max(maxResult, count);
                }
            }
        }
        return maxResult;
    }

    public int orderOfLargestPlusSign(int N, int[][] mines) {
        int[][] grid = new int[N][N];
        // Initialize the Grid with num N:
        for (int i = 0; i < N; i++) {
            Arrays.fill(grid[i], N);
        }
        // Fill out 0's from mines:
        for (int[] mine : mines) {
            grid[mine[0]][mine[1]] = 0;
        }
        for (int i = 0; i < N; i++) {
            int left = 0;
            for (int j = 0; j < N; j++) {
                // j is a column index, iterate from left to right
                // every time check how far left it can reach.
                // if grid[i][j] is 0, l needs to start over from 0 again, otherwise increment
                if (grid[i][j] == 0) {
                    left = 0;
                }
                else {
                    left++;
                }
                grid[i][j] = Math.min(grid[i][j], left);
            }
            int right = 0;
            for (int j = N-1; j >= 0; j--) {
                // j is a column index, iterate from right to left
                // every time check how far right it can reach.
                // if grid[i][j] is 0, r needs to start over from 0 again, otherwise increment
                if (grid[i][j] == 0) {
                    right = 0;
                }
                else {
                    right++;
                }
                grid[i][j] = Math.min(grid[i][j], right);
            }
            int up = 0;
            for (int j = 0; j < N; j++) {
                // j is a row index, iterate from top to bottom
                // every time check how far up it can reach.
                // if grid[j][i] is 0, u needs to start over from 0 again, otherwise increment
                if (grid[j][i] == 0) {
                    up = 0;
                }
                else {
                    up++;
                }
                grid[j][i] = Math.min(grid[j][i], up);
            }
            int down = 0;
            for (int j = N-1; j >= 0; j--) {
                // j is a row index, iterate from bottom to top
                // every time check how far down it can reach.
                // if grid[j][i] is 0, d needs to start over from 0 again, otherwise increment
                if (grid[j][i] == 0) {
                    down = 0;
                }
                else {
                    down++;
                }
                grid[j][i] = Math.min(grid[j][i], down);
            }
                
            // after four loops each time taking Math.min over the grid value itself
            // all grid values will eventually take the min of the 4 direcitons.
        }
        // Find max in the grid:
        int maxNum = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                maxNum = Math.max(maxNum, grid[i][j]);
            }
        }
        return maxNum;
    }
}