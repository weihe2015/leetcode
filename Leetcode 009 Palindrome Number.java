/**
Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

Example 1:

Input: 121
Output: true
Example 2:

Input: -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
Example 3:

Input: 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

Follow up:
Coud you solve it without converting the integer to a string?
*/
public class Solution {
    // solution 1
    // Running Time Complexity: O(n)
    public boolean isPalindrome(int x) {
        if (x < 0){
            return false;
        }
        return isPalindrome(String.valueOf(x));
    }

    private boolean isPalindrome(String s) {
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++; 
            j--;
        }
        return true;
    }

    // solution 2
    // Running Time Complexity: O(lenOf(x))
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        int originNum = x;
        int revertNum = 0;
        while (x > 0) {
            int digit = x % 10;
            revertNum = revertNum * 10 + digit;
            x /= 10;
        }
        return revertNum == originNum;
    }
}