/**
Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:

Integers in each row are sorted from left to right.
The first integer of each row is greater than the last integer of the previous row.
For example,

Consider the following matrix:

[
  [1,   3,  5,  7],
  [10, 11, 16, 20],
  [23, 30, 34, 50]
]
Given target = 3, return true.

Example 2:

Input:
matrix = [
  [1,   3,  5,  7],
  [10, 11, 16, 20],
  [23, 30, 34, 50]
]
target = 13
Output: false
**/
public class Solution {
    // Binary Search without considering boundary:
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        int m = matrix.length, n = matrix[0].length;
        int res = -1;
        int low = 0, high = m*n-1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            // 我们用 matrix的宽度作为分母去找mid val
            int val = matrix[mid/n][mid%n];
            if (val == target) {
                res = val;
                break;
            }
            else if (val < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res != -1;
    }

    // binary search, solution 1
    public boolean searchMatrix(int[][] matrix, int target) {
        // handle case that [[]] or [] or null
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }

        int low = 0, m = matrix.length, n = matrix[0].length;
        int high = n * m - 1;
        // while condition cannot be low == right
        // If we put low <= right as while condition, we need to check if low/n is out of bound or not.
        while (low < high) {
            int mid = low + (high - low)/2;
            int midVal = matrix[mid/n][mid%n];
            if (midVal == target) {
                return true;
            }
            else if (midVal < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return matrix[low/n][low%n] == target;
    }

    // solution 2, snake search
    public boolean searchMatrix2(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        int m = matrix.length;
        int n = matrix[0].length;
        int x = 0;
        int y = n - 1;
        while (x < m && y >= 0) {
            if (target == matrix[x][y]) {
                return true;
            }
            else if (target < matrix[x][y]) {
                y--;
            }
            else {
                x++;
            }
        }
        return false;
    }

    public boolean searchMatrix3(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        // find the row number using binary search:
        int low = 0, high = matrix.length - 1;
        while (low < high) {
            int mid = low + (high - low)/2;
            int midVal = matrix[mid][0];
            if (midVal == target) {
                return true;
            }
            else if (midVal < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        int rowNumber = -1;
        if (matrix[low][0] == target) {
            return true;
        }
        if (low > 0 && matrix[low][0] > target) {
            rowNumber = low - 1;
        }
        else {
            rowNumber = low;
        }
        // perform binary search on selected row
        low = 0; high = matrix[0].length - 1;
        while (low < high) {
            int mid = low + (high - low) / 2;
            int midVal = matrix[rowNumber][mid];
            if (midVal == target) {
                return true;
            }
            else if (midVal < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return matrix[rowNumber][low] == target;
    }
}