/**
Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

Example:

Input: head = 1->4->3->2->5->2, x = 3
Output: 1->2->2->4->3->5
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode partition(ListNode head, int x) {
        ListNode sHead = new ListNode(0), bHead = new ListNode(0);
        ListNode sCurr = sHead, bCurr = bHead, curr = head;
        while (curr != null) {
            if (curr.val < x){
                sCurr.next = curr;
                sCurr = sCurr.next;
            }
            else{
                bCurr.next = curr;
                bCurr = bCurr.next;
            }
            curr = curr.next;
        }
        sCurr.next = bHead.next;
        bCurr.next = null;
        return sHead.next;
    }
}