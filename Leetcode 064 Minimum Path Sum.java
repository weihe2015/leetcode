/*
Given a m x n grid filled with non-negative numbers,
find a path from top left to bottom right which minimizes the sum of all numbers along its path.

Note: You can only move either down or right at any point in time.

Example:

Input:
[
  [1,3,1],
  [1,5,1],
  [4,2,1]
]
Output: 7
Explanation: Because the path 1→3→1→1→1 minimizes the sum.
*/
public class Solution {
    // O(m*n) time, O(m*n) space
    public int minPathSum(int[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int[][] dp = new int[m][n];
        dp[0][0] = grid[0][0];
        for (int i = 1; i < m; i++) {
            dp[i][0] = grid[i][0] + dp[i-1][0];
        }

        for (int j = 1; j < n; j++) {
            dp[0][j] = grid[0][j] + dp[0][j-1];
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = grid[i][j] + Math.min(dp[i-1][j], dp[i][j-1]);
            }
        }
        return dp[m-1][n-1];
    }

    // solution 2
    /**
    dp[i] = min(dp[i],dp[i-1]) + grid[i][j]
    1. array dp stores the first row of min path sum
    2. for each i..m, dp[0] = grid[i][0]
    3. increase i, dp[j] = min(dp[j],dp[j-1]) + grid[i][j]
    */
    // Running Time Complexity: O(m * n), Space Complexity: O(m)
    public int minPathSum(int[][] grid) {
        // sub problem: dp[i] = grid[i][j] + Math.min(dp[i],dp[i-1]);
        // First, set first column of min path sum
        // Second, for each row, add grid[0][j] to dp[0];

        // check input:
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;

        int[] dp = new int[n];
        dp[0] = grid[0][0];
        for (int j = 1; j < n; j++) {
            dp[j] = dp[j-1] + grid[0][j];
        }

        for (int i = 1; i < m; i++) {
            dp[0] += grid[i][0];
            for (int j = 1; j < n; j++) {
                dp[j] = grid[i][j] + Math.min(dp[j-1], dp[j]);
            }
        }
        return dp[n-1];
    }
}