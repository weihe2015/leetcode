/**
Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

Note:

The same word in the dictionary may be reused multiple times in the segmentation.
You may assume the dictionary does not contain duplicate words.
Example 1:

Input: s = "leetcode", wordDict = ["leet", "code"]
Output: true
Explanation: Return true because "leetcode" can be segmented as "leet code".
Example 2:

Input: s = "applepenapple", wordDict = ["apple", "pen"]
Output: true
Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
             Note that you are allowed to reuse a dictionary word.
Example 3:

Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
Output: false


01234567
leetcode
j..i
dp[i] -> break here
it is likely that the match word will be found at the end of the finished part of the string,
but not a really long word which begins from the beginning of the string
*/

public class Solution {
    // dfs:
    // Running Time Complexity: O(N^2), Space Complexity: O(M)
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> wordSet = new HashSet<String>(wordDict);
        int n = s.length();
        // dp[i]: substring(0, i) can be segmented with words in wordDict:
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for (int i = 1; i <= n; i++) {
            for (int j = i-1; j >= 0; j--) {
                // The same as for (int j = 0; j < i; j++), but it is slower
                String substr = s.substring(j, i);
                // dp[j] means that substring(0, j) can be segmented with words in wordDict,
                // if substring(j, i) is in wordDict, it means all substring(0, i) can be segmented with words in wordDict
                if (dp[j] && wordSet.contains(substr)){
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }

    // Second solution:
    // Running Time Complexity: O(n * w), Space Complexity: O(n)
    public boolean wordBreak(String s, List<String> wordDict) {
        int n = s.length();
        // dp[i]: substring(0, i) can be segmented with words in wordDict:
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for (int i = 0; i < n; i++) {
            // If substring(0...i) cannot be segmented with words in wordDict,
            // there is no need to see if appended word in wordDict can be segmented.
            if (!dp[i]) {
                continue;
            }
            // Try each word and find substring(i, i+word.length()):
            // "applepenapple": word = pen, i = 5, dp[i] = true, substring(i, i+word.length()) = substring(5, 8) = pen:
            // so dp[8] = true;
            for (String word : wordDict) {
                int end = i + word.length();
                // If either appended word to substring(0,i) is longer than s, or we already found dp[end] == true
                if (end > n || dp[end]) {
                    continue;
                }
                // substr like "pen", s="applepenapple", ["apple","pen"]
                String substr = s.substring(i, end);
                if (substr.equals(word)) {
                    dp[end] = true;
                }
            }
        }
        return dp[n];
    }

    // BFS
    public boolean wordBreak(String s, List<String> wordDict) {
        int n = s.length();
        Set<String> wordSet = new HashSet<>(wordDict);
        
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(0);

        boolean[] visited = new boolean[n+1];
        visited[0] = true;
        
        while (!queue.isEmpty()) {
            int idx = queue.poll();
            for (int i = idx+1; i <= n; i++) {
                if (visited[i]) {
                    continue;
                }
                String substr = s.substring(idx, i);
                if (wordSet.contains(substr) && i == n) {
                    return true;
                }
                else if (wordSet.contains(substr)) {
                    queue.offer(i);
                    visited[i] = true;
                }
            }
        }
        return false;
    }
}