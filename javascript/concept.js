// javascript type detemination
// Types: string, number/BigInt, object, function, undefined, boolean
// ES 5: Symbol: Primitive data type, return value of type symbol, static properties that expose serveral members of built-in objects.

// Symbol:
let sym1 = Symbol();
// It creates a new symbol each time
Symbol('foo') === Symbol('foo')  // false

// returns an array of symbols and find symbol properties
Object.getOwnPropertySymbols()

// typeof vs instanceof:
// Use instanceof for custom types.
// Use typeof for simple built-in types
// Use instanceof for complex built in types

typeof null; // object

// Curry and Funciton Composition:
// Curry function: takes multiple arguments one at a time.
const add = a => b => a + b;

var add = function (a) {
    return function (b) {
        return a + b
    };
};

const result = add(2)(3)

// Function apply: arguments is in array:
func.apply(thisArg, [argsArray])

// find out Math.min/max:
let max = Math.max.apply(null, [1, 2, 3])

// risk: exceeding javascript engine's argument length limit: 65536, 2^16

// Function call:, argument splits by comma
func.call(thisArg, arg1, arg2,);

const animals = [
    { species: 'Lion', name: 'King' },
    { species: 'Whale', name: 'Fail' }
];

for (let i = 0; i < animals.length; i++) {
    (function (i) {
        this.print = function () {
            console.log('#' + i + ' ' + this.species
                + ': ' + this.name);
        }
        this.print();
    }).call(animals[i], i);
}
// #0 Lion: King
// #1 Whale: Fail

function greet() {
    const reply = [this.animal, 'typically sleep between', this.sleepDuration].join(' ');
    console.log(reply);
}

const obj = {
    animal: 'cats', sleepDuration: '12 and 16 hours'
};

greet.call(obj);
// cats typically sleep between 12 and 16 hours

// ES 6 features:
// 1. Default Parameters:
var link = function (height = 50, color = 'red', url = 'http://azat.co') {

}
// 2. Template Literals in ES 6:
var name = `Your name is ${first} ${last}.`
var url = `http://localhost:3000/api/messages/${id}`

// 3. multi-line strings in ES 6
var roadPoem = `Then took the other, as just as fair,
    And having perhaps the better claim
    Because it was grassy and wanted wear,
    Though as for that the passing there
    Had worn them really about the same,`;

// 4. Destructing argument in ES 6
const body = {
    username: '',
    password: ''
}
const { username, password } = body;
// Also works with array:
const [a, b, ...rest] = [10, 20, 30, 40, 50];
console.log(a); // 10
console.log(b); // 20
console.log(rest); // [30, 40, 50]

// 5 Arrow function:
add = (a, b) => {
    return a + b
}

// implicit return
// ES 5:
var ids = ['5632953c4e345e145fdf2df8', '563295464e345e145fdf2df9'];
var messages = ids.map(function (value, index, list) {
    return 'ID of ' + index + ' element is ' + value + ' ' // explicit return
})
// ES 6:
var ids = ['5632953c4e345e145fdf2df8', '563295464e345e145fdf2df9']
var messages = ids.map((value, index, list) => `ID of ${index} element is ${value} `) // implicit return

// 6: Promise:
var wait1000 = new Promise((resolve, reject) => {
    setTimeout(resolve, 1000)
}).then(() => {
    console.log('Yay!')
})

// Spread arguments:
// allows


// Class, Module in ES 6
class baseModel {
    constructor(options = {}, data = []) { // class constructor
        this.name = 'Base'
        this.url = 'http://azat.co/api'
        this.data = data
        this.options = options
    }

    getName() { // class method
        console.log(`Class name: ${this.name}`)
    }
}

import { port, getAccounts } from 'module'

// Javascript is not class based langauge, it is prototype base language
// When you create object, it gets a prototype. If method not in object, it will go to prototype to check

// Generator:
function* test() {
    yield "1";
    yield "2";
}

test().next()

// Prototype:
class PersonC {
    constructor(name, id) {
        this.name = name;
        this.id = id;
    }

    getDetail() {
        return `${this.name} and ${this.id}`;
    }
}

let PersonP = function (name, id) {
    this.name = name;
    this.id = id;
}
PersonP.prototype.getDetail = function () {
    return `${this.name} and ${this.id}`;
}

let EmployeeP = function (name, id, salary) {
    PersonP.call(this, name, id);
    this.salary = salary;
}
// extends, links the prototype of extend class to itself:
Object.setPrototypeOf(EmployeeP, PersonP.prototype);

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain
function a() {
    this.b = 3
}
a.prototype.b = 7;
var t = new a();
var b = 2;
a();
console.log(t.b);
console.log(b);
// 3
// 2
// {b : 3} -> {b : 7} -> Object.prototype -> null

// publish / subscribe pattern:
class Emitter {
    constructor() {
        this._eventPool = {};
    }

    on = (event, fn) => {
        if (this._eventPool[event]) {
            this._eventPool[event].push(fn);
        }
        else {
            this._eventPool[event] = [fn];
        }
    }

    off = (event) => {
        if (!this._eventPool[event]) {
            return;
        }
        delete this._eventPool[event];
    }

    offOne = (event, fn) => {
        if (!this._eventPool[event]) {
            return;
        }
        this._eventPool[event] = this._eventPool[event].filter(cb => cb !== fn);
    }

    emit = (event, data) => {
        if (!this._eventPool[event]) {
            return;
        }
        this._eventPool[event].forEach(callBack => callBack(data));
    }

    once = (event, fn) => {
        this.on(event, data => {
            fn(data);
            this.off(event);
        });
    }
}
// Observer Pattern:
function Subject() {
    this.observers = [];
}

Subject.prototype = {
    subscribe: function (fn) {
        this.observers.push(fn);
    },
    unsubscribe: function (fnToRemove) {
        this.observers = this.observers.filter(fn => fn != fnToRemove);
    },
    fire: function (o, thisObj) {
        const scope = thisObj || window;
        this.observers.forEach(fn => fn.call(scope, o));
    }
}
var messageHandler = function (item) {
    console.log("fired item " + item);
}
var subject = new Subject();
subject.subscribe(messageHandler);
subject.fire("event #1");
subject.unsubscribe(messageHandler);
subject.fire("event #2");
subject.subscribe(messageHandler);
subject.fire("event #3");

// Redux: application state management
// maintains the state of an entire application in a single immutable state tree (object),
// UI triggers -> Actions -> send to Reducer -> updates stores -> contains state -> define UI.

// Deep copy v.s Shallow copy:
// Shallow copy: A shallow copy successfully copies primitive types like numbers and strings, but any object reference will not be recursively copied, but instead the new, copied object will reference the same object.
// Deep copy: external objects are copied as well
// Solution 1:
const clone = require('lodash.clone')
const clonedeep = require('lodash.clonedeep')

const externalObject = {
    color: 'red'
};

const cloneObj = clone(externalObject);

// shallow copy
const copied = Object.assign({}, externalObject)
const copied = { ...original }

const obj = { name: { first: 'Jean-Luc', last: 'Picard' } };
const copy = JSON.parse(JSON.stringify(obj));

// deep copy:
function isPrimitive(val) {
    return (typeof val === 'string' || typeof val === 'number' || typeof val === 'symbol' || typeof val === 'boolean')
}

function isObject(val) {
    return Object.prototype.toString.call(val) === "[object Object]"
}

function deepCopy(val) {
    let memo = {};

    function clone(val) {
        let res = null;
        if (isPrimitive(val)) {
            return val;
        }
        else if (Array.isArray(val)) {
            res = [...val];
        }
        else if (isObject(val)) {
            res = { ...val };
        }
        // 检测object的属性值是不是有引用数据类型。如果是，则递归copy
        Reflect.ownKeys(res).forEach(key => {
            if (typeof res[key] === 'object' && res[key] !== null) {
                // 用memo来记录被copy reference, 来解决循环引用的问题
                if (memo[res[key]]) {
                    res[key] = memo[res[key]]
                }
                else {
                    memo[res[key]] = res[key];
                    res[key] = clone(res[key])
                }
            }

        })
        return res;
    }
    return clone(val)
};

var obj = {};
var b = { obj };
obj.b = b;
var copy = deepCopy(obj);
console.log(copy)

// concurrency in Javascript:
// Javascript does not support multi-threading, because the JavaScript interpreter in the browser is a single thread (AFAIK). Even Google Chrome will not let a single web page’s JavaScript run concurrently because this would cause massive concurrency issues in existing web pages.

// Javascript event loop:
// Frame: arguments and local variables, push to call stack
// WebAPI: DOM, ajax, setTimeout
// callbackqueue: onClick, onLoad, onDone
// https://www.youtube.com/watch?v=8aGhZQkoFbQ
// webAPI push event to task queue. once it finished, wait until call stack is clear, push it to call stack

// MIME type: application/octet-stream: binary data
// multipart/form-data, POST method in HTML form
// text/plain
// text/css
// text/html
// text/javascript

// React:
// life cycle:
// Mounting:
// 1. constructor()
// 2. static getDeriviedStateFromProps()
// getDerivedStateFromProps is invoked right before calling the render method, both on the initial mount and on subsequent updates. It should return an object to update the state, or null to update nothing.
// 3. render()
// 4. componentDidMount()

// Updating:
// 1. static getDerivedStateFromProps()
// 2. shouldComponentUpdate()
// 3. render()
// 4. getSnapshotBeforeUpdate()
// 5. componentDidUpdate()

// Unmounting:
// 1. componentWillUnmount()

// React Hooks: allow you to use React Features without writing a class
// avoid confusion with 'this' keyword
// reuse stateful logic
// cannot use hooks inside of a class component
// The effect Hooks lets you perform side effects in functional components
// close replacement for componentDidMount, componentDidUpdate, componentWillUnmount
// useEffect runs after each render, execute the function that is passed as argument to useEffect

// localStorage, sessionStorage and cookie
// key value store
// localStorage:
//    storage with no expiration date
//    storage limit about 5MB
//    plaintext, limited to string data, can only be used on client side
localStorage.setItem('key', 'value');
localStorage.removeItem('key');
localStorage.clear();

// Session Storage:
// data is stored until the browser is closed.
// open multiple tabs/window with same URL creates sessionStorage for each tab/window
// localstorage share across tab

// Cookies: primarily for server-side reading
// size must less than 4KB
// stored data has to be sent back to the server with subsequent XHR requests. Its expiration varies

// React hooks
// useState - state
// useEffect - side effects
// useContext - context API
// useReducer: a hook that is used for state management
const reducer = (accumulator, currVal) => accumulator + currVal
Array.reduce(reducer, initialValue)
useReducer(reducer, initialState)

singleValue = reducer(accumulator, itemValue)
newState = reducer(currState, action)

// returns a pair of values [newState, dispatch]

// HTTPS: TLS/SSL encrypt HTTP requests:
// public key encryption, two keys, public key and private key.
// public key shared with client devices via server's SSL certificate
// when client opens a connection with server, two device will use public & private key to generate session keys, to further communicate

// HTTP method: https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
// GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH
// GET: retrieve data
// HEAD: response identical to GET request, but no response body
// POST: submit entity to server
// PUT: update content
// DELETE: delete content
// CONNECT: establish a tunnel to the server identified by the target resource
// OPTIONS: describe the communication options for the target resource
// TRACE: performs message look-back test along the path to the target resource
// PATCH: apply partial modification to a resource

// HTTP 1.1 vs HTTP 2
// 1. Request multiplexing (多重) send mutiple requests for data in parallel over a single TCP connection, download web files asynchronously from one server
// 2. Binary protocol, different from text commands in HTTP 1.1 to complete request response: lighter network footprint, eliminating security concerns, low overhead in parsing data
// 3. Header compression: compress large number of redundant header frames
// 4. Server push: allow server to send additional cacheable info to client. if the client requests for the resource X and it is understood that the resource Y is referenced with the requested file, the server can choose to push Y along with X instead of waiting for an appropriate client request.
// 5. Increase security: support HTTP/2 via encrpted connections, increasing user and application security

