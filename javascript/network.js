// TCP 三次握手，4次挥手
/*
   TCP: reliable in order byte stream,
   UDP: deliver best effort, User Digram Protocol, not responsible in losing package.
   https://docs.google.com/document/d/1cllo74xs1v5yNaNmtD7YXvGWxuNGSdOTuh9LXIncHW4/edit

   TCP 连接有三个阶段：启动，数据传输，退出
   客户端，服务器

    TCP Header: source port, destination port
    头部长度（图中的数据偏移）以32位字为单位，也就是以4bytes为单位，它只有4位，最大为15，因此头部最大长度为60字节，而其最小为5，也就是头部最小为20字节（可变选项为空）。
    TCP model: two IP address and two ports


    ACK —— 确认，使得确认号有效。
    RST —— 重置连接（经常看到的reset by peer）就是此字段搞的鬼。
    SYN —— 用于初如化一个连接的序列号。
    FIN —— 该报文段的发送方已经结束向对方发送数据。

    当一个连接被建立或被终止时，交换的报文段只包含TCP头部，而没有数据。
   3次握手:
   1. 客户端发送一个SYN段，并指明客户端的初始序列号，即ISN(c). Initial Sequence Number)
   2. 服务端发送自己的SYN段作为应答，同样指明自己的ISN(s)。为了确认客户端的SYN，将ISN(c)+1作为ACK数值。这样，每发送一个SYN，序列号就会加1. 如果有丢失的情况，则会重传。
   3. 为了确认服务器端的SYN，客户端将ISN(s)+1作为返回的ACK数值。

   4 次挥手:
   1. 客户端发送一个FIN段，并包含一个希望接收者看到的自己当前的序列号K. 同时还包含一个ACK表示确认对方最近一次发过来的数据。
   2. 服务端将K值加1作为ACK序号值，表明收到了上一个包。这时上层的应用程序会被告知另一端发起了关闭操作，通常这将引起应用程序发起自己的关闭操作。
   3. 服务端发起自己的FIN段，ACK=K+1, Seq=L
   4. 客户端确认。ACK=L+1

   为什么建立连接是三次握手，而关闭连接却是四次挥手呢？
   这是因为服务端在LISTEN状态下，收到建立连接请求的SYN报文后，把ACK和SYN放在一个报文里发送给客户端。而关闭连接时，当收到对方的FIN报文时，仅仅表示对方不再发送数据了但是还能接收数据，己方是否现在关闭发送数据通道，需要上层应用来决定，因此，己方ACK和FIN一般都会分开发送。
*/