// function Subject() {
//     this.observers = [];
// }

// Subject.prototype = {
//     subscribe: function (fn) {
//         this.observers.push(fn);
//     },
//     unsubscribe: function (fnToRemove) {
//         this.observers = this.observers.filter(fn => fn != fnToRemove);
//     },
//     fire: function (o, thisObj) {
//         const scope = thisObj;
//         this.observers.forEach(fn => fn.call(scope, o));
//     }
// }

// var messageHandler = function (item) {
//     console.log("fired item " + item);
// }
// var subject = new Subject();
// subject.subscribe(messageHandler);
// subject.fire("event #1");
// subject.unsubscribe(messageHandler);
// subject.fire("event #2");
// subject.subscribe(messageHandler);
// subject.fire("event #3");


// function sum(...args) {
//     if ([...args].length == 1) {
//         let cache = [...args][0];
//         let add = (num) => {
//             cache += num;
//             return add;
//         }
//         return add;
//     }
//     else {
//         let res = 0;
//         let list = [...args];
//         list.forEach((num) => { res += num; });
//         return res;
//     }
// }
// console.log(sum(2, 3, 4));
function foo() {
    const args = arguments;
    console.log(args);
    console.log(args.join())
}

foo(1, 2, 3);

