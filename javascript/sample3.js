function reflowAndJustify(lines, maxLen) {
    if (!lines || lines.length === 0) {
        return [];
    }
    const words = lines.join(' ').split(' ');
    const result = [];
    let i = 0;
    while (i < words.length) {
        // split words into lines first
        let remain = maxLen;
        let count = 0;
        while (i < words.length) {
            if (remain - words[i].length < 0) {
                break;
            }
            count++;
            remain -= words[i++].length + 1;
        }
        const line = words.slice(i - count, i);
        console.log("line: ");
        console.log(line);
        // after splitting into lines, calculate the required dashes between each word
        const n = line.reduce((n, word) => n + word.length, 0);
        console.log("n: " + n);
        let reflowed = ''; // line result with padded dashes
        const baseDash = '-'.repeat(parseInt((maxLen - n) / (line.length - 1)));
        let extra = (maxLen - n) % (line.length - 1); // some dashes at the beginning has one extra dash
        for (let j = 0; j < line.length; j++) {
            if (j === line.length - 1) {
                reflowed += line[j];
            } else {
                reflowed += extra-- <= 0 ? line[j] + baseDash : line[j] + baseDash + '-';
            }
        }
        result.push(reflowed);
    }
    return result;
}

var lines = ["The day began as still as the", "night abruptly lighted with", "brilliant flame"];
var maxLen = 25;
var res = reflowAndJustify(lines, maxLen);
console.log(res);

