// Map:
let freqMap = new Map();
freqMap.set(1, "One");
freqMap.set(2, "Two");
let contains = freqMap.has(1);
let size = freqMap.size;

// Array:
let arr = [];
arr.push(1);
arr.push("2");

// remove item from the end of array:
let last = arr.pop();
// remove item from the beginning of the array
let first = arr.shift();
// add item at the beginning of an array:
let newArr = arr.unshift(3);
