/**
You are given coins of different denominations and a total amount of money amount.
Write a function to compute the fewest number of coins that you need to make up that amount.
If that amount of money cannot be made up by any combination of the coins, return -1.

Example 1:

Input: coins = [1, 2, 5], amount = 11
Output: 3
Explanation: 11 = 5 + 5 + 1
Example 2:

Input: coins = [2], amount = 3
Output: -1
Note:
You may assume that you have an infinite number of each kind of coin.
*/

public class Solution {
    /**
    for all j from 0...coins
         without picking  picking
         up jth coin      jth coin
    dp[i] = min(dp[i], 1+dp[i-coins[j]])
    */
    // Running Time Complexity: O(m*n), Space Complexity: O(m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return -1;
        }
        int n = coins.length;
        int m = amount;
        int[] dp = new int[amount+1];
        // can not be Integer.MAX_VALUE because of overflow
        Arrays.fill(dp, amount+1);
        dp[0] = 0;
        for (int i = 1; i <= n; i++) {
            int currCoin = coins[i-1];
            // 完全背包问题，就要 for (int j = 0; j <= m; j++)
            for (int j = 1; j <= m; j++) {
                if (j >= currCoin) {
                    dp[j] = Math.min(dp[j], dp[j-currCoin] + 1);
                }
            }
        }
        return dp[m] > amount ? -1 : dp[m];
    }

    // Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return 0;
        }
        int n = coins.length;
        int m = amount;
        // dp[i][j]: min coins to make up j amount using the first i types of coins
        int[][] dp = new int[n+1][amount+1];
        for (int[] list : dp) {
            Arrays.fill(list, amount+1);
        }
        dp[0][0] = 0;
        for (int i = 1; i <= n; i++) {
            int currCoin = coins[i-1];
            for (int j = 0; j <= m; j++) {
                if (j >= currCoin) {
                    // 跟backPackIV是一样的，用dp[i][j-currCoin] + 1 来比较，而不是dp[i-1][j-currCoin] + 1，因为每个coin可以用无限个
                    dp[i][j] = Math.min(dp[i-1][j], dp[i][j-currCoin] + 1);
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][amount] > amount ? -1 : dp[n][amount];
    }

    // If one coin can be used only once:
    // Running Time Complexity: O(m*n), Space Complexity: O(m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return -1;
        }
        int n = coins.length;
        int m = amount;
        int[] dp = new int[amount+1];
        Arrays.fill(dp, amount+1);
        dp[0] = 0;
        for (int i = 0; i < n; i++) {
            int currCoin = coins[i];
            // 0/1 背包问题，就要 for (int j = m; j >= 1; j--)
            for (int j = m; j >= 1; j--) {
                if (j >= currCoin) {
                    dp[j] = Math.min(dp[j], dp[j-currCoin] + 1);
                }
            }
        }
        return dp[m] > amount ? -1 : dp[m];
    }

    // Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return 0;
        }
        int n = coins.length;
        int m = amount;
        int[][] dp = new int[n+1][amount+1];
        for (int i = 0; i <= n; i++) {
            Arrays.fill(dp[i], amount+1);
        }
        dp[0][0] = 0;
        for (int i = 1; i <= n; i++) {
            int currCoin = coins[i-1];
            for (int j = 0; j <= m; j++) {
                if (j >= currCoin) {
                    // 跟backPackV是一样的，用dp[i-1][j-currCoin] + 1 来比较，而不是dp[i][j-currCoin] + 1，因为每个coin只能用一次
                    dp[i][j] = Math.min(dp[i-1][j], dp[i-1][j-currCoin] + 1);
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m] > amount ? -1 : dp[n][m];
    }
}