/**
Given n nodes labeled from 0 to n-1 and a list of undirected edges (each edge is a pair of nodes), write a function to check whether these edges make up a valid tree.

Example 1:
Input: n = 5, and edges = [[0,1], [0,2], [0,3], [1,4]]
Output: true

Example 2:
Input: n = 5, and edges = [[0,1], [1,2], [2,3], [1,3], [1,4]]
Output: false

Note: you can assume that no duplicate edges will appear in edges. Since all edges are undirected, [0,1] is the same as [1,0] and thus will not appear together in edges.

Show Hint
Given n = 5 and edges = [[0, 1], [1, 2], [3, 4]], what should your return? Is this case a valid tree?

Show More Hint
According to the definition of tree on Wikipedia: “a tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any connected graph without simple cycles is a tree.”
Note: you can assume that no duplicate edges will appear in edges. Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.
*/
public class Solution {
    /**
     * @param n: An integer
     * @param edges: a list of undirected edges
     * @return: true if it's a valid tree, or false
     */
    /**
    Check if the undirected graph has cycle

        Solution 1: DFS
    */
    public boolean validTree(int n, int[][] edges) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }
        for (int[] edge : edges) {
            int from = edge[0];
            int to = edge[1];
            graph.get(from).add(to);
            graph.get(to).add(from);
        }
        // 0 -> not visited
        // 1 -> visiting
        // 2 -> have visited
        int[] visited = new int[n];
        if (isCirclic(graph, visited, 0, -1)) {
            return false;
        }
        // check if all nodes have been visited.
        for (int num : visited) {
            if (num == 0) {
                return false;
            }
        }
        return true;
    }

    private boolean isCirclic(List<List<Integer>> graph, int[] visited, int from, int prev) {
        if (visited[from] == 2) {
            return false;
        }
        else if (visited[from] == 1) {
            return true;
        }
        visited[from] = 1;
        for (int neighbor : graph.get(from)) {
            if (neighbor != prev && isCirclic(graph, visited, neighbor, from)) {
                return true;
            }
        }
        visited[from] = 2;
        return false;
    }

    // Solution 2: BFS:
    public boolean validTree(int n, int[][] edges) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<Integer>());
        }

        for (int[] edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        boolean[] visited = new boolean[n];
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(0);
        while (!queue.isEmpty()) {
            int from = queue.poll();
            if (visited[from]) {
                return false;
            }
            visited[from] = true;
            for (int neighbor : graph.get(from)) {
                if (!visited[neighbor]) {
                    queue.offer(neighbor);
                }
            }
        }

        // check if all nodes have been visited.
        for (boolean visit : visited) {
            if (!visit) {
                return false;
            }
        }

        return true;
    }

    // Solution 3: Union Find:
    public boolean validTree(int n, int[][] edges) {
        // write your code here
        UnionFind uf = new UnionFind(n);
        for (int[] edge : edges) {
            int p1 = edge[0];
            int p2 = edge[1];
            if (uf.isConnected(p1, p2)) {
                return false;
            }
            uf.union(p1, p2);
        }
        return uf.count == 1;
    }

    class UnionFind {
        int[] id;
        int[] size;
        int count;

        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
            this.count = N;
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            count--;
        }

        public boolean isConnected(int p, int q) {
            return find(p) == find(q);
        }
    }

    public boolean validTree(int n, int[][] edges) {
        // initialize n isolated islands
        int[] nums = new int[n];
        Arrays.fill(nums, -1);

        // perform union find
        for (int i = 0; i < edges.length; i++) {
            int x = find(nums, edges[i][0]);
            int y = find(nums, edges[i][1]);

            // if two vertices happen to be in the same set
            // then there's a cycle
            if (x == y) {
                return false;
            }

            // union
            nums[x] = y;
        }

        return edges.length == n - 1;
    }

    private int find(int nums[], int i) {
        if (nums[i] == -1) {
            return i;
        }
        return find(nums, nums[i]);
    }
}