/**
A complete binary tree is a binary tree in which every level, except possibly the last, is completely filled, 
and all nodes are as far left as possible.

Write a data structure CBTInserter that is initialized with a complete binary tree and supports the following operations:

CBTInserter(TreeNode root) initializes the data structure on a given tree with head node root;
CBTInserter.insert(int v) will insert a TreeNode into the tree with value node.val = v 
so that the tree remains complete, and returns the value of the parent of the inserted TreeNode;
CBTInserter.get_root() will return the head node of the tree.

Example 1:

Input: inputs = ["CBTInserter","insert","get_root"], inputs = [[[1]],[2],[]]
Output: [null,1,[1,2]]
Example 2:

Input: inputs = ["CBTInserter","insert","insert","get_root"], inputs = [[[1,2,3,4,5,6]],[7],[8],[]]
Output: [null,3,4,[1,2,3,4,5,6,7,8]]

Note:

The initial given tree is complete and contains between 1 and 1000 nodes.
CBTInserter.insert is called at most 10000 times per test case.
Every value of a given or inserted node is between 0 and 5000.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class CBTInserter {
    /**
    Case 1: Input is complete binary tree
    */
    private List<TreeNode> nodes;
    public CBTInserter(TreeNode root) {
        this.nodes = new ArrayList<>();
        // level order traversal:
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                TreeNode curr = queue.poll();
                nodes.add(curr);
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
        }
    }
    
    public int insert(int v) {
        TreeNode node = new TreeNode(v);
        nodes.add(node);
        // Index of last leaf: n-1
        // Index of parent of this last leaf: (n-1-1) / 2 = n/2 - 1
        int n = nodes.size();
        int parIdx = n/2 - 1;
        TreeNode parNode = nodes.get(parIdx);
        
        if (n % 2 == 0) {
            parNode.left = node;
        }
        else {
            parNode.right = node;
        }
        return parNode.val;
    }
    
    public TreeNode get_root() {
        return nodes.get(0);
    }
}

// Case 2: Input is not complete binary tree
public class CBTInserter {

    private Deque<TreeNode> deque;
    private TreeNode root;
    public CBTInserter(TreeNode root) {
        this.root = root;
        this.deque = new LinkedList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                TreeNode curr = queue.poll();
                // 通过广度优先搜索将 deque 中插入含有 0 个或者 1 个孩子的节点编号。
                if (curr.left == null || curr.right == null) {
                    deque.offerLast(curr);
                }
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
        }
    }
    
    public int insert(int v) {
        TreeNode node = new TreeNode(v);
        TreeNode parNode = deque.peekFirst();
        deque.offerLast(node);
        
        if (parNode.left == null) {
            parNode.left = node;
        }
        else {
            parNode.right = node;
            deque.pollFirst();
        }
        return parNode.val;
    }
    
    public TreeNode get_root() {
        return root;
    }
}

/**
 * Your CBTInserter object will be instantiated and called as such:
 * CBTInserter obj = new CBTInserter(root);
 * int param_1 = obj.insert(v);
 * TreeNode param_2 = obj.get_root();
 */