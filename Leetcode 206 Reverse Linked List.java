/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {

    // Iterative
    // 1 -> 2 -> 3 -> 4 -> 5
    /** 
     *  2 -> 3 -> 4 -> 5
     *  next
     *  head
     * 
     *     1 -> null
     *  newHead
    */

    /**
       1 -> 2
      newHead  curr   next
        null   [1] -> [2] -> null
        newHead  (curr.next = newHead)
        [1]    -> null

    */
    // Iterative:
    public ListNode reverseList(ListNode head) {
        ListNode newHead = null;
        ListNode curr = head;
        while (curr != null) {
            ListNode next = curr.next;
            curr.next = newHead;
            newHead = curr;
            curr = next;
        }
        return newHead;
    }

    // recursive
    public ListNode reverseList(ListNode head) {
        return reverseList(head, null);
    }
    
    public ListNode reverseList(ListNode head, ListNode newHead) {
        if (head == null) {
            return newHead;
        }
        ListNode next = head.next;
        head.next = newHead;
        newHead = head;
        return reverseList(next, newHead);
    }

    // Iterative Solution with the same method of Leetcode 92: Reverse LinkedList II
    // Not move curr and prev pointer.
    public ListNode reverseList(ListNode head) {
        ListNode newHead = new ListNode(0);
        ListNode curr = head;
        newHead.next = head;
        
        int max = countListLength(head);
        for (int i = 1; i < max; i++) {
            ListNode nextNode = curr.next;
            curr.next = nextNode.next;
            nextNode.next = newHead.next;
            newHead.next = nextNode;
        }
        return newHead.next;
    }
    
    private int countListLength(ListNode head) {
        int count = 0;
        if (head == null) {
            return count;
        }
        while (head != null) {
            count++;
            head = head.next;
        }
        return count;
    }

    // Iterative without modifying original linkedlist
    public ListNode reverseList(ListNode head) {
        ListNode newHead = new ListNode(0);
        ListNode node = newHead;
        ListNode curr = head;
        while (curr != null) {
            node.val = curr.val;
            // create a new node and let it pointing to curr Node
            newHead = new ListNode(0);
            newHead.next = node;
            node = newHead;
            // forward current pointer
            curr = curr.next;
        }
        return newHead.next;
    }

    // Stack Iterative Version:
    public ListNode reverseList(ListNode head) {
        Stack<Integer> s = new Stack<Integer>();
        ListNode curr = head;
        while (curr != null) {
            s.push(curr.val);
            curr = curr.next;
        }
        ListNode newHead = new ListNode(0);
        curr = newHead;
        while (!s.isEmpty()) {
            curr.next = new ListNode(s.pop());
            curr = curr.next;
        }
        return newHead.next;
    }
}