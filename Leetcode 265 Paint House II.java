There are a row of n houses, each house can be painted with one of the k colors. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.

The cost of painting each house with a certain color is represented by a n x k cost matrix. For example, costs[0][0] is the cost of painting house 0 with color 0; costs[1][2] is the cost of painting house 1 with color 2, and so on... Find the minimum cost to paint all houses.

Note:
All costs are positive integers.

Follow up:
Could you solve it in O(nk) runtime?


public class solution{
	// solution 1 
	public int minCostII(int[][] costs) {
	    if(costs == null || costs.length == 0) return 0;
	    int m = costs.length, n = costs[0].length;
	    int min1 = 0, min2 = 0, idMin1 = -1;

	    for(int i = 0; i < m; i++) {
	        int m1 = Integer.MAX_VALUE, m2 = m1, idm1 = -1;

	        for(int j = 0; j < n; j++) {
	            // If same color as j - 1, we can only extend from 2nd min of j - 1
	            int cost = costs[i][j] + (j == idMin1 ? min2 : min1);

	            // Update m1 m2 if cost is smaller than any of them
	            if(cost < m1) {               
	                m2 = m1; m1 = cost; idm1 = j;
	            } else if(cost < m2) {        
	                m2 = cost;
	            }
	        }
	        min1 = m1; idMin1 = idm1; min2 = m2; 
	    }
	    return min1;   
	}
	
	// solution 2
	public int minCostII(int[][] costs) {
	    if (costs == null || costs.length == 0) return 0;

	    int n = costs.length, k = costs[0].length;
	    // min1 is the index of the 1st-smallest cost till previous house
	    // min2 is the index of the 2nd-smallest cost till previous house
	    int min1 = -1, min2 = -1;

	    for (int i = 0; i < n; i++) {
	        int last1 = min1, last2 = min2;
	        min1 = -1; min2 = -1;

	        for (int j = 0; j < k; j++) {
	            if (j != last1) {
	                // current color j is different to last min1
	                costs[i][j] += last1 < 0 ? 0 : costs[i - 1][last1];
	            } else {
	                costs[i][j] += last2 < 0 ? 0 : costs[i - 1][last2];
	            }

	            // find the indices of 1st and 2nd smallest cost of painting current house i
	            if (min1 < 0 || costs[i][j] < costs[i][min1]) {
	                min2 = min1; min1 = j;
	            } else if (min2 < 0 || costs[i][j] < costs[i][min2]) {
	                min2 = j;
	            }
	        }
	    }

	    return costs[n - 1][min1];
	}	
}
