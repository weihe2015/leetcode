/*
Given a binary tree

    struct TreeLinkNode {
      TreeLinkNode *left;
      TreeLinkNode *right;
      TreeLinkNode *next;
    }
Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.

Initially, all next pointers are set to NULL.

Note:

You may only use constant extra space.
You may assume that it is a perfect binary tree (ie, all leaves are at the same level, and every parent has two children).
For example,
Given the following perfect binary tree,
         1
       /  \
      2    3
     / \  / \
    4  5  6  7
After calling your function, the tree should look like:
         1 -> NULL
       /  \
      2 -> 3 -> NULL
     / \  / \
    4->5->6->7 -> NULL
*/
/**
 * Definition for binary tree with next pointer.
 * public class TreeLinkNode {
 *     int val;
 *     TreeLinkNode left, right, next;
 *     TreeLinkNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive:
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        if (root.left != null) {
            root.left.next = root.right;
            if (root.next != null) {
                root.right.next = root.next.left;
            }
        }
        connect(root.left);
        connect(root.right);
    }
    // Iterative:
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        // PrevNode is always the leftmost node of each level in the tree:
        TreeLinkNode prev = root, curr = null;
        while (prev.left != null) {
            curr = prev;
            while (curr != null) {
                // connect left and right node in same parent
                if (curr.left != null) {
                    curr.left.next = curr.right;
                }
                // connect right and left node in diff parent
                if (curr.next != null) {
                    curr.right.next = curr.next.left;
                }
                // move to next right sub tree
                curr = curr.next;
            }
            // Go to next level:
            prev = prev.left;
        }
    }

    // more general solution
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        TreeLinkNode newHead = new TreeLinkNode(0), prev = newHead, curr = root;
        while (curr != null) {
            // get the first Node of each level and move prev node to one node rightward:
            if (curr.left != null) {
                prev.next = curr.left;
                prev = prev.next;
            }
            if (curr.right != null) {
                prev.next = curr.right;
                prev = prev.next;
            }
            // Move curr node to one node rightward
            curr = curr.next;
            if (curr == null) {
                // Move the curr node back to the first node of each level.
                curr = newHead.next;
                // Set prev node back to newHead pointer:
                prev = newHead;
                newHead.next = null;
            }
        }
    }

    // Level Order Traversal:, Space Complexity: O(N)
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        Queue<TreeLinkNode> queue = new LinkedList<TreeLinkNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            TreeLinkNode prevNode = null;
            for (int i = 1; i <= size; i++) {
                TreeLinkNode currNode = queue.poll();
                if (i > 1) {
                    prevNode.next = currNode;
                }
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
                prevNode = currNode;
            }
        }
    }
}