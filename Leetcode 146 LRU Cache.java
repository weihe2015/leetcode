/**
Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

Follow up:
Could you do both operations in O(1) time complexity?

Example:

LRUCache cache = new LRUCache( 2 / capacity / );

cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // returns 1
cache.put(3, 3);    // evicts key 2
cache.get(2);       // returns -1 (not found)
cache.put(4, 4);    // evicts key 1
cache.get(1);       // returns -1 (not found)
cache.get(3);       // returns 3
cache.get(4);       // returns 4
*/

// My Solution Time Complexity for put: O(N), Time Complexity for get: O(N)
class LRUCache {
    int capacity;
    Map<Integer, Integer> map;
    // The item in the front of Deque is the most frequently used item.
    // The item at the end of Deque is the least frequently used item.
    Deque<Integer> orderList;
    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.orderList = new LinkedList<>();
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        int val = map.get(key);
        if (orderList.peekFirst() != key) {
            // make this key on the top of the order list queue:
            pushKeyToTopAsFrequentUsed(key);
        }
        return val;
    }

    public void put(int key, int value) {
        if (capacity > 0) {
            // When this item is in the map, we update this item in the order list, but keep the number of capacity the same
            if (map.containsKey(key)) {
                orderList.remove(key);
            }
            // If this item is not in the map, we decrement the number of capacity
            else {
                capacity--;
            }
            orderList.offerFirst(key);
        }
        else {
            // If this item is not in the map, evict the least used item:
            if (!map.containsKey(key)) {
                int prevKey = orderList.pollLast();
                map.remove(prevKey);
                orderList.offerFirst(key);
            }
            // If this item is in the map, we update this item to the top of the deque:
            else {
                // make this key on the top of the order list queue:
                pushKeyToTopAsFrequentUsed(key);
            }
        }
        map.put(key, value);
    }

    // simple version of put:
    public void put(int key, int value) {
        if (!map.containsKey(key) && map.size() == capacity) {
            removeLastKey();
        }
        map.put(key, value);
        pushKeyToTopAsFrequentUsed(key);
    }

    private void pushKeyToTopAsFrequentUsed(int key) {
        orderList.remove(key);
        orderList.offerFirst(key);
    }

    private void removeLastKey() {
        int lastKey = orderList.pollLast();
        map.remove(lastKey);
    }
}

/**
   https://www.youtube.com/watch?v=4wVp97-uqr0
    Time Complexity for PUT: O(1), GET: O(1)
    Idea: Use HashMap to store key value, and the value will be the DoubleLinkedListNode
    Use a doubleLinkedList to have head and tail pointer.
    1. PUT:
         If the key is already existed in the map, we get the DoubleLinkedListNode
            , update its value and push it to the header
         If the key is not in the map:
            1.1 If there is still have capacity:
                We create a new DoubleLinkedListNode, decrement the capacity counter and push it to the header
            1.2 If there is no more capacity:
                We evict the tail node, remove the key of this tail node from the map, create a new DoubleLinkedListNode and push it to the header
    2. GET:
         If the map does not contain this key: return -1
         If the map contains this key:
             2.1 Get this DoubleLinkedListNode from HashMap, push it to the header of the list, and return the value
*/
class LRUCache {
    class DoubleLinkedListNode {
        DoubleLinkedListNode prev, next;
        int key;
        int val;
        public DoubleLinkedListNode() {

        }
        public DoubleLinkedListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    private final int capacity;
    // key: original key value from put(key, value), value: the DoubleLinkedListNode
    private Map<Integer, DoubleLinkedListNode> map;
    private DoubleLinkedListNode head;
    private DoubleLinkedListNode tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.head = new DoubleLinkedListNode();
        this.tail = new DoubleLinkedListNode();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        DoubleLinkedListNode node = map.get(key);
        // If the first item is not this key: we pop it to the top of the list:
        if (head.next != node) {
            // remove itself:
            unLinkNode(node);
            addNodeToFront(node);
        }
        return node.val;
    }

    public void put(int key, int value) {
        DoubleLinkedListNode node = null;
        if (!map.containsKey(key)) {
            if (map.size() == capacity) {
                // Remove the tail node from the list
                removeLastNode();
            }
            node = new DoubleLinkedListNode(key, value);
        }
        else {
            // When this item is in the map, we update this item in the order list
            // make this key on the top of the order list queue:
            node = map.get(key);
            // update the node value:
            node.val = value;
            // remove itself:
            unLinkNode(node);
        }
        // add itself to the header:
        addNodeToFront(node);
        map.put(key, node);
    }

    /**
        remove key in the LRU cache if exists.return the previous value associated with {@code key}
        If the key does not exist in LRU, return -1 
    */
    public int remove(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        ListNode node = map.get(key);
        unLinkNode(node);
        map.remove(key);
        capacity++;
        return node.value;
    }

    private void removeLastNode() {
        DoubleLinkedListNode lastNode = tail.prev;
        if (lastNode == head) {
            return;
        }

        unLinkNode(lastNode);
        int key = lastNode.key
        map.remove(key);
    }

    private void unLinkNode(DoubleLinkedListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
        resetNode(node);
    }

    private void addNodeToFront(DoubleLinkedListNode node) {
        // add itself to the header:
        DoubleLinkedListNode prevItem = head.next;
        head.next = node;
        node.prev = head;

        node.next = prevItem;
        prevItem.prev = node;
    }

    private void resetNode(DoubleLinkedListNode node) {
        node.prev = null;
        node.next = null;
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */