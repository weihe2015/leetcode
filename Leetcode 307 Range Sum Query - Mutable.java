/**
 * Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.

    The update(i, val) function modifies nums by updating the element at index i to val.

    Example:

    Given nums = [1, 3, 5]

    sumRange(0, 2) -> 9
    update(1, 2)
    sumRange(0, 2) -> 8
    Note:

    The array is only modifiable by the update function.
    You may assume the number of calls to update and sumRange function is distributed evenly.
 * 
*/
public class NumArray {
    private int[] BIT;
    private int[] nums;
    private int N;
    public NumArray(int[] nums) {
        this.nums = nums;
        int n = nums.length;
        this.N = n+1;
        this.BIT = new int[N];
        for (int i = 1; i <= n; i++) {
            update(i, nums[i-1], N);
        }
    }
    
    private void update(int idx, int value, int N) {
        while (idx < N) {
            BIT[idx] += value;
            idx = getNext(idx);
        }
    }
    
    public int getSum(int idx) {
        idx = idx + 1;
        int sum = 0;
        while (idx > 0) {
            sum += BIT[idx];
            idx = getParent(idx);
        }
        return sum;
    }
    
    private int getParent(int idx) {
        return idx - (idx & (-idx));
    }
    
    private int getNext(int idx) {
        return idx + (idx & (-idx));
    }
    
    public void update(int i, int val) {
        int diff = val - nums[i];
        update(diff, i+1, N);
        nums[i] = val;
    }
    
    public int sumRange(int i, int j) {
        return getSum(j) - getSum(i-1);
    }
}

// Your NumArray object will be instantiated and called as such:
// NumArray numArray = new NumArray(nums);
// numArray.sumRange(0, 1);
// numArray.update(1, 10);
// numArray.sumRange(1, 2);


// Solution 2:
// UPDATE: Running Time Complexity O(1),
// GET SUM: Running Time Complexity: O(N)
public class NumArray {
    private int[] nums; 
    public NumArray(int[] nums) {
        this.nums = nums;
    }
    
    public void update(int i, int val) {
        nums[i] = val;
    }
    
    public int sumRange(int i, int j) {
        int sum = 0;
        for (int k = i; k <= j; k++) {
            sum += nums[k];
        }
        return sum;
    }
}

// Solution 3: Use PrefixSum:
// UPDATE: Running Time Complexity O(N),
// GET SUM: Running Time Complexity: O(1)
class NumArray {
    private int[] nums;
    private int[] prefixSum;

    public NumArray(int[] nums) {
        this.nums = nums;
        this.prefixSum = new int[nums.length];
        int sum = 0;
        for (int i = 0, n = nums.length; i < n; i++) {
            sum += nums[i];
            prefixSum[i] = sum;
        }
    }
    
    public int sumRange(int i, int j) {
        if (i == 0) {
            return prefixSum[j];
        }
        return prefixSum[j] - prefixSum[i-1];
    }
    
    public void update(int i, int val) {
        int diff = val - nums[i];
        for (int k = i, n = nums.length; k < n; k++) {
            prefixSum[k] += diff;
        }
        nums[i] = val;
    }
}
