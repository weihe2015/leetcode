/**
Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

Note:

The solution set must not contain duplicate triplets.

Example:

Given array nums = [-1, 0, 1, 2, -1, -4],

A solution set is:
[
  [-1, 0, 1],
  [-1, -1, 2]
]
*/
public class Solution {
    // Use Set to avoid duplicate answers:
    public List<List<Integer>> threeSum(int[] nums) {
        int target = 0;
        Set<List<Integer>> result = new HashSet<List<Integer>>();
        if (nums == null || nums.length < 3) {
            return new ArrayList<List<Integer>>(result);
        }
        Arrays.sort(nums);
        for (int i = 0, n = nums.length; i < n-2; i++) {
            int diff1 = target - nums[i];
            int low = i+1, high = n-1;
            while (low < high) {
                int sum = nums[low] + nums[high];
                if (sum == diff1) {
                    result.add(Arrays.asList(nums[i], nums[low], nums[high]));
                    low++; 
                    high--;
                }
                else if (sum < diff1) {
                    low++;
                }
                else {
                    high--;
                }
            }
        }
        return new ArrayList<List<Integer>>(result);
    }

    // Running time: O(n^2) Space Complexity: O(1)
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        int n = nums.length;
        if (n == 0) {
            return result;
        }
        int target = 0;
        // It uses quickSort to sort the array.
        java.util.Arrays.sort(nums);

        for (int i = 0; i < N - 2; i++){
            // skip the duplicates;
            if (i > 0 && nums[i] == nums[i-1]) {
                continue;
            }
            int j = i+1;
            int k = n-1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == target) {
                    result.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    // skip the duplicates:
                    while (j < k && nums[j] == nums[j+1]) {
                        j++;
                    }
                    // skip the duplicates;
                    while (j < k && nums[k] == nums[k-1]) {
                        k--;
                    }
                    j++; 
                    k--;
                }
                if (sum < target) {
                    j++;
                }
                else {
                    k--;
                }
            }
        }
        return result;
    }
}