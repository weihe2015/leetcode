public class Solution {
Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.

For example:

Given num = 38, the process is like: 3 + 8 = 11, 1 + 1 = 2. 
Since 2 has only one digit, return it.
	// formula
    public int addDigits(int num) {
        if(num == 0)
          return 0;
        else
          return (num - 1) % 9 + 1;
    }

    // my solution no formula
    public int addDigits(int num) {
        int sum = 0;
        while(num >= 10){
            while(num > 0){
                sum += num % 10;
                num /= 10;
            }
            num = sum;
            sum = 0;
        }
        return num;
    }
}