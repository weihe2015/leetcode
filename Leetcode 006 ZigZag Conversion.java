public class Solution {
    public String convert(String s, int nRows) {
        int len = s.length();
        StringBuffer[] sb = new StringBuffer[nRows];
        for(int i = 0; i < nRows; i++)
            sb[i] = new StringBuffer();
            
        int i = 0;
        while(i < len){
            for(int j = 0; j < nRows && i < len; j++){
                sb[j].append(s.charAt(i));
                i++;
            }
            for(int j = nRows - 2; j >= 1 && i < len; j--){
                sb[j].append(s.charAt(i));
                i++;
            }
        }
        for(i = 1; i < nRows; i++){
            sb[0].append(sb[i]);
        }
        return sb[0].toString();
    }
}