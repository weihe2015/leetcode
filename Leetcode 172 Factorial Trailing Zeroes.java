Given an integer n, return the number of trailing zeroes in n!.

public class Solution {
    public int trailingZeroes(int n) {
        if(n < 0)
            return -1;
        int count = 0;
        for(int i = 5; (n/i)>=1; n/=5){
            count += n/i;
        }
        return count;
    }
    // one line solution
    public int trailingZeroes(int n) {
        return n == 0 ? 0 : n/5 + trailingZeroes(n/5);
    }
    public int trailingZeroes(int n) {
        int c = 0;
        while(n > 0){
            n /= 5;
            c += n;
        }
        return c;
    }
}