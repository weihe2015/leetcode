/**
Suppose you are at a party with n people (labeled from 0 to n - 1) 
and among them, there may exist one celebrity. 

The definition of a celebrity is that all the other n - 1 people know him/her but he/she does not know any of them.

Now you want to find out who the celebrity is or verify that there is not one. 
The only thing you are allowed to do is to ask questions like: 
"Hi, A. Do you know B?" to get information of whether A knows B. 

You need to find out the celebrity (or verify there is not one) by asking as few questions as possible (in the asymptotic sense).

You are given a helper function bool knows(a, b) which tells you whether A knows B. 

Implement a function int findCelebrity(n), your function should minimize the number of calls to knows.

Note: There will be exactly one celebrity if he/she is in the party. 
Return the celebrity's label if there is a celebrity in the party. If there is no celebrity, return -1.
*/
public class Solution extends Relation {
    // Running Time Complexity: O(N^2), Space Complexity: O(N)
    public int findCelebrity(int n) {
        // Assume all people are celebrity
        boolean[] candidates = new boolean[n];
        Arrays.fill(candidates, true);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (candidates[i] && i != j) {
                    // if i knows j or j does not know i, ==> i is not celebrity:
                    if (knows(i, j) || !knows(j, i)) {
                        candidates[i] = false;
                        break;
                    }
                    else {
                        // if j knows i or i does not know j, ==> j is not celebrity:
                        candidates[j] = false;
                    }
                }
            }
            if (candidates[i]) {
                return i;
            }
        }
        return -1;
    }
    // Running Time Complexity: O(N^2), Space Complexity: O(1)
    // It will reduce the space complexity but have more calls to knows function
    public int findCelebrity(int n) {
        for (int i = 0, j = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (i != j && (knows(i, j) || !knows(j, i))) {
                    break;
                }    
            }
            if (j == n) {
                return i;
            }
        }
        return -1;
    }

    public int findCelebrity(int n) {
        int candidate = 0;
        for (int i = 0; i < n; i++) {
            // if person candidate knows person i, then person i may be the celebrity
            if (knows(candidate, i)) {
                candidate = i;
            }
        }
        // verify this celebrity:
        for (int i = 0; i < n; i++) {
            if (i != candidate && (knows(candidate, i) || !knows(i, candidate))) {
                return -1;
            }
        }
        return candidate;
    }
}