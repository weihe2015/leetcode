/**
Given a sorted integer array without duplicates, return the summary of its ranges.

Example 1:

Input:  [0,1,2,4,5,7]
Output: ["0->2","4->5","7"]
Explanation: 0,1,2 form a continuous range; 4,5 form a continuous range.
Example 2:

Input:  [0,2,3,4,6,8,9]
Output: ["0","2->4","6","8->9"]
Explanation: 2,3,4 form a continuous range; 8,9 form a continuous range.
*/
public class Solution {
    // Brute Force: Use Interval Class:
    // Similar to Meeting Room I: https://leetcode.com/problems/meeting-rooms
    // Running Time Complexity: O(N), Space Complexity: O(1)
    public List<String> summaryRanges(int[] nums) {
        List<String> res = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return res;
        }
        int n = nums.length;
        Interval prevInterval = new Interval(nums[0], nums[0]);
        for (int i = 1; i < n; i++) {
            if (nums[i] == prevInterval.end + 1) {
                prevInterval.end = nums[i];
            }
            else {
                String path = buildPath(prevInterval);
                res.add(path);
                prevInterval.start = nums[i];
                prevInterval.end = nums[i];
            }
        }
        String path = buildPath(prevInterval);
        res.add(path);
        return res;
    }

    private String buildPath(Interval interval) {
        if (interval.start == interval.end) {
            return String.valueOf(interval.start);
        }
        return String.format("%d->%d", interval.start, interval.end);
    }

    class Interval {
        int start;
        int end;
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    // Solution 2: use two pointers:
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public List<String> summaryRanges(int[] nums) {
        List<String> res = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return res;
        }
        int start = nums[0], end = nums[0], n = nums.length;
        for (int i = 1; i < n; i++) {
            if (end == nums[i] - 1) {
                end = nums[i];
            }
            else {
                res.add(buildPath(start, end));
                start = nums[i];
                end = nums[i];
            }
        }
        res.add(buildPath(start, end));
        return res;
    }

    private String buildPath(int start, int end) {
        if (start == end) {
            return String.valueOf(start);
        }
        return String.format("%d->%d", start, end);
    }
}