/**
There is a ball in a maze with empty spaces and walls. 
The ball can go through empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the ball's start position, the destination and the maze, find the shortest distance for the ball to stop at the destination. The distance is defined by the number of empty spaces traveled by the ball from the start position (excluded) to the destination (included). If the ball cannot stop at the destination, return -1.

The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. 
You may assume that the borders of the maze are all walls. The start and destination coordinates are represented by row and column indexes.

Example 1:

Input 1: a maze represented by a 2D array

0 0 1 0 0
0 0 0 0 0
0 0 0 1 0
1 1 0 1 1
0 0 0 0 0

Input 2: start coordinate (rowStart, colStart) = (0, 4)
Input 3: destination coordinate (rowDest, colDest) = (4, 4)

Output: 12

Explanation: One shortest way is : left -> down -> left -> down -> right -> down -> right.
             The total distance is 1 + 1 + 3 + 1 + 2 + 2 + 2 = 12.

Example 2:

Input 1: a maze represented by a 2D array

0 0 1 0 0
0 0 0 0 0
0 0 0 1 0
1 1 0 1 1
0 0 0 0 0

Input 2: start coordinate (rowStart, colStart) = (0, 4)
Input 3: destination coordinate (rowDest, colDest) = (3, 2)

Output: -1

Explanation: There is no way for the ball to stop at the destination.

Note:

There is only one ball and one destination in the maze.
Both the ball and the destination exist on an empty space, and they will not be at the same position initially.
The given maze does not contain border (like the red rectangle in the example pictures), but you could assume the border of the maze are all walls.
The maze contains at least 2 empty spaces, and both the width and height of the maze won't exceed 100.
*/
public class Solution {

    class Position {
        int x;
        int y;
        int len;
        public Position(int x, int y, int len) {
            this.x = x;
            this.y = y;
            this.len = len;
        }
    }

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        // write your code here
        int m = maze.length;
        int n = maze[0].length;
        boolean[][] visited = new boolean[m][n];

        Queue<Position> pq = new PriorityQueue<>(Comparator.comparingInt(p -> p.len));
        pq.offer(new Position(start[0], start[1], 0));

        while (!pq.isEmpty()) {
            Position currPos = pq.poll();
            int i = currPos.x;
            int j = currPos.y;
            if (i == destination[0] && j == destination[1]) {
                return currPos.len;
            }
            if (visited[i][j]) {
                continue;
            }
            visited[i][j] = true;
            for (int[] dir : dirs) {
                int x = i + dir[0];
                int y = j + dir[1];
                int currLen = currPos.len + 1;
                while (x >= 0 && x < m && y >= 0 && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    currLen++;
                }
                x -= dir[0];
                y -= dir[1];
                currLen--;
                if (!visited[x][y]) {
                    pq.offer(new Position(x, y, currLen));
                }
            }
        }
        return -1;
    }

    // BFS: use array instead of a new class
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int shortestDistance(int[][] maze, int[] start, int[] end) {
        // write your code here
        int m = maze.length;
        int n = maze[0].length;
        
        boolean[][] visited = new boolean[m][n];
        Queue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(l -> l[2]));
        pq.offer(new int[]{start[0], start[1], 0});
        
        while (!pq.isEmpty()) {
            int[] curr = pq.poll();
            int i = curr[0];
            int j = curr[1];
            int dist = curr[2];
            if (i == end[0] && j == end[1]) {
                return dist;
            }
            if (visited[i][j]) {
                continue;
            }
            visited[i][j] = true;
            for (int[] dir : dirs) {
                int x = i + dir[0];
                int y = j + dir[1];
                int currDist = dist + 1;
                if (!isValid(maze, x, y, m, n)) {
                    continue;
                }
                while (isValid(maze, x, y, m, n)) {
                    x += dir[0];
                    y += dir[1];
                    currDist++;
                }
                x -= dir[0];
                y -= dir[1];
                currDist--;
                if (!visited[x][y]) {
                    pq.offer(new int[]{x, y, currDist});
                }
            }
        }
        return -1;
    }
    
    private boolean isValid(int[][] maze, int x, int y, int m, int n) {
        return x >= 0 && x < m && y >= 0 && y < n && maze[x][y] == 0;
    }
}