Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    private ListNode node;
    public TreeNode sortedListToBST(ListNode head) {
        if(head == null)
            return null;
        int size = 0;
        ListNode curr = head;
        node = head;
        while(curr != null){
            curr = curr.next;
            size++;
        }
        return inorder(0,size-1);
    }
    
    public TreeNode inorder(int start, int end){
        if(start > end)
            return null;
        int mid = start + (end - start) / 2;
        TreeNode left = inorder(start, mid - 1);
        TreeNode newhead = new TreeNode(node.val);
        newhead.left = left;
        node = node.next;
        
        TreeNode right = inorder(mid+1,end);
        newhead.right = right;
        return newhead;
    }
}