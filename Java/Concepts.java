/**
Generic: 
    -- Introduced in JDK 5.0, aim of reducing bugs and adding an extra layer of abstraction over types.
    -- T, E, K, V:
        -- T: Type: specific type
        -- E: Element
        -- K, V, key value

Abstraction: 
    - Simple things to represent complexity: Object/Classes

Encapsulation: 
    - Data hiding: A language feature to restrict access to members of an object. For example, private and protected members
    - Bundling of data and methods together: Data and methods that operate on that data are bundled together.
    - Keep fields within class as private and provide access by using public method, keep the data and code safe within the class itself

Inheritance:
    - A class is based on another class and uses data and implementation of the other class
    - Code reuse
    - In Java this is achieved by using extends keyword. Only properties with access modifier public and protected  can be accessed in child class.
    - Java does not allow to extend multiple classes because it leads to Diamond Problem
    - Java allows implement multiple interfaces

Polymorphism:
    - Compile time polymorphsim (Overloading)
        - Overriding occurs when a class method in a child class has the same name and signature as a method in the parent class. 
          When you override methods, JVM determines the proper method to call at the program’s run time, not at the compile time.
    - Runtime Polymorphism
        - Overloading is determined at the compile time. It occurs when several methods have same names with
            - Different method signature and different number or type of parameters.
            - Same method signature but the different number of parameters.
            - Same method signature and same number of parameters but of different type
    - Not support multilevel polymorphism
    - Compared with C++:
        -- method in parent class needs to have "Virtual" keyword, can be overwritten
        -- Need to check all the virtual function table,

Aggregation:
    - Aggregation is a specialized form of Association where all objects have their own lifecycle but there is ownership and child object can not belongs to another parent object
    - It represents Has-A relationship.
    - It is a unidirectional association i.e. a one way relationship. For example, department can have students but vice versa is not possible and thus unidirectional in nature.
    - In aggregation (City, Tree, Car) "sub objects" (Tree, Car) will NOT be destroyed when City is destroyed.

    - Dependency: Aggregation implies a relationship where the child can exist independently of the parent. For example, Bank and Employee, delete the Bank and the Employee still exist. whereas Composition implies a relationship where the child cannot exist independent of the parent. Example: Human and heart, heart don’t exist separate to a Human
        - Type of association: Composition is a strong Association whereas Aggregation is a weak Association.
        - Type of Relationship: Aggregation relation is “has-a” and composition is “part-of” relation.
    Composition:
        - composition stresses on mutual existence
        - "sub objects" (Heart, Hand) will be destroyed as soon as Person is destroyed.

Class:
    - A class is the blueprint from which individual objects are created

Interface:
    - Use interface when something in my design/algorithm will change frequently, Strategy pattern 

Abstract Class:
    - You may have abstract class that provide default behavior, such as event, message and error handling

Interface vs Abstract class:
    - Type of methods: Interface can have only abstract methods, Abstract class can have abstract and non-abstract methods. From Java 8, it can have default and static method in Interface
    - Final Variables: Variables declared in a Java Interface are by default final. An abstract class may contain non-final variables
    - Types of variables: Abstract class can have final, non-final, static and non-static variables. Interface has only static and final variables

    - Interface: implements; Abstract: extends
    - Multiple implementation: An interface can extend one Interface only, Abstract Class can extends another Abstract Class and implements multiple Interface.
    - Member of Java Interface are public by default. A Java abstract class can have class member like private, protected, etc.

Abstract Method:
    - It is a method that is declared, but contains no implementation, may not be instantiated, and require subclass to provide implementation.

Primitive Types:
    -- Wrapper class: Integer, Byte, Long, Float, Double, Character, Boolean and Short
    -- All primitive wrapper classes (Integer, Byte, Long, Float, Double, Character, Boolean and Short) are immutable in Java
    -- Stack memory stores primitive types and the addresses of objects

Volatile Keyword:
    - Mark a Java variable as "being stored in main memory". Every read of a volatile variable will be read from computer's main memory, not from CPU cache, and every write to a volatile variable will be written to main memory, and not just the CPU cache.
    - When a volatile variable is written, all variables visible to this thread are also written to main memory

Exception: https://www.youtube.com/watch?v=jWQbHE0lC2I
    * Exception:
        -- An event that disrupts the normal flow of the application.
    * hierarchy:
        -- Throwable -> Error + Exception (checked) -> Runtime Exceptio (unchecked)
            -- Error: Virtual Machine Error, Assertion Error
    * Checked Exception:
        -- Represent a potential problem that the user knows can happen. Force the user to establish an error handling policy at compile time.
        -- IOException, InterruptedException, FileNotFoundException
    * Uncheck Exception:
        --  Represent an unexpected defect in the application that happens at runtime. They do not force the user to establish an error handling policy at compile time
        -- Examples:
            - NullPointerException
            - IllegalArgumentException
    * Custom Exception
        - Create a class that derives from Exception or RuntimeException

Concurrency:
    http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html

    - Concurrency v.s multiple tasks:
        * Concurrency: multiple tasks which start, run, completed in overlapping time periods.
        * Parallemlism: multiple tasks running at the same time.
    - Synchronized keyword:
        * Place: 
            -- Code Block, method
        * Lock:
            - acquire the lock on lockObject‘s monitor
        
    - Synchronized v.s ReentrantLock:
        * Can obtain the lock that it is currently have.
        * Synchronized depends on JVM while ReentrantLock depends on API, implemented in JDK
        * Synchornized, no fair: meaning if fair, first come first get the lock
        * ReentrantLock, default is no fair, but constructor can have fair.
    - Thread status:
        - Runnable
        - Waiting
        - Terminated
        - Blocked
        - Timed_waiting
    - DeadLock:
        - multiple threads are in blocked state to wait for the resources infinity, the program cannot be stopped.
        - Ex: thread1 holds res2, and ask for res1, thread2 holds res1 and ask for res2. They are in deadlock.
        - Four conditions:
            - mutual exclusion: one resource is hold by one thread
            - hold and wait: The thread holds the resource while waiting for others.
                - Break: request all resouces at once
            - no pre-emption: the thread has the resource until it has completed using it.
                - Break: release its resouce when the thread cannot obtain other resource
            - circular wait: A circular chain of process
                - Break: release resource in the reverse order of obtaining the resource.

    - sleep() vs wait():
        - sleep(): 
            -- stop executing, no release lock.
            -- sleep(long timeout) will be wakeup after timeout.
            -- Send current thread into "Not Runnable" state, keeps the monitors it has required.
            -- Will be broken when t.interrupt() is called.
            -- call on a thread, always currently executing thread.
        - wait(): 
            -- thread communication, release lock.
            -- need notify() or notifyAll() to wake up threads
            -- or wait(long timeout) to wake up itself after timeout.
            -- call on an object.
    - start() vs run():
        - start(): trigger thread to become runnable/ready, and execute run() method
        - run(): theat run() as normal method to execute in main thread.
    - execute() vs submit():
        - execute(): submit task that do not need to return value. Cannot determine if it has been executed or not.
        - submit(): submit task that requires return value, ThreadPool will return a Future Object, get() to get the return value, get() will be blocked until currennt thread has been completed.

    - CAS: Compare And Swap:
        - optimisitic lock, hope that you can complete it without interference.
        - Atomic Operation
        - compareAndSet()
        - Step:
            -- 需要读写的内存值 V,
            -- 进行比较的值 A。
            -- 要写入的新值 B。

    - scheduling:
        - preemptive, priority-based scheduler, each thread is assigned certain priority. Default priority is 5. range [1,10], where 1 is MIN_PRIORITY and 10 is MAX_PRIORITY. thread.setPriority()
    - Yield:
        - Not guaranteed to have any effect at all.
        - A yielding thread tells the virtual machine that it is willing to let other threads be scheduled in its place.
        - There is no guarantee that Yield will make the currently executing thread to runnable state immediately.
        - It can only make a thread from Running State to Runnable State, not in wait or blocked state.
    - Join:
        - join the start of thread's execution to the end of another thread's execution.
        - join() is called on a Thread instance, the currently running thread will block until the Thread instance has finished executing.

CountDownLatch:
    -- A synchronization aid that allows one or more threads to wait until a set of operations being performed in other threads completes.

CyclicBarrier:
    -- A synchronization aid that allows a set of threads to all wait for each other to reach a common barrier point.

    -- Both CountDownLatch and CyclicBarrier are used for managing multi-threaded applications

    -- CyclicBarrier allows a number of threads to wait on each other, whereas CountDownLatch allows one or more threads to wait for a number of tasks to complete.

JVM Memory:
    https://plumbr.io/handbook/garbage-collection-algorithms

    * Component in JVM Memory Model
        * Heap: permenent and their reference located at. static variables, static objects, reference to classes (Loader), Permenant Generation -> class loaders (remove since Java 8, replace by Metaspace)
            -- Further split as (Young Generation + Old/Tenured Generation -> objects older than 15 cycles)
            -- New Object created are in Young Generation.
            -- Minor GC gets in when YG is filled up, will empty YG.
                -- Young Generation can be further divided into 3 parts:
                    -- Eden Space
                    -- FromSpace (Survivor1)
                    -- ToSpace (Survivor2)

        * Stack: local primitive variables, method parameters, local variables inside method
        * Method Area (Perm Generation): compiled class files are stored. A part of Heap. all class level informations like class name, immediate parent class name, methods and variables information are stored.
        * Native Memory, socket connection:
        * PC register: Program Counter, thread next to do.
    
    * How does young generation work?
        -- All new allocation happens in Eden Space, pointer increment.
        -- When eden fills up, stop-the-world copy the collection to Servivor Space
            -- references on the stack in almost any garbage collection scheme.
            -- Live Object are traversed from GC roots, which is stack and CPU 
            -- Dead objects cost zero to collect
            -- After several collections, surviviors get tenured into old generation. 
    
    * Ideal Young Generation Ops:
        -- Big enough to hold more than one set of all concurrent request response cycle objects
            -- HTTP server, 50 threads, each needs 10 MB RAM.
        -- Each survivor space big enough to hold active requests objects + tenuring ones.

    * Tuning Young Generation:
        -- Enable -XX:+PrintGCDetails, -XX:+PrintHeapAtGC, and -XX:+PrintTenuringDistribution
        -- Determine "desired survivor size"
        -- no such thing called "desired Eden size", the bigger Eden size, the better.
        -- Watch the tenuring thresholds, might need to tune it to tenure long live objects faster

        -- -XX:+PrintHeapAtGC:
            eden
            from  better below 100%. If goes to 100%, not all survivor objects can fit into the surviror space, force into tenure/Old Generation
            to
        -- -XX:+PrintTenuringDistribution:
            -- If not strongly declining, memory load is increasing. It means 2 things: 1) application is starting, creating object buffer. 2) Memory Leak.
    
    * Tuning the CMS (Concurrent Mark Sweep):
        -- More memory, less punitive miscalculation.
        -- Try using CMS without tuning. Use -verbosegc and -XX:+PrintGCDetails.
        -- If full GC, tune Young Generation.

    * Tuning Old Generation
        -- Find min or max working set size (Observe Full GC numbers under stable state and under load)
        -- Overprovision the number by 25-33% 
            -- Gives CMS a cushion to concurrently clean memory as it's used.
        -- Set -XX:InitiatingOccupancyFraction to between 80-75, respectively.
            -- Corresponds to overprovisioned heap ratio.
        -- Lower initiating occupancy fraction to 0 if you have CPU to spare.

    * Tuning Young Generation:
        -- Too many live objects during YG GC:
            -- Reduce NewSize, reduce survivor spaces, reduce tenuring threshold.
        -- Too many threads:
            -- Find the min concurrency level
            -- Split the service into several JVMs.

    * Garbage Collection: -XX:+UseSerialGC
        * Step:
            1. Mark: what objects are not used.
            2. Normal Deletion: being removed from heap
            3. Deletion + Compacting: Memory allocation are group together and can be faster

        * Static:
            * Static variables cannot be elected for GC while the class is loaded. They can be elected for GC only when the ClassLoader drops the class or class loader itself become garbage collected during program.
            * They are GC root when their classes are loaded.

        * JVM provideds different Garbage Collection to choose from
            * Serial GC: (Thoughput collectors)
                Mark & Delete object from young and Old Generation. Smaller CPU, not solution for large scale application, where lots of thread running in the heap.
            * Parallel GC: (Thoughput collectors)
                Similar to Serial GC but are multiple thread, based on # of CPU cores. Use 1 thread for old Generation but N thread for Young Generation in parallel. Less chance for old generation to get GC
            * Parallel old GC: (Thoughput collectors)
                similar to Parallel GC, but have N threads on both Young & Old Generation.
            * Concurrent Mark Sweep: (Low Pause Collectors)
                -- It does the GC for Old Generation only. Minimize the pause during GC. Use concurrently mechanism in application thread, Young Generation use same algorithm as in Parallel GC.
                -- Keep the gragmentation low
                -- Avoid full GC stops. 
            * G1 GC: Garbage 1st GC. Java 7+, default GC for Java 9, no concept of Young/Old generation, first collect the region of less live data.

        * Different Types:
            * Minor Garbage Collection: 
                * Collecting garbage from Young Generation
                * It is triggered when JVM is unable to allocate space to a new Object,
                    -- e.g: Eden is full.
                * Whenever the pool is filled, whole content is copied so that pointer can start tracking from zero.
                * During Minor GC Event, Tenured Generation is ignored.
                * all minor GC do trigger stop-the-world pauses, stopping application threads, the length of pause is negligible latency-wise.

            * Major GC:
                * Clean the Old/Tenure Generation
                * many major GCs are triggered by Minor GC
            * Full GC:
                * Clean entire Heap, Young + Old Generation.

        * Algorithm:
            * GC roots:
                -- Local variable and input parameters of the currently executing methods
                -- Active threads
                -- Static field of the loaded classes
                -- JNI references
            * Idea:
                * Next, GC traverses the whole object graph in your memory, starting from those Garbage Collection Roots and following references from the roots to other objects, e.g. instance fields. Every object the GC visits is marked as alive.

    * PermGen vs Metaspace:
        * PermGen == Method Area, replaced by Metaspace.
            ** Used to store object and class.
            ** Apps have lots of class loaded, have more PermGen Utilization
            ** Results in OOM error due to PermGen Space.
        * Metaspace:
            ** A part of Native Memory (Process Memory) OS level, do not merge with heap memory.
            ** Process size can go large since Metaspace goes into the OS.
            ** Cons: Too many class can take down server.

    * Monitor JVM:
        * Jvisual VM:
            * JVM arguments, System Properties, 
            * Monitor GC activities. Heap Space and Metaspace.
            * Check status of threads.
            * Connect to remote server on UI.
        * Thread Dump: either in JVisual VM or JStack command
            -- Take a snapshot of threads and their stacks.
            -- See flow of threads and current stage of threads, which methods is processing long time.
            -- jstack -l PID
        * Jstat:
            -- Get Stats of JVM.

Memory Leak:
    * Heap Size:
        -- For Java 11 and above: 
            -- -Xmx (Maximum Java Heap Size) is 25% of available memory with 25 GB max
            -- 2GB less memory, value is set to 50% available memory with min Val is 16 MB and 512 MB
        -- For Java 8:
            -- Half available memory with min 16 MB and max 512 MB.
    
    * Memory Leak Example:
        -- Static variables, where they cannot be collected in GC when classes are loaded.
        -- Unclosed stream.
        -- Unclosed connection ex: socket connection
        -- Adding object with no hashcode equals defined in the hashset.
        -- String.intern() -> place a large string in JVM memory pool.

    * Object Header:
        -- JVM object header, 2 machine words.
            -- 1 word for class pointer
            -- 1 word for lock
        -- Object takes 16 bytes
        -- new byte[0] takes 24 bytes.
            -- 16 bytes of header,
            -- 4 bytes of array size
            -- 4 bytes padding of next multiple of 8. JVM adds padding to the objects so that their size is a multiple of 8 bytes
        -- A class is 16 bytes.
        -- Compress Pointer: performs a little bit more computation to save some space

    * Methodology:
        1. Do I have a leak?
            -- Check GC Log, Curve after full GC is flat, but with Memory leak, it is increasing.
        2. What is leaking?
            -- Class histogram.
        3. What is keeping object alive (instace in the app)?
            -- JVisualVM, stack trace, find out which method is leaking.
        4. Where is it leaking from (code where the objects are created and/or assigned)?

    * Prevent:
        1. Enable verbose GC
        2. Do Profiling
        3. Review the code

    * Tools:
        * GC Logging:
            -- Logs remains after JVM terminates
            -- Always have GC Logging turn on.
            -- How long they take, How much heap is being used.
            -- Can see Memory leak in GC log much eariler than OutOfMemoryException.
        * GCViewer:
            -- Suitable for prod, views GC logs
        * Heap dumping: 
            -- Suitable for prod, but freezes JVM so only when necessary
        * Eclipse MAT:
            -- Suitable for prod, views Heap Dumps.
        * JVisualVM:
            -- Not Suitable for prod, needs a live JVM and can crash it.
        * JPS:
            -- Inside the jdk
            -- Find the PID of Java program.
    * Heap Dump:
        -- -XX:+HeapDumpOnOutOfMemoryError
        -- jmap -dumplive,file=<file-path> <pid>
            -- or without "live", if you want to see dead objects that have not yet been GCed. "live" forces a GC before the dump  
        -- JMX: HotSpotDiagositc.dumpHeap()
            -- Eg from jconsole, visualvm, even programmatically
        -- jcmd <pid> GC.heap_dump <file-path>
    * Class histogram:
        * jmap -histo:live <pid>
            -- Heap dump histogram
            -- Most profilers memory analysis histogram.
            -- Use jmap to see # of instances, # of bytes of classes,
            -- Tells you which class is leaking.


Java Class:
    * Class Loader Subsystem:
        * Loading, Linking, Initialization
        * Loading: JVM reads .class file -> binary, save it to method area, JVM creates a Class Object to represent this file
        * Linking: It performs verification, preparation, and resolution.
            * Verification: ensure correctness of .class file
            * Preparation: allocates memory for class varaibles
            * Resolution: replace symbolic reference 
        * Three Types:
            * Boostrap classloader: 
                java core libraries, java.lang
            * Extension classloader:
                load classes in jar files at $JAVA_HOME$/lib/ext
            * System/Application classloader:
                load classes from CLASSPATH, classes user wrote and import.
            * Custom classloader
        * Execution Order:
            * 1. System Class Loader, 2. Extension classloader, 3. Bootstrap classloader
        * How it works:
            * https://www.jianshu.com/p/1e4011617650
            * Delegation model, recursively
            * loadClass(String className) load class. If failed, requests parent class loader to find it.
            * If it still cannot find it, last child class will call findClass() to look for class in file system.
            * If not, ClassNotFoundException throws
            * child classloader has "parent" variable of parent classLoader, not simply extends from parent class.
            * children class loaders are visible to classes loaded by its parent class loaders.
            * Pros:
                -- Prevent same class being loaded twice
                -- Ensure core class will not be modified. Even if it is modified, that class will not be loaded. Safty concern.
        * Custom ClassLoader:
            * Use cases:
                * Modify existing bytecode
                * Create classes dynamically, Ex: JDBC, switch between different driver implementation
        * ClassNotFoundException vs. NoClassDefFoundError
            -- both occur when a particular class is not found at runtime.
            -- ClassNotFoundException:
                -- An exception occurs when try to load a class with Class.forName() and loadClass(), findSystemClass() methods
                   and mentioned classes not found in the classpath
                -- Type of java.lang.Exception

            -- NoClassDefFoundErr:
                -- Error occurs when a class is present at compile time, but missing at runtime.
                -- type of Java.lang.Error
                -- Thrown by Java Runtime System


Java Equality comparsion:
    * == : address comparsion, check if both objects points to the same memory location
    * equals(): content comparsion, check the values in the objects.
    * Java Pass by Reference, not by value

    * Override both equals() and hashcode, You must override hashCode() in every class that overrides equals(), If a class overrides equals, it must override hashCode

    * If two objects are equal with (equals()) method, then hashcode() will return the same
    * If the hashcode of two objects are the same, these two objects may not be the same.

    * HashMap use hashCode() method to calculate hash val


Spring Boot:
    * Unit Test:
        * @JdbcTest: can be used for a typical jdbc test when a test focuses only on jdbc-based components.
        * @JsonTest: It is used when a test focuses only on JSON serialization.
        * @RestClientTest: is used to test REST clients.
        * @WebMvcTest: used for Spring MVC tests with configuration relevant to only MVC tests.

Hibernate:
    *. What is Hibernate Framework?
        -- Hibernate provides reference implementation of Java Persistence API, that makes it a great choice as ORM tool with benefits of loose coupling. We can use Hibernate persistence API for CRUD operations. Hibernate framework provide option to map plain old java objects to traditional database tables with the use of JPA annotations as well as XML based configuration.

    -- Works with plain Java objects (POJO)
    -- Eager Loading is a design pattern in which data initialization occurs on the spot
    -- Lazy Loading is a design pattern which is used to defer initialization of an object as long as it's possible
    -- In eager loading strategy, if we load the User data, it will also load up all orders associated with it and will store it in a memory. But, when lazy loading is enabled, if we pull up a UserLazy, OrderDetail data won't be initialized and loaded into a memory until an explicit call is made to it.

    * Entity:
        * Lifecycle States:
            * Transient
                -- Transient entities exist in heap memory as normal Java Object
            * Persistent
                -- Persistent entities exist in the database, and Hibernate manages the persistence for persistent objects.
            * Detached
                -- Detached entities have a representation in the database
            * Removed
                -- Removed entities are objects that are being managed by Hibernate (persistent entities, in other words) that have been passed to the session’s remove() method.

    * Lazy Loading:
        * fetch = FetchType.Lazy
        * Only when you are defining mapping between two entities.
        * Only when I need it, I want to initialize or load its data.

    * Entity Equality:
        * Same Persistent Object must from same Hibernate sessions
    * JPA Cascade Types:
        -- Purpose: 
            -- we wanted to save the mapped entity whenever relationship owner entity got saved
        -- Types:
            * CascadeType.PERSIST:
                * save() or persist() operations cascade to related entities.
            * CascadeType.MERGE:
                * related entities are merged when the owning entity is merged.
            * CascadeType.REFRESH
                * cascade type refresh does the same thing for the refresh() operation.
            * CascadeType.REMOVE
                * removes all related entities association with this setting when the owning entity is deleted.
            * CascadeType.DETACH
                * cascade type detach detaches all related entities if a “manual detach” occurs.
            * CascadeType.ALL:
                * cascade type all is shorthand for all of the above cascade operations.

*/

@Entity
public class Email {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;
 
}

@Entity
public class Employee {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
 
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Email> emails;
    
}

/**
    Join Column: indicates that this entity is the owner of the relationship (that is: the corresponding table has a column with a foreign key to the referenced table)

    mappedBy: indicates that the entity in this side is the inverse of the relationship, and the owner resides in the "other" entity
*/
