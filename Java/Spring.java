/**
Spring Core:
    https://howtodoinjava.com/spring-core

    * IoC Container: (Inverse of Control)
        -- Concept: Container will create objects, wire them, connfigure them, and manage their life cycle from creation till destruction.
        -- Types:
            -- Two types:
                -- BeanFactory container:
                    * An interface for an advanced factory to maintain a registry of different beans and their dependencies
                    * Enable us to read bean definitions and access them using the bean factory.
                    * Use in lightweight application, data volumn and speed is significant.

                -- ApplicationContext container
                    * Adds more exterprise-specific functionality like resolve texual messages from properties file
                    * Adds ability to publish application events to interest event listeners.
                    * an interface
                    * It covers all BeanFactory container

            -- Create Bean Factory:
                -- Resource resource = new FileSystemResource("beans.xml");
                -- BeanFactory factory = new XmlBeanFactory(resource);
            
            -- Common Types of ApplicationContext:
                * FileSystemXmlApplicationContext
                * ClassPathXmlApplicationContext
                * WebXmlApplicationContext
            
            -- Create ApplicationContext:
                -- ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");

        -- Inverse of control:
            * Let Spring to manage the control of object creation and the object dependencies
            * Simplfy the appilcation development,
            * There is a decoupling of the execution of a certain task from implementation.
            * Inject the object when needed, no need to think of how this object being created.

        -- Dependency Injection:
            * create instances of objects that other objects rely on without knowing at compile time which class will be used to provide that functionality

        -- Bean creation in Spring:
            * Constructor:
            * Static factory method
            * instance factory method
        
        -- Dependency Injection in Spring:
            * Setter injection
            * Constructor Injection
            * Interface Injection

        -- Component v.s Service
            * Component of the application, no change, used locally
            * Service, used by foreign applications, remotely, webservice, message system 
        
        -- Constructor Injection v.s Setter Injection:
            * Constructor: hide fields that are immutable by simply not providing a setter, create multiple constructors with multiple parameters
            * Setter: Give each setter a namee to indicate what string is suppose to do, for multiple parameters.
    
    * Bean Scope:
        -- Six built-in spring bean scope:
            * Singleton scope
                -- Single bean object instance per spring IoC container
            * Prototype scope
                -- Opposite to singleton, it produces a new instance each and every time a bean is requested.
            * Request scope
                -- A single instance will be created and available during complete lifecycle of an HTTP request.
                -- only valid in web-aware Spring Application Context
                -- @RequestScope / @Scope("request")
            * Session scope
                -- A single instance will be created and available during complete lifecycle of an HTTP Session.
                -- Only valid in web-aware Spring ApplicationContext.
                -- @SessionScope / @Scope("session")
                -- container creates a new instance for each and every HTTP session
            * application scope
                -- A single instance will be created and available during complete lifecycle of ServletContext.
                -- Only valid in web-aware Spring ApplicationContext.
                -- @Applicationcope / @Scope("application")
                -- container creates one instance per web application runtime
            * websocket scope
                -- A single instance will be created and available during complete lifecycle of websocket.
                -- Only valid in web-aware Spring ApplicationContext.
                -- @Scope("websocket")
                -- WebSocket Protocol provides a single TCP connection for traffic in both directions
                -- Enables two-way communication between a client and a remote host that has opted-in to communication with client

    * Bean Life Cycle:
        -- Life Cycle callback methods:
            * InitializingBean and DisposableBean callback interfaces
            * Aware interfaces for specific behavior
            * Custom init() and destroy() methods in bean configuration file
            * @PostConstruct and @PreDestroy annotations

        -- Start:
            * Instantiation -> Populate Properties -> setBeanName() -> setBeanFactory() -> Pre-init BeanPostProcessors() -> initializingBean's afterPropertiesSet() -> Call Custom init-method -> BeanPostProcessors
        -- End: DisposableBean's destroy() -> call custom destroy method

    * Autowiring:
        -- Autowiring modes:
            * ByName
            * ByType
            * Constructor
        -- Using Autowired annotation
            * on properties:
                @Autowired
                private DepartmentBean departmentBean;
            * on setters:
                @Autowired
                public void setDepartmentBean(DepartmentBean departmentBean) {
                    this.departmentBean = departmentBean;
                }
            * on constructors
                @Autowired
                public EmployeeBean(DepartmentBean departmentBean)
                {
                    this.departmentBean = departmentBean;
                }
        -- @Qualifier:
            -- To resolve a specific bean using qualifier, we need to use @Qualifier annotation 
        -- required=false:
            -- make autowiring optional so that if no dependency is found

    * Annotation:
        -- Bean Annotations:
            * @Bean
            * @Component: annotate class
            * @Repository: annotated DAO classes into the DI container
            * @Service: Business Service Facade
            * @Controller
            * @Qualifier
            * @Autowired
            * @Value(): field or method/constructor parameter level
            * @Lazy: initialization of that bean will not happen until referenced by another bean or explicitly retrieved from the enclosing BeanFactory
            * @Lookup: Indicates a method as ‘lookup’ method. It is best used for injecting a prototype-scoped bean into a singleton bean.
            * @Primary: when multiple candidates are qualified to autowire a single-valued dependency.

        -- Context configuration annotations:
            * @ComponentScan: 
                -- spring can auto-scan all classes annotated with the stereotype annotations @Component, @Controller, @Service, and @Repository and configure them with BeanFactory.
            * @Configuration: 
                -- Indicates that a class declares one or more @Bean methods and can be processed by the container to generate bean definitions when used along with @ComponentScan.
            * @Profile: 
                -- Indicates that a class declares one or more @Bean methods and can be processed by the container to generate bean definitions when used along with @ComponentScan.
            * @Import: 
                -- Indicates one or more component classes to import — typically @Configuration classes
            * @ImportResource: 
                -- Indicates one or more resources containing bean definitions to import. It is used for XML bean definitions just like @Import is used for java configuration using @Bean.

    * Design Pattern in Spring Framework:
        * Proxy: 
            -- Used in AOP, and remoting
        * Singleton:
            -- beans defined in spring config files are singletons by default.
        * Template:
            -- used extensively to deal with boilerplate repeated code e.g. RestTemplate, JmsTemplate, JpaTemplate.
        * Factory pattern:
            -- BeanFactory for creating instance of an object.

Spring AOP:
    * Aspect Oriented Programming
        * Pros:
            -- Enable modularization of concerns such as transaction management, logging or security that cut across multiple types and objects.
        * Advice:
            -- The action taken by an aspect at a particular join-point
            -- Advice is associated with a pointcut expression and runs at any join point matches by the pointcuts
            * Different Types:
                -- Before advice:
                    * Advice that executes before a join point, but which does not have the ability to prevent execution flow proceeding to the join point
                -- After returning advice:
                    * Advice to be executed after a join point completes normally: for example, if a method returns without throwing an exception.
                -- After throwing advice:
                    * Advice to be executed if a method exits by throwing an exception.
                -- After advice:
                    * Advice to be executed regardless of the means by which a join point exits
                -- Around advice:
                    * Advice that surrounds a join point such as a method invocation.
        * Joinpoint:
            -- A point of execution of the program. In Spring AOP, joinpoint always represents a method execution.
        * Pointcut:
            -- Predicate or expression that matches join points
    * Spring AOP Proxy:
        -- A proxy is an object that looks like another object, but adds special functionality behind the scene.
        -- Spring AOP is proxy-based. AOP proxy is an object created by the AOP framework in order to implement the aspect contracts in runtime.
*/