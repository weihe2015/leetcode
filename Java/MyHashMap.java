package GoldmanSachs;

import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertEquals;

public class MyHashMap<K,V> {

    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    static final int DEFAULT_INITIAL_CAPACITY = 16;

    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    static class Node<K,V> {
        final int hash;
        final K key;
        V value;
        Node<K,V> next;

        public Node(int hash, K key, V value, Node<K,V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public final K getKey() { return key; }
        public final V getValue() { return value; }

        public final int hashCode() { return Objects.hashCode(key) ^ Objects.hashCode(value); }

        public final boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof Node) {
                Node n = (Node) o;
                if (Objects.equals(key, n.getKey()) &&
                        Objects.equals(value, n.getValue())) {
                    return true;
                }
            }
            return false;
        }
    }

    private Node<K,V>[] table;

    private int capacity;
    private int size;
    private int threshold;
    private float loadFactor;

    public MyHashMap() {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        this.capacity = DEFAULT_INITIAL_CAPACITY;
    }

    public V get(Object key) {
        int hash = hash(key);
        if (table == null || table.length == 0) {
            return null;
        }
        int n = table.length;
        int idx = (n-1) & hash;
        if (table[idx] == null) {
            return null;
        }
        // check on the linked list:
        Node<K,V> firstNode = table[idx];
        if (firstNode.hash == hash && Objects.equals(firstNode.key, key)) {
            return firstNode.value;
        }
        while (firstNode.next != null) {
            if (firstNode.hash == hash && Objects.equals(firstNode.key, key)) {
                return firstNode.value;
            }
            firstNode = firstNode.next;
        }
        return null;
    }

    // implement get and put:
    public void put(K key, V value) {
        int hash = hash(key);
        // table未初始化或者长度为0，进行扩容
        if (table == null || table.length == 0) {
            resize();
        }
        int n = table.length;
        int idx = (n-1) & hash;
        if (table[idx] == null) {
            table[idx] = new Node(hash, key, value, null);
        }
        else {
            Node<K,V> ptr = table[idx];
            Node<K,V> node;
            // compare with the first element in the linked list:
            if (ptr.hash == hash && (Objects.equals(key, ptr.key))) {
                node = ptr;
            }
            else {
                // iterate to the end of linkedlist and insert new node to that.
                node = ptr.next;
                boolean found = false;
                while (node.next != null) {
                    if (node.hash == hash && (Objects.equals(node.key, key))) {
                        found = true;
                        break;
                    }
                    node = node.next;
                }
                if (!found) {
                    node.next = new Node(hash, key, value, null);
                }
            }
            if (node != null) {
                node.value = value;
            }
        }
        size++;
        if (size > threshold) {
            resize();
        }
    }

    /**
     * resize the table:
     * calculate the new capacity and new threshold,
     * if it is only a node: we calculate the new index
     * */
    public void resize() {
        Node<K,V>[] oldTable = table;
        int oldCapacity = table == null ? 0 : table.length;
        int oldThreshold = threshold;
        int newCapcity = 0;
        int newThreshold = 0;

        if (oldCapacity > 0) {
            // we double the original size:
            newThreshold = oldThreshold * 2;
        }
        else {
            newCapcity = DEFAULT_INITIAL_CAPACITY;
            newThreshold = (int) (DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
        }
        threshold = newThreshold;
        table = new Node[newCapcity];

        if (oldTable == null) {
            return;
        }
        // move each item to new bucket:
        for (int i = 0; i < oldCapacity; i++) {
            Node<K,V> element = oldTable[i];
            if (element != null) {
                oldTable[i] = null;

                if (element.next == null) {
                    int idx = element.hash & (newCapcity - 1);
                    table[idx] = element;
                }
                // Linked List:
                else {
                    Node<K,V> loHead = null, loTail = null;
                    Node<K,V> hiHead = null, hiTail = null;
                    Node<K,V> next;
                    do {
                        next = element.next;
                        // original index:
                        if ((element.hash & oldCapacity) == 0) {
                            if (loTail == null) {
                                loHead = element;
                            }
                            else {
                                loTail.next = element;
                            }
                            loTail = element;
                        }
                        else {
                            if (hiTail == null) {
                                hiHead = element;
                            }
                            else {
                                hiTail.next = element;
                            }
                            hiTail = element;
                        }
                    } while ((element = next) != null);

                    // original place:
                    if (loTail != null) {
                        table[i] = loHead;
                        loTail.next = null;
                    }
                    if (hiTail != null) {
                        table[i + oldCapacity] = hiHead;
                        hiHead.next = null;
                    }
                }
            }
        }
    }

    @Test
    public void test1() {
        MyMap<String, String> map = new MyMap<>();
        // 键不能重复，值可以重复
        map.put("san", "张三");
        map.put("si", "李四");
        map.put("wu", "王五");
        map.put("wang", "老王");
        map.put("wang", "老王2");// 老王被覆盖
        map.put("lao", "老王");

        assertEquals("张三", map.get("san"));
        assertEquals("李四", map.get("si"));
        assertEquals("王五", map.get("wu"));
        assertEquals("老王2", map.get("wang"));
        assertEquals("老王", map.get("lao"));
    }
}
