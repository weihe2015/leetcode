import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This program tries to print the following program:
 * T1 -> 1
 * T2 -> 2
 * T3 -> 3
 * T1 -> 4
 * T2 -> 5
 * T3 -> 6
 * */
public class PrintSequence3 {

    private static final int MAX_NUM = 12;
    private static volatile int number = 0;

    public static void main(String[] args) {
        Object obj = new Object();
        Thread t1 = new Thread(new NumberRunnable(obj, 0), "T1");
        Thread t2 = new Thread(new NumberRunnable(obj, 1), "T2");
        Thread t3 = new Thread(new NumberRunnable(obj, 2), "T3");
        t1.start();
        t2.start();
        t3.start();
    }

    static class NumberRunnable implements Runnable {

        Object obj;
        int threadId;

        public NumberRunnable(Object obj, int threadId) {
            this.obj = obj;
            this.threadId = threadId;
        }

        @Override
        public void run() {
            synchronized (obj) {
                while (number < MAX_NUM - 2) {
                    while (number % 3 != threadId) {
                        try {
                            obj.wait();
                        }
                        catch (InterruptedException e) {

                        }
                    }
                    number++;
                    System.out.println(Thread.currentThread().getName() + " - " + number);
                    obj.notifyAll();
                }
            }
        }
    }
}
