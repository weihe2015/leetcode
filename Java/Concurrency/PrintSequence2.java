/**
 * This program tries to print the following program:
 * T1 -> 1
 * T2 -> 2
 * T3 -> 3
 * T1 -> 4
 * T2 -> 5
 * T3 -> 6
 * */
public class PrintSequence2 {

    private static Lock lock = new ReentrantLock();
    private static final int MAX_NUM = 12;
    private static volatile int number = 0;

    public static void main(String[] args) {
        Thread t1 = new Thread(new NumberRunnable(0), "T1");
        Thread t2 = new Thread(new NumberRunnable(1), "T2");
        Thread t3 = new Thread(new NumberRunnable(2), "T3");
        t1.start();
        t2.start();
        t3.start();
    }

    static class NumberRunnable implements Runnable {

        int threadId;
        public NumberRunnable(int threadId) {
            this.threadId = threadId;
        }

        @Override
        public void run() {
            while (number < MAX_NUM - 2) {
                lock.lock();
                if (number % 3 == threadId) {
                    number++;
                    System.out.println(Thread.currentThread().getName() + " - " + number);
                }
                lock.unlock();
            }
        }
    }
}