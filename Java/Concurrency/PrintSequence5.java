package Concurrency;

/**
 * Thread 1 :: 0
 * Thread 1 :: 1
 * Thread 1 :: 2
 * Thread 2 :: 0
 * Thread 2 :: 1
 * Thread 2 :: 2
 * */
public class PrintSequence5 {

    static class MathClass {
        public void print(int n) {
            // if the method is not static. ‘this’ refer to reference to current object in which synchronized method is invoked.
            synchronized (this) {
                for (int i = 0; i < n; i++) {
                    System.out.println(Thread.currentThread().getName() + " :: " +  i);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        MathClass math = new MathClass();

        new Thread(() -> {
            math.print(3);
        }, "Thread 1").start();

        new Thread(() -> {
            math.print(3);
        }, "Thread 2").start();
    }
}
