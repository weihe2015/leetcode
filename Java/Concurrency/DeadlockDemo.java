package Concurrency;

public class DeadlockDemo {

    private static Object resource1 = new Object();
    private static Object resource2 = new Object();

    public static void main(String[] args) {
        new Thread(() -> {
            synchronized (resource1) {
                System.out.println(Thread.currentThread() + " get resource 1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
                System.out.println(Thread.currentThread() + " waiting to get resource 2");
                synchronized (resource2) {
                    System.out.println(Thread.currentThread() + " get resource 2");
                }
            }
        }, "Thread1").start();

        new Thread(() -> {
            synchronized (resource2) {
                System.out.println(Thread.currentThread() + " get resource 2");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
                System.out.println(Thread.currentThread() + " waiting to get resource 1");
                synchronized (resource1) {
                    System.out.println(Thread.currentThread() + " get resource 1");
                }
            }
        }, "Thread2").start();

        /**
        It should change to:
        new Thread(() -> {
            synchronized (resource1) {
                System.out.println(Thread.currentThread() + " get resource 1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
                System.out.println(Thread.currentThread() + " waiting to get resource 2");
                synchronized (resource2) {
                    System.out.println(Thread.currentThread() + " get resource 2");
                }
            }
        }, "Thread2").start();
        */
    }
}
