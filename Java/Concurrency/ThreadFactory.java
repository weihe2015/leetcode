import org.junit.Test;

public class ThreadFactory {

    /**
     * Method 1: create a subclass of thread and overwrites run() method:
     * */
    class MyThread extends Thread {
        public void run() {
            System.out.println("Thread " + Thread.currentThread() + " is running ");
        }
    }

    @Test
    public void test() {
        MyThread thread = new MyThread();
        thread.start();
    }

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        Thread thread = new Thread("Thread1 Name") {
            @Override
            public void run() {
                System.out.println("Thread " + currentThread().getId() + " is running ");
            }
        };
        thread.start();
        /**
         * The same as Lambda
         * */
        new Thread(() -> {
            System.out.println("Thread 2 is running ");
        }, "Thread Name2").start();
    }

    /**
     * Method 2:
     *  anonymous subclass of thread:
     * */
    @Test
    public void test2() {
        System.out.println(Thread.currentThread().getName());
        Thread thread = new Thread("Thread1 Name") {
            @Override
            public void run() {
                System.out.println("Thread " + currentThread().getId() + " is running ");
            }
        };
        thread.start();
        /**
         * The same as Lambda
         * */
        new Thread(() -> {
            System.out.println("Thread 2 is running ");
        }, "Thread Name2").start();
    }

    /**
     * Runnable Interface implementation:
     * public interface Runnable() { public void run(); }
     *
     * 1. Create a Java class that implements the Runnable interface
     * 2. Create an anonnymous class that implements the Runnable interface
     * 3. Create a Java Lambda that implements the Runnable interface
     * */

    class MyRunnable implements Runnable {
        public void run() {
            System.out.println("MyRunnable running");
        }
    }

    @Test
    public void test3() {
        MyRunnable runnable = new MyRunnable();
        new Thread(runnable).start();

        Runnable myRunnable2 = new Runnable() {
            @Override
            public void run() {
                System.out.println("MyRunnable2 running");
            }
        };
        new Thread(myRunnable2).start();

        Runnable myRunnable3 = () -> {
            System.out.println("MyRunnable3 running");
        };
        new Thread(myRunnable3, "Thread Name").start();
    }
}
