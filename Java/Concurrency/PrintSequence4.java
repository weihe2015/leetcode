/**
 * T1 -> 1
 * T1 -> 2
 * T1 -> 3
 * T2 -> 4
 * T2 -> 5
 * T2 -> 6
 * T3 -> 7
 * T3 -> 8
 * T3 -> 9
 *
 * */
public class PrintSequence4 {

    static private final int MAX_NUM = 9;
    static private volatile int num = 0;
    static private volatile int numOfThread = 0;

    public static void main(String[] args) {
        Object obj = new Object();
        Thread t1 = new Thread(new NumberRunnable(obj, 0), "T1");
        Thread t2 = new Thread(new NumberRunnable(obj, 1), "T2");
        Thread t3 = new Thread(new NumberRunnable(obj, 2), "T3");
        t1.start();
        t2.start();
        t3.start();
    }

    static class NumberRunnable implements Runnable {

        Object obj;
        int threadId;

        public NumberRunnable(Object obj, int threadId) {
            this.obj = obj;
            this.threadId = threadId;
        }

        @Override
        public void run() {
            synchronized (obj) {
                while (num < MAX_NUM) {
                    while (numOfThread % 3 != threadId) {
                        try {
                            obj.wait();
                            if (num >= MAX_NUM) {
                                return;
                            }
                        } catch (InterruptedException ex) {

                        }
                    }
                    for (int i = 0; i < 3; i++) {
                        num++;
                        System.out.println(Thread.currentThread().getName() + " - " + num);
                    }
                    numOfThread++;
                    obj.notifyAll();
                }
            }
        }
    }
}
