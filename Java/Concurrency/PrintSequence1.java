/**
 * This program tries to print the following program:
 * T1 -> 1
 * T2 -> 2
 * T3 -> 3
 * T1 -> 4
 * T2 -> 5
 * T3 -> 6
 * */
public class PrintSequence {

    static private final int MAX_NUM = 10;

    public static void main(String[] args) {
        //
        Object obj = new Object();
        Thread t1 = new Thread(new NumberRunnable(obj, 0), "T1");
        Thread t2 = new Thread(new NumberRunnable(obj, 1), "T2");
        Thread t3 = new Thread(new NumberRunnable(obj, 2), "T3");

        t1.start();
        t2.start();
        t3.start();
    }

    static class NumberRunnable implements Runnable {
        Object obj;
        int threadNum;
        static int num = 0;

        NumberRunnable(Object obj, int threadNum) {
            this.obj = obj;
            this.threadNum = threadNum;
        }

        @Override
        public void run() {
            while (num < MAX_NUM) {
                synchronized (obj) {
                    if (num < MAX_NUM && num % 3 == threadNum) {
                        num++;
                        System.out.println(Thread.currentThread().getName() + " - " + num);
                    }
                }
            }
        }
    }
}
