public class Solution {
    public void runTasks() throws InterruptedException {
        // 3 permits, only allows 3 threads each time
        Semaphore semaphore = new Semaphore(3);
    }

    class Task implements Runnable {
        @Override
        public void run() {
            semaphore.acquireUninterruptibly();

            semaphore.release();
        }
    }
}