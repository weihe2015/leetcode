public class ThreadSafeSingleton {
    private static volatile ThreadSafeSingleton instance;

    /**
    * Double checked locking code on Singleton 
    * Pros: minimize the number of threads wait and only allow for first time.
    */
    public static ThreadSafeSingleton getInstance() {
        //先判断对象是否已经实例过，没有实例化过才进入加锁代码
        if (instance == null) {
            /**
            synchronized block on class ThreadSafeSingleton

            synchronized(X.class) => exactly one Thread in the block, 
                  use class instance as a monitor, static method.

            synchronized(this) => only thread per instance. non static method
            */
            synchronize (ThreadSafeSingleton.class) {
                if (instance == null) {
                    instance = new ThreadSafeSingleton();
                }
            }
        }
        return instance;
    }

    /**
        Alternative: which will always block the threads with Class Object locker:
        Cons: all threads are being blocked for the first time.
    */
    public synchronized static ThreadSafeSingleton getInstance() {
        if (instance == null) {
            instance = new ThreadSafeSingleton();
        }
        return instance;
    }
}