/**
Mockito:
    -- Test Mocks:
        -- Stubs: an object that predefined returnn values to method executions made during test
        -- Spies: are objects that are similar to stubs, but record how they were executed
        -- Mocks: are objects that have return values to method executions made during the test and has recorded expectations of these executions

    -- Mock Annotation:
        -- @Mock: used to create and inject mocked instances. We do not create real objects, rather ask mockito to create a mock for the class.
        -- @Spy: is used to create a real object and spy on that real object
        -- @Captor: used to create an ArgumentCaptor instance which is used to capture method argument values for further assertions.
        -- @InjectMocks: create the object of class to be tested and than insert it’s dependencies (mocked) to completely test the behavior
*/