/**
Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:

The number of elements initialized in nums1 and nums2 are m and n respectively.
You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2.
Example:

Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

Output: [1,2,2,3,5,6]

*/
public class Solution {
    /**
     The key to solve this problem is moving element of A and B backwards.
     If B has some elements left after A is done, also need to handle that case.
     */
    // Time Complexity: O(m + n), Space Complexity: O(1)
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if (nums1 == null || nums2 == null) {
            return;
        }
        // move the element backward to avoid creating aux array:
        int i = m-1, j = n-1, idx = m+n-1;
        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[idx--] = nums1[i--];
            }
            else {
                nums1[idx--] = nums2[j--];
            }
        }
        // handle to case that nums2.length > nums1.length:
        while (j >= 0) {
            nums1[idx--] = nums2[j--];
        }
    }

    // Merge Sort solution:
    // Time Complexity: O(m + n), Space Complexity: O(m)
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if (nums1 == null || nums2 == null) {
            return;
        }
        // copy nums1 to aux array
        int[] aux = new int[m];
        for (int i = 0; i < m; i++) {
            aux[i] = nums1[i];
        }
        int i = 0, j = 0, idx = 0;
        while (i < m && j < n) {
            if (aux[i] < nums2[j]) {
                nums1[idx++] = aux[i++];
            }
            else {
                nums1[idx++] = nums2[j++];
            }
        }
        while (i < m) {
            nums1[idx++] = aux[i++];
        }
        while (j < n) {
            nums1[idx++] = nums2[j++];
        }
    }
}