/**
Given two words (beginWord and endWord), and a dictionary's word list, find all shortest transformation sequence(s) from beginWord to endWord, such that:

Only one letter can be changed at a time
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
Note:

Return an empty list if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
Example 1:

Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output:
[
  ["hit","hot","dot","dog","cog"],
  ["hit","hot","lot","log","cog"]
]
Example 2:

Input:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]

Output: []

Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
*/
public class Solution {
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> result = new ArrayList<>();
        Set<String> wordDict = new HashSet<>(wordList);
        if (!wordDict.contains(endWord)) {
            return result;
        }
        // Key -> Word, Value: distance from beginWord to this word
        Map<String, Integer> distanceMap = new HashMap<>();
        distanceMap.put(beginWord, 0);

        // Key -> Word, Value: List of its neighbors:
        Map<String, List<String>> neighborMap = new HashMap<>();
        neighborMap.put(beginWord, new ArrayList<>());
        for (String word : wordDict) {
            neighborMap.put(word, new ArrayList<>());
        }
        // BFS: Trace every node's distance from the start node (level by level).
        bfs(beginWord, wordDict, distanceMap, neighborMap);
        // DFS: output all paths with the shortest distance.
        List<String> list = new ArrayList<>();
        dfs(beginWord, endWord, distanceMap, neighborMap, result, list);
        return result;
    }

    private void bfs(String beginWord, Set<String> wordDict, Map<String, Integer> distanceMap, Map<String, List<String>> neighborMap) {
        Queue<String> queue = new LinkedList<>();
        queue.offer(beginWord);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                String currWord = queue.poll();
                char[] cList = currWord.toCharArray();
                for (int j = 0, n = cList.length; j < n; j++) {
                    char oldChar = cList[j];
                    for (char c = 'a'; c <= 'z'; c++) {
                        if (cList[j] == c) {
                            continue;
                        }
                        cList[j] = c;
                        String newWord = String.valueOf(cList);
                        if (wordDict.contains(newWord)) {
                            neighborMap.get(currWord).add(newWord);
                            if (!distanceMap.containsKey(newWord)) {
                                int dist = distanceMap.get(currWord);
                                distanceMap.put(newWord, dist+1);
                                queue.offer(newWord);
                            }
                            // cannot remove newWord from wordDict, because we need to find all paths, not only one
                        }
                    }
                    cList[j] = oldChar;
                }
            }
        }
    }

    private void dfs(String beginWord, String endWord, Map<String, Integer> distanceMap,
                     Map<String, List<String>> neighborMap, List<List<String>> result, List<String> list) {
        list.add(beginWord);
        if (endWord.equals(beginWord)) {
            result.add(new ArrayList<>(list));
            list.remove(list.size()-1);
            return;
        }
        List<String> neighbors = neighborMap.get(beginWord);
        for (String neighbor : neighbors) {
            int neighborDist = distanceMap.get(neighbor);
            int currDist = distanceMap.get(beginWord);
            if (neighborDist == currDist + 1) {
                dfs(result, neighbor, endWord, distanceMap, neighborMap, list);
            }
        }
        list.remove(list.size()-1);
    }
}