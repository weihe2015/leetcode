/**
Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive),
prove that at least one duplicate number must exist.
Assume that there is only one duplicate number, find the duplicate one.

Note:

    You must not modify the array (assume the array is read only).
    You must use only constant, O(1) extra space.
    Your runtime complexity should be less than O(n2).
    There is only one duplicate number in the array, but it could be repeated more than once.
*/

public class Solution {
    // Navive solution, scan the array twice
    // Time complexity: O(n^2), Space complexity: O(1)
    public int findDuplicate(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        for (int i = 0, max = nums.length; i < max; i++) {
            for (int j = i+1; j < max; j++) {
                if (nums[i] == nums[j]) {
                    return nums[i];
                }
            }
        }
        return -1;
    }

    // Use hashSet, Time complexity O(n), Space Complexity O(n)
    public int findDuplicate2(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        Set<Integer> set = new HashSet<Integer>();
        for(int num : nums){
            if(!set.add(num))
                return num;
        }
        return -1;
    }

    // If we allow to modify the original array, we can sort the array, and scan it once
    // Time complexity: O(NlogN), space complexity O(1) Floyd Cycle Detection
    public int findDuplicate3(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        Arrays.sort(nums);
        for (int i = 0, max = nums.length; i < max; i++) {
            if (i > 0 && nums[i] == nums[i-1]) {
                return nums[i];
            }
        }
        return -1;
    }

    // Detect cycle algorithm. Time complexity O(n), Space Complexity O(1)
    public int findDuplicate(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int slow = nums[0];
        int fast = nums[nums[0]];
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[nums[fast]];
        }
        fast = 0;
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
        }
        return slow;
    }
}