/**
Write a function that reverses a string. 
The input string is given as an array of characters char[].

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

You may assume all the characters consist of printable ascii characters.

Example 1:

Input: ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]
Example 2:

Input: ["H","a","n","n","a","h"]
Output: ["h","a","n","n","a","H"]
*/

/*
    Note: remember only need to iterate to middle of the string, 
    otherwise it will returns the same string
*/

public class Solution {
    public void reverseString(char[] cList) {
        int i = 0;
        int j = cList.length - 1;
        while (i < j) {
            swap(cList, i, j);
            i++;
            j--;
        }
    }
    
    private void swap(char[] cList, int l, int r) {
        char tmp = cList[l];
        cList[l] = cList[r];
        cList[r] = tmp;
    }
}