/**
Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.

You have the following 3 operations permitted on a word:

Insert a character
Delete a character
Replace a character
Example 1:

Input: word1 = "horse", word2 = "ros"
Output: 3
Explanation:
horse -> rorse (replace 'h' with 'r')
rorse -> rose (remove 'r')
rose -> ros (remove 'e')
Example 2:

Input: word1 = "intention", word2 = "execution"
Output: 5
Explanation:
intention -> inention (remove 't')
inention -> enention (replace 'i' with 'e')
enention -> exention (replace 'n' with 'x')
exention -> exection (replace 'n' with 'c')
exection -> execution (insert 'u')
*/

public class Solution {
/**
For two strings:
X of length m,
Y of length n:

dp[i][j]: the minimum edit distance between X[0...i] and Y[0...j]
   i.e., the first i characters of X and the first j characters of Y
   当字符串 X 的长度为 i，字符串 Y 的长度为 j 时，将 X 转化为 Y 所使用的最少操作次数为 dp[i][j]
dp[m][n]: The edit distance between X and Y

1. Initialization:
 dp[i][0] = i
 dp[0][j] = j

2. Recurrence Relation:
 for each i = 1 ... m
    for each j = 1 ... n
       dp[i][j] = min {
                        dp[i-1][j] + 1  => Deleting 1 character from X
                        dp[i][j-1] + 1  => Inserting 1 character to Y
                        dp[i-1][j-1] + 0 if (X[i-1] == Y[j-1])
                        dp[i-1][j-1] + 1 if (X[i-1] != Y[j-1]) Substitution 1 character from X to Y to make them match
                    }
3. Result:
dp[m][n]

(一)、当word1[i]==word2[j]时,由于遍历到了i和j,说明word1的0~i-1和word2的0~j-1的匹配结果已经生成,
由于当前两个字符相同,因此无需做任何操作,dp[i][j]=dp[i-1][j-1]
(二)、当word1[i]!=word2[j]时,可以进行的操作有3个:
      ① 替换操作:可能word1的0~i-1位置与word2的0~j-1位置的字符都相同,
           只是当前位置的字符不匹配,进行替换操作后两者变得相同,
           所以此时dp[i][j]=dp[i-1][j-1]+1(这个加1代表执行替换操作)
      ②删除操作:若此时word1的0~i-1位置与word2的0~j位置已经匹配了,
         此时多出了word1的i位置字符,应把它删除掉,才能使此时word1的0~i(这个i是执行了删除操作后新的i)
         和word2的0~j位置匹配,因此此时dp[i][j]=dp[i-1][j]+1(这个加1代表执行删除操作)
      ③插入操作:若此时word1的0~i位置只是和word2的0~j-1位置匹配,
          此时只需要在原来的i位置后面插入一个和word2的j位置相同的字符使得
          此时的word1的0~i(这个i是执行了插入操作后新的i)和word2的0~j匹配得上,
          所以此时dp[i][j]=dp[i][j-1]+1(这个加1代表执行插入操作)
*/
    // http://www.stanford.edu/class/cs124/lec/med.pdf
    // https://www.youtube.com/watch?v=Xxx0b7djCrs
    // https://zhuanlan.zhihu.com/p/91582909
    // Runinng Time Complexity: O(m * n), Space Complexity: O(m * n)
    public int minDistance(String word1, String word2) {
        int m = word1.length();
        int n = word2.length();
        // dp[i][j]: the minimum edit distance between X[0...i] and Y[0...j]
        int[][] dp = new int[m+1][n+1];
        for (int i = 1; i <= m; i++) {
            dp[i][0] = dp[i-1][0] + 1;
        }
        for (int j = 1; j <= n; j++) {
            dp[0][j] = dp[0][j-1] + 1;
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (word1.charAt(i-1) == word2.charAt(j-1)) {
                    dp[i][j] = dp[i-1][j-1];
                }
                else {
                    // 如果在字符串 word1末尾插入一个与 word2[j] 相等的字符，则有 dp[i][j] = dp[i][j-1] + 1;
                    // insert a character
                    int num1 = dp[i][j-1] + 1;
                    // 如果把字符 word1[i] 删除，则有 dp[i][j] = dp[i-1][j] + 1;
                    // delete a character
                    int num2 = dp[i-1][j] + 1;
                    // 如果把字符 word1[i] 替换成与 word2[j] 相等，则有 dp[i][j] = dp[i-1][j-1] + 1;
                    // replace a character
                    int num3 = dp[i-1][j-1] + 1;
                    dp[i][j] = minOfThreeValues(num1, num2, num3);
                }
            }
        }
        return dp[m][n];
    }

    private int minOfThreeValues(int num1, int num2, int num3) {
        int minVal = Math.min(num1, num2);
        return Math.min(minVal, num3);
    }

    // Solution 2:
    // Running Time Complexity: O(m * n),
    // Space Complexity: O(n)
    public int minDistance(String word1, String word2) {
        int m = word1.length();
        int n = word2.length();

        int[] dp = new int[n+1];
        for (int j = 1; j <= n; j++) {
            dp[j] = dp[j-1] + 1;
        }

        for (int i = 1; i <= m; i++) {
            // prev 相当于之前的 dp[i-1][j-1]
            int prev = dp[0];
            dp[0] = i;
            for (int j = 1; j <= n; j++) {
                int temp = dp[j];
                if (word1.charAt(i-1) == word2.charAt(j-1)) {
                    dp[j] = prev;
                }
                else {
                    // insert a character
                    int num1 = dp[j-1] + 1;
                    // delete a character
                    int num2 = dp[j] + 1;
                    // replace a character
                    int num3 = prev + 1;
                    dp[j] = minOfThreeValues(num1, num2, num3);
                }
                prev = temp;
            }
        }
        return dp[n];
    }
}