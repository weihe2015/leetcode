/*
Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.

For example,
If n = 4 and k = 2, a solution is:

[[2,4],[3,4],[2,3],[1,2],[1,3],[1,4]]
*/
public class Solution {
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        combine(result, new ArrayList<Integer>(), 1, n, k);
        return result;
    }
    
    public void combine(List<List<Integer>> result, List<Integer> list, int start, int n, int k) {
      // determine end point
        if(k == 0) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for(int i = start; i <= n; i++) {
            list.add(i);
            combine(result,list,i+1,n,k-1);
            list.remove(list.size()-1);
        }
    }

    // 11-16-17:
    public List<List<Integer>> combine2(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        if (n < 1 || k < 1) {
            return result;
        }
        generateCombination(result, new ArrayList<Integer>(), n, k, 1);
        return result;
    }
    
    private void generateCombination(List<List<Integer>> result, ArrayList<Integer> list, int n, int k, int level) {
        if (list.size() == k) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = level; i <= n; ++i) {
            list.add(i);
            generateCombination(result, list, n, k, i+1);
            list.remove(list.size()-1);
        }
    }
}