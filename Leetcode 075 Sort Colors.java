/*
Given an array with n objects colored red, white or blue,
sort them so that objects of the same color are adjacent,
with the colors in the order red, white and blue.

Here, we will use the integers 0, 1, and 2
to represent the color red, white, and blue respectively.
*/
public class Solution {

    // counter sort:
    public void sortColors(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int[] counts = new int[3];
        for (int num : nums) {
            counts[num]++;
        }
        int idx = 0;
        for (int i = 0, n = counts.length; i < n; i++) {
            int count = counts[i];
            for (int j = 0; j < count; j++) {
                nums[idx++] = i;
            }
        }
    }

    // two pointers solution
    public void sortColors(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int i = 0, zero = 0, n = nums.length, two = n-1;
        while (i <= two) {
            while (nums[i] == 2 && i < two) {
                swap(nums, i, two--);
            }
            while (nums[i] == 0 && i > zero) {
                swap(nums, i, zero++);
            }
            i++;
        }
    }
    private void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // merge sort
    public void sortColors3(int[] nums) {
        int n = nums.length;
        int[] aux = new int[n];
        mergesort(nums,aux,0,n-1);
    }
    public void mergesort(int[] nums, int[] aux, int lo, int hi){
        if (lo >= hi)
            return;
        int mid = lo + (hi-lo)/2;
        mergesort(nums,aux,lo,mid);
        mergesort(nums,aux,mid+1,hi);
        merge(nums,aux,lo,mid,hi);
    }

    public void merge(int[] nums, int[] aux, int lo, int mid, int hi){
        for(int k = lo; k <= hi; k++){
            aux[k] = nums[k];
        }
        int i = lo, j = mid+1;
        for(int k = lo; k <= hi; k++){
            if(i > mid)
                nums[k] = aux[j++];
            else if(j > hi)
                nums[k] = aux[i++];
            else if(aux[i] < aux[j])
                nums[k] = aux[i++];
            else
                nums[k] = aux[j++];
        }
    }

    // quick sort
    public void sortColors4(int[] nums) {
         int n = nums.length;
         quicksort(nums,0,n-1);
    }

    public void quicksort(int[] nums, int lo, int hi) {
        if (lo >= hi) {
            return;
        }
        int m = partition(nums, lo, hi);
        quicksort(nums, lo, m-1);
        quicksort(nums, m+1, hi);
    }

    public int partition(int[] nums, int lo, int hi) {
        int i = lo+1, j = hi;
        while (true) {
            while (i < hi && nums[i] < nums[lo])
                i++;
            while (j > lo && nums[j] > nums[lo])
                j--;
            if (i >= j) {
                break;
            }
            swap(nums, i, j);
        }
        swap(nums, lo, j);
        return j;
    }

    // Solution 4:
    // with minimal swaps.
    // O(N) time, and O(1) Space. one pass.
    public void sortColors(int[] nums) {
        // any idx before begin is 0
        int begin = 0;
        // any idx after end is 2
        int end = nums.length - 1;
        int curr = 0;
        while (curr <= end) {
            if (nums[curr] == 0) {
                if (begin != curr) {
                    swap(nums, begin, curr);
                }
                begin++;
                curr++;
            }
            else if (nums[curr] == 1) {
                curr++;
            }
            else if (nums[curr] == 2) {
                if (end != curr) {
                    swap(nums, end, curr);
                }
                end--;
            }
        }
    }

    // Solution 5
    // O(N) time, and O(1) Space. two pass.
    public void sortColors(int[] nums) {
        // two pass:
        // count num of 0,1,2:
        int[] cntArr = new int[3];
        for (int num : nums) {
            cntArr[num]++;
        }
        int i = 0;
        int j = cntArr[0];
        int k = cntArr[0] + cntArr[1];
        int n = nums.length;
        while (i < cntArr[0] || j < cntArr[0] + cntArr[1] || k < n) {
            while (i < cntArr[0] && nums[i] == 0) {
                i++;
            }
            int num1 = -1;
            if (i < cntArr[0]) {
                num1 = nums[i];
            }

            while (j < cntArr[0] + cntArr[1] && nums[j] == 1) {
                j++;
            }
            int num2 = -1;
            if (j < cntArr[0] + cntArr[1]) {
                num2 = nums[j];
            }

            while (k < n && nums[k] == 2) {
                k++;
            }
            int num3 = -1;
            if (k < n) {
                num3 = nums[k];
            }
            if (num1 != -1 && num2 != -1 && num3 != -1) {
                if ((num1 == 1 && num2 != 1) || (num2 == 0 && num1 != 0)) {
                    swap(nums, i, j);
                }
                else if ((num1 == 2 && num3 != 2) || (num3 == 0 && num1 != 0)) {
                    swap(nums, i, k);
                }
                else if ((num2 == 2 && num3 != 2) || (num3 == 1 && num2 != 1)) {
                    swap(nums, j, k);
                }
            }
            else if (num1 != -1 && num2 != -1) {
                if ((num1 == 1 && num2 != 1) || (num1 != 0 && num2 == 0)) {
                    swap(nums, i, j);
                }
            }
            else if (num1 != -1 && num3 != -1) {
                if ((num1 == 2 && num3 != 2) || (num1 != 0 && num3 == 0)) {
                    swap(nums, i, k);
                }
            }
            else if (num2 != -1 && num3 != -1) {
                if ((num2 == 2 && num3 != 2) || (num2 != 1 && num3 == 1)) {
                    swap(nums, j, k);
                }
            }
        }
    }

}