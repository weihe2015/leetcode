/**
This is a follow up of Shortest Word Distance. The only difference is now word1 could be the same as word2.

Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.

word1 and word2 may be the same and they represent two individual words in the list.

For example,
Assume that words = ["practice", "makes", "perfect", "coding", "makes"].

Given word1 = “makes”, word2 = “coding”, return 1.
Given word1 = "makes", word2 = "makes", return 3.

Note:
You may assume word1 and word2 are both in the list.
*/
public class Solution {
    // Running Time Complexity: O(N)
    public int shortestWordDistance(String[] words, String word1, String word2) {
        if (word1.equals(word2)) {
            return shortestWordDistanceWithSameWord(words, word1);
        }
        else {
            return shortestWordDistanceWithDiffWords(words, word1, word2);
        }
    }

    public int shortestWordDistanceWithSameWord(String[] words, String word1) {
        int idx1 = -1, idx2 = -1, minDis = words.length;
        for (int i = 0, max = words.length; i < max; i++) {
            String word = words[i];
            if (!word.equals(word1)) {
                continue;
            }
            if (idx1 == -1) {
                idx1 = i;
            }
            else {
                idx2 = i;
            }
            if (idx1 != -1 && idx2 != -1) {
                minDis = Math.min(minDis, Math.abs(idx1))
            }
        }
        return minDis;
    }

    public int shortestWordDistanceWithDiffWords(String[] words, String word1, String word2) {
        int idx1 = -1, idx2 = -1, dist = words.length;
        for (int i = 0, n = words.length; i < n; i++) {
            if (word1.equals(words[i])) {
                idx1 = i;
            }
            else if (word2.equals(words[i])) {
                idx2 = i;
            }
            if (idx1 != -1 && idx2 != -1) {
                dist = Math.min(dist, Math.abs(idx1 - idx2));
            }
        }
        return dist;
    }
}