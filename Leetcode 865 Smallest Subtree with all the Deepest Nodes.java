/**
Given the root of a binary tree, the depth of each node is the shortest distance to the root.

Return the smallest subtree such that it contains all the deepest nodes in the original tree.

A node is called the deepest if it has the largest depth possible among any node in the entire tree.

The subtree of a node is tree consisting of that node, plus the set of all descendants of that node.

Example 1:


Input: root = [3,5,1,6,2,0,8,null,null,7,4]
Output: [2,7,4]
Explanation: We return the node with value 2, colored in yellow in the diagram.
The nodes coloured in blue are the deepest nodes of the tree.
Notice that nodes 5, 3 and 2 contain the deepest nodes in the tree but node 2 is the smallest subtree among them, so we return it.

Example 2:
Input: root = [1]
Output: [1]
Explanation: The root is the deepest node in the tree.

Example 3:
Input: root = [0,1,3,null,2]
Output: [2]
Explanation: The deepest node in the tree is 2, the valid subtrees are the subtrees of nodes 2, 1 and 0 but the subtree of node 2 is the smallest.
 
Note: This question is the same as 1123: https://leetcode.com/problems/lowest-common-ancestor-of-deepest-leaves/

Constraints:

The number of nodes in the tree will be in the range [1, 500].
0 <= Node.val <= 500
The values of the nodes in the tree are unique.
*/
public class Solution {
    /**
    如果左右子树高度相等，则当前结点为要找的结点。否则要找的结点在高度较大的子树中。自底向上的计算高度并返回寻找到的结点。
    */
    public TreeNode subtreeWithAllDeepest(TreeNode root) {
        Object[] res = find(root, 0);
        return (TreeNode) res[0];
    }
    
    private Object[] find(TreeNode node, int depth) {
        if (node == null) {
            return new Object[]{node, depth};
        }
        Object[] left = find(node.left, depth+1);
        Object[] right = find(node.right, depth+1);
        
        int leftDepth = (int) left[1];
        int rightDepth = (int) right[1];
        if (leftDepth == rightDepth) {
            return new Object[]{node, leftDepth};
        }
        else if (leftDepth > rightDepth) {
            return left;
        }
        else {
            return right;
        }
    }

    /**
    如果当前节点是最深叶子节点的最近公共祖先，那么它的左右子树的高度一定是相等的，
    否则高度低的那个子树的叶子节点深度一定比另一个子树的叶子节点的深度小，
    因此不满足条件。所以只需要dfs遍历找到左右子树高度相等的根节点即出答案。
    */
    public TreeNode subtreeWithAllDeepest(TreeNode root) {
        if (root == null) {
            return null;
        }
        int leftDepth = depth(root.left);
        int rightDepth = depth(root.right);
        
        if (leftDepth == rightDepth) {
            return root;
        }
        else if (leftDepth > rightDepth) {
            return lcaDeepestLeaves(root.left);
        }
        else {
            return lcaDeepestLeaves(root.right);
        }
    }
    
    private int depth(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int leftDepth = depth(node.left);
        int rightDepth = depth(node.right);
        return Math.max(leftDepth, rightDepth) + 1;
    }
}