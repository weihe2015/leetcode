/**
Given an array of n integers nums and a target, find the number of index triplets i, j, k with 0 <= i < j < k < n that satisfy the condition nums[i] + nums[j] + nums[k] < target.

Example
Given nums = [-2,0,1,3], target = 2, return 2.

Explanation:
Because there are two triplets which sums are less than 2:
[-2, 0, 1]
[-2, 0, 3]
*/
public class Solution {
    // Brute For Solution
    // Running Time Complexity: O(N^3)
    public int threeSumSmaller(int[] nums, int target) {
        if (nums == null || nums.length < 3) {
            return 0;
        }
        int count = 0;
        for (int i = 0, n = nums.length; i < n-2; i++) {
            for (int j = i+1; j < n-1; j++) {
                for (int k = j+1; k < n; k++) {
                    int sum = nums[i] + nums[j] + nums[k];
                    if (sum < target) {
                      count++;  
                    }
                }
            }
        }
        return count;
    }
    // Running time: O(n^2) Space Complexity O(1)
    public int threeSumSmaller(int[] nums, int target) {
        if (nums == null || nums.length < 3) {
            return 0;
        }
        Arrays.sort(nums);
        int count = 0;
        for (int i = 0, n = nums.length; i < n-2; i++) {
            int low = i+1, high = n-1;
            while (low < high) {
                int sum = nums[i] + nums[low] + nums[high];
                if (sum >= target) {
                    high--;
                }
                // i 和 left 不动，介于 left 和 right (包括 right) 之间的所有元素 sum 也一定小于 target 的单调性。
                else {
                    count += high - low;
                    low++;
                }
            }
        }
        return count;
    }
}