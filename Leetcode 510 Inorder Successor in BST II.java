/**
Given a binary search tree and a node in it, find the in-order successor of that node in the BST.

The successor of a node p is the node with the smallest key greater than p.val.

You will have direct access to the node but not to the root of the tree. Each node will have a reference to its parent node.

Example 1:
Input: 
root = {"$id":"1","left":{"$id":"2","left":null,"parent":{"$ref":"1"},"right":null,"val":1},"parent":null,"right":{"$id":"3","left":null,"parent":{"$ref":"1"},"right":null,"val":3},"val":2}
   2
1     3

p = 1
Output: 2
Explanation: 1's in-order successor node is 2. Note that both p and the return value is of Node type.

Example 2:
Input: 
root = {"$id":"1","left":{"$id":"2","left":{"$id":"3","left":{"$id":"4","left":null,"parent":{"$ref":"3"},"right":null,"val":1},"parent":{"$ref":"2"},"right":null,"val":2},"parent":{"$ref":"1"},"right":{"$id":"5","left":null,"parent":{"$ref":"2"},"right":null,"val":4},"val":3},"parent":null,"right":{"$id":"6","left":null,"parent":{"$ref":"1"},"right":null,"val":6},"val":5}

            5
         3     6
       2   4
     1  
p = 6
Output: null
Explanation: There is no in-order successor of the current node, so the answer is

Example 3:
Input: 
root = {"$id":"1","left":{"$id":"2","left":{"$id":"3","left":{"$id":"4","left":null,"parent":{"$ref":"3"},"right":null,"val":2},"parent":{"$ref":"2"},"right":{"$id":"5","left":null,"parent":{"$ref":"3"},"right":null,"val":4},"val":3},"parent":{"$ref":"1"},"right":{"$id":"6","left":null,"parent":{"$ref":"2"},"right":{"$id":"7","left":{"$id":"8","left":null,"parent":{"$ref":"7"},"right":null,"val":9},"parent":{"$ref":"6"},"right":null,"val":13},"val":7},"val":6},"parent":null,"right":{"$id":"9","left":{"$id":"10","left":null,"parent":{"$ref":"9"},"right":null,"val":17},"parent":{"$ref":"1"},"right":{"$id":"11","left":null,"parent":{"$ref":"9"},"right":null,"val":20},"val":18},"val":15}

p = 15
Output: 17

               15
        6              18
   3        7       17    20
2    4        13
            9


*/
class Node {
    int val;
    Node left;
    Node right;
    Node parent;
    public Node(int val) {
        this.val = val;
    }
}

public class Solution {
    public Node inorderSuccessor(Node node) {
        if (node == null) {
            return null;
        }
        // 当右子结点存在时，我们需要找到右子结点的最左子结点.
        if (node.right != null) {
            Node curr = node.right;
            while (curr != null && curr.left != null) {
                curr = curr.left;
            }
            return curr;
        } 
        // case 2: 当右子结点不存在，我们就要找到第一个比其值大的祖先结点，也是用个 while 循环去找即可
        /**
        当 node 是其 parent 的左子结点时，我们知道此时 parent 的结点值一定大于 node，因为这是二叉搜索树的性质。若 node 是其 parent 的右子结点时，则将 node 赋值为其 parent，继续向上找，直到其 parent 结点不存在了，此时说明不存在大于 node 值的祖先结点，这说明 node 是 BST 的最后一个结点了，没有后继结点，直接返回 null 即可
        */
        Node curr = node.parent;
        while (curr != null) {
            if (curr.parent == null) {
                return null;
            }
            if (curr == curr.parent.left) {
                return curr.parent;
            }
            curr = curr.parent;
        }
        return null;
    }
}