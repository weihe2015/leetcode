/**
Given a string s, find the longest palindromic subsequence's length in s. You may assume that the maximum length of s is 1000.

Example 1:
Input:

"bbbab"
Output:
4
One possible longest palindromic subsequence is "bbbb".
Example 2:
Input:

"cbbd"
Output:
2
One possible longest palindromic subsequence is "bb".
*/
public class Solution {
    /**
    dp[i][j]: length of longest palindrome subsequence in substring(i,j)
    dp[i][j]: if s.charAt(i) == s.charAt(j)
                 dp[i][j] = dp[i+1][j-1] + 2
              else
                 dp[i][j] = Math.max(dp[i+1][j], dp[i][j-1])
    */
    // Running Time Complexity: O(m^2), Space Complexity: O(m^2)
    public int longestPalindromeSubseq(String s) {
        int m = s.length();
        int[][] dp = new int[m][m];
        // Longest Palindrome Subsequence for 1 character will be 1:
        for (int i = 0; i < m; i++) {
            dp[i][i] = 1;
        }
        // we can't start the outer loop from 0, because dp[i][j] = dp[i+1][j-1] + 2, 
        // the new dp[i][j] use the information from dp[i+1][j-1] which is 0 if the outer loop starts from 0.
        for (int i = m-1; i >= 0; i--) {
            for (int j = i+1; j < m; j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    dp[i][j] = dp[i+1][j-1] + 2; 
                }
                else {
                    dp[i][j] = Math.max(dp[i+1][j], dp[i][j-1]);
                }
            }
        }
        return dp[0][m-1];
    }

    // Running Time Complexity: O(m^2), Space Complexity: O(m)
    public int longestPalindromeSubseq(String s) {
        int m = s.length(), res = -1;
        int[] dp = new int[m];
        Arrays.fill(dp, 1);
        for (int i = 0; i < m; i++) {
            int currLen = 0;
            for (int j = i-1; j >= 0; j--) {
                int prevLen = dp[j];
                if (s.charAt(i) == s.charAt(j)) {
                    dp[j] = currLen + 2;
                }
                currLen = Math.max(currLen, prevLen);
                if (i == m-1) {
                    res = Math.max(res, dp[j]);
                }
            }
        }
        return res == -1 ? 1 : res;
    }
}