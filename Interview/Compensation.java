seattle, 130-160
SF, 160-190
NYC: 140-170

https://www.youtube.com/watch?v=1rAWA6CsAGU
https://candor.co/guides/salary-negotiation

3. 谈判

#1. Understand the incentives.

在工资谈判阶段，我个人的体会是：网上有相当多的misinformation，比如认为谈判会造成offer被撤回，比如要对recruiter态度强硬，等等。当然，每个人每个公司的情况都有所不同，但我当时的策略是：寻找权威的信息来源，不过多得把决策依赖于某个论坛上路人甲的偶然经历。理解各种论坛上受关注最多的帖子常常是特别sensational/unusual的，不一定具有代表性。

在此推荐一个对我非常有帮助的视频，是对一位Google/MSFT recruiter的采访：Google Recruiter Tips On Offer Negotiation, Interviews, And More（by Clément Mihailescu, on YouTube）基于我和老公在offer谈判阶段和大厂recruiter接触的经验（样本量n=7），Amy在这个采访里的回答非常准确、权威、具有代表性。下面的一些内容是基于我/老公个人的经历，以及Amy提供的信息。

首先，介绍一下工资谈判过程中有哪些stakeholder，他们的激励结构（incentive structure）是什么。

1. Candidate

2. Recruiter
几乎没有大厂的recruiter会因为给出更低的工资接受奖励，大部分的recruiter根据他们能够成功的雇佣到多少candidate而接受奖励，有一些recruiter还会参考candidate的工资拿一定的提成。所以，没有recruiter主观上希望给candidate很低的工资（他们的提成可能减少/candidate来的概率会降低）。

3. Compensation team
一般candidate不会直接和compensation team接触。他们负责确定对于每一个candidate来说，什么样的工资对于公司来说是可以接受的。

4. Hiring manager
如果到了offer阶段，HM的唯一动机就是想要雇佣candidate。HM一般不怎么参与谈判的过程。多数情况下，他们也没有权利决定工资高低，当然有很多例外，有时候HM可能可以给candidate提供更多的支持，帮candidate争取更高的工资。有时候特别高的package可能需要更多的和级别更高的approval。

所以，谈判不是在candidate和recruiter之间发生的，而是在recruiter（代表candidate）和comp team（代表公司）之间发生的。理解这一点对于navigate谈判过程至关重要。

*Disclaimer：我/老公接触过的大厂运行模式都和上述一致，但中小公司可能有所不同。我提供的信息是否可靠/适用，请大家自行判断。

#2. Be transparent.

理解上述的激励结构之后，我觉得我和recruiter接触有了新的视角和态度。recruiter在某种意义上是candidate的代理人，而不是谈判的另一方。所以在谈判的过程中，我的策略是：友好，迅速并且全面地分享（大部分的）信息。

Amy在视频中深入讨论了哪些信息有效，哪些信息无关。一般来说，个人情况是无关信息（我要赚钱养家/买房/付贷款/etc），别人拿多少钱是无关信息（glassdoor/levels.fyi/我的同学/朋友甲）因为每个candidate的能力/面试表现不同。competing offer是最重要的有效信息，面试表现和team fit也是比较有用的。

在这里分享一下我和G谈判的经历。在G的recruiter最开始和我沟通，告诉我我过了Hiring Committee之后，我就告诉了他我当时in progress的所有面试/offer情况。比如，我当时拿到了M的offer（在西雅图）但是因为老公在湾区工作，我很诚恳地告诉了G的recruiter我因为想留在湾区，并不是很想去M（但仍然在争取remote），而G是我的top choice之一。之后形势更明朗之后，我及时地和recruiter分享了competing offer。我的recruiter完全没有任何意图在得知这些信息之后降低我的compensation，反而，我觉得recruiter感到

（1）我对这个职位很感兴趣，是非常有可能接offer的，所以值得他花时间帮我谈判；
（2）我很主动且透明地分享了自己其他的offer信息，不是空口无凭地谈判，而且有credible viable outside options，有必要让他努力争取；
（3）我很信任&感谢他对我提供的帮助和支持。

Recruiter对我也很透明，他告诉我

（1）他不打算告诉comp team我的competing offer是什么，而会让comp先出一个数，如果低于M他才会提供competing offer的信息；（很多人担心很早分享这些信息会影响初始offer，但其实好的recruiter会站在candidate的角度谈判，并且决定哪些信息应该/不应该reveal给comp）；
（2）他介绍了一下之后的流程是什么样的：comp会先出一个PhD New Grad的标准package，会有不少可以谈的空间；如果有XXX公司的competing offer，G会match + bid（略超过competing offer）；如果有XXX公司的competing offer，G只会match（等于competing offer）。他帮助我理解了哪些其他的competing offer对于辅助G的谈判最有效；
（3）他帮助我分析了一下，我的其他offer在所有L4中大致是什么位置，还有多少上升的空间。在这点上，glassdoor和levels.fyi提供的薪酬信息是非常不准确的（blind上好一些）。

caveat：我的谈判过程很顺利，部分原因是有一个offer明显高于其他，所以主要都在match offer。我没有无compete谈判的经验，在此就不误人子弟了。还有一种tricky的情况，就是所有的offer都薪资持平。我谈判时发现，competing offer的薪资并不是谈判唯一的lever。在决定去X公司还是Y公司时，我很诚恳地告诉了recruiter所有我在考虑的因素，以及我对于这两个公司的评价。即便当时在薪资上X > Y，而且我在两边都分享了对方offer的信息，X公司的recruiter还是主动地提出了可以继续加薪资。更多地让recruiter参与决策过程，他们会更愿意/更有激励提升offer，因为他们更能看到自己的effort的直接效果。

我有一个小伙伴在谈判时处理过若干offer薪资相似的情况，并且写了非常详细的帖子分享。她使用了一个negotiation service，和我的approach有诸多差别，大家自行判断什么样的策略对自己更适用吧。
My Journey from Econ PhD to Tech — Part 3: Negotiations part 1
My Journey from Econ PhD to Tech — Part 4: Negotiations part 2 + Decision
(By Scarlet Chen on Medium)

#3. Ask Nicely.

我很幸运，遇到的几个recruiter都非常支持我，并且让我感到：They will act in good faith and on my behalf。整个过程中，我们的interaction非常高效&友好。我个人感觉，谈判期间的confrontation，不仅没有必要，而且不利于达成最终的目标。

比如，永远，永远，永远不要“强硬地谈判”：
I wouldn’t be able to take the offer if ...
I will take an offer from another company if ...
I don’t think this offer is attractive if …
因为这可能会被理解为rejection。

再比如，不要过多地讨论对offer的不满，不管是薪资还是别的方面。但是可以说，这个offer对我来说如此attractive（列举理由），但是某个competing offer也特别好（列举更多理由），我好难决定呀！etc。recruiter最不想理睬的candidate就是没有实际意愿接offer，只想要leverage的人；他们不想花很多时间帮助一个candidate，但最后没有任何收获。所以对recruiter表现出对职位本身的兴趣，对工作和team的兴趣，以及热情是非常重要的（当然，不要轻易承诺一定会接offer）。

再比如，如果要initiate谈判，不要说：这个offer让我很失望。XXX公司包裹更大，你们不match我就不来了。可以说：我希望这个offer在薪资上有所提升，让我给recruiter提供更多的信息/证据，以证明我的能力和在市场上的价值，并且给他们和comp team的谈判提供更多的数据支持。

总之，candidate和recruiter是合作关系，在沟通中体现出这一点，可以让谈判的过程容易许多。

写了这么多，经历过求职过程的小伙伴可能觉得这些都很obvious，但我是在开始求职之后才了解这些信息的，希望对大家，尤其是刚毕业的小伙伴有所帮助。