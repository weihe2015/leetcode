/**
Gravity:
F is the box, # is the wall, and . is the empty space.
F can move down until it hits the wall.
Return the matrix after all F have reach the end due to gravity.

input:
# # # # #
# # F # #
# F . . #
# F . . #
# . . # #

output:
# # # # #
# # F # #
# . . . #
# F . . #
# F F # #

*/
public class Solution {
    static private final String F = "F";
    static private final String DOT = ".";
    static private final String POUND = "#";

    // If the block is not connected.
    public String[][] moveGravityGrid(String[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        for (int j = 0; j < n; j++) {
            List<int[]> fList = new ArrayList<>();
            for (int i = 0; i < m; i++) {
                if (grid[i][j].equals(F)) {
                    fList.add(new int[]{i, j});
                    grid[i][j] = DOT;
                }
            }
            if (fList.isEmpty()) {
                continue;
            }
            int[] lastF = fList.get(fList.size() - 1);
            while (true) {
                boolean isMovable = canMoveRow(lastF, grid, m);
                if (!isMovable) {
                    break;
                }
                moveBlock(fList);
            }
            setGrid(fList, grid);
        }
        return grid;
    }

    private boolean canMoveRow(int[] lastF, String[][] grid, int m) {
        int i = lastF[0];
        int y = lastF[1];
        int x = i+1;
        if (x == m || grid[x][y].equals(POUND)) {
            return false;
        }
        return true;
    }


    /**
    If the F boxes are viewed as the connected component.
    */
    public String[][] gravityGrid(String[][] grid) {
        List<int[]> fList = new ArrayList<>();
        int m = grid.length;
        int n = grid[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j].equals(F)) {
                    fList.add(new int[]{i, j});
                    grid[i][j] = DOT;
                }
            }
        }
        while (true) {
            boolean isMovable = canMoveBlock(fList, grid, m);
            if (!isMovable) {
                break;
            }
            moveBlock(fList);
        }
        setGrid(fList, grid);
        return grid;
    }

    private void setGrid(List<int[]> fList, String[][] grid) {
        for (int[] fPos : fList) {
            int i = fPos[0];
            int j = fPos[1];
            grid[i][j] = F;
        }
    }

    private boolean canMoveBlock(List<int[]> fList, String[][] grid, int m) {
        for (int[] fPos : fList) {
            int i = fPos[0];
            int y = fPos[1];
            int x = i+1;
            if (x == m || (x < m && grid[x][y].equals(POUND))) {
                return false;
            }
        }
        return true;
    }

    private void moveBlock(List<int[]> fList) {
        for (int[] fPos : fList) {
            fPos[0]++;
        }
    }
}