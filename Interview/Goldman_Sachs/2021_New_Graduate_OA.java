/**
Leetcode 724: Good
1. Balanced Array:
Given an array of numbers, find the index of the smalles array element (the pivot) for which the sums of all elements to the left and to the right are equal. The array may not be reordered

Example:
arr=[1,2,3,4,6]

* The sum of the first three elements, 1+2+3=6, the value of the last element is 6
* Using zero based indexing, arr[3]=4 is the pivot between two subarrays
* the index of the pivot is 3

Function Description:
Complete the function balancedSum in the editor below
    int arr[n]: an array of integers
returns:
    int: an integer representing the index of the pivot

Constraints: 3 <= n <= 10^6
             1 <= arr[i] <= 2 * 10^4, where 0 <= i < n
             it is guaranteed that a solution always exists.
*/

/**
// Leetcode 532L k-diff pairs in an array
Counting Pairs:

Given an integer k and a list of integers, count the number of distinct valid pairs of integers (a,b) in the list for which a + k = b. Two pairs of integers (a,b) and (c,d) are considered distinct if at least one element of (a,b) does not also belong to (c,d)

Example
n = 4
nums=[1,1,1,2]
k = 1
this array has two different valid pairs (1,1) and (1,2). For k = 1, there is only 1 valid pair which satisfies a+k=b; the pair (a,b) = (1,2)

Function Description:
Complete the function countPairs in the edit below:

countPairs has the following parameters
int numbers[n]: array of integers
int k: target difference

returns:
    int: number of valid (a,b) pairs in the number array that have a difference of k

Constraints:
    * 2 <= n <= 2 * 10^5
    * 0 <= nums[i] <-= 10^9, where 0 <= i < n
    * 0 <= k <= 10^9

Leetcode coin change:

Leetcode 332: Reconstruct Itinerary:

** Leetcode 443 String Compression:

** Leetcode 532 K-diff pairs in an array

** Leetcode 547: Friend Circles: input is int[] arr

** Leetcode 647: Palindromic substrings: remove duplicate

** Leetcode 724: Find Pivot Index

** Leetcode 780: Reaching Points

Leetcode 870: Advantage Shuffle

Leetcode 1010: Pairs of sons with total duration divisible by 60

Leetcode 1041: Robot Bounded in circle

Leetcode 1051: Height check

Leetcode 1163: last sustring in lexicographical order

Condensed linked list:

https://www.bookstack.cn/read/algorithm-exercise/linked_list-remove_duplicates_from_an_unsorted_linked_list.md
*/

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode curr = head;
        Set<Integer> set = new HashSet<Integer>();
        set.put(curr.val);
        while (curr.next != null) {
            if (hash.contains(curr.next.val)) {
                curr.next = curr.next.next;
            } else {
                hash.put(curr.next.val);
                curr = curr.next;
            }
        }
        return head;
    }