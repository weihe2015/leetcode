/*
** 1    Two Sum
** 3    Longest Substring Without Repeating Characters (Done)
** 4    Median of Two Sorted Arrays (电面考过了)
8    String to Integer (atoi)
** 9    Palindrome Number
** 11    Container With Most Water
13    Roman to Integer
** 15    3Sum
** 16    3Sum Closest
** 19    Remove Nth Node From End of List
** 20    Valid Parentheses
** 33    Search in Rotated Sorted Array
** 39    Combination Sum    
42    Trapping Rain Water
      https://www.1point3acres.com/bbs/thread-654002-1-1.html
44    Wildcard Matching
** 45    Jump Game II
** 46    Permutations    
** 49    Group Anagrams
** 50    Pow(x, n)
      https://www.1point3acres.com/bbs/thread-689263-1-1.html

** 53    Maximum Subarray
** 54    Spiral Matrix
** 56    Merge Intervals
** 62    Unique Paths
** 63    Unique Paths II
** 64    Minimum Path Sum
** 70    Climbing Stairs
** 74    Search a 2D Matrix
** 76    Minimum Window Substring
** 78    Subsets
88    Merge Sorted Array
** 91    Decode Ways
** 98    Validate Binary Search Tree
** 99    Recover Binary Search Tree
** 104    Maximum Depth of Binary Tree
** 118    Pascal's Triangle
** 119    Pascal's Triangle II
** 121    Best Time to Buy and Sell Stock
** 122    Best Time to Buy and Sell Stock II
** 141    Linked List Cycle
** 146    LRU Cache
** 153    Find Minimum in Rotated Sorted Array
** 155    Min Stack
** 160    Intersection of Two Linked Lists
** 166    Fraction to Recurring Decimal
** 167    Two Sum II - Input array is sorted
** 189    Rotate Array
** 198    House Robber
** 200    Number of Islands
** 206    Reverse Linked List
** 207    Course Schedule 
** 209    Minimum Size Subarray Sum
** 215    Kth Largest Element in an Array
** 218    The Skyline Problem
** 238    Product of Array Except Self
** 240    Search a 2D Matrix II
253    Meeting Rooms II
** 268    Missing Number
** 283    Move Zeroes
** 289    Game of Life 
** 295    Find Median from Data Stream 
** 311    Sparse Matrix Multiplication
** 317    Shortest Distance from All Buildings
** 322    Coin Change
** 326    Power of Three
332    Reconstruct Itinerary 
** 344    Reverse String
** 380    Insert Delete GetRandom O(1) 
** 387    First Unique Character in a String
** 438    Find All Anagrams in a String 
** 443    String Compression
457    Circular Array Loop 
** 460    LFU Cache
535    Encode and Decode TinyURL 
** 560    Subarray Sum Equals K 
** 592    Fraction Addition and Subtraction
** 628    Maximum Product of Three Numbers
** 657    Robot Return to Origin
688    Knight Probability in Chessboard 
706    Design HashMap TODO:
      https://www.1point3acres.com/bbs/interview/goldmansachs-software-engineer-661596.html
      https://www.1point3acres.com/bbs/interview/goldmansachs-software-engineer-654002.html
** 720    Longest Word in Dictionary 
722    Remove Comments 
** 724    Find Pivot Index
** 836    Rectangle Overlap
862    Shortest Subarray with Sum at Least K (Follow up with 209)
887    Super Egg Drop
** 931    Minimum Falling Path Sum
** 992    Subarrays with K Different Integers
** 1010    Pairs of Songs With Total Durations Divisible by 60 
** 1086    High Five
      https://www.1point3acres.com/bbs/interview/goldmansachs-software-engineer-661596.html
      https://www.1point3acres.com/bbs/thread-656419-1-1.html
求学生中最好的平均分
给一个String[][]记录学生分数，其中每一个String[]有两个elements，
第一个是学生名字，第二个是学生分数，求所有学生中最好的平均分。如果不能整除，就得用floor，然后分数可能会有负数
https://www.1point3acres.com/bbs/thread-656419-1-1.html

** 1109    Corporate Flight Bookings
1116    Print Zero Even Odd
** 1427    Perform String Shifts
*/