/**
Get all prime factors of a number:

Ex: 24 => 2 * 2 * 2 * 3
*/
public class Solution {
    public List<Integer> primeFactorization(int num) {
        List<Integer> res = new ArrayList<>();
        if (num < 0) {
            num = Math.abs(num);
        }
        // try all prime numbers:
        for (int i = 2, max = (int) Math.sqrt(num); i < max; i++) {
            while (num % i == 0) {
                res.add(i);
                num /= i;
            }
        }
        if (num > 2) {
            res.add(num);
        }
        return res;
    }

    @Test
    public void test() {
        int num = 75;
        List<Integer> res = primeFactorization(num);
        System.out.println(res);

        num = 24;
        res = primeFactorization(num);
        System.out.println(res);
    }
}