/**
Magic Map
就是给你一个数据结构 MagicMap, get,put,remove 操作都是严格 O(1) （不存在 re-hash)。让你 implement 另一个数据结构 RandomMap, 要求 get,put,remove, getRandom(), 要求严格 O(1)。其中 getRandom 要求出现的概率跟 value 存在的数量相等。
比如一个 map {(k1, v1), (k2, v1), (k3, v2)}，那么 getRandom 要求 v1 出现概率为 v2 两倍。
MagicMap 就当成普通 hashmap 就行了。

解法是 map + ArrayList. list 里存 value，map 里存 (key, index in array).
get put 都好说，getRandom 直接生成一个随机数在 list 里取值。
remove 直接从 map 里拿到 index，然后把当前 index 的元素和 list 里最后一个元素交换，再删掉 map 里该元素。
*/
public class MagicMap<K,V> {

    class Item {
        // index of the keyList
        int idx;
        V value;
        public Item(int idx, V value) {
            this.idx = idx;
            this.value = value;
        }
    }

    private List<K> keyList;
    // key -> K, value: index of ArrayList:
    private Map<K, Item> map;

    public MagicMap() {
        this.keyList = new ArrayList<>();
        this.map = new HashMap<>();
    }

    public V get(K key) {
        if (!map.containsKey(key)) {
            return null;
        }
        return map.get(key).value;
    }

    public void put(K key, V value) {
        Item item;
        if (!map.containsKey(key)) {
            int idx = keyList.size();
            item = new Item(idx, value);
            keyList.add(key);
        }
        else {
            item = map.get(key);
            item.value = value;
        }
        map.put(key, item);
    }

    // remove 直接从 map 里拿到 index，然后把当前 index 的元素和 list 里最后一个元素交换，再删掉 map 里该元素。
    public boolean remove(K key) {
        if (!map.containsKey(key)) {
            return false;
        }
        Item delItem = map.remove(key);
        int delIdx = delItem.idx;

        K lastKey = keyList.remove(keyList.size() - 1);
        map.get(lastKey).idx = delIdx;
        keyList.set(delIdx, lastKey);

        return true;
    }
}