public class Solution {
    static private final String COMMA = ",";
    static private final String SPACE = " ";
    static private final String X = "X";

    class Artifact {
        int idx;
        int x;
        int y;

        public Artifact(int idx, int x, int y) {
            this.idx = idx;
            this.x = x;
            this.y = y;
        }
    }

    public int[] solution(int N, String artifacts, String searched) {
        int[] res = new int[2];
        String[][] grid = new String[N][N];
        for (int i = 0; i < N; i++) {
            Arrays.fill(grid[i], SPACE);
        }
        Map<Integer, List<Artifact>> artifactMap = new LinkedHashMap<>();

        String[] infos = artifacts.split(COMMA);
        int artifactIdx = 1;
        for (String info : infos) {
            String[] subInfos = info.split(SPACE);
            for (String subInfo : subInfos) {
                int[] arr = getNumFromInfo(subInfo);
                int x = arr[0];
                int y = arr[1];
                List<Artifact> artifactList = artifactMap.getOrDefault(artifactIdx, new ArrayList<>());
                artifactList.add(new Artifact(artifactIdx, x, y));
                artifactMap.put(artifactIdx, artifactList);
                grid[x][y] = String.valueOf(artifactIdx);
            }
            artifactIdx++;
        }

        // mark artifact
        for (int key : artifactMap.keySet()) {
            artifactIdx = key;
            List<Artifact> artifactList = artifactMap.get(artifactIdx);
            if (artifactList.size() == 2) {
                Artifact art1 = artifactList.get(0);
                Artifact art2 = artifactList.get(1);
                int minX = Math.min(art1.x, art1.x);
                int maxX = Math.max(art1.x, art2.x);

                int minY = Math.min(art1.y, art1.y);
                int maxY = Math.max(art1.y, art2.y);
                if (art1.x == art2.x) {
                    for (int i = minY+1; i < maxY; i++) {
                        grid[art1.x][i] = String.valueOf(artifactIdx);
                        artifactList.add(new Artifact(artifactIdx, art1.x, i));
                    }
                }
                else if (art1.y == art2.y) {
                    for (int i = minX+1; i < maxX; i++) {
                        grid[i][art1.y] = String.valueOf(artifactIdx);
                        artifactList.add(new Artifact(artifactIdx, i, art1.y));
                    }
                }
                else if (minX == maxX - 1 && minY == maxY - 1) {
                    grid[maxX][minY] = String.valueOf(artifactIdx);
                    grid[minX][maxY] = String.valueOf(artifactIdx);
                    artifactList.add(new Artifact(artifactIdx, maxX, minY));
                    artifactList.add(new Artifact(artifactIdx, minX, maxY));
                }
            }
        }
        infos = searched.split(SPACE);
        for (String info : infos) {
            int[] arr = getNumFromInfo(info);
            int x = arr[0];
            int y = arr[1];
            if (grid[x][y] != SPACE) {
                grid[x][y] = X;
            }
        }

        int foundCnt = 0;
        int notFoundCnt = 0;
        for (int key : artifactMap.keySet()) {
            boolean found = false;
            boolean allFound = true;
            List<Artifact> artifactList = artifactMap.get(key);
            for (Artifact art : artifactList) {
                int x = art.x;
                int y = art.y;
                if (grid[x][y] == X) {
                    found = true;
                }
                else {
                    allFound = false;
                }
            }
            if (allFound) {
                foundCnt++;
            }
            else if (found) {
                notFoundCnt++;
            }
        }

        res[0] = foundCnt;
        res[1] = notFoundCnt;
        return res;
    }

    private int[] getNumFromInfo(String subInfo) {
        int[] res = new int[2];
        StringBuffer digitsBuffer = new StringBuffer();
        StringBuffer charBuffer = new StringBuffer();
        for (char c : subInfo.toCharArray()) {
            if (Character.isDigit(c)) {
                digitsBuffer.append(c);
            }
            else {
                charBuffer.append(c);
            }
        }
        res[0] = Integer.parseInt(digitsBuffer.toString()) - 1;
        res[1] = charBuffer.charAt(0) - 'A';
        return res;
    }
}