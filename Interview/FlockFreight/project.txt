Prompt:

You may use JDK 11 and any open source framework/library/toolkit to speed your development but the spirit of the simulator must be implemented by you.
 
Your task is to create a simplified elevator simulator with the following requirements:

The number of elevators and floors in the building, and a scheduler must be provided when the simulator is created;
The system should support up to 10 elevators spanning 100 floors;
When the system is first started, every elevator must be on the ground floor;
Your implementation must be single-threaded and deterministic;
The system accepts synchronous input from destination-specific call buttons on each floor;
The elevator scheduler is the most likely component of the system to change. In fact, another team will be dedicated to creating and testing different scheduler algorithms for their effectiveness. You must design a system that makes it possible for this team to work in parallel with you. To complete your assignment, you will need to create a default scheduler that enqueues each call request to the elevator that is closest to the requesting floor. Tie breaks are at your discretion. Although not required by your algorithm, other schedulers may also require the requested destination floor to make an informed decision.


To reduce the scope of this assignment, you may assume that:
Every elevator services every floor of the building (there are no express elevators);
The building has a 13th floor;
There are no subterranean basements or parking garages, nor any specialty floors;
Once scheduled, elevators travel instantaneously from their current floor, past the request floor (where they pick up their passenger), and finally to their destination floor (where they drop off their passenger);
Elevators themselves do not have call, stop or, for that matter, buttons of any kind. The call button panel that is typically found in an elevator is instead found on each floor. Passengers must express their intended destination with a destination-specific call button located on each floor prior to entering the elevator. Signage above each elevator shaft directs passengers to their appropriate elevator.

Terminology:
-- Call Request: The pressing of a call button on a request floor.
-- Destination Floor: The floor number requested by a passenger on another floor;
-- Destination-Specific Call Button: A button on a request floor that signals the intent of a passenger on her request floor to travel to a destination floor;
-- Request Floor: The floor number from which a destination-specific call button is pressed