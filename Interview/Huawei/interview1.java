num > 0, arr = [1,2,3,4,6]
idx

(x,y) x = idx, y = nums[idx]

Q: points, tg: Math.abs(y1 - y2) / Math.abs(x1 - x2)

2 points: Math.abs(y1 - y2) / Math.abs(x1 - x2)

public List<Integer> getMaxTangent(int[] nums) {
    List<Integer> result = new ArrayList<>();
    int max = -1;
    int pointX1 = -1;
    int pointX2 = -1;

    for (int i = 0, n = nums.length; i < n; i++) {
        for (int j = i+1; j < n; j++) {
            float val = (float) Math.abs(nums[j] - nums[i]) / (float) (j-1);
            if (val > max) {
                max = val;
                pointX1 = i;
                pointX2 = j;
            }
        }
    }
    result.add(pointX1);
    result.add(pointX2);
    return result;
}