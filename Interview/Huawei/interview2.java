string
sort characters by frequency;

input: tree
output eert

input cccaaa
output cccaaa

input Abbb
output bbAa

A-Za-z


public class Solution {

    class NumChar {
        char c;
        int freq;
        public NumChar(char c, int freq) {
            this.c = c;
            this.freq = freq;
        }
    }

    public String resortString(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        List<NumChar> list = new ArrayList<>();

        for (char c : map.keySet()) {
            int freq = map.get(c);
            list.add(new NumChar(c, freq));
        }

        Collections.sort(list, (l1, l2) -> Integer.compare(l2.freq, l1.freq));

        StringBuffer sb = new StringBuffer();
        for (NumChar numChar : list) {
            char c = numChar.c;
            int freq = numChar.freq;
            for (int i = 0; i < freq; i++) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    @Test
    public void test1()
    {
        String s = "tree";
        String res = resortString(s);
        assertEqual("eert", res);
    }
}