/**
Print Board:

There is a board (matrix). Every cell of the board contains one integer, which is 0 initially.

Thre following operations can be applied to the Query Board.

 -- SetRow i x: change all values in the cells on the row i to value x
 -- SetCol j x: change all values in the cells on the column j to value x
 -- QueryRow i: output the sum of values on row i
 -- QueryCol j: output the sum of values on column j

 The board's dimensions are 256x256, i and j are integers from 0 to 255. x is an integer from 0 to 31.

Input:
Your program should read lines from standard input. Each line contains one of the above operations.

Ouptut: For each query, output the result of the query. 

Ex1:
SetCol 32 20
SetRow 15 7
SetRow 16 31
QueryCol 32
SetCol 2 14
QueryRow 10

Output:
5118
34
*/
public class Solution {
    static private final int MAX_SIZE = 256;
    static private final String SET_ROW = "SetRow";
    static private final String SET_COL = "SetCol";
    static private final String QUERY_COL = "QueryCol";
    static private final String QUERY_ROW = "QueryRow";
    static private final String SPACE = " ";

    public void executeQuery(List<String> queries) {
        int[][] board = new int[MAX_SIZE][MAX_SIZE];
        for (String query : queries) {
            String[] infos = query.split(SPACE);
            String action = infos[0];
            if (action.equals(SET_ROW)) {
                int i = Integer.parseInt(infos[1]);
                int x = Integer.parseInt(infos[2]);
                Arrays.fill(board[i], x);
            }
            else if (action.equals(SET_COL)) {
                int j = Integer.parseInt(infos[1]);
                int x = Integer.parseInt(infos[2]);
                for (int i = 0; i < MAX_SIZE; i++) {
                    board[i][j] = x;
                }
            }
            else if (action.equals(QUERY_ROW)) {
                int i = Integer.parseInt(infos[1]);
                int sum = 0;
                for (int num : board[i]) {
                    sum += num;
                }
                System.out.println(sum);
            }
            else if (action.equals(QUERY_COL)) {
                int j = Integer.parseInt(infos[1]);
                int sum = 0;
                for (int i = 0; i < MAX_SIZE; i++) {
                    sum += board[i][j];
                }
                System.out.println(sum);
            }
        }
    }
}