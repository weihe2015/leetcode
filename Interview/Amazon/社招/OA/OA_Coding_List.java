Experienced:
1. Amazon Fresh Promotion

2. Amazon Music Pair

3. Items in Container

4. Largest Item Association

5. Turnstile (Same as New Grad)

6. Five Star Sellers

7. Beta Testing

8. Utilization Checks

9. Top K Frequently Mentioned Keywords

10. Transaction Logs

11. Substrings of Size K with K-1 Distinct Chars

12. Number of islands (Same as New Grad)

13. Most Common Word

14. Distance between Nodes in BST

15. K Cloest Points to Origin, LC 973: https://leetcode.com/problems/k-closest-points-to-origin/

16. Shopping Patterns

17: Minimum Distance Between BST Nodes: LC 783: https://leetcode.com/problems/minimum-distance-between-bst-nodes/

18: Treasure Island + Treasure Island II

New Grad:

1. Amazon Go Store Turnstile:

2. Amazon Server Cluster (number of islands)

3. Associates Roster

4. Count ways to split strings into Prime Numbers:

5. Distinct Product IDs after removing k

6. Maximum Units:

7. Movies on Flight | IDs of two songs

8. Maimum Available Disk Space in Segment Minima (变种 Sliding Window Maximum)

9. Maximum Profit for selling Amazon Basics Product:

10. Max of Min Altitudes:

11. Min Cost to Connect All Servers:

12. Nearest cities that share X or Y:

13: Shortest Distance Between Two Robots:

14. Sort Website Items in Page

15. SubTree With maximum Average: