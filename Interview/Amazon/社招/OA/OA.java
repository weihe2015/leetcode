/**
https://leetcode.com/discuss/interview-question/344650/Amazon-Online-Assessment-Questions

DFS
BFS
Sliding Window
Tree
LinkedList.
*/

/**
Amazon Fresh Promotion: https://leetcode.com/discuss/interview-question/762546/

The code list contains groups of fruits. Both the order of the groups within the code list and the order of the fruits within the groups matter. However, between the groups of fruits, any number, and type of fruit is allowable. The term "anything" is used to allow for any type of fruit to appear in that location within the group.
C
onsider the following secret code list: [[apple, apple], [banana, anything, banana]]
Based on the above secret code list, a customer who made either of the following purchases would win the prize:
orange, apple, apple, banana, orange, banana
apple, apple, orange, orange, banana, apple, banana, banana
Write an algorithm to output 1 if the customer is a winner else output 0.

Input
The input to the function/method consists of two arguments: codeList, a list of lists of strings representing the order and grouping of specific fruits that must be purchased in order to win the prize for the day. shoppingCart, a list of strings representing the order in which a customer purchases fruit.

Output
Return an integer 1 if the customer is a winner else return 0.

Note
'anything' in the codeList represents that any fruit can be ordered in place of 'anything' in the group. 'anything' has to be something, it cannot be "nothing." 'anything' must represent one and only one fruit.
If secret code list is empty then it is assumed that the customer is a winner.

Example 1:
Input: codeList = [[apple, apple], [banana, anything, banana]] shoppingCart = [orange, apple, apple, banana, orange, banana]
Output: 1
Explanation:
codeList contains two groups - [apple, apple] and [banana, anything, banana].

The second group contains 'anything' so any fruit can be ordered in place of 'anything' in the shoppingCart. The customer is a winner as the customer has added fruits in the order of fruits in the groups and the order of groups in the codeList is also maintained in the shoppingCart.

Example 2:
Input: codeList = [[apple, apple], [banana, anything, banana]]
shoppingCart = [banana, orange, banana, apple, apple]

Output: 0

Explanation:
The customer is not a winner as the customer has added the fruits in order of groups but group [banana, orange, banana] is not following the group [apple, apple] in the codeList.

Example 3:
Input: codeList = [[apple, apple], [banana, anything, banana]] shoppingCart = [apple, banana, apple, banana, orange, banana]
Output: 0
Explanation:
The customer is not a winner as the customer has added the fruits in an order which is not following the order of fruit names in the first group.

Example 4:
Input: codeList = [[apple, apple], [apple, apple, banana]] shoppingCart = [apple, apple, apple, banana]
Output: 0
Explanation:
The customer is not a winner as the first 2 fruits form group 1, all three fruits would form group 2, but can't because it would contain all fruits of group 1.
*/
// Pattern matching: LC 10: Regular Expression Matching:
// https://leetcode.com/problems/regular-expression-matching
    private static final String DOT = ".";
    private static final String STAR = "*";
    private static final String ANYTHING = "anything";

    public boolean canWin(String[][] codeLists, String[] shoppingCart) {
        if (codeLists == null || codeLists.length == 0) {
            return true;
        }
        if (shoppingCart == null || shoppingCart.length == 0) {
            return false;
        }
        // use wildcard-matching:
        List<String> secreteCode = new ArrayList<>();
        secreteCode.add(DOT);
        secreteCode.add(STAR);

        for (String[] codeList : codeLists) {
            for (String code : codeList) {
                if (code.equals(ANYTHING)) {
                    secreteCode.add(DOT);
                }
                else {
                    secreteCode.add(code);
                }
            }
            secreteCode.add(DOT);
            secreteCode.add(STAR);
        }
        List<String> shoppingCartList = Arrays.asList(shoppingCart);
        return isMatch(shoppingCartList, secreteCode);
    }

    private boolean isMatch(List<String> strlist, List<String> pattern) {
        int m = strlist.size();
        int n = pattern.size();
        boolean[][] dp = new boolean[m+1][n+1];
        // empty regex matches empty string:
        dp[0][0] = true;

        // scan the regex string and initialize the * match with 2 characters before:
        // s = "", p = a*  ba* = b
        // s = "", p = a?  ba? = b
        for (int j = 1; j <= n; j++) {
            // j > 1 in case index out of bound
            if (j > 1 && pattern.get(j-1).equals(STAR)) {
                dp[0][j] = dp[0][j-2];
            }
        }

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if two characters from s and p at same index are matching,
                // or current regex character is .
                if (strlist.get(i-1).equals(pattern.get(j-1)) || pattern.get(j-1).equals(DOT)) {
                    dp[i][j] = dp[i-1][j-1];
                }
                // if current regex character is *:
                else if (j > 1 && pattern.get(j-1).equals(STAR)) {
                    // assume regex string is always valid, no * at the beginning:
                    // the character before * is a .
                    // or current character from s matches character before *:
                    if (strlist.get(i-1).equals(pattern.get(j-2)) || pattern.get(j-2).equals(DOT)) {
                        // dp[i-1][j]: match previous character with p.substring(0,j) regex
                        dp[i][j] = dp[i-1][j] || dp[i][j-1] || dp[i][j-2];
                    }
                    // in this case: a* or .* count as empty:
                    else {
                        dp[i][j] = dp[i][j-2];
                    }
                }
                else {
                    dp[i][j] = false;
                }
            }
        }

        return dp[m][n];
    }

// Amzon Music Pairs: https://leetcode.com/discuss/interview-question/861432/
/**
Users can choose any songs they want to add to the community list, but
only in pairs of songs with durations that add up to a multiple of 60 seconds (e.g. 60, 120, 180).

n = 3

songs = [37, 23, 60]
One pair of songs can be chosen whose combined duration is a multiple of a whole minute (37 + 23 = 60) and the
return value would be 1. While the third song is a single minute long, songs must be chosen in pairs.

Example 1:

n = 5, songs = [30, 20, 150, 100, 40]
Ans = 3

Example 2:
n = 3, songs = [60, 60, 60], 
Ans: 3
*/

/**
Input:   int songs[n]: array of integers representing song durations in seconds
Output: long: the total number of songs pairs that add up to a multiple of 60 seconds. If there are no suitable pairs,
return 0.


*/
    public long getSongPairCount(int[] songs) {
        int res = 0;
        // key -> num % 60, val : freq
        Map<Integer, Integer> map = new HashMap<>();
        for (int song : songs) {
            song %= 60;
            int key = song == 0 ? 0 : (60 - song);
            if (map.containsKey(key)) {
                res += map.get(key);
            }
            int val = map.getOrDefault(song, 0) + 1;
            map.put(song, val);
        }
        return res;
    }

/**
Items in Containers: https://leetcode.com/discuss/interview-question/861453/

Amazon would like to know how much inventory exists in their closed inventory compartments. Given a string s
consisting of items as "*" and closed compartments as an open and close "|", an array of starting indices
startIndices, and an array of ending indices endIndices, determine the number of items in closed compartments
within the substring between the two indices, inclusive.

An item is represented as an asterisk ('*' = ascii decimal 42)
A compartment is represented as a pair of pipes that may or may not have items between them ('|' = ascii decimal 124).

Example

s = '|**|*|*'

startIndices = [1, 1]

endIndices = [5, 6]

The string has a total of 2 closed compartments, one with 2 items and one with 1 item. For the first pair of
indices, (1, 5), the substring is '|**|*'. There are 2 items in a compartment.

For the second pair of indices, (1, 6), the substring is '|**|*|' and there are 2 + 1 = 3 items in compartments.

Both of the answers are returned in an array, [2, 3].

Function Description .

Complete the numberOfItems function in the editor below. The function must return an integer array that contains
the results for each of the startIndices[i] and endIndices[i] pairs.

numberOfItems has three parameters:

s: A string to evaluate
startIndices: An integer array, the starting indices.
endIndices: An integer array, the ending indices.

Constraints

1 ≤ m, n ≤ 105
1 ≤ startIndices[i] ≤ endIndices[i] ≤ n
Each character of s is either '*' or '|'

Sample Case 0
Sample Input For Custom Testing

*|*| -> s = "*|*|"
input:
1 -> startIndices[] size n = 1
1 -> startIndices = 1
1 -> endIndices[] size n = 1
3 -> endIndices = 3

** Sample Output**
0

Explanation
s = *|*|

n = 1
startIndices = [1]
n = 1
startIndices = [3]

The substring from index = 1 to index = 3 is '*|*'. There is no compartments in this string.

Sample Case 1
Sample Input For Custom Testing

*|*|*| -> s = "*|*|*|"
1 -> startIndices[] size n = 1
1 -> startIndices = 1
1 -> endIndices[] size n = 1
6 -> endIndices = 6

Sample Output
2

Explanation
s = '*|*|*|'
n = 1
startIndices = [1]
n = 1
startIndices = [1]

The substring from index = 1 to index = 6 is '*|*|*|'. There are two compartments in this string at (index = 2,
index = 4) and (index = 4, index = 6). There are 2 items between these compartments.

*/
/**
     0 1 2 3 4 5 6
s = "| * * | * | *"
l    0 0 0 3 3 5 5
r    0 3 3 3 5 5 -1
s    0 1 2 2 3 3 4 
*/

    public List<Integer> numOfItems(String s, int[] startIndices, int[] endIndices) {
        List<Integer> res = new ArrayList<>();
        int n = s.length();
        int m = startIndices.length;
        // idx of left bar:
        int[] left = new int[n];
        // idx of right bar:
        int[] right = new int[n];
        // num of stars from left:
        int[] stars = new int[n];

        int leftIdx = -1;
        for (int i = 0; i < n; i++) {
            if (s.charAt(i) == '|') {
                leftIdx = i;
            }
            left[i] = leftIdx;
        }

        int rightIdx = -1;
        for (int i = n-1; i >= 0; i--) {
            if (s.charAt(i) == '|') {
                rightIdx = i;
            }
            right[i] = rightIdx;
        }

        int starCnt = 0;
        for (int i = 0; i < n; i++) {
            if (s.charAt(i) == '*') {
                starCnt++;
            }
            stars[i] = starCnt;
        }

        for (int i = 0; i < m; i++) {
            int startIdx = startIndices[i] - 1;
            int endIdx = endIndices[i] - 1;
            int leftBar = right[startIdx];
            int rightBar = left[endIdx];
            if (leftIdx >= rightIdx) {
                res.add(0);
            }
            else {
                starCnt = stars[rightBar] - stars[leftBar];
                res.add(starCnt);
            }
        }

        return res;
    }

/**
Largest Item Association: https://leetcode.com/discuss/interview-question/782606/

If an item A is ordered by a customer, them item B is also likely to be ordered by the same customer. All items that are linked together by an item association can be considered to be in the same group. An item without any association to any other item can be considered to be in its own item association group of size 1.

Given a list of item association relationships, write an algorithm that outputs the largest association group. If two groups have the same number of items, then select the group which contains the item that appears first in lexicographic order.

Ex:
Input: [[Item1, Item2], [Item3, Item4], [Item4, Item5]];
Output: [Item3, Item4, Item5]

Explanation: There are two item association groups: group1: [Item1, Item2], group2: [Item3, Item4, Item5]. Group 2 has the largest association. So the output is [Item3, Item4, Item5]

Helper Description:

*/
    class PairString {
        String first;
        String second;
        public PairString(String first, String second) {
            this.first = first;
            this.second = second;
        }
    }

    public List<String> largestItemAssociation(List<PairString> itemAssociation) {
        // undirected Graph:
        Set<String> itemSet = new TreeSet<>();
        Set<String> dupAssoSet = new HashSet<>();

        Map<String, List<String>> graphMap = new TreeMap<>();
        for (PairString pair : itemAssociation) {
            String fstItem = pair.first;
            String sndItem = pair.second;
            // remove duplicate item association:
            String key = fstItem + sndItem;
            if (dupAssoSet.contains(key)) {
                continue;
            }
            dupAssoSet.add(key);
            
            addItemToList(graphMap, fstItem, sndItem);
            addItemToList(graphMap, sndItem, fstItem);

            itemSet.add(fstItem);
            itemSet.add(sndItem);
        }

        Set<String> visitedSet = new HashSet<>();

        Queue<List<String>> associations = new PriorityQueue<>((list1, list2) -> {
            if (list1.size() != list2.size()) {
                return list2.size() - list1.size();
            }
            for (int i = 0, n = list1.size(); i < n; i++) {
                String s1 = list1.get(i);
                String s2 = list2.get(i);
                if (s1.equals(s2)) {
                    continue;
                }
                return s1.compareTo(s2);
            }
            return 0;
        });

        for (String item : itemSet) {
            Set<String> treeSet = new TreeSet<>();
            dfs(graphMap, item, visitedSet, treeSet);
            associations.add(new ArrayList<>(treeSet));
        }

        List<String> res = associations.peek();
        return res;
    }

    private void addItemToList(Map<String, List<String>> graphMap, String key, String val) {
        List<String> list = graphMap.getOrDefault(key, new ArrayList<>());
        list.add(val);
        graphMap.put(key, list);
    }

    private void dfs(Map<String, List<String>> graphMap, String from, Set<String> visitedSet, Set<String> treeSet) {
        if (visitedSet.contains(from)) {
            return;
        }
        visitedSet.add(from);
        treeSet.add(from);
        if (!graphMap.containsKey(from)) {
            return;
        }
        List<String> neighbors = graphMap.get(from);
        for (String neighbor : neighbors) {
            dfs(graphMap, neighbor, visitedSet, treeSet);
        }
    }

/*
Amazon Go Store Turnstile:
https://leetcode.com/discuss/interview-question/798231/

Imagine a small Amazon Go store that has exactly one turnstile. It can be used by customers either as an entrance or an exit. Sometimes multiple customers want to pass through the turnstile and their directions can be different. The ith customer comes to the turnstile at time[i] and wants to either exit the store if direction[i] = 1 or enter the store if direction[i] = 0. Customers form 2 queues, one to exit and one to enter. they are ordered by the time when they came to the turnstile and, if the times are equal, by their indices.

If one customer wants to enter the store and another customer wants to exit at the same moment. There are three cases:
    1. If in the previous second the turnstile was not used (maybe it was used before, but not at the previous second), then the customer who wants to exit goes first.
    2. If in the previous second the turnstile was used as an exit, then the customer who wants to leave goes first.
    3. If in the previous second the turnstile was used as an entrance, then the customer who wants to enter goes first.

Passing through the turnstile takes 1 second.

Input:
    The function/method consists of three arguments:
     numCustomers, an integer representing the number of customers (n)
     arrTime, a list of integers where the value at index i is the time in seconds when the ith customer will come to the turnstile
     direction, a list of integers where the value at index i is the direction of the ith customer

Output:
    Return a list of integers where the value at index i is the time when the ith customer will pass the turnstile.

Constraints:
    1 <= numCustomers <= 10^5
    0 <= arrTime[i] <= arrTime[i+1] <= 10^9
    0 <= directions[j] <= 1
    0 <= j <= numCustomers - 1

Ex1:
   input:
        numCustomers = 4
        arrTime = [0,0,1,5]
        direction = [0,1,1,0]
    output:
        [2,0,1,5]

Explanation:
    At time 0, customers 0 and 1 want to pass through the turnstile. Customer 0 wants to enter the store and customer 1 wants to leave the store. The turnstile was not used in the previous second, so the priority is on the side of the customer 1.
    At time 1, customers 0 and 2 want to pass through the turnstile, Customer 2 wants to leave the store and at the previous second the turnstile was used as an exit, so the customer 2 passes through the turnstile.
    At time 2, customer 0 passes through the turnstile.
    At time 5, customer 3 passes through the turnstile

Ex2:
    input:
        numCustomers = 5
        arrTime = [0,1,1,3,3]
        direction = [0,1,0,0,1]
    output:
        [0,2,1,4,3]

Explanation:
    At time 0: customer 0 passes through the turnstile (enters)
    At time 1, customers 1 (exit) and 2 (enter) want to pass through the turnstile, and customer 2 passes through the turnstile becuase their direction is equal to the direction at the previous second.
    At time 2, customer 1 passes through the turnstile (exit)
    At time 3, customers 3 (enter) and 4 (exit) want to pass through the turnstile, Customer 4 passes through the turnstile because at the previous second the turnstile was used to exit
    At time 4, customer 3 passes through the turnstile
*/

    private int[] turnstile(int numCustomers, int[] arrTime, int[] directions) {
        int[] res = new int[numCustomers];
        // exitTurtile
        int prevDir = 1;
        int i = 0, j = 1, time = 0;
        while (j < numCustomers) {
            // i goes first
            if (arrTime[i] != arrTime[j]) {
                res[i] = time;
                prevDir = directions[i];
                i = j;
            }
            else {
                // j goes first
                if (directions[i] != prevDir) {
                    res[j] = time;
                    arrTime[i]++;
                }
                else {
                    res[i] = time;
                    arrTime[j]++;
                    i = j;
                }
            }
            j++;
            time++;
        }
        res[i] = Math.max(time, arrTime[i]);
        return res;
    }

/**
Five Star Sellers: https://leetcode.com/discuss/interview-question/854110/

Given the number of five-star and total reviews for each product a company sells, as well as the threshold percentage, what is the minimum number of additional five-star reviews the company needs to become five star seller.
For example, there are 3 products (n=3) with productRatings = [[4,4],[1,2],[3,6]], percentage rating threshold = 77.
[1,2] indicates => [1 (five star reviews) ,2 (total reviews)].
We need to get the seller reach the threshold with minimum number of additional five star reviews.

Before we add more five star reviews, the percentage for this seller is ((4/4) + (1/2) + (3/6))/3 = 66.66%
If we add a five star review to 2nd product, ((4/4) + (2/3) + (3/6))/3 = 72.22%
If we add another five star review to 2nd product, ((4/4) + (3/4) + (3/6))/3 = 75%
If we add a five star review to 3rd product, ((4/4) + (3/4) + (4/7))/3 = 77.38%

At this point, 77% (threshold) is met. Therefore, answer is 3 (because that is the minimum five star reviews we need to add, to get the seller reach the threshold).

*/

    class ListComparator implements Comparator<List<Integer>> {
        @Override
        public int compare(List<Integer> list1, List<Integer> list2) {
            double rateDiff1 = getRateDiff(list1);
            double rateDiff2 = getRateDiff(list2);
            return Double.compare(rateDiff2, rateDiff1);
        }
    }

    public int fiveStarReviews(List<List<Integer>> productRatings, int ratingsThreshold){
        int cnt = 0;
        int n = productRatings.size();
        // maxHeap
        Comparator comp = new ListComparator();
        Queue<List<Integer>> pq = new PriorityQueue(comp);
        double ratingSum = 0.0;
        for (List<Integer> rating : productRatings) {
            pq.offer(rating);
            ratingSum += getCurrRating(rating);
        }

        while (ratingSum < ratingsThreshold * n) {
            List<Integer> rating = pq.poll();
            double currRating = getCurrRating(rating);
            double newRating = getNewRating(rating);
            ratingSum = ratingSum - currRating + newRating;

            List<Integer> newList = createNewList(rating);
            pq.offer(newList);
            cnt++;
        }

        return cnt;
    }

    private double getRateDiff(List<Integer> rating) {
        double currRating = getCurrRating(rating);
        double newRating = getNewRating(rating);
        return newRating - currRating;
    }

    private List<Integer> createNewList(List<Integer> rating) {
        return Arrays.asList(rating.get(0) + 1, rating.get(1) + 1);
    }

    private double getCurrRating(List<Integer> rating) {
        return 100.0 * (double) rating.get(0) / rating.get(1);
    }


    private double getNewRating(List<Integer> rating) {
        return 100.0 * (double) (rating.get(0) + 1) / (rating.get(1) + 1);
    }

/**
Beta Testing:

Given the complexity of the tasks, as well as how many days the test is running, what is the overall minimum complexity that can be achieved with optimal planning?

For example, let's say there are n = 5 tasks, where complexity = [1, 5, 3, 2, 4], and the length of the test is days = 2. (Note that complexity denotes both the assigned complexity of each task as well as the order in which they need to be executed.) The best option is to execute the first task on the first day and the rest on the second day. The complexity of the first day would be 1, since that's the only task, and the complexity of the second day would be 5, because that's the complexity level of the most complex task that day. Therefore, the answer is 1 + 5 = 6.

Example 1: n = 5, Complexity: [30,10,40,20,50], days = 2
Output: 80

Example 2: n = 6, Complexity: [74303, 20452, 66120, 44483, 5370, 68585], days = 5

Output: 234830
Explanation: task 3 and 4 on same day

https://leetcode.com/problems/minimum-difficulty-of-a-job-schedule/
*/
    // 2D DP array:
    public int findMinComplexity(int[] complexity, int days) {
        int n = complexity.length;
        if (n < days) {
            return -1;
        }
        int[][] dp = new int[n+1][days+1];
        for (int i = 0; i <= n; i++) {
            Arrays.fill(dp[i], Integer.MAX_VALUE);
        }
        
        int currMax = 0;
        for (int i = n-1; i >= 0; i--) {
            if (complexity[i] > currMax) {
                currMax = complexity[i];
            }
            dp[i][1] = currMax;
        }
        
        for (int i = 2; i <= days; i++) {
            for (int j = 0; j <= n-i; j++) {
                int localMax = 0;
                for (int k = j; k <= n-i; k++) {
                    localMax = Math.max(localMax, complexity[k]);
                    dp[j][i] = Math.min(dp[j][i], dp[k+1][i-1] + localMax);
                }
            }
        }
        return dp[0][d];
    }

    // 1D DP array:
    public int findMinComplexity(int[] complexity, int days) {
        int n = complexity.length;
        if (n < days) {
            return -1;
        }
        int[] dp = new int[n+1];
        for (int i = n-1; i >= 0; i--) {
            dp[i] = Math.max(dp[i+1], complexity[i]);
        }

        for (int d = 2; d <= days; d++) {
            for (int j = 0; j <= n-d; j++) {
                int currMax = 0;
                dp[j] = Integer.MAX_VALUE;
                for (int k = j; k <= n-d; k++) {
                    currMax = Math.max(currMax, complexity[k]);
                    dp[j] = Math.min(dp[j], currMax + dp[k+1]);
                }
            }
        }

        return dp[0];
    }

/**
Utilization Checks: https://leetcode.com/discuss/interview-question/895097/

Once an action of adding or reducing instances depending on the currennt load. Once an action of adding or reducing the number of instances is performed, the system will stop monitoring for 10s. During that time, the number of instances does not change.

-- Average utilization < 25%: An action is instantiated to reduce the number of instances by half if the number of instances is greater than 1 (take the ceiling if the number is not an integer). If the number of instance is 1, take no action.
-- 25% <= Average utilization <= 60%: take no action
-- Average utilization > 60%. An action is instantiated to double the number of instances if the doubled value does not exceed 2*10^8. If the number of instances exceedes this limit upon doubling, perform no action.

Given an array of integers that represents the average utilizationn at each second, determine the number of instances at the end of the time frame.

Example:
instances = 2
averageUtils = [25,23,1,2,3,4,5,6,7,8,9,10,76,80]

At second 1: 25 <= 25, no action is perform
At second 2: 23 < 25, reduce the number of instances by half, 2/2 = 1
stop from second 3 to second 11
At second 12: 76 > 60: double the instances
stop from second 12 to second 21 

Function Description:
finalInstances has the following parameter(s):
    int instances: an integer that represents the original number of instances running.
    int averageUtil[n]: an array of integers that represents the average utilization at each second of the time frame
Returns:
    int: an integer that represents the final number of instances running.
*/

    static private final int MAX_INSTANCES = 2 * 100000000;
    public int finalInstances(int instances, List<Integer> averageUtil) {
        int i = 0;
        int n = averageUtil.size();
        while (i < n) {
            int rate = averageUtil.get(i);
            if (rate < 25 && instances > 1) {
                if (instances % 2 == 0) {
                    instances /= 2;
                }
                else {
                    instances = (instances + 1) / 2;
                }
                i += 10;
            }
            else if (rate > 60 && 2 * instances <= MAX_INSTANCES) {
                instances = 2 * instances;
                i += 10;
            }
            i += 1;
        }
        return instances;
    }


/**
Top K Frequently Mentioned Keywords:

Given a list of reviews, a list of keywords and an integer k. Find the most popular k keywords in order of most to least frequently mentioned.

The comparison of strings is case-insensitive.
Multiple occurances of a keyword in a review should be considred as a single mention.
If keywords are mentioned an equal number of times in reviews, sort alphabetically.

Example 1:

Input:
k = 2
keywords = ["anacell", "cetracular", "betacellular"]
reviews = [
  "Anacell provides the best services in the city",
  "betacellular has awesome services",
  "Best services provided by anacell, everyone should use anacell",
]

Output:
["anacell", "betacellular"]

Explanation:
"anacell" is occuring in 2 different reviews and "betacellular" is only occuring in 1 review.
Example 2:

Input:
k = 2
keywords = ["anacell", "betacellular", "cetracular", "deltacellular", "eurocell"]
reviews = [
  "I love anacell Best services; Best services provided by anacell",
  "betacellular has great services",
  "deltacellular provides much better services than betacellular",
  "cetracular is worse than anacell",
  "Betacellular is better than deltacellular.",
]

Output:
["betacellular", "anacell"]

Explanation:
"betacellular" is occuring in 3 different reviews. "anacell" and "deltacellular" are occuring in 2 reviews, but "anacell" is lexicographically smaller.

*/

    class FreqWord {
        int count;
        String word;
        FreqWord(int count, String word) {
            this.count = count;
            this.word = word;
        }
    }

    static private final String SPACE = " ";

    public List<String> findMostPopularWords(String[] keywords, String[] reviews, int k) {
        List<String> res = new ArrayList<>();
        Map<String, Integer> freqMap = new HashMap<>();
        for (String keyword : keywords) {
            freqMap.put(keyword, 0);
        }

        for (String review : reviews) {
            String[] words = review.split(SPACE);
            Set<String> visited = new HashSet<>();
            for (String word : words) {
                word = word.toLowerCase();
                if (freqMap.containsKey(word) && !visited.contains(word)) {
                    int freq = freqMap.get(word);
                    freq++;
                    freqMap.put(word, freq);

                    visited.add(word);
                }
            }
        }

        Queue<FreqWord> pq = new PriorityQueue<>((fw1, fw2) -> {
            if (fw1.count != fw2.count) {
                return Integer.compare(fw2.count, fw1.count);
            } else {
                return fw1.word.compareTo(fw2.word);
            }
        });

        for (String key : freqMap.keySet()) {
            int count = freqMap.get(key);
            System.out.println("word: " + key + ", count: " + count);
            pq.offer(new FreqWord(count, key));
        }

        while (!pq.isEmpty() && k > 0) {
            res.add(pq.poll().word);
            k--;
        }
        return res;
    }

/**
Transaction Logs: https://leetcode.com/discuss/interview-question/862600/

Amazon parses logs of user transactions/activity to flag fraudulent activity. The log file is represented as an Array of arrays. The arrays consist of the following data:

[<userid1> <userid2> <# of transactions>]

For example:

[345366 89921 45]
Note the data is space delimited

So, the log data would look like:

[
[345366 89921 45],
[029323 38239 23]
...
]

Write a function to parse the log data to find distinct users that meet or cross a certain threshold. The function will take in 2 inputs:
Input 1: Log data in form an array of arrays
Input 2: threshold as an integer

Output should be an array of userids that are sorted. output order is sorted by ID's value. 345366>38239>29323.

If same userid appears in the transaction as userid1 and userid2, it should count as one occurence, not two.

Input:
[
[345366 89921 45],
[029323 38239 23],
[38239 345366 15],
[029323 38239 77],
[345366 38239 23],
[029323 345366 13],
[38239 38239 23]
...
]
k = 2;

Output:
[345366 , 38239, 029323]
345366 -4 , 38239 -5, 029323-3
*/

    public List<String> getFraudIds(String[] inputs, int threshold) {
        Map<String, Integer> freqMap = new HashMap<>();

        for (String input : inputs) {
            String[] texts = input.split(SPACE);
            String id1 = texts[0];
            String id2 = texts[1];

            updateFrequency(id1, freqMap);
            if (!id1.equals(id2)) {
                updateFrequency(id2, freqMap);
            }

        }
        List<String> res = new ArrayList<>();
        for (String id : freqMap.keySet()) {
            int freq = freqMap.get(id);
            if (freq >= threshold) {
                res.add(id);
            }
        }
        Collections.sort(res, (s1, s2) ->
                Integer.compare(Integer.parseInt(s2), Integer.parseInt(s1))
        );

        return res;
    }

    private void updateFrequency(String id, Map<String, Integer> freqMap) {
        int freq = freqMap.getOrDefault(id, 0);
        freq++;
        freqMap.put(id, freq);
    }

/**
Substrings of Size K with K-1 Distinct Chars: https://leetcode.com/discuss/interview-question/877624/

The word game begins with your manager writing a string, and a number K on the board.
You and your teammates must find a substring of size K such that there is exactly one character that is repeated once.
In other words, there should be K - 1 distinct characters in the substring.

Write an algorithm to help your teammates find the correct answer. If no such substring can be found, return an empty list; If multiple such substrings exit, return all of them, without repetitions. The order in which the substrings are returned does not matter.

Input: it has two arguments:
1. inputString: representing the string written by the manager.
2. num: an integer representing the number K, written by the manager on the board.

Return a list of all substrings of inputString with K characters, that have K - 1 distinct character, i.e. exactly one character is repeated, or an empty list if no such substring exists in inputString. The order in which the substrings are returned does not matter.

Constraints:
    The input integer can only be greater than or equal to 0 and less than or equal to 26 (0 <= num <= 26).
The input string consists of only lowercase alphabetic characters.

Ex1: 
Input:
     inputString = awaglk
     num = 4:
Output: [awag]

Explanation:
The substrings are {awag, wagl, aglk}
The answer is awag as it has 3 distinct characters in a string of size 4, and only one character is repeated twice.

Ex2: 
Input: 
    inputString = democracy
    num = 5

Output: [ocrac, cracy]

Ex3:
Input: 
    inputString = wawaglknagagwunagkwkwagl
    num = 4

Output: [awag, naga, gagw, gkwk, wkwa]
*/

    public List<String> findKDistinctChar(String s, int k) {
        List<String> res = new ArrayList<>();
        int[] charMap = new int[26];
        int l = 0;
        int r = 0;
        int n = s.length();
        int numOfChars = 0;
        int numOfDistict = 0;

        while (r < n) {
            if (numOfChars < k) {
                char rc = s.charAt(r);
                charMap[rc-'a']++;
                numOfChars++;
                if (charMap[rc-'a'] == 1) {
                    numOfDistict++;
                }
                r++;
            }
            while (numOfChars == k) {
                if (numOfDistict == k-1) {
                    String substr = s.substring(l, r);
                    res.add(substr);
                }
                char lc = s.charAt(l);
                charMap[lc-'a']--;
                numOfChars--;
                if (charMap[lc-'a'] == 0) {
                    numOfDistict--;
                }
                l++;
            }
        }

        return res;
    }

/**
Number of Islands:

Given a 2d grid map of '1's (land) and '0's (water),
count the number of islands. An island is surrounded by water and
is formed by connecting adjacent lands horizontally or vertically.
You may assume all four edges of the grid are all surrounded by water.

Example 1:

Input:
11110
11010
11000
00000

Output: 1
Example 2:

Input:
11000
11000
00100
00011

Output: 3
*/
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int res = 0;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1' && !visited[i][j]) {
                    res++;
                    numIslands(grid, visited, i, j, m, n);
                }
            }
        }
        return res;
    }

    private void numIslands(char[][] grid, boolean[][] visited, int i, int j, int m, int n) {
        if (i < 0 || i >= m || j < 0 || j >= n) {
            return;
        }
        if (grid[i][j] != '1' || visited[i][j]) {
            return;
        }
        visited[i][j] = true;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            numIslands(grid, visited, x, y, m, n);
        }
    }

/**
Most Common Words: Leetcode 819: https://leetcode.com/problems/most-common-word/

Given a paragraph and a list of banned words, 
return the most frequent word that is not in the list of banned words.  
It is guaranteed there is at least one word that isn't banned, and that the answer is unique.

Words in the list of banned words are given in lowercase, and free of punctuation.  
Words in the paragraph are not case sensitive.  The answer is in lowercase.

Example:

Input: 
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
banned = ["hit"]
Output: "ball"
Explanation: 
"hit" occurs 3 times, but it is a banned word.
"ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph. 
Note that words in the paragraph are not case sensitive,
that punctuation is ignored (even if adjacent to words, such as "ball,"), 
and that "hit" isn't the answer even though it occurs more because it is banned.
 
*/

    public String mostCommonWord(String paragraph, String[] banned) {
        Set<String> bannedSet = new HashSet<>();
        for (String ban : banned) {
            bannedSet.add(ban);
        }
        Map<String, Integer> freqMap = new HashMap<>();
        int i = 0;
        int j = 0;
        int n = paragraph.length();
        while (j < n) {
            char c = paragraph.charAt(j);
            if (Character.isLetter(c)) {
                j++;
            }
            else {
                updateFreqMap(freqMap, paragraph, i, j, bannedSet);
                j++;
                i = j;
            }
        }
        updateFreqMap(freqMap, paragraph, i, j, bannedSet);
        
        String res = "";
        int maxFreq = -1;
        for (String key : freqMap.keySet()) {
            int freq = freqMap.get(key);
            if (freq > maxFreq) {
                maxFreq = freq;
                res = key;
            }
        }
        return res;
    }
    
    private void updateFreqMap(Map<String, Integer> freqMap, String paragraph, int i, int j, Set<String> bannedSet) {
        if (i >= j) {
            return;
        }
        String key = paragraph.substring(i, j).toLowerCase();
        if (!bannedSet.contains(key)) {
            int freq = freqMap.getOrDefault(key, 0);
            freq++;
            freqMap.put(key, freq);
        }
    }

/**
Distance Between Nodes in BST

Given a list of unique integers nums, construct a BST from it (you need to insert nodes one-by-one with the given order to get the BST) and find the distance between two nodes node1 and node2. Distance is the number of edges between two nodes. If any of the given nodes does not appear in the BST, return -1.

Example 1:

Input: nums = [2, 1, 3], node1 = 1, node2 = 3
Output: 2
Explanation:
     2
   /   \
  1     3
*/

    class TreeNode {
        TreeNode left;
        TreeNode right;
        int val;
        public TreeNode(int val) {
            this.val = val;
        }
    }

    public int distanceBetweenTwoNodes(int[] nums, int node1, int node2) {
        TreeNode root = null;
        Set<Integer> nodeSet = new HashSet<>();
        for (int num : nums) {
            root = insertIntoBST(root, num);
            nodeSet.add(num);
        }

        if (!nodeSet.contains(node1) || !nodeSet.contains(node2)) {
            return -1;
        }

        TreeNode parNode = lowestCommonAncestor(root, node1, node2);
        int dist1 = distanceFromPar(parNode, node1);
        int dist2 = distanceFromPar(parNode, node2);

        return dist1 + dist2;
    }

    private int distanceFromPar(TreeNode parNode, int childVal) {
        int dist = 0;
        TreeNode curr = parNode;
        while (curr != null) {
            if (curr.val == childVal) {
                break;
            }
            else if (curr.val < childVal) {
                curr = curr.right;
            }
            else {
                curr = curr.left;
            }
            dist++;
        }
        return dist;
    }

    private TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        TreeNode curr = root;
        while (curr != null) {
            if (curr.val < val) {
                if (curr.right == null) {
                    curr.right = new TreeNode(val);
                    break;
                }
                else {
                    curr = curr.right;
                }
            }
            else if (curr.val > val) {
                if (curr.left == null) {
                    curr.left = new TreeNode(val);
                    break;
                }
                else {
                    curr = curr.left;
                }
            }
        }
        return root;
    }

    public TreeNode lowestCommonAncestor(TreeNode root, int p, int q) {
        TreeNode curr = root;
        TreeNode res = null;
        while (curr != null) {
            if (p < curr.val && q < curr.val) {
                curr = curr.left;
            }
            else if (p > curr.val && q > curr.val) {
                curr = curr.right;
            }
            else {
                res = curr;
                break;
            }
        }
        return res;
    }

/**
K Closest Points to Origin; 

Leetcode 973: https://leetcode.com/problems/k-closest-points-to-origin/
*/
    class Point {
        int x;
        int y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public int[][] kClosest(int[][] points, int K) {
        Point originPt = new Point(0, 0);
        // minHeap
        Queue<Point> pq = new PriorityQueue<>((p1, p2) -> 
                                              Integer.compare(distance(p1, originPt), 
                                                              distance(p2, originPt)));
        
        for (int[] point : points) {
            int x = point[0];
            int y = point[1];
            Point p = new Point(x, y);
            pq.offer(p);
        }
        
        List<Point> list = new ArrayList<>();
        while (!pq.isEmpty() && list.size() < K) {
            list.add(pq.poll());
        }

        int n = list.size();
        int[][] res = new int[n][2];
        for (int i = 0; i < n; i++) {
            Point p = list.get(i);
            res[i][0] = p.x;
            res[i][1] = p.y;
        }
        
        return res;
    }
    
    private int distance(Point p1, Point p2) {
        int x = p1.x - p2.x;
        int y = p1.y - p2.y;
        return (x * x + y * y);
    }

/**
Minimum Distance Between BST Nodes: 
Leetcode 783: https://leetcode.com/problems/minimum-distance-between-bst-nodes/
*/
    // Iterative:
    public int minDiffInBST(TreeNode root) {
        // inorder traversal:
        int minDiff = Integer.MAX_VALUE;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = root;
        TreeNode prev = null;
        while (!stack.isEmpty() || curr != null) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if (prev != null) {
                int diff = curr.val - prev.val;
                minDiff = Math.min(minDiff, diff);
            }
            prev = curr;
            curr = curr.right;
        }
        return minDiff;
    }

/**
Shopping Patterns: https://leetcode.com/discuss/interview-question/906481/

Amazon is trying to understand customer shopping patterns and offer items that are regularly bought together to new customers. Each item that has been bought together can be represented as an undirected graph where edges join often bundled products. A group of n products is uniquely numbered from 1 of product_nodes. A trio is defined as a group of three related products that all connected by an edge. Trios are scored by counting the number of related products outside of the trio, this is referred as a product sum.

Given product relation data, determine the minimum product sum for all trios of related products in the group. If no such trio exists, return -1.

Example
products_nodes = 6
products_edges = 6
products_from = [1,2,2,3,4,5]
products_to =   [2,4,5,5,5,6]

Product           Related Product
    1                  2
    2                  1, 4, 5
    3                  5
    4                  2, 5  
    5                  2, 3, 4, 6
    6                  5

A graph of n = 6 products where the only trio of related products is (2, 4, 5).

The product scores based on the graph above are:

Product           OutSide Product                   Which Products Are Outside
    2                  1                                              1  
    4                  0                                               
    5                  2                                              3, 6
In the diagram above, the total product score is 1 + 0 + 2 = 3 for the trio (2, 4, 5).

getMinScore has the following parameters: 
int products_nodes: the total number of products
int products_edges the total number of edges representing related products
int products_from[products_nodes]: each element is a node of one side of an edge.
int products_to[products edges]: each products_to[i] is a node connected to products_from[i]

Returns:
int: the minimum product sum for all trios of related products in the group. If no such trio exists, return -1.

Input:
5 6 -> products_nodes = 5 products_edges = 6
1 2 -> products_from[0] = 1 products_to[0] = 2
1 3 -> products_from[1] = 1 products_to[1] = 3
2 3 -> products_from[2] = 2 products_to[2] = 3
2 4 -> products_from[3] = 2 products_to[3] = 4
3 4 -> products_from[4] = 3 products_to[4] = 4
4 5 -> products_from[5] = 4 products_to[5] = 5

Sample Output

2

Explanation

There are two possible trios: {1,2,3} and {2,3,4}

The score for {1,2,3} is 0 + 1 + 1 = 2.

The score for {2,3,4} is 1 + 1 + 1 = 3.

Return 2.
*/

    public int getMinScore(int productNodes, List<Integer> productsFrom, List<Integer> productsTo) {
        int n = productNodes;
        int m = productsFrom.size();
        // undirected graph.
        List<Set<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new HashSet<>());
        }

        for (int i = 0; i < m; i++) {
            int from = productsFrom.get(i) - 1;
            int to = productsTo.get(i) - 1;
            graph.get(from).add(to);
            graph.get(to).add(from);
        }

        int minScore = Integer.MAX_VALUE;
        for (int u = 0; u < n; u++) {
            Set<Integer> neighborU = graph.get(u);
            if (neighborU.size() < 2) {
                continue;
            }
            for (int v : neighborU) {
                Set<Integer> neighborV = graph.get(v);
                if (neighborV.size() < 2) {
                    continue;
                }
                for (int w : neighborV) {
                    if (neighborU.contains(w)) {
                        Set<Integer> neighborW = graph.get(w);
                        int score = neighborU.size() + neighborV.size() + neighborW.size() - 6;
                        minScore = Math.min(minScore, score);
                    }
                }
            }
        }

        return minScore == Integer.MAX_VALUE ? -1 : minScore;
    }

/**
Cut off Rank: https://leetcode.com/discuss/interview-question/890290/

A group of friends at Amazon is playing a competitive video game together. During the game, each player receives a certain amount of points based on their performance. At the end of each round, players who achieve at least a cutoff rank get to "level up" their character, gaining increased abilities for them. Given the scores of the players at the end of the round, how many players will be able to level up their character?

Note that players with equal scores will have equal ranks, but the player with the next lowest score will be ranked based on the position within the list of all players' scores. For example, if there are four players, annnd three players tie for first place. their ranks would be 1,1,1, and 4. Also, no player with a score of 0 can be level up, no matter what their rank.

Write an algorithm that returns the count of players able to level up their character.

Example 1:
Input: cutOffRank = 3, num = 4, scores = [100,50,50,25]
Output: 3

Explanantion: There are num = 4 players, where the cutOffRank is 3 and scores = [100,50,50,25]. These players' ranks are [1,2,2,4]. Because the players need to have a rank of at least 3 to level up their characters, only the first three players will be able to do so. The output is 3.

Example 2:
Input: cutOffRank = 4, num = 5, scores = [2,2,3,4,5]
Output: 5

Explanation: In order, the players achieve the ranks [4,4,3,2,1]. Since the cutoffRank is 4, all 5 players will be able to level up their characters. So the output is 5.
*/

    public int cutOffRank(int cutOffRank, int num, int[] scores) {
        int rank = 1;
        int position = 1;
        Arrays.sort(scores);
        for (int i = num - 1; i >= 0; i--) {
            if (i == num - 1 || scores[i] != scores[i + 1]) {
                rank = position;
                if (rank > cutOffRank) {
                    return position - 1;
                }
            }
            position++;
        }
        return num;
    }

/**
Golf Event: 起始点可以从field的四个角选取，所以要选取不同起始点下步数最小的返回

public int flatFields (int numRows, int numColumns, List<List<Integer>> fields) {}
让小明帮公司球场修场地，给一个二维的链表fields，场地里有坑，不能走。 场地里有树要砍掉。最后目的返回是修好一层的场地的最小步数。
Ex1:
[
[1, 3, 0, 2]
[1, 1, 3, 1]
]
上图中的1代表平地，可以走。 0代表坑，不能走。 大于1的数字代表树木，需要砍掉。规则是从上下左右四个角开始任选一个开始走，先砍数字小的树木。 比如2 < 3，那么就得先走2。
上图如果从右下角开始走依次经过的坐标是： （1,3） -> (0,3) -> (1,3) -> (1,2) -> (1,1) -> (1,0) 所以返回的最小步数是5， 因为通过这个路径可以修平第二层的球场[1, 1, 3, 1]， 并且走到左下角终点。
Ex2:
[
[1, 0]
[3, 2]
]
上图中的最小步数返回-1因为，没有办法修好一层， 因为从左上角1开始走，不能走到0, 也不能走3， 因为在全局中3比2大，必须先走2。所以就没法走了。

是需要从最小的树开始砍，按照树的高度从低到高的砍，如果不能按照顺序砍就得return -1，把所有的步长加起来算出总结果返回，field里面有0不能走。

把树按照高度sort一下。然后把field当maze，高度低的树为起始点，比它高的为终止点，当成maze题来解，记下步数，全加起来就成。

把任意一个角当做起始点，用个循环把树从低到高的设置成起始和终止点就行啦。 当然，我是把四个角都当做起始点算了一遍然后返回最小的那个。如果中间发现无法先到达最低高度的树，那就得返回-1.
*/
TODO:

/**
golf event要砍树。。。每次只能砍所有树里面最矮的那颗。其实就是maze题的变形。2D-array. 0不能走，1可以走，>1 就是树，要求的输出就是从原点开始，走到每颗当前树里面最矮的那颗所需的步数+需要砍得树的高度的总和。方法我就是先找好所有的树，排好序，然后从一个点到另一个点做BFS。 找出最小步数。
举个例子 [[1,1,0,2],[3,1,1,1]], 从（0，0）走到 （0，3）--》2 这棵树，就是5步+2（树高），然后从（0，3）走到 （1，0）->3 这棵树 4步+3（树高）所以5+2+4+3返回14
*/
    class Point {
        int x;
        int y;
        int val;
        public Point(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};

    public int flatFields(List<List<Integer>> fields) {
        int m = fields.size();
        int n = fields.get(0).size();
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int val = fields.get(i).get(j);
                if (val > 1) {
                    Point p = new Point(i, j, val);
                    points.add(p);
                }
            }
        }
        // Sorted by the value
        Collections.sort(points, Comparator.comparingInt(p -> p.val));

        // start from four corners:
        int total = 0;
        int sx = 0;
        int sy = 0;
        for (Point p : points) {
            int tx = p.x;
            int ty = p.y;
            int step = bfs(fields, sx, sy, tx, ty, m, n);
            if (step == -1) {
                return -1;
            }
            total += step + p.val;
            fields.get(tx).set(ty, 1);
            sx = tx;
            sy = ty;
        }

        return total;
    }

    private int bfs(List<List<Integer>> copiedForest, int sx, int sy, int tx, int ty, int m, int n) {
        boolean[][] visited = new boolean[m][n];
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{sx, sy});

        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                int[] curr = queue.poll();
                int cx = curr[0];
                int cy = curr[1];
                if (cx == tx && cy == ty) {
                    return steps;
                }
                for (int[] dir : dirs) {
                    int x = cx + dir[0];
                    int y = cy + dir[1];
                    if (x < 0 || x >= m || y < 0 || y >= n || copiedForest.get(x).get(y) == 0 || visited[x][y]) {
                        continue;
                    }
                    visited[x][y] = true;
                    queue.offer(new int[]{x, y});
                }
            }
            steps++;
        }
        return -1;
    }

/*
Zombie in Matrix:
https://leetcode.com/discuss/interview-question/411357/

Given a 2D grid, each cell is either a zombie 1 or a human 0. 
Zombies can turn adjacent (up/down/left/right) human beings into zombies every hour. 

Find out how many hours does it take to infect all humans?

Example:

Input:
[[0, 1, 1, 0, 1],
 [0, 1, 0, 1, 0],
 [0, 0, 0, 0, 1],
 [0, 1, 0, 0, 0]]

Output: 2

Explanation:
At the end of the 1st hour, the status of the grid:
[[1, 1, 1, 1, 1],
 [1, 1, 1, 1, 1],
 [0, 1, 0, 1, 1],
 [1, 1, 1, 0, 1]]

At the end of the 2nd hour, the status of the grid:
[[1, 1, 1, 1, 1],
 [1, 1, 1, 1, 1],
 [1, 1, 1, 1, 1],
 [1, 1, 1, 1, 1]]
*/

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int minHours(int rows, int columns, int[][] grid) {
        Queue<int[]> queue = new LinkedList<>();
        int humanCnt = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (grid[i][j] == 0) {
                    humanCnt++;
                }
                else if (grid[i][j] == 1) {
                    queue.offer(new int[]{i, j});
                }
            }
        }
        int step = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] currPos = queue.poll();
                int i = currPos[0];
                int j = currPos[1];
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    if (x < 0 || x >= rows || y < 0 || y >= columns || grid[x][y] == 1) {
                        continue;
                    }
                    grid[x][y] = 1;
                    humanCnt--;
                    if (humanCnt == 0) {
                        return step+1;
                    }
                    queue.offer(new int[]{x, y});
                }
            }
            step++;
        }
        return -1;
    }

/**
Treasure Island:
https://leetcode.com/discuss/interview-question/347457

You have a map that marks the location of a treasure island. Some of the map area has jagged rocks and dangerous reefs. Other areas are safe to sail in. There are other explorers trying to find the treasure. So you must figure out a shortest route to the treasure island.

Assume the map area is a two dimensional grid, represented by a matrix of characters. You must start from the top-left corner of the map and can move one block up, down, left or right at a time. The treasure island is marked as X in a block of the matrix. X will not be at the top-left corner. Any block with dangerous rocks or reefs will be marked as D. You must not enter dangerous blocks. You cannot leave the map area. Other areas O are safe to sail in. The top-left corner is always safe. Output the minimum number of steps to get to the treasure.

Example:

Input:
[['O', 'O', 'O', 'O'],
 ['D', 'O', 'D', 'O'],
 ['O', 'O', 'O', 'O'],
 ['X', 'D', 'D', 'O']]

Output: 5
Explanation: Route is (0, 0), (0, 1), (1, 1), (2, 1), (2, 0), (3, 0) The minimum route takes 5 steps.


Follow up:
https://leetcode.com/discuss/interview-question/356150

You have a map that marks the locations of treasure islands. Some of the map area has jagged rocks and dangerous reefs. Other areas are safe to sail in. There are other explorers trying to find the treasure. So you must figure out a shortest route to one of the treasure islands.

Assume the map area is a two dimensional grid, represented by a matrix of characters. You must start from one of the starting point (marked as S) of the map and can move one block up, down, left or right at a time. The treasure island is marked as X. Any block with dangerous rocks or reefs will be marked as D. You must not enter dangerous blocks. You cannot leave the map area. Other areas O are safe to sail in. Output the minimum number of steps to get to any of the treasure islands.

Example:

Input:
[['S', 'O', 'O', 'S', 'S'],
 ['D', 'O', 'D', 'O', 'D'],
 ['O', 'O', 'O', 'O', 'X'],
 ['X', 'D', 'D', 'O', 'O'],
 ['X', 'D', 'D', 'D', 'O']]

Output: 3
Explanation:
You can start from (0,0), (0, 3) or (0, 4). The treasure locations are (2, 4) (3, 0) and (4, 0). Here the shortest route is (0, 3), (1, 3), (2, 3), (2, 4).
*/

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};

    public int shortestStepToFindTreasure(char[][] grid) {
        int m = grid.length;
        int n = grid.length;
        boolean[][] visited = new boolean[m][n];
        int step = 0;
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{0,0});
        visited[0][0] = true;

        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] currPos = queue.poll();
                int i = currPos[0];
                int j = currPos[1];
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] == 'D' ||
                            (grid[x][y] == 'O' && visited[x][y])) {
                        continue;
                    }
                    if (grid[x][y] == 'X') {
                        return step + 1;
                    }
                    visited[i][j] = true;
                    queue.offer(new int[]{x, y});
                }
            }
            step++;
        }
        return -1;
    }

    public int shortestPathToFindTreasureII(char[][] grid) {
        int m = grid.length;
        int n = grid.length;
        boolean[][] visited = new boolean[m][n];
        Queue<int[]> queue = new LinkedList<>();

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 'S') {
                    queue.offer(new int[]{i,j});
                    visited[i][j] = true;
                }
            }
        }

        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] currPos = queue.poll();
                int i = currPos[0];
                int j = currPos[1];
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] == 'D' ||
                            (grid[x][y] == 'O' && visited[x][y])) {
                        continue;
                    }
                    if (grid[x][y] == 'X') {
                        return steps + 1;
                    }
                    visited[i][j] = true;
                    queue.offer(new int[]{x, y});
                }
            }
            steps++;
        }
        return -1;
    }

/**
Given 2 lists a and b. Each element is a pair of integers where the first integer represents the unique id and the second integer represents a value. Your task is to find an element from a and an element form b such that the sum of their values is less or equal to target and as close to target as possible. Return a list of ids of selected elements. If no pair is possible, return an empty list.

Example 1:

Input:
a = [[1, 2], [2, 4], [3, 6]]
b = [[1, 2]]
target = 7

Output: [[2, 1]]

Explanation:
There are only three combinations [1, 1], [2, 1], and [3, 1], which have a total sum of 4, 6 and 8, respectively.
Since 6 is the largest sum that does not exceed 7, [2, 1] is the optimal pair.
Example 2:

Input:
a = [[1, 3], [2, 5], [3, 7], [4, 10]]
b = [[1, 2], [2, 3], [3, 4], [4, 5]]
target = 10

Output: [[2, 4], [3, 2]]

Explanation:
There are two pairs possible. Element with id = 2 from the list `a` has a value 5, and element with id = 4 from the list `b` also has a value 5.
Combined, they add up to 10. Similarily, element with id = 3 from `a` has a value 7, and element with id = 2 from `b` has a value 3.
These also add up to 10. Therefore, the optimal pairs are [2, 4] and [3, 2].
Example 3:

Input:
a = [[1, 8], [2, 7], [3, 14]]
b = [[1, 5], [2, 10], [3, 14]]
target = 20

Output: [[3, 1]]
Example 4:

Input:
a = [[1, 8], [2, 15], [3, 9]]
b = [[1, 8], [2, 11], [3, 12]]
target = 20

Output: [[1, 3], [3, 2]]
*/
   public List<int[]> optimalUtilization(List<int[]> aList, List<int[]> bList, int target) {
        // int[]: 0: id, 1: val
        // sort them by value
        Collections.sort(aList, Comparator.comparing(l -> l[1]));
        Collections.sort(bList, Comparator.comparing(l -> l[1]));
        List<int[]> res = new ArrayList<>();
        int n = aList.size();
        int m = bList.size();
        int l = 0;
        int r = m - 1;
        int minDiff = Integer.MAX_VALUE;

        while (l < n && r >= 0) {
            int id1 = aList.get(l)[0];
            int val1 = aList.get(l)[1];

            int id2 = bList.get(r)[0];
            int val2 = bList.get(r)[1];

            int sum = val1 + val2;
            int diff = target - sum;
            if (sum <= target && diff < minDiff) {
                minDiff = target-sum;
                res.clear();
                res.add(new int[]{id1, id2});
            }
            else if (diff == minDiff) {
                res.add(new int[]{id1, id2});
            }

            if (sum <= target) {
                l++;
            }
            else {
                r--;
            }
        }
        return res;
    }

/**
Articulation Point:
https://leetcode.com/discuss/interview-question/436073/

Leetcode 1192: Critical Connections in a Network

You are given an undirected connected graph. An articulation point (or cut vertex) is defined as a vertex which, when removed along with associated edges, makes the graph disconnected (or more precisely, increases the number of connected components in the graph). The task is to find all articulation points in the given graph.

Input:
The input to the function/method consists of three arguments:

numNodes, an integer representing the number of nodes in the graph.
numEdges, an integer representing the number of edges in the graph.
edges, the list of pair of integers - A, B representing an edge between the nodes A and B.
Output:
Return a list of integers representing the critical nodes.

Example:

Input: numNodes = 7, numEdges = 7, edges = [[0, 1], [0, 2], [1, 3], [2, 3], [2, 5], [5, 6], [3, 4]]

Output: [2, 3, 5]
*/
public class ArticulationPoint {
    // https://www.youtube.com/watch?v=aZXi1unBdJA
    private int id = 0;
    private int outEdgeCount = 0;
    private int[] lowLinkVals;
    private int[] ids;
    private boolean[] visited;
    private boolean[] isArt;
    private Map<Integer, Set<Integer>> graphMap;

    public List<Integer> findArticulationPoints(int numOfNodes, int[][] inputs) {
        List<Integer> res = new ArrayList<>();
        lowLinkVals = new int[numOfNodes];
        ids         = new int[numOfNodes];
        visited     = new boolean[numOfNodes];
        isArt       = new boolean[numOfNodes];

        graphMap = new HashMap<>();
        for (int[] input : inputs) {
            int from = input[0];
            int to = input[1];
            addEdge(from, to);
            addEdge(to, from);
        }

        for (int i = 0; i < numOfNodes; i++) {
            if (!visited[i]) {
                outEdgeCount = 0;
                dfs(i, i, -1);
                isArt[i] = outEdgeCount > 1;
            }
        }
        for (int i = 0; i < numOfNodes; i++) {
            if (isArt[i]) {
                res.add(i);
            }
        }
        return res;
    }

    private void addEdge(int from, int to) {
        Set<Integer> set = graphMap.getOrDefault(from, new HashSet<>());
        set.add(to);
        graphMap.put(from, set);
    }

    private void dfs(int root, int from, int parent) {
        if (parent == root) {
            outEdgeCount++;
        }
        visited[from] = true;
        id++;
        lowLinkVals[from] = id;
        ids[from] = id;

        // from each edge from node t
        if (!graphMap.containsKey(from)) {
            return;
        }
        Set<Integer> neighbors = graphMap.get(from);
        for (int to : neighbors) {
            if (to == parent) {
                continue;
            }
            if (visited[to]) {
                lowLinkVals[from] = Math.min(lowLinkVals[from], ids[to]);
            }
            else {
                dfs(root, to, from);
                lowLinkVals[from] = Math.min(lowLinkVals[from], lowLinkVals[to]);
                // Articulation point found via bridge:
                if (ids[from] < lowLinkVals[to]) {
                    isArt[from] = true;
                }
                // Articulation point found via cycle:
                if (ids[from] == lowLinkVals[to]) {
                    isArt[from] = true;
                }
            }
        }
    }
}