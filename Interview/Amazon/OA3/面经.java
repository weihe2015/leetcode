小土豆：https://wdxtub.com/interview/14520850399861.html
https://aonecode.com/amazon-online-assessment

Ex1:
一上来看两分半的宣传视频，然后5道work simulation。
亚麻设计了两个安排送货时间表的算法，1和2，又给了两个判断条件，每天每条线至少送80个货，每条线的长度小于200英里。 先问你哪个算法比较好，我只能根据这两个条件判断。然后问你需要什么额外信息，可以选几个。然后给了一个100天和3天内的交通拥堵情况， 再问哪个算法更好。
产品出现了小部分人遇到的bug，PM过来说一天之内解决，而且不用测试直接上线， 因为他认为这是小bug，问你应该怎么办。我坚持了必须测试才能上线，还选了诸如寻求senior engineer帮忙之类的。过了几周，同事又遇到相同bug，他找你帮忙，你怎么办，我选了一些“帮助他”之类的选项。最后manager说让你总结一下这次问题， 以防日后再遇到，你怎么办，我选的是写份报告总结一下。
code review的时候所有人都觉得你代码没问题， 只有一个老哥跳出来说你写的是个垃圾， 必须全部重构，我选择了找他私下谈，还有组织team meeting之类的。后来manager听说了，问你跟他解决了没有，你表示了解了那个人的想法，但不知道怎么实现， 这时候怎么办？我选了找他再商量之类的选项。
有用户的网页加载不出来，给了两份log，找问题在哪。问题出在应答中的“用户评价”和“评分”数组的元素数量不同。
external team评估了两个新feature，找出feature1的4个优势，找出feature2的两个优势，然后从中推荐一个。这时候一个senior engineer跳出来表示“你新来的你不懂，我选另一个，不服来辩”。猜测这里她一定会和你意见相反。我选择和她讨论。后来vice president说决定做你选的那个，但让你一个人在3个月之内做完，你觉得自己做不完怎么办，我选了寻求帮助之类的选项。


然后35分钟做24道logical和reasoning，全是新题，只有appointing distributor和environmental-friendly enterprise是原题。

Ex2:
刚做完OA3，5个simulation和去年一摸一样，这里有个非常好的帖子可以参考
https://www.1point3acres.com/bbs/interview/amazon-software-engineer-562078.html

Ex3:
https://www.1point3acres.com/bbs/thread-562078-1-1.html
https://www.1point3acres.com/bbs/thread-561038-1-1.html
https://www.1point3acres.com/bbs/thread-561587-1-1.html

所以关于work simulation：
这个地里有几个帖子很有用，我按照自己的记忆整理、补充一下，就不隐藏了：
1）deadline 是最重要的，有用户需求的时候requirement更重要；
2）多跟manager交流，不能自己暗搓搓做；
3）自己累点无所谓；

1) 选deadline更重要 和用户体验更重要。
2) 满足requirements。
3) 第一个选deadlline，因为这时没提出用户。后两个全用户优先。。。。。
4) 只要坚持deadline最好不要拖，自己辛苦一点无所谓，多咨询manager，找其他有经验的人合作啥的，随机应变吧

Leadership Principle:
1. Customer Obsession
    Leaders start with the customer and work backwards. They work vigorously to earn and keep customer trust. Although leaders pay attention to competitors, they obsess over customers.

2. Ownership:
    Leaders are owners. They think long term and don’t sacrifice long-term value for short-term results. They act on behalf of the entire company, beyond just their own team. They never say "that’s not my job."

3. Invent and Simplify
    Leaders expect and require innovation and invention from their teams and always find ways to simplify. They are externally aware, look for new ideas from everywhere, and are not limited by "not invented here." As we do new things, we accept that we may be misunderstood for long periods of time.

4. Are Right, A Lot
    Leaders are right a lot. They have strong judgment and good instincts. They seek diverse perspectives and work to disconfirm their beliefs.

5. Learn and Be Curious
    Leaders are never done learning and always seek to improve themselves. They are curious about new possibilities and act to explore them.

6. Hire and Develop the Best
    Leaders raise the performance bar with every hire and promotion. They recognize exceptional talent, and willingly move them throughout the organization. Leaders develop leaders and take seriously their role in coaching others. We work on behalf of our people to invent mechanisms for development like Career Choice.

7. Insist on the Highest Standards
    Leaders have relentlessly high standards — many people may think these standards are unreasonably high. Leaders are continually raising the bar and drive their teams to deliver high quality products, services, and processes. Leaders ensure that defects do not get sent down the line and that problems are fixed so they stay fixed.

8. Think Big
    Thinking small is a self-fulfilling prophecy. Leaders create and communicate a bold direction that inspires results. They think differently and look around corners for ways to serve customers.

9. Bias for Action
    Speed matters in business. Many decisions and actions are reversible and do not need extensive study. We value calculated risk taking.

10. Frugality
    Accomplish more with less. Constraints breed resourcefulness, self-sufficiency, and invention. There are no extra points for growing headcount, budget size, or fixed expense.

11. Earn Trust
    Leaders listen attentively, speak candidly, and treat others respectfully. They are vocally self-critical, even when doing so is awkward or embarrassing. Leaders do not believe their or their team’s body odor smells of perfume. They benchmark themselves and their teams against the best.

12. Dive Deep
    Leaders operate at all levels, stay connected to the details, audit frequently, and are skeptical when metrics and anecdote differ. No task is beneath them.

13. Have Backbone; Disagree and Commit
    Leaders are obligated to respectfully challenge decisions when they disagree, even when doing so is uncomfortable or exhausting. Leaders have conviction and are tenacious. They do not compromise for the sake of social cohesion. Once a decision is determined, they commit wholly.

14. Deliver Results
    Leaders focus on the key inputs for their business and deliver them with the right quality and in a timely fashion. Despite setbacks, they rise to the occasion and never settle.

应用题：
1. If northwest becomes east, northeast becomes south, and so on, what does southeast become? (West)

2. Lily can't find her home, she is 25 yards southwest of her home, then she walked 20 yards toward north, where is her home from her now? (15 yards, east)

3. 一个面朝北的朋友，先左走15m，然后一个about-turn走了30，这货在哪？15m East

4. 小明往东南走4 miles，往西走8 miles， 再往西北走4 miles。现在小明离出发点是什么方位？西 4 miles

5. 小明面朝南，往左走20miles， 再往右走 10miles， 再往左走30miles。 现在小明离出发点是什么方位？ 40 miles East

6. 南5西4南7东4北5; Ans: 南边 7miles

7.