/**
1. Amazon Go Store Turnstile:
https://leetcode.com/discuss/interview-question/798231/

Imagine a small Amazon Go store that has exactly one turnstile. It can be used by customers either as an entrance or an exit. Sometimes multiple customers want to pass through the turnstile and their directions can be different. The ith customer comes to the turnstile at time[i] and wants to either exit the store if direction[i] = 1 or enter the store if direction[i] = 0. Customers form 2 queues, one to exit and one to enter. they are ordered by the time when they came to the turnstile and, if the times are equal, by their indices.

If one customer wants to enter the store and another customer wants to exit at the same moment. There are three cases:
    1. If in the previous second the turnstile was not used (maybe it was used before, but not at the previous second), then the customer who wants to exit goes first.
    2. If in the previous second the turnstile was used as an exit, then the customer who wants to leave goes first.
    3. If in the previous second the turnstile was used as an entrance, then the customer who wants to enter goes first.

Passing through the turnstile takes 1 second.

Input:
    The function/method consists of three arguments:
     numCustomers, an integer representing the number of customers (n)
     arrTime, a list of integers where the value at index i is the time in seconds when the ith customer will come to the turnstile
     direction, a list of integers where the value at index i is the direction of the ith customer

Output:
    Return a list of integers where the value at index i is the time when the ith customer will pass the turnstile.

Constraints:
    1 <= numCustomers <= 10^5
    0 <= arrTime[i] <= arrTime[i+1] <= 10^9
    0 <= directions[j] <= 1
    0 <= j <= numCustomers - 1

Ex1:
   input:
        numCustomers = 4
        arrTime = [0,0,1,5]
        direction = [0,1,1,0]
    output:
        [2,0,1,5]

Explanation:
    At time 0, customers 0 and 1 want to pass through the turnstile. Customer 0 wants to enter the store and customer 1 wants to leave the store. The turnstile was not used in the previous second, so the priority is on the side of the customer 1.
    At time 1, customers 0 and 2 want to pass through the turnstile, Customer 2 wants to leave the store and at the previous second the turnstile was used as an exit, so the customer 2 passes through the turnstile.
    At time 2, customer 0 passes through the turnstile.
    At time 5, customer 3 passes through the turnstile

Ex2:
    input:
        numCustomers = 5
        arrTime = [0,1,1,3,3]
        direction = [0,1,0,0,1]
    output:
        [0,2,1,4,3]

Explanation:
    At time 0: customer 0 passes through the turnstile (enters)
    At time 1, customers 1 (exit) and 2 (enter) want to pass through the turnstile, and customer 2 passes through the turnstile becuase their direction is equal to the direction at the previous second.
    At time 2, customer 1 passes through the turnstile (exit)
    At time 3, customers 3 (enter) and 4 (exit) want to pass through the turnstile, Customer 4 passes through the turnstile because at the previous second the turnstile was used to exit
    At time 4, customer 3 passes through the turnstile
*/

    private int[] turnstile(int numCustomers, int[] arrTime, int[] directions) {
        int[] res = new int[numCustomers];
        // exitTurtile
        int prevDir = 1;
        int i = 0, j = 1, time = 0;
        while (j < numCustomers) {
            // i goes first
            if (arrTime[i] != arrTime[j]) {
                res[i] = time;
                prevDir = directions[i];
                i = j;
            }
            else {
                // j goes first
                if (directions[i] != prevDir) {
                    res[j] = time;
                    arrTime[i]++;
                }
                else {
                    res[i] = time;
                    arrTime[j]++;
                    i = j;
                }
            }
            j++;
            time++;
        }
        res[i] = Math.max(time, arrTime[i]);
        return res;
    }

/**
2. Amazon Server Cluster (number of islands)
Amazon has a 2D grid of servers, All servers are running a special software which is represented by a single lowercase letter. Adjacent server running the same software are organized in clusters. An adjacent server is either on the left, right, above or below a given server.

As part of the daily diagnostics, a grid of all servers is printed, where each cell contains a single lowercase letter - the software that the server in that particular cell is running. Write an algorithm to find how many clusters are in the grid currently.

Input:
    The input to the function/method consists of two arguments:
    numOfRows, an integer representing the number of rows in the grid;
    grid, a list of strings representing the 2D grid of servers.

Output:
    Return an integer reprsenting the number of clusters present in the grid.

Constraints:
    1 <= numOfRows <= 10^5
    1 <= len <= 10^5, where len is the length of each string in the grid.
    1 <= numOfRows * len <= 10^5

Note:
    grid[i][j] will consist of English alphabets 'a', 'b' and 'c' only.
Example:
    Input:
        numOfRows = 3
        grid = ["aabba", "aabba", "aaacb"]
    output:
        5

    Example:
        ['a','a','b','b','a']
        ['a','a','b','b','a']
        ['a','a','a','c','b']
*/
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numofServerCluster(int numOfRows, List<String> servers) {
        int m = numOfRows, n = servers.get(0).length();
        char[][] grid = new char[m][n];
        for (int i = 0; i < m; i++) {
            grid[i] = servers.get(i).toCharArray();
        }

        int N = m * n;
        UnionFind uf = new UnionFind(N);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int idx = i * n + j;
                char c = grid[i][j];
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    if (isInside(x, y, m, n) && grid[x][y] == c) {
                        int neighborIdx = x * n + y;
                        uf.union(idx, neighborIdx);
                    }
                }
            }
        }
        int count = 0;
        for (int i = 0; i < N; i++) {
            if (i == uf.id[i]) {
                count++;
            }
        }
        return count;
    }

    private boolean isInside(int i, int j, int m, int n) {
        return (i >= 0 && i < m && j >= 0 && j < n);
    }

    class UnionFind {
        int[] id;
        int[] size;

        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
        }
    }


/**
3. Associates Roster
An amzon Area Manager is trying to assemble a specialized team from a roaster of available associates. There is a minimum number of associates to be involved, and each associate needs to have a skill rating within a certain range. Given a list of associates' skill levels with desired upper and lower bounds, determine how many team can be created from the list

Write an algorithm to find the number of teams that can be created fulfilling the criteria.

Input:
    The input to the function/method consists of five arguments:
    1. num: an integer representing the number of associates
    2. skills, a list of integers representing the skill levels of associates;
    3. minAssociates, an integer representing the min number of team member required.
    4. minLevel, an integer representing the lower limit for skill level, inclusive;
    5. maxLevel, an integer representing the upper limit for skill level, inclusive.

Output:
    Return an integer representing the total number of teams that can be formed per the criteria

Constraints:
    1 <= num <= 20
    1 <= minAssociates <= num
    1 <= minLevel <= maxLevel <= 1000
    1 <= skills[i] <= 1000
    0 <= i < num

Input:
    num = 6
    skills = [12,4,6,13,5,10]
    minAssociates = 3
    minLevel = 4
    maxLevel = 10

Output:
    5:

Examplation:
    The list includes associates with skill level [12,4,6,13,5,10]. They want to hire at least 3 associates with skill levels between 4 and 10. Four of the associates with the following skill levels {4,6,5,10} meet the criteria. There are 5 ways to form a team of 3 associates: {4,5,6}, {4,6,10}, {4,5,10}, {5,6,10}, {4,5,6,10}

    4C4 + 4C3 = 1 + 4 = 5
*/
    import java.math.BigInteger;
    public int getNumOfTeam(int num, int[] skills, int minAssociates, int minLevel, int maxLevel)
    {
        int cnt = 0;
        for (int skill : skills) {
            if (minLevel <= skill && skill <= maxLevel) {
                cnt++;
            }
        }
        int res = 0;
        for (int i = minAssociates; i <= cnt; i++) {
            res += binomial(cnt, i).intValue();
        }
        return res;
    }

    private BigInteger binomial(final int N, final int K) {
        BigInteger ret = BigInteger.ONE;
        for (int k = 0; k < K; k++) {
            ret = ret.multiply(BigInteger.valueOf(N-k))
                    .divide(BigInteger.valueOf(k+1));
        }
        return ret;
    }


/**
4. Count ways to split strings into Prime Numbers:
https://www.geeksforgeeks.org/count-of-ways-to-split-a-given-number-into-prime-segments/
https://www.geeksforgeeks.org/find-all-possible-ways-to-split-the-given-string-into-primes/
*/

    private static final int MOD = 1000000007;
    private static boolean[] primeArray;
    private static final int maxCnt = 1000000;

    public int countPrimeString(String numText) {
        int n = numText.length();
        int[] dp = new int[n+1];
        Arrays.fill(dp, -1);
        dp[0] = 1;

        primeArray = new boolean[maxCnt];
        buildPrimeArray();

        return dfs(numText, dp, n);
    }

    private void buildPrimeArray()
    {
        Arrays.fill(primeArray, true);
        primeArray[0] = false;
        primeArray[1] = false;

        for (int i = 2; i * i <= maxCnt; i++) {
            if (primeArray[i]) {
                for (int j = i*i; j < maxCnt; j += i) {
                    primeArray[j] = false;
                }
            }
        }
    }

    private int dfs(String numText, int[] dp, int j) {
        if (dp[j] != -1) {
            return dp[j];
        }

        int cnt = 0;
        for (int i = 1; i <= 6; i++) {
            if (j - i >= 0) {
                char c = numText.charAt(j-i);
                if (c != '0') {
                    String subStr = numText.substring(j-i, j);
                    int num = Integer.parseInt(subStr);
                    if (primeArray[num]) {
                        cnt += dfs(numText, dp, j-i);
                        cnt %= MOD;
                    }
                }
            }
        }
        return dp[j] = cnt;
    }

/**
5. Distinct Product IDs after removing k
Leetcode 1481: https://leetcode.com/problems/least-number-of-unique-integers-after-k-removals/
You have been given a number of product IDs to collect from an Amazon fulfillment center. Each product ID must be found and packaged. However, you have been given too many product IDs to collect today, and so you need to trim some products from your list so that you can efficiently collect the rest before the day's end.

Given a list of product IDs of length n, you are tasked with removing m product IDs from the list so that you have the smallest distinct number of products to collect.

Write an algorithm that returns the least number of distinct product IDs remaining after m removals.

Input:
    The input to the function/method consists of three arguments:
    1. num, an integer representing the number of products in the list(n)
    2. Ids, a list of integers representing product IDs
    3. rem, an integer presenting the maximum number of removals(m)

Output:
    Return an integer representing the minimum number of product IDs remaining after required removals.

Constraints:
    1 <= num <= 10^5
    1 <= ids[i] <= 10^6
    0 <= i <= num
    1 <= rem <= 10^5

Example:
Input:
    num = 6
    ids = [1,1,1,2,3,2]
    rem = 2
Output:
    2
Explanation:
    Two possible actions that give the minimum 2 different product IDs.
    Remove 2 items with productID = 2 and the final bag will contain items with product IDs = [1,1,1,3]
    Remove 1 item with productID = 2 and 1 item with productID = 3 and the final bag will contain items with product IDs = [1,1,1,2]
    So the minimum number of distinct productIds is 2

    https://www.hackerearth.com/zh/problem/algorithm/chinmay-and-his-work-ids/description/
*/

    public int findLeastNumOfUniqueInts(int[] arr, int k) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        // occurrence:
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : arr) {
            map.put(num, map.getOrDefault(num, 0)+1);
        }
        // minQueue, sorted by occurrence
        Queue<Integer> pq = new PriorityQueue<>(Comparator.comparing(map::get));
        pq.addAll(map.keySet());
        while (k > 0) {
            int num = pq.poll();
            k -= map.get(num);
        }
        if (k < 0) {
            return pq.size() + 1;
        }
        else {
            return pq.size();
        }
    }


/**
6. Maximum Units:
https://leetcode.com/discuss/interview-question/793606/Amazon-OA-Question/
An Amazon warehouse mamanger needs to create a shipment to fill a truck. All the products in the warehouse are in boxes of the same size. Each product is packed in some number of units per box.

Given the number of the boxes the truck can hold, write an algorithm to determine the maximum number of units of any mix of products that can be shipped.

Input:
    The input to the function/method consists of five arguments
    1. num: an integer representing number of products.
    2. boxes, a list of integers representing the number of available boxes for products.
    3. unitSize, an integer representing size of unitsPerBox.
    4. unitsPerBox, a list of integers representing the number of units packed in each box.
    5. truckSize: an integer representing the number of boxes the truck can carry.

Output:
    Return an integer representing the maximum units that can be carried by the truck

Constraints:
    1 <= |boxes| <= 10^5
    |boxes| == |unitsPerBox|
    1 <= boxes[i] <= 10^7
    1 <= i < |boxes|
    1 <= unitesPerBox[j] <= 10^5
    1 <= j < |unitsPerBox|
    1 <= truckSize <= 10^8

Example:
    Input. num = 3, boxes = [1,2,3], unitSize = 3, unitsPerBox = [3,2,1], truckSize = 3
    Output: 7

Explanation:
    Product 0: 1 box with 3 units
    Product 1: 2 boxes with 2 units each
    Product 2: 3 Boxes with 1 unit each

    Maximum number of units that can be shipped = 3 + 2 + 2 = 7
*/

    class Box {
        int numOfBox;
        int size;

        public Box(int numOfBox, int size) {
            this.numOfBox = numOfBox;
            this.size = size;
        }
    }

    public long getMaxUnit(int num, ArrayList<Integer> boxes, int unitSize, ArrayList<Integer> unitsPerBox, long truckSize) {
        // sort boxes by their size:
        Queue<Box> pq = new PriorityQueue<>((b1, b2) -> (Integer.compare(b2.size, b1.size)));
        for (int i = 0, max = boxes.size(); i < max; i++) {
            pq.offer(new Box(boxes.get(i), unitsPerBox.get(i)));
        }
        long res = 0;
        while (!pq.isEmpty() && truckSize > 0) {
            Box currBox = pq.poll();
            long numOfBox = Math.min(truckSize, currBox.numOfBox);
            res += numOfBox * currBox.size;
            truckSize -= numOfBox;
        }
        return res;
    }

/**
import heapq

def maxUnits(num, boxes, unitSize, unitsPerBox, truckSize):
    heap = []

    for i in range(len(boxes)):
        units_per_box = unitsPerBox[i]
        heapq.heappush(heap, (-units_per_box, boxes[i]))

    ret = 0

    while truckSize > 0 and heap:
        curr_max = heapq.heappop(heap)
        max_boxes = min(truckSize, curr_max[1])
        truckSize -= max_boxes
        ret += max_boxes * (curr_max[0] * -1)

    return ret


# test cases
print(maxUnits(3, [1,2,3], 3, [3,2,1], 3))
print(maxUnits(3, [2,5,3], 3, [3,2,1], 50))
*/

/**
7. Movies on Flight | IDs of two songs
https://leetcode.com/discuss/interview-question/313719/Amazon-or-Online-Assessment-2019-or-Movies-on-Flight

https://gist.github.com/yitonghe00/872075380b0e4c7dee4f95c6f4b368b8
https://leetcode.com/playground/vJzk7SbB

You are on a flight and want to watch two movies during this flight.
You are given List<Integer> movieDurations which includes all the movie durations.
You are also given the duration of the flight which is d in minutes.
Now, you need to pick two movies and the total duration of the two movies is less than or equal to (d - 30min).

Find the pair of movies with the longest total duration and return they indexes. If multiple found, return the pair with the longest movie.

Example 1:

Input: movieDurations = [90, 85, 75, 60, 120, 150, 125], d = 250
Output: [0, 6]
Explanation: movieDurations[0] + movieDurations[6] = 90 + 125 = 215 is the maximum number within 220 (250min - 30min)
*/
    class Movie {
        int idx;
        int len;
        public Movie(int idx, int len) {
            this.idx = idx;
            this.len = len;
        }
    }

    public int[] getMaxLengOfMovies(int d, int[] movies) {
        int[] res = new int[2];
        d -= 30;
        List<Movie> list = new ArrayList<>();
        for (int i = 0, n = movies.length; i < n; i++) {
            int movieLen = movies[i];
            list.add(new Movie(i, movieLen));
        }
        Collections.sort(list, Comparator.comparingInt(m -> m.len));

        // two points:
        int l = 0, r = list.size() - 1;
        int maxLen = 0;
        int idx1 = -1, idx2 = -1;
        while (l < r) {
            Movie m1 = list.get(l);
            Movie m2 = list.get(r);
            int sum = m1.len + m2.len;
            if (sum > d) {
                r--;
            }
            else {
                if (maxLen < sum) {
                    maxLen = sum;
                    idx1 = m1.idx;
                    idx2 = m2.idx;
                }
                l++;
            }
        }
        res[0] = idx1;
        res[1] = idx2;
        return res;
    }


/**
Leetcode 239 Sliding Window Maximum:
8. Maximum Available Disk Space in Segment Minima (变种 Sliding Window Maximum)

Amazon is performing an analysis on the computers at one of its offices. The computers spaced along a single row. The analysis is performed in the following way:
Choose a contiguous segment of a certain number of computers, starting from the begining of the row. Analyze the available hard disk space on each of the computers. Determine the minimum available disk space within this segment. After performing the steps for the first segment, it is then repeated for the next segment, continuing this procedure until the end of the row, (i.e. if the semgent size is 4, computers 1 to 4 would be analyzed, then 2 to 5, etc)

Given this analysis prodcedure, write an algorithm to find the maximum available disk space among all the minima that are found during the analysis.

Input:
    The input to the function/method consists of three arguments:
    numComputer, an integer representing the number of computers.
    hardDiskSpace, a list of integers representing the hard disk space of the computers.
    segmentLength, an integer representing the length of contiguous segment of the computers to be consider in each iterations.

Output:
    Return an integer representing the maximum available disk space among all the minima that are found during the analysis.

Constraints:
    1 <= numComputer <= 10^6
    1 <= segmentLength <= numComputer
    1 <= hardDiskSpace[i] <= 10^9

Example:
    Input:
        numComputer = 3
        hardDiskSpace = [8,2,4]
        segmentLength = 2
    Output:
        2

Explanation:
    In this array of computers, the subarrays of size 2 are [8,2] and [2,4]. Thus the initial analysis return 2 and 2 because those are the minima for the segments. Finally, the maximum of these values is 2
    Therefore, the answer is 2.
*/

    class MinQueue {
        Deque<Integer> queue = new ArrayDeque<>();

        // Increasing Monotonic queue, the min item always on the leftmost
        // push only takes O(K), where k is the size of the sliding window.

        public void push(int num) {
            while (!queue.isEmpty() && queue.peekLast() > num) {
                queue.pollLast();
            }
            queue.offerLast(num);
        }

        public int getFront() {
            return queue.peekFirst();
        }

        public void popMinIfExists(int num) {
            if (num != queue.peekFirst()) {
                return;
            }
            queue.pollFirst();
        }
    }

    public int maxSlidingWindow(int[] nums, int n, int k) {
        int maxVal = Integer.MIN_VALUE;
        MinQueue mq = new MinQueue();
        for (int i = 0; i < n; i++) {
            // 1 to k-2 items
            int num = nums[i];
            mq.push(num);
            if (i >= k-1) {
                int val = mq.getFront();
                maxVal = Math.max(maxVal, val);
                int leftItem = nums[i-k+1];
                mq.popMinIfExists(leftItem);
            }
        }
        return maxVal;
    }



/**
9. Maximum Profit for selling Amazon Basics Product:
Amazon Basics has several suppliers for its products. For each of the products, the stock is represented by a list of a number items for each each supplier. As items are purchased, the supplier raises the price by 1 per item purchased. Let's assume Amazon profit on any single item is the same as the number of items the supplier has left. For example, if a supplier has 4 items, Amazon profit on the first item sold is 4, then 3, then 2 and the profit of the last one is 1

Given a list where each value in the list is the number of item at a given supplier and also given the number of items to be ordered. Write an algorithm to find the highest profit that can be generated for the given product.

Input:
    Three arguments:
    1. numSuppliers, an integer representng the number of suppliers.
    2. inventory, a list of long integers representing the value of the item at given supplier;
    3. order, a long integer representing the number of items to be ordered.

Output:
    Return a long integer representing the hights profit that can be generated for the given product.

Constraints:
    1 <= numSuppliers <= 10^5
    1 <= inventory[i] <= 10^5
    0 <= i < numSuppliers
    1 <= order <= sum of inventory

Example:
    Input:
        numSuppliers = 2
        inventory = [3,5]
        order = 6
    Output:
        19:
Explanation:
    There are two suppliers, one with inventory 3 and the other with inventory 5, and 6 items were ordered.
    The maximum profit is maded by selling 1 for 5, 1 for 4 and 2 at 3 and 2 at 2 units profit. The two suppliers are left with a unit of product each.
    The maximum profit generated is 5 + 4 + 2*3 + 2*2 = 19

    Prices = [5,4,3,2,1]
    Counts = [1,2,4,6]

    https://leetcode.com/playground/MYZqdy95
*/

    public int getMaxProfitForProducts(int numSuppiers, int[] inventories, int order) {
        Queue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());

        for (int item : inventories) {
            if (item > 0){
                pq.offer(item);
            }
        }
        int profit = 0;

        while (!pq.isEmpty() && order > 0) {
            int curr = pq.poll();
            profit += curr;
            curr -= 1;
            pq.offer(curr);
            order -= 1;
        }
        return profit;
    }


/***
https://leetcode.com/discuss/interview-question/383669/
10. Max of Min Altitudes:
Similar Question: Leetcode 1102: https://leetcode.com/problems/path-with-maximum-minimum-value

Given a matrix with r rows and c columns, find the maximum score of a path starting at [0, 0] and ending at [r-1, c-1]. The score of a path is the minimum value in that path. For example, the score of the path 8 → 4 → 5 → 9 is 4.

Don't include the first or final entry. You can only move either down or right at any point in time.

Example 1:

Input:
[[5, 1],
 [4, 5]]

Output: 4
Explanation:
Possible paths:
5 → 1 → 5 => min value is 1
5 → 4 → 5 => min value is 4
Return the max value among minimum values => max(4, 1) = 4.
Example 2:

Input:
[[1, 2, 3]
 [4, 5, 1]]

Output: 4
Explanation:
Possible paths:
1-> 2 -> 3 -> 1
1-> 2 -> 5 -> 1
1-> 4 -> 5 -> 1
So min of all the paths = [2, 2, 4]. Note that we don't include the first and final entry.
Return the max of that, so 4.
Related problems:

https://leetcode.com/problems/minimum-path-sum/
https://leetcode.com/problems/unique-paths-ii/
https://leetcode.com/problems/path-with-maximum-minimum-value (premium) is a different problem. In this problem we can only move in 2 directions.
*/
    public int maxOfMinAltitudes(int[][] grid)
    {
        int m = grid.length, n = grid[0].length;
        int[][] dp = new int[m][n];
        // first entry is not consider
        dp[0][0] = Integer.MAX_VALUE;
        for (int i = 1; i < m; i++) {
            dp[i][0] = Math.min(dp[i-1][0], grid[i][0]);
        }
        for (int j = 1; j < n; j++) {
            dp[0][j] = Math.min(dp[0][j-1], grid[0][j]);
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                // last entry is not consider:
                if (i == m-1 && j == n-1) {
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }
                else {
                    // left item:
                    int score1 = Math.min(dp[i][j-1], grid[i][j]);
                    int score2 = Math.min(dp[i-1][j], grid[i][j]);
                    dp[i][j] = Math.max(score1, score2);
                }
            }
        }
        return dp[m-1][n-1];
    }

    public int maxOfMinAltitudes2(int[][] grid)
    {
        int m = grid.length, n = grid[0].length;
        int[] dp = new int[n];
        // first entry is not consider
        dp[0] = Integer.MAX_VALUE;
        for (int j = 1; j < n; j++) {
            dp[j] = Math.min(dp[j-1], grid[0][j]);
        }

        for (int i = 1; i < m; i++) {
            // update the first element in each row
            dp[0] = Math.min(dp[0], grid[i][0]);
            for (int j = 1; j < n; j++) {
                // last entry is not consider:
                if (i == m-1 && j == n-1) {
                    dp[j] = Math.max(dp[j], dp[j-1]);
                }
                else {
                    // left item:
                    int score1 = Math.min(dp[j-1], grid[i][j]);
                    int score2 = Math.min(dp[j], grid[i][j]);
                    dp[j] = Math.max(score1, score2);
                }
            }
        }
        return dp[n-1];
    }


    public int maxOfMinAltitudes(int columnCount, int rowCount,
                                 int[][] mat)
    {
        // WRITE YOUR CODE HERE
        if (mat == null || mat.length == 0 || mat[0].length == 0) {
            return -1;
        }
        if (mat.length != rowCount || mat[0].length != columnCount) {
            return -1;
        }
        int m = mat.length, n = mat[0].length;
        int[] dp = new int[n];
        dp[0] = mat[0][0];
        for (int j = 1; j < n; j++) {
            dp[j] = Math.min(dp[j-1], mat[0][j]);
        }

        for (int i = 1; i < m; i++) {
            dp[0] = Math.min(dp[0], mat[i][0]);
            for (int j = 1; j < n; j++) {
                int score1 = Math.min(dp[j-1], mat[i][j]);
                int score2 = Math.min(dp[j], mat[i][j]);
                dp[j] = Math.max(score1, score2);
            }
        }
        return dp[n-1];
    }

/*
11. Min Cost to Connect All Servers:
https://assets.leetcode.com/users/images/7d59b05e-e62c-41b2-b11d-e86e7abd85e0_1597640779.4292634.png
Your team at Amazon is overseeing the design of a new, hgih-efficiency data center at HQ2. A power grid needs to be generated for supplying power to N servers. All servers in the grid have to be connected such that they have access to power. The cost of connections between different servers varies.

Assume that there are no ties, the names of servers are unique, connecitons are directionless, there is at most one connection between a pair of servers, all costs are greater that zero, and a server does not connect to itself.

Write an algorithm to minimize the cost of connecting all servers in the power grid.

Input:
    The input of the function/methods consists of two arguments:
    1. num, an integer representing the number of connections.
    2. connection, representing a list of Connections where each element of the list consists of two servers and the cost of connection between the servers.

Output:
    Return a list of Connections where each element of the list consists of two servers and the const of connection between the servers such that all servers are connected at the lowest total cost. If no such Connections exists, then return a list with empty Connection.

Note:
    The cost of connection between the servers is always greater than 0.

Example:
    Input:
        num = 5
        connection =
        [[A,B,1],
         [B,C,4]
         [B,D,6],
         [D,E,5],
         [C,E,1]]

    Output:
        [[A,B,1],
        [B,C,4],
        [C,E,1],
        [D,E,5]]
Explanation:
   1     4
A --- B --- C
      |     |
    6 |     | 1
      |     |
      D --- E
         5
By connecting A to B, B to C, C to E, and E to D, the cost of connection is minimum.
So Output:
        [[A,B,1],
        [B,C,4],
        [C,E,1],
        [D,E,5]]
*/

    class Connection {
        char firstTown;
        char secondTown;
        int cost;

        public Connection(char firstTown, char secondTown, int cost) {
            this.firstTown = firstTown;
            this.secondTown = secondTown;
            this.cost = cost;
        }
    }

    class UnionFind {
        int[] id;
        int[] size;
        int count;

        public UnionFind(int N) {
            this.id = new int[N+1];
            this.size = new int[N+1];
            for (int i = 0; i <= N; i++) {
                this.id[i] = i;
                this.size[i] = 1;
            }
            this.count = N;
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public boolean isConnected(int p, int q) {
            return find(p) == find(q);
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            this.count--;
        }
    }

    public List<Connection> findMinimumCostCities(int num, List<Connection> connections) {
        boolean[] visited = new boolean[num];
        UnionFind uf = new UnionFind(num);
        List<Connection> res = new ArrayList<>();
        Collections.sort(connections, (c1, c2) -> (c1.cost - c2.cost));
        int cost = 0;
        for (int i = 0; i < num; i++) {
            Connection conn = connections.get(i);
            char city1 = conn.firstTown;
            char city2 = conn.secondTown;
            int city1Num = city1 - 'A' + 1;
            int city2Num = city2 - 'A' + 1;
            if (!uf.isConnected(city1Num, city2Num)) {
                uf.union(city1Num, city2Num);
                cost += conn.cost;
                visited[i] = true;
            }
            if (uf.count == 1) {
                for (int j = 0; j < num; j++) {
                    if (visited[j]) {
                        res.add(connections.get(j));
                    }
                }
                return res;
            }
        }
        return new ArrayList<>();
    }

/**
12. Nearest cities that share X or Y:
Amazon has Fulfillment Centers in multiple cities within a large geographic region. The cities are arranged on a graph that has been divided up like an ordinary Cartesian plane. Each city is located at an integral (x,y) coordinate intersection. City names and locations are given in the form of three arrays: c, x, and y, which are aligned by the index to provide the city name (c[i]), and its coordinates, (x[i], y[i])

Write an algorithm to determine the name of the neartest city that shares either an x or a y coordinate with the queried city. If no other cities share an x or y coordinate, return NONE. If two cities have the same distance to the queried city, q[i], consider the one with an alphabetically smaller name (i.e. 'ab' < 'aba' < 'abb') as the closest choice.

The distance is denoted on a Euclidean plane: the difference in x plus the difference in y.

Input:
    The input to the function/method consists of six arguments.
    1. numOfCities, an integer representing the number of cities.
    2. cities, a list of strings represeting the names of each city[i]
    3. xCoordinates, a list of integers representing the X coordinates of each city[i]
    4. yCoordinates, a list of integers representing the Y coordinates of each city[i]
    5. numOfQueries, an integer representing the number of queries
    6. Queries: a list of strings representing the names of the queried cities.

Output:
    Return a list of strings representing the name of the nearest city that shares either an x or a y coordinate with the queried city.

Constraints:
    1 <= numOfCities, numOfQueries <= 10^5
    1 <= xCoordinates[i], yCoordinates[i] <= 10^9
    1 <= length of queries[i] and cities[i] <= 10

Note:
    Each character of all c[i] and q[i] is in the range ascii [a-z, 0-9,-]
    All city name values, c[i], are unique. Al cities have unique coordinates.

Example:
    Input:
        numOfCities = 3
        cities = ["c1", "c2", "c3"]
        xCoordinates = [3,2,1]
        yCoordinates = [3,2,3]
        numOfQueries = 3
        queries = ["c1", "c2", "c3"]
    Output:
        ["c3", "NONE", "c1"]

对x， y分别排序放进两个list。
*/

    class City {
        String name;
        int x;
        int y;
        public City(String name, int x, int y) {
            this.name = name;
            this.x = x;
            this.y = y;
        }
    }

    public List<String> getCitiesSharedXOrY(int numOfCities, String[] cities, int[] xCoordinates,
                                            int[] yCoordinates, int numOfQueries, String[] queries)
    {
        List<String> res = new ArrayList<>();
        Map<Integer, Queue<String>> xMap = new LinkedHashMap<>();
        Map<Integer, Queue<String>> yMap = new LinkedHashMap<>();
        Map<String, City> map = new LinkedHashMap<>();
        for (int i = 0; i < numOfCities; i++) {
            String name = cities[i];
            int x = xCoordinates[i];
            Queue<String> xpq = xMap.getOrDefault(x, new PriorityQueue<>());
            xpq.offer(name);
            xMap.put(x, xpq);

            int y = yCoordinates[i];
            Queue<String> ypq = yMap.getOrDefault(y, new PriorityQueue<>());
            ypq.offer(name);
            yMap.put(y, ypq);

            City city = new City(name, x, y);
            map.put(name, city);
        }

        for (int i = 0; i < numOfQueries; i++) {
            String query = queries[i];
            City city = map.get(query);
            Queue<String> xList = xMap.get(city.x);
            boolean found = false;
            for (String name : xList) {
                if (!name.equals(query)) {
                    found = true;
                    res.add(name);
                    break;
                }
            }
            if (!found) {
                Queue<String> yList = yMap.get(city.y);
                for (String name : yList) {
                    if (!name.equals(query)) {
                        found = true;
                        res.add(name);
                        break;
                    }
                }
            }
            if (!found) {
                res.add("NONE");
            }
        }
        return res;
    }

/**
13: Shortest Distance Between Two Robots:
https://www.youtube.com/watch?v=0W_m46Q4qMc
finding the closest pair of points in plane

The year is 2125 and Amazon has built a galactic scale warehouse on the edge of the Orion Spur of the Milky Way Galaxy. The warehouse is a single story space station measuring 1E9 meters in length and width. The warehouse runs a fleet of up to 10,000 autonomous robots which magnetically attach to the floor of the station, eliminating the need for artificial gravity. Amazon has asked you to find the squared distance between the two closest robots.

Given the position of n robots in the warehourse, write an algorithm to find the squared shortest distance between them.

Input:
    The input to the function/method consists of three arguments
    1. numOfRobots, an integer representing the number of robots (n)
    2. positionX, a list of integers representing the x coordinates of the robot's position, in meters
    2. positionY, a list of integers representing the xycoordinates of the robot's position, in meters

Output:
    Return an integer representing the squared shortest distance between the pairs of robots.

Constraints:
    2 <= numRobots <= 10^5
    0 <= positionX[i], positionY[i] < 10^9
    0 <= i < numRobots

Note:
    The squared distance between a pair of robots with xy coordiantes of positions (x1, y1) and (x2, y2) is calculated using the formula (x1-x2)^2 + (y1-y2)^2
    If the squared distance is 0 between a pair of robots then the robots are present at the same position and the distance will not be considered.

Example:
    Input:
        numRobots = 3
        positionX = [0,1,2]
        positionY = [0,1,4]

    Output: 2

Explanation:
    There are 3 robots with positions of x coordinates = [0,1,2] and y coordinates = [0,1,4]. The robots have the xy coordinates of positions of (0,0), (1,1) and (2,4). The closest robots are (0,0), and (1,1) and closest squared Euclidean distance is (1-0)^2 + (1-0)^2 = 2

*/

    class Point {
        int x;
        int y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private int min(int val1, int val2) {
        return val1 < val2 ? val1 : val2;
    }

    private int distance(Point p1, Point p2) {
        int x = p1.x - p2.x;
        int y = p1.y - p2.y;
        return (x * x + y * y);
    }

    private int bruteForce(List<Point> points, int n) {
        int minVal = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                int dist = distance(points.get(i), points.get(j));
                minVal = Math.min(minVal, dist);
            }
        }
        return minVal;
    }

    public int shortestDistance(int numRobots, int[] positionX, int[] positionY) {
        Set<String> set = new HashSet<>();
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < numRobots; i++) {
            int x = positionX[i];
            int y = positionY[i];
            String key = x + String.valueOf(y);
            if (!set.contains(key)) {
                points.add(new Point(x, y));
            }
            set.add(key);
        }

        Collections.sort(points, Comparator.comparingInt(p -> p.x));

        return dfs(points, points.size());
    }

    private int dfs(List<Point> points, int n) {
        if (n <= 3) {
            return bruteForce(points, n);
        }

        int mid = n / 2;
        Point midPoint = points.get(mid);

        List<Point> leftPoints = new ArrayList<>();
        for (int i = 0; i <= mid; i++) {
            leftPoints.add(points.get(i));
        }
        List<Point> rightPoints = new ArrayList<>();
        for (int i = mid; i < n; i++) {
            rightPoints.add(points.get(i));
        }
        int leftDist = dfs(leftPoints, mid);
        int rightDist = dfs(rightPoints, n-mid);

        int minDist = min(leftDist, rightDist);

        List<Point> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int absDist = Math.abs(points.get(i).x - midPoint.x);
            if (absDist < minDist) {
                list.add(points.get(i));
            }
        }

        int closest = getCloestPoint(list, list.size(), minDist);
        return min(minDist, closest);
    }

    private int getCloestPoint(List<Point> points, int n, int minDist) {
        int min = minDist;
        Collections.sort(points, Comparator.comparingInt(p -> p.y));

        for (int i = 0; i < n; i++) {
            int j = i + 1;
            while (j < n && (points.get(j).y - points.get(i).y < min)) {
                min = distance(points.get(i), points.get(j));
                j += 1;
            }
        }
        return min;
    }

/**
14. Sort Website Items in Page
https://instant.1point3acres.com/thread/661685

Amazon's website contains one to many items in each page. To minic the logic of the website, an Amazon programmer has a list of items and each item has its name, relevance and price. After sorting the items by (name: 0, relevance: 1, price 2), the programmer is trying to find out a list of items displayed in a chosen page.

Given a list of items, the sort column, the sort order (0: ascending, 1: descending), the number of items to be displayed in each page and a page number, write an algorithm to determine the list of item names in the specified page while respecting the item's order (Page number starts at 0).

Input:
    The input to the function/method consists of six arguments
    1. numOfItems, an integer representing the number of items.
    2. items, a map of string as key representing the name and pair of integers as values representing the relevance, price
    3. sortParameter, an integer representing the value used for sorting (0 for name, 1 for relevance)
    4. sortedOrder, an integer representing the order of sorting (0 for ascending order and 1 descending order)
    5. ItemsPerPage:
    6. pageNumber:

Output:
    Return a list of strings representing the item names on the requested page in the order they are displayed.

Constraints
    1 <= numOfItems < 10^5
    0 <= relevance, price < 10^8
    0 <= pageNumber < 10

Note:
    itemsPerPage is always greater than 0, and is always less than the minimum of numOfItems and 20.

Example:
    Input:
        numOfItems = 3
        items = [["item1",10,15], ["item2",3,4], ["item3",17,8]]
        sortParameter = 1
        sortOrder = 0
        itemsPerPage = 2
        pageNumber = 1

    Output:
        ["item3"]

Explanation:
    There are 3 items:
        Sort them by relevance(sortParameter = 1) in ascending order (items = [["item2",3,4],["item1",10,15],["item3",17,8])
        Display up to 2 items on each page.
        The page 0 contains 2 item names ["item2", "item2"] and page 1 contains only 1 item name ["item3"]
    So the output is "item3"

今天刚做了，改回文+sort item。sort item comparator
*/
    class Item {
        String name;
        int relevance;
        int price;
        public Item(String name, int relevance, int price)
        {
            this.name = name;
            this.relevance = relevance;
            this.price = price;
        }
    }

    public List<String> sortWebsitItemsInPage(int numOfItems, Map<String, int[]> items, int sortParameter,
                                              int sortOrder, int itemsPerPage, int pageNumber)
    {
        List<String> result = new ArrayList<>();
        List<Item> list = new ArrayList<>();
        for (String name : items.keySet()) {
            int[] val = items.get(name);
            list.add(new Item(name, val[0], val[1]));
        }

        ItemComparator cmp = new ItemComparator(sortParameter, sortOrder);
        Collections.sort(list, cmp);

        int n = pageNumber * itemsPerPage;
        for (int i = n; i < numOfItems; i++) {
            result.add(list.get(i).name);
        }
        return result;
    }

    class ItemComparator implements Comparator<Item> {
        int sortedParameter;
        int sortedOrder;

        public ItemComparator(int sortedParameter, int sortedOrder) {
            this.sortedParameter = sortedParameter;
            this.sortedOrder = sortedOrder;
        }
        public int compare(Item item1, Item item2) {
            if (sortedParameter == 0) {
                if (sortedOrder == 0) {
                    return item1.name.compareTo(item2.name);
                }
                else {
                    return item2.name.compareTo(item1.name);
                }
            }
            else if (sortedParameter == 1) {
                if (sortedOrder == 0) {
                    return Integer.compare(item1.relevance, item2.relevance);
                }
                else {
                    return Integer.compare(item2.relevance, item1.relevance);
                }
            }
            else if (sortedParameter == 2) {
                if (sortedOrder == 0) {
                    return Integer.compare(item1.price, item2.price);
                }
                else {
                    return Integer.compare(item2.price, item1.price);
                }
            }
            return 0;
        }
    }


/**
15. SubTree With maximum Average:
https://leetcode.com/discuss/interview-question/349617

https://leetcode.com/problems/maximum-average-subtree

1.
https://assets.leetcode.com/users/images/d53ce057-6346-4550-aa5b-52738d266a86_1597640779.6681678.png
2.
https://assets.leetcode.com/users/images/0dd6c9d6-f4c7-4c5c-b606-77706422d3a1_1597640779.4719155.png

Tree:
Imagine that an employment tree represents the formal employee hierarchy at Amazon. Manager nodes have child nodes for each employee that reports to them; each of these employees can, in turn, have child nodes representing their respective reportees. Each node in the tree contains an integer representing the number of months the employee has spent at the company. Team tenure is computed as the average tenure of the manager and all the company employees working below the manager. The oldes team has the highest team tenure.

Write an algorithm to find the manager of the team with the highest tenure. An employee must have child nodes to be a manager.

Input:
    The input to the function/method consists of an argument
    president, a node representing the root node of the employee hierachy.

Output:
    Return the node which has the oldest team.

Note:
    There will be at least one child node in the tree and there will be no ties.

Example:
    Input:
    president:
           20
         /    \
        12      18
     / / \    /   \
    11 2 3   15   8

    output:
    18:
Explanation:
    There are three managers in this tree with the following team tenures:
    12 => (11+2+3+12) / 4 = 7
    18 => (18+15+8) / 3 = 13.67
    20 => (12+11+2+3+18+15+8+20)/8 = 11.125

    The oldest team is the team of the maanger with 18 months at the company. So the output is 18
*/
    class Node {
        int val;
        List<Node> children;

        public Node(int val) {
            this.val = val;
            this.children = new ArrayList<>();
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    class Content {
        Node node;
        int sum;
        int cnt;

        public Content(Node node, int sum, int cnt)
        {
            this.node = node;
            this.sum = sum;
            this.cnt = cnt;
        }
    }

    private Content resCont;

    private Node findSubtree(Node root) {
        if (root == null) {
            return null;
        }
        findsubtreeWithMaxAverage(root);
        return resCont.node;
    }

    private Content findsubtreeWithMaxAverage(Node root) {
        if (root == null) {
            return new Content(root, 0, 0);
        }

        int sum = root.val;
        int cnt = 1;
        for (Node child : root.children)  {
            Content subCont = findsubtreeWithMaxAverage(child);
            sum += subCont.sum;
            cnt += subCont.cnt;
        }
        Content currCont = new Content(root, sum, cnt);
        if (isLeaf(root)) {
            return currCont;
        }
        if (resCont == null || sum * resCont.cnt > resCont.sum * cnt) {
            resCont = currCont;
        }
        return currCont;
    }

    private boolean isLeaf(Node root) {
        return root.children.isEmpty();
    }