# A divide and conquer program in Python3
# to find the smallest distance from a
# given set of points.
import math

# A class to represent a Point in 2D plane
class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

# A utility function to find the
# distance between two points
def dist(p1, p2):
    x = p1.x - p2.x
    y = p1.y - p2.y
    return x * x + y * y

# A Brute Force method to return the
# smallest distance between two points
# in P[] of size n
def bruteForce(P, n):
    minVal = float('inf')
    for i in range(n):
        for j in range(i + 1, n):
            d = dist(P[i], P[j])
            if d < minVal:
                minVal = d
    return minVal

# A utility function to find the
# distance beween the closest points of
# strip of given size. All points in
# strip[] are sorted accordint to
# y coordinate. They all have an upper
# bound on minimum distance as d.
# Note that this method seems to be
# a O(n^2) method, but it's a O(n)
# method as the inner loop runs at most 6 times
def listClosest(points, size, d):

    # Initialize the minimum distance as d
    minVal = d

    points.sort(key = lambda point: point.y)

    # Pick all points one by one and
    # try the next points till the difference
    # between y coordinates is smaller than d.
    # This is a proven fact that this loop
    # runs at most 6 times
    for i in range(size):
        j = i + 1
        while j < size and (points[j].y - points[i].y) < minVal:
            minVal = dist(points[i], points[j])
            j += 1

    return minVal

# A recursive function to find the
# smallest distance. The array P contains
# all points sorted according to x coordinate
def getClosetOfTwoPoints(P, n):

    # If there are 2 or 3 points,
    # then use brute force
    if n <= 3:
        return bruteForce(P, n)

    # Find the middle point
    mid = n // 2
    midPoint = P[mid]

    # Consider the vertical line passing
    # through the middle point calculate
    # the smallest distance dl on left
    # of middle point and dr on right side
    dl = getClosetOfTwoPoints(P[:mid], mid)
    dr = getClosetOfTwoPoints(P[mid:], n - mid)

    # Find the smaller of two distances
    d = min(dl, dr)

    # Build an array strip[] that contains
    # points close (closer than d)
    # to the line passing through the middle point
    points = []
    for i in range(n):
        if abs(P[i].x - midPoint.x) < d:
            points.append(P[i])

    # Find the closest points in strip.
    # Return the minimum of d and closest
    # distance is strip[]
    d1 = listClosest(points, len(points), d)
    return min(d, d1)

# The main function that finds
# the smallest distance.
# This method mainly uses closestUtil()
def closest(P, n):
    P.sort(key = lambda point: point.x)

    # Use recursive function closestUtil()
    # to find the smallest distance
    return getClosetOfTwoPoints(P, n)

# Driver code
# P = [Point(2, 3), Point(12, 30),
#      Point(40, 50), Point(5, 1),
#      Point(12, 10), Point(3, 4)]
P = [Point(0,0), Point(1,1), Point(2,4)]
n = len(P)
print("The smallest distance is", closest(P, n))