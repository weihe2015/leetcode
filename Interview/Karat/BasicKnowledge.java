/**
1. Web Application: Building user interfaces for web services and sites
2. Production issues: Diagnosing problems with production services
3. System internals: How computer mannage and allocate resources like CPU and memory
4. OOP: best practices for building classes and systems of classes
5. Testing: Validating code and related concerns

System internals:
1 Graph(CPU usage, RAM usage), 大概就是随着time，CPU资源消耗不变, increase; How to detect; What could cause that; How to resolve?
maybe memory leak, try to find
2 Memory Leak, how to solve?

内存泄漏, memory leak:
内存泄漏：对象已经没有被应用程序使用，但是垃圾回收器没办法移除它们，因为还在被引用着。
在Java中，内存泄漏就是存在一些被分配的对象，这些对象有下面两个特点，首先，这些对象是可达的，即在有向图中，存在通路可以与其相连；其次，这些对象是无用的，即程序以后不会再使用这些对象。如果对象满足这两个条件，这些对象就可以判定为Java中的内存泄漏，这些对象不会被GC所回收，然而它却占用内存。

Memory Leak: some objects are referenced, but not used
    Symptoms of a Memory leak: Works fast at first, but slows over time; OutOfMemoryError after runing; There are occasionally crashes in the applications.
    Why
       1. Memory Leak Through static Fields
       2. Through Unclosed Resources
       3. Improper equals() and hashCode() Implementations
    Handle:
       1. Enable Profiling: Java profilers are tools that monitor and diagnose the memory leaks through the application.
       2. Verbose Garbage Collection: enable verbose garbage collection to track detailed trace of GC
       3. use tools like Eclipse to show warnings and errors whenever it encounters obvious cases of memory leaks
       4. use benchmarks to measure and analyze the Java code's performance after changed code.
       5. Code Reviews
3 Connection Pool Timeout

内存分配策略: 静态分配,栈式分配,和堆式分配

静态存储区（方法区）：主要存放静态数据、全局 static 数据和常量。这块内存在程序编译时就已经分配好，并且在程序整个运行期间都存在。

栈区: 当方法被执行时，方法体内的局部变量（其中包括基础数据类型、对象的引用）都在栈上创建，并在方法执行结束时这些局部变量所持有的内存将会自动被释放。因为栈内存分配运算内置于处理器的指令集中，效率很高，但是分配的内存容量有限。

堆区: 又称动态内存分配，通常就是指在程序运行时直接 new 出来的内存，也就是对象的实例。这部分内存在不使用时将会由 Java 垃圾回收器来负责回收。

线程耗尽:
    Situation:
        请求超时，系统假死
        大部分线程都是在等待
        cpu使用率不高

    cause:
        IO处理未设置超时时间
        使用java 的queue超时时间 是否设置了
        一个业务中的多个任务，由不同线程池处理

    handle:
        使用jstack查看堆栈信息

线程间通信:
    使用全局变量: 主要由于多个线程可能更改全局变量，因此全局变量最好声明为volatile
    使用消息队列实现通信, 消息进行线程间通信sendMessage,postMessage。

进程间通信:
    1、低级通信,控制信息的通信(主要用于进程之间的同步,互斥,终止和挂起等等控制信息的传递)
        Pipe
        Semophore
    2、高级通信,大批数据信息的通信(主要用于进程间数据块数据的交换和共享,常见的高级通信有管道,消息队列,共享内存等).
        Message Queue


1.cpu 不变 memory上升
2.堆栈区别
3.线程耗尽
4.1tps的服务，上1000tps的系统（这纯属扯淡）

Object Orientated Design:
https://juejin.im/post/6844903673672237063

https://www.jianshu.com/p/e378025920f8

Six Design Principle:
1. Single Responsibility Principle
    A class should have a single responsibility, where a responsibility is nothing but a reason to change.
    一个类只允许有一个职责，即只有一个导致该类变更的原因。
2. Open Close Principle
    Software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification.
    一个软件实体如类、模块和函数应该对扩展开放，对修改关闭。
    用抽象构建框架，用实现扩展细节。
    不以改动原有类的方式来实现新需求，而是应该以实现事先抽象出来的接口（或具体类继承抽象类）的方式来实现。
3. Liskov Substitution Principle
4. Law of Demeter （ Least Knowledge Principle）
5. Interface Segregation Principle
6. Dependency Inversion Principle
    Depend upon Abstractions. Do not depend upon concretions.
    Abstractions should not depend upon details. Details should depend upon abstractions
    High-level modules should not depend on low-level modules. Both should depend on abstractions.

Polymorphism:
    Single task can be done in different way.
* method overriding:
    Specific implementation of a method for child class.
* method overloading:
    If a class have multiple methods by same name but different parameters, it is known as Method Overloading.

Encapsulation:
    Encapsulation is an attribute of an object, and it contains all data which is hidden. That hidden data can be restricted to the members of that class.

inheritance:
    Is-A

composition:
    Has-A

dependency injection:
    transferring the task of creating the object to someone else and directly using the dependency is called dependency injection.

*/