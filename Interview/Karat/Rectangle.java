
    class RectangleInfo {
        int x;
        int y;
        int width;
        int height;

        public RectangleInfo(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }

    @Test
    public void test1() {
        int[][] grid = {{1,0,0,1},
                        {1,0,0,1},
                        {1,0,0,1}};

        RectangleInfo info = findRectangle(grid);

        assertEquals(0, info.x);
        assertEquals(1, info.y);
        assertEquals(2, info.width);
        assertEquals(3, info.height);
    }

    @Test
    public void test2() {
        int[][] grid = {{1,1,1,1},
                        {1,0,0,0},
                        {1,0,0,0}};

        RectangleInfo info = findRectangle(grid);

        assertEquals(1, info.x);
        assertEquals(1, info.y);
        assertEquals(3, info.width);
        assertEquals(2, info.height);
    }