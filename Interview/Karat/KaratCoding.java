/**
Link: https://www.jianshu.com/p/fdbcba5fe5bc
*/
// Leetcode 811:
// https://leetcode.com/problems/subdomain-visit-count/
/**
We are given a list cpdomains of count-paired domains.
We would like a list of count-paired domains, (in the same format as the input, and in any order),
that explicitly counts the number of visits to each subdomain.

Example 1:
Input:
["9001 discuss.leetcode.com"]
Output:
["9001 discuss.leetcode.com", "9001 leetcode.com", "9001 com"]

Example 2:
Input:
["900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"]
Output:
["901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com"]
*/
    // Running Time Complexity: O(n), Space Complexity: O(1)
    // n = cpdomains.length;
    public List<String> subdomainVisits(String[] cpdomains) {
        List<String> res = new ArrayList<>();
        if (cpdomains == null || cpdomains.length == 0) {
            return res;
        }
        // key -> domain, val -> count
        Map<String, Integer> map = new HashMap<>();
        for (String cpdomain : cpdomains) {
            String[] texts = cpdomain.split(" ");
            int num = Integer.parseInt(texts[0]);

            String[] subdomains = texts[1].split("\\.");
            StringBuffer sb = new StringBuffer();
            for (int n = subdomains.length, i = n-1; i >= 0; i--) {
                if (i == n-1) {
                    sb.append(subdomains[i]);
                }
                else {
                    sb.insert(0, subdomains[i] + ".");
                }
                String key = sb.toString();
                int count = map.getOrDefault(key, 0);
                count += num;
                map.put(key, count);
            }
        }

        for (String key : map.keySet()) {
            int num = map.get(key);
            String text = num + " " + key;
            res.add(text);
        }

        return res;
    }

/**
Longest Common conntinuous subarray:
input:
[
  ["3234.html", "xys.html", "7hsaa.html"], // user1
  ["3234.html", "sdhsfjdsh.html", "xys.html", "7hsaa.html"] // user2
]

output:
["xys.html", "7hsaa.html"]
*/
    // Running Time Complexity: O(m*n), Space Complexity: O(m*n)
    // m = history1.length, n = history2.length
    public List<String> longestCommonSubArray(String[] history1, String[] history2) {
        List<String> res = new ArrayList<>();
        int m = history1.length;
        int n = history2.length;

        int[][] dp = new int[m+1][n+1];
        int max = 0;
        int start = -1;
        int end = -1;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (history1[i-1] == history2[j-1]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                }
                else {
                    dp[i][j] = 0;
                }
                if (dp[i][j] > max) {
                    max = dp[i][j];
                    start = i - dp[i][j];
                    end = i;
                }
            }
        }
        if (start == -1) {
            return res;
        }
        for (int i = start; i < end; i++) {
            res.add(history1[i]);
        }
        return res;
    }


/**
3. Ads Conversion Rate
The people who buy ads on our network don't have enough data about how ads are working for their business.
They've asked us to find out which ads produce the most purchases on their website.

Our client provided us with a list of user IDs of customers who bought something on a landing page
after clicking one of their ads:

Each user completed 1 purchase.
completed_purchase_user_ids = ["3123122444","234111110", "8321125440", "99911063"]

And our ops team provided us with some raw log data from our ad server showing every time a user clicked on one of our ads:
ad_clicks = [
  "IP_Address,Time,Ad_Text",
  "122.121.0.1,2016-11-03 11:41:19,Buy wool coats for your pets",
  "96.3.199.11,2016-10-15 20:18:31,2017 Pet Mittens",
  "122.121.0.250,2016-11-01 06:13:13,The Best Hollywood Coats",
  "82.1.106.8,2016-11-12 23:05:14,Buy wool coats for your pets",
  "92.130.6.144,2017-01-01 03:18:55,Buy wool coats for your pets",
  "92.130.6.145,2017-01-01 03:18:55,2017 Pet Mittens",
]

The client also sent over the IP addresses of all their users.

all_user_ips = [
  "User_ID, IP_Address",
  "2339985511,122.121.0.155",
  "234111110,122.121.0.1",
  "3123122444,92.130.6.145",
  "39471289472,2001:0db8:ac10:fe01:0000:0000:0000:0000",
  "8321125440,82.1.106.8",
  "99911063,92.130.6.144"
]

Write a function to parse this data, determine how many times each ad was clicked,
then return the ad text, that ad's number of clicks, and how many of those ad clicks
were from users who made a purchase.

Expected output:
Bought Clicked Ad Text
1 of 2  2017 Pet Mittens
0 of 1  The Best Hollywood Coats
3 of 3  Buy wool coats for your pets
*/
    class AdInfo {
        String text;
        int bought;
        int click;
        public AdInfo() {}
    }

    // Running Time Complexity: O(m + n + l), Space Complexity: O(m + n + l)
    // m = userIds.length, n = adClicks.length, l = allUserIps.length
    public List<String> adsConversionRate(String[] userIds, String[] adClicks, String[] allUserIps) {
        // key -> userId, val -> ipAddress
        Map<String, String> userIpMap = new LinkedHashMap<>();
        for (String allUserIp : allUserIps) {
            String[] texts = allUserIp.split(",");
            String userId = texts[0];
            String ipAddress = texts[1];
            if (!userIpMap.containsKey(userId)) {
                userIpMap.put(userId, ipAddress);
            }
        }

        Map<String, AdInfo> adsClickMap = new LinkedHashMap<>();
        // ipaddress -> product text
        Map<String, String> ipAddressProductMap = new LinkedHashMap<>();

        for (String adClick : adClicks) {
            String[] texts = adClick.split(",");
            String ipAddress = texts[0];
            String text = texts[2];
            AdInfo adInfo = adsClickMap.getOrDefault(text, new AdInfo());
            adInfo.text = text;
            adInfo.click++;
            adsClickMap.put(text, adInfo);

            ipAddressProductMap.put(ipAddress, text);
        }

        for (String userId : userIds) {
            if (!userIpMap.containsKey(userId)) {
                continue;
            }
            String ipAddress = userIpMap.get(userId);
            if (!ipAddressProductMap.containsKey(ipAddress)) {
                continue;
            }
            String text = ipAddressProductMap.get(ipAddress);
            AdInfo adInfo = adsClickMap.get(text);
            adInfo.bought++;
        }

        List<String> res = new ArrayList<>();
        for (String text : adsClickMap.keySet()) {
            AdInfo adInfo = adsClickMap.get(text);
            String info = adInfo.bought + " of " + adInfo.click + "  " + adInfo.text;
            res.add(info);
        }

        return res;
    }

/**
Course Overlapp:
You are a developer for a university. Your current project is to develop a system for students to find courses they share with friends.
The university has a system for querying courses students are enrolled in, returned as a list of (ID, course) pairs.
Write a function that takes in a list of (student ID number, course name) pairs and returns, for every pair of students,
a list of all courses they share.

Sample Input:

student_course_pairs_1 = [
  ["58", "Software Design"],
  ["58", "Linear Algebra"],
  ["94", "Art History"],
  ["94", "Operating Systems"],
  ["17", "Software Design"],
  ["58", "Mechanics"],
  ["58", "Economics"],
  ["17", "Linear Algebra"],
  ["17", "Political Science"],
  ["94", "Economics"],
  ["25", "Economics"],
]

Sample Output (pseudocode, in any order):

find_pairs(student_course_pairs_1) =>
{
  [58, 17]: ["Software Design", "Linear Algebra"]
  [58, 94]: ["Economics"]
  [58, 25]: ["Economics"]
  [94, 25]: ["Economics"]
  [17, 94]: []
  [17, 25]: []
}

Additional test cases:

Sample Input:

student_course_pairs_2 = [
  ["42", "Software Design"],
  ["0", "Advanced Mechanics"],
  ["9", "Art History"],
]

Sample output:

find_pairs(student_course_pairs_2) =>
{
  [0, 42]: []
  [0, 9]: []
  [9, 42]: []
}
*/

    class PairCourses {
        String userId1;
        String userId2;
        List<String> courses;
        public PairCourses() {
            this.courses = new ArrayList<>();
        }
    }
    // Running Time Complexity: O(n*n), Space Complexity: O(n)
    //
    public void findPairs(String[][] coursePairs) {
        Set<String> userIdsSet = new LinkedHashSet<>();
        // key -> user Id, val -> set of courses this user takes
        Map<String, Set<String>> userCoursesMap = new LinkedHashMap<>();

        for (String[] coursePair : coursePairs) {
            String userId = coursePair[0];
            String course = coursePair[1];
            userIdsSet.add(userId);
            Set<String> courses = userCoursesMap.getOrDefault(userId, new HashSet<>());
            courses.add(course);
            userCoursesMap.put(userId, courses);
        }

        List<PairCourses> pairCourseList = new ArrayList<>();

        List<String> userIds = new ArrayList<>(userIdsSet);
        for (int i = 0, n = userIds.length(); i < n; i++) {
            String userId1 = userIds.get(i);
            Set<String> courses1 = userCoursesMap.get(userId1);
            for (int j = i+1; j < n; j++) {
                String userId2 = userIds.get(j);
                Set<String> courses2 = userCoursesMap.get(userId2);
                List<String> commonCourses = new ArrayList<>();
                for (String course : courses2) {
                    if (courses1.contains(course)) {
                        commonCourses.add(course);
                    }
                }
                pairCourseList.add(userId1, userId2, commonCourses);
            }
        }

        return pairCourseList;
    }

/*
Students may decide to take different "tracks" or sequences of courses in the Computer Science curriculum. There may be more than one track that includes the same course, but each student follows a single linear track from a "root" node to a "leaf" node. In the graph below, their path always moves left to right.

Write a function that takes a list of (source, destination) pairs, and returns the name of all of the courses that the students could be taking when they are halfway through their track of courses.

Sample input:
all_courses = [
    ["Logic", "COBOL"],
    ["Data Structures", "Algorithms"],
    ["Creative Writing", "Data Structures"],
    ["Algorithms", "COBOL"],
    ["Intro to Computer Science", "Data Structures"],
    ["Logic", "Compilers"],
    ["Data Structures", "Logic"],
    ["Creative Writing", "System Administration"],
    ["Databases", "System Administration"],
    ["Creative Writing", "Databases"],
    ["Intro to Computer Science", "Graphics"],
]

Sample output (in any order):
          ["Data Structures", "Creative Writing", "Databases", "Intro to Computer Science"]

All paths through the curriculum (midpoint *highlighted*):

*Intro to C.S.* -> Graphics
Intro to C.S. -> *Data Structures* -> Algorithms -> COBOL
Intro to C.S. -> *Data Structures* -> Logic -> COBOL
Intro to C.S. -> *Data Structures* -> Logic -> Compiler
Creative Writing -> *Databases* -> System Administration
*Creative Writing* -> System Administration
Creative Writing -> *Data Structures* -> Algorithms -> COBOL
Creative Writing -> *Data Structures* -> Logic -> COBOL
Creative Writing -> *Data Structures* -> Logic -> Compilers

Visual representation:

                    ____________
                    |          |
                    | Graphics |
               ---->|__________|
               |                          ______________
____________   |                          |            |
|          |   |    ______________     -->| Algorithms |--\     _____________
| Intro to |   |    |            |    /   |____________|   \    |           |
| C.S.     |---+    | Data       |   /                      >-->| COBOL     |
|__________|    \   | Structures |--+     ______________   /    |___________|
                 >->|____________|   \    |            |  /
____________    /                     \-->| Logic      |-+      _____________
|          |   /    ______________        |____________|  \     |           |
| Creative |  /     |            |                         \--->| Compilers |
| Writing  |-+----->| Databases  |                              |___________|
|__________|  \     |____________|-\     _________________________
               \                    \    |                       |
                \--------------------+-->| System Administration |
                                         |_______________________|

Complexity analysis variables:

n: number of pairs in the input

*/
    // Running Time Complexity: O(m + n),
    // where m is the num of node in the graph, and n is the num of edges in the graph
    // Space Complexity: O(n)
    public List<String> findAllMidWayCourse(String[][] allCourses) {
        // build graph:
        // convert course to integer:
        int idx = 0;
        // key -> course name, val -> courseId
        Map<String, Integer> courseMap = new LinkedHashMap<>();
        // key -> courseId, val -> course name:
        Map<Integer, String> courseIdMap = new LinkedHashMap<>();
        for (String[] course : allCourses) {
            String from = course[0];
            String to = course[1];
            if (!courseMap.containsKey(from)) {
                courseMap.put(from, idx);
                courseIdMap.put(idx, from);
                idx++;
            }
            if (!courseMap.containsKey(to)) {
                courseMap.put(to, idx);
                courseIdMap.put(idx, to);
                idx++;
            }
        }

        int n = courseMap.keySet().size();
        int[] indegree = new int[n];
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }

        for (String[] course : allCourses) {
            String from = course[0];
            String to = course[1];
            int fromIdx = courseMap.get(from);
            int toIdx = courseMap.get(to);
            graph.get(fromIdx).add(toIdx);
            indegree[toIdx]++;
        }

        Set<String> set = new LinkedHashSet<>();
        for (int i = 0; i < n; i++) {
            if (indegree[i] != 0) {
                continue;
            }
            boolean[] visited = new boolean[n];
            List<List<Integer>> paths = new ArrayList<>();
            dfs(graph, i, visited, paths, new ArrayList<>());
            for (List<Integer> path : paths) {
                int m = path.size();
                // midway course arr idx:
                int j = (m-1) / 2;
                // midway course id:
                int courseId = path.get(j);
                String midCourse = courseIdMap.get(courseId);
                set.add(midCourse);
            }
        }

        return new ArrayList<>(set);
    }

    private void dfs(List<List<Integer>> graph, int from, boolean[] visited,
                     List<List<Integer>> paths, List<Integer> path) {
        if (visited[from]) {
            return;
        }
        visited[from] = true;
        path.add(from);
        List<Integer> neighbors = graph.get(from);
        // if no new out going neighbors, we know it is the leaf,
        // or ending point of the graph
        if (neighbors.isEmpty()) {
            paths.add(new ArrayList<>(path));
        }
        else {
            for (int neighbor : neighbors) {
                dfs(graph, neighbor, visited, paths, path);
            }
        }
        path.remove(path.size()-1);
        visited[from] = false;
    }

    @Test
    public void test1() {
        String[][] courses = {
                {"Logic", "COBOL"},
                {"Data Structures", "Algorithms"},
                {"Creative Writing", "Data Structures"},
                {"Algorithms", "COBOL"},
                {"Intro to Computer Science", "Data Structures"},
                {"Logic", "Compilers"},
                {"Data Structures", "Logic"},
                {"Creative Writing", "System Administration"},
                {"Databases", "System Administration"},
                {"Creative Writing", "Databases"},
                {"Intro to Computer Science", "Graphics"},
        };
        List<String> midCourses = findAllMidWayCourse(courses);
        for (String course : midCourses) {
            System.out.println(course);
        }
    }

/**
Find a rectangle

There is an image filled with 0s and 1s. There is at most one rectangle in this image filled with 0s, find the rectangle. Output could be the coordinates of top-left and bottom-right elements of the rectangle, or top-left element, width and height.

[
 [1,0,0,1],
 [1,0,0,1],
 [1,0,0,1]
]

*/
    class RectangleInfo {
        int x;
        int y;
        int width;
        int height;

        public RectangleInfo(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }
    // Running Time Complexity: O(m * n)
    // where m = grid.length, n = grid[0].length
    public RectangleInfo findRectangle(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        RectangleInfo info = null;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != 0) {
                    continue;
                }
                // search right:
                int width = 0;
                int k = j;
                while (k < n && grid[i][k] == 0) {
                    width++;
                    k++;
                }
                // search down:
                int height = 0;
                k = i;
                while (k < m && grid[k][j] == 0) {
                    height++;
                    k++;
                }
                info = new RectangleInfo(i, j, width, height);
                break;

            }
            if (info != null) {
                break;
            }
        }
        return info;
    }

    @Test
    public void test1() {
        int[][] grid = {{1,0,0,1},
                        {1,0,0,1},
                        {1,0,0,1}};

        RectangleInfo info = findRectangle(grid);
        System.out.println("x: " + info.x + ", y:" + info.y +
                ", width:" + info.width + ", height:" + info.height);
    }

    @Test
    public void test2() {
        int[][] grid = {{1,1,1,1},
                {1,0,0,0},
                {1,0,0,0}};

        RectangleInfo info = findRectangle(grid);
        System.out.println("x: " + info.x + ", y:" + info.y +
                ", width:" + info.width + ", height:" + info.height);
    }

/**
Find many rectangles:
for the same image, it is filled with 0s and 1s.

It may have multiple rectangles filled with 0s. The rectangles are separated by 1s. Find all the rectangles.

*/
    // Running Time Complexity: O(m * n)
    // where m = grid.length, n = grid[0].length
    public List<RectangleInfo> findAllRectangles(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;

        List<RectangleInfo> infos = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != 0) {
                    continue;
                }
                int width = 0;
                int k = j;
                while (k < n && grid[i][k] == 0) {
                    k++;
                    width++;
                }

                int height = 0;
                k = i;
                while (k < m && grid[k][j] == 0) {
                    height++;
                    k++;
                }
                infos.add(new RectangleInfo(i, j, width, height));
                // mark these area as 1 to avoid visiting again.
                for (int ii = i; ii < i+height; ii++) {
                    for (int jj = j; jj < j+width; jj++) {
                        grid[ii][jj] = 1;
                    }
                }
            }
        }
        return infos;
    }

    @Test
    public void test1() {
        int[][] grid = {{1,0,0,1,1,1,1},
                        {1,0,0,1,0,0,0},
                        {1,0,0,1,0,0,0},
                        {1,1,1,1,0,0,0}};
        List<RectangleInfo> infos = findAllRectangles(grid);
        for (RectangleInfo info : infos) {
            System.out.println("x: " + info.x + ", y:" + info.y +
                    ", width:" + info.width + ", height:" + info.height);
        }
    }

/**
Find the image has random shapes filled with 0s, separated by 1s. Find all the shapes. Each shape is represented by coordinates of all the elements inside.
*/

    class Point {
        int x;
        int y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    // Running Time Complexity: O(m * n), Space Complexity: O(m + n) => longest path of one search, slack depth
    // where m = grid.length, n = grid[0].length
    public List<Point> findAllShapes(int[][] grid) {
        List<Point> points = new ArrayList<>();
        if (grid == null || grid.length == 0) {
            return points;
        }
        int m = grid.length;
        int n = grid[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    continue;
                }
                dfs(grid, i, j, m, n, points);
            }
        }
        return points;
    }

    private void dfs(int[][] grid, int i, int j, int m, int n, List<Point> points) {
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 1) {
            return;
        }
        points.add(new Point(i, j));
        grid[i][j] = 1;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(grid, x, y, m, n, points);
        }
    }

/**
Reflow 字符串
Word wrap: 给一个word list 和最大的长度，要求把这些word用 - 串联起来，但不能超过最大的长度。
input:
words = ["the", "day", "began", "as", "still", "as", "the", "night", "abruptely", "lighted", "with", "brilliant", "flame"]
maxLen = 10
[ 'the-day',
  'began-as',
  'still-as',
  'the-night',
  'abruptely',
  'lighted',
  'with',
  'brilliant',
  'flame' ]

*/
    // Running Time Complexity: O(n), Space Complexity: O(1)
    // where n = words.length
    public List<String> wordWrap(String[] words, int maxLen) {
        List<String> res = new ArrayList<>();
        int i = 0;
        int n = words.length;
        StringBuffer sb = new StringBuffer();
        while (i < n) {
            String word = words[i];
            if (word.length() <= maxLen) {
                sb.append(word);
                i++;
                if (i < n) {
                    word = words[i];
                }
            }
            // meaning cannot make a line of words that fit the maxlen
            else {
                break;
            }
            while (i < n && sb.length() + word.length() + 1 <= maxLen) {
                sb.append("-").append(word);
                i++;
                if (i < n) {
                    word = words[i];
                }
            }
            res.add(sb.toString());
            sb.setLength(0);
        }
        return res;
    }

    @Test
    public void test1() {
        String[] words = {"the", "day", "began", "as", "still", "as", "the", "night",
                "abruptely", "lighted", "with", "brilliant", "flame"};
        int maxLen = 9;
        List<String> res = wordWrap(words, maxLen);
        for (String text : res) {
            System.out.println(text);
        }
    }

/*
Word Processor:
We are building a word processor and we would like to implement a "reflow" functionality that also applies full justification to the text.

Given an array containing lines of text and a new maximum width, re-flow the text to fit the new width. Each line should have the exact specified width. If any line is too short, insert '-' (as stand-ins for spaces) between words as equally as possible until it fits.
Note: we are using '-' instead of spaces between words to make testing and visual verification of the results easier.


lines = [ "The day began as still as the",
          "night abruptly lighted with",
          "brilliant flame" ]

reflowAndJustify(lines, 24) ... "reflow lines and justify to length 24" =>

        [ "The--day--began-as-still",
          "as--the--night--abruptly",
          "lighted--with--brilliant",
          "flame" ] // <--- a single word on a line is not padded with spaces

reflowAndJustify(lines, 25) ... "reflow lines and justify to length 25" =>

        [ "The-day-began-as-still-as"
          "the-----night----abruptly"
          "lighted---with--brilliant"
          "flame" ]

reflowAndJustify(lines, 26) ... "reflow lines and justify to length 26" =>

        [ "The--day-began-as-still-as",
          "the-night-abruptly-lighted",
          "with----brilliant----flame" ]

reflowAndJustify(lines, 40) ... "reflow lines and justify to length 40" =>

        [ "The--day--began--as--still--as-the-night",
          "abruptly--lighted--with--brilliant-flame" ]

n = number of words OR total characters

*/

    static private final String DASH = "-";
    static private final String SPACE = " ";

    // Runnig Time Complexity: O(n), Space Complexity: O(n)
    // where n = sentences.length.
    public List<String> reflowAndJustify(String[] sentences, int maxLen) {
        List<String> res = new ArrayList<>();
        if (sentences == null || sentences.length == 0) {
            return res;
        }
        List<String> wordList = new ArrayList<>();
        for (String sentence : sentences) {
            String[] words = sentence.split(SPACE);
            for (String word : words) {
                wordList.add(word);
            }
        }
        int i = 0;
        int n = wordList.size();
        StringBuffer sb = new StringBuffer();
        while (i < n) {
            String word = wordList.get(i);
            sb.append(word);
            i++;
            if (i < n) {
                word = wordList.get(i);
            }
            int dashCnt = 0;
            while (i < n && sb.length() + word.length() + 1 <= maxLen) {
                sb.append(DASH).append(word);
                dashCnt++;
                i++;
                if (i < n) {
                    word = wordList.get(i);
                }
            }
            // add additional DASH if the length of StringBuffer is less than maxLen
            if (dashCnt > 0 && sb.length() < maxLen) {
                int baseDashCnt = 1;
                if (maxLen - sb.length() >= dashCnt) {
                    baseDashCnt = (maxLen - sb.length()) / dashCnt;
                }

                int extra = (maxLen - sb.length()) % dashCnt;
                String[] words = sb.toString().split(DASH);
                sb.setLength(0);
                for (int j = 0, m = words.length; j < m; j++) {
                    if (j == m-1) {
                        sb.append(words[j]);
                    }
                    else {
                        if (extra-- <= 0) {
                            sb.append(words[j]);
                            for (int k = 0; k < baseDashCnt; k++) {
                                sb.append(DASH);
                            }
                        }
                        else {
                            sb.append(words[j]);
                            for (int k = 0; k < baseDashCnt; k++) {
                                sb.append(DASH);
                            }
                            sb.append(DASH);
                        }
                    }
                }
            }
            res.add(sb.toString());
            sb.setLength(0);
        }

        return res;
    }

    @Test
    public void test1() {
        String[] sentences = {"The day began as still as the",
                              "night abruptly lighted with",
                              "brilliant flame"};
        int maxLen = 24;
        List<String> res = reflowAndJustify(sentences, maxLen);
        for (String text : res) {
            System.out.println(text);
        }
    }

    @Test
    public void test2() {
        String[] sentences = {"The day began as still as the",
                "night abruptly lighted with",
                "brilliant flame"};
        int maxLen = 25;
        List<String> res = reflowAndJustify(sentences, maxLen);
        for (String text : res) {
            System.out.println(text);
        }
    }

    @Test
    public void test3() {
        String[] sentences = {"The day began as still as the",
                "night abruptly lighted with",
                "brilliant flame"};
        int maxLen = 26;
        List<String> res = reflowAndJustify(sentences, maxLen);
        for (String text : res) {
            System.out.println(text);
        }
    }

    @Test
    public void test4() {
        String[] sentences = {"The day began as still as the",
                "night abruptly lighted with",
                "brilliant flame"};
        int maxLen = 40;
        /**
         *         [ "The--day--began--as--still--as-the-night",
         *           "abruptly--lighted--with--brilliant-flame" ]
         * */
        List<String> res = reflowAndJustify(sentences, maxLen);
        for (String text : res) {
            System.out.println(text);
        }
    }

/**
Basic Calculator:
给输入为string，例如"2+3-999"，之包含+-操作，返回计算结果。

*/
    // Running Time Complexity: O(n)
    // Space Complexity: O(# of brackets)
    // where n = s.length();
    public int basicCalculator(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int sum = 0;
        int num = 0;
        int sign = 1;
        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) {
                num = 10 * num + (int) (c - '0');
            }
            else if (c == '+') {
                sum += sign * num;
                num = 0;
                sign = 1;
            }
            else if (c == '-') {
                sum += sign * num;
                num = 0;
                sign = -1;
            }
        }
        if (num != 0) {
            sum += sign * num;
        }
        return sum;
    }

/**
Leetcode 224: Basic Calculator:
https://leetcode.com/problems/basic-calculator
加上parenthesis， 例如"2+((8+2)+(3-999))"，返回计算结果。
*/
    // Running Time Complexity: O(n)
    // Space Complexity: O(# of brackets)
    // where n = s.length()
    public int basicCalculator(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        Stack<Integer> stack = new Stack<>();
        int sum = 0;
        int num = 0;
        int sign = 1;
        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) {
                num = 10 * num + (int) (c - '0');
            }
            else if (c == '+') {
                sum += sign * num;
                num = 0;
                sign = 1;
            }
            else if (c == '-') {
                sum += sign * num;
                num = 0;
                sign = -1;
            }
            else if (c == '(') {
                stack.push(sum);
                stack.push(sign);
                num = 0;
                sum = 0;
                sign = 1;
            }
            else if (c == ')') {
                sum += sign * num;
                int prevSign = stack.pop();
                int prevSum = stack.pop();
                sum = prevSum + prevSign * sum;
                num = 0;
                sign = 1;
            }
        }
        if (num != 0) {
            sum += sign * num;
        }
        return sum;
    }

/**
Leetcode 227 Basic Calculator II:
Implement a basic calculator to evaluate a simple expression string.

The expression string contains only non-negative integers, +, -, *, / operators and empty spaces . The integer division should truncate toward zero.

Example 1:

Input: "3+2*2"
Output: 7
Example 2:

Input: " 3/2 "
Output: 1
Example 3:

Input: " 3+5 / 2 "
Output: 5
*/
    // Running Time Complexity: O(N)
    public int calculate(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int res = 0;
        int num = 0;
        int prevNum = 0;
        char prevSign = '+';
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                continue;
            }
            else if (Character.isDigit(c)) {
                num = 10 * num + (int) (c - '0');
            }
            else {
                int[] arr = handleOperator(res, num, prevNum, prevSign);
                res = arr[0];
                num = 0;
                prevNum = arr[1];
                prevSign = c;
            }
        }
        if (num != 0) {
            int[] arr = handleOperator(res, num, prevNum, prevSign);
            res = arr[0];
        }
        return res;
    }

    private int[] handleOperator(int res, int num, int prevNum, char prevSign) {
        if (prevSign == '+') {
            prevNum = num;
        }
        else if (prevSign == '-') {
            prevNum = -1 * num;
        }
        else if (prevSign == '*') {
            res -= prevNum;
            prevNum = prevNum * num;
        }
        else if (prevSign == '/') {
            res -= prevNum;
            prevNum = prevNum / num;
        }
        res += prevNum;

        int[] arr = new int[2];
        arr[0] = res;
        arr[1] = prevNum;
        return arr;
    }

/**
Basic Calculator III:
Implement a basic calculator to evaluate a simple expression string.

The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

The expression string contains only non-negative integers, +, -, *, / operators , open ( and closing parentheses ) and empty spaces . The integer division should truncate toward zero.

You may assume that the given expression is always valid. All intermediate results will be in the range of [-2147483648, 2147483647].

Some examples:

"1 + 1" = 2
" 6-4 / 2 " = 4
"2*(5+5*2)/3+(6/2+8)" = 21
"(2+6* 3+5- (3*14/7+2)*5)+3"=-12
*/

    class Record {
        int res;
        char prevSign;
        public Record(int res, char prevSign) {
            this.res = res;
            this.prevSign = prevSign;
        }
    }

    public int calculate(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        Stack<Record> stack = new Stack<>();
        int res = 0;
        int num = 0;
        int prevNum = 0;
        char prevSign = '+';

        for (char c : s.toCharArray()) {
            if (c == ' ') {
                continue;
            }
            else if (Character.isDigit(c)) {
                num = 10 * num + (int) (c - '0');
            }
            else if (c == '(') {
                Record record = new Record(res, prevSign);
                stack.push(record);
                num = 0;
                res = 0;
                prevNum = 0;
                prevSign = '+';
            }
            else if (c == ')') {
                int[] arr = handleOperators(res, num, prevNum, prevSign);
                num = arr[0];

                Record record = stack.pop();
                res = record.res;
                prevNum = res;
                prevSign = record.prevSign;
            }
            else {
                int[] arr = handleOperators(res, num, prevNum, prevSign);
                res = arr[0];
                prevNum = arr[1];
                num = 0;
                prevSign = c;
            }
        }
        if (num != 0) {
            int[] arr = handleOperators(res, num, prevNum, prevSign);
            res = arr[0];
        }

        return res;
    }

    private int[] handleOperators(int res, int num, int prevNum, char prevSign) {
        if (prevSign == '+') {
            prevNum = num;
        }
        else if (prevSign == '-') {
            prevNum = -1 * num;
        }
        else if (prevSign == '*') {
            res -= prevNum;
            prevNum = prevNum * num;
        }
        else if (prevSign == '/') {
            res -= prevNum;
            prevNum = prevNum / num;
        }
        res += prevNum;

        int[] arr = new int[2];
        arr[0] = res;
        arr[1] = prevNum;
        return arr;
    }

/**
Leetcode 770 Basic Calculator IV:
https://leetcode.com/problems/basic-calculator-iv/
https://leetcode-cn.com/problems/basic-calculator-iv
*/
/**
时间复杂度: 时间复杂度即为 O(2^N + M)，其中 N 为 expression 的长度， M 为 evalvars 和 evalints 的长度。
空间复杂度: O(N + M)。
*/
    class Poly {
        Map<List<String>, Integer> count;
        public Poly() {
            this.count = new HashMap<>();
        }

        public Poly(Map<List<String>, Integer> map) {
            this.count = new HashMap<>(map);
        }

        public void update(List<String> key, int val) {
            int oldVal = count.getOrDefault(key, 0);
            count.put(key, oldVal + val);
        }

        public Poly add(Poly that) {
            Poly res = new Poly(this.count);

            for (List<String> key : that.count.keySet()) {
                int val = that.count.get(key);
                res.update(key, val);
            }

            return res;
        }

        public Poly sub(Poly that) {
            Poly res = new Poly(this.count);

            for (List<String> key : that.count.keySet()) {
                int val = that.count.get(key) * -1;
                res.update(key, val);
            }

            return res;
        }

        public Poly mul(Poly that) {
            Poly res = new Poly();
            for (List<String> keys1 : this.count.keySet()) {
                int val1 = this.count.get(keys1);
                for (List<String> keys2 : that.count.keySet()) {
                    List<String> newKeys = new ArrayList<>(keys1);
                    newKeys.addAll(keys2);
                    Collections.sort(newKeys);

                    int val2 = that.count.get(keys2);
                    res.update(newKeys, val1 * val2);
                }
            }
            return res;
        }

        public Poly evaluate(Map<String, Integer> evalMap) {
            Poly res = new Poly();
            for (List<String> keys : this.count.keySet()) {
                int val = this.count.get(keys);
                List<String> freeKeys = new ArrayList<>();
                for (String key : keys) {
                    if (evalMap.containsKey(key)) {
                        val *= evalMap.get(key);
                    }
                    else {
                        freeKeys.add(key);
                    }
                }
                res.update(freeKeys, val);
            }
            return res;
        }

        public int compareList(List<String> list1, List<String> list2) {
            int i = 0;
            for (String x : list1) {
                String y = list2.get(i++);
                int res = x.compareTo(y);
                if (res != 0) {
                    return res;
                }
            }
            return 0;
        }

        public List<String> toList() {
            List<String> res = new ArrayList<>();
            List<List<String>> keys = new ArrayList<>(this.count.keySet());

            ListComparator comp = new ListComparator();
            Collections.sort(keys, comp);

            for (List<String> key : keys) {
                int val = this.count.get(key);
                if (val == 0) {
                    continue;
                }
                StringBuffer sb = new StringBuffer();
                sb.append(val);
                for (String token : key) {
                    sb.append("*");
                    sb.append(token);
                }
                res.add(sb.toString());
            }

            return res;
        }
    }

    public class ListComparator implements Comparator<List<String>> {
        @Override
        public int compare(List<String> list1, List<String> list2) {
            if (list1.size() != list2.size()) {
                return Integer.compare(list2.size(), list1.size());
            }
            else {
                return compareList(list1, list2);
            }
        }

        public int compareList(List<String> list1, List<String> list2) {
            int i = 0;
            for (String x : list1) {
                String y = list2.get(i++);
                int res = x.compareTo(y);
                if (res != 0) {
                    return res;
                }
            }
            return 0;
        }
    }

    public List<String> basicCalculatorIV(String expression, String[] evalvars, int[] evalints) {
        Map<String, Integer> evalMap = new HashMap<>();
        for (int i = 0, n = evalvars.length; i < n; i++) {
            String key = evalvars[i];
            int val = evalints[i];
            evalMap.put(key, val);
        }
        return parse(expression).evaluate(evalMap).toList();
    }

    private Poly make(String expression) {
        Poly res = new Poly();
        List<String> list = new ArrayList<>();
        char firstChar = expression.charAt(0);
        if (Character.isDigit(firstChar)) {
            res.update(list, Integer.valueOf(expression));
        }
        else {
            list.add(expression);
            res.update(list, 1);
        }
        return res;
    }

    private Poly combine(Poly left, Poly right, char symbol) {
        if (symbol == '+') {
            return left.add(right);
        }
        else if (symbol == '-') {
            return left.sub(right);
        }
        else if (symbol == '*') {
            return left.mul(right);
        }
        return null;
    }

    private Poly parse(String expression) {
        List<Poly> bucket = new ArrayList<>();
        List<Character> symbols = new ArrayList<>();

        int i = 0;
        int n = expression.length();
        while (i < n) {
            char c = expression.charAt(i);
            if (c == '(') {
                int bal = 0;
                int j = i;
                for (; j < n; j++) {
                    char c2 = expression.charAt(j);
                    if (c2 == '(') {
                        bal++;
                    }
                    else if (c2 == ')') {
                        bal--;
                    }
                    if (bal == 0) {
                        break;
                    }
                }
                String subStr = expression.substring(i+1, j);
                Poly subPoly = parse(subStr);
                bucket.add(subPoly);
                i = j;
            }
            else if (Character.isLetterOrDigit(c)) {
                int j = i;
                search : {
                    for (; j < n; j++) {
                        char c2 = expression.charAt(j);
                        if (c2 == ' ') {
                            String subStr = expression.substring(i, j);
                            Poly subPoly = make(subStr);
                            bucket.add(subPoly);
                            break search;
                        }
                    }
                    String subStr = expression.substring(i);
                    Poly subPoly = make(subStr);
                    bucket.add(subPoly);
                }
                i = j;
            }
            else if (c != ' ') {
                symbols.add(c);
            }
            i++;
        }

        for (int j = symbols.size() - 1; j >= 0; j--) {
            if (symbols.get(j) == '*') {
                Poly leftPoly = bucket.get(j);
                Poly rightPoly = bucket.remove(j+1);
                char symbol = symbols.remove(j);
                Poly subPoly = combine(leftPoly, rightPoly, symbol);
                bucket.set(j, subPoly);
            }
        }

        if (bucket.isEmpty()) {
            return new Poly();
        }
        Poly res = bucket.get(0);
        for (int j = 0, max = symbols.size(); j < max; j++) {
            Poly rightPoly = bucket.get(j+1);
            char symbol = symbols.get(j);
            res = combine(res, rightPoly, symbol);
        }
        return res;
    }

    @Test
    public void test1() {
        String expression = "e + 8 - a + 5";
        String[] evalvars = {"e"};
        int[] evalints = {1};

        List<String> res = basicCalculatorIV(expression, evalvars, evalints);
        for (String text : res) {
            System.out.println(text);
        }
    }

    @Test
    public void test2() {
        String expression = "(e + 8) * (e - 8)";
        String[] evalvars = {};
        int[] evalints = {};

        List<String> res = basicCalculatorIV(expression, evalvars, evalints);
        for (String text : res) {
            System.out.println(text);
        }
    }

/**
给一个N*N的矩阵，判定是否是有效的矩阵。
有效矩阵的定义是每一行或者每一列的数字都必须正好是1到N的数。输出一个bool。
*/
    // Running Time Complexity: O(m * n)
    // where m = matrix.length, n = matrix[0].length
    public boolean isValidMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        int n = matrix.length;
        for (int i = 0; i < n; i++) {
            Set<Integer> rowSet = new HashSet<>();
            Set<Integer> colSet = new HashSet<>();
            int rowMin = n+1;
            int rowMax = -1;
            int colMin = n+1;
            int colMax = -1;
            for (int j = 0; j < n; j++) {
                int num = matrix[i][j]
                if (!rowSet.contains(num)) {
                    rowSet.add(num);
                    rowMin = Math.min(rowMin, num);
                    rowMax = Math.max(rowMax, num);
                }
                else {
                    return false;
                }
                num = matrix[j][i];
                if (!colSet.contains(num)) {
                    colSet.add(num);
                    colMin = Math.min(colMin, num);
                    colMax = Math.max(colMax, num);
                }
                else {
                    return false;
                }
            }
            if (rowMin != -1 || rowMax != n || colMin != 1 || colMax != n) {
                return false;
            }
        }
        return true;
    }

/**
Nonogram:

"""
A nonogram is a logic puzzle, similar to a crossword, in which the player is given a blank grid and has to color it according to some instructions. Specifically, each cell can be either black or white, which we will represent as 0 for black and 1 for white.

+------------+
| 1  1  1  1 |
| 0  1  1  1 |
| 0  1  0  0 |
| 1  1  0  1 |
| 0  0  1  1 |
+------------+

For each row and column, the instructions give the lengths of contiguous runs of black (0) cells.
For example, the instructions for one row of [ 2, 1 ] indicate that there must be a run of two black cells, followed later by another run of one black cell, and the rest of the row filled with white cells.

These are valid solutions: [ 1, 0, 0, 1, 0 ] and [ 0, 0, 1, 1, 0 ] and also [ 0, 0, 1, 0, 1 ]
This is not valid: [ 1, 0, 1, 0, 0 ] since the runs are not in the correct order.
This is not valid: [ 1, 0, 0, 0, 1 ] since the two runs of 0s are not separated by 1s.

Your job is to write a function to validate a possible solution against a set of instructions.
Given a 2D matrix representing a player's solution; and instructions for each row along with additional instructions for each column; return True or False according to whether both sets of instructions match.

Example instructions #1

matrix1 = [[1,1,1,1],
           [0,1,1,1],
           [0,1,0,0],
           [1,1,0,1],
           [0,0,1,1]]
rows1_1    =  [], [1], [1,2], [1], [2]
columns1_1 =  [2,1], [1], [2], [1]
validateNonogram(matrix1, rows1_1, columns1_1) => True

Example solution matrix:
matrix1 ->
                                   row
                +------------+     instructions
                | 1  1  1  1 | <-- []
                | 0  1  1  1 | <-- [1]
                | 0  1  0  0 | <-- [1,2]
                | 1  1  0  1 | <-- [1]
                | 0  0  1  1 | <-- [2]
                +------------+
                  ^  ^  ^  ^
                  |  |  |  |
  column       [2,1] | [2] |
  instructions      [1]   [1]


Example instructions #2

(same matrix as above)
rows1_2    =  [], [], [1], [1], [1,1]
columns1_2 =  [2], [1], [2], [1]
validateNonogram(matrix1, rows1_2, columns1_2) => False

The second and third rows and the first column do not match their respective instructions.

Example instructions #3

matrix2 = [
[ 1, 1 ],
[ 0, 0 ],
[ 0, 0 ],
[ 1, 0 ]
]
rows2_1    = [], [2], [2], [1]
columns2_1 = [1, 1], [3]
validateNonogram(matrix2, rows2_1, columns2_1) => False

The black cells in the first column are not separated by white cells.

n: number of rows in the matrix
m: number of columns in the matrix
"""

matrix1 = [
    [1,1,1,1], # []
    [0,1,1,1], # [1] -> a single run of _1_ zero (i.e.: "0")
    [0,1,0,0], # [1, 2] -> first a run of _1_ zero, then a run of _2_ zeroes
    [1,1,0,1], # [1]
    [0,0,1,1], # [2]
]

# True
rows1_1 = [[],[1],[1,2],[1],[2]]
columns1_1 = [[2,1],[1],[2],[1]]
# False
rows1_2 = [[],[],[1],[1],[1,1]]
columns1_2 = [[2],[1],[2],[1]]

matrix2 = [
    [1,1],
    [0,0],
    [0,0],
    [1,0]
]
# False
rows2_1 = [[],[2],[2],[1]]
columns2_1 = [[1,1],[3]]
*/
    // Running Time Complexity: O(m * n), Space Complexity: O(m + n)
    public boolean isValidNonogram(int[][] grid, int[][] rows, int[][] cols) {
        if (grid == null || grid.length == 0) {
            return false;
        }
        int m = grid.length;
        int n = grid[0].length;
        // validate rows:
        for (int i = 0; i < m; i++) {
            List<Integer> list = rowZeros(grid[i], n);
            if (list.size() != rows[i].length) {
                return false;
            }
            for (int j = 0, max = list.size(); j < max; j++) {
                if (list.get(j) != rows[i][j]) {
                    return false;
                }
            }
        }

        // validate columns:
        for (int i = 0; i < n; i++) {
            List<Integer> list = colZeros(grid, i, m);
            if (list.size() != cols[i].length) {
                return false;
            }
            for (int j = 0, max = list.size(); j < max; j++) {
                if (list.get(j) != cols[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<Integer> rowZeros(int[] row, int n) {
        List<Integer> list = new ArrayList<>();
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            if (row[i] == 0) {
                cnt++;
            }
            else {
                if (cnt > 0) {
                    list.add(cnt);
                }
                cnt = 0;
            }
        }
        if (cnt > 0) {
            list.add(cnt);
        }
        return list;
    }

    private List<Integer> colZeros(int[][] grid, int colIdx, int m) {
        List<Integer> list = new ArrayList<>();
        int cnt = 0;
        for (int i = 0; i < m; i++) {
            if (grid[i][colIdx] == 0) {
                cnt++;
            }
            else {
                if (cnt > 0) {
                    list.add(cnt);
                }
                cnt = 0;
            }
        }
        if (cnt > 0) {
            list.add(cnt);
        }
        return list;
    }

    @Test
    public void rowZeroTest1() {
        int[][] data = {{1,1,1,1}, {0,1,1,1}, {0,1,0,0}, {0,0,1,1}};
        int n = 4;
        List<Integer> row1 = rowZeros(data[0], n);
        assertTrue(row1.isEmpty());

        row1 = rowZeros(data[1], n);
        assertTrue(row1.size() == 1);
        assertTrue(row1.get(0) == 1);

        row1 = rowZeros(data[2], n);
        assertTrue(row1.size() == 2);
        assertTrue(row1.get(0) == 1);
        assertTrue(row1.get(1) == 2);

        row1 = rowZeros(data[3], n);
        assertTrue(row1.size() == 1);
        assertTrue(row1.get(0) == 2);
    }

    @Test
    public void colZerosTest1() {
        int[][] data = {{1,1,1,1},
                        {0,1,1,1},
                        {0,1,0,0},
                        {1,1,0,1},
                        {0,0,1,1}};
        int n = 5;
        List<Integer> col1 = colZeros(data,0, n);
        assertFalse(col1.isEmpty());
        assertTrue(col1.size() == 2);
        assertTrue(col1.get(0) == 2);
        assertTrue(col1.get(1) == 1);

        col1 = colZeros(data,1, n);
        assertTrue(col1.size() == 1);
        assertTrue(col1.get(0) == 1);

        col1 = colZeros(data,2, n);
        assertTrue(col1.size() == 1);
        assertTrue(col1.get(0) == 2);

        col1 = colZeros(data,3, n);
        assertTrue(col1.size() == 1);
        assertTrue(col1.get(0) == 1);
    }

    @Test
    public void nonogramTest1() {
        int[][] data = {{1,1,1,1},
                {0,1,1,1},
                {0,1,0,0},
                {1,1,0,1},
                {0,0,1,1}};

        int[][] rows = {{}, {1}, {1,2}, {1}, {2}};
        int[][] cols = {{2,1}, {1}, {2}, {1}};

        boolean res = isValidNonogram(data, rows, cols);
        assertTrue(res);
    }

    @Test
    public void nonogramTest2() {
        int[][] data = {{1,1,1,1},
                {0,1,1,1},
                {0,1,0,0},
                {1,1,0,1},
                {0,0,1,1}};

        int[][] rows = {{}, {}, {1}, {1}, {1,1}};
        int[][] cols = {{2}, {1}, {2}, {1}};

        boolean res = isValidNonogram(data, rows, cols);
        assertFalse(res);
    }

    @Test
    public void nonogramTest3() {
        int[][] data = {{1,1},
                {0,0},
                {0,0},
                {1,0}};

        int[][] rows = {{}, {2}, {2}, {1}};
        int[][] cols = {{1,1}, {3}};

        boolean res = isValidNonogram(data, rows, cols);
        assertFalse(res);
    }

    @Test
    public void nonogramTest4() {
        int[][] data = {{1,1},
                        {0,0},
                        {0,0},
                        {1,0}};

        int[][] rows = {{}, {2}, {2}, {1}};
        int[][] cols = {{2}, {3}};

        boolean res = isValidNonogram(data, rows, cols);
        assertTrue(res);
    }

/**
0个或1个parent的节点:

输入是int[][] input, input[0]是input[1] 的parent，比如 {{1,4}, {1,5}, {2,5}, {3,6}, {6,7}}会形成上面的图
第一问是只有0个parents和只有1个parent的节点

  1    2    3
/  \  /      \
4    5        6
                \
                  7

*/

    class Info {
        List<Integer> zeroParNodes;
        List<Integer> oneParNodes;
        public Info() {
            this.zeroParNodes = new ArrayList<>();
            this.oneParNodes = new ArrayList<>();
        }
    }
    // Running Time Complexity: O(n), Space Complexity: O(n)
    // where n is the num of nodes
    private Info findNodesWithZeroOrOneParent(int[][] pairs) {
        Set<Integer> nodes = new LinkedHashSet<>();
        Map<Integer, Integer> parMap = new LinkedHashMap<>();
        for (int[] pair : pairs) {
            int par = pair[0];
            int child = pair[1];
            int count = parMap.getOrDefault(child, 0);
            count++;
            parMap.put(child, count);
            nodes.add(par);
            nodes.add(child);
        }
        Info info = new Info();
        for (int node : nodes) {
            if (!parMap.containsKey(node)) {
                info.zeroParNodes.add(node);
            }
            else if (parMap.get(node) == 1) {
                info.oneParNodes.add(node);
            }
        }
        return info;
    }


    @Test
    public void test1() {
        int[][] data = {{1,4}, {1,5}, {2,5}, {3,6}, {6,7}};
        Info info = findNodesWithZeroOrOneParent(data);
        List<Integer> zeroNodes = info.zeroParNodes;
        // Expect: 1,2,3
        System.out.println("Zero parent nodes:");
        for (int node : zeroNodes) {
            System.out.println(node);
        }

        // Expect: 4,6,7
        List<Integer> oneNodes = info.oneParNodes;
        System.out.println("One parent nodes:");
        for (int node : oneNodes) {
            System.out.println(node);
        }
    }

/**
两个节点是否有公共祖先
输入是int[][] input, input[0]是input[1] 的parent，
*/
    // Running Time Complexity: O(N), Space Complexity: O(N)
    // where n is the num of nodes
    public boolean hasCommonAncestor(int[][] pairs, int p1, int p2) {
        if (pairs == null || pairs.length == 0) {
            return false;
        }
        // key -> node, val -> list of parNodes
        Map<Integer, Set<Integer>> parMap = new LinkedHashMap<>();
        for (int[] pair : pairs) {
            int par = pair[0];
            int child = pair[1];
            Set<Integer> parSet = parMap.getOrDefault(child, new HashSet<>());
            parSet.add(par);
            parMap.put(child, parSet);
        }
        // If either node is parNode, which does not have parent, return false.
        if (!parMap.containsKey(p1) || !parMap.containsKey(p2)) {
            return false;
        }
        Set<Integer> parSet1 = findAllParents(parMap, p1);
        Set<Integer> parSet2 = findAllParents(parMap, p2);

        for (int num : parSet1) {
            if (parSet2.contains(num)) {
                return true;
            }
        }

        return false;
    }

    private Set<Integer> findAllParents(Map<Integer, Set<Integer>> parMap, int p) {
        Queue<Integer> queue = new LinkedList<>();
        Set<Integer> parSet = new HashSet<>();
        queue.offer(p);
        while (!queue.isEmpty()) {
            int child = queue.poll();
            if (!parMap.containsKey(child)) {
                continue;
            }
            Set<Integer> parents = parMap.get(child);
            parSet.addAll(parents);
            for (int parent : parents) {
                queue.offer(parent);
            }
        }
        return parSet;
    }

/**
Find Earilest Ancestor:
"""

Suppose we have some input data describing a graph of relationships between parents and children over multiple generations. The data is formatted as a list of (parent, child) pairs, where each individual is assigned a unique integer identifier.

For example, in this diagram, the earliest ancestor of 6 is 14, and the earliest ancestor of 15 is 2.

         14
         |
  2      4
  |    / | \
  3   5  8  9
/ \ / \     \
15  6   7    11

Write a function that, for a given individual in our dataset, returns their earliest known ancestor -- the one at the farthest distance from the input individual. If there is more than one ancestor tied for "earliest", return any one of them. If the input individual has no parents, the function should return null (or -1).

Sample input and output:

parentChildPairs1 = [
    (2, 3), (3, 15), (3, 6), (5, 6), (5, 7),
    (4, 5), (4, 8), (4, 9), (9, 11), (14, 4),
]

findEarliestAncestor(parentChildPairs1, 8) => 14
findEarliestAncestor(parentChildPairs1, 7) => 14
findEarliestAncestor(parentChildPairs1, 6) => 14
findEarliestAncestor(parentChildPairs1, 15) => 2
findEarliestAncestor(parentChildPairs1, 14) => null or -1
findEarliestAncestor(parentChildPairs3, 11) => 14

Additional example:

  14
  |
  2      4
  |    / | \
  3   5  8  9
/ \ / \     \
15  6   7    11

parentChildPairs2 = [
    (2, 3), (3, 15), (3, 6), (5, 6), (5, 7),
    (4, 5), (4, 8), (4, 9), (9, 11), (14, 2), (1, 9),
]

findEarliestAncestor(parentChildPairs2, 8) => 4
findEarliestAncestor(parentChildPairs2, 7) => 4
findEarliestAncestor(parentChildPairs2, 6) => 14
findEarliestAncestor(parentChildPairs2, 15) => 14
findEarliestAncestor(parentChildPairs2, 14) => null or -1
findEarliestAncestor(parentChildPairs2, 11) => 4 or 1

"""
*/
    // Running Time Complexity: O(N), Space Complexity: O(N)
    // where n is the num of nodes
    public int findEarliestAncestor(int[][] pairs, int node) {
        if (pairs == null || pairs.length == 0) {
            return 0;
        }
        // key -> child, val: all parents this child has.
        Map<Integer, Set<Integer>> parMap = new HashMap<>();
        for (int[] pair : pairs) {
            int parent = pair[0];
            int child = pair[1];
            Set<Integer> parSet = parMap.getOrDefault(child, new HashSet<>());
            parSet.add(parent);
            parMap.put(child, parSet);
        }
        // if parMap does not have node as key, it means that it is the top parent node.
        if (!parMap.containsKey(node)) {
            return -1;
        }

        Queue<Integer> queue = new LinkedList<>();
        queue.offer(node);
        Set<Integer> parSet = new HashSet<>();

        // key -> parNode, val: dist
        Map<Integer, Integer> parDistMap = new LinkedHashMap<>();
        parDistMap.put(node, 0);
        while (!queue.isEmpty()) {
            int child = queue.poll();
            if (!parMap.containsKey(child)) {
                continue;
            }
            Set<Integer> parents = parMap.get(child);
            parSet.addAll(parents);
            int dist = parDistMap.get(child);
            for (int parent : parents) {
                parDistMap.put(parent, dist+1);
                queue.offer(parent);
            }
        }
        int max = -1;
        int res = -1;
        for (int parent : parSet) {
            int dist = parDistMap.get(parent);
            if (dist > max) {
                max = dist;
                res = parent;
            }
        }
        return res;
    }

    @Test
    public void test1() {
        int[][] pairs = {{2,3},{3,15},{3,6},{5,6},{5,7},
                {4,5},{4,8},{4,9},{9,11},{14,4}};
        int res = findEarliestAncestor(pairs, 8);
        assertEquals(14, res);

        res = findEarliestAncestor(pairs, 7);
        assertEquals(14, res);

        res = findEarliestAncestor(pairs, 6);
        assertEquals(14, res);

        res = findEarliestAncestor(pairs, 15);
        assertEquals(2, res);

        res = findEarliestAncestor(pairs, 14);
        assertEquals(-1, res);
    }

    @Test
    public void test2() {
        int[][] pairs = {{2, 3}, {3, 15}, {3, 6}, {5, 6}, {5, 7},
                {4, 5}, {4, 8}, {4, 9}, {9, 11}, {14, 2}};
        int res = findEarliestAncestor(pairs, 8);
        assertEquals(4, res);

        res = findEarliestAncestor(pairs, 7);
        assertEquals(4, res);

        res = findEarliestAncestor(pairs, 6);
        assertEquals(14, res);

        res = findEarliestAncestor(pairs, 15);
        assertEquals(14, res);

        res = findEarliestAncestor(pairs, 14);
        assertEquals(-1, res);
    }

/**
门禁刷卡
1. 找进出记录不符的人
Given a list of people who enter and exit, find the people who entered without
their badge and who exited without their badge.

Input:
badge_records = [
    ["Martha",   "exit"],
    ["Paul",     "enter"],
    ["Martha",   "enter"],
    ["Martha",   "exit"],
    ["Jennifer", "enter"],
    ["Paul",     "enter"],
    ["Curtis",   "enter"],
    ["Paul",     "exit"],
    ["Martha",   "enter"],
    ["Martha",   "exit"],
    ["Jennifer", "exit"],
]

Output:
Expected output: ["Paul", "Curtis"], ["Martha"]
                       Enter            Exit
*/
    class Info {
        Set<String> invalidExitRecords;
        Set<String> invalidEnterRecords;
        public Info() {
            this.invalidExitRecords = new HashSet<>();
            this.invalidEnterRecords = new HashSet<>();
        }
    }

    static private final String ENTER = "enter";
    static private final String EXIT = "exit";

    // Running Time Complexity: O(N), Space Complexity: O(N)
    public Info getInvalidBadgeRecords(String[][] records) {
        Info info = new Info();
        // 0 -> enter, 1 -> exit
        // key -> name, val -> status:
        Map<String, Integer> map = new HashMap<>();
        for (String[] record : records) {
            String name = record[0];
            String action = record[1];
            if (!map.containsKey(name)) {
                map.put(name, 0);
            }
            if (action.equals(ENTER)) {
                if (map.get(name) == 0) {
                    map.put(name, 1);
                }
                else {
                    info.invalidEnterRecords.add(name);
                }
            }
            else if (action.equals(EXIT)) {
                if (map.get(name) == 1) {
                    map.put(name, 0);
                }
                else {
                    info.invalidExitRecords.add(name);
                }
            }
        }
        // In case there is a combination of "enter, exit, enter"
        for (String name : map.keySet()) {
            if (map.get(name) == 1) {
                info.invalidEnterRecords.add(name);
            }
        }
        return info;
    }

    @Test
    public void test1() {
        String[][] records = {
                {"A", "enter"},
                {"A", "exit"},
                {"A", "enter"}};
        Info info = getInvalidBadgeRecords(records);
        assertEquals(1, info.invalidEnterRecords.size());
        assertEquals(0, info.invalidExitRecords.size());
    }

    @Test
    public void test2() {
        String[][] records = {
                {"Martha",   "exit"},
                {"Paul",     "enter"},
                {"Martha",   "enter"},
                {"Martha",   "exit"},
                {"Jennifer", "enter"},
                {"Paul",     "enter"},
                {"Curtis",   "enter"},
                {"Paul",     "exit"},
                {"Martha",   "enter"},
                {"Martha",   "exit"},
                {"Jennifer", "exit"}};
        Info info = getInvalidBadgeRecords(records);
        assertEquals(2, info.invalidEnterRecords.size());
        assertTrue(info.invalidExitRecords.contains("Paul"));
        assertTrue(info.invalidExitRecords.contains("Curtis"));

        assertEquals(1, info.invalidExitRecords.size());
        assertTrue(info.invalidExitRecords.contains("Martha"));
    }

/**

2. 一小时内access多次
给 list of [name, time], time is string format: '1300' // 下午一点
return: list of names and the times where their swipe badges within one hour. if there are multiple intervals that satisfy the condition, return any one of them.
name1: time1, time2, time3...
name2: time1, time2, time3, time4, time5...

example:
input: [['James', '1300'], ['Martha', '1600'], ['Martha', '1620'], ['Martha', '1530']]
output: {
'Martha': ['1600', '1620', '1530']
}
*/

    public Map<String, List<Integer>> frequencyAccess(String[][] records) {
        Map<String, List<Integer>> res = new HashMap<>();

        Map<String, List<Integer>> map = new HashMap<>();
        for (String[] record : records) {
            String name = record[0];
            int time = Integer.parseInt(record[1]);
            List<Integer> list = map.getOrDefault(name, new ArrayList<>());
            list.add(time);
            map.put(name, list);
        }

        for (String name : map.keySet()) {
            List<Integer> times = map.get(name);
            Collections.sort(times, (t1, t2) -> timeDifference(t1, t2));
            List<Integer> timeWindows = new ArrayList<>();
            int i = 0;
            timeWindows.add(times.get(i));
            for (int j = 1, n = times.size(); j < n; j++) {
                int time1 = times.get(i);
                int time2 = times.get(j);
                if (timeDifference(time1, time2) < 60) {
                    timeWindows.add(time2);
                }
                else {
                    timeWindows.clear();
                    timeWindows.add(times.get(j));
                    i = j;
                }
            }
            if (timeWindows.size() >= 3) {
                res.put(name, timeWindows);
            }
        }
        return res;
    }

    private int timeDifference(int time1, int time2) {
        int hour1 = (int) Math.floor(time1 / 100);
        int hour2 = (int) Math.floor(time2 / 100);

        int minute1 = time1 % 100;
        int minute2 = time2 % 100;

        return hour1 * 60 + minute1 - (hour2 * 60 + minute2);
    }

    @Test
    public void test1() {
        String[][] records = {{"James", "1300"}, {"Martha", "1600"}, {"Martha", "1620"}, {"Martha", "1530"}};
        Map<String, List<Integer>> accessMap = frequencyAccess(records);
        for (String name : accessMap.keySet()) {
            System.out.println("name: " + name);
            List<Integer> times = accessMap.get(name);
            for (int time : times) {
                System.out.print(time + ",");
            }
            System.out.println();
        }
    }

/**
Meetings:
1. 是否有空余时间
第一题：类似meeting rooms，输入是一个int[][] meetings, int start, int end, 每个数都是时间，13：00 => 1300， 9：30 => 930， 看新的meeting 能不能安排到meetings
ex: {[1300, 1500], [930, 1200],[830, 845]}, 新的meeting[820, 830], return true; [1450, 1500] return false;

*/

    class Interval {
        int start;
        int end;
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public boolean canScheduleMeeting(int[][] meetings, int start, int end) {
        List<Interval> intervals = new ArrayList<>();
        for (int[] meeting : meetings) {
            int start = meeting[0];
            int end = meeting[1];
            intervals.add(new Interval(start, end));
        }
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        for (Interval interval : intervals) {
             // old new old  new
             // [   [    ]   ]
            if ((start >= interval.start && start < interval.end) ||
                // new old  new old
                // [   [     ]    ]
                (end >= interval.start && end < interval.end) ||
                // new old  old new
                // [    [   ]    ]
                (start < interval.start && end > interval.end)) {
                    return false;
                }
        }
        return true;
    }

/**
2. 返回空闲时间段:
类似merge interval，唯一的区别是输出，输出空闲的时间段，merge完后，再把两两个之间的空的输出就好，注意要加上0 - 第一个的start time
*/

    class Interval {
        int start;
        int end;
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public List<int[]> getAvailableIntervals(int[][] meetings) {
        List<Interval> intervals = new ArrayList<>();
        for (int[] meeting : meetings) {
            int start = meeting[0];
            int end = meeting[1];
            intervals.add(new Interval(start, end));
        }
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        List<Interval> mergedIntervals = new ArrayList<>();
        Interval prev = null;
        for (Interval interval : intervals) {
            // add non-overlap interval:
            if (prev == null || prev.end < interval.start) {
                mergedIntervals.add(interval);
                prev = interval;
            }
            else if (prev.end <= interval.end) {
                prev.end = interval.end;
            }
        }

        List<int[]> res = new ArrayList<>();
        prev = null;
        for (int i = 0, n = mergedIntervals.size(); i < n; i++) {
            Interval interval = mergedIntervals.get(i);
            if (prev == null) {
                res.add(new int[]{0, interval.start});
            }
            else {
                res.add(new int[]{prev.end, interval.start});
            }
            prev = interval;
        }
        return res;
    }

    @Test
    public void test1() {
        int[][] meetings = {{1,3}, {2,6}, {8,10}, {15,18}};
        List<int[]> res = getAvailableIntervals(meetings);
        for (int[] interval : res) {
            System.out.println("start: " + interval[0] + ", end: " + interval[1]);
        }
    }

/**
找宝藏:
Find legal moves:
1.    第一问就是给一个i和j，找出身边四个方向里为0的所有格子。

2. 找能去的所有0区域
给一个二维matrix，-1代表墙，0代表路。问给定一个起点坐标为0，是否能到达所有的0。
*/

    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};

    public boolean canReach(int[][] matrix, int x, int y) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        int m = matrix.length;
        int n = matrix[0].length;
        if (x < 0 || x >= m || y < 0 || y >= n) {
            return false;
        }
        boolean[][] visited = new boolean[m][n];
        dfs(matrix, x, y, m, n, visited);

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0 && !visited[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private void dfs(int[][] matrix, int i, int j, int m, int n, boolean[][] visited) {
        if (i < 0 || i >= m || j < 0 || j >= n || matrix[i][j] == -1 || visited[i][j]) {
            return;
        }
        visited[i][j] = true;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(matrix, x, y, m, n, visited);
        }
    }

/**
3. 最短路径找treasure:
board3 中1代表钻石，给出起点和终点，问有没有一条不走回头路的路线，能从起点走到终点，并拿走所有的钻石，给出所有的最短路径。

Input:
board3 = [
    [  1,  0,  0, 0, 0 ],
    [  0, -1, -1, 0, 0 ],
    [  0, -1,  0, 1, 0 ],
    [ -1,  0,  0, 0, 0 ],
    [  0,  1, -1, 0, 0 ],
    [  0,  0,  0, 0, 0 ],
]

treasure(board3, (5, 0), (0, 4)) -> None

treasure(board3, (5, 1), (2, 0)) ->
[(5, 1), (4, 1), (3, 1), (3, 2), (2, 2), (2, 3), (1, 3), (0, 3), (0, 2),
(0, 1), (0, 0), (1, 0), (2, 0)]
*/
    static private final int[][] dirs = {{0,1}, {0,-1}, {1,0}, {-1,0}};
    public List<int[]> findShortestPathOfTreasure(int[][] matrix, int[] start, int[] end) {
        List<int[]> res = new ArrayList<>();
        if (matrix == null || matrix.length == 0) {
            return res;
        }
        int numOfTreasure = 0;
        int m = matrix.length;
        int n = matrix[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 1) {
                    numOfTreasure++;
                }
            }
        }
        if (numOfTreasure == 0) {
            return res;
        }
        List<List<int[]>> paths = new ArrayList<>();
        List<int[]> path = new ArrayList<>();
        dfs(matrix, start[0], start[1], m, n, end, paths, path, numOfTreasure);

        if (paths.isEmpty()) {
            return res;
        }
        int minLen = m * n + 1;
        for (List<int[]> subPath : paths) {
            if (subPath.size() < minLen) {
                res = subPath;
                minLen = subPath.size();
            }
        }
        return res;
    }

    private void dfs(int[][] matrix, int i, int j, int m, int n, int[] end, List<List<int[]>> paths, List<int[]> path, int numOfTreasure) {
        if (i < 0 || i >= m || j < 0 || j >= n || matrix[i][j] == -1 || matrix[i][j] == 2) {
            return;
        }
        int prev = matrix[i][j];
        if (matrix[i][j] == 1) {
            numOfTreasure--;
        }
        // mark matrix = 2 as visited
        matrix[i][j] = 2;
        path.add(new int[]{i, j});
        if (i == end[0] && j == end[1] && numOfTreasure == 0) {
            paths.add(new ArrayList<>(path));
            path.remove(path.size()-1);
            matrix[i][j] = prev;
            return;
        }
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(matrix, x, y, m, n, end, paths, path, numOfTreasure);
        }
        path.remove(path.size()-1);
        matrix[i][j] = prev;
    }

    @Test
    public void test1() {
        int[][] matrix = {
                {  1,  0,  0, 0, 0 },
                {  0, -1, -1, 0, 0 },
                {  0, -1,  0, 1, 0 },
                { -1,  0,  0, 0, 0 },
                {  0,  1, -1, 0, 0 },
                {  0,  0,  0, 0, 0 }
        };
        int[] start = {5,0};
        int[] end = {0,4};
        List<int[]> res = findShortestPathOfTreasure(matrix, start, end);
        assertTrue(res.isEmpty());
    }

    @Test
    public void test2() {
        int[][] matrix = {
                {  1,  0,  0, 0, 0 },
                {  0, -1, -1, 0, 0 },
                {  0, -1,  0, 1, 0 },
                { -1,  0,  0, 0, 0 },
                {  0,  1, -1, 0, 0 },
                {  0,  0,  0, 0, 0 }
        };
        int[] start = {5,1};
        int[] end = {2,0};
        List<int[]> res = findShortestPathOfTreasure(matrix, start, end);
        assertFalse(res.isEmpty());
        for (int[] p : res) {
            System.out.print("(" + p[0] + "," + p[1] + "),");
        }
    }

/**
Sparse Vector:
1. 设计Sparse Vector:
sparseVector v = new sparseVector(100); //size constructor; size is 100.
 v.set(0, 1.0);
 v.set(3, 2.0);
 v.set(80,-4.5);
 System.out.println(v.get(80)); //should print -4.5
 System.out.println(v.get(50)); //should print 0.0
 try {
    System.out.println(v.get(101)); //error -- index out of range
    throw new IllegalStateException("We should not get here, an exception should have been thrown");
 } catch (IndexOutOfBoundsException t) {
    // success
 }
 System.out.println(v.toString()); //should print something like [1.0, 0.0, 0.0, 2.0, 0.0, ...]

Add these operations to your library: Addition, dot product, and cosine. Formulae for each are provided below;
we’re more interested in you writing the code than whether you’ve memorized the formula. For each operation, your code should throw an error if the two input vectors are not equal length.
Sample input/output:
//Note: This is pseudocode. Your actual syntax will vary by language.
v1 = new vector(5)
v1[0] = 4.0
v1[1] = 5.0

v2 = new vector(5)
v2[1] = 2.0
v2[3] = 3.0

v3 = new vector(2)
print v1.add(v2) // should print [4.0, 7.0, 0.0, 3.0, 0.0]
print v1.add(v3) // error -- vector lengths don’t match

print v1.dot(v2) // should print 10
print v1.dot(v3) // error -- vector lengths don’t match

print v1.cos(v2) //should print 0.433
print v1.cos(v3) //error -- vector lengths don’t match


Formulae:
Addition
a.add(b) = [a[0]+b[0], a[1]+b[1], a[2]+b[2], ...]
Dot product
a.dot(b) = a[0]*b[0] + a[1]*b[1] + a[2]*b[2] + ...

Cosine
a.cos(b) = a.dot(b) / (norm(a) * norm(b))
//norm(a) = sqrt(a[0]^2 + a[1]^2 + a[2]^2 + ...).
*/
    class SparseVector {
        Map<Integer, Double> map;
        int size;
        public SparseVector(int size) {
            this.map = new HashMap<>();
            this.size = size;
        }

        public SparseVector(Map<Integer, Double> map, int size) {
            this.map = map;
            this.size = size;
        }

        public void set(int idx, double val) {
            if (idx < 0 || idx >= size) {
                throw new IndexOutOfBoundsException("Index out of bound");
            }
            map.put(idx, val);
        }

        public SparseVector add(SparseVector v2) {
            if (this.size != v2.size) {
                throw new IllegalStateException("Error: vector lengths don't match");
            }
            Map<Integer, Double> v1Map = new HashMap<>(map);
            // merge two hashMap:
            for (int key : v2.map.keySet()) {
                double val2 = v2.map.get(key);
                double val1 = map.getOrDefault(key, 0.0);
                v1Map.put(key, val1+val2);
            }
            return new SparseVector(v1Map, this.size);
        }

        public int dot(SparseVector v2) {
            if (this.size != v2.size) {
                throw new IllegalStateException("Error: vector lengths don't match");
            }
            int res = 0;
            for (int key : v2.map.keySet()) {
                double val1 = map.getOrDefault(key, 0.0);
                double val2 = v2.map.get(key);
                res += (int) val1 * val2;
            }
            return res;
        }

        public double norm() {
            double res = 0;
            for (double val : map.values()) {
                res += Math.pow(val, 2);
            }
            res = Math.sqrt(res);
            return res;
        }

        public double cos(SparseVector v2) {
            if (this.size != v2.size) {
                throw new IllegalStateException("Error: vector lengths don't match");
            }
            double res = dot(v2) / (norm() * v2.norm());
            res = round(res, 3);
            return res;
        }

        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append("[");
            for (int i = 0; i < size; i++) {
                double val = 0;
                if (map.containsKey(i)) {
                    val = map.get(i);
                }
                sb.append(val + ", ");
            }
            if (size > 0) {
                sb.setLength(sb.length()-2);
            }
            sb.append("]");
            return sb.toString();
        }

        private double round(double value, int places) {
            if (places < 0) {
                throw new IllegalArgumentException();
            }

            BigDecimal bd = BigDecimal.valueOf(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
    }

    @Test
    public void test1() {
        SparseVector v1 = new SparseVector(5);
        v1.set(0, 4.0);
        v1.set(1, 5.0);

        SparseVector v2 = new SparseVector(5);
        v2.set(1, 2.0);
        v2.set(3, 3.0);

        SparseVector v3 = new SparseVector(2);
        System.out.println(v1.add(v2));

        System.out.println(v1.dot(v2));

        System.out.println(v1.cos(v2));
    }

/**
1. input是String[] dictionary, String s. 求出s对应的dictionary中的单词
 比如input是["cat", "dog"], s是tcajlkfjd, 就返回cat
*/



/**
2. Leetcode 79, 但是要打印出路径所有的点,返回 List<int[]>
https://leetcode.com/problems/word-search/
给一个word让你找，输出path
*/
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public List<int[]> findPath(char[][] board, String word) {
        List<int[]> path = new ArrayList<>();
        if (board == null || board.length == 0) {
            return path;
        }
        int m = board.length;
        int n = board[0].length;
        boolean[][] visited = new boolean[m][n];
        List<List<int[]>> paths = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                dfs(board, i, j, m, n, visited, 0, word, path, paths);
            }
        }
        if (paths.isEmpty()) {
            return path;
        }
        return paths.get(0);
    }

    private void dfs(char[][] board, int i, int j, int m, int n, boolean[][] visited, int idx, String word, List<int[]> path, List<List<int[]>> paths) {
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j] || board[i][j] != word.charAt(idx)) {
            return;
        }
        visited[i][j] = true;
        path.add(new int[]{i,j});
        idx++;
        if (idx == word.length()) {
            paths.add(new ArrayList<>(path));
        }
        else {
            for (int[] dir : dirs) {
                int x = i + dir[0];
                int y = j + dir[1];
                dfs(board, x, y, m, n, visited, idx, word, path, paths);
            }
        }
        visited[i][j] = false;
        path.remove(path.size()-1);
    }

    @Test
    public void test() {
        char[][] board = {
                {'A','B','C','E'},
                {'S','F','C','S'},
                {'A','D','E','E'}
        };
        System.out.println("word: ABCCED");
        List<int[]> res = findPath(board, "ABCCED");
        for (int[] p : res) {
            System.out.println("x: " + p[0] + ", y:" + p[1]);
        }

        System.out.println("word: SEE");
        res = findPath(board, "SEE");
        for (int[] p : res) {
            System.out.println("x: " + p[0] + ", y:" + p[1]);
        }
    }

/***
Friend Cycle:
Pt 1.Given employees and friendships, find all adjacencies that denote the friendship,
A friendship is bi-directional/mutual so if 1 is friends with 2, 2 is also friends with 1.

Input:
employees = [
  "1, Bill, Engineer",
  "2, Joe, HR",
  "3, Sally, Engineer",
  "4, Richard, Business",
  "6, Tom, Engineer"
]

friendships = [
  "1, 2",
  "1, 3",
  "3, 4"
]

Output:

1: 2, 3
2: 1
3: 1, 4
4: 3
6: None
*/
    // Running Time Complexity: O(n^2) relationship between n employees,
    // Space Complexity:O(n) space
    public List<String> friendCycle(String[] employees, String[] friendships) {
        Set<Integer> userIds = new LinkedHashSet<>();
        for (String employee : employees) {
            String[] texts = employee.split(", ");
            int userId = Integer.parseInt(texts[0]);
            userIds.add(userId);
        }

        Map<Integer, List<Integer>> friendMap = new LinkedHashMap<>();
        for (String friendship : friendships) {
            String[] texts = friendship.split(", ");
            int userId1 = Integer.parseInt(texts[0]);
            int userId2 = Integer.parseInt(texts[1]);
            List<Integer> list1 = friendMap.getOrDefault(userId1, new ArrayList<>());
            list1.add(userId2);
            friendMap.put(userId1, list1);

            List<Integer> list2 = friendMap.getOrDefault(userId2, new ArrayList<>());
            list2.add(userId1);
            friendMap.put(userId2, list2);
        }

        List<String> res = new ArrayList<>();
        for (int userId : userIds) {
            if (!friendMap.containsKey(userId)) {
                res.add(userId + ": None");
            }
            else {
                List<Integer> friends = friendMap.get(userId);
                StringBuffer sb = new StringBuffer();
                sb.append(userId + ": ");
                for (int friendId : friends) {
                    sb.append(friendId + ", ");
                }
                sb.setLength(sb.length()-2);
                res.add(sb.toString());
            }
        }
        return res;
    }

    @Test
    public void test1() {
        String[] employees = {"1, Bill, Engineer",
                "2, Joe, HR",
                "3, Sally, Engineer",
                "4, Richard, Business",
                "6, Tom, Engineer"};
        String[] friendships = {"1, 2",
                "1, 3",
                "3, 4"};
        List<String> res = friendCycle(employees, friendships);
        for (String text : res) {
            System.out.println(text);
        }
    }

/**
Now for each department count the number of employees that have a friend in another department

employees = [
  "1, Bill, Engineer",
  "2, Joe, HR",
  "3, Sally, Engineer",
  "4, Richard, Business",
  "6, Tom, Engineer"
]

friendships = [
  "1, 2",
  "1, 3",
  "3, 4"
]

1: 2, 3
2: 1
3: 1, 4
4: 3
6: None

Sample Output:
Output:
"Engineer: 2 of 3"
"HR: 1 of 1"
"Business: 1 of 1"
*/
    // Running Time Complexity: O(n^2), Space Complexity: O(n)
    public List<String> friendCycle2(String[] employees, String[] friendships) {
        Map<String, Integer> departmentCountMap = new LinkedHashMap<>();

        Map<Integer, String> userDepartmentMap = new LinkedHashMap<>();
        Set<Integer> userIds = new LinkedHashSet<>();
        for (String employee : employees) {
            String[] texts = employee.split(", ");
            int userId = Integer.parseInt(texts[0]);
            userIds.add(userId);

            String department = texts[2];
            int cnt = departmentCountMap.getOrDefault(department, 0);
            cnt++;
            departmentCountMap.put(department, cnt);

            userDepartmentMap.put(userId, department);
        }

        Map<Integer, List<Integer>> friendMap = new LinkedHashMap<>();

        for (String friendship : friendships) {
            String[] texts = friendship.split(", ");
            int userId1 = Integer.parseInt(texts[0]);
            int userId2 = Integer.parseInt(texts[1]);
            List<Integer> list1 = friendMap.getOrDefault(userId1, new ArrayList<>());
            list1.add(userId2);
            friendMap.put(userId1, list1);
            List<Integer> list2 = friendMap.getOrDefault(userId2, new ArrayList<>());
            list2.add(userId1);
            friendMap.put(userId2, list2);
        }

        Map<String, Integer> otherDepartmentFriendCountMap = new LinkedHashMap<>();
        for (int userId1 : userIds) {
            String department1 = userDepartmentMap.get(userId1);
            if (!friendMap.containsKey(userId1)) {
                continue;
            }

            List<Integer> friends = friendMap.get(userId1);
            boolean found = false;
            for (int userId2 : friends) {
                String department2 = userDepartmentMap.get(userId2);
                if (!department1.equals(department2)) {
                    found = true;
                    int val = otherDepartmentFriendCountMap.getOrDefault(department1, 0);
                    val++;
                    otherDepartmentFriendCountMap.put(department1, val);
                    break;
                }
            }
            if (!found) {
                otherDepartmentFriendCountMap.put(department1, 0);
            }
        }

        List<String> res = new ArrayList<>();
        for (String department : departmentCountMap.keySet()) {
            StringBuffer sb = new StringBuffer();
            int total = departmentCountMap.get(department);
            int cnt = otherDepartmentFriendCountMap.getOrDefault(department, 0);
            sb.append(department + ": " + cnt + " of " + total);
            res.add(sb.toString());
        }
        return res;
    }

    @Test
    public void test2() {
        String[] employees = {"1, Bill, Engineer",
                "2, Joe, HR",
                "3, Sally, Engineer",
                "4, Richard, Business",
                "5, Sam, Solution",
                "6, Tom, Engineer",
                "7, John, Solution"};
        String[] friendships = {"1, 2",
                "1, 3", "3, 4", "5, 7"};

        List<String> res = friendCycle2(employees, friendships);
        for (String text : res) {
            System.out.println(text);
        }
    }

/**
Pt 3.Output if all the employees are in a same friend cycle.

employees = [
  "1, Bill, Engineer",
  "2, Joe, HR",
  "3, Sally, Engineer",
  "4, Richard, Business",
  "6, Tom, Engineer"
]

friendships = [
  "1, 2",
  "1, 3",
  "3, 4"
]
*/

public boolean isAllEmployeeOnSameCycle(String[] employees, String[] friendships) {
    List<Integer> userIds = new ArrayList<>();
    Map<Integer, Boolean> visited = new LinkedHashMap<>();
    for (String employee : employees) {
        String[] texts = employee.split(", ");
        int userId = Integer.parseInt(texts[0]);
        userIds.add(userId);
        visited.put(userId, false);
    }

    Map<Integer, List<Integer>> friendMap = new LinkedHashMap<>();

    for (String friendship : friendships) {
        String[] texts = friendship.split(", ");
        int userId1 = Integer.parseInt(texts[0]);
        int userId2 = Integer.parseInt(texts[1]);
        List<Integer> list1 = friendMap.getOrDefault(userId1, new ArrayList<>());
        list1.add(userId2);
        friendMap.put(userId1, list1);
        List<Integer> list2 = friendMap.getOrDefault(userId2, new ArrayList<>());
        list2.add(userId1);
        friendMap.put(userId2, list2);
    }
}

/**
Leetcode 133 Clone Graph:
https://leetcode.com/problems/clone-graph
*/
    // Solution 1: DFS:
    private Map<Integer, Node> map = new HashMap<>();

    public Node cloneGraph(Node node) {
        return cloneNode(node);
    }

    private Node cloneNode(Node node) {
        if (node == null) {
            return null;
        }
        if (map.containsKey(node.val)) {
            return map.get(node.val);
        }

        Node clonedNode = new Node(node.val);
        map.put(node.val, clonedNode);

        for (Node neighbor : node.neighbors) {
            Node clonedNeighbor = cloneNode(neighbor);
            clonedNode.neighbors.add(clonedNeighbor);
        }
        return clonedNode;
    }

    // Solution: BFS:
    public Node cloneGraph(Node node) {
        if (node == null) {
            return node;
        }
        Node root = new Node(node.val, new ArrayList<>());
        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);
        Map<Node, Node> map = new LinkedHashMap<>();
        map.put(node, root);

        while (!queue.isEmpty()) {
            Node curr = queue.poll();
            // handle the neighbors:
            for (Node neighbor : curr.neighbors) {
                if (!map.containsKey(neighbor)) {
                    // clone the neighbor:
                    Node clonedNeighbor = new Node(neighbor.val, new ArrayList<>());
                    map.put(neighbor, clonedNeighbor);
                    queue.add(neighbor);
                }
                map.get(curr).neighbors.add(map.get(neighbor));
            }
        }
        return root;
    }