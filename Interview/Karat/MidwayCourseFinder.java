import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MidwayCourseFinder {

    public List<String> findAllMidWayCourse(String[][] allCourses) {
        // build graph:
        // convert course to integer:
        int idx = 0;
        // key -> course name, val -> courseId
        Map<String, Integer> courseMap = new LinkedHashMap<>();
        // key -> courseId, val -> course name:
        Map<Integer, String> courseIdMap = new LinkedHashMap<>();
        for (String[] course : allCourses) {
            String from = course[0];
            String to = course[1];
            if (!courseMap.containsKey(from)) {
                courseMap.put(from, idx);
                courseIdMap.put(idx, from);
                idx++;
            }
            if (!courseMap.containsKey(to)) {
                courseMap.put(to, idx);
                courseIdMap.put(idx, to);
                idx++;
            }
        }

        int n = courseMap.keySet().size();
        int[] indegree = new int[n];
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }

        for (String[] course : allCourses) {
            String from = course[0];
            String to = course[1];
            int fromIdx = courseMap.get(from);
            int toIdx = courseMap.get(to);
            graph.get(fromIdx).add(toIdx);
            indegree[toIdx]++;
        }

        Set<String> set = new LinkedHashSet<>();
        for (int i = 0; i < n; i++) {
            if (indegree[i] != 0) {
                continue;
            }
            boolean[] visited = new boolean[n];
            List<List<Integer>> paths = new ArrayList<>();
            dfs(graph, i, visited, paths, new ArrayList<>());
            for (List<Integer> path : paths) {
                int m = path.size();
                // midway course arr idx:
                int j = (m-1) / 2;
                // midway course id:
                int courseId = path.get(j);
                String midCourse = courseIdMap.get(courseId);
                set.add(midCourse);
            }
        }

        return new ArrayList<>(set);
    }

    private void dfs(List<List<Integer>> graph, int from, boolean[] visited,
                     List<List<Integer>> paths, List<Integer> path) {
        if (visited[from]) {
            return;
        }
        visited[from] = true;
        path.add(from);
        List<Integer> neighbors = graph.get(from);
        // if no new out going neighbors, we know it is the leaf,
        // or ending point of the graph
        if (neighbors.isEmpty()) {
            paths.add(new ArrayList<>(path));
        }
        else {
            for (int neighbor : neighbors) {
                dfs(graph, neighbor, visited, paths, path);
            }
        }
        path.remove(path.size()-1);
        visited[from] = false;
    }

    @Test
    public void test1() {
        String[][] courses = {
                {"Logic", "COBOL"},
                {"Data Structures", "Algorithms"},
                {"Creative Writing", "Data Structures"},
                {"Algorithms", "COBOL"},
                {"Intro to Computer Science", "Data Structures"},
                {"Logic", "Compilers"},
                {"Data Structures", "Logic"},
                {"Creative Writing", "System Administration"},
                {"Databases", "System Administration"},
                {"Creative Writing", "Databases"},
                {"Intro to Computer Science", "Graphics"},
        };
        List<String> midCourses = findAllMidWayCourse(courses);
        for (String course : midCourses) {
            System.out.println(course);
        }
    }
}
