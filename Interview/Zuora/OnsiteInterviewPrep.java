/**
Nicolas Fusseder: Hiring Manager
You've met with Nick before, but this time he will do a coding interview with you. Very similar format to what you did with Bryan. Some sort of real world problem data structures, algorithms in Java. Be confident, ask lots of questions, don't be discouraged if something doesn't go your way. The point is to test your limit :) He likes to ask questions like standard adjacency list question so be prepared for things like that.

Tom Hartwell: Another Manager
Tom will focus on a design/architecture conversation. He'll want to learn about what you are currently working on. Be specific - talk about "you" not the "team." He asks things like REST API for a Public Library -- so be prepared for something similar.

Kate McNally: Product Manager
Kate is a product manager and will talk to you about the relationship between product and engineering. This is more cultural and will give you a chance to ask a lot of questions and get to Zuora's culture.

1.Here's the company's website and crunchbase link for more details about them:

2.Learn the company mission and be prepared to ask questions expressing interest and enthusiasm. Do your interests/goals align with theirs?
3.Navigate their website, check their github (if available) - What problems are they solving? Any correlation to your interests and contributions you could potentially make?
4.Be prepared to express genuine and unique interests about their company and alignment. They will ask why you are interested in them and priorities for your next position.
5.Take away data points, good and not so good, and roll them into your future interviews.

Pros:
1. API:
2. Automation
3. Easy to use
4. Plenty of Integration
5. Fast

Problems of the products:
1. UI needs to be updated.
2. Report capacity
3. Complex System
4. Searching/Reporting
5. Tons of settings and configuations.
6. SOAP API / REST API are not comparable
*/
/**



*/