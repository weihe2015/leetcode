/**
Q1. Valid Square:
给平面上四个点，判断是否能组成一个正方形。每个点是由（x，y）坐标表示。
Leetcode 593: Valid Square: https://leetcode.com/problems/valid-square/
*/
// http://www.cs.uu.nl/research/techreps/repo/CS-1989/1989-10.pdf
    public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        List<Integer> distances = new ArrayList<>();
        distances.add(getDistance(p1, p2));
        distances.add(getDistance(p1, p3));
        distances.add(getDistance(p1, p4));
        distances.add(getDistance(p2, p3));
        distances.add(getDistance(p2, p4));
        distances.add(getDistance(p3, p4));

        int side = distances.get(0);
        int diag = 0;
        // key -> distance, val -> freq
        Map<Integer, Integer> map = new HashMap<>();
        for (int distance : distances) {
            side = Math.min(side, distance);
            diag = Math.max(diag, distance);
            map.put(distance, map.getOrDefault(distance, 0) + 1);
        }
        return map.keySet().size() == 2 && map.get(side) == 4 && map.get(diag) == 2;
    }

    private int getDistance(int[] p, int[] q) {
        int x = p[0] - q[0];
        int y = p[1] - q[1];
        return x * x + y * y;
    }

// Solution 2:
    class Point {
        double x;
        double y;
        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        Point pp1 = new Point(p1[0], p1[1]);
        Point pp2 = new Point(p2[0], p2[1]);
        Point pp3 = new Point(p3[0], p3[1]);
        Point pp4 = new Point(p4[0], p4[1]);

        List<Point> points = Arrays.asList(pp1, pp2, pp3, pp4);
        Set<String> set = new HashSet<>();
        for (Point p : points) {
            String key = convertPointToKey(p);
            if (!set.contains(key)) {
                set.add(key);
            }
        }
        if (set.size() != 4) {
            return false;
        }

        Point p = forthPointFormSquare(pp1, pp2, pp3);
        return p != null && p.x == pp4.x && p.y == pp4.y;
    }

    // Follow up: 给n个点，问可以组成多少个valid square，要求先O(n^4)，再改进到O(n^3)，最后改进到 O(n^2)
    // Running Time Complexity: O(N^4), Space Complexity: O(N)
    public int getNumOfValidSquaresN4(List<Point> points) {
        int cnt = 0;
        // remove duplicates:
        Set<String> set = new HashSet<>();
        List<Point> uniquePoints = new ArrayList<>();
        for (Point p : points) {
            String key = convertPointToKey(p);
            if (!set.contains(key)) {
                set.add(key);
                uniquePoints.add(p);
            }
        }
        set.clear();
        // O(N^4)
        int n = uniquePoints.size();
        for (int i = 0; i < n-3; i++) {
            for (int j = i+1; j < n-2; j++) {
                for (int k = j+1; k < n-1; k++) {
                    for (int l = k+1; l < n; l++) {
                        Point p1 = uniquePoints.get(i);
                        Point p2 = uniquePoints.get(j);
                        Point p3 = uniquePoints.get(k);
                        Point p4 = uniquePoints.get(l);
                        if (!validSquare(p1, p2, p3, p4)) {
                            continue;
                        }
                        String key = convertPointsToString(Arrays.asList(p1, p2, p3, p4));
                        if (!set.contains(key)) {
                            set.add(key);
                            cnt++;
                        }
                    }
                }
            }
        }
        return cnt;
    }

    // Running Time Complexity: O(N^3), Space Complexity: O(N)
    public int numOfSquaresN3(List<Point> points) {
        int cnt = 0;
        // remove duplicates:
        Set<String> pointKeySet = new HashSet<>();
        List<Point> uniquePoints = new ArrayList<>();
        for (Point p : points) {
            String key = convertPointToKey(p);
            if (!pointKeySet.contains(key)) {
                pointKeySet.add(key);
                uniquePoints.add(p);
            }
        }
        Set<String> fourPointsKeySet = new HashSet<>();
        // O(N^3)
        int n = uniquePoints.size();
        for (int i = 0; i < n-3; i++) {
            for (int j = i+1; j < n-2; j++) {
                for (int k = j+1; k < n-1; k++) {
                    Point p1 = uniquePoints.get(i);
                    Point p2 = uniquePoints.get(j);
                    Point p3 = uniquePoints.get(k);
                    Point p4 = forthPointFormSquare(p1, p2, p3);
                    String key = convertPointToKey(p4);
                    if (pointKeySet.contains(key)) {
                        String fullKey = convertPointsToString(Arrays.asList(p1, p2, p3, p4));
                        if (!fourPointsKeySet.contains(fullKey)) {
                            fourPointsKeySet.add(fullKey);
                            cnt++;
                        }
                    }
                }
            }
        }
        return cnt;
    }

    private String convertPointToKey(Point point) {
        StringBuffer sb = new StringBuffer();
        sb.append(point.x);
        sb.append("-");
        sb.append(point.y);
        return sb.toString();
    }

    private Point forthPointFormSquare(Point p1, Point p2, Point p3) {
        // p1 is the corner point:
        if (isPointValidCornerPoint(p1, p2, p3)) {
            // validate if vector P3_1 is the same length of P2_1
            return createNewPoint(p1, p2, p3);
        }
        // validate if vector P3_1 is the same length of P2_1
        // p2 is the corner point:
        else if (isPointValidCornerPoint(p2, p1, p3)) {
            // validate if vector P3_1 is the same length of P2_1
            return createNewPoint(p2, p1, p3);
        }
        // p3 is the corner point
        else if (isPointValidCornerPoint(p3, p1, p2)) {
            return createNewPoint(p3, p1, p2);
        }
        return null;
    }

    private boolean isPointValidCornerPoint(Point p1, Point p2, Point p3) {
        int res = (p1.x - p3.x) * (p2.x - p3.x) + (p1.y - p3.y) * (p2.y - p3.y);
        return res == 0 && getDistance(p1, p3) == getDistance(p2, p3);
    }

    private double getDistance(Point p1, Point p2) {
        double x = p1.x - p2.x;
        double y = p1.y - p2.y;
        return x * x + y * y;
    }

    // P1 is the corner point:
    private Point createNewPoint(Point p1, Point p2, Point p3) {
        double x = (p2.x - p1.x) + (p3.x - p1.x) + p1.x;
        double y = (p2.y - p1.y) + (p3.y - p1.y) + p1.y;
        return new Point(x, y);
    }

    // O(N^2)
    public int numOfSquaresN2(List<Point> points) {
        int cnt = 0;
        // remove duplicates:
        Set<String> pointKeySet = new HashSet<>();
        List<Point> uniquePoints = new ArrayList<>();
        for (Point p : points) {
            String key = convertPointToKey(p);
            if (!pointKeySet.contains(key)) {
                pointKeySet.add(key);
                uniquePoints.add(p);
            }
        }
        Set<String> fourPointsKeySet = new HashSet<>();
        for (int i = 0, n = uniquePoints.size(); i < n-1; i++) {
            for (int j = i+1; j < n; j++) {
                Point p1 = uniquePoints.get(i);
                Point p2 = uniquePoints.get(j);
                Point[] res = getDiagonalPoints(p1, p2);
                Point p3 = res[0];
                Point p4 = res[1];
                String p3Key = convertPointToKey(p3);
                String p4Key = convertPointToKey(p4);
                if (pointKeySet.contains(p3Key) && pointKeySet.contains(p4Key)) {
                    String fullKey = convertPointsToString(Arrays.asList(p1, p2, p3, p4));
                    if (!fourPointsKeySet.contains(fullKey)) {
                        fourPointsKeySet.add(fullKey);
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }
    private Point[] getDiagonalPoints(Point p1, Point p2) {
        Point[] res = new Point[2];
        double midX = (p1.x + p2.x) / 2.0;
        double midY = (p1.y + p2.y) / 2.0;

        double xx = p1.x - midX;
        double yy = p1.y - midY;

        double p3x = (yy + midX);
        double p3y = (-xx + midY);
        res[0] = new Point(p3x, p3y);

        double p4x = (-yy + midX);
        double p4y = (xx + midY);
        res[1] = new Point(p4x, p4y);
        return res;
    }


/**
Q2: Happy Number:
// Leetcode 202 Happy Number
// https://leetcode.com/problems/happy-number
*/
    /**
    Surprisingly, we can apply the Floyd Cycle Detection (the one we used in Detect Linked List Cycle) on this problem: think of what is a cycle in this case:

    from a number A, we can get to another B using the ways given in this case
    from number B, when we doing the transformation, we will eventually get back to B again ---> this forms a cycle (infinite loop)

    for example:
    1^2 + 1^2 = 2
    2^2 = 4 ------> notice that from here we are starting with 4
    4^2 = 16
    1^2 + 6^2 = 37
    3^2 + 7^2 = 58
    5^2 + 8^2 = 89
    8^2 + 9^2 = 145
    1^2 + 4^2 + 5^2 = 42
    4^2 + 2^2 = 20
    2^2 + 0^2 = 4 -------> notice that we just get back to 4 again

    Using Floyd Cycle Detection algorithm (fast and slow pointer), we will be able to actually get the value of B. Then the rest of task would be very simple, we simply check whether this value will be 1 or not.

    You may ask: what if value "1" also appears in the cycle and we are skipping over it. Well, in that case, the value that slow and fast are equal to will be 1, as transformation of 1 is still 1, so we still cover this case.
    7 -> 49 -> 97 -> 130 -> 10 -> 1
    4 -> 16 -> 37 -> 58 -> 89 -> 145 -> 42 -> 20 -> 4
    */
    // adapt the Floyd Cycle detection algorithm
    public boolean isHappy(int n) {
        int slow = n;
        int fast = n;
        do {
            slow = digitSquareSum(slow);
            fast = digitSquareSum(fast);
            fast = digitSquareSum(fast);
        } while (slow != fast);

        return slow == 1;
    }

    private int digitSquareSum(int n) {
        int sum = 0;
        while (n > 0) {
            int digit = n % 10;
            sum += digit * digit;
            n /= 10;
        }
        return sum;
    }
    /**
    true  (1) -> 1
	false (2) -> 4 -> 16 -> 37 -> 58 -> 89 -> 145 -> 42 -> 20 -> 4
	false (3) -> 9 -> 81 -> 65 -> 61 -> 37 (look at 2)
	false (4) -> (look at 2)
	false (5) -> 25 -> 29 -> 85 -> 89 (look at 2)
	false (6) -> 36 -> 45 -> 41 -> 17 -> 50 -> 25  (look at 5)
	true  (7) -> 49 -> 97 -> 10
	false (8) -> 64 -> 52 -> 29 (look at 5)
	false (9) -> 9 -> 81 -> 65 (look at 3)

	All other n >= 10, while computing will become [1-9],
	So there are two cases 1 and 7 which are true.
    and when n == 4, it automatically leads to false
    */
    public boolean isHappy(int n) {
        while (true) {
            n = digitSquareSum(n);
            if (n == 1) {
                break;
            }
            else if (n == 4) {
                return false;
            }
        }
        return true;
    }

    public boolean isHappy(int n) {
        if (n == 1) {
            return true;
        }
        else if (n == 4) {
            return false;
        }
        n = digitSquareSum(n);
        return isHappy(n);
    }

    private int digitSquareSum(int n) {
        int sum = 0;
        while (n > 0) {
            int digit = n % 10;
            sum += digit * digit;
            n /= 10;
        }
        return sum;
    }

/**
Concurrency:
Multi-threading: multiple threads of execution inside the program
Shared State vs Separated State.
Shared State: race condition and deadlock
*/
// Creating and Staring Threads:
import java.lang.Thread;

Thread thread = new Thread();
thread.start();
// Thread subclass:
public class MyThread extends Thread {
    public void run() {
        //...
    }
}
MyThread myThread = new MyThread();
myThread.start();

// anonymous subclass:
Thread thread = new Thread(){
    public void run() {
        /// ....
    }
}
thread.start();

// Runnable Interface Implementation
public interface Runnable() {
    public void run();
}

public class MyRunnable implements Runnable {
    public void run() {
        // xxx
    }
}

Runnable myRunnable = new Runnable() {
    public void run() {
        // xxx
    }
}

// Lambda Implementation:
Runnable runnable = () -> {
    // xxx
};

// Thread Names:
MyRunnable runnable = new MyRunnable();
Thread thread = new Thread(runnable, "New Thread Name");
thread.start();
String name = thread.getName();

// Starting a Thread with a Runnable:
Runnable runnable = new MyRunnable();
Thread thread = new Thread(runnable);
thread.start();

// Thread.currentThread():
// The Thread.currentThread() method returns a reference to the Thread instance executing currentThread()

// Local variable: stored in each thread's own stack. all local primitive variables are thread safe
// Local object: object referenced are stored in the shared heap. No thread safe
// object member variables: they are stored on the heap along with object.

/*
We want to implement a callback mechanism that allows listerners to register a function that will be invoked when the event fires.

The API functions are register_callback and event_fired.
1. There is ony one event and it will fire only once.
2. Callbacks registered before the event fires shouldn't block waiting for the event to fire.
3. Callbacks registered after the event fires should get invoked ASAP.

*/
public class Callback {

    public void call() {

    }
}
// Single thread version:
public class Event {
    private Queue<Callback> eventQueue;
    private boolean isFired;

    public Event() {
        this.eventQueue = new LinkedList<>();
        this.isFired = false;
    }

    public void register_cb(Callback cb) {
        if (isFired) {
            cb.call();
        } else {
            this.eventQueue.offer(cb);
        }
    }

    public void fire() {
        this.isFired = true;
        for (Callback cb : this.eventQueue) {
            cb.call();
        }
        eventQueue.clear();
    }
}
// multiple threads version:
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BlockingEvent {
    private Queue<Callback> eventQueue;
    private boolean isFired;

    private final ReentrantLock lock = new ReentrantLock();

    public BlockingEvent() {
        this.eventQueue = new LinkedList<>();
        this.lock = new ReentrantLock();
        this.isFired = false;
    }

    public void register_cb(Callback cb) throws InterruptedException {
        lock.lock();
        try
        {
            if (!this.isFired) {
                this.eventQueue.offer(cb);
            } else {
                cb.call();
            }
        }
        finally {
            lock.unlock();
        }
    }

    public void fire() throws InterruptedException {
        lock.lock();
        try {
            this.isFired = true;
            while (!this.eventQueue.isEmpty()) {
                CallbackThread cbt = this.eventQueue.poll();
                cbt.call();
            }
        }
        finally {
            lock.unlock();
        }
    }

    @Test
    public void test1()
    {
        try
        {
            BlockingEvent be = new BlockingEvent();

            Callback t1 = new Callback("Thread1");
            Callback t2 = new Callback("Thread2");
            Callback t3 = new Callback("Thread3");

            be.register_cb(t1);
            be.register_cb(t2);
            be.fire();

            be.register_cb(t3);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

/**
Version 4: no lock, but register comes in before the while loop of fire() is finished,
it will be called instantly.
*/
class BlockingEvent {
    private Deque<CallBack> queue;
    private boolean isFired;

    public BlockingEvent() {
        this.queue = new ArrayDeque<>();
        this.isFired = false;
    }

    public void fire() {
        this.isFired = true;
        while (!this.queue.isEmpty()) {
            Callback cb = this.queue.pollLast();
            cb.call();
        }
    }

    public void register_cb(Callback newCallBack) {
        if (this.isFired) {
            newCallBack.call();
        }
        else {
            queue.offerFirst(newCallBack);
            if (this.isFired) {
                if (!this.queue.isEmpty()) {
                    Callback cb = this.queue.pollLast();
                    cb.call();
                }
            }
        }
    }
}

/**
    Leetcode 146: LRU Cache:
    https://leetcode.com/problems/lru-cache/
*/
class LRUCache {
    int capacity;
    Map<Integer, Integer> map;
    // The item in the front of Deque is the most frequently used item.
    // The item at the end of Deque is the least frequently used item.
    Deque<Integer> orderList;
    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.orderList = new LinkedList<>();
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        int val = map.get(key);
        if (orderList.peekFirst() != key) {
            // make this key on the top of the order list queue:
            pushKeyToTopAsFrequentUsed(key);
        }
        return val;
    }

    public void put(int key, int value) {
        if (capacity > 0) {
            // When this item is in the map, we update this item in the order list, but keep the number of capacity the same
            if (map.containsKey(key)) {
            	orderList.remove(key);
            }
            // If this item is not in the map, we decrement the number of capacity
            else {
                capacity--;
            }
            orderList.offerFirst(key);
        }
        else {
        	// If this item is not in the map, evict the least used item:
        	if (!map.containsKey(key)) {
        		int prevKey = orderList.pollLast();
        		map.remove(prevKey);
        		orderList.offerFirst(key);
        	}
        	// If this item is in the map, we update this item to the top of the deque:
        	else {
                // make this key on the top of the order list queue:
        		pushKeyToTopAsFrequentUsed(key);
        	}
        }
        map.put(key, value);
    }

    // simple version of put:
    public void put(int key, int value) {
        if (!map.containsKey(key)) {
            if (capacity > 0) {
                capacity--;
            }
            else {
                int prevKey = orderList.pollLast();
                map.remove(prevKey);
            }
        }
        map.put(key, value);
        pushKeyToTopAsFrequentUsed(key);
    }

    private void pushKeyToTopAsFrequentUsed(int key) {
        orderList.remove(key);
        orderList.offerFirst(key);
    }
}

/**
   https://www.youtube.com/watch?v=4wVp97-uqr0
    Time Complexity for PUT: O(1), GET: O(1)
    Idea: Use HashMap to store key value, and the value will be the DoubleLinkedListNode
    Use a doubleLinkedList to have head and tail pointer.
    1. PUT:
         If the key is already existed in the map, we get the DoubleLinkedListNode
            , update its value and push it to the header
         If the key is not in the map:
            1.1 If there is still have capacity:
                We create a new DoubleLinkedListNode, decrement the capacity counter and push it to the header
            1.2 If there is no more capacity:
                We evict the tail node, remove the key of this tail node from the map, create a new DoubleLinkedListNode and push it to the header
    2. GET:
         If the map does not contain this key: return -1
         If the map contains this key:
             2.1 Get this DoubleLinkedListNode from HashMap, push it to the header of the list, and return the value
*/
class LRUCache {
    class DoubleLinkedListNode {
        DoubleLinkedListNode prev, next;
        int key;
        int val;
        public DoubleLinkedListNode() {

        }
        public DoubleLinkedListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    private int capacity;
    // key: original key value from put(key, value), value: the DoubleLinkedListNode
    private Map<Integer, DoubleLinkedListNode> map;
    private DoubleLinkedListNode head;
    private DoubleLinkedListNode tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.head = new DoubleLinkedListNode();
        this.tail = new DoubleLinkedListNode();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        DoubleLinkedListNode node = map.get(key);
        int val = node.val;
        // If the first item is not this key: we pop it to the top of the list:
        if (head.next.key != key) {
            // remove itself:
            unLinkNode(node);
            addNodeToFront(node);
        }
        return val;
    }

    public void put(int key, int value) {
        DoubleLinkedListNode node = null;
        if (!map.containsKey(key)) {
            if (capacity > 0) {
                capacity--;
            }
            else {
                // Remove the tail node from the list
                int prevKey = removeTailNode();
                // Remove this key from HashMap
                this.map.remove(prevKey);
            }
            node = new DoubleLinkedListNode(key, value);
        }
        else {
            // When this item is in the map, we update this item in the order list
            // make this key on the top of the order list queue:
        	node = map.get(key);
            // update the node value:
            node.val = value;
            // remove itself:
            unLinkNode(node);
        }
        // add itself to the header:
        addNodeToFront(node);
        map.put(key, node);
    }

    private void removeTailNode() {
        DoubleLinkedListNode lastNode = tail.prev;
        lastNode.prev.next = tail;
        tail.prev = lastNode.prev;

        resetPrevNext(lastNode);
        return lastNode.key;
    }

    private void unLinkNode(DoubleLinkedListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
        resetPrevNext(node);
    }

    private void addNodeToFront(DoubleLinkedListNode node) {
        // add itself to the header:
        DoubleLinkedListNode prevItem = head.next;
        head.next = node;
        node.prev = head;

        node.next = prevItem;
        prevItem.prev = node;
    }

    private void resetPrevNext(DoubleLinkedListNode node) {
        node.prev = null;
        node.next = null;
    }
}

/**
Leetcode 218: The Skyline problem:
https://leetcode.com/problems/the-skyline-problem/
*/
public class Solution {
    /**
     * Priority Version of solution: Running Time Complexity: O(N^2):
     *      PriorityQueue Remove is O(N), because find(obj) is O(N), remove(obj) is O(logN)
     * TreeMap Version of solution: Running Time Complexity: O(logN)
     *      Treemap remove is O(logN)
     * https://www.youtube.com/watch?v=GSBLe8cKu0s
     *
    */
    class Point {
        int x;
        int y;
        boolean isStart;
        public Point(int x, int y, boolean isStart) {
            this.x = x;
            this.y = y;
            this.isStart = isStart;
        }
    }

    class PointComparator implements Comparator<Point>{
        /**
         * If two points' x are different: we choose the one with smaller x:
         * If two points' x are the same:
         *     1. If they are all starting points:
         *              Ex: (0,3,s) vs (0,1,s), (0,3,s) should be in the front. The higher building should be in the first
         *     2. If they are all ending points:
         *              Ex: (5,2,e) vs (5,4,e), (5,2,e) should be in the front. The lower building should be in the first
         *     3. Else: If one point is starting point and the other point is ending point:
         *              Ex: (7,3,s) vs (7,2,e), (7,3,s) should be in the front
        */
        public int compare(Point p1, Point p2) {
            if (p1.x != p2.x) {
                return Integer.compare(p1.x, p2.x);
            }
            else {
                if (p1.isStart && p2.isStart) {
                    return Integer.compare(p2.y, p1.y);
                }
                else if (!p1.isStart && !p2.isStart) {
                    return Integer.compare(p1.y, p2.y);
                }
                else {
                    return p1.isStart ? -1 : 1;
                }
            }
        }
    }
    // Running Time Complexity: O(N^2)
    /**
      * PriorityQueue offer: log(N), remove: O(N)
      * Another solution by using TreeMap: Because PriorityQueue Remove is O(N), which will lead to overall runtime to be O(N^2)
      * By using TreeMap, remove is O(logN), so overall running time complexity is O(NlogN)
      */
    public List<List<Integer>> getSkyline(int[][] buildings) {
        List<List<Integer>> result = new ArrayList<>();
        if (buildings == null || buildings.length == 0) {
            return result;
        }
        List<Point> points = new ArrayList<>();
        for (int[] building : buildings) {
            int x1 = building[0];
            int x2 = building[1];
            int h = building[2];
            points.add(new Point(x1, h, true));
            points.add(new Point(x2, h, false));
        }
        PointComparator comp = new PointComparator();
        Collections.sort(points, comp);

        // Use priorityQueue:
        // maxHeap:
        Queue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
        // initial max height is 0:
        int prevMaxHeight = 0;
        pq.offer(prevMaxHeight);
        for (Point point : points) {
            // It is the start point, add it to result list and add height to priorityQueue, update the maxHeight:
            if (point.isStart) {
                pq.offer(point.y);
            }
            // If it is the end point, find the value height and remove itself from prorityQueue.
            // If the maxHeight change, add it to the result list.
            // If the maxHeight does not change, it means this point overlapps with other rectangle:
            else {
                pq.remove(point.y);
            }
            int currMaxHeight = pq.peek();
            if (prevMaxHeight != currMaxHeight) {
                result.add(Arrays.asList(point.x, currMaxHeight));
                prevMaxHeight = currMaxHeight;
            }
        }

        return result;
    }

    // Time Complexity: O(NlogN), tree insert: O(logN), tree remove: O(logN)
    public List<List<Integer>> getSkyline(int[][] buildings) {
        List<List<Integer>> result = new ArrayList<>();
        if (buildings == null || buildings.length == 0) {
            return result;
        }
        List<Point> points = new ArrayList<>();
        for (int[] building : buildings) {
            int x1 = building[0];
            int x2 = building[1];
            int y  = building[2];
            points.add(new Point(x1, y, true));
            points.add(new Point(x2, y, false));
        }

        PointComparator comp = new PointComparator();
        Collections.sort(points, comp);

        // key: height, val: frequency
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
        int prevMaxHeight = 0;
        treeMap.put(prevMaxHeight, 1);
        for (Point p : points) {
            if (p.isStart) {
                treeMap.put(p.y, treeMap.getOrDefault(p.y, 0)+1);
            }
            else {
                int freq = treeMap.get(p.y);
                freq--;
                if (freq == 0) {
                    treeMap.remove(p.y);
                }
                else {
                    treeMap.put(p.y, freq);
                }
            }
            int currMaxHeight = treeMap.lastKey();
            if (prevMaxHeight != currMaxHeight) {
                result.add(Arrays.asList(p.x, currMaxHeight));
                prevMaxHeight = currMaxHeight;
            }
        }

        return result;
    }
}

/**
Leetcode 46: Permutation: (unique numbers only)
https://leetcode.com/problems/permutations/

Leetcode 47: Permutation II: (with duplicate numbers)
https://leetcode.com/problems/permutations-ii/
*/
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        List<Integer> list = new ArrayList<>();
        int n = nums.length;
        boolean[] visited = new boolean[n];
        dfs(result, nums, n, list, visited);
        return result;
    }

    private void dfs(List<List<Integer>> result, int[] nums, int n, List<Integer> list, boolean[] visited) {
        if (list.size() == n) {
            result.add(new ArrayList<>(list));
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i] || (i > 0 && nums[i] == nums[i-1] && !visited[i-1])) {
                continue;
            }
            visited[i] = true;
            list.add(nums[i]);
            dfs(result, nums, n, list, visited);
            visited[i] = false;
            list.remove(list.size()-1);
        }
    }

/**
Leetcode 661: Image Smoother:
https://leetcode.com/problems/image-smoother/
*/
    public int[][] imageSmoother(int[][] M) {
        int m = M.length;
        int n = M[0].length;
        int[][] res = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res[i][j] = imageSmoother(M, i, j, m, n);
            }
        }
        return res;
    }

    private int imageSmoother(int[][] M, int i, int j, int m, int n) {
        int sum = 0;
        int cnt = 0;
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                int x1 = i + x;
                int y1 = j + y;
                if (isValid(x1, y1, m, n)) {
                    cnt++;
                    sum += M[x1][y1];
                }
            }
        }
        return sum / cnt;
    }

    private boolean isValid(int i, int j, int m, int n) {
        return (i >= 0 && i < m && j >= 0 && j < n);
    }

/**
Leetcode 5: Longest Palidrome substring:
https://leetcode.com/problems/longest-palindromic-substring
*/
     public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int start = -1, end = -1;
        int n = s.length();
        int maxLen = -1;
        // odd length palindrome:
        for (int mid = 0; mid < n; mid++) {
            for (int i = 1; mid - i >= 0 && mid + i < n; i++) {
                if (s.charAt(mid - i) != s.charAt(mid + i)) {
                    break;
                }
                int len = 2 * i + 1;
                if (len > maxLen) {
                    maxLen = len;
                    start = mid - i;
                    end = mid + i;
                }
            }
        }
        // even length palidrome:
        for (int mid = 0; mid < n-1; mid++) {
            for (int i = 1; mid - i + 1 >= 0 && mid + i < n; i++) {
                if (s.charAt(mid-i+1) != s.charAt(mid+i)) {
                    break;
                }
                int len = 2 * i;
                if (len > maxLen) {
                    maxLen = len;
                    start = mid-i+1;
                    end = mid+i;
                }
            }
        }
        if (start == -1) {
            return s.substring(0,1);
        }
        else {
            return s.substring(start, end+1);
        }
    }

/**
Draw a circle:
https://www.tutorialspoint.com/computer_graphics/circle_generation_algorithm.htm
https://www.cs.uic.edu/~jbell/CourseNotes/ComputerGraphics/Circles.html
http://groups.csail.mit.edu/graphics/classes/6.837/F98/Lecture6/circle.html

Given a function dot that draws x,y on a 2d grid, how would you draw a circle?

*/
    // center: (x, y), radius: r
    // bruth force:
    public void drawCircle(int x, int y, int r) {
        // x*x + y*y = r*r
        for (int i = x-r; i <= x+r; i++) {
            for (int j = y-r; j <= y+r; j++) {
                if (i*i + j*j == r*r) {
                    setPixel(i, j);
                }
            }
        }
    }

    // point (x,y) on the circle, radius: r, center is (0,0)
    public void drawCircle(int x, int y, int r) {
        for (int i = -r; i <= r; i++) {
            for (int j = -r; j <= r; j++) {
                if (i*i + j*j == r*r) {
                    setPixel(i, j);
                }
            }
        }
    }

    // 8分法: divide the circle into 8 parts:
    public void drawCircle(int x, int y, int r) {
        int x1 = 1/2 * r*r;
        x1 = Math.sqrt(x1);
        while (x1 * x1 <= r*r) {
            for (int y1 = 0; y1*y1 <= 1/2 * r*r; y1++) {
                if (x1 * x1 + y1*y1 == r*r) {
                    setPixel(x1,y1);
                    setPixel(-x1,y1);
                    setPixel(x1,-y1);
                    setPixel(-x1,-y1);
                    setPixel(y1,x1);
                    setPixel(-y1,x1);
                    setPixel(y1,-x1);
                    setPixel(-y1,-x1);
                }
            }
            x1++;
        }
    }

/**
Follow up:
1. 如何高效地计算像素点的位置？
2. 是否可以只计算上一部分像素的位置，其余像素用极其简单的数学计算得到？(提示：四个象限，1/8)
3. x坐标是否每次都加1？y坐标是否每次都加1？为什么？
4. 当圆上的点的理论值不是整数时候，如何取整，舍去？补足，还是四舍五入？
5. 如何不用开根号sqrt() 函数？
6. 如何不用取绝对值？(提示：理论值是否一定在两个整数之间？)
7. 下次画点的像素点的候选位置有几个？
8. 下次画点的像素点的候选位置是否可以根据上一次的点计算出来？
9. 下次画点的像素点的候选位置可否不用求平方就可以根据上一次的点计算出来？
*/
/**
构造判别式: F(x,y) = x*x + y*y - r*r
F(x,y) > 0, point (x, y) is outside of circle
F(x,y) < 0, point (x, y) is inside of circle

https://blog.csdn.net/MMogega/article/details/53055625
https://www.cs.uic.edu/~jbell/CourseNotes/ComputerGraphics/Circles.html

中心画圆法:
P1: (x+1,y)
P2: (x+1, y-1)
mid point: (x+1, y-0.5)

x1 = x+1, y1 = y-0.5
if F(x1, y1) > 0, meaning mid point is outside of circle, we choose P2 (x+1, y-1) as the point of the circle
if F(x1, y1) < 0, meaning mid point is inside of the circle, we choose P1 (x+1, y) as the point of the circle

d' = (x+1)^2 + (y-1/2)^2 - r^2
if d' < 0, d'' = (x+2)^2 + (y-1/2)^2 - r^2
d'' = d' + 2x + 3

If d' > 0, d'' = (x+2)^2 + (y-3/2)^2 - r^2
d'' = d'+2(x-y) + 5

point(0,r)
d0 = F(1, r-1/2) = 1 + (r-1/2)^2 - r^2 = 5/4 - r
*/

public void drawCircle(int xc, int yc, int r) {
    int x = 0;
    int y = r;
    double d = 1.25 - r;
    // set eight points in the circle
    setEightPixels(x, y);
    while (x < y) {
        if (d < 0) {
            d = d + 2 * x + 3;
        }
        else {
            d = d + 2 * (x-y) + 5;
            y--;
        }
        x++;
        setEightPixels(x, y);
    }
}

/**
Leetcode 075: sort color:
https://leetcode.com/problems/sort-colors/
只需要把红旗全部移到最前，要求用最少的swap。

一个disk有很多chunks，但只有三种类型称为A，B，C。要求把乱序的disk chunks重排为[A A A B B B B C C]，所有A在前面，C在后面，B在中间，只能通过1) read某个index的chunk 2) swap两个chunk的data 这两个操作来实现。follow up是尽量减少2）的次数

要求swap次数最少 最优解是O(N) 时间, O(1) Space
// https://my.oschina.net/Tsybius2014/blog/661514
*/

    // O(N) time, and O(1) Space. one pass.
    public void sortColors(int[] nums) {
        // any idx before i is 0
        int i = 0;
        // any idx after k is 2
        int k = nums.length - 1;
        int j = 0;
        while (j <= k) {
            if (nums[j] == 0) {
                // j idx is ahead of i
                if (i != j) {
                    swap(nums, i, j);
                }
                i++;
                j++;
            }
            else if (nums[j] == 1) {
                j++;
            }
            else if (nums[j] == 2) {
                if (k != j) {
                    swap(nums, k, j);
                }
                k--;
            }
        }
    }

    // O(N) time, and O(1) Space. two pass.
    public void sortColors(int[] nums) {
        // two pass:
        // count num of 0,1,2:
        int[] cntArr = new int[3];
        for (int num : nums) {
            cntArr[num]++;
        }
        int i = 0;
        int j = cntArr[0];
        int k = cntArr[0] + cntArr[1];
        int n = nums.length;
        while (i < cntArr[0] || j < cntArr[0] + cntArr[1] || k < n) {
            while (i < cntArr[0] && nums[i] == 0) {
                i++;
            }
            int num1 = -1;
            if (i < cntArr[0]) {
                num1 = nums[i];
            }

            while (j < cntArr[0] + cntArr[1] && nums[j] == 1) {
                j++;
            }
            int num2 = -1;
            if (j < cntArr[0] + cntArr[1]) {
                num2 = nums[j];
            }

            while (k < n && nums[k] == 2) {
                k++;
            }
            int num3 = -1;
            if (k < n) {
                num3 = nums[k];
            }
            if (num1 != -1 && num2 != -1 && num3 != -1) {
                if ((num1 == 1 && num2 != 1) || (num2 == 0 && num1 != 0)) {
                    swap(nums, i, j);
                }
                else if ((num1 == 2 && num3 != 2) || (num3 == 0 && num1 != 0)) {
                    swap(nums, i, k);
                }
                else if ((num2 == 2 && num3 != 2) || (num3 == 1 && num2 != 1)) {
                    swap(nums, j, k);
                }
            }
            else if (num1 != -1 && num2 != -1) {
                if ((num1 == 1 && num2 != 1) || (num1 != 0 && num2 == 0)) {
                    swap(nums, i, j);
                }
            }
            else if (num1 != -1 && num3 != -1) {
                if ((num1 == 2 && num3 != 2) || (num1 != 0 && num3 == 0)) {
                    swap(nums, i, k);
                }
            }
            else if (num2 != -1 && num3 != -1) {
                if ((num2 == 2 && num3 != 2) || (num2 != 1 && num3 == 1)) {
                    swap(nums, j, k);
                }
            }
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

/**
Buddy System:
A complete binary tree, node value is 0 or 1, the node value is 1 if and only if all children's value is 1

A complete binary tree is a binary tree in every level, except possibly the last, is completely filled, and all nodes are as far left as possible.

Given a complete binary tree with nodes of values of either 1 or 0, the following rules always hold:
    1. a node's value is 1 if and only if all its subtree nodes' values are 1
    2. a leaf node can have either 1 or 0

Implement 2 APIs:
setBit(offset, length), set the bits at range from offset to offset+length-1;
clearBit(offset, length), clear the bits at range from offset to offset+length-1

height表示树的高度；从最后一层开始；

bits[level][index]
*/
// from bottom, the last row:
public void setBits(int[][] bits, int start, int length) {
    if (bits.length == 0 || start < 0) {
        return;
    }
    int level = bits.length-1;
    if (bits[level].length < length) {
        return;
    }

    int l = start;
    int r = start + length - 1;
    while (level >= 0) {
        // set bits:
        for (int i = l; i <= r; i++) {
            bits[level][i] = 1;
        }
        // If start is a right leaf, we need to check if left leaf is also 1
        // If it is not, we increment left
        int ll = isRightLeaf(l) ? l-1 : l+1;
        if (bits[level][ll] == 0) {
            l = l / 2 + 1;
        }
        else {
            l = l / 2;
        }
        // if end is the left leaf, we need to check if the right leaf is also 1
        // If it is, we keep r = r / 2;
        // If it is not, we let r = r / 2 - 1;

        // If end is the right leaf, we need to check if the left leaf is also 1
        // If it is, we keep r = r / 2;
        // If it is not , we let r = r / 2 - 1;
        int rr = isRightLeaf(r) ? r-1 : r+1;
        if (rr < bits[level].length && bits[level][rr] == 0) {
            r = r / 2 - 1;
        }
        else {
            r = r / 2;
        }
        level--;
    }
}

private boolean isRightLeaf(int num) {
    return num % 2 == 1;
}

public void clearBits(int[][] bits, int start, int length) {
    if (bits == null || bits.length == 0) {
        return;
    }
    int level = bits.length-1;
    if (bits[level].length < length) {
        return;
    }
    int l = start;
    int r = start + length - 1;
    while (level >= 0) {
        for (int i = l; i <= r; i++) {
            bits[level][i] = 0;
        }
        l /= 2;
        r /= 2;
        level--;
    }
}

/**
Follow up: 如果我想知道最下层有没有连续的8个为1的bit，我怎么用这个data structure做呢？就是从root开始判断，最顶层如果是1代表了最底层有2^(number of level - 1)个1，如果最顶层是0，再看第一层有几个连续的1能代表最后一层有几个连续的1。大概就是这样，然后让return 最后一层连续n个1的第一个1的index。
*/

public int firstIdxOfOneBitInLastlevel(int[][] bits, int n) {
    if (bits == null || bits.length == 0) {
        return -1;
    }
    int maxLevel = bits.length - 1;
    // count number of 1 of each level:
    // last row: 2 ^ (maxlevel-1)
}

/**
Implement O(1) set: O(1) add, O(1) find, O(1) remove
两个数组实现 O(1) set
*/
public class MySet {

    // idx: the order of add num, val -> num
    private int[] map;
    // idx: num, val: the order of add num
    private int[] arr;
    // current num of items in the set
    private int count;

    public MySet(int N) {
        this.map = new int[N];
        this.arr = new int[N];

        Arrays.fill(map, -1);
        Arrays.fill(arr, -1);
        this.count = 0;
    }

    public boolean add(int num) {
        if (contains(num)) {
            return false;
        }
        arr[num] = count;
        map[count] = num;
        count++;
        return true;
    }

    public int remove(int num) {
        if (!contains(num)) {
            return -1;
        }
        int idx = arr[num];
        int oldNum = map[idx];

        int lastNum = map[count-1];
        if (count > 1) {
            map[idx] = lastNum;
            arr[lastNum] = idx;
        }
        else {
            map[idx] = -1;
            arr[lastNum] = -1;
        }
        return oldNum;
    }

    public boolean contains(int num) {
        int idx = arr[num];
        if (idx != -1 && idx < count && map[idx] == num) {
            return true;
        }
        return false;
    }
}

// O(1) Map:
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Add O(1), deletion: O(1), lookup: O(1), Iterate O(Num of elements)
public class MyMap {

    private Map<Integer, Item> map;
    private List<Integer> keys;

    public MyMap() {
        this.map = new HashMap<>();
        this.keys = new ArrayList<>();
    }

    public void add(int key, int value) {
        if (!map.containsKey(key)) {
            Item item = new Item(keys.size(), value);
            keys.add(key);
            map.put(key, item);
        }
        else {
            Item item = map.get(key);
            item.value = value;
            map.put(key, item);
        }
    }

    public Integer delete(int key) {
        if (!map.containsKey(key)) {
            return null;
        }
        Item item = map.remove(key);
        int lastKey = keys.get(keys.size()-1);
        Item lastItem = map.get(lastKey);
        keys.set(item.idx, lastKey);
        lastItem.idx = item.idx;

        keys.remove(keys.size()-1);
        return item.value;
    }

    public Integer get(int key) {
        if (!map.containsKey(key)) {
            return null;
        }
        return map.get(key).value;
    }

    class Item {
        // the idx of the keys ArrayList
        int idx;
        int value;
        public Item(int idx, int value) {
            this.idx = idx;
            this.value = value;
        }
    }
}


/**
Get One Id:
You are given a get_ids function which takes in a size argument and returns an array corresponding to size argument of unique IDs each time. This function takes one second to run (doesn't matter the size).

You want to write a get_id function that returns a singular id. This function is called at 1000 times per second. Want to write it such that there is no spike in run time.

主要就是用一个“批发商”producer线程调用get_ids()批量获得id并且缓存起来，然后其他consumer线程从这个批发商这里一个batch一个batch的拿

最后需要多线程


给出了two buffer和ring buffer两种解法
*/
// Single thread:
public class IDEvent {
    private Queue<Integer> queue;
    private int size;

    public IDEvent(int size) {
        this.queue = new LinkedList<>();
        this.size = size;
    }

    public int get_id() {
        if (queue.isEmpty()) {
            int[] ids = get_ids(size);
            for (int id : ids) {
                newQueue.offer(id);
            }
        }
        return queue.poll();
    }
}

// Multithread version:
// producer consumer pattern:
// create a blocking queue:
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * You are given a get_ids function which takes in a size argument and returns an array
 * corresponding to size argument of unique IDs each time.
 * This function takes one second to run (doesn't matter the size).
 *
 * You want to write a get_id function that returns a singular id.
 * This function is called at 1000 times per second.
 * Want to write it such that there is no spike in run time.
 * */
public class IDEventWithConcurrency {

    private Queue<Integer> queue;
    private final int capacity;
    // fairness = true
    /** Lock held by take, poll, etc */
    private final ReentrantLock takeLock = new ReentrantLock(true);

    /** Wait queue for waiting takes */
    private final Condition notEmpty = takeLock.newCondition();

    /** Lock held by put, offer, etc */
    private final ReentrantLock putLock = new ReentrantLock(true);

    /** Wait queue for waiting puts */
    private final Condition notFull = putLock.newCondition();

    public IDEventWithConcurrency(int capacity) {
        this.queue = new LinkedList<>();
        this.capacity = capacity;
    }

    public void producer() throws InterruptedException {
        final ReentrantLock putLock = this.putLock;
        putLock.lock();
        try {
            while (queue.size() == capacity) {
                notFull.await();
            }
            int size = capacity - queue.size();
            int[] ids = getIds(size);
            for (int num : ids) {
                queue.offer(num);
            }
            notEmpty.signal();
        }
        finally {
            putLock.unlock();
        }
    }

    public int getId() throws InterruptedException  {
        final int num;
        final ReentrantLock takeLock = this.takeLock;
        takeLock.lock();
        try {
            while (queue.isEmpty()) {
                notEmpty.await();
            }
            num = queue.poll();
            notFull.signal();
        }
        finally {
            takeLock.unlock();
        }
        return num;
    }

    private int[] getIds(int size) {
        int[] nums = new int[size];
        for (int i = 0; i < size; i++) {
            nums[i] = 1;
        }
        return nums;
    }
}

/**
https://www.youtube.com/watch?v=UOr9kMCCa5g
Producer consumer pattern
two options:
    1. Using locks and conditions
    2. Wait-notify
*/
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MyBlockingQueue {
    private Queue<Integer> queue;
    private final int max;
    // fairness = true
    private final ReentrantLock lock = new ReentrantLock(true);
    private final Condition notEmpty = lock.newCondition();
    private final Condition notFull = lock.newCondition();

    public MyBlockingQueue(int size) {
        this.queue = new LinkedList<>();
        this.max = size;
    }
    // producer threads
    public void put(int num) throws InterruptedException {
        lock.lock();
        try {
            // recheck the queue size when thread wakes up
            while (queue.size() == max) {
                notFull.await();
            }
            queue.offer(num);
            notEmpty.signalAll();
        }
        finally {
            lock.unlock();
        }
    }

    // consumer threads
    public int take() throws InterruptedException {
        lock.lock();
        try {
            // recheck the queue size when thread wakes up
            while (queue.isEmpty()) {
                // If the condition is not satisfied,
                // the thread will release the lock and switch to wait state.
                notEmpty.await();
            }
            int num = queue.poll();
            notFull.signalAll();
            return num;
        }
        finally {
            lock.unlock();
        }
    }
}

// Option 2: use wait and notify:
import java.util.LinkedList;
import java.util.Queue;

public class MyBlockingQueue2 {
    private Queue<Integer> queue;
    private final int max;
    private Object notEmpty = new Object();
    private Object notFull = new Object();

    public MyBlockingQueue2(int size) {
        this.queue = new LinkedList<>();
        this.max = size;
    }

    public synchronized void put(int num) throws InterruptedException {
        while (queue.size() == max) {
            notFull.wait();
        }
        queue.offer(num);
        notEmpty.notifyAll();
    }

    public synchronized int take() throws InterruptedException {
        while (queue.isEmpty()) {
            notEmpty.wait();
        }
        int num = queue.poll();
        notFull.notifyAll();
        return num;
    }
}

/**
Reverse a file with limit memory
*/

/**
Leetcode 1206: Skip List:
https://leetcode.com/problems/design-skiplist/

Reference:
https://www.cnblogs.com/binyue/p/4545555.html
https://blog.csdn.net/qq575787460/article/details/16371287
http://www.mathcs.emory.edu/~cheung/Courses/323/Syllabus/Map/skip-list-impl.html
https://www.sanfoundry.com/java-program-implement-skip-list/
http://m.2cto.com/kf/201702/599228.html
\https://link.1point3acres.com/?url=https%3A%2F%2Fleetcode.com%2Fproblems%2Fdesign-skiplist%2Fdiscuss%2F400028%2FC%252B%252B-SkipList.-2-pointer-for-each-node.-64ms.
https://zhuanlan.zhihu.com/p/33674267
*/

/**
Min Stack:
*/
class MinStack {
    private Stack<Integer> stack;
    private int minVal;

    /** initialize your data structure here. */
    public MinStack() {
        this.stack = new Stack<>();
        this.minVal = Integer.MAX_VALUE;
    }

    public void push(int x) {
        if (x <= minVal) {
            stack.push(minVal);
            minVal = x;
        }
        stack.push(x);
    }

    public void pop() {
        int val = stack.pop();
        if (val == minVal) {
            minVal = stack.pop();
        }
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return minVal;
    }
}

class Worker {
    public Worker(func work) {

    }

    // asynchronous,
    // this method will run as non-blocking method
    public void startWork(func callBack) {

    }
}

class MultiWorker {
    private Queue<Worker> workers;
    public MultiWorker() {
        this.worker = new LinkedList<>();
    }

    public void addWork(func work) {
        this.workers.add(new Worker(work));
    }

    public void startWork(func callBack) {
        // TODO: let all workers to start working.
        // Call the callBack method onces all workers have completed their work.
        // This function should return immediately without waiting all workers to complete their work.
    }
}