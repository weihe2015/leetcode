// https://www.1point3acres.com/bbs/interview/data-science-451380.html

/**
Q1:
A million numbers, an array & doubly linked list:

10 ns to process a single item: How long to find a item:

1. Array: Binary Search: log N, N = 10^6, log (10^6) = 20
   20 * 10ns = 200 ns

2. Double linked list: n/2 = 5 * 10^5 * 10ns = 5 * 10^6 ns

Q2: Compare Two Words:
    return True if the first word comes first alphabetically, False otherwise
*/
function compare(word1, word2):
    // this should return true if value 1 is alphabetically before value 2, false otherwise
    letter_index = 0
    while letter_index < word1.length()
        letter_index += 1
        letter1 = word1.get_character_at(letter_index)
        letter2 = word2.get_character_at(letter_index)
        if alphabet.indexOf(letter1) > alphabet.indexOf(letter2)
            return True
        else if alphabet.indexOf(letter2) > alphabet.indexOf(letter1)
            return False
    // if they're are the smae word, we'll exit the loop and it doesn't matter what we return
    return True

// 1): what will be output if you use this compare function to sort the list["fff","hhh"]? At the end of sorting, compare will return True when comparing the first and second element of the list.
// (A) ["fff", "hhh"]
// (B) ["hhh", "fff"]
// (C) ["hhh", "hhh"]
// (D) ["fff", "fff"]
// (E) Program will crash, throw and error or exhibit some other undefined behavior

// Ans: (B)

// 2) what will be output if you use this compare function to sort the list["abcabc","bbbaaa"]? At the end of sorting, compare will return True when comparing the first and second element of the list.

// (A) ["bbbaaa", "abcabc"]
// (B) ["abcabc", "bbbaaa"]
// (C) ["bbbaaa", "bbbaaa"]
// (D) ["abcabc", "abcabc"]
// (E) Program will crash, throw and error or exhibit some other undefined behavior

// Ans: (B)

// 3) what will be output if you use this compare function to sort the list["fgh","fghi"]? At the end of sorting, compare will return True when comparing the first and second element of the list.

// (A) ["fgh", "fghi"]
// (B) ["fghi", "fgh"]
// (C) ["fghi", "fghi"]
// (D) ["fgh", "fgh"]
// (E) Program will crash, throw and error or exhibit some other undefined behavior

// Ans: (E)

/*
Q7:
    Score system:
*/
public int score(int num) {
    int res = 0;
    String s = String.valueOf(num);
    int score1 = rule1(s);
    int score2 = rule2(s);
    int score3 = rule3(s);
    int score4 = rule4(num);
    int score5 = rule5(s);
    res = score1 + score2 + score3 + score4 + score5;
    return res;
}

private int rule1(String s) {
    int res = 0;
    for (char c : s.toCharArray()) {
        if (c == '7') {
            res += 5;
        }
    }
    return res;
}

private int rule2(String s) {
    int res = 0;
    char prevChar = 'a';
    for (char c : s.toCharArray()) {
        if (c == '2' && prevChar == '2') {
            res += 6;
        }
        prevChar = c;
    }
    return res;
}

private int rule3(String s) {
    int res = 0;
    int contArrLen = 1;
    char prevNumChar = 'a';
    for (char c : s.toCharArray()) {
        if (c == prevNumChar + 1) {
            contArrLen++;
        }
        else {
            res += Math.pow(contArrLen, 2);
            contArrLen = 1;
        }
    }
    return res;
}

private int rule4(int num) {
    if (num % 3 == 0) {
        return 4;
    }
    return 0;
}

private int rule5(String s) {
    int res = 0;
    for (char c : s.toCharArray()) {
        int num = Integer.parseInt(c);
        if (num % 2 == 0) {
            res += 3;
        }
    }
    return res;
}
/**
Q8: Lock Use Analyzer:

*/
static private final String ACQUIRE = "ACQUIRE";
static private final String RELEASE = "RELEASE";

public static int checkLockSequence(String[] sequences) {
    if (sequences == null || sequences.length == 0) {
        return 0;
    }
    int res = 0;
    Stack<Integer> stack = new Stack<>();
    Set<Integer> set = new HashSet<>();
    for (int i = 0, n = sequences.length; i < n; i++) {
        String sequence = sequences[i];
        String[] infos = sequence.split(" ");
        String action = infos[0];
        int num = Integer.parseInt(infos[1]);
        switch (action) {
            case ACQUIRE: {
                if (set.contains(num)) {
                    return i+1;
                }
                set.add(num);
                stack.push(num);
                break;
            }
            case RELEASE: {
                if (!stack.isEmpty() && stack.peek() == num) {
                    stack.pop();
                    set.remove(num);
                }
                else {
                    return i+1;
                }
                break;
            }
            default:
                break;
        }
    }
    return stack.isEmpty() ? 0 : sequences.length + 1;
}

/**
Alice Builds a Cache:
1. Type A, 10GB, read an object from memory takes 2ms
2. Type B, 1TB, read an object from memory takes 500ms
2048 objects, same size, all second type of memory storage.
1 TB = 1024 GB
2048 objects = 1024 GB. 1 object = 500MB.

主要考察cache hit rate概念。cache（faster storage）里只够放20个object，hit rate 20/2048。计算10次总时间时就是 10 * 20/2048 * cache latency + 10* （1-20/2048）* storage latency。第二空，naive strategy时50%workload seen in past 30sec是迷惑信息，由于cache里数据不更新，hit rate和第一空时没区别。所以第二空 = 第一空/10。best case是100%cache hit， worse case是0%cache hit。

2ms * 10 = 20 ms

10 GB =

1) cache: type A: put 20 objects. so hit rate is 20/2048
10 times cache = 10 * (20/2048 * 2ms (cache latency) + (1-20/2048) * 500 ms(storage latency)) = 4951.367ms

2) 20/2048 * 2ms + (1-20/2048) * 500ms = 495.137 ms

3) 100% hit rate: 2ms

4) 0% hit rate: (500+2)/2 = 251ms


*/
// Fix Bugs:
public int countPairs(int[] arr, int len, int diff) {
    int i = 0, j = 1, pairs = 0;
    while (i < len) {
        for (j = i+1; j < len; j++) {
            if (arr[j] == arr[i] + diff) {
                pairs++;
            }
        }
        i++;
    }
    return pairs;
}

// Remove all N integers
static LinkedListNode removeAll(int n, LinkedListNode head) {
    if (head == null) {
        return null;
    }

    // remove element in the header:
    while (head != null && head.val == n) {
        head = head.next;
    }

    LinkedListNode curr = head;
    while (curr != null && curr.next != null) {
        if (curr.next.val == val) {
            curr.next = curr.next.next;
        }
        else {
            curr = curr.next;
        }
    }
    return head;
}

/**
C# code:
Question: which best describes the growth of Foo.f(x) as a funciton of x?
1. Logarithmic
2. Linear
3. Quadratic
4. Cubic
5. Exponential

Growth of Foo.f(x): Exponential ，此函数类似于斐波那契数列求解
** Exponential
*/
Class Foo {
    static int f(int x) {
        if (x < 1) {
            return 1;
        }
        else {
            return f(x-1) + g(x);
        }
    }

    static int g(int x) {
        if (x < 2) {
            return 1;
        }
        else {
            return f(x-1) + g(x/2);
        }
    }
}
/**
A black-and-white computer graphics display is divided up into an array of pixel

Each of the pixels can take on one of eight gray levels ranging from 0(white) to 7(black). In order to prevent sharp discontinuties of shade, the software system that causes pictures to be displayed enforced the rule that the gray levels of two adjacent pixels cannot differ by more than two.

How many of the 64 possible assignments of gray levels to two adjacent pixels satisfy this rule?

4. gray pixel ： 34
    表格对应如下：
    pixel1   pixel2
    0     --> 0,1,2
    1     --> 0,1,2,3
    2     --> 0,1,2,3,4
    ....
    7     --> 5,6,7
    所以一共有：3x2 + 4x2+5x4 = 34种
*/

/**
Which of the following decimal numbers has an exact representations in binary notations?
(A) 0.1  (B) 0.2  (C) 0.3  (D) 0.4  (E) 0.5

Ans: (E) 0.5.   0.5 = 1/2 = 2^-1 The representation in binary notation of 0.5 is 0.1
*/

/**
7. worst scenario search a value from 1 to 1000:  10

Bob writes down a number between 1 and 1,000. Mary must identify that
number by asking ”yes/no” questions to Bob. Mary knows that Bob always
tells the truth. If Mary uses an optimal algorithm, then she will determine the
answer at the end of exactly how many questions in the worst case?

(A) 1,000  (B) 999 (C) 500  (D) 32  (E) 10

Ans: (E): Mary can divide the interval (number) of possible answers by 2 with each question, Then Mary will have to do at most
log2(1000) = 10 questions (10 in the worst case).
*/

/**
Consider a single linked list of the form. Where F is a pointer to the first element in the list and L is a pointer to the last element. The time of which of the following operations depends on the length of the list?

(A): delete the last element of the list
(B): delete the first element of the list
(C): Add an element after the last element of the list
(D): Add an elemnt before the first element of the list
(E): Interchange the first wo elements of the list;

Ans: (A) Delete the last element in the list
8. Delete the last element in the list。 其他操作都是O（1）
*/

/**
For the program fragment above involving integers p, q, and n, which of the following is a loop invariant; i.e., true at the beginning of each execution of the loop and at the completion of the loop?
(A): p = k+1; (B) p = (k+1)^2; (C) p=(k+1)2^k; (D) p = 2^k, (E) p = 2^(k+1)

p = 2^k, 跟踪几次p，k值即可推导出关系.
Ans: (D) p = 2^k
*/
p := 1; k := 0;
while k < n do
begin
    p := 2 * p;
    k := k + 1
end;


/**
BNF: A particular BNF definition for a "word" is given by the following rules:

<word> ::= <letter> | <letter><pairlet> | <letter><pairdig>
<pairlet> ::= <letter><letter> | <pairlet><letter><letter>
<pairdig> ::= <digit><digit> | <pairdig><digit><digit>
<letter> ::= a|b|c|...|y|z
<digit> ::= 0|1|2|...|9

xx (A) word:
(B) words: <letter><pairlet> -> <letter><pairlet><letter><letter> -> <letter><letter><letter><letter><letter> -> words
(C) C22  <letter><pairdig> -> <letter><<digit><digit> -> C22
xx (D) 42
Ans: BC

5. BNF :
words: 对应的是word->letter,pairlet->letter,pairlet,letter,letter->letter,letter,letter,letter,letter,->"words"
c22: 对应的是word->letter,pairdig->letter,digit,digit->"c22"
*/

/**
If the variables below are properly initialized, and if i remains within array bounds, then the code below implements the stack operations Push and Pop. Note that the stack is held in array S[1..N] indexed by the variable i.

Push: begin S[i] := x; i := i+1 end
Pop: begin i := i-1; x := S[i] end
Which of the following statements correctly initializes i for this implementation of a stack?

(A): i := N
(B): i := N-1
(C): i := 1
(D): i := 0
(E): i := N/2
Ans: i := 1

6. Implementation of stack
I:= 1
*/


/*
3. Concurrency task:
a==0->b==1
b==0->a==1
a==1 ->b==1 不选，因为这个顺序时不满足：
  y=1
  b=x
  x=1
  a=y, 此时b=0



binary search case: search 1
sequence: 500,250,125,63,32,16,8,4,2,1, 10次

10. base 2: 0.5, 其他数m,不存在n, 使得m x 2^n 是个整数
11. 递归求解： 8

*/

/*


Ex:1
Q1: Concurrent Container
Q2: Efficient Memory Layout
Q3: Cat Pictures Backup
Q4: DFS in Binary Tree
Q5: Lock Use Analyzer
Q6: Count Palindromes
Q7: Breaking Binary Search
*/