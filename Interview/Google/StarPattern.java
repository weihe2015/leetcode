/**
input: 2
output:
 *
***
 *

input 3:
output:
  *
 ***
*****
 ***
  *

*/
public class Solution {
    static private final String SPACE = " ";
    static private final String STAR = "*";

    public String starPattern(int n) {
        List<String> list = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        // j: total number of characters in a line
        int j = n;
        // k: num of star of each line:
        int k = 1;
        // i: index of line:
        int total = n + n - 1;
        for (int i = 1; i <= total; i++) {
            for (int l = 0; l < j; l++) {
                if (l < j-k) {
                    sb.append(SPACE);
                }
                else {
                    sb.append(STAR);
                }
            }
            if (i < n) {
                k += 2;
                j++;
            }
            else {
                k -= 2;
                j--;
            }
            list.add(sb.toString());
            sb.setLength(0);
        }

        for (String str : list) {
            sb.append(str).append("\n");
        }
        // remove last newline
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    @Test
    public void test1() {
        int n = 2;
        String res = getStarPattern(n);
        System.out.println(res);
    }

    @Test
    public void test2() {
        int n = 3;
        String res = getStarPattern(n);
        System.out.println(res);
    }

    @Test
    public void test3() {
        int n = 4;
        String res = getStarPattern(n);
        System.out.println(res);
    }
}