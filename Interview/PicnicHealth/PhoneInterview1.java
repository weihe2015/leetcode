import java.io.*;
import java.util.*;

/*
We are building a word processor and we would like to implement a "reflow" functionality that also applies full justification to the text.

Given an array containing lines of text and a new maximum width, re-flow the text to fit the new width. Each line should have the exact specified width. If any line is too short, insert '-' (as stand-ins for spaces) between words as equally as possible until it fits.
Note: we are using '-' instead of spaces between words to make testing and visual verification of the results easier.


lines = [ "The day began as still as the",
          "night abruptly lighted with",
          "brilliant flame" ]

reflowAndJustify(lines, 24) ... "reflow lines and justify to length 24" =>

        [ "The--day--began-as-still",
          "as--the--night--abruptly",
          "lighted--with--brilliant",
          "flame" ] // <--- a single word on a line is not padded with spaces

reflowAndJustify(lines, 25) ... "reflow lines and justify to length 25" =>

        [ "The-day-began-as-still-as"
          "the-----night----abruptly"
          "lighted---with--brilliant"
          "flame" ]

reflowAndJustify(lines, 26) ... "reflow lines and justify to length 26" =>

        [ "The--day-began-as-still-as",
          "the-night-abruptly-lighted",
          "with----brilliant----flame" ]

reflowAndJustify(lines, 40) ... "reflow lines and justify to length 40" =>

        [ "The--day--began--as--still--as-the-night",
          "abruptly--lighted--with--brilliant-flame" ]

n = number of words OR total characters


*/
public class Solution {
  public static void main(String[] argv) {
    String[] lines = {"The day began as still as the","night abruptly lighted with","brilliant flame"};
    int testReflowWidth1 = 24;
    System.out.println("Test1 with maxLen " + testReflowWidth1);
    List<String> res = reflowAndJustify(lines, testReflowWidth1);
    for (String text : res) {
      System.out.println(text);
    }

    int testReflowWidth2 = 25;

    System.out.println("Test2 with maxLen " + testReflowWidth1);
    res = reflowAndJustify(lines, testReflowWidth2);
    for (String text : res) {
      System.out.println(text);
    }
    int testReflowWidth3 = 26;

    System.out.println("Test3 with maxLen " + testReflowWidth1);
    res = reflowAndJustify(lines, testReflowWidth3);
    for (String text : res) {
      System.out.println(text);
    }


    int testReflowWidth4 = 40;

    System.out.println("Test4 with maxLen " + testReflowWidth1);
    res = reflowAndJustify(lines, testReflowWidth4);
    for (String text : res) {
      System.out.println(text);
    }


  }

  static private final String DASH = "-";
  static private final String SPACE = " ";

  public static List<String> reflowAndJustify(String[] lines, int maxLen) {
    List<String> wordList = new ArrayList<>();
    for (String line : lines) {
      String[] words = line.split(SPACE);
      for (String word : words) {
        wordList.add(word);
      }
    }
    int i = 0;
    int n = wordList.size();
    StringBuffer sb = new StringBuffer();
    List<String> res = new ArrayList<>();
    while (i < n) {
      String word = wordList.get(i);
      if (word.length() < maxLen) {
        sb.append(word);
        i++;
        if (i < n) {
          word = wordList.get(i);
        }
      }
      int dashCnt = 0;
      while (i < n && sb.length() + word.length() + 1 <= maxLen) {
        sb.append(DASH).append(word);
        dashCnt++;
        i++;
        if (i < n) {
          word = wordList.get(i);
        }
      }
      String s = sb.toString();
      if (dashCnt > 0 && s.length() < maxLen) {
        int remain = maxLen - s.length();
        s = addDash(s, remain, dashCnt);
      }
      res.add(s);
      sb.setLength(0);
    }
    return res;
  }

  private static String addDash(String s, int remain, int dashCnt) {
    StringBuffer sb = new StringBuffer();
    int i = 0;
    int n = s.length();

    while (i < n && remain > 0) {
      char c = s.charAt(i);
      if (c != '-') {
        sb.append(c);
      }
      else {
        remain--;
        sb.append(DASH);
        sb.append(DASH);
      }
      i++;
    }
    while (i < n) {
      char c = s.charAt(i);
      sb.append(c);
      i++;
    }

    s = sb.toString();
    sb.setLength(0);
    i = 0;
    n = s.length();
    while (i < n && remain > 0) {
      char c = s.charAt(i);
      if (c == '-') {
        for (int j = 0; j < remain; j++) {
          sb.append(DASH);
        }
        remain = 0;
        break;
      }
      else {
        sb.append(c);
      }
      i++;
    }
    while (i < n) {
      char c = s.charAt(i);
      sb.append(c);
      i++;
    }
    return sb.toString();
  }


  // N -> words:
  public static List<String> wordWrap(String[] words, int maxLen) {
    List<String> res = new ArrayList<>();
    int i = 0;
    int n = words.length;
    StringBuffer sb = new StringBuffer();
    while (i < n) {
      String word = words[i];
      if (word.length() <= maxLen) {
        sb.append(word);
        i++;
        if (i < n) {
          word = words[i];
        }
      }
      while (i < n && sb.length() + word.length() + 1 <= maxLen) {
        sb.append("-").append(word);
        i++;
        if (i < n) {
          word = words[i];
        }
      }
      res.add(sb.toString());
      sb.setLength(0);
    }
    return res;
  }
}
