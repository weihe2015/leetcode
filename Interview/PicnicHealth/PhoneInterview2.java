/*

Imagine we have an image. We'll represent this image as a simple 2D array where every pixel is a 1 or a 0.

There are N shapes made up of 0s in the image. They are not necessarily rectangles -- they are odd shapes ("islands"). Find them.

image1 = [
  [1, 0, 1, 1, 1, 1, 1],
  [1, 0, 0, 1, 0, 1, 1],
  [1, 1, 1, 0, 0, 0, 1],
  [1, 0, 1, 1, 0, 1, 1],
  [1, 0, 1, 0, 1, 1, 1],
  [1, 0, 0, 0, 0, 1, 1],
  [1, 1, 1, 0, 0, 1, 1],
  [1, 1, 1, 1, 1, 1, 0],
]

The function should return every single pixel in each shape. For reference, these are (in [row,column] format):

findShapes(image1) =>
  [
    [[0,1],[1,1],[1,2]],
    [[1,4],[2,3],[2,4],[2,5],[3,4]],
    [[3,1],[4,1],[4,3],[5,1],[5,2],[5,3],[5,4],[6,3],[6,4]],
    [[7,6]],
  ]


Other test cases:

image2 = [
  [0],
]

findShapes(image2) =>
  [
    [[0,0]],
  ]

image3 = [
  [1],
]

findShapes(image3) => []

n: number of rows in the input image
m: number of columns in the input image


*/

import java.io.*;
import java.util.*;

public class Solution {

  static class Point {
    int x;
    int y;
    public Point(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }


  static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
  public static List<List<Point>> findShapes(int[][] image) {
    int m = image.length;
    int n = image[0].length;
    // copy image to new grid;
    int[][] grid = new int[m][n];
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        grid[i][j] = image[i][j];
      }
    }
    List<List<Point>> res = new ArrayList<>();
    // begin to explore:
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (grid[i][j] == 0) {
          List<Point> list = new ArrayList<>();
          // perform dfs:
          dfs(grid, i, j, m, n, res, list);
          res.add(list);
        }
      }
    }
    return res;
  }

  private static void dfs(int[][] grid, int i, int j, int m, int n, List<List<Point>> res, List<Point> list) {
    if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 1) {
      return;
    }
    list.add(new Point(i, j));
    grid[i][j] = 1;
    for (int[] dir : dirs) {
      int x = i + dir[0];
      int y = j + dir[1];
      dfs(grid, x, y, m, n, res, list);
    }
  }

  static class Info {
    int x;
    int y;
    int width;
    int height;
    public Info(int x, int y, int width, int height) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
    }
  }

  public static List<Info> findRectangles(int[][] image) {
    // copy image into a new 2d array:
    int m = image.length;
    int n = image[0].length;
    int[][] grid = new int[m][n];
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        grid[i][j] = image[i][j];
      }
    }
    List<Info> infos = new ArrayList<>();
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (grid[i][j] != 0) {
          continue;
        }
        // search right
        int width = 0;
        int k = j;
        while (k < n && image[i][k] == 0) {
          width++;
          k++;
        }

        // search down
        int height = 0;
        k = i;
        while (k < m && image[k][j] == 0) {
          height++;
          k++;
        }

        Info info = new Info(j, i, width, height);
        infos.add(info);

        // mark this rectangle to 1 to avoid being search again.
        for (int ii = i; ii < i + height; ii++) {
          for (int jj = j; jj < j + width; jj++) {
            grid[ii][jj] = 1;
          }
        }
      }
    }
    return infos;
  }

  public static Info findRectangle(int[][] image) {
    int m = image.length;
    int n = image[0].length;
    Info info = null;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (image[i][j] != 0) {
          continue;
        }
        // search right
        int width = 0;
        int k = j;
        while (k < n && image[i][k] == 0) {
          width++;
          k++;
        }

        // search down
        int height = 0;
        k = i;
        while (k < m && image[k][j] == 0) {
          height++;
          k++;
        }
        info = new Info(j, i, width, height);
        break;
      }
      if (info != null) {
        break;
      }
    }
    return info;
  }

  public static void main(String[] argv) {
    int[][] image1 = {
  {1, 0, 1, 1, 1, 1, 1},
  {1, 0, 0, 1, 0, 1, 1},
  {1, 1, 1, 0, 0, 0, 1},
  {1, 0, 1, 1, 0, 1, 1},
  {1, 0, 1, 0, 1, 1, 1},
  {1, 0, 0, 0, 0, 1, 1},
  {1, 1, 1, 0, 0, 1, 1},
  {1, 1, 1, 1, 1, 1, 0},
};

int[][] image2 = {
  {0},
};

int[][] image3 = {
  {1},
};
    /**
      [
    [[0,1],[1,1],[1,2]],
    [[1,4],[2,3],[2,4],[2,5],[3,4]],
    [[3,1],[4,1],[4,3],[5,1],[5,2],[5,3],[5,4],[6,3],[6,4]],
    [[7,6]],
  ]
    */
    System.out.println("Image 1");
    List<List<Point>> res = findShapes(image1);
    for (List<Point> list : res) {
      StringBuffer sb = new StringBuffer();
      sb.append("[");
      for (Point p : list) {
        sb.append("[" + p.x + "," + p.y + "]");
      }
      sb.append("]\n");
      System.out.println(sb.toString() + "\n");
    }

    System.out.println("Image 2");
    res = findShapes(image2);
    for (List<Point> list : res) {
      StringBuffer sb = new StringBuffer();
      sb.append("[");
      for (Point p : list) {
        sb.append("[" + p.x + "," + p.y + "]");
      }
      sb.append("]\n");
      System.out.println(sb.toString() + "\n");
    }

    System.out.println("Image 3");
    res = findShapes(image3);
    for (List<Point> list : res) {
      StringBuffer sb = new StringBuffer();
      sb.append("[");
      for (Point p : list) {
        sb.append("[" + p.x + "," + p.y + "]");
      }
      sb.append("]\n");
      System.out.println(sb.toString() + "\n");
    }

  /*
    [
      [[0,0],[1,1]],
      [[0,2],[1,1]],
      [[3,2],[3,2]],
      [[1,3],[1,3]],
      [[3,5],[2,2]],
      [[6,7],[1,1]],
    ]
  **/

//     List<Info> infos = findRectangles(image1);
//     StringBuffer sb = new StringBuffer();
//     System.out.println("\n Image 1: ");
//     for (Info info : infos) {
//       sb.append("[[" + info.x + "," + info.y + "],[" + info.width + "," + info.height + "]],\n");
//     }
//     System.out.println(sb.toString());

//     System.out.println("\n Image 2: ");
//     sb.setLength(0);
//     infos = findRectangles(image2);
//     for (Info info : infos) {
//       sb.append("[[" + info.x + "," + info.y + "],[" + info.width + "," + info.height + "]],\n");
//     }
//     System.out.println(sb.toString());

//     System.out.println("\n Image 3: ");
//     sb.setLength(0);
//     infos = findRectangles(image3);
//     for (Info info : infos) {
//       sb.append("[[" + info.x + "," + info.y + "],[" + info.width + "," + info.height + "]],\n");
//     }
//     System.out.println(sb.toString());

//     System.out.println("\n Image 4: ");
//     sb.setLength(0);
//     infos = findRectangles(image4);
//     for (Info info : infos) {
//       sb.append("[[" + info.x + "," + info.y + "],[" + info.width + "," + info.height + "]],\n");
//     }
//     System.out.println(sb.toString());

//     Info info = findRectangle(image1);
//     // x: 6, y: 4, width: 1, height: 1
//     System.out.println("x: " + info.x + ", y: " + info.y + ", width: " + info.width + ", height: " + info.height);

//     info = findRectangle(image2);
//     System.out.println("x: " + info.x + ", y: " + info.y + ", width: " + info.width + ", height: " + info.height);

//     info = findRectangle(image3);
//     System.out.println("x: " + info.x + ", y: " + info.y + ", width: " + info.width + ", height: " + info.height);

//     info = findRectangle(image4);
//     System.out.println("x: " + info.x + ", y: " + info.y + ", width: " + info.width + ", height: " + info.height);

//     info = findRectangle(image5);
//     System.out.println("x: " + info.x + ", y: " + info.y + ", width: " + info.width + ", height: " + info.height);

  }
}
