/**
就一道题，给一个log file, <userId, timestamp, operation>，operation有两种， open或者close。 
可以理解成一个用户session时间就是open和close之间的时间差。
1）假设input都valid, 计算所有用户的平均session 长度
2）某一个时间段平均session长度
3）某一个用户的平均session长度
4）single Server放不下log的时候， multiple Server如何处理
5）stream如何处理
6）如果有invalid input如何处理

https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=600106&ctid=230377
给定a list of events [userId, timestamp, action] 其中 action 可能是login 或者logoff。 events是有序的 就是对于同一个user logoff肯定出现在login之后
1: 求所有session的average time
2: 求每个user的average session time

Followup：数据量特别大怎么办  

map reduce 根据user id send到不同的reducer

1348
稍微有点变形 比原题还简单一点只用考虑分钟
自己定义输入输出
followup是如果有很多人发推怎么scale（变成更大的时间单位然后求每分钟平均）
如果只记录一段时间的log呢 （随便用个数据结构丢弃之前的log...）

https://www.1point3acres.com/bbs/thread-517376-1-1.html
设计一个class 有两个function record_event(event, time) 和 get_event_count(event, startTime, endTime) 其实就是record event记录一下这个event在这个时间点触发了 get_event_count就是给定这个event 找到在start和end之间触发的次数很快做完以后 对方开始follow up 如果这个event很多 那肯定不能存在memory里面 怎么办 如何support twitter size的user 怎么保证 record_event不被block 如何确保没有data loss, database挂了怎么办 bla bla.....


https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=507226&ctid=229037
1. 设计一个数据结构 class，有两个function 

1. record (int timestamp, String event)
2. getCount(int startTime, int endTime, String event, String granularity)
就是给两个时间点timestamp，求之间一共有多找个该类型的event发生（tweet, retweet之类的）granularity是按照日期，小时，分钟划分的。
返回List<Integer>

2.4sum 变型。

https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=689456&ctid=230377
写一个音乐播放列表，需要包含其他的播放列表，并且播放完一个再播下一个。
实际上就是一个简化版的 Iterator 的题，注意 hasNext() 的时候用 while 检测并且最后也返回检测结果就好了。

https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=692352&ctid=230377
1. Design a tweet service, return the latest 1000 tweets from all the people that I'm following
2. First Non-repeating char in a string, followup, in a stream. 2nd, the shortest path between two nodes on graph.
3. Reverse words in place. Inorder & preorder array to reconstruct the tree.
4. LRU cache

给一个string "aaaaaabbbbcccc", 给出一个string每一个character相邻的都不能是重复的，比如"abacabacabc"这样。
我一脸懵逼，先说用hashmap吧，count个数，再一个一个往里面塞，但是这样明显有bug，就是数量多的容易后来堆积。再三哥的提示下，想到用heap，每次拿最头部的top()节点，用完后还得把它拿出来，因为已经用过了，排下一个，然后再放回去，接着一起排。

https://www.1point3acres.com/bbs/interview/twitter-software-engineer-489856.html
一道是Binary Tree，说如果你站在树根右侧往上看，求看到的Node之和，理解下来就是求右侧子树的Node之和
另一道是给一个int array，shift left k element

https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=501852&ctid=229037
(1) 设计一个跟踪用户看广告，点击广告，以及下载应用的系统。
(2）蠡口  幺叁玖：分词之壹

https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=664714&ctid=230377
b组最开始的hm聊天就有半个小时的技术题，不用写，只用口述思路，题目是给两个general tree，不是binary tree，找他们间最大的相似树。相似树的定义是，结构相同，但是每个node存的val可以不一样。没见过这题，说了几个思路，反正属于讨论性质的，最后的答案不是他心里要的那个，但他觉得想法还挺新颖的，就给过了。

LC 380 的延伸，就是除了原题的要求，还要求getLast也是O(1)。所以要用map+doubly linked list来找getlast, map+list来找getrandom。写完了还要写unit tests，refactor code，还问有更多时间还可以做什么，比如加更多的api啊，注释啊，之类的

*/