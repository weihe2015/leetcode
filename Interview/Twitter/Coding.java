/**
题目是，每行规定150个字，我们输入一条很长的string，怎么切。
每行最后要加页数，例如：XXX（1/3）
我说这个页数很tricky，美国小哥说假设我们只有单数页码，先把单数页的代码写了出来。自己跑test case，发现bug 改掉。
然后follow up，没写代码但激情讨论怎么处理多页的情况。给了好多hint
先是假设不会有超级长的文章，就是那个页数沾满了一行中太多地方连一个单词都放不下。

然后是问，怎么猜页数，我就说，除个150吧，小哥说其实可以除144（这个是单数页的limit，上限，可以降loop的次数）
然后做法就是再loop几遍来finalize每行到底多少个字母。
最后问了一下复杂度，我说这个到底要loop几遍好像不能定哦，因为每次有可能把一个单词分到下一页什么然后又得改什么的。
小哥说其实2到3遍即可，第一遍如果猜中了magnitude的话，第二遍只用调数字，如果第一遍没猜中，才会run 3次。

Ex2:
Input: 
 String s = "this is a very long string and you need to break it into smaller pieces."
 maxLen = 30

output:

this is a very long (1/4)
string and you need to (2/4)
break it into smaller (3/4)
pieces. (4/4)
*/
import java.util.ArrayList;
import java.util.List;

public class WordWrapper {

    static private final String SPACE = " ";
    public List<String> wordWrap(String s, int maxLen) {
        maxLen = maxLen - 6;
        List<String> res = new ArrayList<>();
        String[] words = s.split(SPACE);
        StringBuffer sb = new StringBuffer();
        int i = 0;
        int n = words.length;
        while (i < n) {
            String word = words[i];
            if (word.length() <= maxLen) {
                sb.append(word);
                i++;
                if (i < n) {
                    word = words[i];
                }
            }
            else {
                break;
            }
            while (i < n && sb.length() + word.length() + 1 <= maxLen) {
                sb.append(SPACE).append(word);
                i++;
                if (i < n) {
                    word = words[i];
                }
            }
            res.add(sb.toString());
            sb.setLength(0);
        }
        appendPageNumber(res);
        return res;
    }

    private void appendPageNumber(List<String> res) {
        int i = 1;
        int n = res.size();
        for (String str : res) {
            str = str + " (" + i + "/" + n + ")";
            res.set(i-1, str);
            i++;
        }
    }

    @Test
    public void test1() {
        String s = "this is a very long string and you need to break it into smaller pieces.";
        List<String> list = wordWrap(s, 30);
        /**
        'this is a very long (1/4)' 25
        'string and you need to (2/4)' 28
        'break it into smaller (3/4)' 27
        'pieces. (4/4)' 13
        */
        System.out.println(list);
    }

    @Test
    public void test2() {
        String s = "Write a function that split long SMS string"
                + " into smaller pieces. Each piece should be less than or "
                + "equal to 160 characters and contains indices at the end."
                + " Function should nnot break words into pieces. If word does"
                + " not fit -- it should go to the next SMS.";
        List<String> lines = wordWrap(s, 60);
        /**
        'Write a function that split long SMS string into (1/5)' 54
        'smaller pieces. Each piece should be less than or (2/5)' 55
        'equal to 160 characters and contains indices at the (3/5)' 57
        'end. Function should nnot break words into pieces. If (4/5)' 59
        'word does not fit -- it should go to the next SMS. (5/5)' 56
        */
        for (String line : lines) {
            System.out.println("'" + line + "' " + line.length());
        }
    }
}

/**
有数字1， 2， 3， 40，3， 2 。。。。 一个一个的进来
设计一个class能够实现下面的function
1） add(0)
2） getMean（）
3） getMedian（）

Solution 1: use quick select: quickly finds the k-th smallest element of an unsorted array of n elements.
Quick select time complexity: O(N), with worst case of O(N^2), increasing order of element.
*/
    private double getMedium() {
        int size = nums.length;
        // use quick select to find the kth smallest item in the arraylist
        if (size % 2 == 1) {
            int k = size / 2;
            return kthSmallest(0, size-1, k);
        }
        else {
            int num1 = kthSmallest(0, size-1, size/2);
            int num2 = kthSmallest(0, size-1, size/2-1);
            return (num1 + num2) / 2.0;
        }
    }

    private int findKthSmallest(int[] nums, int k) {
        int n = nums.length;
        return kthSmallest(nums, 0, n-1, k);
    }
    
    private int kthSmallest(int[] nums, int low, int high, int k) {
        int idx = partition(nums, low, high);
        if (idx == k) {
            return nums[idx];
        }
        else if (idx < k) {
            return kthSmallest(nums, idx+1, high, k);
        }
        else {
            return kthSmallest(nums, low, idx-1, k);
        }
    }
    
    private int partition(int[] nums, int low, int high) {
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (nums[j] <= nums[pivot]) {
                swap(nums, i, j);
                i++;
            }
        }
        swap(nums, i, pivot);
        return i;
    }
    
    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    private Queue<Integer> minQueue;
    private Queue<Integer> maxQueue;

    /** initialize your data structure here. */
    public MedianFinder() {
        this.minQueue = new PriorityQueue<>();
        this.maxQueue = new PriorityQueue<>(Collections.reverseOrder());
    }

    public void addNum(int num) {
        if (minQueue.isEmpty()) {
            minQueue.offer(num);
            return;
        }
        if (minQueue.peek() <= num) {
            minQueue.offer(num);
        }
        else {
            maxQueue.offer(num);
        }
        rebalance();
    }

    private void rebalance() {
        if (minQueue.size() - maxQueue.size() > 1) {
            maxQueue.offer(minQueue.poll());
        }
        else if (maxQueue.size() > minQueue.size()) {
            minQueue.offer(maxQueue.poll());
        }
    }

    public double findMedian() {
        if (minQueue.size() == maxQueue.size()) {
            return (minQueue.peek() + maxQueue.peek()) / 2.0;
        }
        else {
            return minQueue.peek();
        }
    }

/**
给一个string "aaaaaabbbbcccc", 给出一个string每一个character相邻的都不能是重复的，比如"abacabacabc"这样。
我一脸懵逼，先说用hashmap吧，count个数，再一个一个往里面塞，但是这样明显有bug，就是数量多的容易后来堆积。再三哥的提示下，想到用heap，每次拿最头部的top()节点，用完后还得把它拿出来，因为已经用过了，排下一个，然后再放回去，接着一起排。
*/
    class Item {
        char c;
        int freq;
        public Item(char c, int freq) {
            this.c = c;
            this.freq = freq;
        }
    }

    public List<Character> taskRearrange(String tasks) {
        List<Character> res = new ArrayList<>();
        int[] charMap = new int[26];
        for (char c : tasks.toCharArray()) {
            charMap[c-'a']++;
        }
        // maxHeap
        Queue<Item> pq = new PriorityQueue<>((i1, i2) -> {
            if (i1.freq != i2.freq) {
                return Integer.compare(i2.freq, i1.freq);
            }
            else {
                return Character.compare(i1.c, i2.c);
            }
        });
        for (int i = 0, n = charMap.length; i < n; i++) {
            char c = (char) (i + 'a');
            int freq = charMap[i];
            if (freq > 0) {
                pq.offer(new Item(c, freq));
            }
        }
        int uniqueCount = pq.size();
        List<Item> todoList = new ArrayList<>();
        while (!pq.isEmpty()) {
            int i = 0;

            todoList.clear();
            while (i <= uniqueCount) {
                if (!pq.isEmpty()) {
                    Item item = pq.poll();
                    res.add(item.c);
                    if (item.freq > 1) {
                        item.freq--;
                        todoList.add(item);
                    }
                }
                else if (pq.isEmpty() && todoList.isEmpty()) {
                    break;
                }
                i++;
            }
            for (Item item : todoList) {
                pq.offer(item);
            }
        }
        return res;
    }

/**
Leetcode 001 Two Sum:
*/
    // O(n) time, O(n) space
    public int[] twoSum(int[] nums, int target){
        // key --> nums[i], value -> index
        Map<Integer, Integer> indexMap = new HashMap<>();
        int[] res = new int[2];
        if (nums == null || nums.length <= 1) {
            return res;
        }
        for (int i = 0, n = nums.length; i < n; i++){
            int diff = target - nums[i];
            if (map.containsKey(diff)){
                res[0] = indexMap.get(diff);
                res[1] = i;
                break;
            }
            indexMap.put(nums[i],i);
        }
        return res;
    }

/**
Leetcode 10: Regular Expression Matching

Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.

'.' Matches any single character.
'*' Matches zero or more of the preceding element.
The matching should cover the entire input string (not partial).

Note:

s could be empty and contains only lowercase letters a-z.
p could be empty and contains only lowercase letters a-z, and characters like . or *.
*/
    // Running Time Complexity: O(m*n), Space Complexity: O(m*n)
    public boolean isMatch(String s, String p) {
        int m = s.length();
        int n = p.length();
        boolean[][] dp = new boolean[m+1][n+1];
        // empty regex matches empty string:
        dp[0][0] = true;
        // scan the regex string and initialize the * match with 2 characters before:
        // s = "", p = a*  ba* = b
        // s = "", p = a?  ba? = b
        for (int j = 1; j <= n; j++) {
            // j > 1 in case index out of bound
            if (j > 1 && p.charAt(j-1) == '*') {
                dp[0][j] = dp[0][j-2];
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if two characters from s and p at same index are matching,
                // or current regex character is .
                if (s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) == '.') {
                    dp[i][j] = dp[i-1][j-1];
                }
                // if current regex character is *:
                else if (j > 1 && p.charAt(j-1) == '*') {
                    // assume regex string is always valid, no * at the beginning:
                    // the character before * is a .
                    // or current character from s matches character before *:
                    if (s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.') {
                        // dp[i-1][j]: match previous character with p.substring(0,j) regex
                        // dp[i][j-1]: a* or .* matches empty string
                        dp[i][j] = dp[i-1][j] || dp[i][j-2];
                        //dp[i][j] = dp[i-1][j] || dp[i][j-1] || dp[i][j-2];
                    }
                    // in this case: a* or .* count as empty:
                    else {
                        dp[i][j] = dp[i][j-2];
                    }
                }
                else {
                    dp[i][j] = false;
                }
            }
        }
        return dp[m][n];
    }

    /** 
        Recursive Solution:
        Running Time Complexity: O(m*n)
     */
    public boolean isMatch(String s, String p) {
        int m = s.length();
        int n = p.length();
        return isMatch(s, p, 0, 0, m, n);
    }

    public boolean isMatch(String s, String p, int i, int j, int m, int n) {
        if (i == m && j == n) {
            return true;
        }
        if (j == n) {
            return false;
        }
        if (j < n-1 && p.charAt(j+1) == '*') {
            // first or condition, treat a* as empty match
            // second or condition, treat a* as multiple match. 
            return isMatch(s, p, i, j+2, m, n) || isMatchCharacter(s, p, i, j, m) && isMatch(s, p, i+1, j, m, n);
        }
        else {
            // If current character is match, we will check if next character is match or not.
            return isMatchCharacter(s, p, i, j, m) && isMatch(s, p, i+1, j+1, m, n);
        }
    }

    private boolean isMatchCharacter(String s, String p, int i, int j, int m) {
        return i < m && (s.charAt(i) == p.charAt(j) || p.charAt(j) == '.');
    }

/**
Leetcode 012: Integer to Roman

Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
For example, two is written as II in Roman numeral, just two one's added together. 
Twelve is written as, XII, which is simply X + II. 
The number twenty seven is written as XXVII, which is XX + V + II.
*/
    public String intToRoman(int num) {
        Map<Integer, String> symbolMap = new LinkedHashMap<>();
        symbolMap.put(1000, "M");
        symbolMap.put(900, "CM");
        symbolMap.put(500, "D");
        symbolMap.put(400, "CD");
        symbolMap.put(100, "C");
        symbolMap.put(90, "XC");
        symbolMap.put(50, "L");
        symbolMap.put(40, "XL");
        symbolMap.put(10, "X");
        symbolMap.put(9, "IX");
        symbolMap.put(5, "V");
        symbolMap.put(4, "IV");
        symbolMap.put(1, "I");
        
        StringBuffer sb = new StringBuffer();
        for (int key : symbolMap.keySet()) {
            String letter = symbolMap.get(key);
            while (num >= key) {
                sb.append(letter);
                num -= key;
            }
        }
        return sb.toString();
    }

/**
Leetcode 042: Trapping Rain Water:

Given n non-negative integers representing an elevation map where the width of each bar is 1, 
compute how much water it is able to trap after raining.
*/

    // Native Solution: Computer the volumn by column
    // Running Time complexity: O(N^2), Space Complexity: O(1)
    /**
     * Basic Idea:
     *  1. For each bar, search left and right side to find the max heigh bar on both left and right side
     *  2. Look at each single column, the water trap area is = Math.min(maxLeft, maxRight) - currHeight;
     *  3. Sum all water trap area of each bar.
     */
    public int trap(int[] height) {
        int totalArea = 0;
        for (int i = 0, n = height.length; i < n; i++) {
            int currHeight = height[i];
            int maxLeft = currHeight;
            int maxRight = currHeight;
            // search the max high bar on the left hand side:
            for (int j = i - 1; j >= 0; j--) {
                maxLeft = Math.max(maxLeft, height[j]);
            }
            
            // search the max high bar on the right hand side:
            for (int j = i + 1; j < n; j++) {
                maxRight = Math.max(maxRight, height[j]);
            }
            totalArea += Math.min(maxLeft, maxRight) - currHeight;
        }
        return totalArea;
    }

    // Running Time Complexity: O(n), Space Complexity: O(1)
    /**
     * Basic Idea:
     *  1. Two pointer, left = 0, right = height.length;
     *      maxLeft = height[left], maxRight = height[right];
     *  2. Because the water trapped depends on the min(maxLeft, maxRight), 
     *     so when right bar is larger, we move the left bar, otherwise, we move the right bar.
     *  3. For each iteration, compare maxLeft/maxRight with next bar, and update the maxLeft and maxRight bar with next bar
     *       currArea of each column area: maxLeft - height[l] / maxRight - height[r]
    */
    public int trap(int[] height) {
        int totalArea = 0;
        if (height == null || height.length == 0) {
            return totalArea;
        }
        int l = 0;
        int r = height.length - 1;
        int maxLeft = height[l];
        int maxRight = height[r];
        
        while (l < r) {
            if (maxLeft < maxRight) {
                l++;
                maxLeft = Math.max(maxLeft, height[l]);
                totalArea += maxLeft - height[l];
            }    
            else {
                r--;
                maxRight = Math.max(maxRight, height[r]);
                totalArea += maxRight - height[r];
            }
        }
        return totalArea;
    }

/**
Leetcode 043: Multiply Strings 

Given two non-negative integers num1 and num2 represented as strings, 
return the product of num1 and num2, also represented as a string.

Example 1:

Input: num1 = "2", num2 = "3"
Output: "6"
Example 2:

Input: num1 = "123", num2 = "456"
Output: "56088"
*/

    public String multiply(String num1, String num2) {
        if (num1 == null || num2 == null) {
            return "";
        }
        int m = num1.length();
        int n = num2.length();
        int[] res = new int[m+n];
        for (int i = m-1; i >= 0; i--) {
            int n1 = num1.charAt(i) - '0';
            for (int j = n-1; j >= 0; j--) {
                int n2 = num2.charAt(j) - '0';
                int sum = n1 * n2 + res[i+j+1];
                res[i+j+1] = sum % 10;
                res[i+j] += sum / 10;
            }
        }
        // skip leading 0:
        int i = 0;
        while (i < m+n && res[i] == 0) {
            i++;
        }
        if (i == m+n) {
            return "0";
        }
        // append int array into string:
        StringBuffer sb = new StringBuffer();
        while (i < m+n) {
            sb.append(res[i]);
            i++;
        }
        return sb.toString();
    }

/**
Leetcode 056: Merge Interval
Given a collection of intervals, merge all overlapping intervals.

Example 1:

Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considerred overlapping.
*/
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> res = new ArrayList<>();
        if (intervals == null || intervals.isEmpty()) {
            return res;
        }
        // sort by starting time.
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        Interval prevInterval = null;
        for (Interval interval : intervals) {
            // add non-overlap interval:
            if (prevInterval == null || prevInterval.end < interval.start) {
                res.add(interval);
                prevInterval = interval;
            }
            else if (prevInterval.end <= interval.end) {
                prevInterval.end = interval.end;
            }
        }
        return res;
    }

    public int[][] merge(int[][] intervals) {
        // sort by starting time.
        Arrays.sort(intervals, Comparator.comparingInt(l -> l[0]));
        List<int[]> intervalList = new ArrayList<>();
        int[] prev = new int[]{-1, -1};
        for (int[] curr : intervals) {
            // add non-overlap interval:
            if (prev[0] == -1 || prev[1] < curr[0]) {
                intervalList.add(curr);
                prev = curr;
            }
            else if (prev[1] <= curr[1]) {
                prev[1] = curr[1];
                int n = intervalList.size();
                intervalList.get(n-1)[1] = curr[1];
            }
        }
        int n = intervalList.size();
        // System.out.println(Arrays.deepToString(res));
        return intervalList.toArray(new int[n][2]);
    }

/**
Leetcode 057: Insert Interval
Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

You may assume that the intervals were initially sorted according to their start times.

Example 1:

Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
Output: [[1,5],[6,9]]
Example 2:

Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
Output: [[1,2],[3,10],[12,16]]
Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].
*/

    /**
    Running Time Complexity: O(N), Space Complexity: O(N);
    */ 
    public int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> res = new ArrayList<>();
        boolean insert = false;
        
        for (int[] interval : intervals) {
            if (insert) {
                res.add(interval);
            }
            else {
                // currInterval end times is far less than newInterval
                // currInterval ... newInterval,
                if (interval[1] < newInterval[0]) {
                    res.add(interval);
                }
                // newInterval is before the current interval:
                else if (newInterval[1] < interval[0]) {
                    res.add(newInterval);
                    res.add(interval);
                    insert = true;
                }
                // overlapped:
                else {
                    newInterval[0] = Math.min(newInterval[0], interval[0]);
                    newInterval[1] = Math.max(newInterval[1], interval[1]);
                }
            }
        }
        if (!insert) {
            res.add(newInterval);
        }
        int n = res.size();
        return res.toArray(new int[n][2]);
    }

/**
Leetcode 118 Pascal's Triangle:

For example, given numRows = 5,
Return

[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
*/
    // Running Time Complexity: O(N^2), Space Complexity: O(1)
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        if (numRows <= 0) {
            return result;
        }
        // let i and j starts from 1 will be easier.
        for (int i = 1; i <= numRows; i++) {
            List<Integer> row = new ArrayList<>();
            for (int j = 1; j <= i; j++) {
                if (j == 1 || j == i) {
                    row.add(1);
                }
                else {
                    int sum = result.get(i-2).get(j-2) + result.get(i-2).get(j-1);
                    row.add(sum);
                }
            }
            result.add(row);
        }
        return result;
    }

/**
Leetcode 101 Symmetric Tree:
Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
*/

    // recursive
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.left, root.right);
    }

    private boolean isSymmetric(TreeNode leftNode, TreeNode rightNode) {
        if (leftNode == null && rightNode == null) {
            return true;
        }
        else if (leftNode == null || rightNode == null) {
            return false;
        }
        else if (leftNode.val != rightNode.val) {
            return false;
        }
        return isSymmetric(leftNode.left, rightNode.right) && isSymmetric(leftNode.right, rightNode.left);
    }

    // Iterative
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        else if (root.left == null && root.right == null) {
            return true;
        }
        else if (root.left == null || root.right == null) {
            return false;
        }
        Stack<TreeNode> leftStack = new Stack<>();
        Stack<TreeNode> rightStack = new Stack<>();
        leftStack.push(root.left);
        rightStack.push(root.right);

        while (!leftStack.isEmpty() && !rightStack.isEmpty()) {
            TreeNode leftNode = leftStack.pop();
            TreeNode rightNode = rightStack.pop();
            if (leftNode.val != rightNode.val) {
                return false;
            }
            if (leftNode.left != null && rightNode.right != null) {
                leftStack.push(leftNode.left);
                rightStack.push(rightNode.right);
            }
            else if (leftNode.left != null || rightNode.right != null) {
                return false;
            }

            if (leftNode.right != null && rightNode.left != null) {
                leftStack.push(leftNode.right);
                rightStack.push(rightNode.left);
            }
            else if (leftNode.right != null || rightNode.left != null) {
                return false;
            }
        }
        return true;
    }

/**
Leetcode 105: Construct Binary Tree from Preorder and Inorder Traversal

*/
    /*
             root <--left--> <--right-->
    preorder: 50, 30, 10, 40, 70, 60, 90
            <---left-->  root <--right-->
    inorder: 10, 30, 40, 50, 60, 70, 90
    */
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return buildTree(preorder, 0, preorder.length-1, inorder, 0, inorder.length-1);
    }

    private TreeNode buildTree(int[] preorder, int preStart, int preEnd, int[] inorder, int inStart, int inEnd) {
        // firstly check if there is no element or only one element
        // then immediately return
        if (preStart > preEnd || inStart > inEnd) {
            return null;
        }
        int rootVal = preorder[preStart];
        TreeNode root = new TreeNode(rootVal);

        // Otherwise, we use our defined strategy, as first element in
        // preorder is the root and we find the value in in-order array
        // to determine how many 'left' elemnt in there and construct the left sub tree
        // Find the root index in inorder array
        int rootIdx = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }
         // calculate the left subtree size in inorder array:
        int leftTreeSize = rootIdx - inStart;
        root.left = buildTree(preorder, preStart+1, preStart + leftSubTreeSize, inorder, inStart, rootIdx-1);
        root.right = buildTree(preorder, preStart + leftSubTreeSize + 1, preEnd, inorder, rootIdx+1, inEnd);
        return root;
    }

    // Iterative:
    // https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/discuss/34555/The-iterative-solution-is-easier-than-you-think!
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length == 0) {
            return null;
        }
        // build a map of indices of values that the appear in the inorder map:
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, n = inorder.length; i < n; i++) {
            map.put(inorder[i], i);
        }
        
        Stack<TreeNode> stack = new Stack<>();
        int rootVal = preorder[0];
        TreeNode root = new TreeNode(rootVal);
        stack.push(root);
        
        for (int i = 1, n = preorder.length; i < n; i++) {
            int val = preorder[i];
            TreeNode curr = new TreeNode(val);
            TreeNode prev = stack.peek();
            
            // the new node is on the left of the last node, so it must be its left child (that's the way preorder works)
            if (map.get(val) < map.get(prev.val)) {
                prev.left = curr;
            }
            else {
                TreeNode parNode = null;
                /**
                the new node is on the right of the last node,
                so it must be the right child of either the last node or one of the last node's ancestors.
                pop the stack until we either run out of ancestors
                or the node at the top of the stack is to the right of the new node
                */
                while (!stack.isEmpty() && map.get(val) > map.get(stack.peek().val)) {
                    parNode = stack.pop();
                }
                parNode.right = curr;
            }
            stack.push(curr);
        }
        
        return root;
    }

/**
    Leetcode 106 Construct Binary Tree from Inorder and Postorder Traversal
*/
    // Recursive Solution
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return buildTree(inorder, 0, inorder.length-1, postorder, 0, postorder.length-1);
    }
    
    private TreeNode buildTree(int[] inorder, int inStart, int inEnd, int[] postorder, int postStart, int postEnd) {
        if (postStart > postEnd || inStart > inEnd) {
            return null;
        }
        int rootVal = postorder[postEnd];
        TreeNode root = new TreeNode(rootVal);
        int rootIdx = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }
        int leftSubTreeSize = rootIdx - inStart;
        root.left = buildTree(inorder, inStart, rootIdx-1, postorder, postStart, postStart + leftSubTreeSize - 1);
        root.right = buildTree(inorder, rootIdx + 1, inEnd, postorder, postStart + leftSubTreeSize, postEnd-1);
        return root;
    }

    /** Iterative Solution:
    preorder: node, left, right
    Inorder: left, node, right
    postOrder: left, right, node
    */
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        if (postorder.length == 0 || inorder.length == 0) {
            return null;
        }
        // key -> node's value, value -> inorder index
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, n = inorder.length; i < n; i++) {
            map.put(inorder[i], i);
        }
        
        int m = postorder.length;
        int rootVal = postorder[m-1];
        TreeNode root = new TreeNode(rootVal);
        
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        
        for (int i = m-2; i >= 0; i--) {
            int val = postorder[i];
            TreeNode curr = new TreeNode(val);
            TreeNode prev = stack.peek();
            
            // if new node is on the right of the last node, so it must be its right child.
            if (map.get(val) > map.get(prev.val)) {
                prev.right = curr;
            }
            else {
                /**
                The new node is on the left of the last nnode,
                so it must be the left childd of eith the last node or one of the last node's ancestors.
                pop the stack until we either run out of ancestors or 
                the node at the top of stack is to the right of the new node.
                */
                TreeNode parNode = null;
                while (!stack.isEmpty() && map.get(val) < map.get(stack.peek().val)) {
                    parNode = stack.pop();
                }
                parNode.left = curr;
            }
            stack.push(curr);
        }
        return root;
    }

/**
    Leetcode 124 Binary Tree Maximum Path Sum:
    Given a non-empty binary tree, find the maximum path sum.

    For this problem, a path is defined as any node sequence from some starting node to any node in the tree along the parent-child connections. The path must contain at least one node and does not need to go through the root.

*/
    private int maxVal;
    public int maxPathSum(TreeNode root) {
        this.maxVal = Integer.MIN_VALUE;
        maxSum(root);
        return maxVal;
    }
    
    private int maxSum(TreeNode node) {
        if (node == null) {
            return 0;
        }
         // 只有在最大贡献值大于 0 时，才会选取对应子节点
        int leftMax = Math.max(0, maxSum(node.left));
        int rightMax = Math.max(0, maxSum(node.right));
        
        // 当前最大的贡献值是left + right + 自己
        maxVal = Math.max(maxVal, leftMax + rightMax + node.val);
        // 只能选左边或者右边 + 上自己的node
        return Math.max(leftMax, rightMax) + node.val;
    }

    /**
        If this question changes to be the max Path Sum includes root node:
    */
    public int maxPathSumWithRoot(TreeNode root) {
        return helper(root);
    }

    private int helper(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int leftMax = Math.max(0, helper(node.left));
        int rightMax = Math.max(0, helper(node.right));

        int val = leftMax + rightMax + node.val;
        return val;
    }

/**
 Leetcode 133 Clone Graph:
*/
    // Solution 1: DFS:
    private Map<Integer, Node> map = new HashMap<>();

    public Node cloneGraph(Node node) {
        return cloneNode(node);
    }

    private Node cloneNode(Node node) {
        if (node == null) {
            return null;
        }
        if (map.containsKey(node.val)) {
            return map.get(node.val);
        }

        Node clonedNode = new Node(node.val);
        map.put(node.val, clonedNode);

        for (Node neighbor : node.neighbors) {
            Node clonedNeighbor = cloneNode(neighbor);
            clonedNode.neighbors.add(clonedNeighbor);
        }
        return clonedNode;
    }

    // Solution: BFS:
    public Node cloneGraph(Node node) {
        if (node == null) {
            return node;
        }
        Node clonedNode = new Node(node.val);
        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);

        // key -> node's val, value -> cloned Node
        Map<Integer, Node> map = new HashMap<>();
        map.put(node.val, clonedNode);

        while (!queue.isEmpty()) {
            Node curr = queue.poll();
            // handle the neighbors:
            for (Node neighbor : curr.neighbors) {
                if (!map.containsKey(neighbor.val)) {
                    // clone the neighbor:
                    Node clonedNeighbor = new Node(neighbor.val);
                    map.put(neighbor.val, clonedNeighbor);
                    queue.add(neighbor);
                }
                map.get(curr.val).neighbors.add(map.get(neighbor.va;));
            }
        }
        return clonedNode;
    }

/**
    Leetcode 139 Word Break
*/

    // Running Time Complexity: O(n * w), Space Complexity: O(n)
    public boolean wordBreak(String s, List<String> wordDict) {
        int n = s.length();
        // dp[i]: substring(0, i) can be segmented with words in wordDict:
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for (int i = 0; i < n; i++) {
            // If substring(0...i) cannot be segmented with words in wordDict,
            // there is no need to see if appended word in wordDict can be segmented.
            if (!dp[i]) {
                continue;
            }
            // Try each word and find substring(i, i+word.length()):
            // "applepenapple": word = pen, i = 5, dp[i] = true, substring(i, i+word.length()) = substring(5, 8) = pen:
            // so dp[8] = true;
            for (String word : wordDict) {
                int end = i + word.length();
                // If either appended word to substring(0,i) is longer than s, or we already found dp[end] == true
                if (end > n || dp[end]) {
                    continue;
                }
                // substr like "pen", s="applepenapple", ["apple","pen"]
                String substr = s.substring(i, end);
                if (substr.equals(word)) {
                    dp[end] = true;
                }
            }
        }
        return dp[n];
    }

    // BFS
    public boolean wordBreak(String s, List<String> wordDict) {
        int n = s.length();
        Set<String> wordSet = new HashSet<>(wordDict);
        
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(0);

        boolean[] visited = new boolean[n+1];
        visited[0] = true;
        
        while (!queue.isEmpty()) {
            int idx = queue.poll();
            for (int i = idx+1; i <= n; i++) {
                if (visited[i]) {
                    continue;
                }
                String substr = s.substring(idx, i);
                if (wordSet.contains(substr) && i == n) {
                    return true;
                }
                else if (wordSet.contains(substr)) {
                    queue.offer(i);
                    visited[i] = true;
                }
            }
        }
        return false;
    }

/**
Leetcode 140: Word Break II:
*/
    // Backtracking and memorization:
    // Running Time Complexity: O(N!)
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> result = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return result;
        }
        Set<String> set = new HashSet<>(wordDict);
        Map<String, List<String>> map = new HashMap<>();
        return wordBreak(s, set, map);
    }

    private List<String> wordBreak(String s, Set<String> wordDict, Map<String, List<String>> map) {
        // To reduce the time when s already iterated
        if (map.containsKey(s)) {
            return map.get(s);
        }
        List<String> result = new ArrayList<>();
        // when s is the leading words like cats or cat, it will be in the list:
        // It cannot return because if s = pineapple, and wordDict has pine, apple, it will need to continue to scan:
        if (wordDict.contains(s)) {
            result.add(s);
        }
        int n = s.length();
        for (int i = n-1; i >= 1; i--) {
            String currStr = s.substring(i);
            if (wordDict.contains(currStr)) {
                String prevStr = s.substring(0, i);
                List<String> strs = wordBreak(prevStr, wordDict, map);
                for (String str : strs) {
                    StringBuffer sb = new StringBuffer(str);
                    sb.append(" ");
                    sb.append(currStr);
                    result.add(sb.toString());
                }
            }
        }
        map.put(s, result);
        return result;
    }

/**
    Leetcode 146 LRU Cache:

*/
class LRUCache {
    int capacity;
    Map<Integer, Integer> map;
    // The item in the front of Deque is the most frequently used item.
    // The item at the end of Deque is the least frequently used item.
    Deque<Integer> orderList;
    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.orderList = new LinkedList<>();
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        int val = map.get(key);
        if (orderList.peekFirst() != key) {
            // make this key on the top of the order list queue:
            pushKeyToTopAsFrequentUsed(key);
        }
        return val;
    }

    public void put(int key, int value) {
        if (capacity > 0) {
            // When this item is in the map, we update this item in the order list, but keep the number of capacity the same
            if (map.containsKey(key)) {
                orderList.remove(key);
            }
            // If this item is not in the map, we decrement the number of capacity
            else {
                capacity--;
            }
            orderList.offerFirst(key);
        }
        else {
            // If this item is not in the map, evict the least used item:
            if (!map.containsKey(key)) {
                int prevKey = orderList.pollLast();
                map.remove(prevKey);
                orderList.offerFirst(key);
            }
            // If this item is in the map, we update this item to the top of the deque:
            else {
                // make this key on the top of the order list queue:
                pushKeyToTopAsFrequentUsed(key);
            }
        }
        map.put(key, value);
    }

    // simple version of put:
    public void put(int key, int value) {
        if (!map.containsKey(key)) {
            if (capacity > 0) {
                capacity--;
            }
            else {
                int prevKey = orderList.pollLast();
                map.remove(prevKey);
            }
        }
        map.put(key, value);
        pushKeyToTopAsFrequentUsed(key);
    }

    private void pushKeyToTopAsFrequentUsed(int key) {
        orderList.remove(key);
        orderList.offerFirst(key);
    }
}

/**
   https://www.youtube.com/watch?v=4wVp97-uqr0
    Time Complexity for PUT: O(1), GET: O(1)
    Idea: Use HashMap to store key value, and the value will be the DoubleLinkedListNode
    Use a doubleLinkedList to have head and tail pointer.
    1. PUT:
         If the key is already existed in the map, we get the DoubleLinkedListNode
            , update its value and push it to the header
         If the key is not in the map:
            1.1 If there is still have capacity:
                We create a new DoubleLinkedListNode, decrement the capacity counter and push it to the header
            1.2 If there is no more capacity:
                We evict the tail node, remove the key of this tail node from the map, create a new DoubleLinkedListNode and push it to the header
    2. GET:
         If the map does not contain this key: return -1
         If the map contains this key:
             2.1 Get this DoubleLinkedListNode from HashMap, push it to the header of the list, and return the value
*/
class LRUCache {
    class DoubleLinkedListNode {
        DoubleLinkedListNode prev, next;
        int key;
        int val;
        public DoubleLinkedListNode() {

        }
        public DoubleLinkedListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    private int capacity;
    // key: original key value from put(key, value), value: the DoubleLinkedListNode
    private Map<Integer, DoubleLinkedListNode> map;
    private DoubleLinkedListNode head;
    private DoubleLinkedListNode tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.head = new DoubleLinkedListNode();
        this.tail = new DoubleLinkedListNode();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        DoubleLinkedListNode node = map.get(key);
        // If the first item is not this key: we pop it to the top of the list:
        if (head.next != node) {
            // remove itself:
            unLinkNode(node);
            addNodeToFront(node);
        }
        return node.val;
    }

    public void put(int key, int value) {
        DoubleLinkedListNode node = null;
        if (!map.containsKey(key)) {
            if (capacity > 0) {
                capacity--;
            }
            else {
                // Remove the tail node from the list
                removeLastNode();
            }
            node = new DoubleLinkedListNode(key, value);
        }
        else {
            // When this item is in the map, we update this item in the order list
            // make this key on the top of the order list queue:
            node = map.get(key);
            // update the node value:
            node.val = value;
            // remove itself:
            unLinkNode(node);
        }
        // add itself to the header:
        addNodeToFront(node);
        map.put(key, node);
    }

    /**
        remove key in the LRU cache if exists.return the previous value associated with {@code key}
        If the key does not exist in LRU, return -1 
    */
    public int remove(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        ListNode node = map.get(key);
        unLinkNode(node);
        map.remove(key);
        capacity++;
        return node.value;
    }

    private void removeLastNode() {
        DoubleLinkedListNode lastNode = tail.prev;
        if (lastNode == head) {
            return;
        }

        unLinkNode(lastNode);
        int key = lastNode.key
        map.remove(key);
    }

    private void unLinkNode(DoubleLinkedListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
        resetNode(node);
    }

    private void addNodeToFront(DoubleLinkedListNode node) {
        // add itself to the header:
        DoubleLinkedListNode prevItem = head.next;
        head.next = node;
        node.prev = head;

        node.next = prevItem;
        prevItem.prev = node;
    }

    private void resetNode(DoubleLinkedListNode node) {
        node.prev = null;
        node.next = null;
    }
}

/**
Leetcode 149: Max Points on a Line
/**
Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.

Example 1:

Input: [[1,1],[2,2],[3,3]]
Output: 3
Explanation:
^
|
|        o
|     o
|  o  
+------------->
0  1  2  3  4
Example 2:

Input: [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]]
Output: 4
Explanation:
^
|
|  o
|     o        o
|        o
|  o        o
+------------------->
0  1  2  3  4  5  6
*/

    /**
    经过某个点的直线，哪条直线上的点最多。
    当确定一个点后，平面上的其他点都和这个点可以求出一个斜率，斜率相同的点就意味着在同一条直线上。

    所以我们可以用 HashMap 去计数，斜率作为 key，然后遍历平面上的其他点，相同的 key 意味着在同一条直线上。

    上边的思想解决了「经过某个点的直线，哪条直线上的点最多」的问题。接下来只需要换一个点，然后用同样的方法考虑完所有的点即可。

    当然还有一个问题就是斜率是小数，怎么办。

    之前提到过了，我们用分数去表示，求分子分母的最大公约数，然后约分，最后将 「分子 + "@" + "分母"」作为 key 即可。

    最后还有一个细节就是，当确定某个点的时候，平面内如果有和这个重叠的点，如果按照正常的算法约分的话，会出现除 0 的情况，
    所以我们需要单独用一个变量记录重复点的个数，而重复点一定是过当前点的直线的。

    */
    /**
     * 思路：对于每个点p1，计算他之后的每一个点与p1的斜率dx和dy. 
     * 如果dx = 0 && dy == 0, 那么相同的点++
     * 局部最优解就是 maxPoint = Math.max(maxPoint, map.get(dx).get(dy))
     * 全局最优解就是 max = Math.max(max, maxPoint + samePoint);
     */
    public int maxPoints(Point[] points) {
        if (points == null) {
            return 0;
        }
        int n = points.length;
        if (n <= 2) {
            return n;
        }
        int max = 0;
        // Key -> dx, value -> map of (dy, freq)
        Map<Integer, Map<Integer, Integer>> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            Point p1 = points[i];
            int samePoints = 1;
            int maxPoints = 0;
            // 每次都要重置map是因为对于每一个点来说，之前计算的斜率和结果都不相同，不能使用。
            map.clear();
            for (int j = i+1; j < n; j++) {
                Point p2 = points[j];
                int dx = p1.x - p2.x;
                int dy = p1.y - p2.y;
                if (dx == 0 && dy == 0) {
                    samePoints++;
                }
                else {
                    // 化简斜率:, 用dx和dy的最大公约数来化简
                    // d 不可能等于零，因为p1和p2不相等，dx != 0 || dy != 0
                    int d = gcd(dx, dy);
                    dx /= d;
                    dy /= d;

                    Map<Integer, Integer> subMap = map.getOrDefault(dx, new HashMap<>());
                    int freq = subMap.getOrDefault(dy, 0) + 1;
                    subMap.put(dy, freq);
                    map.put(dx, subMap);

                    maxPoints = Math.max(maxPoints, freq);
                }
            }
            max = Math.max(max, samePoints + maxPoints);
        }
        return max;
    }
    
    private int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }


/**
Leetcode 239: Sliding Window Maximum

Given an array nums, there is a sliding window of size k which is moving from the very left of 
the array to the very right.
You can only see the k numbers in the window.
Each time the sliding window moves right by one position. Return the max sliding window.

Example:

Input: nums = [1,3,-1,-3,5,3,6,7], and k = 3
Output: [3,3,5,5,6,7]
Explanation:

Window position                Max
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7
Note:
You may assume k is always valid, 1 ≤ k ≤ input array's size for non-empty array.
*/
    // Brute Force Solution:
    // Running Time Complexity: O(N * K)
    public int[] maxSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        int[] res = new int[n-k+1];
        for (int i = 0, max = n-k; i <= max; i++) {
            int maxVal = Integer.MIN_VALUE;
            for (int j = i; j < i+k; j++) {
                maxVal = Math.max(maxVal, nums[j]);
            }
            res[i] = maxVal;
        }
        return res;
    }

    class MaxQueue {
        Deque<Integer> queue = new ArrayDeque<>();
        // Decreasing Monotonic queue, the max item always on the leftmost
        // push only takes O(K), where k is the size of the sliding window

        public void push(int num) {
            while (!queue.isEmpty() && queue.peekLast() < num) {
                queue.pollLast();
            }
            queue.offerLast(num);
        }

        public int getFront() {
            return queue.peekFirst();
        }

        public void popMaxIfExists(int num) {
            if (num != queue.peekFirst()) {
                return;
            }
            queue.pollFirst();
        }
    }
    // Use Monotonic Queue: Running Time Complexity: O(n)
    public int[] maxSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        int[] res = new int[n-k+1];
        int idx = 0;
        MaxQueue mq = new MaxQueue();
        for (int i = 0; i < n; i++) {
            // 1 to k-2 items
            int num = nums[i];
            mq.push(num);
            if (i >= k-1) {
                res[idx] = mq.getFront();
                int leftItem = nums[i-k+1];
                // In next iteration, the leftItem will be exclude.
                // If it is the maximum, we need to pop it from the deque.
                mq.popMaxIfExists(leftItem);
                idx++;
            }
        }
        return res;
    }

/**
Leetcode 218 The Skyline Problem:

*/
    /**
    The critical points are on either start of the building or end of the building.
    The height of the building is critical. The algorithm is as following:
    1. We move from left to right encountering the starts and the ends of the building.
    2. Whenever we encounter a start of a building, we push the height into a PriorityQueue.
        2.1 If the max of the priorityQueue changes, it means that this building at this start point must be taller than
        every other building, which is overallapping at that start point, so it needs to be a part of final answer.
    3. Whenever we encounter an end of a building, we need to remove that building from priorityQueue
        If max of the priorityQueue changes, that value needs to be a part of final answer as well.

        Split start point and end point into two different points
        Sort the points by x values.
        
        If two points' x are different: we choose the one with smaller x:
        If two points' x are the same:
           1. If they are all starting points:
                    Ex: (0,3,s) vs (0,1,s), (0,3,s) should be in the front. The higher building should be in the first
           2. If they are all ending points:
                    Ex: (5,2,e) vs (5,4,e), (5,2,e) should be in the front. The lower building should be in the first
           3. Else: If one point is starting point and the other point is ending point:
                    Ex: (7,3,s) vs (7,2,e), (7,3,s) should be in the front
    
        Initial max value is 0.

    3 edge cases:
    (1): [(0,3,s), (0,2,s), (1,2,e), (2,3,e)] starts points of x values are the same, higher y should look at first
    (2): [(3,3,s), (4,2,s), (5,2,e), (5,3,e)] ends points of x values are the same, lower y should look at first
    (3): [(6,2,s), (7,3,s), (7,2,e), (8,3,e)] end point of x overlap with start point of x, start point should look at first
    */
    class Point {
        int x;
        int y;
        boolean isStart;
        public Point(int x, int y, boolean isStart) {
            this.x = x;
            this.y = y;
            this.isStart = isStart;
        }
    }
    /**
     * Another solution by using TreeMap: Because PriorityQueue Remove is O(N), which will lead to overall runtime to be O(N^2)
     * By using TreeMap, remove is O(logN), so overall running time complexity is O(NlogN)
     */
    // key -> maxHeight, value: frequency
    // Time Complexity: O(NlogN), tree insert: O(logN), tree remove: O(logN)
    public List<List<Integer>> getSkyline(int[][] buildings) {
        List<List<Integer>> result = new ArrayList<>();
        if (buildings == null || buildings.length == 0) {
            return result;
        }
        List<Point> points = new ArrayList<>();
        for (int[] building : buildings) {
            int x1 = building[0];
            int x2 = building[1];
            int y  = building[2];
            points.add(new Point(x1, y, true));
            points.add(new Point(x2, y, false));
        }

        Collections.sort(points, (p1, p2) -> {
            if (p1.x != p2.x) {
                return Integer.compare(p1.x, p2.x);
            }
            else {
                if (p1.isStart && p2.isStart) {
                    return Integer.compare(p2.y, p1.y);
                }
                else if (!p1.isStart && !p2.isStart) {
                    return Integer.compare(p1.y, p2.y);
                }
                else {
                    return p1.isStart ? -1 : 1;
                }
            }
        });

        // key: height, val: frequency
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
        int prevMaxHeight = 0;
        treeMap.put(prevMaxHeight, 1);
        for (Point p : points) {
            if (p.isStart) {
                treeMap.put(p.y, treeMap.getOrDefault(p.y, 0)+1);
            }
            else {
                int freq = treeMap.get(p.y)--;
                if (freq == 0) {
                    treeMap.remove(p.y);
                }
                else {
                    treeMap.put(p.y, freq);
                }
            }
            int currMaxHeight = treeMap.lastKey();
            if (prevMaxHeight != currMaxHeight) {
                result.add(Arrays.asList(p.x, currMaxHeight));
                prevMaxHeight = currMaxHeight;
            }
        }

        return result;
    }

/**
Leetcode 295: Find Median From Data Stream.

*/
public class MedianFinder {
    // Similar to solution Leetcode 480: Slidign Window Median:
    // https://leetcode.com/problems/sliding-window-median
    private Queue<Integer> minQueue;
    private Queue<Integer> maxQueue;

    /** initialize your data structure here. */
    public MedianFinder() {
        minQueue = new PriorityQueue<>();
        maxQueue = new PriorityQueue<>(Collections.reverseOrder());
    }

    public void addNum(int num) {
        if (minQueue.isEmpty()) {
            minQueue.add(num);
            return;
        }
        else if (minQueue.peek() <= num) {
            minQueue.offer(num);
        }
        else {
            maxQueue.offer(num);
        }
        rebalance();
    }

    private void rebalance() {
        if (minQueue.size() - maxQueue.size() > 1) {
            maxQueue.offer(minQueue.poll());
        }
        else if (maxQueue.size() > minQueue.size()) {
            minQueue.offer(maxQueue.poll());
        }
    }

    public double findMedian() {
        if (minQueue.size() == maxQueue.size()) {
            return ((double) minQueue.peek() + (double) maxQueue.peek()) / 2.0;
        }
        else {
            return (double) minQueue.peek();
        }
    }
}

/**
Leetcode 307: Range Sum Query: Mutable:

*/
class NumArray {
    private int[] nums;
    private int[] prefixSum;

    public NumArray(int[] nums) {
        this.nums = nums;
        this.prefixSum = new int[nums.length];
        int sum = 0;
        for (int i = 0, n = nums.length; i < n; i++) {
            sum += nums[i];
            prefixSum[i] = sum;
        }
    }
    
    public int sumRange(int i, int j) {
        if (i == 0) {
            return prefixSum[j];
        }
        return prefixSum[j] - prefixSum[i-1];
    }
    
    public void update(int i, int val) {
        int diff = val - nums[i];
        for (int k = i, n = nums.length; k < n; k++) {
            prefixSum[k] += diff;
        }
        nums[i] = val;
    }
}

/**
Leetcode 341 Flatten Nested List Iterator:

*/
public class NestedIterator implements Iterator<Integer> {
    private Stack<NestedInteger> stack;
    public NestedIterator(List<NestedInteger> nestedList) {
        this.stack = new Stack<>();
        for (int n = nestedList.size(), i = n-1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }

    @Override
    public Integer next() {
        return stack.pop().getInteger();
    }

    @Override
    public boolean hasNext() {
        while (!stack.isEmpty()) {
            NestedInteger ni = stack.peek();
            if (ni.isInteger()) {
                return true;
            }
            stack.pop();
            List<NestedInteger> list = ni.getList();
            for (int n = list.size(), i = n-1; i >= 0; i--) {
                stack.push(list.get(i));
            }
        }
        return false;
    }
}

/**
Leetcode 380 Insert Delete GetRandom O(1)

*/

public class RandomizedSet {

    class ListNode {
        ListNode prev;
        ListNode next;
        int key;
        int idx;
        // It can be easily extend to HashMap by adding new field
        // V value
        public ListNode() { }
        public ListNode(int idx, int key) {
            this.idx = idx;
            this.key = key;
        }
    }

    private static final Random rand = new Random();

    // List of values:
    private List<Integer> keyArray;
    // key -> insert val, val -> idx
    private Map<Integer, ListNode> map;
    private ListNode head;
    private ListNode tail;

    /** Initialize your data structure here. */
    public RandomizedSet() {
        this.keyArray = new ArrayList<>();
        this.map = new HashMap<>();
        this.head = new ListNode();
        this.tail = new ListNode();
        head.next = tail;
        tail.prev = head;
    }

    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int key) {
        if (map.containsKey(key)) {
            return false;
        }
        int idx = keyArray.size();
        ListNode node = new ListNode(idx, key);
        addNodeToBack(node);

        map.put(key, node);
        keyArray.add(key);
        return true;
    }
    /**
     * Last key is always at the tail:
     * */
    private void addNodeToBack(ListNode node) {
        ListNode prevItem = tail.prev;
        tail.prev = node;
        node.next = tail;

        node.prev = prevItem;
        prevItem.next = node;
    }

    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int key) {
        if (!map.containsKey(key)) {
            return false;
        }
        // swap this key with the last key in keys List:
        /**
         There are two cases here:
         1. If the item to be removed is the last item, remove it from the list;
         2. If the item to be removed is not the last item, swap it with the last item on the list:
         */
        int lastKeyIdx = keyArray.size()-1;
        ListNode node = map.get(key);
        int oldKeyIdx = node.idx;
        unLinkNode(node);

        if (oldKeyIdx != lastKeyIdx) {
            int lastKey = keyArray.get(lastKeyIdx);
            map.get(lastKey).idx = oldKeyIdx;
            keyArray.set(oldKeyIdx, lastKey);
        }
        map.remove(key);
        keyArray.remove(lastKeyIdx);

        return true;
    }

    private void unLinkNode(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;

        node.prev = null;
        node.next = null;
    }

    /**
     * Get last key inserted into the set:
     * Require runtime: O(1)
     * */
    public int getLast() {
        if (map.isEmpty()) {
            return -1;
        }
        return tail.prev.key;
    }

    /** Get a random element from the set. */
    public int getRandom() {
        int n = keyArray.size();
        int randomIdx = rand.nextInt(n);
        return keyArray.get(randomIdx);
    }
}

/**
Leetcode 387 Find First Unique Character in Stream:
*/

public class FirstUniqueCharInStream {

    class ListNode {
        ListNode prev;
        ListNode next;
        char symbol;

        public ListNode() {}

        public ListNode(char symbol) {
            this.symbol = symbol;
        }
    }

    private Character findFirstUniqueChar(String s) {

        ListNode head = new ListNode();
        ListNode tail = new ListNode();

        head.next = tail;
        tail.prev = head;

        // indicate whether this character is repeated or not
        boolean[] repeatedMap = new boolean[26];
        ListNode[] hashArray = new ListNode[26];

        for (char c : s.toCharArray()) {
            // this character has been repeated
            if (repeatedMap[c-'a']) {
                continue;
            }
            // this character has been appeared previously,
            else if (hashArray[c-'a'] != null) {
                repeatedMap[c-'a'] = true;
                // remove it from head:
                ListNode node = hashArray[c-'a'];
                unLinkNode(node);
            }
            // new unique character has been introduced:
            else {
                ListNode node = new ListNode(c);
                appendNode(node, tail);
                hashArray[c-'a'] = node;
            }
        }
        if (head.next == tail) {
            return null;
        }
        return head.next.symbol;
    }

    private void unLinkNode(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;

        node.prev = null;
        node.next = null;
    }

    private void appendNode(ListNode node, ListNode tail) {
        ListNode prevNode = tail.prev;
        tail.prev = node;
        node.next = tail;

        prevNode.next = node;
        node.prev = prevNode;
    }

    @Test
    public void test1() {
        String s = "abcbbac";
        Character res = findFirstUniqueChar(s);
        assertNull(res);

        s = "loveleetcode";
        res = findFirstUniqueChar(s);
        assertNotNull(res);
    }
}

/**
Leetcode 1348 Tweet Counts Per Frequency:

Implement the class TweetCounts that supports two methods:

1. recordTweet(string tweetName, int time)

Stores the tweetName at the recorded time (in seconds).
2. getTweetCountsPerFrequency(string freq, string tweetName, int startTime, int endTime)

Returns the total number of occurrences for the given tweetName per minute, hour, or day (depending on freq) starting from the startTime (in seconds) and ending at the endTime (in seconds).

freq is always minute, hour or day, representing the time interval to get the total number of occurrences for the given tweetName.
*/
public class TweetCounts {
    // key -> tweetName, value -> TreeMap (key -> time, value -> frequency)
    private Map<String, TreeMap<Integer, Integer>> tweetMap;
    
    public TweetCounts() {
        this.tweetMap = new HashMap<>();
    }
    
    public void recordTweet(String tweetName, int time) {
        // 当前用户推文集合
        TreeMap<Integer, Integer> freqMap = tweetMap.getOrDefault(tweetName, new TreeMap<>());
        // 推文时间记录，比之前次数多1
        int freq = freqMap.getOrDefault(time, 0) + 1;
        freqMap.put(time, freq);
        
        tweetMap.put(tweetName, freqMap);
    }
    
    public List<Integer> getTweetCountsPerFrequency(String freq, String tweetName, int startTime, int endTime) {
        List<Integer> res = new ArrayList<>();
        if (!tweetMap.containsKey(tweetName)) {
            res.add(0);
            return res;
        }
        int freqTime = getTimeByFreqKeyword(freq);
        
        // 用户的全部推文时间集合
        TreeMap<Integer, Integer> freqMap = tweetMap.get(tweetName);
        int start = startTime;
        int end = Math.min(start + freqTime, endTime + 1);
        
        while (start <= endTime) {
            int count = 0;
            // 找到发文时间大于等于start的推文
            Map.Entry<Integer, Integer> entry = freqMap.ceilingEntry(start);
            while (entry != null && entry.getKey() < end) {
                count += entry.getValue();
                // 找比当前大的推文时间
                entry = freqMap.higherEntry(entry.getKey());
            }
            res.add(count);
            // 时间后移
            start = end;
            end = Math.min(end + freqTime, endTime + 1);
        }
        return res;
    }
    
    private int getTimeByFreqKeyword(String freq) {
        switch (freq) {
            case "minute":
                return 60;
            case "hour":
                return 3600;
            case "day":
                return 86400;
            default:
                return 1;
        }
    }
}