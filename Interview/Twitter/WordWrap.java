/**
题目是，每行规定150个字，我们输入一条很长的string，怎么切。
每行最后要加页数，例如：XXX（1/3）
我说这个页数很tricky，美国小哥说假设我们只有单数页码，先把单数页的代码写了出来。自己跑test case，发现bug 改掉。
然后follow up，没写代码但激情讨论怎么处理多页的情况。给了好多hint
先是假设不会有超级长的文章，就是那个页数沾满了一行中太多地方连一个单词都放不下。

然后是问，怎么猜页数，我就说，除个150吧，小哥说其实可以除144（这个是单数页的limit，上限，可以降loop的次数）
然后做法就是再loop几遍来finalize每行到底多少个字母。
最后问了一下复杂度，我说这个到底要loop几遍好像不能定哦，因为每次有可能把一个单词分到下一页什么然后又得改什么的。
小哥说其实2到3遍即可，第一遍如果猜中了magnitude的话，第二遍只用调数字，如果第一遍没猜中，才会run 3次。

Ex2:
Input: 
 String s = "this is a very long string and you need to break it into smaller pieces."
 maxLen = 30

output:

this is a very long (1/4)
string and you need to (2/4)
break it into smaller (3/4)
pieces. (4/4)
*/
import java.util.ArrayList;
import java.util.List;

public class WordWrapper {

    static private final String SPACE = " ";
    public List<String> wordWrap(String s, int maxLen) {
        maxLen = maxLen - 6;
        List<String> res = new ArrayList<>();
        String[] words = s.split(SPACE);
        StringBuffer sb = new StringBuffer();
        int i = 0;
        int n = words.length;
        while (i < n) {
            String word = words[i];
            if (word.length() <= maxLen) {
                sb.append(word);
                i++;
                if (i < n) {
                    word = words[i];
                }
            }
            else {
                break;
            }
            while (i < n && sb.length() + word.length() + 1 <= maxLen) {
                sb.append(SPACE).append(word);
                i++;
                if (i < n) {
                    word = words[i];
                }
            }
            res.add(sb.toString());
            sb.setLength(0);
        }
        appendPageNumber(res);
        return res;
    }

    private void appendPageNumber(List<String> res) {
        int i = 1;
        int n = res.size();
        for (String str : res) {
            str = str + " (" + i + "/" + n + ")";
            res.set(i-1, str);
            i++;
        }
    }

    @Test
    public void test1() {
        String s = "this is a very long string and you need to break it into smaller pieces.";
        List<String> list = wordWrap(s, 30);
        // [this is a very long (1/4), string and you need to (2/4), break it into smaller (3/4), pieces. (4/4)]
        System.out.println(list);
    }
}

