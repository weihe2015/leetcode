/* 
    Account Activity Replay API is a data recovery tool that lets developers retrieve events from as far back as five days

    Increasing the productivity of our engineers.
    Keeping the system easy to maintain

    The real-time system is built on a publish-subscribe architecture

    the volume of events to be stored by the replay system isn't petabyte scale

    spinning Hadoop’s MapReduce jobs is more expensive and slower than consuming data on Kafka,

    using snappy as the compression

    https://blog.twitter.com/engineering/en_us/topics/infrastructure/2020/kafka-as-a-storage-system.html
*/