/**
Thread state:
* New.
* Runnable.
* Blocked.
* Waiting.
* Timed Waiting.
* Terminated.
*/
/**
Implement Lock:

Attempt 1: Test and set:
Hardware executes it atomically
Spin lock
*/
boolean testAndSet(boolean flag) {
    boolean old = flag;
    flag = true;
    return old;
}

class Lock {
    int held = 0;

    public void acquire(Lock lock) {
        while (testAndSet(lock.held));
    }

    public void release(Lock lock) {
        lock.held = 0;
    }
}

/**
Attempt 2: Disable interrupts:
*/
class Lock {
    int held = 0;
    Queue<E> queue;

    public void acquire(Lock lock) {
        // Disable interrupts
        while (lock.held) {
            // put current thread on lock queue
            // block current thread
        }
        lock.held = 1;
        // Enable interrupts
    }

    public void release(Lock lock) {
        // Disable interrupts
        if (!queue.isEmpty()) {
            // remove waiting thread
        }
        // unblock waiting thread
        lock.held = 0;
        // enable interrupts
    }
}

// Two common high-level mechanisms:
// Semaphores: binary (mutex) and counting
// Monitors: metexes and condition variables

/**
Semaphore:
    an abstract data type that provides mutual exclusion to critical sections.
    can be used as atomic counters
    safety property: the semaphore value is always >= 0
    support two operations:
        Wait(): decrement, block until semaphore is open
            Java: semaphore.acquire();
        Signal: increment, allow another thread to enter
            Java: semaphore.release();
    When wait() is called by a thread:
        If semaphore is open, thread continues
        If semaphore is closed, thread blocks on queue

    When signal() opens the semaphore:
        If a thread is waiting on the queue, the thread is unblocked
        If no threads are waiting on the queue, the signal is remembered for the next thread
*/

/**
Monitor:
    A monitor is a programmming language construct that controls access to shared data
    A module that encapsulates:
        Shared data structures
        procedures that operate on the share data structure
        Synchronization between concurrent threads that invoke the procedures
*/

/**
Starvation:
    A situation where a process is prevented from making progress becuase some other process has the resource it requires

Deadlock:
    One process tries to allocate a resource that a second process holds, they can nevet make progress
    Four conditions:
        1. Mutual exclusion
        2. Hold and wait
        3. No preemption: Resources cannot be preempted
        4. Circular wait

Context Switch:
    * 在该线程被 CPU 剥夺时间片后又再次运行恢复上次所保存的信息的过程就称为上下文切换。
    * 解决方案:
        * 采用无锁编程，比如将数据按照 Hash(id) 进行取模分段，每个线程处理各自分段的数据，从而避免使用锁。
        * 采用 CAS(compare and swap) 算法，如 Atomic 包就是采用 CAS 算法(
        * 合理的创建线程，避免创建了一些线程但其中大部分都是处于 waiting 状态，因为每当从 waiting 状态切换到 running 状态都是一次上下文切换。
*/