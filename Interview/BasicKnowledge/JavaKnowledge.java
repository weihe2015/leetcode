/**
Collections:
    * List
        * ArrayList
            * 以数组实现，有自动扩容，当增加数据的时候，如果ArrayList的大小已经不满足需求时，那么就将数组变为原长度的1.5倍，之后的操作就是把老的数组拷到新的数组里面
        * Vector
        * LinkedList
            * 链表，node, prev, next pointer,
    * Queue
        * LinkedList
        * PriorityQueue
            * 堆, heap, 每次删除只能删除头节点，数组第一个节点。
            * 逻辑上是完全二叉树, 物理存储上是数组。
            * 当前节点位置为 i ，父节点位置 i/2，左孩子节点位置2i，右孩子节点2i+1。
            * 默认是min heap, 可以同过自定义Comparator函数来实现max heap
            * Iterator 出来的结果并不是排序好的
    * Set
        * HashSet
            * 储存唯一的值
            * 用hash 算法来计算object hash value, hashMap 是计算key的hash value, hashset 计算value的hash value
            * 默认的构造函数指定的初始化容量是16，负载因子是0.75.。也就是说创建了一个长度为16的数组，默认的负载因子为0.75，当达到容量时，map会自动扩容。
            * add: value 是静态的 Object PRESENT = new Object(); HashSet的实例都共享这个对象
            * remove: 只需要判断map.remove(o)的返回值是不是PRESENT，就可以确定是否成功移除了
            *
        * LinkedHashSet
        * TreeSet
Map:
    * HashMap
        * Capacity就是buckets的数目，Load factor就是buckets填满程度的最大比例。当bucket填充的数目（即hashmap中元素的个数）大于capacity * load factor时就需要调整buckets的数目为当前的2倍。
        * put:
            * 对key的hashCode()做hash，然后再计算index;
            * 如果没碰撞直接放到bucket里；
            * 如果碰撞了，以链表的形式存在buckets后；
            * 如果碰撞导致链表过长(大于等于TREEIFY_THRESHOLD = 8)，就把链表转换成红黑树;
            * 如果节点已经存在就替换old value(保证key的唯一性)
            * 如果bucket满了(超过load factor*current capacity)，就要resize。
        * get:
            * bucket里的第一个节点，直接命中；
            * 如果有冲突，则通过key.equals(k)去查找对应的entry
            * 若为树，则在树中通过key.equals(k)查找，O(logn)；
            * 若为链表，则在链表中通过key.equals(k)查找，O(n)。
        * Java 8:利用红黑树替换链表，这样复杂度就变成了O(1)+O(logn)了
        * Resize:
            * 在resize的过程，简单的说就是把bucket扩充为2倍，之后重新计算index，把节点再放到新的bucket中。
            * 遍历原来table中每个位置的链表，并对每个元素进行重新hash，在新的newTable找到归宿，并插入。
            * 当超过限制的时候会resize，然而又因为我们使用的是2次幂的扩展(指长度扩为原来2倍)，
            * 所以，元素的位置要么是在原位置，要么是在原位置再移动2次幂的位置
            * 不需要重新计算hash，只需要看看原来的hash值新增的那个bit是1还是0就好了，是0的话索引没变，是1的话索引变成“原索引+oldCap”。
        * hash: 是通过hashCode()的高16位异或低16位实现的：(h = k.hashCode()) ^ (h >>> 16)，主要是从速度、功效、质量来考虑的，
                这么做可以在bucket的n比较小的时候，也能保证考虑到高低bit都参与到hash的计算中，同时不会有太大的开销。
        * index = hashcode & (length - 1)
    * LinkedHashMap
        *

Iterator:

Concurrency:
    * HashMap vs HashTable:
        HashMap: 线程不安全, 环状链表, resize 扩容的rehash的时候发生。
        HashTable： 不允许key 和value是null，线程安全
            * get/put 都是synchronized。整个hashtable 一个大锁。
    * concurrentHashMap 分段锁
        1.7:
            * Segment array
            * 一个segment 一个 可重入锁(ReentrantLock), 一个segment 维护着一个hash entry<K,V>
            * Array + linkedList
            * value and linkedlist is volatile。可以保证内存可见性，所以不会读取到过期数据
            * 理论上 ConcurrentHashMap 支持 CurrencyLevel (Segment 数组数量)的线程并发。
            * 每当一个线程占用锁访问一个 Segment 时，不会影响到其他的 Segment。
            * put:
                * segmentMask: 段掩码，假如segments数组长度为16，则段掩码为16-1=15；segments长度为32，段掩码为32-1=31。这样得到的所有bit位都为1，可以更好地保证散列的均匀性
                * 是通过key 定位到Segment, 之后在对应的Segment中进行put的操作
        1.8:
            * 抛弃了原有的 Segment 分段锁，而采用了 CAS (Compare And Swap) + synchronized 来保证并发安全性
            * 根据 key 计算出 hashcode 判断是否需要进行初始化。
            * f 即为当前 key 定位出的 Node，如果为空表示当前位置可以写入数据，利用 CAS 尝试写入，失败则自旋保证成功。
            * 如果当前位置的 hashcode == MOVED == -1,则需要进行扩容。
            * 如果都不满足，则利用 synchronized 锁写入数据。
            * 如果数量大于 TREEIFY_THRESHOLD 则要转换为红黑树。
            * 甚至取消了 ReentrantLock 改为了 synchronized

Java transient:
    java中的修饰符transient可以应用于类的字段成员，以关闭这些字段成员的序列化。
    每个标记为transient的字段将不会序列化。使用transient关键字向java虚拟机表明，
    transient变量不是对象的持久状态的一部分。
    field not going to be Seriablizable/Deseriable

*/

/**
Java Garbage Collection (垃圾回收)

    * System.gc(): 我们调用System.gc()方法只是起通知作用，JVM什么时候扫描回收对象是JVM自己的状态决定的。

    * 判定Object 对象是否垃圾:
        * 引用计数算法 counter 计数法: 每个对象实例都有一个引用计数器，被引用则+1，完成引用则-1
            * 优点: 执行效率高，程序执行受影响较小
            * 缺点: 无法检测出循环引用的情况，导致无法回收垃圾，从而引发内存泄漏
        * 可达性分析算法: 通过判断对象的引用链遍历（从GC Root开始）是否可达来决定对象是否可以被回收。

    https://juejin.im/post/6844903665241686029
    * 引用 Reference:
        * 强引用 StrongReference:
            * 如果一个对象具有强引用，那垃圾回收器绝不会回收它
            * Ex: Object strongReference = new Object();
            * 当内存空间不足时，Java虚拟机宁愿抛出OutOfMemoryError错误，使程序异常终止，也不会靠随意回收具有强引用的对象来解决内存不足的问题。
            * strongReference = null;
            * 显式地设置strongReference对象为null，或让其超出对象的生命周期范围，则gc认为该对象不存在引用，这时就可以回收这个对象。
            * for (int i = 0; i < size; i++) { elementData[i] = null; }
        * 软引用: SoftReference
            * SoftReference softReference = new SoftReference(page);
            * 如果一个对象只具有软引用，则内存空间充足时，垃圾回收器就不会回收它；
            * 如果内存空间不足了，就会回收这些对象的内存。只要垃圾回收器没有回收它，该对象就可以被程序使用。
            * 当内存不足时，JVM首先将软引用中的对象引用置为null，然后通知垃圾回收器进行回收
            * 软引用可以和一个引用队列(ReferenceQueue)联合使用。如果软引用所引用对象被垃圾回收，
            * JAVA虚拟机就会把这个软引用加入到与之关联的引用队列中。

        * 弱引用
            * WeakReference<String> weakReference = new WeakReference<>(str);
            * 具有弱引用的对象拥有更短暂的生命周期。
            * 在垃圾回收器线程扫描它所管辖的内存区域的过程中，一旦发现了只具有弱引用的对象，不管当前内存空间足够与否，都会回收它的内存。
        * 虚引用
            * 虚引用并不会决定对象的生命周期。如果一个对象仅持有虚引用，那么它就和没有任何引用一样，在任何时候都可能被垃圾回收器回收

    * 回收垃圾的算法:
        * 标记-清除算法（Mark and Sweep）
            * 标记：从根集合进行扫描，对存活的对象进行标记（可达性分析算法）
            * 清除：对堆内存从头到尾进行线性遍历，回收不可达的对象内存
            * 缺点：产生大量的碎片，使得无法给较大的对象分配内存。(下图中BE之间的产生的占用两个单位的不连续碎片)
        * 复制算法:
            * 分为对象面和空闲面
            * 对象在对象面上创建
            * 存活的对象被从对象面复制到空闲面
            * 将对象面所有对象内存清除
            * 优点：
                * 解决了碎片化问题
                * 顺序分配内存，简单高效
            * 不足：
                * 复制耗费时间，且要浪费一半的内存用作空闲面。
                * 适用场景：适用于对象存活率低的场景（年轻代）
        * 标记-整理算法(Compacting)
            * 标记：从根集合进行扫描，对存活的对象进行标记
            * 清除：移动所有存活的对象，且按照内存地址次序依次排列，然后将末端内存地址以后的内存全部回收。
            * 优点:
                * 避免了内存的不连续行，防止出现大量内存碎片，不用浪费一半的内存（对比于复制算法）。
            * 适用场景：适用于存活率高的场景(老年代）
        * 标记分代收集算法(Generational Collector)
            * 垃圾回收算法的组合拳
            * 按照对象生命周期的不同划分区域以采用不同的垃圾回收算法
            * 目的：用于提高JVM垃圾回收的效率 JDK1.8及以后堆中的划分如下：

    * GC 的分类:
        * Minor GC：发生在年轻代(对象出生的地方，该区域的对象几乎都是“朝生夕灭”)中的GC动作，采用的收集算法是复制(需要复制的对象很少).
        * Full GC：主要对老年代(该区域的对象是“幸存”下来的对象，一般不会再轻易“死亡”)的回收，但同时包含了对年轻代的回收（即包含了Minor GC）。采用的收集算法是标记-清除和标记-整理。相比Minor GC慢，执行频率低。
    * 年轻代 (Young Generation):
        * Eden（伊甸园）区：
            1.正常情况对象刚被创建出来的时候在eden区，当eden区内存不足时会放到Survivor区，对象很大时会放入老年代。
            2.发生Minor GC后有用对象年龄+1并被放入到Survivor区
            3.Eden默认占年轻代8的权重，Survivor区两空间分别占1
            两个Survivor区（from、to）:发生Minor GC后有用对象年龄+1，并将存活对象从From使用复制算法到To中，清空From空间，当对象年龄达到一定值后进入老年区（默认15）
    * 对象如何晋升到老年代:
        * 经历一定Minor GC次数后依然存活的对象
        * Survivor区中存放不下的对象
        * 新生成的大对象（-XX:+PretenuerSizeThreshold）
    * 触发Full GC的条件:
        * 老年代空间不足
        * Minor GC晋升到老年代的平均大小大于老年代的剩余空间
        * CMS GC时出现promotion failed，concurrent mode failures


    * 内存结构:
        * 堆: Heap: 线程共享。所有的对象实例以及数组都要在堆上分配。垃圾回收的主要操作区域
            * 控制参数:
                * -Xms设置堆的最小空间大小。-Xmx设置堆的最大空间大小。
                * -XX:NewSize设置新生代最小空间大小。-XX:MaxNewSize设置新生代最小空间大小。
        * 方法区: Method Area: 线程共享。存储类信息、常量、静态变量
            * 控制参数:
                * -XX:PermSize 设置最小空间 -XX:MaxPermSize 设置最大空间。
        * 方法栈: JVM Stack: 线程私有。存储局部变量表、操作栈、动态链接、方法出口，对象指针。
            * -Xss控制每个线程栈的大小。
            * 异常情况:
                * - StackOverflowError： 异常线程请求的栈深度大于虚拟机所允许的深度时抛出；
                * - OutOfMemoryError 异常： 虚拟机栈可以动态扩展，当扩展时无法申请到足够的内存时会抛出。
        * 本地方法栈(Native Method Stack): 线程私有。为虚拟机使用到的Native 方法服务。如Java使用c或者c++编写的接口服务时，代码在此区运行。
            * 控制参数: -Xss控制每个线程的大小
        * 程序计数器（Program Counter Register) 线程私有。
            * 此内存区域是唯一一个在Java 虚拟机规范中没有规定任何OutOfMemoryError 情况的区域。
    * 通信:
        * 线程A与线程B之间如要通信,
            1. 首先，线程A把本地内存A中更新过的共享变量刷新到主内存中去
            2. 然后，线程B到主内存中去读取线程A之前已更新过的共享变量。

    * ClassLoader:
        * 双亲委派模型:
            * 当一个类加载器收到类加载任务的时候，不会先自己加载，而是将这个类加载任务交给其父类加载器来完成，以此类推
            * 当父类加载器无法完成这个类加载任务的时候，才自己完成这个类加载任务。
                * 优点:
                    * 确保类的全局唯一性。不管是哪个加载器加载这个类，最终都是委托给顶层的启动类加载器进行加载，这样就保证了使用不同的类加载器最终得到的都是同样一个Object对象。
                    * 更加安全。解决了各个类加载器的基础类的统一问题，如果不使用该种方式，那么用户可以随意定义类加载器来加载核心api，会带来相关隐患。

    * class 类的生命周期:
        * 加载，验证，准备，解析，初始化，使用，卸载。其中其中验证、准备、解析3个部分统称为连接.

    * 内存泄漏: 指程序中动态分配内存给一些临时对象，但是对象不会被GC所回收，它始终占用内存。即被分配的对象可达但已无用。
        * 静态集合类引起内存泄露
        * 各种连接: 数据库连接(dataSourse.getConnection(), 网络连接(socket)和io连接，除非其显式的调用了其close() 方法将其连接关闭，否则是不会自动被GC 回收的。
    * 内存溢出: 指程序运行过程中无法申请到足够的内存而导致的一种错误。内存溢出通常发生于OLD段或Perm段垃圾回收后，仍然无内存空间容纳新的Java对象的情况(OOM)。
        * 存储的数据超出了指定空间的大小，这时数据就会越界
*/