/**
Link:
https://www.1point3acres.com/bbs/collection/229040

https://www.1point3acres.com/bbs/thread-543725-1-1.html

https://docs.google.com/document/d/1kBRTZphHbxugLuQJQBSSYygoMI4yJqixiXETFKLZps4/edit

https://bobbyliujb.github.io/2018/08/21/vmware/

https://docs.google.com/document/d/1kBRTZphHbxugLuQJQBSSYygoMI4yJqixiXETFKLZps4/edit


EX1:
选择题：
postorder, preorder find inorder DBEAFCG
circular linked list extra pointers: 0
data structure to operate string with insert, delete etc: linked list
queue page replacement什么的，讲道理我题目没读很懂然后背的答案后来自己硬往上套的。。。答案196
telephone : graph
graph time complexity adjacent matrix and list
Coding:
image edit
group anagram
break palindrome

EX2:

选择题：
1. if x<y, y=y-x  if x> y x=y-x  求x,y
2. 时间复杂度 sqrt(floor(x))那道， 地理有 loglog(n)
3. for loop 里是x=2*x 直到x==n，求时间复杂度
4. try catch finally -> finally要打出来
5.不记得了
6. telephone data structure -》 graph
coding: 都是地里原题
1. Anagrams 利口肆鸠
2. intelligent string
3. username system

EX3:
选择题还是地理的面经，基本没变。
三道coding 分别是 UserSystem, GroupAnagrams,  和 Stone Collide.

EX4:
1. Bob erica, 字符串 E=1, M=3, H=5, 遍历算分数比较即可，很直接
2. 回文字符串，改一个value比回文子串小而且不能还是回文，若不存在就输出“IMPOSSIBLE”
3. team formation2, 注意阶乘可能出现溢出，一定要用long去存，然后转成int

EX5:
考了break palindrome，字符串计分和最大正方形

EX6:
选择：
1. 无向图，邻接矩阵和邻接表DFS时间复杂度
2. data structure for telephone network
其他几道记不清具体内容了，有两个是给一段伪码求复杂度，还有一个是给一段伪码和input求结果，之类的，都不难


编程：
1. break palindrome
2. email thread
3. team formation 2

coding:
1. perfect team
2. Build the subsequence 纯dfs
3. Climb the hill

Ex 7
90min
前六道是选择题，都是地里的老题
后三道是coding
1. Remove Anagram from a String Array
2. Build All Subsequence of a String s
3. Remove an Increasing Subsequence from an Array to ensure that the Array only contains unique element

Ex 8:
-DFS in Graph：i. adjacency matrix O(n^2) ii. adjacency list O(e+n)
-Single circular linked list, 有head和tail，需要加几个指针做到O(1) push和poll（取queue的head）的queue：0
-FIFO Page Faults：196
-选择数据结构：Linked List
-一个while循环 终止条件是 a比n大， 每个循环 a*2：logn
-Data Structure for Tel Network：graph


*/
