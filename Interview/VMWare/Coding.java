/**
Leetcode 49: Group Anagram:
// https://leetcode.com/problems/group-anagrams
*/
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> result = new ArrayList<>();
        if (strs == null || strs.length == 0) {
            return res;
        }
        // key: anagram str, value: list idx in result:
        Map<String, Integer> map = new LinkedHashMap<>();
        for (String str : strs) {
            char[] cList = str.toCharArray();
            Arrays.sort(cList);
            String key = String.valueOf(cList);
            if (!map.containsKey(key)) {
                List<String> list = new ArrayList<>();
                list.add(str);
                map.put(key, result.size());
                result.add(list);
            }
            else {
                int idx = map.get(key);
                List<String> list = result.get(idx);
                list.add(str);
            }
        }
        return result;
    }

// Image Editing: == Leetcode 221 Maximal Square:
// https://leetcode.com/problems/maximal-square/
    public int maximalSquare(char[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int size = 0;
        int[][] dp = new int[m+1][n+1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (matrix[i-1][j-1] == '1') {
                    int minVal = Math.min(dp[i-1][j], dp[i][j-1]);
                    dp[i][j] = Math.min(minVal, dp[i-1][j-1]) + 1;
                    size = Math.max(size, dp[i][j]);
                }
            }
        }
        return size * size;
    }

// Intelligent Substring:
    public int getNumOfIntelligentString(String s, int k, String charValue) {
        int l = 0, r = 0, n = s.length(), numOfNormalChar = 0;
        int maxLen = 0;
        while (r < n) {
            if (numOfNormalChar <= k) {
                char rc = s.charAt(r);
                int idx = rc - 'a';
                if (charValue.charAt(idx) == '0') {
                    numOfNormalChar++;
                }
                r++;
            }
            while (numOfNormalChar > k) {
                char lc = s.charAt(l);
                int idx = lc - 'a';
                if (charValue.charAt(idx) == '0') {
                    numOfNormalChar--;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

// Break a Palindrome:
// 回文字串改一个字符使其lexicographically的value比原回文字串少，并且不能再是回文字串。
// 如若不存在，return ”IMPOSSIBLE“。 举例：输入 aba。算法算出aaa但仍是回文字串，所以输出IMPOSSIBLE。
// 输入abba，输出aaba
    public String breakPalindrome(String palindrome) {
        if (palindrome == null || palindrome.length() == 0) {
            return "";
        }

        int n = palindrome.length();
        char[] cList = palindrome.toCharArray();
        for (int i = 0; i < n/2; i++) {
            if (cList[i] != 'a') {
                cList[i] = 'a';
                break;
            }
        }
        String res = String.valueOf(cList);
        if (isPalindrome(res)) {
            return "IMPOSSIBLE";
        }
        else {
            return res;
        }
    }

    private boolean isPalindrome(String s) {
        int i = 0, j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    // Shift String:
    public String getShiftedString(String str, int leftShifts, int rightShifts) {
        if (str == null || str.length() == 0) {
            return "";
        }
        if (leftShifts == rightShifts) {
            return str;
        }
        int n = str.length();
        int newLeftShift = (leftShifts - rightShifts) % n;
        StringBuffer sb = new StringBuffer();
        // abcde
        for (int i = newLeftShift; i < n; i++) {
            sb.append(str.charAt(i));
        }
        for (int i = 0; i < newLeftShift; i++) {
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }

    // Usernames System:
    // 输入list：[bob, alice, bob, alice,bob] 输出：[bob,alice,bob1,alice1,bob2]
    public List<String> usernameSystem(List<String> list) {
        List<String> result = new ArrayList<>();
        if (list == null || list.isEmpty()) {
            return result;
        }
        Map<String, Integer> map = new HashMap<>();
        for (String name : list) {
            if (!map.containsKey(name)) {
                result.add(name);
                map.put(name, 0);
            }
            else {
                int idx = map.get(name) + 1;
                String newName = name + String.valueOf(idx);
                result.add(newName);
                map.put(name, idx);
            }
        }
        return result;
    }

    // build-subsequence:  "ba" -- ["a", "b", "ba"]
    private List<String> buildSubsequence(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        StringBuffer sb = new StringBuffer();
        int n = s.length();
        dfs(res, s, 0, n, sb);
        Collections.sort(res);
        return res;
    }


    private void dfs(List<String> result, String s, int level, int n, StringBuffer sb) {
        if (sb.length() > 0) {
            result.add(sb.toString());
        }
        for (int i = level; i < n; i++) {
            sb.append(s.charAt(i));
            dfs(result, s, i+1, n, sb);
            sb.setLength(sb.length() - 1);
        }
    }

    // Climb the hill:
    // Ex1: [1,2,3,4,5,6,5,7] -> [1,2,3,4,5,6,6,7] -> result = 1
    // Ex2: [9,8,7,2,3,3] -> [9,8,7,3,3,3] -> result = 1
    public int getMinimumCost(int[] input) {
        if (input == null || input.length == 0) {
            return 0;
        }
        return Math.min(getMinimumCost(input, false), getMinimumCost(input, true));
    }

    private int getMinimumCost(int[] input, boolean reverse) {
        int n = input.length;
        int[] sorted = Arrays.copyOf(input, n);
        // descending order
        if (reverse) {
            sorted = sortedReverse(sorted);
        }
        // increasing order:
        else {
            Arrays.sort(sorted);
        }

        int[][] dp = new int[n][n];
        dp[0][0] = Math.abs(input[0] - sorted[0]);

        for (int i = 1; i < n; i++) {
            dp[i][0] = dp[i-1][0] + Math.abs(input[i] - sorted[0]);
        }

        for (int j = 1; j < n; j++) {
            int val = Math.abs(input[0] - sorted[j]);
            dp[0][j] = Math.min(dp[0][j-1], val);
        }

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < n; j++) {
                int val = dp[i-1][j] + Math.abs(input[i] - sorted[j]);
                dp[i][j] = Math.min(dp[i][j-1], val);
            }
        }
        return dp[n-1][n-1];
    }

    private int[] sortedReverse(int[] arr) {
        int n = arr.length;
        Integer[] temp = new Integer[n];
        for (int i = 0; i < n; i++) {
            temp[i] = arr[i];
        }
        Arrays.sort(temp, Collections.reverseOrder());
        for (int i = 0; i < n; i++) {
            arr[i] = temp[i];
        }
        return arr;
    }

    // Email Thread:
    class Content {
        int threadId;
        int contentId;
    }

    public int[][] generateEmailThread(String[] contents) {
        int n = contents.length;
        List<Content> list = new ArrayList<>();
        // 0: email thread id, 1: position within the thread:
        int threadId = 1;
        Map<String, Content> map = new LinkedHashMap<>();
        for (String content : contents) {
            String[] infos = content.split(",");
            String email1 = infos[0].trim();
            String email2 = infos[1].trim();
            String emailContent = getEmailContent(infos);
            String key;
            String fstEmailContent;
            if (!emailContent.contains("---")) {
                fstEmailContent = emailContent.trim();
            }
            else{
                String[] emailContents = emailContent.split("---");
                fstEmailContent = emailContents[emailContents.length-1].trim();
            }
            key = generateKey(email1, email2, fstEmailContent);
            if (!map.containsKey(key)) {
                Content cont = new Content();
                cont.threadId = threadId;
                cont.contentId = 1;
                list.add(cont);
                map.put(key, cont);
                threadId++;
            }
            else {
                Content prevCont = map.get(key);
                Content newCont = new Content();
                newCont.threadId = prevCont.threadId;
                newCont.contentId = prevCont.contentId + 1;
                list.add(newCont);
                map.put(key, newCont);
            }
        }
        int[][] res = new int[n][2];
        for (int i = 0, max = list.size(); i < max; i++) {
            Content cont = list.get(i);
            res[i][0] = cont.threadId;
            res[i][1] = cont.contentId;
        }
        return res;
    }

    private String getEmailContent(String[] infos) {
        StringBuffer sb = new StringBuffer();
        int n = infos.length;
        for (int i = 2; i < n; i++) {
            sb.append(infos[i]).append(",");
        }
        // remove last ,
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    private String generateKey(String email1, String email2, String fstEmailContent) {
        StringBuffer sb = new StringBuffer();
        if (email1.compareTo(email2) < 0) {
            sb.append(email1).append("-");
            sb.append(email2).append("-");
            sb.append(fstEmailContent);
        }
        else {
            sb.append(email2).append("-");
            sb.append(email1).append("-");
            sb.append(fstEmailContent);
        }
        return sb.toString();
    }

    @Test
    public void EmailThreadTest()
    {
        String[] strs1 = {
          "abc@gmail.com, x@gmail.com, hello x, how are you?",
          "c@gmail.com, abc@gmail.com, did you take a look at the event?",
          "x@gmail.com, abc@gmail.com, i am great, how are you?---hello x, how are you?"
        };

        int[][] res = generateEmailThread(strs1);
        int[][] exp = {{1,1},{2,1},{1,2}};
        assertEquals(exp, res);

        String[] strs2 = {
                "john@gmail.com, susan@gmail.com, Are you back from vacation?",
                "bob@gmail.com, alice@gmail.com, did you get the key?",
                "susan@gmail.com, john@gmail.com, Just got int.---Are you back from vacation?"
        };

        res = generateEmailThread(strs2);
        assertEquals(exp, res);

        String[] strs3 = {
                "a@gmail.com, b@gmail.com, first",
                "a@gmail.com, b@gmail.com, anotherFirst",
                "b@gmail.com, c@gmail.com, first2",
                "b@gmail.com, a@gmail.com, second---first"
        };
        int[][] exp1 = {{1,1},{2,1},{3,1},{1,2}};
        res = generateEmailThread(strs3);
        assertEquals(exp1, res);
    }

    // Even subarray:
    // nums = [1,2,3,4] max number of odd elements k = 1:
    public int evenSubArray(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int res = 0;
        int l = 0, r = 0, n = nums.length, numOfOddElem = 0;
        while (r < n) {
            if (nums[r] % 2 == 1) {
                numOfOddElem++;
            }
            while (numOfOddElem > k) {
                if (nums[l] % 2 == 1) {
                    numOfOddElem--;
                }
                l++;
            }
            r++;
            res += r-l;
        }
        return res;
    }
