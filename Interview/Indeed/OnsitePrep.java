/**
Coding:
http://interviewsource.blogspot.com/

https://github.com/interviewdiscussion/files/tree/master/Indeed%20Onsite%E8%AE%B0%E5%BD%95
*/
/*
Leetcode 21: Merge Two sorted Lists:
https://leetcode.com/problems/merge-two-sorted-lists/
*/
    // Time Complexity: O(m+n), where m is the length of l1, and n is the length of l2
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        else if (l2 == null) {
            return l1;
        }
        ListNode head = null;
        if (l1.val < l2.val) {
            head = l1;
            l1 = l1.next;
        }
        else {
            head = l2;
            l2 = l2.next;
        }
        ListNode curr = head;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                curr.next = l1;
                l1 = l1.next;
            }
            else {
                curr.next = l2;
                l2 = l2.next;
            }
            curr = curr.next;
        }
        if (l1 != null) {
            iterateRestOfNodes(l1, curr);
        }
        else if (l2 != null) {
            iterateRestOfNodes(l2, curr);
        }
        return head;
    }

    private void iterateRestOfNodes(ListNode node, ListNode curr) {
        while (node != null) {
            curr.next = node;
            node = node.next;
            curr = curr.next;
        }
    }

/**
Leetcode 23: Merge K sorted List:
https://leetcode.com/problems/merge-k-sorted-lists/

*/
    // Time Complexity: O(n*log K)
    // Space Complexity: O(n)
    // Heap insert: O(log N),
    // Heapify: O(log N)
    // Build Heap: O(N*logN)
    // Find min: O(1)
    // delete min: O(log N)
    // Search: O(N)
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        Queue<ListNode> pq = new PriorityQueue<>(Comparator.comparingInt(l -> l.val));
        for (ListNode list : lists) {
            if (list != null) {
                pq.offer(list);
            }
        }

        ListNode dummyHeader = new ListNode(-1);
        ListNode curr = dummyHeader;

        while (!pq.isEmpty()) {
            ListNode list = pq.poll();
            curr.next = list;
            curr = curr.next;
            if (list.next != null) {
                pq.offer(list.next);
            }
        }

        return dummyHeader.next;
    }

    // Solution 2:
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        return mergeSort(lists, 0, lists.length-1);
    }

    private ListNode mergeSort(ListNode[] lists, int low, int high) {
        if (low >= high) {
            return lists[low];
        }
        int mid = low + (high - low) / 2;
        ListNode l1 = mergeSort(lists, low, mid);
        ListNode l2 = mergeSort(lists, mid+1, high);
        return mergeTwoList(l1, l2);
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2){
        if (l1 == null) {
            return l2;
        }
        else if (l2 == null) {
            return l1;
        }

        ListNode head = null;
        if (l1.val < l2.val){
            head = l1;
            l1 = l1.next;
        }
        else {
            head = l2;
            l2 = l2.next;
        }

        ListNode curr = head;
        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val){
                curr.next = l1;
                l1 = l1.next;
            }
            else {
                curr.next = l2;
                l2 = l2.next;
            }
            curr = curr.next;
        }
        curr.next = (l1 != null) ? l1 : l2;
        return head;
    }

/**
Given a stream of input, and a API int getNow() to get the current time stamp,
Finish two methods:

1. void record(int val) to save the record.
2. double getAvg() to calculate the averaged value of all the records in 5 minutes.

follow up: Get Medium:
Q: does getMedium method be called very frequently?
Q: Do you want to maintain O(1) on record()?

Naive solution: Leetcode 295: Find Median from Data Stream, Use two heap, one with max and one with min to get the medium of the data stream.
https://leetcode.com/problems/find-median-from-data-stream/

需要考虑的极端情况是，如果一直都在record，但是没有人call getAverage()，有可能会内存不够
解决的方法有2种
方法1：可以每次record的时候，对queue进行一次过滤，把超过5分钟的entry删掉，然后call getAverage()的时候，再进行一次过滤
这个方法的好处是，把需要过滤的元素平摊到了每一次record上，最后call getAverage()的时候，需要规律的元素会少一些
tradeoff就是，会拉低每次record的效率，因为每次record都在过滤

方法2：每次call getAverage()的时候，对queue进行一次整体过滤
好处在于，record的效率会相对快，因为不用去过滤掉过期的元素
缺点就是，有可能会内存不够，因为过滤这个action只发生在getAverage()的时候
个人建议，和面试官讨论需求，尽可能多的提供解决方案，提出tradeoff，选最符合需求的方案

follow up: 我们需要找median，就是中位数
其实就是一个find median in unsorted array
方法也有两种：
方法1：quick select，每次平均速度O（n），最坏速度O(n^2)，如果需要call n次的话，就是n^2和n^3
方法2：max/min heap，总体速度O(nLogn)
我提出了，可以根据需求来决定用那一种，如果找median的次数没有那么多，那么选quick select，因为单次的平均速度是O(n)
如果call的次数比较多，那么方法2比较好
面试官表示满意，最后留了10分钟聊天
*/

    class Event {
        int val;
        long timeStamp;
        public Event(int val, long timeStamp) {
            this.val = val;
            this.timeStamp = timeStamp;
        }
    }

    private List<Event> eventQueue;
    private int sum;
    private static final int EXPIRE_MINUES = 5;

    public MovingStreamAverage() {
        this.eventQueue = new ArrayList<>();
        this.sum = 0;
    }

    public void record(long timeStamp, int val) {
        long currTimeStamp = getNowTimeStamp();
        removeExpiredEvents(currTimeStamp);

        Event event = new Event(val, timeStamp);
        events.add(event);
        sum += val;
    }

    public double getAvg() {
        long currTimeStamp = getNowTimeStamp();
        removeExpiredEvents(currTimeStamp);
        // avoid divide by 0
        if (events.isEmpty()) {
            return 0;
        }
        return sum / events.size();
    }

    private void removeExpiredEvents(long currTimeStamp) {
        while (!events.isEmpty()) {
            Event event = events.peek();
            if (isEventExpired(currTimeStamp, event)) {
                events.remove(0);
                sum -= event.val;
            }
            else {
                break;
            }
        }
    }

    private boolean isEventExpired(long currTimeStamp, Event event) {
        long prevTimeStamp = event.timeStamp;
        return (currTimeStamp - prevTimeStamp) > EXPIRE_MINUES * 60 * 1000;
    }
    /**
     * Time in milliseconds
     * */
    public long getNowTimeStamp() {
        Date date = new Date();
        return date.getTime();
    }

    // Follow up: getMedium, Solution 1: use quick select: quickly finds the k-th smallest element of an unsorted array of n elements.
    // Quick select time complexity: O(N), with worst case of O(N^2), increasing order of element.
    // pivot is always the largest element and k is always 1
    private double getMedium() {
        int size = events.size();
        // use quick select to find the kth smallest item in the arraylist
        if (size % 2 == 1) {
            int k = size / 2;
            return kthSmallest(0, size-1, k);
        }
        else {
            Event event1 = kthSmallest(0, size-1, size/2);
            Event event2 = kthSmallest(0, size-1, size/2-1);
            return (event1.val + event2.val) / 2.0;
        }
    }

    private Event kthSmallest(int low, int high, int k) {
        int idx = partition(low, high);
        if (idx == k) {
            return events.get(idx);
        }
        else if (idx < k) {
            return kthSmallest(idx+1, high, k);
        }
        else {
            return kthSmallest(low, idx-1, k);
        }
    }

    private int partition(int low, int high) {
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (events.get(j).val <= events.get(pivot).val) {
                swapEvents(i, j);
                i++;
            }
        }
        swapEvents(i, pivot);
        return i;
    }

    private void swapEvents(int i, int j) {
        Event tempEvent = events.get(i);
        events.set(i, events.get(j));
        events.set(j, tempEvent);
    }

    // Follow up: getMedium: solution 2:
    private Queue<Integer> minQueue;
    private Queue<Integer> maxQueue;

    /** initialize your data structure here. */
    public MedianFinder() {
        this.minQueue = new PriorityQueue<>();
        this.maxQueue = new PriorityQueue<>(Collections.reverseOrder());
    }

    public void addNum(int num) {
        if (minQueue.isEmpty()) {
            minQueue.offer(num);
            return;
        }
        if (minQueue.peek() <= num) {
            minQueue.offer(num);
        }
        else {
            maxQueue.offer(num);
        }
        rebalance();
    }

    private void rebalance() {
        if (minQueue.size() - maxQueue.size() > 1) {
            maxQueue.offer(minQueue.poll());
        }
        else if (maxQueue.size() > minQueue.size()) {
            minQueue.offer(maxQueue.poll());
        }
    }

    public double findMedian() {
        if (minQueue.size() == maxQueue.size()) {
            return (minQueue.peek() + maxQueue.peek()) / 2.0;
        }
        else {
            return minQueue.peek();
        }
    }

    // array solution:
    private int getKthSmallestItem(int[] arr, int low, int high, int k) {
        int idx = partition(arr, low, high);
        if (idx == k) {
            return arr[idx];
        }
        else if (idx < k) {
            return getKthSmallestItem(arr, idx+1, high, k);
        }
        else {
            return getKthSmallestItem(arr, low, idx-1, k);
        }
    }

    private int partition(int[] arr, int low, int high) {
        // choose the last item as pivot item:
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (arr[j] <= arr[pivot]) {
                swap(arr, i, j);
                i++;
            }
        }
        swap(arr, i, pivot);
        return i;
    }

/**
Min cost from root to leaf:
*/
class Edge {
    Node node;
    int cost; //大于等于0
}
class Node {
    List<Edge> edges;
}


/**
Dice Sum:
写一个函数float sumPossibility(int dice, int target)，就是投dice个骰子，求最后和为target的概率。因为总共的可能性是6^dice，所以其实就是combination sum，求dice个骰子有多少种组合，使其和为target。

然后要求优化，用dp，最后结束代码写的是两者结合的memorized search吧，面试官走的时候还说了句such a good solution

https://gist.github.com/diegozeng/6c964f623bbeaa716526
*/
    // Solution 1: Brute Force: Time Complexity: O(6^Dice)
    class Info {
        int count;
        public Info() {
            this.count = 0;
        }
    }
    static private final int SIX = 6;
    public float sumPossibility(int dice, int target) {
        int total = (int) Math.pow(6, dice);
        Info info = new Info();
        dfs(dice, target, info);
        return (float) info.count / total;
    }

    private void dfs(int numOfDice, int target, Info info) {
        if (numOfDice == 0 && target == 0) {
            info.count++;
            return;
        }
        if (numOfDice == 0 || target < 0) {
            return;
        }
        for (int i = 1; i <= SIX; i++) {
            dfs(numOfDice-1, target-i, info);
        }
    }

    // With cache:
    private Map<String, Integer> cacheMap = new HashMap<>();

    public float sumPossibility(int dice, int target) {
        int total = (int) Math.pow(6, dice);
        int count = dfsWithCache(dice, target);
        return (float) count / total;
    }

    private int dfsWithCache(int numOfDice, int target) {
        if (numOfDice == 0 && target == 0) {
            return 1;
        }
        if (numOfDice == 0 || target < 0) {
            return 0;
        }
        String key = numOfDice + "-" + target;
        if (cacheMap.containsKey(key)) {
            return cacheMap.get(key);
        }
        int count = 0;
        for (int i = 1; i <= 6; i++) {
            int num = dfsWithCache(numOfDice-1, target-i);
            count += num;
        }
        cacheMap.put(key, count);
        return count;
    }

    // DP solution:
    // Time Complexity: O(6 * dice * target)
    private float sumPossibilityWithDP(int dice, int target) {
        int total = (int) Math.pow(6, dice);
        // dp[i][j]: num of count with i dice to sum up to j
        // dp[i][j] += dp[i-1][j-k] where k = 1...6 and k < j
        // Ex: dp[2][7], j = 7:
        //     dp[1][1] with dice 6
        //     dp[1][2] with dice 5
        //     dp[1][3] with dice 4
        int[][] dp = new int[dice+1][target+1];

        for (int i = 1, n = Math.min(target, 6); i < n; i++) {
            dp[1][i] = 1;
        }
        // Starting from two dices:
        for (int i = 2; i <= dice; i++) {
            for (int j = 1; j <= target; j++) {
                int count = 0;
                for (int k = 1; k <= 6 && k < j; k++) {
                    count += dp[i-1][j-k];
                }
                dp[i][j] = dp[i][j] + count;
            }
        }
        int count = dp[dice][target];
        return (float) count / total;
    }

/**
Git Commit:
int[][] commits = new int[][]{{0, 1}, {1, 3}, {3, 5}, {0, 2}, {2, 4}, {4, 5}};
int[] commit: commit[0] = from, commit[1] = to

Q1: 第一问给一个commit（node），BFS输出所有commits(nodes)
*/

    // Run Time Complexity: O(V + E)
    public List<Integer> getAllCommitsWithCommitNode(int[][] commits, int startNode) {
        // key -> commit, val -> list of its neighbor commits
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int[] commit : commits) {
            int from = commit[0];
            int to = commit[1];
            List<Integer> neighbors = graph.getOrDefault(from, new ArrayList<>());
            neighbors.add(to);
            graph.put(from, neighbors);
        }

        // BFS to find all commits connects with this commit:
        List<Integer> res = new ArrayList<>();
        Queue<Integer> queue = new LinkedList<>();
        Set<Integer> visited = new HashSet<>();
        queue.offer(startNode);

        while (!queue.isEmpty()) {
            int cmt = queue.poll();
            if (visited.contains(cmt)) {
                continue;
            }
            res.add(cmt);
            visited.add(cmt);
            if (!graph.containsKey(cmt)) {
                continue;
            }
            List<Integer> neighbors = graph.get(cmt);
            for (int neighbor : neighbors) {
                queue.offer(neighbor);
            }
        }
        return res;
    }

/**
Q2: Find nearest common parents of two given commit:
两个commits （nodes），找到他们的最近的公共parent
*/
    // Time Complexity: O(V + E), BFS
    public int findNearestCommonParent(int[][] commits, int p1, int p2) {
        // key -> commit, val -> parent commits
        Map<Integer, Set<Integer>> parMap = new HashMap<>();
        for (int[] commit : commits) {
            int from = commit[0];
            int to = commit[1];
            Set<Integer> parSet = parMap.getOrDefault(to, new HashSet<>());
            parSet.add(from);

            parMap.put(to, parSet);
        }

        // p1 or p2 is the root node, so they don't have common ancestor.
        if (!parMap.containsKey(p1) || !parMap.containsKey(p2)) {
            return -1;
        }

        // Find all the parents of p1 and mark the parent distance from p1
        // key -> parNode, val -> distance:
        Map<Integer, Integer> firstDistanceMap = new HashMap<>();
        firstDistanceMap.put(p1, 0);
        // BFS on all the parents of p1:
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(p1);

        Set<Integer> firstParents = new HashSet<>();
        while (!queue.isEmpty()) {
            int child = queue.poll();
            if (!parMap.containsKey(child)) {
                continue;
            }
            Set<Integer> parents = parMap.get(child);
            firstParents.addAll(parents);
            int dist = firstDistanceMap.get(child);
            for (int parent : parents) {
                int parDist = dist + 1;
                firstDistanceMap.put(parent, parDist);
            }
        }

        // BFS on all the parents of p2:
        Map<Integer, Integer> secondDistanceMap = new HashMap<>();
        secondDistanceMap.put(p2, 0);

        // get the min total distance of this parent node to both p1 and p2 nodes.
        int minDistance = Integer.MAX_VALUE;

        int res = -1;
        queue.clear();
        queue.offer(p2);
        while (!queue.isEmpty()) {
            int child = queue.poll();
            if (!parMap.containsKey(child)) {
                continue;
            }
            Set<Integer> parents = parMap.get(child);
            int dist = secondDistanceMap.get(child);
            for (int parent : parents) {
                int parDist = dist + 1;
                secondDistanceMap.put(parent, parDist);
                if (firstParents.contains(parent)) {
                    int firstDist = firstDistanceMap.get(parent);
                    int secondDist = parDist;
                    int sum = firstDist + secondDist;
                    if (sum < minDistance) {
                        minDistance = sum;
                        res = parent;
                    }
                }
            }
        }
        return res;
    }

/**
Unrolled Linked List:
linkedelist中的每个节点里存了个固定长度的数组，但是数组未必满。进行插入操作的时候，如果要插入的节点的数组满了，可以考虑新建个节点插当前节点的数组的溢出的元素。

比如说已知每个node max array size 为5，现在有 [a,b,c] -> [d,e], 那么get(3)要返回d，insert(3,f) 可以变成 [a,b,c,f] -> [d,e]或者[a,b,c] -> [f,d,e]或者[a,b,c] -> [f] -> [d,e], 但是如果max size是3，那么第一种答案就不行。

get(int idx)
insert(char ch, int idx)
*/
    static private final Character EMPTY_CHAR = ' ';
    static private Integer MAX_CHAR_LEN = 5;

    class UnrolledLinkedList {
        Node head;
        // total capacity of this linked list:
        int length;
        public UnrolledLinkedList(Node head, int length) {
            this.head = head;
            this.length = length;
        }
    }

    class Node {
        char[] cList;
        int len;
        Node next;

        public Node() {
            this.cList = new char[MAX_CHAR_LEN];
        }
    }

    private UnrolledLinkedList list;

    public char get(int idx) {
        Node curr = list.head;
        int total = list.length;
        if (curr == null || total <= 0 || idx < 0 || idx > total) {
            return EMPTY_CHAR;
        }
        while (curr != null && curr.len <= idx) {
            idx -= curr.len;
            curr = curr.next;
        }
        // If idx is out of bound:
        if (curr == null) {
            return EMPTY_CHAR;
        }
        return curr.cList[idx];
    }

    public void insert(char ch, int idx) {
        Node curr = list.head;
        int total = list.length;
        if (curr == null || total <= 0 || idx < 0 || idx > total) {
            return;
        }
        while (curr != null && curr.len < idx) {
            idx -= curr.len;
            curr = curr.next;
        }
        // not inserted in the correct idx.
        // Ex: [a,b,c], max = 3, insert('d', 5)
        if (curr == null) {
            return;
        }
        if (curr.len == MAX_CHAR_LEN) {
            Node newNode = new Node();
            // insert character at the end of the the full list.
            // create a new node to insert it
            if (idx == MAX_CHAR_LEN) {
                newNode.cList[0] = ch;
                newNode.len = 1;
            }
            else {
                newNode.len = MAX_CHAR_LEN - idx;
                // overwrite with curr Node, move (idx, curr.len) chars to nextNode
                for (int i = idx, n = curr.len; i < n; i++) {
                    newNode.cList[i-idx] = curr.cList[i];
                    curr.cList[i] = EMPTY_CHAR;
                }
                curr.cList[idx] = ch;
                curr.len = idx+1;
            }
            newNode.next = curr.next;
            curr.next = newNode;
        }
        else {
            // copy all characters (idx to len) forward one step:
            for (int i = curr.len; i > idx; i--) {
                curr.cList[i] = curr.cList[i-1];
            }
            curr.cList[idx] = ch;
            curr.len++;
        }
    }

    public void delete(int idx) {
        Node curr = list.head;
        int len = list.length;
        if (curr == null || idx < 0 || len == 0 || idx > len) {
            return;
        }
        while (curr != null && curr.len <= idx) {
            idx -= curr.len;
            curr = curr.next;
        }
        if (curr == null) {
            return;
        }
        // mark curr idx as empty char
        curr.cList[idx] = EMPTY_CHAR;
        for (int i = idx, n = curr.len; i < n-1; i++) {
            curr.cList[i] = curr.cList[i+1];
        }
        curr.cList[curr.len-1] = EMPTY_CHAR;
        curr.len--;
    }

/**
Sorted Stream App: n个非常长的有序数组，找出其中出现了至少k次的数字

stream1: -1 3 5 6 6 7 9 12 14
stream2: 0 3 5 5 9 13 15
stream3: 5 5 7 11 19 21 51
         k = 3

Arrays in stream are too long, we cannot read through the end of each stream and find the number of frequency >= k

n个sorted list（很长不能放入内存 有个iterator class的interface 能调用next(), hasNext(), peek()三个函数） 找出所有在不同lists里面至少出现k次的数（重复多次出现在一个list只算出现在一个list里面）
heap 空间O(n), 时间O(Nlogn) N为所有元素的个数

<A> merge k sorted streams。但是要求是合并的结果中只包含出现在K个stream以上的元素，同一个stream中重复的次数不算。
<B> 输入是n个data stream 和 一个常数K，返回至少出现K次的数的list, 这个数必须在多于1个Stream中出现过。

Run Time Complexity: O(m*n*log(m)), where m of streams, avg length of stream is n
*/

    class Data {
        int val;
        Stream stream;

        public Data(Stream s) {
            this.stream = s;
            this.val = s.getValue();
        }
    }

    class Stream {
        List<Integer> list;

        public Stream(List<Integer> list) {
            this.list = list;
            this.iter = list.listIterator();
        }

        Iterator<Integer> iter;
        // move()
        public boolean move() {
            return iter.hasNext();
        }
        // getValue()
        public int getValue() {
            return iter.next();
        }
    }

    /**
     Assume item in same stream cannot count multiple times:
     */
    public List<Integer> getFrequentNumWithAtLeastKTimes(List<Stream> streams, int k) {
        List<Integer> res = new ArrayList<>();

        Queue<Data> pq = new PriorityQueue<>(Comparator.comparingInt(d -> d.val));
        for (Stream s : streams) {
            if (s.move()) {
                pq.offer(new Data(s));
            }
        }

        while (!pq.isEmpty()) {
            Data curr = pq.poll();
            int currVal = curr.val;
            int count = 1;
            while (curr.stream.move()) {
                int nextVal = curr.stream.getValue();
                if (currVal != nextVal) {
                    curr.val = nextVal;
                    pq.offer(curr);
                    break;
                }
            }

            // update the head of other stream:
            while (!pq.isEmpty() && pq.peek().val == currVal) {
                count++;
                Data secData = pq.poll();
                while (secData.stream.move()) {
                    int nextVal = secData.stream.getValue();
                    if (currVal != nextVal) {
                        secData.val = nextVal;
                        pq.offer(secData);
                        break;
                    }
                }
            }

            if (count >= k) {
                res.add(currVal);
            }

            // exit in advance
            if (pq.size() < k) {
                break;
            }
        }

        return res;
    }

/**
Job Id Storage:
list of jobIds, Long type

实现： 1. public boolean isExpire();
      2. public void expire();

64bit的操作系统里面，16GB的内存如何存下4 Billion个jobid。
long: 64 bits.
16 GB = 2^4 * 2^30 = 2^34 bits
4 1,000,000,000 = 4 * 2^30

还有用16MB怎么存下一大堆jobid。
2^4 * 2 ^ 20 = 2^24 bits

1. Use Set,
2. Use bitSet: one bit to store one long
3. Bloom Filter? Less Memory Consumption but there will be false positive.
4. Use range?

*/
    // By default it is 2^6 array of boolean
    BitSet set = new BitSet();
    // A one-constructor with an integer argument to create an instance of the BitSet class with an initial size of the integer argument representing the number of bits.
    BitSet set = new BitSet((int) Math.pow(2, 64));

    class Interval {
        long startId;
        long endId;
        public Interval(long startId, long endId) {
            this.startId = startId;
            this.endId = endId;
        }
    }

    private List<Interval> intervals = new ArrayList<>();

    public void expire(long jobId) {
        if (intervals.isEmpty()) {
            intervals.add(new Interval(jobId, jobId));
        }
        else {
            int size = intervals.size();
            // id, [s, e], ...
            if (jobId < intervals.get(0).startId) {
                if (jobId + 1 == intervals.get(0).startId) {
                    intervals.get(0).startId = jobId;
                }
                else {
                    intervals.add(0, new Interval(jobId, jobId));
                }
            }
            // []...[s, e], jobId
            else if (jobId > intervals.get(size - 1).endId) {
                if (intervals.get(size - 1).endId + 1 == jobId) {
                    intervals.get(size - 1).endId = jobId;
                }
                else {
                    intervals.add(new Interval(jobId, jobId));
                }
            }
            else {
                for (int i = 0, n = intervals.size(); i < n; i++) {
                    Interval curr = intervals.get(i);
                    //    jobId
                    // [s,      e]
                    if (curr.startId <= jobId && jobId <= curr.endId) {
                        break;
                    }
                    //        jobId
                    // [s, e]       [s,e]
                    else if (i < n-1 && curr.endId < jobId && jobId < intervals.get(i+1).startId) {
                        Interval next = intervals.get(i+1);
                        // [s,e]jobId[s,e]
                        if (curr.endId == jobId - 1 && next.startId == jobId + 1) {
                            curr.endId = next.endId;
                            intervals.remove(i+1);
                        }
                        else if (curr.endId == jobId - 1) {
                            curr.endId = jobId;
                        }
                        else if (next.startId == jobId + 1) {
                            next.startId = jobId;
                        }
                        else {
                            intervals.add(new Interval(jobId, jobId));
                        }
                        break;
                    }
                }
            }

        }
    }

    public boolean isExpired(long jobId) {
        for (Interval interval : intervals) {
            if (interval.startId <= jobId && jobId <= interval.endId) {
                return true;
            }
        }
        return false;
    }

/**
一道是validate python indentation，就是给三个rules，（
1. 第一行indentation必须是0
2. 上一行是冒号，当前这一行的indentation要比上一行大，
3. 上一行是普通行，这一行的indentation可以小于等于上一行的），然后判断给你的这段python 代码是不是valid的

规则1，第一行的code必须没有缩进，
规则2，尾巴是冒号的行一定是control statement，
规则3，control statement下面一行一定要有更多的缩进，
规则4，同一个block里面的缩进一定要相同。

如果python代码只有一行，这一行最后以：结尾，你这代码怎么办。

如果有独立一行的comment line怎么处理

如果有inline comment line怎么改代码

我吸取了前人的经验，先分析了一下为什么stack比暴力解法要好，
*/

    public boolean validate(List<String> contents) {
        boolean newTab = false;
        Stack<Integer> s = new Stack<>();
        // first line of content starts from zero indentation.
        s.push(0);
        for (int i = 0, n = contents.size(); i < n; i++) {
            String content = contents.get(i);
            int currIndent = s.peek();
            int numOfSpaces = countSpaces(content);
            // It is the comment line, or spaces only. skip:
            if (numOfSpaces == -1 || numOfSpaces == content.length()) {
                continue;
            }
            if (currIndent == numOfSpaces && !newTab) {
                newTab = endsWithColon(content);
            }
            else if (currIndent < numOfSpaces && newTab) {
                s.push(numOfSpaces);
            }
            // If last line has colon and current line has indentation less than it,
            // return false
            /*
              if 5 > 2:
             print("s")
             */
            else if (currIndent >= numOfSpaces && newTab) {
                return false;
            }
            else if (currIndent > numOfSpaces) {
                while (currIndent != numOfSpaces && currIndent > 0) {
                    s.pop();
                    currIndent = s.peek();
                }
                if (currIndent != numOfSpaces) {
                    return false;
                }
            }
            else {
                return false;
            }

            newTab = endsWithColon(content);
        }
        // The last line should not ends with a colon
        return !newTab;
    }

    /*
     inline comment:
     if 5 > 2: # this is a comment:
    */
    private boolean endsWithColon(String content) {
        // check if it has inline comment:
        boolean inlineComment = false;
        int n = content.length();
        int i = n-1;
        for (; i >= 0; i--) {
            char c = content.charAt(i);
            if (c == '#') {
                inlineComment = true;
                break;
            }
        }

        if (inlineComment) {
            n = i;
        }
        else {
            n = content.length();
        }
        i = n-1;

        for (; i >= 0; i--) {
            char c = content.charAt(i);
            if (c == ' ') {
                continue;
            }
            if (c == ':') {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }

    private int countSpaces(String content) {
        int count = 0;
        for (char c : content.toCharArray()) {
            if (c == ' ') {
                count++;
            }
            else if (c == '#') {
                return -1;
            }
            else {
                break;
            }
        }
        return count;
    }

/**
Auto Complete:
Words: "ab", "a", "de", "abde"
Prefix: "ab"
Result: "ab", "abde"

Words: "marketing", "make", "stop", "development", "develop", "dev"
Prefix: "dev"
Result: "development", "develop", "dev"

Solution 2:
给一个string的list作为数据库，输入一个string比如“py”，输出一个string的list中py开头的string的list（解：把给的string的list sort之后用binary search）
【这题挂了。。楞是没想出来用bianry search。。给了提示才写了】
s
*/

    class TrieNode {
        boolean isWord;
        String word;
        TrieNode[] children;
        public TrieNode() {
            this.children = new TrieNode[26];
        }
    }

    private TrieNode root;

    public AutoComplete() {
        root = new TrieNode();
    }

    public void addWords(List<String> words) {
        for (String word : words) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (curr.children[c - 'a'] == null) {
                    curr.children[c - 'a'] = new TrieNode();
                }
                curr = curr.children[c - 'a'];
            }
            curr.isWord = true;
            curr.word = word;
        }
    }

    public List<String> search(String prefix) {
        List<String> res = new ArrayList<>();
        TrieNode curr = findPrefixNode(root, prefix, 0);
        if (curr == null) {
            return res;
        }
        dfs(curr, res);
        return res;
    }

    private TrieNode findPrefixNode(TrieNode curr, String prefix, int idx) {
        if (curr == null) {
            return null;
        }
        if (idx == prefix.length() && curr != null) {
            return curr;
        }
        char c = prefix.charAt(idx);
        return findPrefixNode(curr.children[c-'a'], prefix, idx+1);
    }

    private void dfs(TrieNode curr, List<String> res) {
        if (curr == null) {
            return;
        }
        if (curr.isWord) {
            res.add(curr.word);
        }
        for (int i = 0; i < 26; i++) {
            TrieNode child = curr.children[i];
            dfs(child, res);
        }
    }

    // Solution 2: Use Binary Search:
    public List<String> searchWithBinarySearch(List<String> words, String prefix) {
        Collections.sort(words);
        List<String> res = new ArrayList<>();
        int low = 0;
        int high = words.size() - 1;
        int fstIdx = -1;
        int matchIdx = 0;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            String word = words.get(mid);
            if (matchIdx >= word.length()) {
                low = mid + 1;
                continue;
            }
            char c1 = word.charAt(matchIdx);
            char c2 = prefix.charAt(matchIdx);
            if (c1 == c2) {
                fstIdx = mid;
                matchIdx++;
                high = mid - 1;
            }
            else if (c1 < c2) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
            if (matchIdx == prefix.length()) {
                break;
            }
        }

        if (fstIdx == -1) {
            return res;
        }
        low = fstIdx;
        high = words.size() - 1;
        int sndIdx = -1;
        matchIdx = 0;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            String word = words.get(mid);
            if (matchIdx >= word.length()) {
                high = mid - 1;
                continue;
            }
            char c1 = word.charAt(matchIdx);
            char c2 = prefix.charAt(matchIdx);
            if (c1 == c2) {
                sndIdx = mid;
                matchIdx++;
                low = mid + 1;
            }
            else if (c1 < c2) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
            if (matchIdx == prefix.length()) {
                break;
            }
        }

        for (int i = fstIdx; i <= sndIdx; i++) {
            res.add(words.get(i));
        }
        return res;
    }


/**
输入一组 raw titles, 例如"senior software engineer"，还有一组clean title "software engineer",
"mechanical engineer"
每个raw title 求匹配得分最高的clean title，这里 senior software enginner
和software engineer 有两个词匹配，得分2；和mechanical engineer有一个词匹配，得分1，返回software engineer

follow up 是raw title和clean title中有duplicate word怎么办
raw title和clean title中有duplicate word怎么办
比如raw = "a a a b", clean = "a a b"
*/

    static private final String SPACE = " ";
    public String getHigheestScoreTitle(String rawTitle, String[] cleanTitles) {
        int maxScore = -1;
        String res = "";
        for (String cleanTitle : cleanTitles) {
            String[] A = rawTitle.split(SPACE);
            String[] B = cleanTitle.split(SPACE);
            int score = longestCommonSubstring(A, B);
            if (score > maxScore) {
                maxScore = score;
                res = cleanTitle;
            }
        }
        return res;
    }

    private int longestCommonSubstring(String[] A, String[] B) {
        int m = A.length;
        int n = B.length;
        int[][] dp = new int[m+1][n+1];
        int maxLen = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (A[i-1].equals(B[j-1])) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                    maxLen = Math.max(maxLen, dp[i][j]);
                }
                else {
                    dp[i][j] = 0;
                }
            }
        }
        return maxLen;
    }

/** 
    给一个list， 如何把里面的字符分配到尽量少的子list里，并且每个子list没有重复元素。
    比如
        ['a','b','c','a','a','b']， 可以分成['a', 'b', 'c'], ['a', 'b'], ['a']
        ['a', 'a', 'a', 'b', 'b', 'b']，可以分成['a', 'b'], ['a', 'b'], ['a', 'b']
*/
    public List<List<Character>> divideListEvenly(List<Character> chars) {
        List<List<Character>> res = new ArrayList<>();
        Map<Character, Integer> freqMap = new LinkedHashMap<>();
        for (char c : chars) {
            int count = freqMap.getOrDefault(c, 0);
            count++;
            freqMap.put(c, count);
        }

        for (char c : freqMap.keySet()) {
            int count = freqMap.get(c);
            for (int i = 0; i < count; i++) {
                if (res.size() <= i) {
                    res.add(new ArrayList<>());
                }
                res.get(i).add(c);
            }
        }
        return res;
    }

/**
Leetcode 243: Shortest Word Distance:
https://leetcode.com/problems/shortest-word-distance 
https://www.lintcode.com/problem/shortest-word-distance
*/

    public int shortestDistance(String[] words, String word1, String word2) {
        // Write your code here
        int idx1 = -1;
        int idx2 = -1;
        int minDist = words.length;
        for (int i = 0, n = words.length; i < n; i++) {
            String word = words[i];
            if (word1.equals(word)) {
                idx1 = i;
            }
            else if (word2.equals(word)) {
                idx2 = i;
            }
            if (idx1 != -1 && idx2 != -1) {
                int dist = Math.abs(idx2 - idx1);
                minDist = Math.min(minDist, dist);
            } 
        }
        return minDist;
    }

/**
LC 244: Shortest Word Distance II:
https://leetcode.com/problems/shortest-word-distance-ii

*/
public class WordDistance {
    private Map<String, List<Integer>> indexMap;

    public WordDistance() {
        this.indexMap = new HashMap<>();
    }

    public void addWords(List<String> words) {
        for (int i = 0, n = words.size(); i < n; i++) {
            String word = words.get(i);
            List<Integer> indexes = indexMap.getOrDefault(word, new ArrayList<>());
            indexes.add(i);
            indexMap.put(word, indexes);
        }
    }

    // Run Time Complexity: O(m*n), m = word1 freq, n = word2 freq
    private int shortestDistance(String word1, String word2) {
        if (!indexMap.containsKey(word1) || !indexMap.containsKey(word2)) {
            return -1;
        }
        int minDist = Integer.MAX_VALUE;
        List<Integer> fstIndexes = indexMap.get(word1);
        List<Integer> sndIndexes = indexMap.get(word2);
        for (int i = 0, n = fstIndexes.size(); i < n; i++) {
            for (int j = 0, m = sndIndexes.size(); j < m; j++) {
                int dist = Math.abs(fstIndexes.get(i) - sndIndexes.get(j));
                minDist = Math.min(minDist, dist);
            }
        }
        return minDist;
    }

    private int shortestDistance2(String word1, String word2) {
        if (!indexMap.containsKey(word1) || !indexMap.containsKey(word2)) {
            return -1;
        }
        List<Integer> fstIndexes = indexMap.get(word1);
        List<Integer> sndIndexes = indexMap.get(word2);
        int i = 0;
        int j = 0;
        int m = fstIndexes.size();
        int n = sndIndexes.size();
        int minDist = Integer.MAX_VALUE;

        while (i < m && j < n) {
            int fstIdx = fstIndexes.get(i);
            int sndIdx = sndIndexes.get(j);
            int dist = Math.abs(fstIdx - sndIdx);
            minDist = Math.min(minDist, dist);
            // move forward the smaller idx:
            if (fstIdx < sndIdx) {
                i++;
            }
            else {
                j++;
            }
        }

        return minDist;
    }

}

/**
Min Path from Top to Leaf, follow up，把tree改成DAG
第一轮：给一颗binary tree，有以下数据结构
class Edge {
    Node node;
    int cost; //大于等于0. more info on 1point3acres.com
}
class Node {
    List<Edge> edges;
}
找从root到叶节点cost之和最小的路径，返回该leaf node。（dfs）
follow-up：如果不是binary tree的结构，而是任意的单向图，问代码还是否work（yes）
有没有优化的地方？（我用hashmap存下每个节点到叶节点的distance，这样再次访问到该叶节点就不必dfs下去）。时间复杂度？（优化后是O（V+E））

cache的是边，主面试官表示赞同，shadow说这样cache不行，要cache node

followup：改成DAG行不行---再优化一下（解：加一个hashmap），时间复杂度是多少？（DAG优化之前是O(V!)优化之后是O(V+E)）

Follow up: 问程序能不能提前退出，答当一个node的值已经比你存的最小值大的时候推出

你应该自底向上搜索，而不是自顶向下

最短路径道题，面试官默认是所有cost都是正数，然后也没有环。所以N个点，每个都能连N-1，继续连N-2，所以复杂度是N！
*/
    // Run Time Complexity: O(V!), 
    // After adding hashMap: O(V + E)
    class Edge {
        // the target node it connects to
        Node node;
        int cost;
        public Edge(Node node, int cost) {
            this.node = node;
            this.cost = cost;
        }
    }

    class Node {
        List<Edge> edges;
        // Only if it is used Dijkstra’s Algorithm.
        int dist;
        public Node() {
            this.edges = new ArrayList<>();
            // Only if it is used Dijkstra’s Algorithm.
            this.dist = Integer.MAX_VALUE;
        }
    }

    private int minCost = Integer.MAX_VALUE;
    public List<Edge> minCostFromRootToLeaf(Node root) {
        List<Edge> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        List<Edge> list = new ArrayList<>();
        // key -> node, val: minDist to this node
        Map<Node, Integer> distMap = new HashMap<>();
        dfs(root, 0, res, list, distMap);
        return res;
    }

    private void dfs(Node node, int currCost, List<Edge> res, List<Edge> list,
                     Map<Node, Integer> distMap) {
        if (node == null) {
            return;
        }
        if (distMap.containsKey(node) && currCost >= distMap.get(node)) {
            return;
        }
        // If it does not have edges, then it is the leaf.
        if (node.edges.isEmpty()) {
            if (currCost < minCost) {
                minCost = currCost;
                res.clear();
                res.addAll(list);
            }
            return;
        }
        for (Edge edge : node.edges) {
            int nextCost = currCost + edge.cost;
            if (nextCost > minCost) {
                continue;
            }
            list.add(edge);
            dfs(edge.node, currCost + edge.cost, res, list, distMap);
            list.remove(list.size() - 1);
        }
    }

    // Dijkstra’s Algorithm.
    // If it needs path, store the node -> edge mapping:
    public List<Edge> minCostFromRootToLeaf(Node root) {
        List<Edge> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        root.dist = 0;
        Queue<Node> pq = new PriorityQueue<>(Comparator.comparingInt(n -> n.dist));
        pq.offer(root);

         // Only if the shortest path is needed.
        Map<Node, Node> parNodeMap = new HashMap<>();

        int minCost = Integer.MAX_VALUE;
        Node minCostLeaf = null;
        while (!pq.isEmpty()) {
            Node curr = pq.poll();
            if (curr.edges.isEmpty()) {
                if (curr.dist < minCost) {
                    minCost = curr.dist;
                    minCostLeaf = curr;
                }
                continue;
            }
            for (Edge edge : curr.edges) {
                int weight = curr.dist + edge.cost;
                if (weight < edge.node.dist) {
                    edge.node.dist = weight;
                    pq.offer(edge.node);

                    // Only if the shortest path is needed.
                    parNodeMap.put(edge.node, curr);
                }
            }
        }

        System.out.println(minCostLeaf.dist);

        // If it requires shortest cost path:
        Queue<Node> queue = new LinkedList<>();
        queue.offer(minCostLeaf);
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            if (!parNodeMap.containsKey(node)) {
                continue;
            }
            Node par = parNodeMap.get(node);
            for (Edge edge : par.edges) {
                if (!edge.node.equals(node)) {
                    continue;
                }
                res.add(0, edge);
            }
            queue.offer(par);
        }
        return res;
    }

/**
    lc139的变种，“Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, determine if s can be segmented into a space-separated sequence of one or more dictionary words. You may assume the dictionary does not contain duplicate words.” 
*/
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> res = new ArrayList<>();
        int n = s.length();
        boolean[] dp = new boolean[n+1];
        dp[0] = true;

        Map<Integer, Integer> parMap = new HashMap<>();
        for (int i = 0; i < n; i++) {
            if (!dp[i]) {
                continue;
            }
            for (String word : wordDict) {
                int end = i + word.length();
                if (end > n || dp[end]) {
                    continue;
                }
                String substr = s.substring(i, end);
                if (substr.equals(word)) {
                    dp[end] = true;
                }
                parMap.put(end, i);
            }
        }
        if (!dp[n]) {
            return res;
        }
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(n);
        while (!queue.isEmpty()) {
            int curr = queue.poll();
            if (!parMap.containsKey(curr)) {
                continue;
            }
            int par = parMap.get(curr);
            String substr = s.substring(par, curr);
            res.add(0, substr);
            queue.offer(par);
        }
        return res;
    }

/**
VersionedResume: 
实现int update(String fieldName, String value)和 Map<String, String> get(int version)两个方法。

我最后用的是<FieldName, List<VersionedValue>>做的，VersionedValue是一个自定义的class，里面有两个field: version, value。一开始用TreeMap，后来小姐姐说有没有更快的办法，就想到这个，因为version本来就是排好序的，所以直接加在list后面就好了，如果用TreeMap，加新的进入还要O(logn)，只是用List的话，在call get(int version)的时候需要再写一个binarySearch，而不像TreeMap可以直接用API

Update(String profileID, String keyField, String valueField)
Get(String profileID, int version)
Get(String profileID, String keyField, int version)

举个例子，比如我现在有一份简历，最开始的版本号是1,假设profileID叫做ABC
update(ABC, "skills", "java")------>对应版本1
update(ABC, "skills", "python")------>对应版本2，因为同样的key里面增加了新的元素

update(ABC, "education", "USC")------>对应版本2，因为education这个key是新key
这个时候，如果我们call Get(ABC, 版本1)，那么应该返回{ABC:{"skills" : "java"}}

如果我们call Get(ABC, 版本2)，那么应该返回{ABC:{"skills" : "java, python", "education": "USC"}}

规律是，如果已经现有的key被更新，那么就生成最新版本，版本号+1，如果是新加进来的key，那么版本号就不变
*/
    class Profile {
        String id;
        int version;
        Map<Integer, Map<String, String>> versionToMap;
        public Profile(String id) {
            this.id = id;
            this.version = 1;
            versionToMap = new HashMap<>();
            versionToMap.put(1, new HashMap<>());
        }
    }

    Map<String, Profile> profiles;
    public ProfileSystem() {
        profiles = new HashMap<>();
    }

    public void update(String profileId, String field, String value) {
        if (!profiles.containsKey(profileId)) {
            Profile profile = new Profile(profileId);
            profile.versionToMap.get(profile.version).put(field, value);
            profiles.put(profileId, profile);
        } else {
            Profile profile = profiles.get(profileId);
            Map<String, String> fields = profile.versionToMap.get(profile.version);
            if (fields.containsKey(field)) {
                profile.versionToMap.put(profile.version + 1, new HashMap<>(fields));
                profile.version++;
                profile.versionToMap.get(profile.version).put(field, fields.get(field) + ", " + value);
            } else {
                fields.put(field, value);
            }
        }
    }

    public String get(String profileId, int version) {
        if (!profiles.containsKey(profileId))
            return null;
        else {
            Profile profile = profiles.get(profileId);
            Map<String, String> fields = profile.versionToMap.get(version);
            StringBuilder sb = new StringBuilder();
            sb.append("{\"" + profileId + "\": ");
            for (String field : fields.keySet()) {
                sb.append("\"" + field + "\": " + "\"" + fields.get(field) + "\",");
            }
            sb.append("}");
            return sb.toString();
        }
    }

    public String getField(String profileId, int version, String field) {
        if (!profileId.contains(profileId))
            return null;
        else {
            Profile profile = profiles.get(profileId);
            if (!profile.versionToMap.containsKey(version))
                return null;
            else {
                Map<String, String> fields = profile.versionToMap.get(version);
                if (fields.containsKey(field)) {
                    return fields.get(field);
                } else {
                    return null;
                }
            }
        }
    }

/**
LC 563: Binary Tree Tilt
https://leetcode.com/problems/binary-tree-tilt
*/

    private int tilt = 0;    
    public int findTilt(TreeNode root) {
        if (root == null) {
            return 0;
        }
        findTiltHelper(root);
        return tilt;
    }
    
    private int findTiltHelper(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftTreeSum = findTiltHelper(root.left);
        int rightTreeSum = findTiltHelper(root.right);
        tilt += Math.abs(leftTreeSum - rightTreeSum);
        return leftTreeSum + root.val + rightTreeSum;
    }

/**
给你一个树，让你设计一个数据结构来保存树的结构，用array就好了，follow up就是如果树不full怎么办

存树。用什么办法可以节省空间，如果比较full的tree，用heap的实现方式。比较sparse的tree就用tree本身。介于中间的可以用两个数组，一个表示value，一个表示这个节点在第一种表示方式下的index。

也就是说用两个array, 一个存值，一个存 值对应的index. 
*/

/*
4. 这个环节有点奇怪，不过是by design的。面试官交代清楚了就离开了，打开一个网页网上做题。屋子里就我一人，随便google你需要的东西。given 1. some docs, each contains a list of words; 2. some search strings, each also contains a list of words (call it keyword). Question: for each search keyword, return the top 10 docs that contains the most keywords。
*/
