/**
Binary Tree, min cost from root to leaf:
*/
    // Time Complexity: O(V+E), where V is the num of nodes, E is the num of edges
    class TreeNode {
        TreeNode left;
        TreeNode right;
        int val;
        public TreeNode(int val) {
            this.val = val;
        }
    }

    private int minCost = Integer.MAX_VALUE;
    public List<Integer> minCostPathFromRootToLeaf(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        List<Integer> currList = new ArrayList<>();
        currList.add(root.val);
        dfs(root, root.val, currList, res);
        return res;
    }

    private void dfs(TreeNode node, int cost, List<Integer> currList, List<Integer> res) {
        if (node.left == null && node.right == null) {
            if (cost < minCost) {
                cost = minCost;
                res.clear();
                res.addAll(currList);
            }
            return;
        }
        if (node.left != null) {
            TreeNode leftNode = node.left;
            currList.add(leftNode.val);
            dfs(node.left, cost + leftNode.val, currList, res);
            currList.remove(currList.size()-1);
        }
        if (node.right != null) {
            TreeNode rightNode = node.right;
            currList.add(rightNode.val);
            dfs(node.right, cost + rightNode.val, currList, res);
            currList.remove(currList.size()-1);
        }
    }

/**
N-array Tree: min cost from root to leaf:

面试官默认是所有cost都是正数，然后也没有环。所以N个点，每个都能连N-1，继续连N-2，所以复杂度是N！
*/
    // Time Complexity: O(V+E), where V is the num of nodes, E is the num of edges
    class NArrTreeNode {
        int val;
        NArrTreeNode[] children;

        public NArrayTreeNode(int val) {
            this.val = val;
        }
    }
    private int minCost = Integer.MAX_VALUE;
    public List<Integer> minCostPathFromRootToLeaf(NArrTreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        List<Integer> currList = new ArrayList<>();
        currList.add(root.val);
        return res;
    }

    private void dfs(NArrTreeNode node, int cost, List<Integer> currList, List<Integer> res) {
        if (node.children == null || node.children.length == 0) {
            if (cost < minCost) {
                minCost = cost;
                res.clear();
                res.addAll(currList);
            }
            return;
        }
        for (NArrTreeNode child : node.children) {
            currList.add(child.val);
            dfs(child, cost+child.val, currList, res);
            currList.remove(currList.size()-1);
        }
    }

    // Follow up: Assume the tree is a DAG, directed acyclic graph, where leaf is the node with in-degree 0, find the min cost from the root to leaf.
    // Dijkstras Algorithm:
    // Time Complexity: O(E * log(E)) + V
    class Node {
        NArrTreeNode node;
        int dist;

        public Node(NArrTreeNode node) {
            this.node = node;
            this.dist = Integer.MAX_VALUE;
        }
        public Node(NArrTreeNode node, int dist) {
            this.node = node;
            this.dist = dist;
        }
    }

    private List<Integer> minCostFromRootToLeaf(NArrTreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Map<NArrTreeNode, Boolean> visitedMap = new HashMap<>();
        Map<NArrTreeNode, Integer> distanceMap = new HashMap<>();
        Map<NArrTreeNode, NArrTreeNode> prevNodeMap = new HashMap<>();

        List<NArrTreeNode> leafNodes = new ArrayList<>();
        prevNodeMap.put(root, null);

        // minQueue, min Distance to root appears first
        Queue<Node> pq = new PriorityQueue<>(Comparator.comparingInt(n -> n.dist));
        Node node = new Node(root);
        node.dist = 0;
        pq.offer(node);
        while (!pq.isEmpty()) {
            Node curr = pq.poll();
            NArrTreeNode currTreeNode = curr.node;
            if (visitedMap.containsKey(currTreeNode) && visitedMap.get(currTreeNode)) {
                continue;
            }
            else {
                visitedMap.put(currTreeNode, true);
            }
            // we meet the leaf:
            if (currTreeNode.children == null || currTreeNode.children.length == 0) {
                leafNodes.add(currTreeNode);
                continue;
            }
            for (NArrTreeNode child : currTreeNode.children) {
                int weight = curr.dist + child.val;
                // if this node does not in the map, assume the distance is infinity:
                if (!distanceMap.containsKey(child) || weight < distanceMap.get(child)) {
                    distanceMap.put(child, weight);
                    Node neighborNode = new Node(child, weight);
                    pq.offer(neighborNode);
                    prevNodeMap.put(child, currTreeNode);
                }
            }
        }

        // find the min weight from root:
        NArrTreeNode minCostLeaf = null;
        int minCost = Integer.MAX_VALUE;
        for (NArrTreeNode leaf : leafNodes) {
            int dist = distanceMap.get(leaf);
            if (dist < minCost) {
                minCost = dist;
                minCostLeaf = leaf;
            }
        }
        res.add(minCostLeaf.val);
        while (true) {
            NArrTreeNode prevNode = prevNodeMap.get(minCostLeaf);
            if (prevNode == null) {
                break;
            }
            res.add(0, prevNode.val);
            minCostLeaf = prevNode;
        }

        return res;
    }