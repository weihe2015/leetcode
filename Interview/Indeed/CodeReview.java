/**
basic questions: 
1. Am I able to understand the code easily?
2. Is the code written following the coding standards/guidelines?
3. Is the same code duplicated more than twice?
4. Can I unit test / debug the code easily to find the root cause?
5. Is this function or class too big? If yes, is the function or class having too many responsibilities?
*/

/**
直接给你看代码，让你像平常工作一样写code review撒，哪段代码不行，为啥要改，怎么改

Code review，我觉得我这一面还不错，改了一些bug和syntax

code review interview可以上网查，有些细节拿不准的可以Google一下

你看一段code然后需要你在gitlab里写comments

就是要模拟你平时如何做code review，如何跟别人交流你的idea，为什么这个重要值得改。

我能想到的是codingstye， variable naming，参数判空。功能上的？

至少我的不隐蔽，文件有两个好像，不多，第二个是前端，我说我不太熟那个framework，他说没关系那就不用太花时间在前端了
。我记得我主要回答的都是你说的那些，还有些很通用的错误，比如版权，异常检查，除以零，函数太大要breakdown之类的。
给你的代码功能我记得挺简单的，也忘了有没有功能上的bug需要提出，即便有应该也不会很刁钻那种。我觉得就当平时code review就好。

code review，就是给一点代码，前后端都有点，对着代码风格，语法错误，逻辑问题，效率问题，开始挑刺，一边写下comment，用的是英语，前端我不够强，写的不多，总体这轮自我感觉良好，但并没有突出表现。

改了一些bug和syntax

1. 看变量名
2. 看结构
3. 看会不会对其他地方造成影响
4. 最好不要在不想关的地方加arguments

[request]xxxxxxx　　　　　　　此条评论的代码必须修改才能予以通过
[advise]xxxxxxxx　　　　　　　此条评论的代码建议修改，但不修改也可以通过
[question]xxxxxx　　　　　　　此条评论的代码有疑问，需reviewee进一步解释

讲明该评论的原因。在对代码做出评论时，应当解释清楚原因，如果自己有现成的更好地解决思路，应该把相应的解决思路也评论上，节省reviewee的修改时间。

What to Look for in a code review:
    * Functionality
        - Does this CL do what the developer intended? 
    * Complexity:
        - Is the CL more complex than it should be? 
    * Naming
        * Camel-Case:
        * A good name is long enough to fully communicate what the item is or does, without being so long that it becomes hard to read.
    * Comment:
        * Usually comments are useful when they explain why some code exists, and should not be explaining what some code is doing. 
        * Note that comments are different from documentation of classes, modules, or functions, which should instead express the purpose of a piece of code, how it should be used, and how it behaves when used.
    * Style:
        * Make sure the CL follows the appropriate style guides
        * If you want to improve some style point that isn’t in the style guide, prefix your comment with “Nit:” to let the developer know that it’s a nitpick that you think would improve the code but isn’t mandatory. 

Courtesy:
    * Good: “The concurrency model here is adding complexity to the system without any actual performance benefit that I can see. Because there’s no performance benefit, it’s best for this code to be single-threaded instead of using multiple threads.”

Explain Why:

Giving Guidance:

Accepting Explanations:
*/

UI feature, seach result, filter, salary, refine search, 

refinement, 

Varient, num

