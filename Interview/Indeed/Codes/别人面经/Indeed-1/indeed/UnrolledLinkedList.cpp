#include<iostream>
#include<unordered_map>
#define MAX_LEN 5

using namespace std;

struct Node {
    int len, data[MAX_LEN];
    Node* next;
    Node(Node* n): next(n) {len=0;}
};

class UnrolledList {
    private:
        Node* head;
    public:
        UnrolledList() {
            Node *n3=new Node(NULL);
            Node *n2=new Node(n3);
            head=new Node(n2);
            head->len=2;
            head->data[0]=1; head->data[1]=2;
            n2->len=1;
            n2->data[0]=2; 
            n3->len=5;
            n3->data[0]=1; n3->data[1]=2;
            n3->data[2]=3; n3->data[3]=4;
            n3->data[4]=5; 
        }
        int get(int index) {
            Node* now=head;
            while(now && index>=now->len) {
                index-=now->len;
                now=now->next;
            }
            if(now) return now->data[index];
            else return -1;
        }
        void insert(int index, int num) {
            Node* now=head, *pre;
            while(now && index>=now->len) {
                index-=now->len;
                pre=now;
                now=now->next;
            }
            if(now && now->len<MAX_LEN) {
                for(int i=now->len;i>index;i--) now->data[i]=now->data[i-1];
                now->data[index]=num;
                now->len++;
            } else {
                if(now) {
                    int tmp=now->data[MAX_LEN-1];
                    for(int i=MAX_LEN-1;i>index;i--) now->data[i]=now->data[i-1];
                    now->data[index]=num;
                    num=tmp;
                    pre=now;
                } 
                pre->next=new Node(pre->next);
                now=pre->next;
                now->len=1;
                now->data[0]=num;
            }
        }
};

void print(int n, UnrolledList &l) {
    for(int i=0;i<n;i++) {
        cout << l.get(i) << ' ';
    }
    cout << endl;
}

int main() {
    UnrolledList l;
    print(8,l);
    l.insert(1,4);
    print(9,l);
    l.insert(0,5);
    print(10,l);
    l.insert(4,6);
    print(11,l);
    l.insert(7,7);
    print(12,l);
    l.insert(2,8);
    print(13,l);
    l.insert(3,9);
    print(14,l);
    return 0;
}
