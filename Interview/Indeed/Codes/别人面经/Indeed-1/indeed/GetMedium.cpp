#include<iostream>
#include<vector>
#include<queue>

using namespace std;

class MedianVector {
    private:
        vector<int> arr;
        priority_queue<int> min_heap;
        priority_queue<int, vector<int>, greater<int>> max_heap;
        int quick_select(int l, int r, int k) {     // select kth smallest
            int pivot=arr[r];
            int i=l, j=r;
            while(i<j) {
                if(arr[i]>=pivot) swap(arr[i], arr[--j]);
                else i++;
            }
            swap(arr[i], arr[r]);
            if(i-l+1==k) return arr[i];
            if(i-l+1<k) return quick_select(i+1, r, k-i+l-1);
            else quick_select(l, i-1, k);
        }
    public:
        void addNum(int x) {
            arr.push_back(x);
            int n=arr.size();
            // max_heap: half smallest; min_heap: half largest
            if(n%2!=0) {
                // balance now. add x to min_heap, then pop the smallest to max_heap
                min_heap.push(x);
                max_heap.push(min_heap.top());
                min_heap.pop();
            } else {
                // not balance now. add x to max_heap, then pop the largest to min_heap
                max_heap.push(x);
                min_heap.push(max_heap.top());
                max_heap.pop();
            }
        }
        double getMedian_quickSelect() {
            int n=arr.size();
            if(n%2!=0) return (double)quick_select(0, n-1, n/2+1); // e.g. 5 nums, median=3rd smallest
            else return 0.5*(quick_select(0, n-1, n/2)+quick_select(0, n-1, n/2+1)); // e.g. 6 nums, median*2=3rd+4th
        }
        double getMedian_heap() {
            int n=arr.size();
            if(n%2!=0) return (double)max_heap.top();
            else return 0.5*(min_heap.top()+max_heap.top());
        }
};

int main() {
    MedianVector vec;
    vec.addNum(6);
    vec.addNum(3);
    cout << vec.getMedian_heap() << endl;
    vec.addNum(4);
    vec.addNum(5);
    vec.addNum(7);
    cout << vec.getMedian_heap() << endl;
    vec.addNum(2);
    vec.addNum(3);
    cout << vec.getMedian_heap() << endl;
    vec.addNum(5);
    vec.addNum(6);
    vec.addNum(1);
    // 1 2 3 3 4 5 5 6 6 7
    cout << vec.getMedian_heap() << endl;
    vec.addNum(2);
    // 1 2 2 3 3 4 5 5 6 6 7
    cout << vec.getMedian_heap() << endl;
    return 0;
}
