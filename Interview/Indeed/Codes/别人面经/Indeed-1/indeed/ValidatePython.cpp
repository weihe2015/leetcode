#include<iostream>
#include<string>
#include<vector>
#include<stack>

using namespace std;

class Solution {
    private:
        int cntTab(string &line) {
            int ret=0;
            while(ret<line.size() && line[ret]==' ') ret++;
            if(ret==line.size()) return -1;
            return ret;
        }
    public:
        bool Validate(vector<string> &code) {
            if(code[code.size()-1][code[code.size()-1].size()-1]==':') return false;
            stack<int> lines;
            bool newSection=true;
            for(string line: code) {
                int now=cntTab(line);
                if(newSection) {
                    if(!lines.empty() && now<=lines.top()) {
                        cout << "wrong new section tabs" << endl;
                        return false;
                    }
                    lines.push(now);
                    newSection=false;
                } else if(now>lines.top()) {
                    cout << "more tabs" << endl;
                    return false;
                } else if(now<lines.top()) {
                    while(!lines.empty() && now<lines.top()) lines.pop();
                    if(lines.empty() || now!=lines.top()) {
                        cout << "wrong section tab" << endl;
                        return false;
                    }
                }
                if(line[line.size()-1]==':') {
                    newSection=true;
                }
            }
            return true;
        }
};

int main() {
    vector<string> code={
        "aaa:",
        "   bb",
        "   ccc:",
        "   ddd",
        "       dd",
        "   eeeee",
        "fff"
    };
    Solution sol;
    cout << sol.Validate(code) << endl;
    return 0;
}
