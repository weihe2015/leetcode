#include<iostream>
#include<vector>
#include<queue>

using namespace std;

class Stream {
    private:
        vector<int>::iterator iter, rear;
    public:
        Stream(vector<int>& stream): iter(stream.begin()), rear(stream.end()) {}
        bool move() {
            return !(iter==rear);
        }
        int getVal() {
            int ret=*iter;
            iter++;
            return ret;
        }
};

class Solution {
    private:
        priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;
    public:
        vector<int> getKnum(vector<Stream> &str, int k) {
            for(int i=0;i<str.size();i++) {
                if(str[i].move()) q.push(make_pair(str[i].getVal(), i));
            }
            int cnt=0, num=-1;
            vector<int> ret;
            while(!q.empty()) {
                auto now=q.top();
                q.pop();
                if(now.first!=num) {
                    if(cnt>=k) ret.push_back(num);
                    cnt=1;
                    num=now.first;
                } else {
                    cnt++;
                }
                int val=-1;
                while(str[now.second].move()) {
                    val=str[now.second].getVal();
                    if(val!=num) break;
                }
                if(val!=-1) q.push(make_pair(val, now.second));
            }
            if(cnt>=k) ret.push_back(num);
            return ret;
        }
};

int main() {
    Solution sol;
    vector<Stream> str;
    vector<int> a={1,2,3,4,7};
    vector<int> b={2,5,6};
    vector<int> c={2,2,5,7};
    str.push_back(Stream(a));
    str.push_back(Stream(b));
    str.push_back(Stream(c));
    vector<int> ret=sol.getKnum(str, 2);
    for(auto n: ret) cout << n << endl;
    return 0;
}

