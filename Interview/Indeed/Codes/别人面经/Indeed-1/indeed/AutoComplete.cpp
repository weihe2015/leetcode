#include<iostream>
#include<string>
#include<queue>
#include<cstdio>

using namespace std;

class Solution {
    private:
        struct TrieNode {
            TrieNode* child[26];
            bool isEnd;
            string str;
            TrieNode() {
                for(int i=0;i<26;i++) child[i]=NULL;
                isEnd=false;
                str="";
            }
        } *root;
        vector<string> ret;
        void dfs(TrieNode* now) {
            if(now->isEnd) {
                ret.push_back(now->str);
            }
            for(int i=0;i<26;i++) {
                if(now->child[i]) dfs(now->child[i]);
            }
        }
    public:
        Solution() {
            root=new TrieNode();
        }
        void addWord(string str) {
            TrieNode *now=root;
            for(auto ch: str) {
                int index=ch-'a';
                if(!now->child[index]) now->child[index]=new TrieNode();
                now=now->child[index];
            }
            now->isEnd=true;
            now->str=str;
        }
        vector<string> find(string prefix) {
            TrieNode *now=root;
            ret.clear();
            for(auto ch: prefix) {
                int index=ch-'a';
                if(!now->child[index]) return ret;
                now=now->child[index];
            }
            dfs(now);
            return ret;
        }
}; 

int main() {
    Solution *sol=new Solution();
    sol->addWord("ab");
    sol->addWord("a");
    sol->addWord("de");
    sol->addWord("abde");
    sol->addWord("ade");
    vector<string> dict=sol->find("ab");
    for(auto str: dict) cout << str << endl;
    return 0;
}
