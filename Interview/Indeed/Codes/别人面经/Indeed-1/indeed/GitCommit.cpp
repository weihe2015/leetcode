#include<iostream>
#include<queue>
#include<vector>
#include<unordered_set>

using namespace std;

struct Commit {
    vector<Commit*> parents;
    int id;
    Commit(int n): id(n) {}
}; 

class Solution {
    public:
        Commit* LCA(Commit *a, Commit *b) {
            unordered_set<Commit*> vis_a, vis_b;
            queue<Commit*> q_a, q_b;
            q_a.push(a);
            q_b.push(b);
            vis_a.insert(a);
            vis_b.insert(b);
            while(!q_a.empty() && !q_b.empty()) {
                Commit *now=q_a.front();
                q_a.pop();
                for(auto par: now->parents) {
                    if(vis_b.find(par)!=vis_b.end()) return par;
                    if(vis_a.find(par)==vis_a.end()) {
                        vis_a.insert(par);
                        q_a.push(par);
                    }
                }
                now=q_b.front();
                q_b.pop();
                for(auto par: now->parents) {
                    if(vis_a.find(par)!=vis_a.end()) return par;
                    if(vis_b.find(par)==vis_b.end()) {
                        vis_b.insert(par);
                        q_b.push(par);
                    }
                }
            }
            return NULL;
        }
};

int main() {
    Solution sol;
    Commit *c1 = new Commit(1);
    Commit *c2 = new Commit(2);
    Commit *c3 = new Commit(3);
    Commit *c4 = new Commit(4);
    Commit *c5 = new Commit(5);

    c1->parents.push_back(c3);
    c1->parents.push_back(c4);
    c2->parents.push_back(c4);
    c3->parents.push_back(c5);
    c4->parents.push_back(c5);

    Commit *ret = sol.LCA(c4, c1);
    cout << ret->id << endl;
    return 0;
}
