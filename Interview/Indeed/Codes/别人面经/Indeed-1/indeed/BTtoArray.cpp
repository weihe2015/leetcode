#include<iostream>
#include<unordered_map>

using namespace std;

struct Node {
    Node *left, *right;
    int val;
    Node(int v): val(v), left(NULL), right(NULL) {}
}; 

class Solution {
    private:
        unordered_map<int, int> tree;
        void traverse(Node* now, int index) {
            if(!now) return;
            tree[index]=now->val;
            traverse(now->left, index*2);
            traverse(now->right, index*2+1);
        }
        int print(int index) {
            if(tree.find(index)==tree.end()) return -1;
            int left=print(index*2);
            int right=print(index*2+1);
            cout << "Node " << tree[index] << ": Left=" << left << " Right=" << right <<endl;
            return tree[index];
        }
    public:
        void BTtoArray(Node* root) {
            traverse(root, 1);
        }
        void PrintTree() {
            print(1);
        }
};

int main() {
    Node* root=new Node(5);
    root->left=new Node(3);
    root->right=new Node(8);
    root->left->right=new Node(4);
    root->right->left=new Node(6);
    root->right->right=new Node(9);
    Solution *sol=new Solution();
    sol->BTtoArray(root);
    sol->PrintTree();
    return 0;
}
