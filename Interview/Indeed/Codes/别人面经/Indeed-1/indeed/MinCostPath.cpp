#include<iostream>
#include<vector>

using namespace std;

struct Edge {
    int v, len;
    Edge(int a, int b): v(a), len(b) {}
};

class Solution {
    private:
        int V;
        vector<vector<Edge>> Edges;
    public:
        Solution(int n) {
            V=n;
            Edges.resize(n);
        }
        void addEdge(int from, int to, int len) {
            Edges[from].push_back(Edge(to, len));
        }
        int getMinPath(int src) {
            if(Edges[src].size()==0) return 0;
            int ret=(1<<30);
            for(auto edge: Edges[src]) {
                ret=min(ret, edge.len+getMinPath(edge.v)); 
            }
            return ret;
        }
};

int main() {
    Solution sol(5);
    sol.addEdge(0, 1, 2);
    sol.addEdge(0, 2, 10);
    sol.addEdge(1, 2, 3);
    sol.addEdge(1, 3, 1);
    sol.addEdge(1, 4, 7);
    sol.addEdge(3, 4, 3);
    cout << sol.getMinPath(0) << endl;
    return 0;
}
