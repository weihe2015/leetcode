#include<iostream>
#include<vector>

using namespace std;

class Solution {
    private:
        int search(int n, int k, vector<vector<long long>> &mem) {
            if(k<=0) return 0;
            if(n==1) {
                if(k>=1 && k<=6) return mem[n][k]=1;
                else return mem[n][k]=0;
            }
            if(mem[n][k]>=0) return mem[n][k];
            mem[n][k]=0;
            for(int i=1;i<=6;i++) {
                mem[n][k]+=search(n-1,k-i, mem);
            }
            return mem[n][k];
        }
    public:
        double getPossibility(int dice, int target) {
            long long total=1;
            for(int i=0;i<dice;i++) total*=6;
            vector<vector<long long>> mem(dice+1, vector<long long>(target+1, -1));
            search(dice, target, mem);
            return (double)mem[dice][target]/total;
        }
};

int main() {
    Solution sol;
    cout << sol.getPossibility(6, 20) << endl;
    cout << sol.getPossibility(6, 30) << endl;
    cout << sol.getPossibility(6, 36) << endl;
    return 0;
}
