#include <iostream>
#include <cstdio>
#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

struct Word;

struct User {
    vector<Word*> wordlist;
};

struct Word {
    string str;
    unordered_map<Word*, int> scores;
    Word(string& s): str(s) {}
};

class database {
private:
    int n;
    unordered_map<string, User*> uid_map;
    unordered_map<string, Word*> word_map;
    User* getorcreateUser(string &uid) {
        if(uid_map.find(uid)==uid_map.end()) {
            User* newUser=new User;
            uid_map[uid]=newUser;
            return newUser;
        } else return uid_map[uid];
    }
    Word* getorcreateWord(string &word) {
        if(word_map.find(word)==word_map.end()) {
            Word* newWord=new Word(word);
            word_map[word]=newWord;
            return newWord;
        } else return word_map[word];
    }

public:
    database(int cnt): n(cnt) {}
    void insert(string &uid, string &word) {
        User* u=getorcreateUser(uid);
        Word* w=getorcreateWord(word);
        u->wordlist.push_back(w);
        for(Word* candidate: u->wordlist) {
            if(candidate!=w) {
                w->scores[candidate]++;
                candidate->scores[w]++;
            }
        }
    }
    int query(string &word, vector<string> &ret) {
        Word* w=getorcreateWord(word);
        //cout << "Search " << word << ". has " << w->userlist.size() << "users searched." << endl;
        int maxscore=0;
        for(auto& i: w->scores) {
            if(i.second>maxscore) {
                maxscore=i.second;
                ret.clear();
                ret.push_back(i.first->str);
            } else if(i.second==maxscore) ret.push_back(i.first->str);
        }
        sort(ret.begin(), ret.end());
        return maxscore;
    }
};

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n;  // num of queries
    cin >> n;
    database db(n);
    //vector<User*> users;
    for(int i=0;i<n;i++) {
        string uid, str;
        cin >> uid >> str;
        vector<string> ret;
        int score=db.query(str, ret);
        cout << score;
        for(string& s: ret) cout << ' ' << s;
        cout << endl;
        db.insert(uid, str); 
    }
    return 0;
}
