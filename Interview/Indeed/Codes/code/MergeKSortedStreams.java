package sorting;
import java.util.*;

public class MergeKSortedStreams {

    /*
    follow up是一个stream特别长，其他都很短，怎么处理。

    可以提前结束？min heap的size比k小的时候提前结束。
     */
    public List<Integer> getNumsFromKStreams(List<Stream> streams, int k) {
        List<Integer> res = new ArrayList<>();
        if (streams == null || streams.size() < k)
            return res;

        PriorityQueue<Data> pq = new PriorityQueue<>(new Comparator<Data>() {
            @Override
            public int compare(Data o1, Data o2) {
                return o1.val - o2.val;
            }
        });

        for (Stream s : streams) {
            if (s.move())
                pq.add(new Data(s));
        }

        if (pq.size() < k)
            return res;

        int prevVal = -1, curVal = -1, count = -1;
        while (!pq.isEmpty()) {
            Data cur = pq.remove();
            curVal = cur.val;
            if (count == -1 || curVal != prevVal) {
                prevVal = curVal;
                count = 1;
            } else {
                count++;
            }

            while (cur.stream.move()) {
                int now = cur.stream.getValue();
                if (now != curVal) {
                    cur.val = now;
                    pq.add(cur);
                    break;
                }
            }

            if (count >= k)
                res.add(curVal);

            // exit in advance
            if (pq.size() < k)
                break;
        }
        return res;

    }
}

class Data {
    int val;
    Stream stream;
    public Data(Stream s) {
        this.stream = s;
        this.val = s.getValue();
    }
}
class Stream {

    Iterator<Integer> iter;

    public boolean move() {
        return iter.hasNext();
    }

    public int getValue() {
        return iter.next();
    }
}
