package sorting;

public class DiceSum {

    public double getProb(int dice, int target) {
        if (dice < target)
            return 0;

        int total = (int) Math.pow(6, dice);
        int[] count = new int[1];
        helper(dice, target, count);
        return count[0] * 1.0 / total;
    }

    private void helper(int dice, int target, int[] count) {
        if (dice == 0 && target == 0) {
            count[0]++;
            return;
        }

        if (dice <= 0 || target <= 0)
            return;

        for (int i = 1; i <= 6; i++) {
            helper(dice - 1, target - i, count);
        }
    }

    // time complexity: O(6 * dice * target); # states: dice * target; # state transformation cost: 6
    public double getProbMemorized(int dice, int target) {
        if (dice < target)
            return 0;

        int total = (int) (Math.pow(6, dice));
        int[][] memo = new int[dice+1][target+1];
        return dfs(dice, target, memo) * 1.0 / total;
    }

    private int dfs(int dice, int target, int[][] memo) {
        if (dice == 0 && target == 0)
            return 1;

        if (dice <= 0 || target <= 0)
            return 0;

        if (target < dice || target > dice * 6)
            return 0;

        if (memo[dice][target] != 0)
            return memo[dice][target];

        int count = 0;
        for (int i = 1; i <= 6; i++) {
            count += dfs(dice - 1, target - i, memo);
        }
        memo[dice][target] = count;
        return count;

    }

}
