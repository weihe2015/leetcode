package sorting;

public class ShortestWordDistance {

    public int shortestWordDistanceIII(String[] words, String word1, String word2) {
        int i1 = -1, i2 = -1;
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < words.length; i++) {
            if (!word1.equals(word2)) {
                if (words[i].equals(word1))
                    i1 = i;
                else if (words[i].equals(word2))
                    i2 = i;

                if (i1 != -1 && i2 != -1 && i1 != i2) {
                    res = Math.min(res, Math.abs(i1 - i2));
                }
            } else {
                if (words[i].equals(word1)) {
                    if (i1 == -1) {
                        i1 = i;
                    } else {
                        res = Math.min(res, i - i1);
                        i1 = i;
                    }
                }
            }
        }
        return res;
    }
}
