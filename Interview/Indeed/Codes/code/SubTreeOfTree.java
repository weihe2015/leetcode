package sorting;

import java.util.*;

public class SubTreeOfTree {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }
    public boolean isSubtree(TreeNode s, TreeNode t) {
    if (s == null && t != null)
        return false;

    if (isSame(s, t))
        return true;
    return isSubtree(s.left, t) || isSubtree(s.right, t);
}

    private boolean isSame(TreeNode s, TreeNode t) {
        if (s == null && t == null)
            return true;
        if (s == null || t == null || s.val != t.val)
            return false;

        return isSame(s.left, t.left) && isSame(s.right, t.right);
    }

    public static void main(String[] args) {
        List<TreeNode> list = new ArrayList<>();
        list.add(null);
        list.add(null);
        System.out.println(list.size());
        System.out.println(list);
    }
}
