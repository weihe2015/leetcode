package sorting;

import java.util.*;

public class DivideList {

    public static List<List<Character>> divideList(List<Character> chars) {
        Map<Character, Integer> count = new HashMap<>();
        int maxCount = 0;
        char maxChar = ' ';
        for (char c : chars) {
            count.put(c, count.containsKey(c) ? count.get(c) + 1 : 1);
            if (count.get(c) > maxCount) {
                maxCount = count.get(c);
                maxChar = c;
            }
        }

        List<List<Character>> res = new ArrayList<>();

        for (Map.Entry<Character, Integer> each : count.entrySet()) {
            char key = each.getKey();
            int value = each.getValue();
            for (int i = 0; i < value; i++) {
                if (res.size() <= i)
                    res.add(new ArrayList<>());
                res.get(i).add(key);
            }
        }
        return res;
    }

    /*
    给一个list， 如何把里面的字符分配到尽量少的子list里，并且每个子list没有重复元素。
比如
['a','b','c','a','a','b']， 可以分成['a', 'b', 'c'], ['a', 'b'], ['a']
['a', 'a', 'a', 'b', 'b', 'b']，可以分成['a', 'b'], ['a', 'b'], ['a', 'b']
     */
    public static void main(String[] args) {
        List<Character> input = Arrays.asList('a', 'a', 'a', 'b', 'b', 'b');
        System.out.println(divideList(input));
    }
}
