import numpy as np
from math import sqrt

targets=[3,21,-1.25,13]
predictions=[4,25,0.75,11]

n = len(predictions)

rmse = np.linalg.norm(predictions - targets) / np.sqrt(n)

print(rmse)