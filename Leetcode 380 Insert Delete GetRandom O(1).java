class RandomizedSet {
    class Item {
        int idx;
        // It can be easily extend to HashMap by adding new field
        // V value
        public Item(int idx) {
            this.idx = idx;
        }
    }
    private static final Random rand = new Random();

    // List of values:
    private List<Integer> keys;
    // key -> insert val, val -> idx
    private Map<Integer, Item> map;
    
    /** Initialize your data structure here. */
    public RandomizedSet() {
        this.keys = new ArrayList<>();
        this.map = new HashMap<>();
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int key) {
        if (map.containsKey(key)) {
            return false;
        }
        int idx = keys.size();
        map.put(key, new Item(idx));
        keys.add(key);
        return true;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int key) {
        if (!map.containsKey(key)) {
            return false;
        }
        // swap this key with the last key in keys List:
        /**
        There are two cases here:
        1. If the item to be removed is the last item, remove it from the list;
        2. If the item to be removed is not the last item, swap it with the last item on the list:
        */ 
        int lastKeyIdx = keys.size()-1;
        int oldKeyIdx = map.get(key).idx;

        if (oldKeyIdx != lastKeyIdx) {
            int lastKey = keys.get(lastKeyIdx);
            map.get(lastKey).idx = oldKeyIdx;
            keys.set(oldKeyIdx, lastKey);
        }
        map.remove(key);
        keys.remove(lastKeyIdx);
        
        return true;
    }
    
    /** Get a random element from the set. */
    public int getRandom() {
        int n = keys.size();
        int randomIdx = rand.nextInt(n);
        return keys.get(randomIdx);
    }
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */

 class RandomizedSet {

    class Item {
        int idx;
        public Item(int idx) {
            this.idx = idx;
        }
    }
    
    // List of values;
    private List<Integer> values;
    private Map<Integer, Item> map;
    
    static private Random rand = new Random();
    /** Initialize your data structure here. */
    public RandomizedSet() {
        this.values = new ArrayList<>();
        this.map = new HashMap<>();
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        if (map.containsKey(val)) {
            return false;
        }
        int idx = values.size();
        map.put(val, new Item(idx));
        values.add(val);
        return true;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        if (!map.containsKey(val)) {
            return false;
        }
        int lastValIdx = values.size()-1;
        int oldValIdx = map.get(val).idx;
        if (lastValIdx != oldValIdx) {
            // swap these two item:
            int lastKey = values.get(lastValIdx);
            map.get(lastKey).idx = oldValIdx;
            values.set(oldValIdx, lastKey);
        }
        map.remove(val);
        values.remove(lastValIdx);
        return true;
    }
    
    /** Get a random element from the set. */
    public int getRandom() {
        int n = values.size();
        int randomIdx = rand.nextInt(n);
        return values.get(randomIdx);
    }
}