/**
A citys skyline is the outer contour of the silhouette formed by all the buildings in that city when viewed from a distance.
Now suppose you are given the locations and height of all the buildings as shown on a cityscape photo (Figure A),
write a program to output the skyline formed by these buildings collectively (Figure B).

 Buildings  Skyline Contour
The geometric information of each building is represented by a triplet of integers [Li, Ri, Hi],
where Li and Ri are the x coordinates of the left and right edge of the ith building, respectively, and Hi is its height.
It is guaranteed that 0 ≤ Li, Ri ≤ INT_MAX, 0 < Hi ≤ INT_MAX, and Ri - Li > 0.
You may assume all buildings are perfect rectangles grounded on an absolutely flat surface at height 0.

For instance, the dimensions of all buildings in Figure A are recorded as: [ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ] .

The output is a list of "key points" (red dots in Figure B) in the format of [ [x1,y1], [x2, y2], [x3, y3], ... ] that uniquely defines a skyline.
A key point is the left endpoint of a horizontal line segment.
Note that the last key point, where the rightmost building ends,
is merely used to mark the termination of the skyline, and always has zero height.
Also, the ground in between any two adjacent buildings should be considered part of the skyline contour.

For instance, the skyline in Figure B should be represented as:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ].

Notes:

The number of buildings in any input list is guaranteed to be in the range [0, 10000].
The input list is already sorted in ascending order by the left x position Li.
The output list must be sorted by the x position.
There must be no consecutive horizontal lines of equal height in the output skyline.
For instance, [...[2 3], [4 5], [7 5], [11 5], [12 7]...] is not acceptable;
the three lines of height 5 should be merged into one in the final output as such: [...[2 3], [4 5], [12 7], ...]
*/
public class Solution {
    /**
    The critical points are on either start of the building or end of the building.
    The height of the building is critical. The algorithm is as following:
    1. We move from left to right encountering the starts and the ends of the building.
    2. Whenever we encounter a start of a building, we push the height into a PriorityQueue.
        2.1 If the max of the priorityQueue changes, it means that this building at this start point must be taller than
        every other building, which is overallapping at that start point, so it needs to be a part of final answer.
    3. Whenever we encounter an end of a building, we need to remove that building from priorityQueue
        If max of the priorityQueue changes, that value needs to be a part of final answer as well.

        Split start point and end point into two different points
        Sort the points by x values.
        
        If two points' x are different: we choose the one with smaller x:
        If two points' x are the same:
           1. If they are all starting points:
                    Ex: (0,3,s) vs (0,1,s), (0,3,s) should be in the front. The higher building should be in the first
           2. If they are all ending points:
                    Ex: (5,2,e) vs (5,4,e), (5,2,e) should be in the front. The lower building should be in the first
           3. Else: If one point is starting point and the other point is ending point:
                    Ex: (7,3,s) vs (7,2,e), (7,3,s) should be in the front
    
        Initial max value is 0.

    3 edge cases:
    (1): [(0,3,s), (0,2,s), (1,2,e), (2,3,e)] starts points of x values are the same, higher y should look at first
    (2): [(3,3,s), (4,2,s), (5,2,e), (5,3,e)] ends points of x values are the same, lower y should look at first
    (3): [(6,2,s), (7,3,s), (7,2,e), (8,3,e)] end point of x overlap with start point of x, start point should look at first
    */
    class Point {
        int x;
        int y;
        boolean isStart;
        public Point(int x, int y, boolean isStart) {
            this.x = x;
            this.y = y;
            this.isStart = isStart;
        }
    }

    /**
     * Priority Version of solution: Running Time Complexity: O(N^2):
     *      PriorityQueue Remove is O(N), because find(obj) is O(N), remove(obj) is O(logN)
     * TreeMap Version of solution: Running Time Complexity: O(logN)
     *      Treemap remove is O(logN)
     * https://www.youtube.com/watch?v=GSBLe8cKu0s
     *
    */

    // Running Time Complexity: O(N^2)
    /**
      * PriorityQueue offer: log(N), remove: O(N)
      * Another solution by using TreeMap: Because PriorityQueue Remove is O(N), which will lead to overall runtime to be O(N^2)
      * By using TreeMap, remove is O(logN), so overall running time complexity is O(NlogN)
      */
    public List<List<Integer>> getSkyline(int[][] buildings) {
        List<List<Integer>> result = new ArrayList<>();
        if (buildings == null || buildings.length == 0) {
            return result;
        }
        List<Point> points = new ArrayList<>();
        for (int[] building : buildings) {
            int x1 = building[0];
            int x2 = building[1];
            int h = building[2];
            points.add(new Point(x1, h, true));
            points.add(new Point(x2, h, false));
        }
        
        Collections.sort(points, (p1, p2) -> {
            if (p1.x != p2.x) {
                return Integer.compare(p1.x, p2.x);
            }
            else {
                if (p1.isStart && p2.isStart) {
                    return Integer.compare(p2.y, p1.y);
                }
                else if (!p1.isStart && !p2.isStart) {
                    return Integer.compare(p1.y, p2.y);
                }
                else {
                    return p1.isStart ? -1 : 1;
                }
            }
        });

        // Use priorityQueue:
        // maxHeap:
        Queue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
        // initial max height is 0:
        int prevMaxHeight = 0;
        pq.offer(prevMaxHeight);
        / Iterate all the points:
        for (Point point : points) {
            // It is the start point, add it to result list and add height to priorityQueue, update the maxHeight:
            if (point.isStart) {
                pq.offer(point.y);
            }
            // If it is the end point, find the value height and remove itself from prorityQueue.
            // If the maxHeight change, add it to the result list.
            // If the maxHeight does not change, it means this point overlapps with other rectangle:
            else {
                pq.remove(point.y);
            }
            // If maxHeight change, add it to the result list
            int currMaxHeight = pq.peek();
            if (prevMaxHeight != currMaxHeight) {
                result.add(Arrays.asList(point.x, currMaxHeight));
                prevMaxHeight = currMaxHeight;
            }
        }

        return result;
    }

    /**
     * Another solution by using TreeMap: Because PriorityQueue Remove is O(N), which will lead to overall runtime to be O(N^2)
     * By using TreeMap, remove is O(logN), so overall running time complexity is O(NlogN)
     */
    // key -> maxHeight, value: frequency
    // Time Complexity: O(NlogN), tree insert: O(logN), tree remove: O(logN)
    public List<List<Integer>> getSkyline(int[][] buildings) {
        List<List<Integer>> result = new ArrayList<>();
        if (buildings == null || buildings.length == 0) {
            return result;
        }
        List<Point> points = new ArrayList<>();
        for (int[] building : buildings) {
            int x1 = building[0];
            int x2 = building[1];
            int y  = building[2];
            points.add(new Point(x1, y, true));
            points.add(new Point(x2, y, false));
        }

        Collections.sort(points, (p1, p2) -> {
            if (p1.x != p2.x) {
                return Integer.compare(p1.x, p2.x);
            }
            else {
                if (p1.isStart && p2.isStart) {
                    return Integer.compare(p2.y, p1.y);
                }
                else if (!p1.isStart && !p2.isStart) {
                    return Integer.compare(p1.y, p2.y);
                }
                else {
                    return p1.isStart ? -1 : 1;
                }
            }
        });

        // key: height, val: frequency
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
        int prevMaxHeight = 0;
        treeMap.put(prevMaxHeight, 1);
        for (Point p : points) {
            if (p.isStart) {
                treeMap.put(p.y, treeMap.getOrDefault(p.y, 0)+1);
            }
            else {
                int freq = treeMap.get(p.y)--;
                if (freq == 0) {
                    treeMap.remove(p.y);
                }
                else {
                    treeMap.put(p.y, freq);
                }
            }
            int currMaxHeight = treeMap.lastKey();
            if (prevMaxHeight != currMaxHeight) {
                result.add(Arrays.asList(p.x, currMaxHeight));
                prevMaxHeight = currMaxHeight;
            }
        }

        return result;
    }

    class Point implements Comparable<Point> {
        int x, y;
        boolean isStart;
        public Point(int x, int y, boolean isStart) {
            this.x = x;
            this.y = y;
            this.isStart = isStart;
        }
        /**
         * If two points' x are different: we choose the one with smaller x:
         * If two points' x are the same:
         *     1. If they are all starting points:
         *              Ex: (0,3,s) vs (0,1,s), (0,3,s) should be in the front. The higher building should be in the first
         *     2. If they are all ending points:
         *              Ex: (5,2,e) vs (5,4,e), (5,2,e) should be in the front. The lower building should be in the first
         *     3. Else: If one point is starting point and the other point is ending point:
         *              Ex: (7,3,s) vs (7,2,e), (7,3,s) should be in the front
        */
        @Override
        public int compareTo(Point that) {
            if (this.x != that.x) {
                return this.x - that.x;
            }
            else {
                if (this.isStart && that.isStart) {
                    return that.y - this.y;
                }
                else if (!this.isStart && !this.isStart) {
                    return this.y - that.y;
                }
                else {
                    return this.isStart ? -1 : 1;
                }
                /**
                 * or:
                    int y1 = this.isStart ? -this.y : this.y;
                    int y2 = that.isStart ? -that.y : that.y;
                    return y1 - y2;
                */
            }   
        }
    }
}