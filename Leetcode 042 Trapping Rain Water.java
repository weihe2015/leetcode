/*
Given n non-negative integers representing an elevation map where the width of each bar is 1, 
compute how much water it is able to trap after raining.

For example, 
Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
*/
public class Solution {

    // Native Solution: Computer the volumn by column
    // Running Time complexity: O(N^2), Space Complexity: O(1)
    /**
     * Basic Idea:
     *  1. For each bar, search left and right side to find the max heigh bar on both left and right side
     *  2. Look at each single column, the water trap area is = Math.min(maxLeft, maxRight) - currHeight;
     *  3. Sum all water trap area of each bar.
     */
    public int trap(int[] height) {
        int totalArea = 0;
        for (int i = 0, n = height.length; i < n; i++) {
            int currHeight = height[i];
            int maxLeft = currHeight;
            int maxRight = currHeight;
            // search the max high bar on the left hand side:
            for (int j = i - 1; j >= 0; j--) {
                maxLeft = Math.max(maxLeft, height[j]);
            }
            
            // search the max high bar on the right hand side:
            for (int j = i + 1; j < n; j++) {
                maxRight = Math.max(maxRight, height[j]);
            }
            totalArea += Math.min(maxLeft, maxRight) - currHeight;
        }
        return totalArea;
    }

    // Running Time Complexity: O(n), Space Complexity: O(1)
    // calculate by column
    /**
     * Basic Idea:
     *  1. Two pointer, left = 0, right = height.length;
     *      maxLeft = height[left], maxRight = height[right];
     *  2. Because the water trapped depends on the min(maxLeft, maxRight), 
     *     so when right bar is larger, we move the left bar, otherwise, we move the right bar.
     *  3. For each iteration, compare maxLeft/maxRight with next bar, and update the maxLeft and maxRight bar with next bar
     *       currArea of each column area: maxLeft - height[l] / maxRight - height[r]
    */
    public int trap(int[] height) {
        int totalArea = 0;
        if (height == null || height.length == 0) {
            return totalArea;
        }
        int l = 0;
        int r = height.length - 1;
        int maxLeft = height[l];
        int maxRight = height[r];
        
        while (l < r) {
            if (maxLeft < maxRight) {
                l++;
                maxLeft = Math.max(maxLeft, height[l]);
                totalArea += maxLeft - height[l];
            }    
            else {
                r--;
                maxRight = Math.max(maxRight, height[r]);
                totalArea += maxRight - height[r];
            }
        }
        return totalArea;
    }

    // Monotonic Stack Solution:
    /**
    1. 单调递减栈:
        -- 当后面的柱子比签名的低时候，是无法接雨水的
        -- 当找到一根比前面高的柱子时候，就可以计算接到的雨水
    2. 对更低的柱子入栈:
        -- 更低的柱子以为这后面如果能找到高柱子，这里就能接到雨水，所以入栈把它保存起来
        -- 平地相当于高度为0的柱子
    3. 当出现高于栈顶的柱子时候:
        -- 说明可以对前面的柱子结算了
        -- 计算已经到手的雨水，然后出栈前面更低的柱子
    4. 计算雨水的时候需要注意:
        -- 雨水区域的右边 r 是指当前index i
        -- 底部是栈顶stack.peek() 因为遇到了更高的右边，所以它即将出栈，用curr 来记录它
        -- 左边 l 就是新的栈顶 stack.peek()
        -- 雨水的区域确定了，水坑的高度就是左右两边更低的一遍减去底部，宽度是在左右中间
        -- 使用乘法即可计算面积
    */
    public int trap(int[] height) {
        int n = height.length;
        int totalArea = 0;
        Stack<Integer> s = new Stack<>();
        for (int i = 0; i < n; i++) {
            while (!s.isEmpty() && height[s.peek()] < height[i]) {
                int curr = s.pop();
                if (s.isEmpty()) {
                    break;
                }
                int l = s.peek();
                int r = i;
                int h = Math.min(height[l], height[r]) - height[curr];
                totalArea += (r - l - 1) * h;
            }
            s.push(i);
        }
        return totalArea;
    }

    // Math solution:
    // https://leetcode-cn.com/problems/trapping-rain-water/solution/wei-en-tu-jie-fa-zui-jian-dan-yi-dong-10xing-jie-j/
    /**
    大家应该都知道韦恩图是什么，由此启发可以得到本题思路。
    （借用官方图例）
    如下图，从左往右遍历，不管是雨水还是柱子，都计算在有效面积内，并且每次累加的值根据遇到的最高的柱子逐步上升。面积记为S1。

    从左往右遍历得S1，每步S1+=max1且max1逐步增大;
    同样地，从右往左遍历可得S2。

    从右往左遍历得S2，每步S2+=max2且max2逐步增大
    于是我们有如下发现，S1 + S2会覆盖整个矩形，并且：重复面积 = 柱子面积 + 积水面积

    最终， 积水面积 = S1 + S2 - 矩形面积 - 柱子面积
    */
    public int trap(int[] height) {
        int n = height.length;
        if (n == 0) {
            return 0;
        }
        int s1 = 0;
        int s2 = 0;
        int maxLeft = 0;
        int maxRight = 0;
        int maxHeight = 0;
        int sumHeight = 0;
        
        for (int i = 0; i < n; i++) {
            sumHeight += height[i];
            maxHeight = Math.max(maxHeight, height[i]);
            maxLeft = Math.max(maxLeft, height[i]);
            maxRight = Math.max(maxRight, height[n-i-1]);
            s1 += maxLeft;
            s2 += maxRight;
        }
        
        return s1 + s2 - maxHeight * n - sumHeight;
    }
}