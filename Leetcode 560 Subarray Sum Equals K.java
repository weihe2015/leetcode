/**
 * Given an array of integers and an integer k, you need to find the total number of continuous subarrays whose sum equals to k.

Example 1:
Input:nums = [1,1,1], k = 2
Output: 2
Note:
The length of the array is in range [1, 20,000].
The range of numbers in the array is [-1000, 1000] and the range of the integer k is [-1e7, 1e7].
*/
public class Solution {
    // Naive Solution, Running Time Complexity: O(N^2)
    public int subarraySum(int[] nums, int k) {
        int count = 0;
        for (int i = 0, max = nums.length; i < max; i++) {
            int sum = 0;
            for (int j = i; j < max; j++) {
                sum += nums[j];
                if (sum == k) {
                    count++;
                }
            }
        }
        return count;
    }

    // Solution 2: Use prefix Sum:
    public int subarraySum(int[] nums, int k) {
        int count = 0;
        // key -> sum, value -> freq of this sum:
        Map<Integer, Integer> map = new HashMap<>();
        int sum_i = 0;
        for (int num : nums) {
            sum_i += num;
            int sum_j = sum - k;
            // make sure that we count [0..i] subarray when its sum is k
            if (sum_i == k) {
                count++;
            }
            // cannot be if/else, because of the following case: [0,0,0], k = 0
            if (map.containsKey(sum_j)) {
                count += map.get(sum_j);
            }
            int freq = map.getOrDefault(sum_i, 0) + 1;
            map.put(sum_i, freq);
        }
        return count;
    }

    public int subarraySum(int[] nums, int k) {
        int count = 0;
        // key -> sum, value -> freq of this sum:
        Map<Integer, Integer> map = new HashMap<>();
        // add this 0 index to make sure that we count [0..i] subarray when its sum is k
        map.put(0, 1);
        int sum_i = 0;
        for (int num : nums) {
            sum_i += num;
            int sum_j = sum - k;
            if (map.containsKey(sum_j)) {
                count += map.get(sum_j);
            }
            int freq = map.getOrDefault(sum_i, 0) + 1;
            map.put(sum_i, freq);
        }
        return count;
    }

}