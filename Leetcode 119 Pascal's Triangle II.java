/**
public class Solution {
Given an index k, return the kth row of the Pascals triangle.

For example, given k = 3,
Return [1,3,3,1]
*/
public class Solution {
    public List<Integer> getRow(int rowIndex) {
        List<List<Integer>> result = new ArrayList<>();
        if (rowIndex < 0) {
            return null;
        }
        for (int i = 1; i <= rowIndex+1; i++) {
            List<Integer> list = new ArrayList<>();
            for (int j = 1; j <= i; j++) {
                if (j == 1 || j == i) {
                    list.add(1);
                }
                else {
                    int sum = result.get(i-2).get(j-2) + result.get(i-2).get(j-1);
                    list.add(sum);
                }
            }
            result.add(list);
        }
        return result.get(rowIndex);
    }

    public List<Integer> getRow2(int rowIndex) {
        List<Integer> ret = new ArrayList<Integer>();
        ret.add(1);
        for (int i = 1; i <= rowIndex; i++){
            for (int j = i-1; j >= 1; j--){
                int temp = ret.get(j) + ret.get(j-1);
                ret.set(j,temp);
            }
            ret.add(1);
        }
        return ret;
    }

    /**
     * C40: 1
       C41: 1*4 / 1
       C42: 4*3 / 1*2 = 6
       C43: 4*3*2 / 1*2*3 = C42 * 2/(2+1) = 6*2/3
    */
    public List<Integer> getRow3(int rowIndex) {
        List<Integer> result = new ArrayList<>();
        if (rowIndex < 0) {
            return null;
        }
        long num = 1;
        for (int i = 0; i <= rowIndex; i++) {
            result.add((int) num);
            num = num * (rowIndex-i) / (i+1);
        }
        return result;
    }

    private int getBinomialCoefficient(int n, int i) {
        if (i == 0) {
            return 1;
        }
        if (i <= n/2) {
            long numerator = 1;
            for (int j = 1; j <= i; j++) {
                numerator *= n;
                n--;
            }
            long denumerator = 1;
            for (int j = 1; j <= i; j++) {
                denumerator *= j;
            }
            return (int) (numerator/denumerator);
        }
        else {
            long numerator = 1;
            int m = n;
            for (int j = 1; j <= m-i; j++) {
                numerator *= n;
                n--;
            }
            long denumerator = 1;
            for (int j = 1; j <= m-i; j++) {
                denumerator *= j;
            }
            return (int) (numerator/denumerator);
        }
    }


}