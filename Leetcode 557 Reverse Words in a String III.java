/**
Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

Example 1:
Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"
Note: In the string, each word is separated by single space and there will not be any extra space in the string.

*/
public class Solution {
    // Solution 1: Running Time Complexity O(N * L)
    public String reverseWords(String s) {
        String[] strs = s.split(" ");
        StringBuffer sb = new StringBuffer();
        for (String str : strs) {
            sb.append(reverseString(str));
            sb.append(" ");
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }
    
    private String reverseString(String str) {
        StringBuffer sb = new StringBuffer();
        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }

    // Solution 2: Running Time Compleixty: O(2N)
    public String reverseWords(String s) {
        if (s == null) {
            return "";
        }
        int l = 0, r = 0, n = s.length();
        char[] cList = s.toCharArray();
        while (r < n) {
            while (r < n && cList[r] != ' ') {
                r++;
            }
            reverse(cList, l, r-1);
            r++;
            l = r;
        }
        return new String(cList);
    }
    private void reverse(char[] cList, int l, int r) {
        while (l < r) {
            char tmp = cList[l];
            cList[l] = cList[r];
            cList[r] = tmp;
            l++; r--;
        }
    }
}