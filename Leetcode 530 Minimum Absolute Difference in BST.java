/**
Given a binary search tree with non-negative values, find the minimum absolute difference between values of any two nodes.

Example:

Input:

   1
    \
     3
    /
   2

Output:
1

Explanation:
The minimum absolute difference is 1, which is the difference between 2 and 1 (or between 2 and 3).
*/

class Solution {
    // Iterative
    public int getMinimumDifference(TreeNode root) {
        // inorder traversal:
        int minDiff = Integer.MAX_VALUE;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = root;
        TreeNode prev = null;
        while (!stack.isEmpty() || curr != null) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if (prev != null) {
                int diff = curr.val - prev.val;
                minDiff = Math.min(minDiff, diff);
            }
            prev = curr;
            curr = curr.right;
        }
        return minDiff;
    }

    // recursive:
    private int minDiff = Integer.MAX_VALUE;
    private TreeNode prev = null;
    public int getMinimumDifference(TreeNode root) {
        inorderTraversal(root);
        return minDiff;
    }
    
    private void inorderTraversal(TreeNode root) {
        if (root == null) {
            return;
        }
        inorderTraversal(root.left);
        if (prev != null) {
            int diff = root.val - prev.val;
            minDiff = Math.min(minDiff, diff);
        }
        prev = root;
        inorderTraversal(root.right);
    }
}