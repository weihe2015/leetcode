/*
https://mode.com/sql-tutorial/

Logical Operators:

-- LIKE allows you to match similar values, instead of exact values.
-- IN allows you to specify a list of values you'd like to include.
-- BETWEEN allows you to select only rows within a certain range.
-- IS NULL allows you to select rows that contain no data in a given column.
-- AND allows you to select only rows that satisfy two conditions.
-- OR allows you to select rows that satisfy either of two conditions.
-- NOT allows you to select rows that do not match a certain condition.

LIKE:

SELECT *
  FROM tutorial.billboard_top_100_year_end
  WHERE "group" LIKE 'Snoop%'

  %:  represents any character or set of characters, any character or characters.
  _:   substitute for an individual character

  LIKE: case sensitive
  ILIKE: case insensitive

IN:
    SELECT *
    FROM tutorial.billboard_top_100_year_end
    WHERE artist IN ('Taylor Swift', 'Usher', 'Ludacris')

BETWEEN:
    SELECT *
     FROM tutorial.billboard_top_100_year_end
     WHERE year_rank BETWEEN 5 AND 10

IS NULL:
    allows you to exclude rows with missing data from your results.

    SELECT *
    FROM tutorial.billboard_top_100_year_end
    WHERE artist IS NULL

    WHERE artist = NULL will not work—you can't perform arithmetic on null values.

NOT:
    SELECT *
    FROM tutorial.billboard_top_100_year_end
    HERE year = 2013 AND year_rank NOT BETWEEN 2 AND 3

ORDER BY:
    ascending order by default: When using ORDER BY with a row limit (either through the check box on the query editor or by typing in LIMIT), the ordering clause is executed first. This means that the results are ordered before limiting to only a few rows,

    SELECT *
    FROM tutorial.billboard_top_100_year_end
    WHERE year = 2013
    ORDER BY year_rank DESC

    Ordering data by multiple columns:

    SELECT *
    FROM tutorial.billboard_top_100_year_end
    WHERE year_rank <= 3
    ORDER BY year DESC, year_rank

Aggregate function:
    -- COUNT counts how many rows are in a particular column.
    -- SUM adds together all the values in a particular column.
    -- MIN and MAX return the lowest and highest values in a particular column, respectively.
    -- AVG calculates the average of a group of selected values.

COUNT:
    COUNT(col) A count of all of rows in which the column value is not null.

    COUNT(*) will count the number of records.
    COUNT(column_name) will count the number of records where column_name is not null.

SUM:
    SUM is a SQL aggregate function. that totals the values in a given column. Unlike COUNT, you can only use SUM on columns containing numerical values.

    aggregators only aggregate vertically.
    SUM treats nulls as 0.

MIN/MAX: return the lowest and highest values in a particular column.
    MIN will return the lowest number, earliest date, or non-numerical value as close alphabetically to "A" as possible

AVG:
    calculates the average of a selected group of values.
    First, it can only be used on numerical columns. Second, it ignores nulls completely

    There are some cases in which you'll want to treat null values as 0. For these cases, you'll want to write a statement that changes the nulls to 0

GROUP BY: 
    GROUP BY allows you to separate data into groups, which can be aggregated independently of one another
    SQL evaluates the aggregations before the LIMIT clause

    GROUP BY multiple columns: 

    SELECT year,
       month,
       COUNT(*) AS count
    FROM tutorial.aapl_historical_stock_price
    GROUP BY year, month

HAVING:
    HAVING is the "clean" way to filter a query that has been aggregated, but this is also commonly done using a subquery

    SELECT year,
       month,
       MAX(high) AS month_high
    FROM tutorial.aapl_historical_stock_price
    GROUP BY year, month
    HAVING MAX(high) > 400
    ORDER BY year, month

CASE:
    The CASE statement is followed by at least one pair of WHEN and THEN statements—SQL's equivalent of IF/THEN in Excel.
    Every CASE statement must end with the END statement. The ELSE statement is optional, and provides a way to capture values not specified in the WHEN/THEN statements. CASE is easiest to understand in the context of an example:

    SELECT player_name,
       year,
       CASE WHEN year = 'SR' THEN 'yes'
            ELSE NULL END AS is_a_senior
    FROM benn.college_football_players

    Adding multiple conditions to a CASE statement:

    SELECT player_name,
       weight,
       CASE WHEN weight > 250 THEN 'over 250'
            WHEN weight > 200 AND weight < 250 THEN '201-250'
            WHEN weight > 175 THEN '176-200'
            ELSE '175 or under' END AS weight_group
    FROM benn.college_football_players

    CASE basics:
    1. The CASE statement always goes in the SELECT clause
    2. CASE must include the following components: WHEN, THEN, and END. ELSE is an optional component.
    3. You can make any conditional statement using any conditional operator (like WHERE ) between WHEN and THEN. This includes stringing together multiple conditional statements using AND and OR.
    4. You can include multiple WHEN statements, as well as an ELSE statement to deal with any unaddressed conditions.

DISINCT:
    SELECT DISTINCT : unique values from the column

    include two (or more) columns in SELECT DISTINCT clause, your results will contain all of the unique pairs of those two columns:

    For MAX and MIN, you probably shouldn't ever use DISTINCT because the results will be the same as without DISTINCT, and the DISTINCT function will make your query substantially slower to return results.

    SELECT DISTINCT year, month
    FROM tutorial.aapl_historical_stock_price

SQL JOINS:
    SELECT *
        FROM college_football_players players
        JOIN college_football_teams teams
            ON teams.school_name = players.school_name

    INNER JOINS:
        Inner joins eliminate rows from both tables that do not satisfy the join condition set forth in the ON statement. In mathematical terms, an inner join is the intersection of the two tables.

    LEFT JOINS:
        returns only unmatched rows from the left table.

    RIGHT JOINS:
        returns only unmatched rows from the right table.

    FULL OUTER JOIN:
        returns unmatched rows from both tables.
*/