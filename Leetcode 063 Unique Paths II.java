/*
A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time.
The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

Now consider if some obstacles are added to the grids. How many unique paths would there be?

An obstacle and empty space is marked as 1 and 0 respectively in the grid.

Note: m and n will be at most 100.

Example 1:

Input:
[
  [0,0,0],
  [0,1,0],
  [0,0,0]
]
Output: 2
Explanation:
There is one obstacle in the middle of the 3x3 grid above.
There are two ways to reach the bottom-right corner:
1. Right -> Right -> Down -> Down
2. Down -> Down -> Right -> Right

*/

public class Solution {

    // solution 1
    // Running Time Complexity: O(m * n), Space Complexity: O(m * n)
    public int uniquePathsWithObstacles(int[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            if (grid[i][0] == 1) {
                break;
            }
            else {
                dp[i][0] = 1;
            }
        }
        for (int j = 0; j < n; j++) {
            if (grid[0][j] == 1) {
                break;
            }
            else {
                dp[0][j] = 1;
            }
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (grid[i][j] != 1) {
                    dp[i][j] = dp[i-1][j] + dp[i][j-1];
                }
                else {
                    dp[i][j] = 0;
                }
            }
        }
        return dp[m-1][n-1];
    }

    // solution 2
    /*  iterate row by row
        dp = new int[row size]
        i = 0..n-1
        if(grid[i][j] == 1)
            dp[j] = 0;
        else, set dp[j] = dp[j] + dp[j-1]
    */
    public int uniquePathsWithObstacles(int[][] grid) {
        // one dimensional array for space: dp[n-1];
        // sub problem: dp[i] = dp[i-1];
        // condition: if (obstacleGrid[i][j]) == 1, dp[j] = 0;
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int[] dp = new int[m];
        dp[0] = 1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    dp[j] = 0;
                }
                else if (j > 0){
                    dp[j] = dp[j] + dp[j-1];
                }
            }
        }
        return dp[n-1];
        /**
        用 m或者n来做dp的size都是一样的。
        int[] dp = new int[m];
        dp[0] = 1;
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < m; i++){
                if (grid[i][j] == 1) {
                    dp[i] = 0;
                }
                else if (i > 0) {
                    dp[i] = dp[i] + dp[i-1];
                }
            }
        }
        return dp[m-1];
        */
    }
}