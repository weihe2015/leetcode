/**
Find the total area covered by two rectilinear rectangles in a 2D plane.

Each rectangle is defined by its bottom left corner and top right corner as shown in the figure.

Example:

Input: A = -3, B = 0, C = 3, D = 4, E = 0, F = -1, G = 9, H = 2
Output: 45

Rectangle Area
Assume that the total area is never beyond the maximum possible value of int.
*/
public class Solution {
    public int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int area1 = (C - A) * (D - B);
        int area2 = (G - E) * (H - F);
        int totalArea = area1 + area2;
        // If two retangle are separated in X axios
        if (C < E || G < A) {
            return totalArea;
        }
        
        // If two retangle are separated in Y axios
        if (D < F || H < B) {
            return totalArea;
        }
        
        int left = Math.max(A, E);
        int right = Math.min(C, G);
        int top = Math.min(D, H);
        int buttom = Math.max(B, F);
        
        return totalArea - (right - left) * (top - buttom);
    }
}