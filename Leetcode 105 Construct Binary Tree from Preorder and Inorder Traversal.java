/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
 /**

Given preorder and inorder traversal of a tree, construct the binary tree.

Note:
You may assume that duplicates do not exist in the tree.

For example, given

preorder = [3,9,20,15,7]
inorder = [9,3,15,20,7]
Return the following binary tree:

    3
   / \
  9  20
    /  \
   15   7
 */
public class Solution {

    // solution 1
    private int preStart = 0;
    private int inStart = 0;
    public TreeNode buildTree1(int[] preorder, int[] inorder) {
        // end --> root node in in-order
        return helper(preorder,inorder,null);
    }
    // TreeNode end is the boundary of left subtree
    public TreeNode helper(int[] preorder, int[] inorder, TreeNode end){
        if (preStart > preorder.length-1 || inStart > inorder.length-1) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[preStart]);
        preStart++;
        // have left sub tree
        if (inorder[inStart] != root.val) {
            root.left = helper(preorder,inorder,root);
        }
        inStart++;
        // single element left or have right sub tree
        if (end == null || inorder[inStart] != end.val) {
            root.right = helper(preorder,inorder,end);
        }
        return root;
    }

    // solution 2
    /*
             root <--left--> <--right-->
    preorder: 50, 30, 10, 40, 70, 60, 90
            <---left-->  root <--right-->
    inorder: 10, 30, 40, 50, 60, 70, 90
    */
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return buildTree(preorder, 0, preorder.length-1, inorder, 0, inorder.length-1);
    }

    private TreeNode buildTree(int[] preorder, int preStart, int preEnd, int[] inorder, int inStart, int inEnd) {
        // firstly check if there is no element or only one element
        // then immediately return
        if (preStart > preEnd || inStart > inEnd) {
            return null;
        }
        int rootVal = preorder[preStart];
        TreeNode root = new TreeNode(rootVal);

        // Otherwise, we use our defined strategy, as first element in
        // preorder is the root and we find the value in in-order array
        // to determine how many 'left' elemnt in there and construct the left sub tree
        // Find the root index in inorder array
        int rootIdx = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }
         // calculate the left subtree size in inorder array:
        int leftTreeSize = rootIdx - inStart;
        root.left = buildTree(preorder, preStart+1, preStart + leftSubTreeSize, inorder, inStart, rootIdx-1);
        root.right = buildTree(preorder, preStart + leftSubTreeSize + 1, preEnd, inorder, rootIdx+1, inEnd);
        return root;
    }

    // Iterative:
    // https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/discuss/34555/The-iterative-solution-is-easier-than-you-think!
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length == 0 || inorder.length == 0) {
            return null;
        }
        // build a map of indices of values that the appear in the inorder map:
        // key -> node's value, value -> inorder idx
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, n = inorder.length; i < n; i++) {
            map.put(inorder[i], i);
        }
        
        Stack<TreeNode> stack = new Stack<>();
        int rootVal = preorder[0];
        TreeNode root = new TreeNode(rootVal);
        stack.push(root);
        
        for (int i = 1, n = preorder.length; i < n; i++) {
            int val = preorder[i];
            TreeNode curr = new TreeNode(val);
            TreeNode prev = stack.peek();
            
            // the new node is on the left of the last node, so it must be its left child (that's the way preorder works)
            if (map.get(val) < map.get(prev.val)) {
                prev.left = curr;
            }
            else {
                TreeNode parNode = null;
                /**
                the new node is on the right of the last node,
                so it must be the right child of either the last node or one of the last node's ancestors.
                pop the stack until we either run out of ancestors
                or the node at the top of the stack is to the right of the new node
                */
                while (!stack.isEmpty() && map.get(val) > map.get(stack.peek().val)) {
                    parNode = stack.pop();
                }
                parNode.right = curr;
            }
            stack.push(curr);
        }
        
        return root;
    }
}