/**
Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.

Example:
Given nums = [-2, 0, 3, -5, 2, -1]

sumRange(0, 2) -> 1
sumRange(2, 5) -> -1
sumRange(0, 5) -> -3
Note:
You may assume that the array does not change.
There are many calls to sumRange function.
*/

// Solution 1: Use SCAN array:
// GET SUM: Running Time Complexity: O(N)
public class NumArray {
    private int[] nums;
    public NumArray(int[] nums) {
        this.nums = nums;
    }
    
    public int sumRange(int i, int j) {
        int sum = 0;
        for (int k = i; k <= j; k++) {
            sum += nums[k];
        }
        return sum;
    }
}

// Solution 2: Use PrefixSum:
// GET SUM: Running Time Complexity: O(1)
class NumArray {
    private int[] nums;
    private int[] prefixSum;
    public NumArray(int[] nums) {
        this.nums = nums;
        int n = nums.length, sum = 0;
        this.prefixSum = new int[n];
        for (int i = 0; i < n; i++) {
            sum += nums[i];
            prefixSum[i] = sum;
        }
    }
    
    public int sumRange(int i, int j) {
        if (i == 0) {
            return prefixSum[j];
        }
        else {
            return prefixSum[j] - prefixSum[i-1];
        }
    }
}

// Solution 3: Use Binary Index Tree:
// Build Tree: Running Time Complexity: O(NlogN);
// UPDATE: Running Time Complexity O(logN),
// GET SUM: Running Time Complexity: O(logN)
public class NumArray {
    private int[] nums;
    private int[] BIT;
    private int N;
    public NumArray(int[] nums) {
        this.nums = nums;
        int n = nums.length;
        this.N = n + 1;
        this.BIT = new int[N];
        for (int i = 1; i <= n; i++) {
            update(i, nums[i-1]);
        }
    }
    
    private void update(int idx, int value) {
        while (idx < N) {
            BIT[idx] += value;
            idx = getNextIdx(idx);
        }
    }
    
    private int getNextIdx(int idx) {
        return idx + (idx & (-idx));
    }
    
    private int getParentIdx(int idx) {
        return idx - (idx & (-idx));
    }
    
    private int getSum(int idx) {
        idx = idx + 1;
        int sum = 0;
        while (idx > 0) {
            sum += BIT[idx];
            idx = getParentIdx(idx);
        }
        return sum;
    }
    
    public int sumRange(int i, int j) {
        return getSum(j) - getSum(i-1);
    }
}

// Your NumArray object will be instantiated and called as such:
// NumArray numArray = new NumArray(nums);
// numArray.sumRange(0, 1);
// numArray.sumRange(1, 2);