/**
Given a string, determine if a permutation of the string could form a palindrome.

For example,
"code" -> False, "aab" -> True, "carerac" -> True.

Consider the palindromes of odd vs even length. What difference do you notice?

Count the frequency of each character

If each character occurs even number of times, then it must be a palindrome. How about character which occurs odd number of times?
*/
public class solution{
	// use set
	public boolean canPermutePalindrome(String s) {
		Set<Character> set = new Set<Character>();
		for(char c : s.toCharArray()){
			if(!set.contains(c)) {
				set.add(c);
			}
			else {
				set.remove(c);
			}
		}
		return set.size() <= 1;
	}

	// use count
	public boolean canPermutePalindrome2(String s) {
		int[] cList = new int[256];
		int count = 0;
		for(char c : s.toCharArray()){
			if(cList[c] > 0){
				cList[c]--;
				count--;
			}
			else{
				cList[c]++;
				count++;
			}
		}
		return count <= 1;
	}
	
	// use xor
	public boolean canPermutePalindrome3(String s) {
	    int[] set = new int[256];
	    int min = Integer.MAX_VALUE;
	    int max = Integer.MIN_VALUE;
	    for (char c : s.toCharArray()) {
	        set[c] ^= 1;
	        min = Math.min(min, c);
	        max = Math.max(max, c);
	    }
	    int sum = 0;
	    for (int i = min; i <= max; i++) {
	        sum += set[i];
	    }
	    return (sum <= 1);
	}

	@Test
	public void PalindromePermutationTest() throws Exception {
		PalindromePermutation sol = new PalindromePermutation();
		Assert.assertFalse(sol.canPermutePalindrome("code"));
		Assert.assertTrue(sol.canPermutePalindrome("aab"));
		Assert.assertTrue(sol.canPermutePalindrome("carerac"));
		
		Assert.assertFalse(sol.canPermutePalindrome1("code"));
		Assert.assertTrue(sol.canPermutePalindrome1("aab"));
		Assert.assertTrue(sol.canPermutePalindrome1("carerac"));
	}
}