/**
There is a ball in a maze with empty spaces and walls. 
The ball can go through empty spaces by rolling up, down, left or right, 
but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the ball's start position, the destination and the maze, determine whether the ball could stop at the destination.

The maze is represented by a binary 2D array. 
1 means the wall and 0 means the empty space. 
You may assume that the borders of the maze are all walls. The start and destination coordinates are represented by row and column indexes.

Example 1:

Input 1: a maze represented by a 2D array

0 0 1 0 0
0 0 0 0 0
0 0 0 1 0
1 1 0 1 1
0 0 0 0 0

Input 2: start coordinate (rowStart, colStart) = (0, 4)
Input 3: destination coordinate (rowDest, colDest) = (4, 4)

Output: true

Explanation: One possible way is : left -> down -> left -> down -> right -> down -> right.

Example 2:

Input 1: a maze represented by a 2D array

0 0 1 0 0
0 0 0 0 0
0 0 0 1 0
1 1 0 1 1
0 0 0 0 0

Input 2: start coordinate (rowStart, colStart) = (0, 4)
Input 3: destination coordinate (rowDest, colDest) = (3, 2)

Output: false

Explanation: There is no way for the ball to stop at the destination.

Note:

There is only one ball and one destination in the maze.
Both the ball and the destination exist on an empty space, and they will not be at the same position initially.
The given maze does not contain border (like the red rectangle in the example pictures), but you could assume the border of the maze are all walls.
The maze contains at least 2 empty spaces, and both the width and height of the maze won't exceed 100.

*/
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};

    // BFS:
    public boolean hasPath(int[][] maze, int[] start, int[] end) {
        int m = maze.length;
        int n = maze[0].length;
        boolean[][] visited = new boolean[m][n];
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(start);

        while (!queue.isEmpty()) {
            int[] curr = queue.poll();
            int i = curr[0];
            int j = curr[1];
            if (i == end[0] && j == end[1]) {
                return true;
            }
            if (visited[i][j]) {
                continue;
            }
            visited[i][j] = true;
            for (int[] dir : dirs) {
                int x = i + dir[0];
                int y = j + dir[1];
                if (!isValid(maze, x, y, m, n)) {
                    continue;
                }
                while (isValid(maze, x, y, m, n)) {
                    x += dir[0];
                    y += dir[1];
                }
                x -= dir[0];
                y -= dir[1];
                if (!visited[x][y]) {
                    queue.offer(new int[]{x, y});
                }
            }
        }
        return false;
    }

    private boolean isValid(int[][] maze, int x, int y, int m, int n) {
        return !(x < 0 || x >= m || y < 0 || y >= n || maze[x][y] == 1);
    }

    // DFS:
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public boolean hasPath(int[][] maze, int[] start, int[] end) {
        // write your code here
        int m = maze.length;
        int n = maze[0].length;
        boolean[][] visited = new boolean[m][n];
        return dfs(maze, start[0], start[1], m, n, end, visited);
    }
    
    private boolean dfs(int[][] maze, int i, int j, int m, int n, int[] end, boolean[][] visited) {
        if (i == end[0] && j == end[1]) {
            return true;
        }
        if (visited[i][j]) {
            return false;
        }
        visited[i][j] = true;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            if (!isValid(maze, x, y, m, n)) {
                continue;
            }
            while (isValid(maze, x, y, m, n)) {
                x += dir[0];
                y += dir[1];
            }
            x -= dir[0];
            y -= dir[1];
            if (!visited[x][y] && dfs(maze, x, y, m, n, end, visited)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isValid(int[][] maze, int x, int y, int m, int n) {
        return !(x < 0 || x >= m || y < 0 || y >= n || maze[x][y] == 1);
    }
}