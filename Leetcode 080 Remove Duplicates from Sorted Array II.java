/*
Follow up for "Remove Duplicates":
What if duplicates are allowed at most twice?

For example,
Given sorted array nums = [1,1,1,2,2,3],

Your function should return length = 5, with the first five elements of nums being 1, 1, 2, 2 and 3.
It doesnt matter what you leave beyond the new length.
*/
public class Solution {
    // Bruth Force:
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        // count number of frequences
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        int n = nums.length;
        int[] aux = new int[n];
        int idx = 0;
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (set.contains(num)) {
                continue;
            }
            else {
                int freq = Math.min(map.get(num), 2);
                set.add(num);
                for (int i = 0; i < freq; i++) {
                    aux[idx++] = num;
                }
            }
        }
        // copy the correct output back to nums:
        for (int i = 0; i < idx; i++) {
            nums[i] = aux[i];
        }
        return idx;
    }

    // Better Solution:
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int idx = 1;
        boolean meetTwice = false;
        for (int i = 1, max = nums.length; i < max; i++) {
            if (nums[i] == nums[i-1]) {
                if (!meetTwice) {
                    meetTwice = true;
                    nums[idx++] = nums[i];
                }
            }
            else {
                meetTwice = false;
                nums[idx++] = nums[i];
            }
        }
        return idx;
    }

    // Previous solution
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        // handle case that [1,1]
        if (n < 3) {
            return n;
        }
        // two pointer:
        int prev = 1;
        for (int i = 2; i < n; ++i) {
            if (nums[i] != nums[prev-1]) {
                ++prev;
                // skip the same pointer and same element to save time.
                if (prev != i && nums[prev] != nums[i]) {
                    nums[prev] = nums[i];
                }
            }
        }
        // return array length, so index + 1; prev is the place where removed duplicate array ends.
        return prev+1;
    }
}