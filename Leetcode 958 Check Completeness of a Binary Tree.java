/**
Given the root of a binary tree, determine if it is a complete binary tree.

In a complete binary tree, every level, except possibly the last, is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.

Example 1:
Input: root = [1,2,3,4,5,6]
Output: true
Explanation: Every level before the last is full (ie. levels with node-values {1} and {2, 3}), and all nodes in the last level ({4, 5, 6}) are as far left as possible.

Example 2:
Input: root = [1,2,3,4,5,null,7]
Output: false
Explanation: The node with value 7 isn't as far left as possible.
 

Constraints:
The number of nodes in the tree is in the range [1, 100].
1 <= Node.val <= 1000
*/
public class Solution {
    // Iterative:
    public boolean isCompleteTree(TreeNode root) {
        // level order traversal:
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        TreeNode prev = root;
        
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                TreeNode curr = queue.poll();
                if (prev == null && curr != null) {
                    return false;
                }

                if (curr != null) {
                    queue.offer(curr.left);
                    queue.offer(curr.right);
                }
                prev = curr;
            }
        }
        return true;
    }

    // Recursive:
    /**
    用 1 表示根节点，对于任意一个节点 v，它的左孩子为 2*v 右孩子为 2*v + 1。这就是我们用的规则，
    在这个规则下，一颗二叉树是完全二叉树当且仅当节点编号依次为 1, 2, 3, ... 且没有间隙。
    */
    private int count;
    private int maxIndex;
    public boolean isCompleteTree(TreeNode root) {
        // root index = 1
        // left child index = 2 * index
        // right child index = 2 * index + 1
        this.count = 0;
        this.maxIndex = 0;
        helper(root, 1);
        return count == maxIndex;
    }
    
    private void helper(TreeNode node, int index) {
        if (node == null) {
            return;
        }
        count++;
        maxIndex = Math.max(maxIndex, index);
        helper(node.left, index*2);
        helper(node.right, index*2+1);
    }
}