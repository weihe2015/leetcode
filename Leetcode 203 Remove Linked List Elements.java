Remove all elements from a linked list of integers that have value val.

Example
Given: 1 --> 2 --> 6 --> 3 --> 4 --> 5 --> 6, val = 6
Return: 1 --> 2 --> 3 --> 4 --> 5

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Use dummy Header
    public ListNode removeElements(ListNode head, int val) {
        ListNode newHead = new ListNode(0), prev = newHead, curr = head;
        newHead.next = head;
        while (curr != null) {
            if (curr.val == val) {
                prev.next = curr.next;
            }
            else {
                prev = prev.next;
            }
            curr = curr.next;
        }
        return newHead.next;
    }

    // Without using dummy header
    public ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return null;
        }

        // remove element in the header
        while (head != null && head.val == val) {
            head = head.next;
        }
        
        ListNode curr = head;
        // move the curr pointer only when curr.next.val != val:
        // Handle case: 1 -> 2 -> 2 -> 3, val = 2
        while (curr != null && curr.next != null) {
            if (curr.next.val == val) {
                curr.next = curr.next.next;
            }
            else {
                curr = curr.next;
            }
        }
        return head;
    }
}