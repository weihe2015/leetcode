/*
Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.

For example, you may serialize the following tree

    1
   / \
  2   3
     / \
    4   5
as "[1,2,3,null,null,4,5]", just the same as how LeetCode OJ serializes a binary tree. You do not necessarily need to follow this format, so please be creative and come up with different approaches yourself.
Note: Do not use class member/global/static variables to store states. Your serialize and deserialize algorithms should be stateless.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {
    private static final String NULL = "NULL";
    private static final String COMMA = ",";

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuffer sb = new StringBuffer();
        if (root == null) {
            return sb.toString();
        }
        // Use Level Order Traversal to serialize the tree.
        List<String> nodeTextList = new ArrayList<>();

        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                if (curr == null) {
                    nodeTextList.add(NULL);
                }
                else {
                    nodeTextList.add(String.valueOf(curr.val));
                    queue.offer(curr.left);
                    queue.offer(curr.right);
                }
            }
        }
        // find the first non-null value from the end:
        int j = nodeTextList.size()-1;
        while (j >= 0) {
            String text = nodeTextList.get(j);
            if (!text.equals(NULL)) {
                break;
            }
            j--;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i <= j; i++) {
            String text = nodeTextList.get(i);
            sb.append(text).append(COMMA);
        }
        // remove the last comma ,
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        // Handle Empty Tree: [], and empty data
        if (data == null || data.length() == 0) {
            return null;
        }

        String[] arr = data.split(COMMA);
        TreeNode root = null;
        String nodeVal = arr[0];
        root = convertNumToNode(nodeVal);
        if (root == null) {
            return null;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        int i = 1;
        int n = arr.length;
        while (i < n) {
            int size = queue.size();
            for (int j = 1; j <= size; j++) {
                TreeNode currNode = queue.poll();
                if (currNode == null) {
                    continue;
                }
                if (i == n) {
                    return root;
                }
                currNode.left = convertNumToNode(arr[i++]);

                if (i == n) {
                    return root;
                }
                currNode.right = convertNumToNode(arr[i++]);
                queue.offer(currNode.left);
                queue.offer(currNode.right);
            }
        }
        return root;
    }

    private TreeNode convertNumToNode(String text) {
        if (text.equals(NULL)) {
            return null;
        }
        // handle NumberFormatException as well:
        else {
            int num = Integer.parseInt(text);
            return new TreeNode(num);
        }
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));