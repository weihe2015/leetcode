Given an array of numbers nums, 
in which exactly two elements appear only once 
and all the other elements appear exactly twice. 
Find the two elements that appear only once.

For example:

Given nums = [1, 2, 1, 3, 2, 5], return [3, 5].

public class Solution {
    public int[] singleNumber(int[] nums) {
        int diff = 0;
        for(int n : nums)
            diff ^= n;
        diff &= -diff;
        int[] result = new int[2];
        for(int n : nums){
            if((n & diff) == 0)
                result[0] ^= n;
            else
                result[1] ^= n; 
        }
        return result;
    }
}