/**
Given a char array representing tasks CPU need to do. It contains capital letters A to Z where different letters represent different tasks.Tasks could be done without original order. Each task could be done in one interval. For each interval, CPU could finish one task or just be idle.

However, there is a non-negative cooling interval n that means between two same tasks, there must be at least n intervals that CPU are doing different tasks or just be idle.

You need to return the least number of intervals the CPU will take to finish all the given tasks.

Example 1:
Input: tasks = ["A","A","A","B","B","B"], n = 2
Output: 8
Explanation: A -> B -> idle -> A -> B -> idle -> A -> B.
Note:
The number of tasks is in the range [1, 10000].
The integer n is in the range [0, 100].
*/
public class Solution {
    // Figure out that we can do a "greedy arrangement": always arrange task with most frequency first.
    // only need to get the total idles
    // Running Time Complexity: O(n), where n is the size of interval
    public int leastInterval(char[] tasks, int n) {
        int[] maps = new int[26];
        for (char task : tasks) {
            maps[task - 'A']++;
        }
        Queue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
        for (int num : maps) {
            if (num > 0) {
                pq.offer(num);
            }
        }
        List<Integer> list = new ArrayList<>();
        int time = 0;
        while (!pq.isEmpty()) {
            // from i = 0 to n because from the question requirement:
            // There are at least n intervals that CPU are doing different tasks or just be idle
            // n = 2: A, B, idle,
            // If i = 1 to n, it will becomes: A, B, A, B, which does not fulfill the requirement
            int i = 0;
            list.clear();
            while (i <= n) {
                // execute task time:
                if (!pq.isEmpty()) {
                    int num = pq.poll();
                    // this task executes more than once, add it back to todo list
                    if (num > 1) {
                        list.add(num-1);
                    }
                }
                // Idle time:
                // If all tasks are finished executing, there is no need to count time
                else if (pq.isEmpty() && list.isEmpty()) {
                    break;
                }
                time++;
                i++;
            }
            // add the rest tasks back to the pq and start next execution period.
            for (int num : list) {
                pq.offer(num);
            }
        }
        return time;
    }
}