/**
Given a collection of intervals, find the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.

Note:
You may assume the interval's end point is always bigger than its start point.
Intervals like [1,2] and [2,3] have borders "touching" but they don't overlap each other.
Example 1:
Input: [ [1,2], [2,3], [3,4], [1,3] ]

Output: 1

Explanation: [1,3] can be removed and the rest of intervals are non-overlapping.
Example 2:
Input: [ [1,2], [1,2], [1,2] ]

Output: 2

Explanation: You need to remove two [1,2] to make the rest of intervals non-overlapping.
Example 3:
Input: [ [1,2], [2,3] ]

Output: 0

Explanation: You don't need to remove any of the intervals since they're already non-overlapping.
*/
public class Solution {
    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals == null || intervals.length == 0) {
            return 0;
        }
        int n = intervals.length;
        Arrays.sort(intervals, (i1, i2) -> (i1.end - i2.end));
        // solution 1:
        int end = -1, count = 1;
        for (Interval interval : intervals) {
            if (end == -1) {
                end = interval.end;
            }
            else {
                if (end <= interval.start) {
                    count++;
                    end = interval.end;
                }       
            }
        }
        return n - count;
        // Solution 2:
        // Max Heap, let max end point on the top:
        Queue<Integer> pq = new PriorityQueue<Integer>(n, (i1, i2) -> (i2 - i1));
        for (Interval interval : intervals) {
            if (pq.isEmpty()) {
                pq.offer(interval.end);
            }
            else {
                if (pq.peek() <= interval.start) {
                    pq.offer(interval.end);
                }
            }
        }
        return n - pq.size();
    }
    // Faster Solution: Running Time Complexity: O(NlogN)
    /** Two pointers solution, left and right pointer:
    Move left pointer to right pointer only when:
      1. New Interval is completely inside the previous interval
      2. New Interval is completely outside of previous interval
    Increase counter when two intervals are partially overlap.
    */
    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals == null || intervals.length <= 1) {
            return 0;
        }   
        Arrays.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        int prev = 0, count = 0, n = intervals.length;
        for (int i = 1; i < n; i++) {
            if (intervals[prev].end > intervals[i].start) {
                if (intervals[prev].end > intervals[i].end) {
                    prev = i;
                }
                count++;
            }
            else {
                prev = i;
            }
        }
        return count;
    }
}