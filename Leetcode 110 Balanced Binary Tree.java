/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
   // Top down approach O(N^2)
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        int leftDepth = depth(root.left);
        int rightDepth = depth(root.right);
        if (Math.abs(leftDepth - rightDepth) > 1) {
            return false;
        }
        return isBalanced(root.left) && isBalanced(root.right);
    }
    
    private int depth(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return Math.max(depth(node.left), depth(node.right)) + 1;
    }

    // Bottom up approach, each node visit once O(n)
    public boolean isBalanced(TreeNode root) {
        return depth(root) != -1;
    }
    
    public int depth(TreeNode root){
        if(root == null)
            return 0;
        int left_depth = depth(root.left);
        if(left_depth == -1)
            return -1;
        int right_depth = depth(root.right);
        if(right_depth == -1)
            return -1;
        if(Math.abs(left_depth - right_depth) > 1)
            return -1;
        return Math.max(left_depth,right_depth) + 1;
    }
}