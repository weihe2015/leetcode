/*
Given a collection of distinct numbers, return all possible permutations.

For example,
[1,2,3] have the following permutations:
[1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], and [3,2,1].
*/
public class Solution {
    // Running Time Complexity:
    // Recursion: Running Time Complexity: T(N) = N * T(N-1) = O(N!)
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        int n = nums.length;
        boolean[] visited = new boolean[n];
        dfs(result, nums, visited, n, new ArrayList<Integer>());
        return result;
    }
    
    private void dfs(List<List<Integer>> result, int[] nums, boolean[] visited, int n, List<Integer> list) {
        if (list.size() == n) {
            // use new ArrayList<Integer>(list) to create a new array
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            visited[i] = true;
            list.add(nums[i]);
            dfs(result, nums, visited, n, list);
            list.remove(list.size()-1);
            visited[i] = false;
        }
    }

    // Solution 2: Use hashSet to avoid adding duplicate items into result
    public List<List<Integer>> permute2(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        generatePermutation2(result, nums, new ArrayList<Integer>(), new HashSet<Integer>());
        return result;
    }
    
    private void generatePermutation(List<List<Integer>> result, int[] nums, ArrayList<Integer> list, HashSet<Integer> set) {
        int max = nums.length;
        if (list.size() == max) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = 0; i < max; i++) {
            int num = nums[i];
            if (set.contains(num)) {
                continue;
            }
            set.add(num);
            list.add(num);
            generatePermutation(result, nums, list, set);
            list.remove(list.size()-1);
            set.remove(num);
        }
    }

    // backtrace
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if( nums == null || nums.length == 0) {
            return null;
        }
        permute(result,new ArrayList<Integer>(), nums,0);
        return result;
    }
    public void permute(List<List<Integer>> result, List<Integer> list, int[] nums, int index){
        if(index == nums.length){
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for(int i = 0; i <= index; i++){
            list.add(i,nums[index]);
            permute(result,new ArrayList<Integer>(list),nums,index+1);
            list.remove(i);
        }
    }

    // iterative
    public List<List<Integer>> permute4(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if( nums == null || nums.length == 0) {
            return null;
        }
        result.add(new ArrayList<Integer>());
        for(int n : nums){
            int i = result.size();
            for(int j = i; j > 0; j--){
                List<Integer> list = result.get(0);
                result.remove(0);
                for(int k = 0; k <= list.size(); k++){
                    List<Integer> temp = new ArrayList<Integer>(list);
                    temp.add(k,n);
                    result.add(temp);
                }
            }
        }
        return result;
    }

    /**
     * Basic Idea: num = [1,2,3]
     * 
     * [[2,1],[1,2]] i = 2, num = 3
        for each subList,  make result.size()+1 of itself and add num into 0-i index.:
        copy: [[2,1],[2,1],[2,1],[1,2][1,2],[1,2]]
        add: [[3,2,1], [2,3,1], [2,1,3], [3,1,2], [1,3,2], [1,2,3]]
       Running time: O(N^4)
     * 
    */
    public List<List<Integer>> permute5(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        // Add first item
        List<Integer> subList = new ArrayList<Integer>();
        subList.add(nums[0]);
        result.add(subList);
        for (int i = 1, max = nums.length; i < max; i++) {
            int num = nums[i];
            List<List<Integer>> temp = new ArrayList<>();
            // for each subList:
            for (int j = 0, max_j = result.size(); j < size; j++) {
                // make copy of previous item for n times, n = size + 1
                // and insert num from 0 to i index
                for (int k = 0; k <= i; k++) {
                	subList = new ArrayList<Integer>(result.get(j));
                	subList.add(k, num);
                    temp.add(subList);
                }
            }
            result = temp;
        }
        return result;
    }
}