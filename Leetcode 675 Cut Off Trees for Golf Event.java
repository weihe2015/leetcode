/**
You are asked to cut off trees in a forest for a golf event. 
The forest is represented as a non-negative 2D map, in this map:

0 represents the obstacle can't be reached.
1 represents the ground can be walked through.
The place with number bigger than 1 represents a tree can be walked through, 
and this positive number represents the tree's height.
In one step you can walk in any of the four directions top, bottom, left and right also when standing in a point which is a tree you can decide whether or not to cut off the tree.

You are asked to cut off all the trees in this forest in the order of tree's height - always cut off the tree with lowest height first. And after cutting, the original place has the tree will become a grass (value 1).

You will start from the point (0, 0) and you should output the minimum steps you need to walk to cut off all the trees. If you can't cut off all the trees, output -1 in that situation.

You are guaranteed that no two trees have the same height and there is at least one tree needs to be cut off.

Example 1:

Input: 
[
 [1,2,3],
 [0,0,4],
 [7,6,5]
]
Output: 6
 

Example 2:

Input: 
[
 [1,2,3],
 [0,0,0],
 [7,6,5]
]
Output: -1
 

Example 3:

Input: 
[
 [2,3,4],
 [0,0,5],
 [8,7,6]
]
Output: 6
Explanation: You started from the point (0,0) and you can cut off the tree in (0,0) directly without walking.
 

Constraints:

1 <= forest.length <= 50
1 <= forest[i].length <= 50
0 <= forest[i][j] <= 10^9
*/
class Solution {
    class Point {
        int x;
        int y;
        int val;
        public Point(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }
    
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    
    public int cutOffTree(List<List<Integer>> forest) {
        int m = forest.size();
        int n = forest.get(0).size();
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int val = forest.get(i).get(j);
                Point p = new Point(i, j, val);
                points.add(p);
            }
        }
        // Sorted by the value, and keep the coordinate
        Collections.sort(points, Comparator.comparingInt(p -> p.val));
        
        int sx = 0;
        int sy = 0;
        int total = 0;
        for (Point p : points) {
            int tx = p.x;
            int ty = p.y;
            int step = bfs(forest, sx, sy, tx, ty, m, n);
            if (step == -1) {
                return -1;
            }
            
            forest.get(tx).get(ty) = 1;
            total += step;
            sx = tx;
            sy = ty;
        }
        return total;
    }
    
    private int bfs(List<List<Integer>> forest, int sx, int sy, int tx, int ty, int m, int n) {
        boolean[][] visited = new boolean[m][n];
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{sx, sy});
        
        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                int[] curr = queue.poll();
                int cx = curr[0];
                int cy = curr[1];
                if (cx == tx && cy == ty) {
                    return steps;
                }
                for (int[] dir : dirs) {
                    int x = cx + dir[0];
                    int y = cy + dir[1];
                    if (x < 0 || x >= m || y < 0 || y >= n || forest.get(x).get(y) == 0 || visited[x][y]) {
                        continue;
                    }
                    visited[x][y] = true;
                    queue.offer(new int[]{x, y});
                }
            }
            steps++;
        }

        return -1;
    }
}