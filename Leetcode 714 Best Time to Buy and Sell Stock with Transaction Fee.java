/**
Your are given an array of integers prices, for which the i-th element is the price of a given stock on day i; and a non-negative integer fee representing a transaction fee.

You may complete as many transactions as you like, but you need to pay the transaction fee for each transaction. You may not buy more than 1 share of a stock at a time (ie. you must sell the stock share before you buy again.)

Return the maximum profit you can make.

Example 1:
Input: prices = [1, 3, 2, 8, 4, 9], fee = 2
Output: 8
Explanation: The maximum profit can be achieved by:
Buying at prices[0] = 1
Selling at prices[3] = 8
Buying at prices[4] = 4
Selling at prices[5] = 9
The total profit is ((8 - 1) - 2) + ((9 - 4) - 2) = 8.
Note:

0 < prices.length <= 50000.
0 < prices[i] < 50000.
0 <= fee < 50000.
*/
public class Solution {
    public int maxProfit(int[] prices, int fee) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int[][] dp = new int[n][2];
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                dp[i][0] = 0;
                dp[i][1] = -prices[i];
                // or dp[i][1] = - fee - prices[i]; if fee is deduct on buy transaction
                // correspond to dp[i][1] = Math.max(dp[i-1][1], dp[i-1][0] - prices[i] - fee);
                continue;
            }
            // -fee can be either in dp[i][0] or in dp[i][1]
            dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i] - fee);
            dp[i][1] = Math.max(dp[i-1][1], dp[i-1][0] - prices[i]);
        }
        return dp[n-1][0];
    }

    public int maxProfit(int[] prices, int fee) {
        int n = prices.length;
        if (n == 0) {
            return 0;
        }
        int dp_i_0 = 0;
        int dp_i_1 = -prices[0];

        for (int i = 1; i < n; i++) {
            int temp = dp_i_0;
            dp_i_0 = Math.max(dp_i_0, dp_i_1 + prices[i] - fee);
            dp_i_1 = Math.max(dp_i_1, temp - prices[i]);
        }
        return dp_i_0;
    }

    // Solution 2:
    public int maxProfit(int[] prices, int fee) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        int maxDiff = -prices[0];
        int[] dp = new int[n];
        for (int i = 1; i < n; i++) {
            dp[i] = Math.max(dp[i-1], prices[i] + maxDiff - fee);
            maxDiff = Math.max(maxDiff, dp[i-1] - prices[i]);
        }
        return dp[n-1];
    }

    public int maxProfit(int[] prices, int fee) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int maxSold = 0;
        int maxBought = -prices[0] - fee;
        for (int i = 1, n = prices.length; i < n; i++) {
            int sold = maxBought + prices[i];
            int bought = maxSold - prices[i] - fee;
            maxSold = Math.max(maxSold, sold);
            maxBought = Math.max(maxBought, bought);
        }
        return maxSold;
    }
}