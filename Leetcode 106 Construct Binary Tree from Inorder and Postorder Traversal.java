/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
/**
Given inorder and postorder traversal of a tree, construct the binary tree.

Note:
You may assume that duplicates do not exist in the tree.

For example, given

inorder = [9,3,15,20,7]
postorder = [9,15,7,20,3]
Return the following binary tree:

    3
   / \
  9  20
    /  \
   15   7

Note:
You may assume that duplicates do not exist in the tree.

*/

public class Solution {
    /**
    Recursive Solution:
    */ 
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return buildTree(inorder, 0, inorder.length-1, postorder, 0, postorder.length-1);
    }
    
    private TreeNode buildTree(int[] inorder, int inStart, int inEnd, int[] postorder, int postStart, int postEnd) {
        if (postStart > postEnd || inStart > inEnd) {
            return null;
        }
        int rootVal = postorder[postEnd];
        TreeNode root = new TreeNode(rootVal);
        int rootIdx = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }
        int leftSubTreeSize = rootIdx - inStart;
        root.left = buildTree(inorder, inStart, rootIdx-1, postorder, postStart, postStart + leftSubTreeSize - 1);
        root.right = buildTree(inorder, rootIdx + 1, inEnd, postorder, postStart + leftSubTreeSize, postEnd-1);
        return root;
    }

    /** Iterative Solution:
    preorder: node, left, right
    Inorder: left, node, right
    postOrder: left, right, node
    */
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        if (postorder.length == 0 || inorder.length == 0) {
            return null;
        }
        // key -> node's value, value -> inorder index
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, n = inorder.length; i < n; i++) {
            map.put(inorder[i], i);
        }
        
        int m = postorder.length;
        int rootVal = postorder[m-1];
        TreeNode root = new TreeNode(rootVal);
        
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        
        for (int i = m-2; i >= 0; i--) {
            int val = postorder[i];
            TreeNode curr = new TreeNode(val);
            TreeNode prev = stack.peek();
            
            // if new node is on the right of the last node, so it must be its right child.
            if (map.get(val) > map.get(prev.val)) {
                prev.right = curr;
            }
            else {
                /**
                The new node is on the left of the last nnode,
                so it must be the left childd of eith the last node or one of the last node's ancestors.
                pop the stack until we either run out of ancestors or 
                the node at the top of stack is to the right of the new node.
                */
                TreeNode parNode = null;
                while (!stack.isEmpty() && map.get(val) < map.get(stack.peek().val)) {
                    parNode = stack.pop();
                }
                parNode.left = curr;
            }
            stack.push(curr);
        }
        return root;
    }

    // Other solution
    private int inEnd;
    private int postEnd;
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        inEnd = inorder.length - 1;
        postEnd = postorder.length - 1;
        return helper(inorder,postorder,null);
    }
    
    public TreeNode helper(int[] inorder, int[] postorder, TreeNode end){
        if (inEnd < 0 || postEnd < 0) {
            return null;
        }
            
        TreeNode root = new TreeNode(postorder[postEnd]);
        postEnd--;
        if (inorder[inEnd] != root.val) {
            root.right = helper(inorder,postorder,root);
        }
        inEnd--;
        if (end == null || inorder[inEnd] != end.val) {
            root.left = helper(inorder,postorder,end);
        }
            
        return root;
    }
}