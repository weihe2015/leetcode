/**
You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed.
All houses at this place are arranged in a circle. That means the first house is the neighbor of the last one.
Meanwhile, adjacent houses have security system connected and it will automatically contact the police
if two adjacent houses were broken into on the same night.
Given a list of non-negative integers representing the amount of money of each house,
determine the maximum amount of money you can rob tonight without alerting the police.

Example 1:

Input: [2,3,2]
Output: 3
Explanation: You cannot rob house 1 (money = 2) and then rob house 3 (money = 2),
    because they are adjacent houses.
Example 2:

Input: [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
Total amount you can rob = 1 + 3 = 4.
*/

public class Solution {
    // DP solution: Time O(N), Space O(1)
    public int rob(int[] nums) {
        int n = nums.length;
        if (n == 1) {
            return nums[0];
        }
        if (n == 2) {
            return Math.max(nums[0], nums[1]);
        }
        return Math.max(rob(nums, 0, n-2), rob(nums, 1, n-1));
    }
    
    private int rob(int[] nums, int lo, int hi) {
        int n = hi - lo;
        if (n == 1) {
            return nums[lo];
        }
        int dp_0 = nums[lo];
        int dp_1 = Math.max(nums[lo], nums[lo+1]);
        for (int i = lo + 2; i <= hi; i++) {
            int dp_i = Math.max(dp_1, nums[i] + dp_0);
            dp_0 = dp_1;
            dp_1 = dp_i;
        }
        return dp_1;
    }

    // DP Solution: break the problem into two parts: rob from 0 to n-2, or 
    // rob from 1 to n-1, since 0th house == n-1 house.
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        if (n == 1) {
            return nums[0];
        }
        else if (n == 2) {
            return Math.max(nums[0], nums[1]);
        }
        return Math.max(rob(nums, 0, n-2, n), rob(nums, 1, n-1, n));
    }

    private int rob(int[] nums, int l, int r, int n) {
        int[] dp = new int[n];
        dp[l]   = nums[l];
        dp[l+1] = Math.max(nums[l], nums[l+1]);
        for (int i = l+2; i <= r; i++) {
            dp[i] = Math.max(dp[i-1], dp[i-2] + nums[i]);
        }
        return dp[r];
    }

    public int rob(int[] nums) {
        int n = nums.length;
        if (n == 1) {
            return nums[0];
        }
        
        return Math.max(rob(nums,0, n-2), rob(nums,1, n-1));
    }

    public int rob(int[] nums, int lo, int hi){
        int rob = 0, notRob = 0;
        for (int i = lo; i <= hi; i++){
            int temp = notRob;
            notRob = Math.max(rob, notRob);
            rob = nums[i] + temp;
        }
        return Math.max(rob, notRob);
    }

    // dp solution
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        if (n == 1) {
            return nums[0];
        }
        int[] dp0 = new int[n+1], dp1 = new int[n+1];
        dp0[1] = nums[0];
        for (int i = 2; i <= n; i++) {
            dp0[i] = Math.max(dp0[i-1], dp0[i-2]+nums[i-1]);
            dp1[i] = Math.max(dp1[i-1], dp1[i-2]+nums[i-1]);
        }
        return Math.max(dp0[n-1], dp1[n]);
    }
}