/*
According to the Wikipedias article: "The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970."

Given a board with m by n cells, each cell has an initial state live (1) or dead (0). Each cell interacts with its eight neighbors (horizontal, vertical, diagonal) using the following four rules (taken from the above Wikipedia article):

Any live cell with fewer than two live neighbors dies, as if caused by under-population.
Any live cell with two or three live neighbors lives on to the next generation.
Any live cell with more than three live neighbors dies, as if by over-population..
Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
Write a function to compute the next state (after one update) of the board given its current state.

Follow up:
Could you solve it in-place? Remember that the board needs to be updated at the same time: You cannot update some cells first and then use their updated values to update other cells.
In this question, we represent the board using a 2D array. In principle, the board is infinite, which would cause problems when the active area encroaches the border of the array. How would you address these problems?

The idea is to go through set of live cells, increment its neighbor.
And then iterate the set of neighbors to decide who lives and dies.
You can simply calculate the number of number of live neighbors for one cell because of the re-birth situation.

[2nd bit, 1st bit] = [next state, current state]

- (0) 00  dead (current) -> dead (next)
- (1) 01  live (current) -> dead (next)
- (2) 10  dead (current) -> live (next)
- (3) 11  live (current) -> live (next)
*/
public class Solution {
    public void gameOfLife(int[][] board) {
        if (board == null || board.length == 0) {
            return;
        }
        int m = board.length, n = board[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int lives = countLivesFromNeighbors(board, i, j, m, n);
                // Any live cell with fewer than two live neighbors dies, as if caused by under-population.
                if (board[i][j] == 1 && lives < 2) {
                    // 01: live -> dead
                    board[i][j] = 1;
                }
                // Any live cell with two or three live neighbors lives on to the next generation.
                else if (board[i][j] == 1 && (lives == 2 || lives == 3)) {
                    // 11: live -> live
                    board[i][j] = 3;
                }
                // Any live cell with more than three live neighbors dies, as if by over-population
                else if (board[i][j] == 1 && lives > 3) {
                    // 01: live -> dead
                    board[i][j] = 1;
                }
                // Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
                else if (board[i][j] == 0 && lives == 3) {
                    // 10: dead -> live:
                    board[i][j] = 2;
                }
            }
        }
        // move the number by 1 bit:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] >>= 1;
            }
        }
    }

    private int countLivesFromNeighbors(int[][] board, int i, int j, int m, int n) {
        int lives = 0;
        int minX = Math.max(0, i-1);
        int maxX = Math.min(i+1, m-1);
        int minY = Math.max(0, j-1);
        int maxY = Math.min(j+1, n-1);
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                if (x == i && y == j) {
                    continue;
                }
                lives += board[x][y] & 1;
            }
        }
        return lives;
    }
}