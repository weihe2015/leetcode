/**
There are 8 prison cells in a row, and each cell is either occupied or vacant.

Each day, whether the cell is occupied or vacant changes according to the following rules:

If a cell has two adjacent neighbors that are both occupied or both vacant, then the cell becomes occupied.
Otherwise, it becomes vacant.
(Note that because the prison is a row, the first and the last cells in the row can't have two adjacent neighbors.)

We describe the current state of the prison in the following way: cells[i] == 1 if the i-th cell is occupied, else cells[i] == 0.

Given the initial state of the prison, return the state of the prison after N days (and N such changes described above.)

 

Example 1:

Input: cells = [0,1,0,1,1,0,0,1], N = 7
Output: [0,0,1,1,0,0,0,0]
Explanation: 
The following table summarizes the state of the prison on each day:
Day 0: [0, 1, 0, 1, 1, 0, 0, 1]
Day 1: [0, 1, 1, 0, 0, 0, 0, 0]
Day 2: [0, 0, 0, 0, 1, 1, 1, 0]
Day 3: [0, 1, 1, 0, 0, 1, 0, 0]
Day 4: [0, 0, 0, 0, 0, 1, 0, 0]
Day 5: [0, 1, 1, 1, 0, 1, 0, 0]
Day 6: [0, 0, 1, 0, 1, 1, 0, 0]
Day 7: [0, 0, 1, 1, 0, 0, 0, 0]

Example 2:

Input: cells = [1,0,0,1,0,0,1,0], N = 1000000000
Output: [0,0,1,1,1,1,1,0]
 

Note:

cells.length == 8
cells[i] is in {0, 1}
1 <= N <= 10^9
*/
public class Solution {
    /**
    进一步思考后发现，在状态转移的过程中必定会发生循环：
    初态：0,1,0,1,1,0,0,1

    0:  0,1,1,0,0,0,0,0 <== 循环起始
    1:  0,0,0,0,1,1,1,0
    2:  0,1,1,0,0,1,0,0
    3:  0,0,0,0,0,1,0,0
    4:  0,1,1,1,0,1,0,0
    5:  0,0,1,0,1,1,0,0
    6:  0,0,1,1,0,0,0,0
    7:  0,0,0,0,0,1,1,0
    8:  0,1,1,1,0,0,0,0
    9:  0,0,1,0,0,1,1,0
    10: 0,0,1,0,0,0,0,0
    11: 0,0,1,0,1,1,1,0
    12: 0,0,1,1,0,1,0,0
    13: 0,0,0,0,1,1,0,0
    14: 0,1,1,0,0,0,0,0 <== 回归循环起始

    */

    private static final int MAX_LENGTH = 8;
    public int[] prisonAfterNDays(int[] cells, int N) {
        N = (N-1) % 14 + 1;
        for (int i = 0; i < N; i++) {
            cells = move(cells);
        }
        return cells;
    }
    
    private int[] move(int[] cells) {
        int[] res = new int[MAX_LENGTH];
        for (int i = 0; i < MAX_LENGTH-1; i++) {
            if (i > 0 && cells[i-1] == cells[i+1]) {
                res[i] = 1;
            }
        }
        return res;
    }
}