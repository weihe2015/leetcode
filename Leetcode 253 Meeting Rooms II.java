/**
Given an array of meeting time intervals consisting of
start and end times [[s1,e1],[s2,e2],...] (si < ei),
find the minimum number of conference rooms required.

Example 1:

Input: [[0, 30],[5, 10],[15, 20]]
Output: 2
Example 2:

Input: [[7,10],[2,4]]
Output: 1
*/
public class Solution {
	// Running Time Complexity: O(NlogN), N Nodes * (each node takes logN time to insert into Priority Queue)
	public int minMeetingRooms(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return 0;
        }
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        // Min Heap with min end time on the top
        Queue<Integer> pq = new PriorityQueue<Integer>();
        for (Interval interval : intervals) {
			// If there is no overlap, meaning no new room is needed, remove the top one in the heap.
            if (!pq.isEmpty() && pq.peek() <= interval.start) {
                pq.poll();
            }
            pq.offer(interval.end);
        }
        return pq.size();
    }

	// Running Time Complexity: O(NlogN), N Nodes * (each node takes logN time to insert into Priority Queue)
    public int minMeetingRooms(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return 0;
        }
        int n = intervals.size();
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
		// PriorityQueue with min End Time on the top, Min Heap
        Queue<Interval> pq = new PriorityQueue<>(Comparator.comparingInt(l -> l.end));
        /**
        Java 8:
        Queue<Interval> pq = new PriorityQueue<Interval>(n, (i1, i2) -> (i1.end - i2.end));

        Java 7:
        Queue<Interval> pq = new PriorityQueue<Interval>(n, new Comparator<Interval>(){
            public int compare(Interval i1, Interval i2) {
                return i1.end - i2.end;
            }
        })
        */
        for (Interval interval : intervals) {
            if (pq.isEmpty()) {
                pq.offer(interval);
            }
            else {
                Interval prevInterval = pq.poll();
				// no need for a room, merge these two intervals
                if (prevInterval.end <= interval.start) {
                    prevInterval.end = interval.end;
                }
                // need a new room
                else {
                    pq.offer(interval);
                }
                // put origin meeting room back
                pq.offer(prevInterval);
            }
        }
		// The same as solution 1:
		/**
		for (Interval interval : intervals) {
            if (!pq.isEmpty() && pq.peek().end <= interval.start) {
                pq.poll();
            }
            pq.offer(interval);
        }
		*/
        return pq.size();
    }

    public int minMeetingRooms(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return 0;
        }
        Collections.sort(intervals, Comparator.comparingInt(i -> i.start));
        Queue<Integer> pq = new PriorityQueue<>();
        for (Interval curr : intervals) {
            if (pq.isEmpty()) {
                pq.offer(curr.end);
            }
            else {
                int prevEnd = pq.poll();
                if (prevEnd <= curr.start) {
                    prevEnd = curr.end;
                }
                else {
                    pq.offer(curr.end);
                }
                pq.offer(prevEnd);
            }
        }
        return pq.size();
    }

	// Running Time Complexity: O(NlogN), Space Complexity: O(N)
	public int minMeetingRooms(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return 0;
        }
        int n = intervals.size();
        int[] starts = new int[n], ends = new int[n];
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        Arrays.sort(starts);
        Arrays.sort(ends);
        int res = 0, j = 0;
        for (int i = 0; i < n; i++) {
			// If starting time is less than end time, that meeting times are overrlapped, new room is needed.
			// increase the room number
            if (starts[i] < ends[j]) {
                res++;
            }
			// Otherwise, no new room is needed. increase the j pointer
            else {
                j++;
            }
        }
        return res;
    }
}