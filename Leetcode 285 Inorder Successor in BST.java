/*
Given a binary search tree and a node in it, find the in-order successor of that node in the BST.

The successor of a node p is the node with the smallest key greater than p.val.

Input: root = [2,1,3], p = 1
Output: 2
Explanation: 1's in-order successor node is 2. Note that both p and the return value is of TreeNode type.

Input: root = [5,3,6,2,4,null,null,1], p = 6
Output: null
Explanation: There is no in-order successor of the current node, so the answer is null.

*/

public class Solution {
    /*
     * @param root: The root of the BST.
     * @param p: You need find the successor node of p.
     * @return: Successor of p.
     */
    // Inorder traverse the tree, use prev and curr pointer
    // Iterative:
    // Time Complexity: O(N), Space Complexity: O(1)
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // inorder traversal
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = root;
        TreeNode prev = null;
        while (!stack.isEmpty() || curr != null) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if (prev != null && prev == p) {
                return curr;
            }
            prev = curr;
            curr = curr.right;
        }
        return null;
    }

    // Recursive:
    private TreeNode prev = null;
    private TreeNode res = null;
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // write your code here
        inorderTraversal(root, p);
        return res;
    }
    
    private void inorderTraversal(TreeNode root, TreeNode p) {
        if (root == null) {
            return;
        }
        inorderTraversal(root.left, p);
        if (prev != null && prev == p) {
            res = root;
        }
        prev = root;
        inorderTraversal(root.right, p);
    }
}