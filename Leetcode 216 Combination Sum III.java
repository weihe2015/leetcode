/*
Find all possible combinations of k numbers that add up to a number n, given that only numbers from 1 to 9 can be used and each combination should be a unique set of numbers.

Ensure that numbers within the set are sorted in ascending order.


Example 1:

Input: k = 3, n = 7

Output:

[[1,2,4]]

Example 2:

Input: k = 3, n = 9

Output:

[[1,2,6], [1,3,5], [2,3,4]]
*/
public class Solution {
    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        dfs(result, new ArrayList<Integer>(), k, n, 1);
        return result;
    }
    
    public void dfs(List<List<Integer>> result, List<Integer> list, int k, int n, int level){
        if(list.size() == k && n == 0){
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for(int i = level; i <= 9; i++){
            list.add(i);
            dfs(result,list,k,n-i,i+1);
            list.remove(list.size()-1);
        }
    }
    // or decrement k
    public void dfs(List<List<Integer>> result, List<Integer> list, int k, int n, int level){
        if(k == 0 && n == 0){
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for(int i = level; i <= 9; i++){
            list.add(i);
            dfs(result,list,k-1,n-i,i+1);
            list.remove(list.size()-1);
        }
    }

    // Not modify k or n, use level and sum variables.
    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> res = new ArrayList<>();
        if (k < 1 || n < 1) {
            return res;
        }
        int sum = 0;
        int level = 1;
        List<Integer> list = new ArrayList<>();
        dfs(res, list, k, n, 0, 1);
        return res;
    }
    
    private void dfs(List<List<Integer>> res, List<Integer> list, int k, int n, int sum, int level) {
        if (sum > n) {
            return;
        }
        if (sum == n && list.size() == k) {
            res.add(new ArrayList<>(list));
            return;
        }
        
        for (int i = level; i <= 9; i++) {
            list.add(i);
            dfs(res, list, k, n, sum + i, i+1);
            list.remove(list.size() - 1);
        }
    }
}