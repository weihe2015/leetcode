/**

Given a non-empty array of integers, return the third maximum number in this array.
If it does not exist, return the maximum number. The time complexity must be in O(n).

Example 1:
Input: [3, 2, 1]

Output: 1

Explanation: The third maximum is 1.
Example 2:
Input: [1, 2]

Output: 2

Explanation: The third maximum does not exist, so the maximum (2) is returned instead.
Example 3:
Input: [2, 2, 3, 1]

Output: 1

Explanation: Note that the third maximum here means the third maximum distinct number.
Both numbers with value 2 are both considered as second maximum.

*/
public class Solution {
    // Running Time Complexity: O(n * log(3)) = O(n), Space Complexity: O(3) = O(1)
    public int thirdMax(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        // use min heap, the third maximum number should be the min of the priority queue with size 3
        Queue<Integer> pq = new PriorityQueue<>();
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            // check if priority queue contains this number or not, if not, we process:
            if (!set.contains(num)) {
                // if the priority queue is not full, we add the number:
                if (pq.size() < 3) {
                    set.add(num);
                    pq.offer(num);
                }
                else {
                    // If the priority queue is full, we check if the num is greater than the min of the heap,
                    // if it is, we remove the min add the num into the heap
                    if (num > pq.peek()) {
                        int temp = pq.poll();
                        set.remove(temp);
                        set.add(num);
                        pq.offer(num);
                    }
                }
            }
        }
        // third maximum number does not exist, return the maximum number:
        if (pq.size() < 3) {
            while (pq.size() > 1) {
                pq.poll();
            }
        }
        return pq.peek();
    }

    // Two pointers solution:
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int thirdMax(int[] nums) {
        // three pointers solution:
        Integer max1 = null, max2 = null, max3 = null;
        for (Integer num : nums) {
            if (num.equals(max1) || num.equals(max2) || num.equals(max3)) {
                continue;
            }
            if (max1 == null || num > max1) {
                max3 = max2;
                max2 = max1;
                max1 = num;
            }
            else if (max2 == null || num > max2) {
                max3 = max2;
                max2 = num;
            }
            else if (max3 == null || num > max3) {
                max3 = num;
            }
        }
        return max3 != null ? max3 : max1;
    }

    // Simplier Bruth force solution:
    public int thirdMax(int[] nums) {
        // remove duplicates
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            set.add(num);
        }
        // maxHeap
        Queue<Integer> q = new PriorityQueue<>((o1,o2) -> Integer.compare(o2, o1));
        for (int num : set) {
            q.offer(num);
        }

        if (q.size() <= 2) {
            return q.peek();
        }
        q.poll();
        q.poll();
        return q.poll();
    }
}