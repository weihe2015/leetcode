/**
Implement int sqrt(int x).

Compute and return the square root of x, where x is guaranteed to be a non-negative integer.

Since the return type is an integer, the decimal digits are truncated and only the integer part of the result is returned.

Example 1:

Input: 4
Output: 2
Example 2:

Input: 8
Output: 2
Explanation: The square root of 8 is 2.82842..., and since
             the decimal part is truncated, 2 is returned.
*/
public class Solution {
    // Brute Force:
    // Running Time Complexity: O(sqrt(x))
    public int mySqrt(int x) {
        if (x <= 1) {
            return x;
        }
        for (long r = 1; r <= x; r++) {
            // 乘法要比除法要快，但是为了防止溢出，要用64 bits的long
            if (r * r > x) {
                return (int)r-1;
            }
        }
        return -1;
    }
    public int mySqrt(int x) {
        if (x <= 1) {
            return x;
        }
        for (int r = 1; r <= x; r++) {
            if (r > x / r) {
                return r-1;
            }
        }
        return -1;
    }

    // binary search solution
    public int mySqrt(int x) {
        if (x == 0) {
            return x;
        }
        int low = 1, high = x;
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            // You cannot use val = mid * mid here
            // because it will overflow.
            if (mid == x/mid) {
                res = mid;
                break;
            }
            else if (mid > x/mid) {
                high = mid - 1;
            }
            else {
                res = mid;
                low = mid + 1;
            }
        }
        return res;
    }

    // Newton method
    /**
    f(x) = x^2 - a, x = sqrt(a)
    f'(x) = 2x

    x_n+1 = x_n - f(x_n)/f'(x_n)
          = x_n - ((x_n)^2 - a) / 2 * x_n
          = (x_n + (a/x_n))/2;

    r = (r + x/r) / 2;
    */
    public int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }
        // Use long to avoid overflow.
        long r = x;
        while (r > x/r) {
            r = (r + x/r) / 2;
        }
        return (int)r;
    }

    // binary search solution
    public int mySqrt(int x) {
        if (x == 0) {
            return x;
        }
        int low = 1, high = x, res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (mid <= x/mid) {
                res = mid;
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }
}