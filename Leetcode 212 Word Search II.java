/**
 * Given a 2D board and a list of words from the dictionary, find all words in
 * the board.
 *
 * Each word must be constructed from letters of sequentially adjacent cell,
 * where "adjacent" cells are those horizontally or vertically neighboring. The
 * same letter cell may not be used more than once in a word.
 *
 * For example, Given words = ["oath","pea","eat","rain"] and board =
 *
 * [ ['o','a','a','n'], ['e','t','a','e'], ['i','h','k','r'], ['i','f','l','v']
 * ] Return ["eat","oath"].
 */
// Use Trie to search word
public class Solution {
    private static int[][] dirs = {{0,1}, {0,-1}, {1,0}, {-1,0}};
    // Running Time: O(m*n * L)
    // Build Trie:   n * wordMaxLength
    // Search: boardWidth * boardHeight * (4^wordMaxLength + wordMaxLength[Trie Search])
    public List<String> findWords(char[][] board, String[] words) {
        List<String> result = new ArrayList<>();
        if (board == null || board.length == 0) {
            return result;
        }
        TrieNode root = new TrieNode();
        insert(root, words);

        int m = board.length;
        int n = board[0].length;
        boolean[][] visited = new boolean[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                findWords(result, board, root, i, j, m, n, visited);
            }
        }
        return result;
    }

    private void findWords(List<String> result, char[][] board, TrieNode root, int i, int j,
                           int m, int n, boolean[][] visited) {
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j]) {
            return;
        }
        char c = board[i][j];
        if (root.next[c-'a'] == null) {
            return;
        }
        root = root.next[c-'a'];
        if (root.isWord) {
            result.add(root.word);
            // to avoid duplicate result in the result list:
            root.isWord = false;
        }
        visited[i][j] = true;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            findWords(result, board, root, x, y, m, n, visited);
        }
        visited[i][j] = false;
    }

    public void insert(TrieNode root, String[] words) {
        for (String word : words) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    curr.next[c-'a'] = new TrieNode();
                }
                curr = curr.next[c-'a'];
            }
            curr.word = word;
            curr.isWord = true;
        }
    }

    class TrieNode {
        private boolean isWord;
        private String word;
        private TrieNode[] next;

        public TrieNode() {
            this.isWord = false;
            next = new TrieNode[26];
        }
    }
}

    // TLE case, no work for large set. It needs to use Trie as data structure.
    private static final int[][] dirs = { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 } };
    // Runnning time Complexity: (words.length() * m*n * 4k) where k = max (words.length()) in words
    public List<String> findWords2(char[][] board, String[] words) {
        List<String> result = new ArrayList<>();
        if (board == null || board.length == 0) {
            return result;
        }
        for (String word : words) {
            if (exist(board, word) && !result.contains(word)) {
                result.add(word);
            }
        }
        return result;
    }

    private boolean exist(char[][] board, String word) {
        int m = board.length, n = board[0].length;
        char c = word.charAt(0);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (c == board[i][j] && exist(board, word, i, j, m, n, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean exist(char[][] board, String word, int i, int j, int m, int n, int index) {
        if (index == word.length()) {
            return true;
        }
        char c = word.charAt(index);
        if (i < 0 || i >= m || j < 0 || j >= n || c != board[i][j]) {
            return false;
        }
        board[i][j] = '*';
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            boolean result = exist(board, word, x, y, m, n, index + 1);
            if (result) {
                board[i][j] = c;
                return true;
            }
        }
        board[i][j] = c;
        return false;
    }
}