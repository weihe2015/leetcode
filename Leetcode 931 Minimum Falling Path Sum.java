/**
Given a square array of integers A, we want the minimum sum of a falling path through A.

A falling path starts at any element in the first row, and chooses one element from each row.  The next row's choice must be in a column that is different from the previous row's column by at most one.

Example 1:

Input: [[1,2,3],[4,5,6],[7,8,9]]
Output: 12
Explanation: 
The possible falling paths are:
[1,4,7], [1,4,8], [1,5,7], [1,5,8], [1,5,9]
[2,4,7], [2,4,8], [2,5,7], [2,5,8], [2,5,9], [2,6,8], [2,6,9]
[3,5,7], [3,5,8], [3,5,9], [3,6,8], [3,6,9]
The falling path with the smallest sum is [1,4,7], so the answer is 12.
*/
public class Solution {

    /**
    Add a shell outside of original matrix:
    Ex:
    A: 
       1 2 3
       4 5 6
       7 8 9

    DP:
      0 0 0 0 0
      F 1 2 3 F
      F 4 5 6 F
      F 7 8 9 F

    */
    public int minFallingPathSum(int[][] A) {
        int m = A.length;
        int n = A[0].length;
        
        // add two columns to the original matrix:
        int[][] dp = new int[m+1][n+2];
        
        // fill the first and last column of DP matrix with MAX:
        for (int i = 1; i <= m; i++) {
            dp[i][0] = Integer.MAX_VALUE;
            dp[i][n+1] = Integer.MAX_VALUE;
        }
        
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                dp[i][j] = minOfThree(dp[i-1][j-1], dp[i-1][j], dp[i-1][j+1]) + A[i-1][j-1];
            }
        }

        int minVal = Integer.MAX_VALUE;
        for (int j = 1; j <= n; j++) {
            minVal = Math.min(minVal, dp[m][j]);
        }
        return minVal;
    }
    
    private int minOfThree(int val1, int val2, int val3) {
        return Math.min(val1, Math.min(val2, val3));
    }
}