/**
You have an array of logs.  Each log is a space delimited string of words.

For each log, the first word in each log is an alphanumeric identifier.  Then, either:

Each word after the identifier will consist only of lowercase letters, or;
Each word after the identifier will consist only of digits.
We will call these two varieties of logs letter-logs and digit-logs.  It is guaranteed that each log has at least one word after its identifier.

Reorder the logs so that all of the letter-logs come before any digit-log.  The letter-logs are ordered lexicographically ignoring identifier, with the identifier used in case of ties.  The digit-logs should be put in their original order.

Return the final order of the logs.

Example 1:

Input: logs = ["dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"]
Output: ["let1 art can","let3 art zero","let2 own kit dig","dig1 8 1 5 1","dig2 3 6"]
 

Constraints:

0 <= logs.length <= 100
3 <= logs[i].length <= 100
logs[i] is guaranteed to have an identifier, and a word after the identifier.
https://leetcode.com/problems/reorder-data-in-log-files/
*/

class Solution {
    static private final String SPACE = " ";

    public String[] reorderLogFiles(String[] logs) {
        Arrays.sort(logs, (l1, l2) -> {
            String[] datas1 = l1.split(SPACE, 2);
            String[] datas2 = l2.split(SPACE, 2);
            char c1 = datas1[1].charAt(0);
            char c2 = datas2[1].charAt(0);

            if (!Character.isDigit(c1) && !Character.isDigit(c2)) {
                String val1 = datas1[1];
                String val2 = datas2[1];
                if (!val1.equals(val2)) {
                    return val1.compareTo(val2);
                }
                String key1 = datas1[0];
                String key2 = datas2[0];
                return key1.compareTo(key2);
            }
            else if (Character.isDigit(c1) && Character.isDigit(c2)) {
                return 0;
            }
            else if (!Character.isDigit(c1) && Character.isDigit(c2)) {
                return -1;
            }
            else if (Character.isDigit(c1) && !Character.isDigit(c2)) {
                return 1;
            }
            return 0;
        });
        return logs;
    }
}