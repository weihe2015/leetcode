/**
Given an integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.

For example,
Given n = 3,

You should return the following matrix:
[
 [ 1, 2, 3 ],
 [ 8, 9, 4 ],
 [ 7, 6, 5 ]
]
*/
public class Solution {
    // Use the same idea of Leetcode 54 Spiral Matrix
    public int[][] generateMatrix(int n) {
        int[][] matrix = new int[n][n];
        if (n <= 0) {
            return matrix;
        }
        int count = 1;
        int rowBegin = 0, rowEnd = n-1;
        int colBegin = 0, colEnd = n-1;
        while (rowBegin <= rowEnd && colBegin <= colEnd) {
            // top, traverse right
            for (int i = colBegin; i <= colEnd; i++) {
                matrix[rowBegin][i] = count;
                count++;
            }
            rowBegin++;

            // right, traverse down
            for (int i = rowBegin; i <= rowEnd; i++) {
                matrix[i][colEnd] = count;
                count++;
            }
            colEnd--;

            // bottom, traverse left
            if (rowEnd < rowBegin) {
                break;
            }
            for (int i = colEnd; i >= colBegin; i--) {
                matrix[rowEnd][i] = count;
                count++;
            }
            rowEnd--;

            // left, traverse up
            if (colEnd < colBegin) {
                break;
            }
            for (int i = rowEnd; i >= rowBegin; i--) {
                matrix[i][colBegin] = count;
                count++;
            }
            colBegin++;
        }
        return matrix;
    }
}