/**
The API: int read4(char *buf) reads 4 characters at a time from a file.

The return value is the actual number of characters read. For example, it returns 3 if there is only 3 characters left in the file.

By using the read4 API, implement the function int read(char *buf, int n) that reads n characters from the file.

Example 1:

Input: buf = "abc", n = 4
Output: "abc"
Explanation: The actual number of characters read is 3, which is "abc".
Example 2:

Input: buf = "abcde", n = 5 
Output: "abcde"
Note:
The read function will only be called once for each test case.
*/

public class Solution extends Reader4 {
    
    @Test
	public void ReadNCharactersGivenRead4Test1() throws Exception {
		String str = "abc";
		char[] buff = str.toCharArray();
		setContent(buff);
		Assert.assertEquals(3, read(buff, 4));
		
		str = "abcd";
		buff = str.toCharArray();
		setContent(buff);
		Assert.assertEquals(2, read(buff, 2));
		
		str = "abcde";
		buff = str.toCharArray();
		setContent(buff);
		Assert.assertEquals(5, read(buff, 5));
	}

    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    public int read(char[] buf, int n) {
        char[] tmp = new char[4];
        boolean eof = false;
        int readBytes = 0;
        while (readBytes < n && !eof) {
            int currReadBytes = read4(tmp);
            if (currReadBytes < 4) {
                eof = true;
            }
            // 取buf剩下要读的 n-readBytes 和已读的currReadBytes 的较小值
            int len = Math.min(n - readBytes, currReadBytes);
            System.arraycopy(tmp, 0, buf, readBytes, len);
            readBytes += len;
        }
        return readBytes;
    }
}