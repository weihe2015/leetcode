/**
Given a binary tree, find the largest subtree which is a Binary Search Tree (BST), where largest means subtree with largest number of nodes in it.

Note:
A subtree must include all of its descendants.

Example:

Input: [10,5,15,1,8,null,7]

   10 
   / \ 
  5  15 
 / \   \ 
1   8   7

Output: 3
Explanation: The Largest BST Subtree in this case is the highlighted one.
             The return value is the subtree's size, which is 3.

Show Hint 
You can recursively use algorithm similar to 98. Validate Binary Search Tree at each node of the tree, which will result in O(nlogn) time complexity.

Follow up:
Can you figure out ways to solve it with O(n) time complexity?

*/
public class Solution {

    // Solution 1: 遍历一次整个二叉树
    // https://www.cnblogs.com/grandyang/p/5188938.html
    /**
    helper 函数返回的是以当前结点为根结点的数的最小值，最大值，以及最大的 BST 子树的结点个数
    首先判空，若空，则返回一个默认三元组，整型最大值，最小值，和0
    由于 BST 的定义，当前结点值肯定是大于左子树的最大值，小于右子树的最小值的。
    左子树的最大值保存在 left[1] 中，右子树的最小值保存在 right[0] 中
    如果这两个条件满足了，说明左右子树都是 BST，那么返回的三元组的最小值就是当前结点值和左子树最小值中的较小者，最大值就是当前结点值和右子树最大值中的较大值，返回的 BST 结点个数就是左右子树的结点个数加上1，即算上了当前结点。

    如果当前是叶结点，其也算是 BST，那么肯定希望能进入 if 从句，从而使得三元组的第三项能加1，但是 if 的条件是当前结点值要大于左子树中的最大值，现在左子结点是空的，为了保证条件能通过，我们将空的左子树的最大值设置为整型最小值，这样一定能通过，同理，将空的右子树的最小值设置为整型最大值，这就是空结点的三元组的作用。

    
    */
    public int largestBSTSubtree(TreeNode root) {
        int[] res = helper(root);
        return res[2];
    }

    private int[] helper(TreeNode node) {
        if (node == null) {
            return new int[]{Integer.MAX_VALUE, Integer.MIN_VALUE, 0};
        }
        int[] left = helper(node.left);
        int[] right = helper(node.right);

        if (left[1] < node.val && node.val < right[0]) {
            int min = Math.min(node.val, left[0]);
            int max = Math.max(node.val, right[1]);
            int count = left[2] + right[2] + 1;
            return new int[]{min, max, count};
        }

        return new int[]{Integer.MIN_VALUE, Integer.MAX_VALUE, Math.max(left[2], right[2])};
    }

    // Solution 2:
    // 对于每一个节点，都来验证其是否是 BST，如果是的话，就统计节点的个数即可
    public int largestBSTSubtree(TreeNode root) { 
        // validate whether each nodes is a valid BST.
        if (root == null) {
            return 0;
        }
        if (isValidBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            return count(root);
        }
        return Math.max(largestBSTSubtree(root.left), largestBSTSubtree(root.right));
    } 

    private boolean isValidBST(TreeNode node, int min, int max) {
        if (node == null) {
            return true;
        }
        if (node.val <= min || node.val >= max) {
            return false;
        }
        return isValidBST(node.left, min, node.val) && isValidBST(node.right, node.val, max);
    }

    private int count(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return count(node.left) + count(node.right) + 1;
    }
}