public class Solution {
	private class Trie {
		private final static int R = 256;

		private class Node {
			int value = 0;
			Node[] next;

			Node() {
				next = new Node[R];
			}
		}

		private Node root = new Node();
		private int size = 0;

		public void put(String s, int value) {
			put(s, 0, root, value);
		}

		public Node put(String s, int index, Node root, int value) {
			if (root == null) {
				root = new Node();
			}

			if (index == s.length()) {
				if (root.value == 0) {
					size++;
				}
				root.value = value;
				return root;
			}

			char c = s.charAt(index);
			root.next[c] = put(s, index + 1, root.next[c], value);
			return root;
		}

	}

	public int countDistincSubstring(String input) {
		if (input == null || input.length() == 0) {
			return 0;
		}

		int size = input.length();
		int count = 0;
		Trie t = new Trie();
		for (int i = 0; i < size; i++) {
			for (int j = i; j < size; j++) {
				String sub = input.substring(i, j + 1);
				t.put(sub, 1);
			}
		}

		return t.size;
	}

	public static void main(String[] args) {
		Solution sol = new Solution();
		String s = new String("abcd");
		int count = sol.countDistincSubstring(s);
		System.out.println(count);
	}
}