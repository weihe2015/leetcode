    /**
    A 2d grid map of m rows and n columns is initially filled with water. 
    We may perform an addLand operation which turns the water at position (row, col) into a land. 
    Given a list of positions to operate, count the number of islands after each addLand operation. 
    An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. 
    You may assume all four edges of the grid are all surrounded by water.

    Example:

    Given m = 3, n = 3, positions = [[0,0], [0,1], [1,2], [2,1]].
    Initially, the 2d grid grid is filled with water. 
    (Assume 0 represents water and 1 represents land).

    0 0 0
    0 0 0
    0 0 0
    Operation #1: addLand(0, 0) turns the water at grid[0][0] into a land.

    1 0 0
    0 0 0   Number of islands = 1
    0 0 0
    Operation #2: addLand(0, 1) turns the water at grid[0][1] into a land.

    1 1 0
    0 0 0   Number of islands = 1
    0 0 0
    Operation #3: addLand(1, 2) turns the water at grid[1][2] into a land.

    1 1 0
    0 0 1   Number of islands = 2
    0 0 0
    Operation #4: addLand(2, 1) turns the water at grid[2][1] into a land.

    1 1 0
    0 0 1   Number of islands = 3
    0 1 0
    We return the result as an array: [1, 1, 2, 3]

    Challenge:

    Can you do it in time complexity O(k log mn), where k is the length of the positions?
*/
public class Solution {


    // Solution 1, set counter field in UnionFind Class Object:
    // Running Time Complexity: O(k * log m*n), Space Complexity: O(m * n)
    private static final int[][] dirs = {{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
    public List<Integer> numIslands2(int n, int m, Point[] operators) {

        List<Integer> result = new ArrayList<Integer>();
        if (operators == null || operators.length == 0 || n == 0) {
            return result;
        }
        int N = n * m;
        int[][] grid = new int[n][m];
        
        UnionFind uf = new UnionFind(N);
        for (Point operator : operators) {
            int idx = operator.x * m + operator.y;
            if (grid[operator.x][operator.y] == 1) {
                result.add(uf.count);
                continue;
            }
            uf.count++;
            grid[operator.x][operator.y] = 1;
            
            for (int[] dir : dirs) {
                int x = operator.x + dir[0];
                int y = operator.y + dir[1];
                if (isInside(x, y, n, m) && grid[x][y] == 1) {
                    int neighborIdx = x * m + y;
                    uf.union(idx, neighborIdx);
                }  
            }
            result.add(uf.count);
        }
        return result;
    }
    
    private boolean isInside(int i, int j, int n, int m) {
        return (i >= 0 && i < n && j >= 0 && j < m);
    }
    
    class UnionFind {
        int[] id;
        int[] size;
        int count;
        
        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            this.count = 0;
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }
        
        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }
        
        public boolean isSameRoot(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            return pRoot == qRoot;
        }
        
        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            // when two nodes merge together, decrement the number of island by 1
            count--;
        }
    }

    // Solution 2:
    // Running Time Complexity: O(k * log m*n), Space Complexity: O(m * n)
    private static final int[][] dirs = {{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
    public List<Integer> numIslands2(int n, int m, Point[] operators) {
        List<Integer> result = new ArrayList<Integer>();
        if (operators == null || operators.length == 0 || n == 0) {
            return result;
        }
        int N = n * m;
        UnionFind uf = new UnionFind(N);
        for (Point operator : operators) {
            int idx = operator.x * m + operator.y;
            uf.add(idx);
            for (int[] dir : dirs) {
                int x = operator.x + dir[0];
                int y = operator.y + dir[1];
                if (isInside(x, y, n, m)) {
                    int neighborIdx = x * m + y;
                    if (uf.id[neighborIdx] != -1) {
                        uf.union(idx, neighborIdx);
                    }
                }  
            }
            result.add(uf.count);
        }
        return result;
    }
    
    private boolean isInside(int i, int j, int n, int m) {
        return (i >= 0 && i < n && j >= 0 && j < m);
    }
    
    class UnionFind {
        int[] id;
        int[] size;
        int count;
        
        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            this.count = 0;
            Arrays.fill(this.id, -1);
        }
        
        public void add(int idx) {
            // If this idx already has a root, no need to initialize this node.
            if (id[idx] != -1) {
                return;
            }
            id[idx] = idx;
            size[idx] = 1;
            count++;
        }
        
        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }
        
        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            count--;
        }
    }

    // solution 3
    int[][] dirs = {{0, 1}, {1, 0}, {-1, 0}, {0, -1}};
    public List<Integer> numIslands2(int m, int n, int[][] positions) {
        List<Integer> result = new ArrayList<>();
        if(m <= 0 || n <= 0) return result;

        int count = 0;                      // number of islands
        int[] roots = new int[m * n];       // one island = one tree
        Arrays.fill(roots, -1);            

        for(int[] p : positions) {
            int root = n * p[0] + p[1];     // assume new point is isolated island
            roots[root] = root;             // add new island
            count++;

            for(int[] dir : dirs) {
                int x = p[0] + dir[0]; 
                int y = p[1] + dir[1];
                int nb = n * x + y;
                if(x < 0 || x >= m || y < 0 || y >= n || roots[nb] == -1) continue;

                int rootNb = findIsland(roots, nb);
                if(root != rootNb) {        // if neighbor is in another island
                    roots[root] = rootNb;   // union two islands 
                    root = rootNb;          // current tree root = joined tree root
                    count--;               
                }
            }

            result.add(count);
        }
        return result;
    }

    public int findIsland(int[] roots, int id) {
        while (id != roots[id]) {
            id = roots[id];
        }
        return id;
    }
}