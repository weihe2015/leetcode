/*
You are given two linked lists representing two non-negative numbers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
// least significant digit is first node and most significant digit is last node
public class Solution {
    // Running Time Complexity: O(m+n) m: length of LinkedList l1, n: length of LinkedList l2.
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0), curr = head;
        int sum = 0;
        while (l1 != null || l2 != null) {
            if (l1 != null) {
                sum += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                sum += l2.val;
                l2 = l2.next;
            }
            curr.next = new ListNode(sum % 10);
            curr = curr.next;
            sum /= 10;
        }
        if (sum == 1) {
            curr.next = new ListNode(sum);
        }
        return head.next;
    }

    public ListNode addTwoNumber(ListNode l1, ListNode l2){
        if(l1 == null){
            return l2;
        }
        if(l2 == null){
            return l1;
        }
        ListNode head = null;
        Stack<Integer> s1 = new Stack<Integer>();
        Stack<Integer> s2 = new Stack<Integer>();
        Stack<Integer> s3 = new Stack<Integer>();
        ListNode pt = l1;
        while(pt != null){
            s1.push(pt.val);
            pt = pt.next;
        }
        pt = l2;
        while(pt != null){
            s2.push(pt.val);
            pt = pt.next;
        }
        int sum = 0, carry = 0, n1 = 0, n2 = 0;
        while(!s1.isEmpty() && !s2.isEmpty()){
            n1 = s1.pop();
            n2 = s2.pop();
            sum = n1 + n2 + carry;
            carry = sum / 10;
            s3.push(sum%10);
        }
        while(!s1.isEmpty()){
            n1 = s1.pop();
            sum = n1 + carry;
            carry = sum/10;
            s3.push(sum%10);
        }
        while(!s2.isEmpty()){
            n2 = s2.pop();
            sum = n2 + carry;
            carry = sum/10;
            s3.push(sum%10);
        }
        head = creatList(s3);
        return head;
    }
    
    public ListNode creatList(Stack<Integer> s){
        ListNode new_head = new ListNode(0);
        ListNode curr = new_head;
        while(!s.isEmpty()){
            curr.next = new ListNode(s.pop());
            curr = curr.next;
        }
        return new_head.next;
        
    }
}