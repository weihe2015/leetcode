/**
Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.

Basically, the deletion can be divided into two stages:

Search for a node to remove.
If the node is found, delete the node.
Note: Time complexity should be O(height of tree).
Example:
root = [5,3,6,2,4,null,7]
key = 3
    5
   / \
  3   6
 / \   \
2   4   7
Given key to delete is 3. So we find the node with value 3 and delete it.
One valid answer is [5,4,6,2,null,null,7], shown in the following BST.

    5
   / \
  4   6
 /     \
2       7
Another valid answer is [5,2,6,null,4,null,7].
    5
   / \
  2   6
   \   \
    4   7
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
 // Video Tutorial: https://www.youtube.com/watch?v=gcULXE7ViZw&vl=en
class Solution {
    public TreeNode deleteNode(TreeNode root, int key) {
        TreeNode curr = root, prev = null;
        while (curr != null) {
            if (key < curr.val) {
                prev = curr;
                curr = curr.left;
            }
            else if (key > curr.val) {
                prev = curr;
                curr = curr.right;
            }
            else {
                break;
            }
        }
        // If node to be deleted is not found:
        if (curr == null) {
            return root;
        }
        // If the node to be deleted is the root:
        if (prev == null) {
            if (curr.left == null && curr.right == null) {
                root = null;
            }
            else if (curr.left != null && curr.right != null) {
                deleteNodeWithTwoChildren(curr);
            }
            else {
                if (curr.left != null) {
                    root = curr.left;
                    curr.left = null;
                }
                else {
                    root = curr.right;
                    curr.right = null;
                }
            }
        }
        else {
            // If the node to be deleted is a leaf:
            if (curr.left == null && curr.right == null) {
                // curr node is the right node:
                if (prev.val < curr.val) {
                    prev.right = null;
                }
                // curr node is the left node:
                else {
                    prev.left = null;
                }
            }
            else if (curr.left != null && curr.right != null) {
                deleteNodeWithTwoChildren(curr);
            }
            // Delete Node with One Child
            else if (curr.left != null){
                if (prev.val < curr.val) {
                    prev.right = curr.left;
                }
                else {
                    prev.left = curr.left;
                }
            }
            else {
                if (prev.val < curr.val) {
                    prev.right = curr.right;
                }
                else {
                    prev.left = curr.right;
                }
            }
        }
        return root;
    }
    
    private void deleteNodeWithTwoChildren(TreeNode curr) {
        if (curr == null || curr.left == null || curr.right == null) {
            return;
        }
        // Either find the maxNode on the left subtree or the minNode in the right subtree.
        if (true) {
            TreeNode minNode = findMinNode(curr.right);
            curr.right = deleteNode(curr.right, minNode.val);
            curr.val = minNode.val;
        }
        else {
            TreeNode maxNode = findMaxNode(curr.left);
            curr.left = deleteNode(curr.left, maxNode.val); 
            curr.val = maxNode.val;
        }
    }
    
    private TreeNode findMinNode(TreeNode node) {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }

    private TreeNode findMaxNode(TreeNode node) {
        while (node.right != null) {
            node = node.right;
        }
        return node;
    }

    // Recursive:
    public TreeNode deleteNode(TreeNode root, int key) {
        if (root == null) {
            return null;
        }
        if (key < root.val) {
            root.left = deleteNode(root.left, key);
        }
        else if (key > root.val) {
            root.right = deleteNode(root.right, key);
        }
        else {
            if (root.left == null && root.right == null) {
                return null;
            }
            else if (root.left != null && root.right != null) {
                TreeNode minNode = findMinNode(root.right);
                root.right = deleteNode(root.right, minNode.val);
                root.val = minNode.val;
            }
            else if (root.left != null) {
                TreeNode prev = root;
                root = root.left;
                prev.left = null;
            }
            else {
                TreeNode prev = root;
                root = root.right;
                prev.right = null;
            }
        }
        return root;
    }
    
    private TreeNode findMinNode(TreeNode node) {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }
}