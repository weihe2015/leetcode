/**
Given a collection of numbers that might contain duplicates, return all possible unique permutations.

For example,
[1,1,2] have the following unique permutations:
[1,1,2], [1,2,1], and [2,1,1].
*/
public class Solution {
    // Backtracking Solution: Running Time Complexity: O(N!), Space Complexity: O(N)
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if(nums == null || nums.length == 0) {
            return result;
        }
        boolean[] usedList = new boolean[nums.length];
        Arrays.sort(nums);
        generatePermutationUnique(result,new ArrayList<Integer>(),nums,usedList);
        return result;
    }
    
    public void generatePermutationUnique(List<List<Integer>> result, List<Integer> list, int[] nums, boolean[] usedList){
        if(list.size() == nums.length){
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for(int i = 0; i < nums.length; i++){
            if(usedList[i] || (i > 0 && nums[i-1] == nums[i] && !usedList[i-1])) // used[i-1] because [0,0,3,3], when i = 1, without it, it will skip this element.
                continue;
            used[i] = true;
            list.add(nums[i]);
            generatePermutationUnique(result,list,nums,usedList);
            list.remove(list.size()-1);
            used[i] = false;
        }
    }

    // 11-15-17:
    // Running time: O(N!), Space Complexity: O(n)
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0)
            return result;
        boolean[] used = new boolean[nums.length];
        Set<List<Integer>> set = new HashSet<>();
        helper(set, nums, new ArrayList<Integer>(), used);
        for (List<Integer> list : set) {
            result.add(list);
        }
        return result;
    }
    
    private void helper(Set<List<Integer>> set, int[] nums, ArrayList<Integer> list, boolean[] used) {
        if (list.size() == nums.length) {
            set.add(new ArrayList<Integer>(list));
            return;
        }
        
        for (int i = 0, max = nums.length; i < max; ++i) {
            if (used[i]) {
                continue;
            }
            used[i] = true;
            list.add(nums[i]);
            helper(set, nums, list, used);
            used[i] = false;
            list.remove(list.size()-1);
        }
    }

    // use set solution, solution 2
    // space complexity O(n)
    public List<List<Integer>> permuteUnique(int[] nums) {
        Set<List<Integer>> set = new HashSet<>();
        dfs(set,nums,new ArrayList<Integer>(),0);
        return new ArrayList(set);
    }
    public void dfs(Set<List<Integer>> set, int[] nums, ArrayList<Integer> list, int index){
        if(list.size() == nums.length){
            set.add(new ArrayList<Integer>(list));
            return;
        }
        for(int i = 0; i <= index; i++){
            list.add(i,nums[index]);
            dfs(set,nums,list,index+1);
            list.remove(i);
        }
    }

    // Solution 3, Iterative
    /**
     * Basic Idea: Same as 046 Permutation. But here we need to use Set instead of List to remove duplicate,
     *  num = [1,2,3]
     *  [[2,1],[1,2]] i = 2, num = 3
        for each subList,  make result.size()+1 of itself and add num into 0-i index.:
        copy: [[2,1],[2,1],[2,1],[1,2][1,2],[1,2]]
        add: [[3,2,1], [2,3,1], [2,1,3], [3,1,2], [1,3,2], [1,2,3]]
       Running time: O(N^4)
    */
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        List<Integer> list = new ArrayList<Integer>();
        list.add(nums[0]);
        result.add(list);
        for (int i = 1, max = nums.length; i < max; i++) {
            int val = nums[i];
            Set<List<Integer>> set = new HashSet<>();
            // for each subList:
            for (int j = 0, max_j = result.size(); j < max_j; j++) {
                // make copy of previous item for n times, n = size + 1
                // and insert num from 0 to i index
                for (int k = 0; k <= i; k++) {
                    list = new ArrayList<Integer>(result.get(j));
                    list.add(k, val);
                    set.add(list);
                }
            }
            result = new ArrayList<>(set);
        }
        return result;
    }

    // solution 4
    // use the solution of next permutation
    // O(nlogn) time, O(1) space
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if(nums == null || nums.length == 0)
            return result;
        Arrays.sort(nums);
        add(result,nums);
        while(nextPermutation(nums)){
            add(result,nums);
        }
        return result;
    }
    
    public void add(List<List<Integer>> result, int[] nums){
        List<Integer> list = new ArrayList<>();
        for(int num : nums){
            list.add(num);
        }
        result.add(list);
    }
    
    public boolean nextPermutation(int[] nums){
        int k = -1, n = nums.length;
        for(int i = n-2; i >= 0; i--){
            if(nums[i] < nums[i+1]){
                k = i;
                break;
            }
        }
        if(k == -1){
            reverse(nums,0,n-1);
            return false;
        }
        int l = -1;
        for(int i = n-1; i > k; i--){
            if(nums[k] < nums[i]){
                l = i;
                break;
            }
        }
        swap(nums,k,l);
        reverse(nums,k+1,n-1);
        return true;
    }
    
    public void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
    
    public void reverse(int[] nums, int i, int j){
        while(i < j){
            swap(nums,i,j);
            i++; j--;
        }
    }
}