/*
A message containing letters from A-Z is being encoded to numbers using the following mapping:

'A' -> 1
'B' -> 2
...
'Z' -> 26
Given a non-empty string containing only digits, determine the total number of ways to decode it.

Example 1:

Input: "12"
Output: 2
Explanation: It could be decoded as "AB" (1 2) or "L" (12).
Example 2:

Input: "226"
Output: 3
Explanation: It could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF" (2 2 6).
*/

public class Solution {
    // Running Time Complexity: O(len(s)), Space Complexity: O(len(s))
    public int numDecodings(String s) {
        int n = s.length();
        if (n == 0) {
            return 0;
        }
        char[] cList = s.toCharArray();
        // 以 s[i] 结尾的前缀子串有多少种解码方法。
        int[] dp = new int[n+1];
        dp[0] = 1;
        dp[1] = (cList[0] - '0') == 0 ? 0 : 1;
        for (int i = 2; i <= n; i++) {
            // one digit number:
            int n1 = cList[i-1] - '0';
            // two digits number
            int n2 = n1 + 10 * (cList[i-2] - '0');
            if (n1 >= 1 && n1 <= 9) {
                dp[i] += dp[i-1];
            }
            if (n2 >= 10 && n2 <= 26) {
                dp[i] += dp[i-2];
            }
        }
        return dp[n];  
    }
}