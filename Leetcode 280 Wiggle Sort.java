/** 
Given an unsorted array nums, reorder it in-place such that nums[0] <= nums[1] >= nums[2] <= nums[3]....

For example, given nums = [3, 5, 2, 1, 6, 4], one possible answer is [1, 6, 2, 5, 3, 4].
https://www.cnblogs.com/grandyang/p/5177285.html
https://www.lintcode.com/problem/wiggle-sort/description
*/
public class Solution {
	/** 
	思路是先给数组排个序，然后我们只要每次把第三个数和第二个数调换个位置，第五个数和第四个数调换个位置，以此类推直至数组末尾
	*/
    // Running Time Complexity: O(nlogn), Space Complexity: O(1)
    public void wiggleSort(int[] nums) {
        // sort original array:
        Arrays.sort(nums);
        // swap two elements from second elements:
        int n = nums.length;
        if (n <= 2) {
            return;
        }
        for (int i = 2; i < n; i+=2) {
            swap(nums, i, i-1);
        }
    }

	// Solution 2:
	// Running Time Complexity:	O(n), Space Complexity: O(1)
	/** 
	这道题还有一种O(n)的解法，根据题目要求的nums[0] <= nums[1] >= nums[2] <= nums[3]....，我们可以总结出如下规律：

	当i为奇数时，nums[i] >= nums[i - 1]

	当i为偶数时，nums[i] <= nums[i - 1]
	*/
    public void wiggleSort(int[] nums) {
        int n = nums.length;
        if (n <= 1) {
            return;
        }
        for (int i = 1; i < n; i++) {
            // if it is odd position and it is less than previous number, swap it:
            if (i % 2 == 1) {
                if (nums[i] < nums[i-1]) {
                    swap(nums, i, i-1);
                }
            }
            // if it is even position and it is greater than previous number, swap it:
            else {
                if (nums[i] > nums[i-1]) {
                    swap(nums, i, i-1);
                }
            }
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}