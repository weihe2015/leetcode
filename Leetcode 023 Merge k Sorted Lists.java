/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    // O(n*log K) time O(n) space
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        Queue<ListNode> pq = new PriorityQueue<>(Comparator.comparingInt(l -> l.val));
        // Queue<ListNode> pq = new PriorityQueue<>(new NodeComparator());
        for (ListNode subList : lists) {
            if (subList != null) {
                pq.offer(subList);
            }
        }

        ListNode dummyHead = new ListNode(-1);
        ListNode curr = dummyHead;

        while (!pq.isEmpty()) {
            ListNode subList = pq.poll();
            curr.next = subList;
            curr = curr.next;
            // If this header of the list still have next val, we add this ListNode into the PriorityQueue
            if (subList.next != null) {
                pq.offer(subList.next);
            }
        }

        return dummyHead.next;
    }

    public class NodeComparator implements Comparator<ListNode>{
        @Override
        public int compare(ListNode l1, ListNode l2){
            return l1.val - l2.val;
        }
    }

    // merge 2 sorted lists O(n*k*logk)
    public ListNode mergeKLists(ListNode[] lists) {
        int n = lists.length;
        if (n == 0) {
            return null;
        }
        while (n > 1){
            for (int i = 0; i < n/2; i++) {
                lists[i] = mergeTwoLists(lists[i],lists[n-i-1]);
            }
            n = (n + 1) / 2;
        }
        return lists[0];
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2){
        if (l1 == null) {
            return l2;
        }
        else if (l2 == null) {
            return l1;
        }

        ListNode head = null;
        if (l1.val < l2.val){
            head = l1;
            l1 = l1.next;
        }
        else {
            head = l2;
            l2 = l2.next;
        }

        ListNode curr = head;
        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val){
                curr.next = l1;
                l1 = l1.next;
            }
            else {
                curr.next = l2;
                l2 = l2.next;
            }
            curr = curr.next;
        }
        curr.next = (l1 != null) ? l1 : l2;
        return head;
    }

    // merge sort solution 3
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        return mergeSort(lists, 0, lists.length-1);
    }

    private ListNode mergeSort(ListNode[] lists, int low, int high) {
        if (low >= high) {
            return lists[low];
        }
        int mid = low + (high - low) / 2;
        ListNode l1 = mergeSort(lists, low, mid);
        ListNode l2 = mergeSort(lists, mid+1, high);
        return mergeTwoList(l1, l2);
    }
}