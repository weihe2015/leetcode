/**
Given a string representing an expression of fraction addition and subtraction, 
you need to return the calculation result in string format. The final result should be irreducible fraction. 
If your final result is an integer, say 2, you need to change it to the format of fraction that has denominator 1.
So in this case, 2 should be converted to 2/1.

Example 1:
Input:"-1/2+1/2"
Output: "0/1"

Example 2:
Input:"-1/2+1/2+1/3"
Output: "1/3"

Example 3:
Input:"1/3-1/2"
Output: "-1/6"

Example 4:
Input:"5/3+1/3"
Output: "2/1"

Note:
The input string only contains '0' to '9', '/', '+' and '-'. So does the output.
Each fraction (input and output) has format ±numerator/denominator. If the first input fraction or the output is positive, then '+' will be omitted.
The input only contains valid irreducible fractions, where the numerator and denominator of each fraction will always be in the range [1,10]. If the denominator is 1, it means this fraction is actually an integer in a fraction format defined above.
The number of given fractions will be in the range [1,10].
The numerator and denominator of the final result are guaranteed to be valid and in the range of 32-bit int.
*/

public class Solution {
    // https://leetcode-cn.com/problems/fraction-addition-and-subtraction/solution/fen-shu-jia-jian-yun-suan-by-leetcode/
    // Running Time Complexity: O(NlogC), where N is the number of fractions in string, logC is the time complexity of GCD. 
    public String fractionAddition(String expression) {
        List<Character> signs = new ArrayList<>();
        // start from 1 to ignore if the first number is negative number
        for (int i = 1, n = expression.length(); i < n; i++) {
            char c = expression.charAt(i);
            if (c == '-' || c == '+') {
                signs.add(c);
            }
        }
        
        List<Integer> numerators = new ArrayList<>();
        List<Integer> denumerators = new ArrayList<>();
        
        // split the expression by +:
        String[] texts = expression.split("\\+");
        for (String text : texts) {
            String[] subTexts = text.split("-");
            for (String subText : subTexts) {
                if (subText.length() == 0) {
                    continue;
                }
                String[] factions = subText.split("/");
                numerators.add(Integer.parseInt(factions[0]));
                denumerators.add(Integer.parseInt(factions[1]));
            }
        }
        
        // check if the first number is negative:
        if (expression.startsWith("-")) {
            int num = numerators.get(0);
            numerators.set(0, -num);
        }
        
        int l = 1;
        // get the least common multipliers of denumerators:
        for (int denumerator : denumerators) {
            l = lcm(l, denumerator);
        }
        
        int res = l / denumerators.get(0) * numerators.get(0);
        for (int i = 1, n = numerators.size(); i < n; i++) {
            char sign = signs.get(i-1);
            if (sign == '+') {
                res += l / denumerators.get(i) * numerators.get(i);
            }
            else {
                res -= l / denumerators.get(i) * numerators.get(i);
            }
        }
        // in case that the res is 8/4:
        int g = gcd(Math.abs(res), Math.abs(l));
        
        return (res / g) + "/" + (l / g);
    }
    
    // Least Common Multiplers
    private int lcm(int m, int n) {
        return m * n / gcd(m, n);
    }
    
    // Greatest Common Divisor
    private int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }
    
    /*
    // Greatest Common Divisor
    private int gcd(int num1, int num2) {
        while (num2 > 0) {
            int temp = num2;
            num2 = num1 % num2;
            num1 = temp;
        }
        return num1;
    }
    */
}