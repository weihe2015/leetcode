/** 
Given a positive integer n, find the least number of perfect square numbers 
(for example, 1, 4, 9, 16, ...) which sum to n.

Example 1:

Input: n = 12
Output: 3 
Explanation: 12 = 4 + 4 + 4.
Example 2:

Input: n = 13
Output: 2
Explanation: 13 = 4 + 9.
*/

public class Solution {
    // 
    public int numSquares(int n) {
        int[] dp = new int[n+1];
        // dp[i]: min perfect number to form this number i
        Arrays.fill(dp, n+1);
        dp[0] = 0;
        for (int i = 1; i <= n; i++) {
            // For each number from 1 to n, it must be the sum of some number i - j * j
            // and a perfect square number j * j
            // 状态转移：组成这个数要不就是本身，要么就是前面某一个数 + 一个平方数
            // 从1 开始数到i，所以就是i - j*j 
            for (int j = 1; j * j <= i; j++) {
                dp[i] = Math.min(dp[i], dp[i-j*j] + 1);
            }
        }
        return dp[n];
    }
}