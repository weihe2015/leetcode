/**
Given an array of n integers where n > 1, nums, return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].

Solve it without division and in O(n).

For example, given [1,2,3,4], return [24,12,8,6].

Numbers:  2     3    4    5
lefts     1     2    2*3  2*3*4
rights:   3*4*5 4*5  5    1
*/

public class Solution {
    // O(n^2) Running time complexity
    public int[] productExceptSelf(int[] nums) {
        if (nums == null || nums.length == 0) {
            return null;
        }
        int n = nums.length;
        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            int product = 1;
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    product *= nums[j];
                }
            }
            res[i] = product;
        }
        return res;
    }

    // O(n) Running time complexity, O(n) Space complexity
    public int[] productExceptSelf(int[] nums) {
        if (nums == null || nums.length == 0) {
            return null;
        }
        int n = nums.length;
        int[] A = new int[n];
        int[] B = new int[n];
        int[] res = new int[n];
        A[0] = 1; B[n-1] = 1;
        for (int i = 1; i < n; i++) {
            A[i] = A[i-1] * nums[i-1];
            B[n-i-1] = B[n-i] * nums[n-i];
        }

        // for (int i = n-2; i >= 0; i--) {
        //     B[i] = B[i+1] * nums[i+1];
        // }

        for (int i = 0; i < n; i++) {
            res[i] = A[i] * B[i];
        }
        return res;
    }

    // O(n) Running time complexity, O(1) Space complexity
    // https://www.youtube.com/watch?v=vB-81TB6GUc
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int[] res = new int[n];
        Arrays.fill(res, 1);
        int left = 1;
        for (int i = 0; i < n; i++) {
            res[i] *= left;
            left *= nums[i];
        }
        int right = 1;
        for (int i = n-1; i >= 0; i--) {
            res[i] *= right;
            right *= nums[i];
        }
        return res;
    }

    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int[] result = new int[n];
        result[0] = 1;
        for(int i = 1; i < n; i++){
            result[i] = result[i-1] * nums[i-1];
        }
        int val = 1;
        for(int i = n-1; i >= 0; i--){
            result[i] *= val;
            val *= nums[i];
        }
        return result;
    }
}