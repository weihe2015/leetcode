/**
You are given a map in form of a two-dimensional integer grid where 1 represents land and 0 represents water. 
Grid cells are connected horizontally/vertically (not diagonally). 
The grid is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells). 
The island doesn't have "lakes" (water inside that isn't connected to the water around the island). 
One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100. 

Determine the perimeter of the island.

Example:

[[0,1,0,0],
 [1,1,1,0],
 [0,1,0,0],
 [1,1,0,0]]

Answer: 16
Explanation: The perimeter is the 16 yellow stripes in the image below:
*/
public class Solution {
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int islandPerimeter(int[][] grid) {
        int len = 0;
        if (grid == null || grid.length == 0) {
            return len;
        }
        int m = grid.length, n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    len += dfs(grid, i, j, m, n, visited);
                }
            }
        }
        return len;
    }
    
    private int dfs(int[][] grid, int i, int j, int m, int n, boolean[][] visited) {
        // 遇到水，就加一条边
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 0) {
            return 1;
        }
        // 如果当前cell已经被访问过了，那就返回 0
        if (visited[i][j]) {
            return 0;
        }
        visited[i][j] = true;
        int count = 0;
        for (int[] dir : dirs) {
            int x = dir[0] + i;
            int y = dir[1] + j;
            count += dfs(grid, x, y, m, n, visited);
        }
        return count;
    }
}