/**
Given an n-ary tree, return the postorder traversal of its nodes' values.

 
For example, given a 3-ary tree:
        1
   3      2     4
5    6

Return its postorder traversal as: [5,6,3,2,4,1].
*/
/*
// Definition for a Node.
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val,List<Node> _children) {
        val = _val;
        children = _children;
    }
};
*/
public class Solution {
    public List<Integer> postorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        postorder(result, root);
        return result;
    }
    
    private void postorder(List<Integer> result, Node node) {
        if (node == null) {
            return;
        }
        for (Node childNode : node.children) {
            postorder(result, childNode);
        }
        result.add(node.val);
    }

    // Iterative:
    public List<Integer> postorder(Node root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<Node>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node currNode = stack.pop();
            result.add(currNode.val);
            for (Node childNode : currNode.children) {
                stack.push(childNode);
            }
        }
        Collections.reverse(result);
        return result;
    }

    // Iterative:
    public List<Integer> postorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        Stack<Node> stack = new Stack<Node>();
        Node node = root;
        Set<Node> visitedNodes = new HashSet<Node>();
        while (!stack.isEmpty() || node != null) {
            while(node != null) {
                stack.push(node);
                if (!node.children.isEmpty()) {
                    node = node.children.get(0);
                }
                else {
                    node = null;
                    break;
                }
            }
            Node currNode = stack.peek();
            if (!currNode.children.isEmpty()) {
                Node nextNode = null;
                for (Node childNode : currNode.children) {
                    if (!visitedNodes.contains(childNode)) {
                        nextNode = childNode;
                        break;
                    }
                }
                // If all children have been visited, add this parent node into list
                if (nextNode == null) {
                    currNode = stack.pop();
                    result.add(currNode.val);
                    visitedNodes.add(currNode);
                }
                node = nextNode;
            }
            else {
                currNode = stack.pop();
                result.add(currNode.val);
                visitedNodes.add(currNode);
            }
        }
        return result;
    }
}