/**
Given a non-empty string s and an abbreviation abbr, return whether the string matches with the given abbreviation.

A string such as "word" contains only the following valid abbreviations:

["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
Notice that only the above abbreviations are valid abbreviations of the string "word". Any other string is not a valid abbreviation of "word".

Note:
Assume s contains only lowercase letters and abbr contains only lowercase letters and digits.

Example 1:
Given s = "internationalization", abbr = "i12iz4n":

Return true.
Example 2:
Given s = "apple", abbr = "a2e":

Return false.
*/
public class Solution {
    /**
     * @param word: a non-empty string
     * @param abbr: an abbreviation
     * @return: true if string matches with the given abbr or false
     */
    public boolean validWordAbbreviation(String word, String abbr) {
        int m = word.length(), n = abbr.length();
        int i = 0, j = 0;
        while (i < m && j < n) {
            char c1 = word.charAt(i);
            char c2 = abbr.charAt(j);
            if (Character.isLetter(c2)) {
                if (c1 != c2) {
                    return false;
                }
                else {
                    i++; j++;
                }
            }
            else {
                // Skip leading 0:
                while (j < n && abbr.charAt(j) == '0') {
                    i++; j++;
                }
                int num = 0;
                while (j < n && !Character.isLetter(abbr.charAt(j))) {
                    num = 10 * num + (abbr.charAt(j) - '0');
                    j++;
                }
                if (num > 0) {
                    i += num;
                }
            }
        }
        return i == m && j == n;
    }
}