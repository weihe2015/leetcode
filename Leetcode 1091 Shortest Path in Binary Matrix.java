/*
In an N by N square grid, each cell is either empty (0) or blocked (1).

A clear path from top-left to bottom-right has length k if and only if it is composed of cells C_1, C_2, ..., C_k such that:

Adjacent cells C_i and C_{i+1} are connected 8-directionally (ie., they are different and share an edge or corner)
C_1 is at location (0, 0) (ie. has value grid[0][0])
C_k is at location (N-1, N-1) (ie. has value grid[N-1][N-1])
If C_i is located at (r, c), then grid[r][c] is empty (ie. grid[r][c] == 0).
Return the length of the shortest such clear path from top-left to bottom-right.  If such a path does not exist, return -1.

Ex: [[0,1],[1,0]]
Output: 2

Ex: [[0,0,0],[1,1,0],[1,1,0]]
Output: 4

Note:

1 <= grid.length == grid[0].length <= 100
grid[r][c] is 0 or 1
*/
class Solution {
    static private int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0},{1,1},{1,-1},{-1,1},{-1,-1}};
    public int shortestPathBinaryMatrix(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        if (grid[0][0] == 1) {
            return -1;
        }
        boolean[][] visited = new boolean[m][n];
        int[] start = new int[]{0,0};
        int[] end   = new int[]{m-1, n-1};
        visited[0][0] = true;
        
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{0,0});

        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] curr = queue.poll();
                int i = curr[0];
                int j = curr[1];
                if (i == end[0] && j == end[1]) {
                    return steps + 1;
                }
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] == 1 || visited[x][y]) {
                        continue;
                    }
                    visited[x][y] = true;
                    queue.offer(new int[]{x, y});
                }
            }
            steps++;
        }
        return -1;
    }
}