/**
Implement a MapSum class with insert, and sum methods.

For the method insert, you'll be given a pair of (string, integer).
The string represents the key and the integer represents the value.
If the key already existed, then the original key-value pair will be overridden to the new one.

For the method sum, you'll be given a string representing the prefix,
and you need to return the sum of all the pairs' value whose key starts with the prefix.

Example 1:
Input: insert("apple", 3), Output: Null
Input: sum("ap"), Output: 3
Input: insert("app", 2), Output: Null
Input: sum("ap"), Output: 5

*/
class MapSum {
    private TrieNode root;
    private Map<String, Integer> map;
    /** Initialize your data structure here. */
    public MapSum() {
        this.root = new TrieNode();
        this.map = new HashMap<>();
    }

    public void insert(String key, int val) {
        TrieNode curr = this.root;
        if (!this.map.containsKey(key)) {
            for (char c : key.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    curr.next[c-'a'] = new TrieNode();
                    curr.next[c-'a'].sum = val;
                }
                else {
                    curr.next[c-'a'].sum += val;
                }
                curr = curr.next[c-'a'];
            }
        }
        else {
            int prevVal = map.get(key);
            for (char c : key.toCharArray()) {
                curr = curr.next[c-'a'];
                curr.sum = curr.sum - prevVal + val;
            }
        }
        this.map.put(key, val);
    }

    public int sum(String prefix) {
        TrieNode curr = this.root;
        for (char c : prefix.toCharArray()) {
            if (curr.next[c-'a'] == null) {
                return 0;
            }
            curr = curr.next[c-'a'];
        }
        return curr.sum;
    }

    class TrieNode {
        int sum;
        TrieNode[] next;
        public TrieNode() {
            this.sum = 0;
            this.next = new TrieNode[26];
        }
    }
}

/**
 * Your MapSum object will be instantiated and called as such:
 * MapSum obj = new MapSum();
 * obj.insert(key,val);
 * int param_2 = obj.sum(prefix);
 */