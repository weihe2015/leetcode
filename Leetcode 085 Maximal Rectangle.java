/**
Given a 2D binary matrix filled with 0's and 1's, find the largest rectangle containing only 1's and return its area.

Example:

Input:
[
  ["1","0","1","0","0"],
  ["1","0","1","1","1"],
  ["1","1","1","1","1"],
  ["1","0","0","1","0"]
]
Output: 6
*/
public class Solution {
    // Running Time Complexity: O(m * 4n), Space Complexity: O(3n)
    public int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        // int[] left: the start column index of continuous '1' block
        // int[] right: the end column index of continuous '1' block
        // int[] height: the height of continuous '1' block
        int[] left = new int[n];
        int[] right = new int[n];
        int[] height = new int[n];
        Arrays.fill(right, n-1);
        int maxArea = 0;
        for (int i = 0; i < m; i++) {
            int currLeft = 0, currRight = n-1;
            // compute height:
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    height[j]++;
                }
                else {
                    height[j] = 0;
                }
            }
            // compute left boundary:
            // If 1 is continuous, then left[j] is currLeft.
            // If matrix[i][j] == '0',  then left[j] will be the max of currLeft idx, or previous left[j],
            // to make sure that left boundary shrink to current place.
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    left[j] = Math.max(left[j], currLeft);
                }
                else {
                    left[j] = 0;
                    // set currLeft to index of next item
                    currLeft = j+1;
                }
            }
            // compute right boundary:
            // If 1 is continuous from right to left, then right[j] is always the right most index
            // If matrix[i][j] == '0', then right[j] will be the min of currRight idx, or previous right[j]
            for (int j = n-1; j >= 0; j--) {
                if (matrix[i][j] == '1') {
                    right[j] = Math.min(right[j], currRight);
                }
                else {
                    right[j] = n-1;
                    // set currLeft to index of next item
                    currRight = j-1;
                }
            }
            // compute the area:
            for (int j = 0; j < n; j++) {
                int currArea = (right[j] - left[j] + 1) * height[j];
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }

    // Solution 2: for each row, calculate the heights,
    // and use the solution of Leetcode 84: Largest Rectangle in Histogram:
    public int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[] heights = new int[n];
        int maxArea = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    heights[j]++;
                }
                else {
                    heights[j] = 0;
                }
            }
            int currArea = largestRectangleArea(heights);
            maxArea = Math.max(maxArea, currArea);
        }
        return maxArea;
    }

    public int largestRectangleArea(int[] heights) {
        int maxArea = 0, max = heights.length;
        Deque<Integer> s = new ArrayDeque<Integer>();
        int i = 0;
        while (i <= max) {
            int height = 0;;
            if (i < max) {
                height = heights[i];
            }
            if (s.isEmpty() || height >= heights[s.peek()]) {
                s.push(i);
                i++;
            }
            else {
                // Get the previous top height
                int topIdx = s.pop();
                // If stack is empty, then it means previous elements are greater than heights[topIdx]
                // Else, s.peek() is the starting point of histogram. width = i - (s.peek() + 1)
                int width = i;
                if (!s.isEmpty()) {
                    width = i - (s.peek() + 1);
                }
                int currArea = heights[topIdx] * width;
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }
}