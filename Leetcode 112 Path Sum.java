/*
Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.

For example:
Given the below binary tree and sum = 22,
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \      \
        7    2      1
return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        int val = sum - root.val;
        if (val == 0 && root.left == null && root.right == null) {
            return true;
        }
        return hasPathSum(root.left, val) || hasPathSum(root.right, val);
    }

    // Level Order Traversal:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        Queue<Integer> sumQueue = new LinkedList<Integer>();
        queue.offer(root);
        sumQueue.offer(root.val);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                int currSum = sumQueue.poll();
                if (currNode.left == null && currNode.right == null && currSum == sum) {
                    return true;
                }
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                    sumQueue.offer(currSum + currNode.left.val);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                    sumQueue.offer(currSum + currNode.right.val);
                }
            }
        }
        return false;
    }

    // Preorder Traversal:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        Stack<Integer> numStack = new Stack<Integer>();
        stack.push(root);
        numStack.push(root.val);
        while (!stack.isEmpty()) {
            TreeNode currNode = stack.pop();
            int currSum = numStack.pop();
            if (currNode.left == null && currNode.right == null && currSum == sum) {
                return true;
            }
            if (currNode.right != null) {
                stack.push(currNode.right);
                numStack.push(currSum + currNode.right.val);
            }
            if (currNode.left != null) {
                stack.push(currNode.left);
                numStack.push(currSum + currNode.left.val);
            }
        }
        return false;
    }
    // Postorder traversal
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node = root, prevNode = null;
        int tempSum = 0;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                tempSum += node.val;
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.peek();
            if (currNode.left == null && currNode.right == null && tempSum == sum) {
                return true;
            }
            if (currNode.right != null && currNode.right != prevNode) {
                node = currNode.right;
            }
            else {
                currNode = stack.pop();
                prevNode = currNode;
                tempSum -= currNode.val;
            }
        }
        return false;
    }
}