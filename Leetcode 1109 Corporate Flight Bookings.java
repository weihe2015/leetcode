/**
There are n flights, and they are labeled from 1 to n.

We have a list of flight bookings.  The i-th booking bookings[i] = [i, j, k] means that we booked k seats from flights labeled i to j inclusive.

Return an array answer of length n, representing the number of seats booked on each flight in order of their label.


Example 1:

Input: bookings = [[1,2,10],[2,3,20],[2,5,25]], n = 5
Output: [10,55,45,25,25]
*/
class Solution {

    /**
    1. 换一种思路理解题意，将问题转换为：某公交车共有 n 站，第 i 条记录 bookings[i] = [i, j, k] 表示在 i 站上车 k 人，乘坐到 j 站，在 j+1 站下车，需要按照车站顺序返回每一站车上的人数
    
    2. 根据 1 的思路，定义 res[] res[i] 表示第 i+1 站。遍历 bookings[]：bookings[i] = [i, j, k] 表示在 i 站增加 k 人即 res[i-1] += k，在 j+1 站减少 k 人即 counters[j] -= k

    3. 遍历（整理）res[] 数组，得到每站总人数： 每站的人数为前一站人数加上当前人数变化 res[i] += res[i - 1]

    */
    public int[] corpFlightBookings(int[][] bookings, int n) {
        int[] res = new int[n];
        
        for (int[] booking : bookings) {
            int stop1 = booking[0];
            int stop2 = booking[1];
            res[stop1 - 1] += booking[2];
            if (stop2 < n) {
                res[stop2] -= booking[2];
            }
        }
        
        for (int i = 1; i < n; i++) {
            res[i] += res[i-1];
        }
        return res;
    }
}