/**
Determine if a Sudoku is valid, according to: Sudoku Puzzles - The Rules.
The Sudoku board could be partially filled, where empty cells are filled with the character '.'.
A partially filled sudoku which is valid.
Example 1:

Input:
[
  ["5","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: true
Example 2:

Input:
[
  ["8","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: false
Explanation: Same as Example 1, except with the 5 in the top left corner being 
    modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.

Note:
A valid Sudoku board (partially filled) is not necessarily solvable. Only the filled cells need to be validated.
*/
public class Solution {
    private static final int THREE = 3;
    private static final int NINE = 9;
    public boolean isValidSudoku(char[][] board) {
        if (board == null || board.length != NINE || board[0].length != NINE) {
            return false;
        }
        // check each row to see if each row has same number:
        for (int i = 0; i < NINE; i++) {
            boolean[] flags = new boolean[NINE];
            for (int j = 0; j < NINE; j++) {
                if (board[i][j] != '.') {
                    int index = Character.getNumericValue(board[i][j]) - 1;
                    if (flags[index]) {
                        return false;
                    }
                    flags[index] = true;
                }
            }
        }
        
        // check each column to see if each column has same number:
        for (int j = 0; j < NINE; j++) {
            boolean[] flags = new boolean[NINE];
            for (int i = 0; i < NINE; i++) {
                if (board[i][j] != '.') {
                    int index = Character.getNumericValue(board[i][j]) - 1;
                    if (flags[index]) {
                        return false;
                    }
                    flags[index] = true;
                }
            }
        }
        
        // check each 3*3 matrix to see if there is same number in each matrix.
        // There are totally 9 3*3 matrix, Divide the whole matrix board into 9 pieces:
        // From row = 0 to 2, col = 0 to 2.
        /**
         *  [0,0] => [0,0]
            [0,1] => [0,3]
            [0,2] => [0,6]
            [1,0] => [3,0]
            [1,1] => [3,3]
            [1,2] => [3,6]
        */
        for (int row = 0; row < THREE; row++) {
            for (int col = 0; col < THREE; col++) {
                boolean[] flags = new boolean[NINE];
                int xIdx = row * THREE;
                int yIdx = col * THREE;
                for (int i = 0; i < THREE; i++) {
                    for (int j = 0; j < THREE; j++) {
                        if (board[i+xIdx][j+yIdx] != '.') {
                            int index = Character.getNumericValue(board[i+xIdx][j+yIdx]) - 1;
                            if (flags[index]) {
                                return false;
                            }
                            flags[index] = true;
                        }
                    }
                }
            }
        }
        return true;
    }
}