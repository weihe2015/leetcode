public class Solution {
  // Leetcode 026: Remove Duplicates from Sorted Array
  // https://leetcode.com/problems/remove-duplicates-from-sorted-array/
  /**
  Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.

  Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

  Example 1:

  Given nums = [1,1,2],

  Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.

  It doesn't matter what you leave beyond the returned length.
  Example 2:

  Given nums = [0,0,1,1,1,2,2,3,3,4],

  Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.

  It doesn't matter what values are set beyond the returned length.
  */
  // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        int n = nums.length;
        if (n <= 1) {
            return n;
        }
        int i = 0, j = 1, idx = 1;
        while (j < n) {
            if (nums[i] != nums[j]) {
                nums[idx++] = nums[j];
                i = j;
            }
            j++;
        }
        return idx;
    }

    // Leetcode 027: Remove Element
    // https://leetcode.com/problems/remove-element/
    /**
    Given an array nums and a value val, remove all instances of that value in-place and return the new length.

    Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

    The order of elements can be changed. It doesn't matter what you leave beyond the new length.

    Example 1:

    Given nums = [3,2,2,3], val = 3,

    Your function should return length = 2, with the first two elements of nums being 2.

    It doesn't matter what you leave beyond the returned length.
    Example 2:

    Given nums = [0,1,2,2,3,0,4,2], val = 2,

    Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.

    Note that the order of those five elements can be arbitrary.

    It doesn't matter what values are set beyond the returned length.
    */
    // Running time: O(n), Space Complexity: O(1)
    public int removeElement(int[] nums, int val) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int idx = 0, i = 0, n = nums.length;
        while (i < n) {
            if (nums[i] != val) {
                nums[idx++] = nums[i];
            }
            i++;
        }
        return idx;
    }
    // Leetcode 080: Remove Duplicates from Sorted Array II
    // https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/
    /**
    Given a sorted array nums, remove the duplicates in-place such that duplicates appeared at most twice and return the new length.

    Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

    Example 1:

    Given nums = [1,1,1,2,2,3],

    Your function should return length = 5, with the first five elements of nums being 1, 1, 2, 2 and 3 respectively.

    It doesn't matter what you leave beyond the returned length.
    Example 2:

    Given nums = [0,0,1,1,1,1,2,3,3],

    Your function should return length = 7, with the first seven elements of nums being modified to 0, 0, 1, 1, 2, 3 and 3 respectively.

    It doesn't matter what values are set beyond the returned length.
    */
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        boolean duplicated = false;
        int idx = 1, i = 1, n = nums.length;
        while (i < n) {
            if (nums[i] == nums[i-1]) {
                if (!duplicated) {
                    nums[idx++] = nums[i];
                    duplicated = true;
                }
            }
            else {
                nums[idx++] = nums[i];
                duplicated = false;
            }
            i++;
        }
        return idx;
    }

    // Leetcode 283 Move Zeroes
    // https://leetcode.com/problems/move-zeroes/
    /**
    Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

    Example:

    Input: [0,1,0,3,12]
    Output: [1,3,12,0,0]
    Note:

    You must do this in-place without making a copy of the array.
    Minimize the total number of operations.
     */
    public void moveZeroes(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int idx = 0, j = 0, n = nums.length;
        while (j < n) {
            if (nums[j] != 0) {
                nums[idx] = nums[j];
                idx++;
            }
            j++;
        }
        // mark the rest of array as 0.
        while (idx < n) {
            nums[idx] = 0;
            idx++;
        }
    }

    // Leetcode 028: Implement strStr()
    // https://leetcode.com/problems/implement-strstr/
    /**
    Implement strStr().

    Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

    Example 1:

    Input: haystack = "hello", needle = "ll"
    Output: 2
    Example 2:

    Input: haystack = "aaaaa", needle = "bba"
    Output: -1
     */
    public int strStr(String haystack, String needle) {
        if (haystack == null || needle == null) {
            return 0;
        }
        int m = haystack.length(), n = needle.length();
        for (int i = 0; i <= m-n; i++) {
            int j = 0;
            while (j < n) {
                if (haystack.charAt(i + j) != needle.charAt(j)) {
                    break;
                }
                j++;
            }
            if (j == n) {
                return i;
            }
        }
        return -1;
    }

    // Leetcode 075 Sort Colors:
    // https://leetcode.com/problems/sort-colors/
    /**
    Given an array with n objects colored red, white or blue, sort them in-place so that objects of the same color are adjacent, with the colors in the order red, white and blue.

    Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.

    Note: You are not suppose to use the library's sort function for this problem.

    Example:

    Input: [2,0,2,1,1,0]
    Output: [0,0,1,1,2,2]
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public void sortColors(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int i = 0, zero = 0, n = nums.length, two = n-1;
        while (i <= two) {
            while (nums[i] == 2 && i < two) {
                swap(nums, i, two--);
            }
            while (nums[i] == 0 && i > zero) {
                swap(nums, i, zero++);
            }
            i++;
        }
    }
    private void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}