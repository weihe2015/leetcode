/**
 * A Fenwick tree or Binary Indexed Tree is a data structure to provide efficient methods
 * for calculation and manipulation of the prefix sums of an array of values
 * 
 * Space Complexity: O(N)
 * Time Complexity for creating Fenwick Tree: O(NlogN)
 * Time Complexity for updating value in Fenwick Tree: O(logN)
 * Time Complexity for getting prefix sum in Fenwick Tree: O(logN)
 * 
*/
public class FenwickTree {
    
    private int[] BIT;
    public FenwickTree(int N) {
        this.BIT = new int[N];
    }

    /**
     * Creating tree is like updating Fenwick tree for every value in array
     */
    public void buildTree(int[] nums) {
        for (int i = 1, n = nums.length; i <= n; i++) {
            updateBinaryIndexTree(nums[i-1], i);
        }
    }

    /**
     * Start from index+1 if you updating index in original array. 
     * Keep adding this value for next node till you reach outside range of tree
     * 
    */
    public void updateBinaryIndexTree(int value, int idx) {
        int N = BIT.length;
        while (idx < N) {
            BIT[idx] += value;
            idx = getNextIdx(idx);
        }
    }

    /**
     * Start from index + 1 if you want to get prefix Sum 0 to index
     * Keep adding value till index reaches 0.
     * @param: current index
     * @return: prefix sum of 0 to index
     * 
    */
    public int getSum(int idx) {
        // 这里idx = idx + 1是因为在Binary Index Tree 里，第一个node是dummy node，
        // 然后第一个元素的和存储在了index = 1的地方里，所以要个prefix 0 to index的时候，自然要index = index + 1
        idx = idx + 1;
        int sum = 0;
        while (idx > 0) {
            sum += BIT[idx];
            idx = getParentIdx(idx);
        }
        return sum;
    }
    /**
     * Return the parent index of current index:
     * @param: current index
     * @return: parent index
     * 
     * To get parent index:
     * 1. Get its 2's complement = get its opposite value: (9) => (-9)
     *    1.1  Invert all the bits
     *    1.2  Add one to it
     * 2. And this 2's complement with initial value
     * 3. Subtract this value from initial value
    */
    private int getParentIdx(int idx) {
        return idx - (idx & (-idx));
    }

    /**
     * Return the next index of current index:
     * @param: current index
     * @return: next index
     * 
     * To get parent index:
     * 1. Get its 2's complement = get its opposite value: (9) => (-9)
     *    1.1  Invert all the bits
     *    1.2  Add one to it
     * 2. And this 2's complement with initial value
     * 3. Add this value from initial value
    */
    private int getNextIdx(int idx) {
        return idx + (idx & (-idx));
    }

    @Test
    public void FenwickTreeTest1() throws Exception {
        int[] nums = {1,2,3,4,5,6,7};
        FenwickTree ft = new FenwickTree(nums.length + 1);
        ft.buildTree(nums);
        Assert.assertEquals(1, ft.getSum(0));
        Assert.assertEquals(3, ft.getSum(1));
        Assert.assertEquals(6, ft.getSum(2));
        Assert.assertEquals(10, ft.getSum(3));
        Assert.assertEquals(15, ft.getSum(4));
        Assert.assertEquals(21, ft.getSum(5));
        Assert.assertEquals(28, ft.getSum(5));
    }
}

// Leetcode 315: Count of Smaller Numbers After itself:
/* https://leetcode.com/problems/count-of-smaller-numbers-after-self
You are given an integer array nums and you have to return a new counts array. The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].

Example:

Input: [5,2,6,1]
Output: [2,1,1,0] 
Explanation:
To the right of 5 there are 2 smaller elements (2 and 1).
To the right of 2 there is only 1 smaller element (1).
To the right of 6 there is 1 smaller element (1).
To the right of 1 there is 0 smaller element.
*/
public class Solution {
        /**
     * Convert the question into: Prefix sum of frequencies
     *  [5,2,6,1] => 翻转数组： [1,6,2,5] => 找出之前比自己小的数的个数
     *    用prefix sum来取得这个个数
     * 1. sort 原数组 到一个新的数组里，保留原数组
     * 2. 计算每个元素的排名：
     *     就是历遍sorted的数组，把每个数的排名放在HashMap里
     * 3. 从右向左历遍原数组，
     *      对于每个元素，得到它相应的rank，然后计算rank-1的prefix sum
     *      更新这个rank by 增加1
     * Time Complexity: O(NlogN) NlogN for sorting, + N for creating rankMap, + N * logN for scanning the array
     *            from right to left, and tree.getSum & tree.update is logN
     * Space Complexity: O(N): Store extra array for ranking purpose.
     * 
    */
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> result = new ArrayList<Integer>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        int n = nums.length;
        // 先求出每个元素的rank, 重复的元素一样的rank
        int[] sortedArr = Arrays.copyOf(nums, n);
        Arrays.sort(sortedArr);
        // key -> num, value -> its rank
        // [5,2,6,1] => {5->3, 2->1, 1->1, 6->4}
        Map<Integer, Integer> rankMap = new HashMap<Integer, Integer>();
        int rank = 1;
        for (int num : sortedArr) {
            if (!rankMap.containsKey(num)) {
                rankMap.put(num, rank);  
                rank++;
            }
        }
        // Use number of unique items + 1 to build the Fenwick Tree 
        FenwickTree tree = new FenwickTree(rank);
        // Scan the array from right to left:
        for (int i = n-1; i >= 0; i--) {
            // 得到这个元素的rank
            rank = rankMap.get(nums[i]);
            // 找在这个元素的rank之前一共有多少个。
            int sum = tree.getSum(rank - 1);
            result.add(sum);
            // 更新这个数的rank by increment 1
            tree.update(rank, 1);
        }
        Collections.reverse(result);
        return result;
    }
    
    class FenwickTree {
        int[] BIT;
        int N;
        public FenwickTree(int N) {
            this.N = N;
            this.BIT = new int[N];
        }
        
        public int getSum(int idx) {
            // 这里不用idx = idx + 1 是因为：rankMap是从1开始的。
            int sum = 0;
            while (idx > 0) {
                sum += BIT[idx];
                idx = getParentIdx(idx);
            }
            return sum;
        }
        
        public void update(int idx, int value) {
            while (idx < N) {
                BIT[idx] += value;
                idx = getNextIdx(idx);
            }
        }
        
        public int getParentIdx(int idx) {
            return idx - (idx & (-idx));
        }
        
        public int getNextIdx(int idx) {
            return idx + (idx & (-idx));
        }
    }
}