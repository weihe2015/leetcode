/**
 *  1-4-19:
 *     Binary Search 分为两种问题。1. 搜索目标值，2. 寻找边界
 *  v2更新的内容：
 *  相比与v1的binary search，这个版本的binary search不需要考虑边界问题，不需要考虑是否会infinite loop的问题
*   1. 搜索目标值：跟v1一样，
]     int low = 0, high = nums.length - 1;
      if (nums[mid] == target) return mid;
    2. 寻找边界：
        只要每次都移动low或者high pointer的话，就不用担心会infinite loop
        设置一个variable ans
        根据题意，如果nums[mid]符合条件，ans = mid，然后根据题目来决定是移动low pointer还是high pointer

*/
public class BinarySearch {
    // Updated on 1-4-19:
    // New way of thinking binary search:
    // As long as each time we move the left or right pointer by 1, we don't need to consider infinite loop.
    // Case 1: Find element in a sorted array:
    public int binarySearch(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                res = mid;
                low = mid + 1;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }

    // Case 2: Binary Search for finding first appearence of item in an array, which may have duplicates.
    // If not found, return -1;
    public int binarySearch2(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                res = mid;
                high = mid - 1;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }

    // Case 3: Binary Search for finding last appearence of item in an array, which may have duplicates
    // If not found, return -1;
    public int binarySearch3(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        int ans = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                ans = mid;
                low = mid + 1;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return ans;
    }

    // case 4: Binary Search for returning 刚好小于key的元素下标x，如果不存在返回-1
    public int binarySearch4(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        int ans = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            else if (nums[mid] < target) {
                ans = mid;
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return ans;
    }

    // case 5: Binary Search for returning 刚好大于key的元素下标x, 如果不存在返回-1
    public int binarySearch5(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1, ans = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                ans = mid;
                high = mid - 1;
            }
        }
        return ans;
    }

    // Leetcode 275 H-Index II
    // https://leetcode.com/problems/h-index-ii/
    /**
    Given an array of citations sorted in ascending order (each citation is a non-negative integer) of a researcher,
    write a function to compute the researcher's h-index.

    According to the definition of h-index on Wikipedia: "A scientist has index h if
    h of his/her N papers have at least h citations each, and the other N − h papers have no more than h citations each."

    Example:

    Input: citations = [0,1,3,5,6]
    Output: 3
    Explanation: [0,1,3,5,6] means the researcher has 5 papers in total and each of them had
                received 0, 1, 3, 5, 6 citations respectively.
                Since the researcher has 3 papers with at least 3 citations each and the remaining
                two with no more than 3 citations each, her h-index is 3.
    Note:

    If there are several possible values for h, the maximum one is taken as the h-index.

    Follow up:

    This is a follow up problem to H-Index, where citations is now guaranteed to be sorted in ascending order.
    Could you solve it in logarithmic time complexity?
    */
    public int hIndex(int[] citations) {
        int low = 0, high = citations.length - 1, n = citations.length;
        int ans = 0;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (citations[mid] >= n-mid) {
                ans = Math.max(ans, n-mid);
                high = mid-1;
            }
            else {
                low = mid+1;
            }
        }
        return ans;
    }

    // Leetcode 033: Search in Rotated Sorted Array:
    // https://leetcode.com/problems/search-in-rotated-sorted-array/
    /**
    Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

    (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

    You are given a target value to search. If found in the array return its index, otherwise return -1.

    You may assume no duplicate exists in the array.

    Your algorithm's runtime complexity must be in the order of O(log n).

    Example 1:

    Input: nums = [4,5,6,7,0,1,2], target = 0
    Output: 4
    Example 2:

    Input: nums = [4,5,6,7,0,1,2], target = 3
    Output: -1
    */
    public int search(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            // check if right side subarray is sorted or not.
            if (nums[mid] < nums[high]) {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
            // check if left side subarray is sorted or not.
            else {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
        }
        return -1;
    }

    // Another solution:
    public int search(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        int res = -1;
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                res = mid;
                break;
            }
            // If left array is in order:
            // Without = sign, the test case will failed: nums=[3,1], target = 1
            if (nums[low] <= nums[mid]) {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
            // If right array is in order:
            else {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
        }
        return res;
    }

    // Leetcode 081: Search in Rotated Sorted Array II: with duplicate:
    // https://leetcode.com/problems/search-in-rotated-sorted-array-ii/
    /**
    Follow up for "Search in Rotated Sorted Array", Leetcode 033:
    What if duplicates are allowed?
    (i.e., [0,0,1,2,2,5,6] might become [2,5,6,0,0,1,2]).

    You are given a target value to search. If found in the array return true, otherwise return false.

    Example 1:

    Input: nums = [2,5,6,0,0,1,2], target = 0
    Output: true
    Example 2:

    Input: nums = [2,5,6,0,0,1,2], target = 3
    Output: false

    Would this affect the run-time complexity? How and why?
    Write a function to determine if a given target is in the array.

  *  Yes, the run-time complexity will be O(n), because there may be a case
     where most items are duplicate and nums[low]==nums[mid], so that we need to move low pointer one by one.
      [1,1,1,3,1,1,1,1,1], find 3,
  * When we meet duplicates, just move the low/right pointer.
    */
    // Run time complexity: O(n) worst case
    // use mid and high to determine which part of subarray is sorted
    public boolean search(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return false;
        }
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low)/2;
            if (nums[mid] == target) {
                return true;
            }
            // if right side array is sorted
            if (nums[mid] < nums[high]) {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
            // if left side array is sorted
            else if (nums[mid] > nums[high]) {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
            // if it is duplicated, move high back one step
            // Ex: [3,1,1], target = 3. low = 0, high = 2, mid = 1.
            // If we don't decrement high pointer but jump into the second condition,
            // low = mid + 1 and this target will not be found.
            else {
                high--;
            }
        }
        return false;
    }

    // Leetcode 034: Find First and Last Position of Element in Sorted Array:
    // https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
    // Running Time Complexity: O(logN), Space Complexity: O(1)
    // Follow up, we can also use low variable as class variable, start searching low from where it ends
    // on binarySearchFirstPosition func.
    public int[] searchRange(int[] nums, int target) {
        int[] res = {-1, -1};
        if (nums == null || nums.length == 0) {
            return res;
        }
        int fstPos = binarySearchFirstPosition(nums, target);
        if (fstPos == -1) {
            return res;
        }
        res[0] = fstPos;
        res[1] = binarySearchLastPosition(nums, target);
        return res;
    }

    private int binarySearchFirstPosition(int[] nums, int target) {
        int low = 0, high = nums.length - 1, ans = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                ans = mid;
                // bias to the left, because we are finding the left bounder
                high = mid - 1;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return ans;
    }

    private int binarySearchLastPosition(int[] nums, int target) {
        // low can start from left bounder of first function, which may help the running time a little.
        // low = ans; high = nums.length - 1;
        int low = 0, high = nums.length - 1, ans = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                ans = mid;
                // bias to the right, because we are finding the right bounder
                low = mid + 1;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return ans;
    }

    // Leetcode 35: Search Insert Position
    // https://leetcode.com/problems/search-insert-position/
    // Use Left most search
    /**
    Given a sorted array and a target value, return the index if the target is found.
    If not, return the index where it would be if it were inserted in order.
    You may assume no duplicates in the array.

    Example 1:

    Input: [1,3,5,6], 5
    Output: 2
    Example 2:

    Input: [1,3,5,6], 2
    Output: 1
    Example 3:

    Input: [1,3,5,6], 7
    Output: 4
    Example 4:

    Input: [1,3,5,6], 0
    Output: 0
    */
    // Find the element that is equals or just larger than target.
    // 找刚好大于target的元素的位置
    // Running Time Complexity: O(logN), Space Complexity: O(1)
    public int searchInsert(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int res = -1;
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                ans = mid;
                high = mid - 1;
            }
        }
        // 如果 res == -1, 说明 target大于nums的最大值，那么while loop就会一直在else if (nums[mid] < target)
        // 而且一直向右移动low pointer
        // 或者这里可以initialize ans = nums.length
        if (res == -1) {
            res = nums.length;
        }
        return ans;
    }
    // Left most search, find the position is less or equal than the target.
    // It can also handle the case where the nums array has duplicates values
    public int searchInsert(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        int res = -1, low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] >= target) {
                res = mid;
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        if (res == -1) {
            res = nums.length;
        }
        return res;
    }

    // Leetcode 153: Find Minimum in Rotated Sorted Array:
    // https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/
    /*
    Suppose a sorted array is rotated at some pivot unknown to you beforehand.

    (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

    Find the minimum element.

    You may assume no duplicate exists in the array.
    */
    /**
    * Three cases:
    * 1. The leftmost value is less than the rightmost value in the list: This means that the list is not rotated.
    *   ex: [1,2,4,5,6,9], low = 0, high = 6, mid = 3, 5 < 9
    * 2. The value in the middle of the list is > than the leftmost and rightmost values in the list.
    *  ex: [4,5,6,7,0,1,2], low = 0, high = 6, mid = 3, 7 > 2 => low = mid + 1
    * 3. The value in the middle of the list is < the leftmost and rightmost values in the list.
    *  ex: [5,6,7,0,1,2,4], low = 0, high = 6, mid = 3, 0 < 4 => high = mid;
    *
    */
    // Running Time Complexity: O(logN), Space Complexity: O(1)
    public int findMin(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1, minVal = Integer.MAX_VALUE;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            minVal = Math.min(minVal, nums[mid]);
            if (nums[mid] < nums[high]) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return minVal;
    }

    // Leetcode 154: Find Minimum in Rotated Sorted Array II
    // https://leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/
    /**
    Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
    (i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).

    Find the minimum element.

    The array may contain duplicates.

    Example 1:

    Input: [1,3,5]
    Output: 1
    Example 2:

    Input: [2,2,2,0,1]
    Output: 0
    Note:

    This is a follow up problem to Find Minimum in Rotated Sorted Array.
    Would allow duplicates affect the run-time complexity? How and why?
    */
    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int findMin(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1, minVal = Integer.MAX_VALUE;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            minVal = Math.min(minVal, nums[mid]);
            // If nums[mid] < nums[high], we know that right hand side is sorted and
            // the minVal is on the left hand side, so we put our high border to mid-1
            if (nums[mid] < nums[high]) {
                high = mid - 1;
            }
            else if (nums[mid] > nums[high]) {
                low = mid + 1;
            }
            else {
                high--;
            }
        }
        return minVal;
    }

    // Leetcode 162. Find Peak Element
    // https://leetcode.com/problems/find-peak-element/
    /**
    A peak element is an element that is greater than its neighbors.

    Given an input array nums, where nums[i] ≠ nums[i+1], find a peak element and return its index.

    The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.

    You may imagine that nums[-1] = nums[n] = -∞.

    Example 1:

    Input: nums = [1,2,3,1]
    Output: 2
    Explanation: 3 is a peak element and your function should return the index number 2.
    Example 2:

    Input: nums = [1,2,1,3,5,6,4]
    Output: 1 or 5
    Explanation: Your function can return either index number 1 where the peak element is 2,
                or index number 5 where the peak element is 6.
    */
    // binary search iteration
    // Running Time Complexity: O(logN)
    public int findPeakElement(int[] nums) {
        int low = 0, high = nums.length-1;
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            // when mid == nums.length - 1, it means the whole array is accending.
            if (mid == nums.length - 1) {
                res = mid;
                break;
            }
            if (nums[mid] < nums[mid+1]) {
                low = mid + 1;
            }
            // We cannot stop here because it may have a peak element on the left
            // Ex: [3,2,1]
            else if (nums[mid] > nums[mid+1]) {
                res = mid;
                high = mid - 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }

    // Leetcode 852: Peak Index in a Mountain Array
    // https://leetcode.com/problems/peak-index-in-a-mountain-array/
    /**
        Let's call an array A a mountain if the following properties hold:
        A.length >= 3
        There exists some 0 < i < A.length - 1 such that A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1]
        Given an array that is definitely a mountain, return any i such that A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1].

        Example 1:

        Input: [0,1,0]
        Output: 1
        Example 2:

        Input: [0,2,1,0]
        Output: 1
        Note:

        3 <= A.length <= 10000
        0 <= A[i] <= 10^6
        A is a mountain, as defined above.
    */
    // Running time Complexity: O(logN)
    public int peakIndexInMountainArray(int[] nums) {
        // binary search:
        int low = 0, high = nums.length - 1;
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid-1] < nums[mid] && nums[mid] > nums[mid+1]) {
                res = mid;
                break;
            }
            else if (nums[mid] < nums[mid+1]) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }


    // Leetcode 167: Two Sum II - Input array is sorted
    // https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
    /**
    Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.
    The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2.

    Note:

    Your returned answers (both index1 and index2) are not zero-based.
    You may assume that each input would have exactly one solution and you may not use the same element twice.
    Example:

    Input: numbers = [2,7,11,15], target = 9
    Output: [1,2]
    Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
    */
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        if (nums == null || nums.length == 0) {
            return res;
        }
        for (int i = 0, max = nums.length; i < max; i++) {
            int diff = target - nums[i];
            int low = i+1, high = max-1;
            while (low <= high) {
                int mid = low + (high - low) / 2;
                if (nums[mid] == diff) {
                    res[0] = i+1;
                    res[1] = mid+1;
                    break;
                }
                else if (nums[mid] < diff) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
        }
        return res;
    }

    // Leetcode 278: First Bad Version
    // https://leetcode.com/problems/first-bad-version/
    /**
    Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.

    You are given an API bool isBadVersion(version) which will return whether version is bad.
    Implement a function to find the first bad version. You should minimize the number of calls to the API.

    Example:

    Given n = 5, and version = 4 is the first bad version.

    call isBadVersion(3) -> false
    call isBadVersion(5) -> true
    call isBadVersion(4) -> true

    Then 4 is the first bad version.
    */
    // Find the left boundary of target
    // Running Time Complexity: O(logN), Space Complexity: O(1)
    public int firstBadVersion(int n) {
        int low = 1, high = n,
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (isBadVersion(mid)) {
                res = mid;
                // bias to left because we want to find the first bad version, which is on the left
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return res;
    }

    // Leetcode 074: Search a 2D Matrix:
    // https://leetcode.com/problems/search-a-2d-matrix/
    /**
    Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:

    Integers in each row are sorted from left to right.
    The first integer of each row is greater than the last integer of the previous row.
    For example,

    Consider the following matrix:

    [
    [1,   3,  5,  7],
    [10, 11, 16, 20],
    [23, 30, 34, 50]
    ]
    Given target = 3, return true.

    Example 2:

    Input:
    matrix = [
    [1,   3,  5,  7],
    [10, 11, 16, 20],
    [23, 30, 34, 50]
    ]
    target = 13
    Output: false
    **/
    // Running Time Complexity: O(log(m*n)), Space Complexity: O(1)
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        int m = matrix.length, n = matrix[0].length;
        int res = -1;
        int low = 0, high = m*n-1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            // 我们用 matrix的宽度作为分母去找mid val
            int val = matrix[mid/n][mid%n];
            if (val == target) {
                res = val;
                break;
            }
            else if (val < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res != -1;
    }

    // Leetcode 240 Search a 2D Matrix II:
    // https://leetcode.com/problems/search-a-2d-matrix-ii/
    /**
    Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:

    Integers in each row are sorted in ascending from left to right.
    Integers in each column are sorted in ascending from top to bottom.
    Example:

    Consider the following matrix:

    [
    [1,   4,  7, 11, 15],
    [2,   5,  8, 12, 19],
    [3,   6,  9, 16, 22],
    [10, 13, 14, 17, 24],
    [18, 21, 23, 26, 30]
    ]
    Given target = 5, return true.

    Given target = 20, return false
    start at right up corner
    */
    // Running Time Complexity: O(m + n)
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        int m = matrix.length, n = matrix[0].length;
        int i = 0, j = n-1;
        while (i < m && j >= 0) {
            if (matrix[i][j] == target) {
                return true;
            }
            else if (matrix[i][j] < target) {
                i++;
            }
            else {
                j--;
            }
        }
        return false;
    }

    // Running Time Complexity: O(M * logN), Space Complexity: O(1)
    /**
        For each row, if the target is nums[0] <= target && target <= nums[n-1],
        we perform the binary search on this row.
    */
    public boolean searchMatrix(int[][] matrix, int target) {
        // We may need to consider case like: [], [[]]
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        int m = matrix.length, n = matrix[0].length;
        for (int i = 0; i < m; i++) {
            int[] row = matrix[i];
            // we can skip some rows that must not contains target
            if (row[0] > target || row[n-1] < target) {
                continue;
            }
            boolean found = binarySearch(row, target);
            if (found) {
                return true;
            }
        }
        return false;
    }

    private boolean binarySearch(int[] nums, int target) {
        boolean found = false;
        if (nums == null || nums.length == 0) {
            return false;
        }
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                found = true;
                break;
            }
            else if (nums[mid] < target) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return found;
    }

    // Running Time Complexity: O(2 * log(m) + m * log(N))
    public boolean searchMatrix(int[][] matrix, int target) {
        boolean isFound = false;
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return isFound;
        }
        int m = matrix.length, n = matrix[0].length;
        // perform binary search on first column of matrix,
        // to find the row whose the first item is just less than target
        int low = 0, high = m-1, res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int val = matrix[mid][0];
            if (val == target) {
                return true;
            }
            else if (val < target) {
                res = mid;
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        // If all the item item of all rows are greater than the target, no result will be found
        if (res == -1) {
            return false;
        }
        int temp = res;

        // perform binary search on last column of matrix,
        // to find the row whose last item is just greater than target
        low = 0;
        high = m-1;
        res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int val = matrix[mid][n-1];
            if (val == target) {
                return true;
            }
            else if (val < target) {
                low = mid + 1;
            }
            else {
                res = mid;
                high = mid - 1;
            }
        }
        if (res == -1) {
            return false;
        }
        int start = Math.min(temp, res);
        int end = Math.max(temp, res);
        for (int i = start; i <= end; i++) {
            boolean subFound = binarySearch(matrix[i], target);
            if (subFound) {
                isFound = true;
                break;
            }
        }
        return isFound;
    }

    // Leetcode 069: Sqrt(x)
    // https://leetcode.com/problems/sqrtx/
    /**
    Implement int sqrt(int x).

    Compute and return the square root of x, where x is guaranteed to be a non-negative integer.

    Since the return type is an integer, the decimal digits are truncated and only the integer part of the result is returned.

    Example 1:

    Input: 4
    Output: 2
    Example 2:

    Input: 8
    Output: 2
    Explanation: The square root of 8 is 2.82842..., and since
                the decimal part is truncated, 2 is returned.
     */
    // Running Time Complexity: O(log(x))
    public int mySqrt(int x) {
        if (x == 0) {
            return x;
        }
        int low = 1, high = x, res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (mid <= x/mid) {
                res = mid;
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }
}