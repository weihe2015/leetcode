    // Running Time Complexity: T(N) = N * T(N-1) = O(N!)
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        if (candidates == null || candidates.length == 0) {
            return result;
        }
        int n = candidates.length;
        List<Integer> list = new ArrayList<>();
        combinationSum(result, candidates, 0, n, list, 0, target);
        return result;
    }

    private void combinationSum(List<List<Integer>> result, int[] candidates, int level, int n, List<Integer> list, int sum, int target) {
        if (sum > target) {
            return;
        }
        if (sum == target) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        // use level variable because we don't [3,3,2] in case [2,3,5] target = 8, only for [2,3,3]
        for (int i = level; i < n; i++) {
            int num = candidates[i];
            sum += num;
            list.add(num);
            // 这里用i，而不是i+1是因为一个元素可以无限次用，如果只能用一次的话，那就是i+1
            combinationSum(result, candidates, i, n, list, sum, target);
            sum -= num;
            list.remove(list.size()-1);
        }
        /***
         * 如果是以下的code：没有用level这个变量的话，就会出现以下的结果：
        candidates=[2,3,6,7], target=7, result= [[2,2,3],[2,3,2],[3,2,2],[7]]
        private void dfs(List<List<Integer>> result, int[] candidates, int target, List<Integer> list) {
            if (target < 0) {
                return;
            }
            if (target == 0) {
                result.add(new ArrayList<Integer>(list));
                return;
            }
            for (int i = 0, n = candidates.length; i < n; i++) {
                int num = candidates[i];
                list.add(num);
                dfs(result, candidates, target-num, list);
                list.remove(list.size()-1);
            }
        }
        */
    }