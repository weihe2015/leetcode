public class Solution {

    // https://medium.com/basecs/tagged/algorithms
    /**
     * Stability: A stable sort preserves the relatives order of items with equal keys
     * Stable:
     *    Bubble Sort, Insertion Sort, Merge Sort
     *      Equal items never move past each other
     * Unstable:
     *    ShellSort, Selection Sort
     *      Long distance exchange might move an item past some equal item
    */

    public void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    public boolean isSorted(int[] nums, int low, int high) {
        for (int i = low; i < high; i++) {
            if (nums[i] > nums[i+1]) {
                return false;
            }
        }
        return true;
    }

    // Comparsion Sorting
    /**
     * Loops through the array from left to right,
     *  compare each adjcent elements A[i] and A[i+1],
     *      if A[i] > A[i+1], swap these two elements.
     *
     * Running Time: O(N^2)
     * Worst: O(N^2), average: O(N^2)
     * Space complexity: O(1)
     * Stable:
     *  before sorting:: A[i] is in front of A[j],
     *  after sorting: A[i] is still in front of A[j]
    */
    public void bubbleSort(int[] nums, int N) {
        for (int i = 0; i < N; i++) {
            boolean swapped = false;
            for (int j = 0; j < N-i-1; j++) {
                if (nums[j] > nums[j+1]) {
                    swap(nums, j, j+1);
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
        Assert.assertTrue(isSorted(nums, 0, N-1));
    }

    /**
     * Cocktail Sorting: Variation of bubble Sort:
     * Two Stages:
     *    1. Loops through the array from left to right, just like bubble sort
     *    2. Loops through the array from right to left;
     *        adjacent items are compared and swapped if necessary
     *
     * After first forward pass, greatest element of the array will be present at the last index of array.
     *
     * Worst: O(N^2), average: O(N^2)
     * Space complexity: O(1)
     * Stable:
     *  before sorting:: A[i] is in front of A[j],
     *  after sorting: A[i] is still in front of A[j]
     *
     * https://www.geeksforgeeks.org/cocktail-sort/
    */
    public void cocktailSort(int[] nums, int N) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int low = 0, high = N-1;
        boolean swapped = false;
        while (low < high) {
            // Sort the elements by putting the larger one behind
            for (int i = low; i < high; i++) {
                if (nums[i] > nums[i+1]) {
                    swap(A, i, i+1);
                    swapped = true;
                }
            }
            // If nothing moved, then array is sorted.
            if (!swapped) {
                break;
            }
            // Otherwise, reset the swapped flag so that it
            // can be used in the next stage
            swapped = false;

            // move the end point back by one, because
            // item at the end is in its rightful spot
            high--;
            // Sort the elements by putting the smaller one in front
            for (int i = high; i > low; i--) {
                if (nums[i] < nums[i-1]) {
                    swap(A, i, i-1);
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
            // increase the starting point, because
            // the last stage would have moved the next
            // smallest number to its rightful spot.
            low++;
            swapped = false;
        }
        Assert.assertTrue(isSorted(nums, 0, N-1));
    }

    /**
     * Selection Sorting: https://medium.com/basecs/exponentially-easy-selection-sort-d7a34292b049
     *  Scan from left to right
     *  In iteration i, find index min of the smallest/largest remaining entry
     *  swap a[i] and a[min]
     *
     * 初始时在序列中找到最小（大）元素，放到序列的起始位置作为已排序序列；
     *  然后，再从剩余未排序元素中继续寻找最小（大）元素，放到已排序序列的末尾。
     *
     * Invariants:
     *   Entries the left of $ (including $) fixed and in ascending order
     *   No entry to right of $ is smaller than any entry of the left of $
     *
     * Uses (N-1) + (N-2) + ... + 1 + 0 ~ N^2/2 comparsion and N exchanges
     *
     * Worst: O(N^2), average: O(N^2), insensitive to input
     *    Quadratic time, even if input is sorted
     * Space complexity: O(1)
     *
     * Unstable: because it will change the order of some identical elements:
     *  ex: { 5, 8, 5, 2, 9 }
     *
    */
    public void selectionSort(int[] nums, int N) {
        for (int i = 0; i < N; i++) {
            // find the min value:
            int minIdx = i;
            for (int j = i + 1; j < N; j++) {
                if (nums[j] < nums[minIdx]) {
                    minIdx = j;
                }
            }
            if (minIdx != i) {
                swap(nums, minIdx, i);
            }
        }
        Assert.assertTrue(isSorted(nums, 0, N-1));
    }

    /**
     * Insertion Sort:
     *  Scans from left to right,
     *  In iteration i, swap a[i] with each larger entry to its left
     *
     * 对于未排序数据(右手抓到的牌)，在已排序序列(左手已经排好序的手牌)中
     *  从后向前扫描，找到相应位置并插入。
     *
     * Invariants:
     *  Entries to the left of $ (including $) are in ascending order
     *  Entries to the right of $ have not seen yet
     *
     * Worst: O(N^2), best: O(NlogN), average: O(N^2)
     * Space complexity: O(1)
     *
     * Best case: input array is in ascending order, make N-1 comparison and 0 exchanges
     * Worst case: input array is in descending order, make ~N^2/2 comparison and ~N^2/2 exchanges
     *
     * Stable.
     *
    */
    public void insertionSort(int[] nums, int N) {
        for (int i = 1; i < N; i++) {
            for (int j = i; j > 0; j--) {
                if (nums[j] < nums[j-1]) {
                    swap(nums, j, j-1);
                }
                else {
                    break;
                }
            }
        }
        Assert.assertTrue(isSorted(nums, 0, N-1));
    }

    /**
     * Shell Sort:
     *  Improved based on insertion sort
     *      because insertion sort can move item one place at one time.
     *  H-sorting the array
     *      Insertion sort, with stride length h
     *
     * Shell 排序通过将比较的全部元素分为几个区域来提升插入排序的性能。
     *  这样可以让一个元素可以一次性地朝最终位置前进一大步。
     *  然后算法再取越来越小的步长进行排序，算法的最后一步就是普通的插入排序，
     *  但是到了这步，需排序的数据几乎是已排好的了
     *
     * Best: O(NlogN), Worst: O((NlogN)^2), average: O((NlogN)^2)
     *
     * Which increment sequence to be used? 3x + 1
     *
     * Worst case number of compares used by shellsort with 3x+1 increments is O(N^3/2)
     * But no model is found yet.
     *
     * Unstable
    */
    public void shellSort(int[] A, int N) {
        int h = 1;
        // compute the increment step:
        while (h < N/3) {
            h = 3*h + 1; // 1, 4, 13, 40, 121, 364...
        }
        while (h >= 1) {
            // insertion sort with h step:
            for (int i = h; i < N; i++) {
                for (int j = i; j >= h; j = j-h) {
                    if (A[j] < A[j-h]) {
                        swap(A, j, j-h);
                    }
                    else {
                        break;
                    }
                }
            }
            // decrease the h step
            h = h/3;
        }
        Assert.assertTrue(isSorted(A, 0, N-1));
    }

    /**
     * Merge Sort: (Recursive)
     *  Java sort for objects
     *  Perl, C++ stable sort, python stable sort, Firefox JavaScript
     *
     * Basic Idea:
     *   Divide array into two half
     *   Recursively sort each half
     *   Merge two halves
     *
     * Worst: O(NlogN), best: O(NlogN), average: O(NlogN)
     * Space complexity: O(N)
     *
     * Uses at most NlogN compares and 6NlogN array access to sort any array of size N.
     *
     * D(N) = 2D(N/2) + N for N > 1, with D(1) = 0 => D(N) = NlogN
     *
     * Stable
    */
    public void mergeSort(int[] A, int N) {
        int[] aux = new int[N];
        mergeSort(A, aux, 0, N-1);
        Assert.assertTrue(isSorted(A, 0, N-1));
    }

    public void mergeSort(int[] A, int[] aux, int low, int high) {
        if (low >= high) {
            return;
        }
        int mid = low + (high - low) / 2;
        mergeSort(A, aux, low, mid);
        mergeSort(A, aux, mid+1, high);
        // improvement: STOP if it is already sorted.
        if (A[mid] <= A[mid+1]) {
            return;
        }
        merge(A, aux, low, mid, high);
    }

    public void merge(int[] A, int[] aux, int low, int mid, int high) {
        Assert.assertTrue(isSorted(A, low, mid));
        Assert.assertTrue(isSorted(A, mid+1, high));

        // copy A[low...high] to aux[low...high]
        for (int k = low; k <= high; k++) {
            aux[k] = A[k];
        }

        int i = low, j = mid + 1;
        // Given two sorted subarrays A[low...mid] and A[mid+1...high]
        // replace with sorted subarray A[low...high]
        for (int k = low; k <= high; k++) {
            // Compare min of each subarray
            if (i > mid) {
                A[k] = aux[j++];
            }
            else if (j > high) {
                A[k] = aux[i++];
            }
            else if (aux[i] < aux[j]) {
                A[k] = aux[i++];
            }
            else {
                A[k] = aux[j++];
            }
        }
        Assert.assertTrue(isSorted(A, low, high));
    }

    public void merge2(int[] A, int[] aux, int low, int mid, int high) {
        Assert.assertTrue(isSorted(A, low, mid));
        Assert.assertTrue(isSorted(A, mid+1, high));
        if (low >= high) {
            return;
        }
        // copy A[] array into aux[]
        for (int k = low; k <= high; k++) {
            aux[k] = A[k];
        }

        int i = low, j = mid+1, idx = low;
        while (i <= mid && j <= high) {
            if (aux[i] < aux[j]) {
                A[idx++] = aux[i++];
            }
            else {
                A[idx++] = aux[j++];
            }
        }
        while (i <= mid) {
            A[idx++] = aux[i++];
        }
        while (j <= high) {
            A[idx++] = aux[j++];
        }
        Assert.assertTrue(isSorted(A, low, high));
    }

    /**
     * Merge Sort, buttom up: Non-recursive
     *
     * Basic Idea:
     *  Pass through array, merging subarrays of size 1
     *  Repeat for subarrays of size 2, 4, 8, 16
     *
     * sz = 1:
     *   merge(a, aux, 0, 0, 1);
     *   merge(a, aux, 2, 2, 3);
     *   merge(a, aux, 4, 4, 5);
     *   merge(a, aux, 6, 6, 7);
     * sz = 2:
     *    merge(a, aux, 0, 1, 3);
     *    merge(a, aux, 4, 5, 7);
     *    merge(a, aux, 8, 9, 11);
     *    merge(a, aux, 12, 13, 15);
     * sz = 4:
     *    merge(a, aux, 0, 3, 7);
     *    merge(a, aux, 8, 11, 15);
     * sz = 8:
     *    merge(a, aux, 0, 7, 15);
    */
    public void mergeSortButtomUp(int[] A, int N) {
        int[] aux = new int[N];
        // first for loop only iterate logN times
        for (int sz = 1; sz < N; sz = sz + sz) {
            for (int low = 0; low < N-sz; low += sz + sz) {
                merge(a, aux, low, low+sz-1, Math.min(low+sz+sz-1, N-1));
            }
        }
    }

    /**
     * Quick Sort:
     *   Java sort for primitive types
     *   C qsort, Unix, Visual C++, Python, Matlab, Chrome JavaScript
     *
     * Partition so that, for some j
     *   entry a[j] is in place
     *   no larger entry to the left of j
     *   no smaller entry to the right of j
     *
     * What we choose the pivot point is super important.
     *    Most quick sort implementation will choose either the 1st or the last element as the pivot
     *    In an ideal quicksort algorithm, the pivot would always be the middle-most element,
     *    so that when we partition the list into sublists, they would be equal in same
     *
     * Phase I. Repeat until i an j pointers across
     *   Scan i from left to right so long as a[i] < a[lo]
     *   Scan j from right to left so long as a[j] > a[lo]
     *   exchange a[i] with a[j]
     *
     * Phase II. When pointers cross
     *   Exchange a[lo] with a[j]
     *
     * Worst: O(N^2) 每次选取的基准都是最大（或最小）的元素，导致每次只划分出了一个分区，需要进行n-1次划分才能结束递归
     * Best: O(NlogN)  每次选取的基准都是中位数，这样每次都均匀的划分出两个分区，只需要logn次划分就能结束递归
     * Average: O(NlogN)
     *
     * Proposition:
     *      The average number of compares Cn to quicksort an array of N distinct keys is ~2NlogN
     *      The number of exchanges is ~1/3 NlogN
     *
     * Space complexity: 主要是递归造成的栈空间的使用(用来保存left和right等局部变量)，取决于递归树的深度 一般为O(logn)，最差为O(n)
     * Unstable:
    */
    public void quickSort(int[] A, int N) {
        // Shuffling is needed for performance gurantee
        StdRandom.shuffle(a);
        quickSort(A, 0, N-1);
    }

    public void quickSort(int[] nums, int low, int high) {
        if (low >= high) {
            return;
        }
        // Item pivot is in place.
        int pivot = partition(nums, low, high);
        // recursively sort the left subarray.
        quickSort(nums, low, pivot-1);
        // recursively sort the right subarray.
        quickSort(nums, pivot+1, high);
    }

    /*
        This function takes last element as pivot, places the pivot element at its correct
        position in sorted array, and places all smaller (smaller than pivot) to left of
        pivot and all greater elements to right of pivot
    */
    public int partition(int[] nums, int low, int high) {
        int pivot = high, i = low; // index of smaller element
        for (int j = low; j < high; j++) {
            // If current element is smaller than or
            // equal to pivot
            if (nums[j] <= nums[pivot]) {
                 // swap nums[i] and nums[j]
                swap(nums, i, j);
                i++;
            }
        }

        // swap nums[i] and nums[high] (or pivot)
        swap(nums, i, pivot);
        return i;
    }

    // use first element as pivot point:
    private int partition(int[] nums, int low, int high) {
        int pivot = low, i = high;
        for (int j = high; j > low; j--) {
            if (nums[j] >= nums[pivot]) {
                swap(nums, i, j);
                i--;
            }
        }
        swap(nums, i, pivot);
        return i;
    }

    // We choose the first item as pivot point:
    public int partition(int[] nums, int low, int high) {
        int i = low, j = high+1;
        while (i < j) {
            // find item on the left to swap
            // Scan i from left to right so long as A[i] < A[low]
            while (nums[++i] < nums[low]) {
                if (i == high) {
                    break;
                }
            }
            // find the item on the right to swap
            // Scan j from right to left so long as A[j] > A[low]
            while (nums[--j] > nums[low]) {
                if (j == low) {
                    break;
                }
            }
            if (i < j) {
                swap(nums, i, j);
            }

        }
        // swap the partition item and the right most of first part of subarray
        swap(nums, low, j);
        // return index of item now known to be in place
        return j;
    }

    /**
     * Heap Sort: https://medium.com/basecs/heapify-all-the-things-with-heap-sort-55ee1c93af82
     *  Basic plan for in-place sort:
     *      Create max-heap with all N keys
     *      Repeatedly remove the maximum keys
     *  Heap construction: Build max heap using bottom-up method
     *
     * HeapSort Algorithm for sorting in increasing order:
     *    1. Build a max heap from input data
     *    2. At this point, the largest item is stored at the root of the heap.
     *       Replace it with the last item of the heap followed by redecing the size of heap by 1.
     *    3. Repeat above steps while size of heap is greater than 1.
     *
     * Worest: O(NlogN)
     * Best: O(NlogN)
     * Average: O(NlogN)
     * Space Complexity: O(1), sort in place
     *
     * Proposition:
     *     Heap construction uses <= 2N compares and exchanges
     *     Heapsort uses <= 2NlogN compares and exchanges
     *
     * Unstable
     *
     * Bottom line:
     *     Inner loop longer than quicksort
     *     Makes poor use of cache memory
     *     Not stable
     *
    */
    // O(NlogN) to build a heap:
    // https://www.geeksforgeeks.org/time-complexity-of-building-a-heap/
    public void heapSort(int[] nums, int N) {
        // build heap (rearrange the array)
        for (int i = N/2-1; i >= 0; i--) {
            heapify(nums, N, i);
        }

        // One by one extract an element from heap
        for (int i = N-1; i >= 0; i--) {
            // Move current root to the end
            swap(nums, 0, i);
            // Call max heapify on the reduced heap
            heapify(nums, i, 0);
        }
    }

    public void heapify(int[] nums, int N, int i) {
        int max = i;
        int left = 2*i + 1;
        int right = 2*i + 2;

        // If left child is larger than root:
        if (left < N && nums[left] > nums[max]) {
            max = left;
        }

        // If right child is larger than root:
        if (right < N && nums[right] > nums[max]) {
            max = right;
        }

        // If largest item is not root:
        if (max != i) {
            // swap the max item with the pointing item
            swap(nums, i, max);

            // recursively heapify the affected sub-tree
            heapify(nums, N, max);
        }
    }

    // Iterative method of heapify
	private void heapify(int[] nums, int N, int i) {
	    while (i < N) {
		    int max = i;
			  int left = 2 * i + 1;
			  int right = 2 * i + 2;
			  if (left < N && nums[left] > nums[max]) {
				  max = left;
			  }
			  if (right < N && nums[right] > nums[max]) {
			  	max = right;
			  }
			  if (max == i) {
				  break;
			  }
			  swap(nums, i, max);
		  	i = max;
		  }
	}

    /**
     * Counting Sort:
     *  Assumption: Keys are Integers between 0 and R-1
     *  Implication. Can use key as an array index
     *
     * Proposition:
     *  Counting Sort uses ~11N + 4K array accesses to sort N items whose keys are integers between 0 and K-1
     *  Counting Sort uses extra space proportional to N+K
     *
     * Stable
     *
     * Worst Time Complexity: O(N+K)
     * Average Time Complexity: O(N+K)
     * Best Time Complexity: O(N+k)
     * Space Complexity: O(N+K)
     *
     * 计数排序的时间复杂度和空间复杂度与数组A的数据范围
     * (A中元素的最大值与最小值的差加上1)有关，因此对于数据范围很大的数组，计数排序需要大量时间和内存。
    */
    public void countingSort(int[] nums, int N) {
        int K = 100;
        int[] count = new int[K+1];
        // 使count[i]保存着等于i的元素个数
        for (int i = 0; i < N; i++) {
            count[nums[i]]++;
        }
        // 使count[i]保存着小于等于i的元素个数，排序后元素i就放在第count[i]个输出位置上
        for (int i = 0; i < K; i++) {
            count[i+1] += count[i];
        }
        int[] aux = new int[N];
        // Scan the array from end to beginning to ensure stablility.
        // 把每个元素A[i]放到它在输出数组B中的正确位置上
        // 当再遇到重复元素时会被放在当前元素的前一个位置上保证计数排序的稳定性
        for (int i = N-1; i >= 0; i--) {
        	int idx = --count[nums[i]];
            aux[idx] = nums[i];
        }
        // Copy array aux back to A
        for (int i = 0; i < N; i++) {
            nums[i] = aux[i];
        }
    }

    /**
     * Least Significant Digit First Radix Sort
     * Consider number from right to left
     * Stably sort using dth number as the key by using counting sort
     * 分类 ------------- 内部非比较排序
     * 数据结构 ---------- 数组
     * 最差时间复杂度 ---- O(N*Dn)
     * 最优时间复杂度 ---- O(N*Dn)
     * 平均时间复杂度 ---- O(N*Dn)
     * 所需辅助空间 ------ O(N*Dn)
     * 稳定性 ----------- 稳定
    */
    public void LsdRadixSort(int[] nums, int N) {
        int dn = 3;  // 待排序的元素为三位数及以下
        int k = 10;  // 基数为10，每一位的数字都是[0,9]内的整数
        for (int d = 1; i <= dn; i++) {
            countingSort(nums, N, d);
        }
    }

    /**
     * 计数排序：适用于取值范围不是很大的情况下，性能是快过用比较的方法的排序：O(NlogN)
     *
     */
    public int[] countingSort(int[] nums) {
        // 得到数列的最大值和最小值：
        int max = nums[0], min = nums[0];
        for (int num : nums) {
            max = Math.max(max, num);
            min = Math.min(min, num);
        }
        // 根据数列的最大值和最小值的绝对值来确定统计数组的长度：
        int[] countArray = new int[max-min+1];
        // 遍历数列，填充统计数组：
        for (int num : nums) {
            countArray[num-min]++;
        }
        // 统计数组做变形，后面的元素等于前面的元素之和：
        int sum = 0;
        for (int i = 0, n = countArray.length; i < n; i++) {
            sum += num;
            countArray[i] = sum;
        }
        // 倒序遍历统计数组，输出结果：
        int[] sortedArray = new int[nums.length];
        for (int n = nums.length, i = n-1; i >= 0; i--) {
            int idx = countArray[array[i]-min] - 1;
            sortedArray[idx] = nums[i];
            countArray[nums[i]-min]--;
        }
        return sortedArray;
    }

    // Solution 2:
    private void countingSort(int[] nums, int N, int d) {
        int K = 10;
        int[] count = new int[K+1];
        // 使count[i]保存着等于i的元素个数
        for (int i = 0; i < N; i++) {
            int digit = getDigit(nums[i], d);
            count[digit]++;
        }
        // 使count[i]保存着小于等于i的元素个数，排序后元素i就放在第count[i]个输出位置上
        for (int i = 0; i < K; i++) {
            count[i+1] += count[i];
        }
        int[] aux = new int[N];
        // Scan the array from end to beginning to ensure stablility.
        // 把每个元素A[i]放到它在输出数组B中的正确位置上
        // 当再遇到重复元素时会被放在当前元素的前一个位置上保证计数排序的稳定性
        for (int i = N-1; i >= 0; i--) {
            int digit = getDigit(nums[i], d);
        	int idx = --count[digit];
            aux[idx] = nums[i];
        }
        // Copy array aux back to A
        for (int i = 0; i < N; i++) {
            nums[i] = aux[i];
        }
    }

    // 获得元素num的第d位数字
    private int getDigit(int num, int d) {
        // 最大为三位数，所以这里只要到百位就满足了
        int radix[] = { 1, 1, 10, 100 };
        return (num / radix[d]) % 10;
    }

}