
public class Solution {
    // Leetcode 163: Missing Ranges
    // https://leetcode.com/problems/missing-ranges
    // https://www.lintcode.com/problem/missing-ranges/description
    //
    /*
    Given a sorted integer array where the range of elements are
    [lower, upper] inclusive, return its missing ranges.

    For example, given [0, 1, 3, 50, 75], lower = 0 and upper = 99,
    return ["2", "4->49", "51->74", "76->99"].
    */
    // Running Time Complexity: O(N), Space Complexity: O(1)
    public List<String> findMissingRanges(int[] nums, int lo, int hi) {
        List<String> res = new ArrayList<>();
        // the start missing range we need to find
        int start = lo;
        for (int num : nums) {
            if (num == start) {
            	start++;
            }
            else if (num > start) {
                int end = num-1;
                res.add(getRange(start, end));
                // to avoid case like [0, 2147483647], that num+1 is overflow.
                if (num == hi) {
                	return res;
                }
                start = num+1;
            }
        }
        if (start <= hi) {
        	res.add(getRange(start, hi));
        }
    	return res;
    }

    public String getRange(int n1, int n2) {
        return (n1 == n2) ? String.valueOf(n1) : String.format("%d->%d", n1, n2);
    }

    // Leetcode 228: Summary Ranges
    // https://leetcode.com/problems/summary-ranges/
    /*
    Given a sorted integer array without duplicates,
    return the summary of its ranges.

    Example 1:

    Input:  [0,1,2,4,5,7]
    Output: ["0->2","4->5","7"]
    Explanation: 0,1,2 form a continuous range; 4,5 form a continuous range.
    Example 2:

    Input:  [0,2,3,4,6,8,9]
    Output: ["0","2->4","6","8->9"]
    Explanation: 2,3,4 form a continuous range; 8,9 form a continuous range.
    */
    // Running Time Complexity: O(N), Space Complexity: O(1)
    public List<String> summaryRanges(int[] nums) {
        List<String> res = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return res;
        }
        int start = nums[0], end = nums[0];
        for (int num : nums) {
            if (num == end) {
                end++;
            }
            else if (num > end) {
                res.add(getRange(start, end-1));
                start = num;
                end = num+1;
            }
        }
        res.add(getRange(start, end-1));
        return res;
    }

    private String getRange(int start, int end) {
        if (start == end) {
            return String.valueOf(start);
        }
        return String.format("%d->%d", start, end);
    }

}