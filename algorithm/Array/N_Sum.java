public class Solution {

    // Leetcode 001: Two Sum:
    // https://leetcode.com/problems/two-sum
    /**
    Given an array of integers, return indices of the two numbers such that they add up to a specific target.
    You may assume that each input would have exactly one solution, and you may not use the same element twice.

    Example:

    Given nums = [2, 7, 11, 15], target = 9,

    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1].
    */
    // Scan the array twice: Running Time Complexity: O(N^2), Space Complexity: O(1)
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        if (nums == null) {
            return res;
        }
        for(int i = 0, n = nums.length; i < n-1; i++){
            for(int j = i+1; j < n; j++){
                int ans = nums[i] + nums[j];
                if(ans == target){
                    res[0] = i;
                    res[1] = j;
                    break;
                }
            }
        }
        return res;
    }
    // Use HashMap to store current value and its index:
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        if (nums == null) {
            return res;
        }
        // key -> pre item's value, value -> prev item's index.
        Map<Integer, Integer> map = new LinkedHashMap<>();
        for (int i = 0, n = nums.length; i < n; i++) {
            int num = nums[i];
            int diff = target - nums[i];
            if (!map.containsKey(diff)) {
                map.put(num, i);
            }
            else {
                res[0] = map.get(diff);
                res[1] = i;
                break;
            }
        }
        return res;
    }

    // Leetcode 167: Two Sum II - Input array is sorted
    // https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
    /**
    Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.
    The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2.

    Note:

    Your returned answers (both index1 and index2) are not zero-based.
    You may assume that each input would have exactly one solution and you may not use the same element twice.
    Example:

    Input: numbers = [2,7,11,15], target = 9
    Output: [1,2]
    Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
    */
    // Two pointers solution. Running Time Complexity: O(N)
    public int[] twoSum(int[] nums, int target) {
        int[] res = {-1, -1};
        if (nums == null) {
            return res;
        }
        int max = nums.length, i = 0, j = max-1;
        while (i < j) {
            int sum = nums[i] + nums[j];
            if (sum == target) {
                res[0] = i+1;
                res[1] = j+1;
                break;
            }
            else if (sum < target) {
                i++;
            }
            else {
                j--;
            }
        }
        return res;
    }

    // Use Binary Search, For each index, binary search its target on the right.
    // Running Time Complexity: O(NlogN)
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        for (int i = 0, n = nums.length; i < n-1; i++) {
            int diff = target - nums[i];
            int low = i+1, high = n-1;
            while (low <= high) {
                int mid = low + (high - low)/2;
                if (nums[mid] == diff) {
                    res[0] = i+1;
                    res[1] = mid+1;
                    break;
                }
                if (nums[mid] < diff) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
        }
        return res;
    }

    // Use Binary search for finding first appearence of item in an array, which may have duplicates
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        for (int i = 0, max = nums.length; i < max-1; i++) {
            int low = i+1, high = n-1;
            int diff = target - nums[i];
            while (low <= high) {
                int mid = low + (high - low)/2;
                if (nums[mid] == diff) {
                    res[0] = i+1;
                    res[1] = low+1;
                    break;
                }
                else if (nums[mid] < diff) {
                    low = mid + 1;
                }
                else  {
                    high = mid - 1;
                }
            }
        }
        return res;
    }

    // Naive way to solve this problem:
    // Running Time Complexity: O(N^2)
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        for(int i = 0, n = nums.length; i < n-1; i++){
            for(int j = i+1; j < n; j++){
                int ans = nums[i] + nums[j];
                if(ans == target){
                    result[0] = i+1;
                    result[1] = j+1;
                    break;
                }
            }
        }
        return result;
    }

    // Leetcode 170: Two Sum III Data Structure Design:
    // https://leetcode.com/problems/two-sum-iii-data-structure-design
    // https://www.lintcode.com/problem/two-sum-iii-data-structure-design/
    /*
    Design and implement a TwoSum class. It should support the following operations: add and find.

    add - Add the number to an internal data structure.
    find - Find if there exists any pair of numbers which sum is equal to the value.

    For example,
    add(1); add(3); add(5);
    find(4) -> true
    find(7) -> false
    */
    // Key -> number, value -> its occurence:
    // Running Time Complexity of add: O(1)
    // Running Time Complexity of find: O(N), if we have N distinct values. Space Complexity: O(1)
    Map<Integer, Integer> map;
    public TwoSum() {
        map = new HashMap<Integer, Integer>();
    }

    public void add(int number) {
        map.put(number, map.getOrDefault(number, 0) + 1);
    }

    public boolean find(int value) {
        for (int key : map.keySet()) {
            int diff = value - key;
            // Example: add(2), find(4), diff = 2,
            if (key != diff && map.containsKey(diff)) {
                return true;
            }
            // Example: add(2), add(2), find(4), diff = 2
            else if (key == diff && map.get(key) > 1) {
                return true;
            }
        }
        return false;
    }

    // Naive Solution:
    // List of nums as storage, and solution of two sum I.
    // Running Time Complexity of add: O(1)
    // Running Time Complexity of find: O(N), Space Complexity: O(N)
    private List<Integer> nums = new ArrayList<>();
    /**
     * @param number: An integer
     * @return: nothing
     */
    public void add(int number) {
        // write your code here
        nums.add(number);
    }

    /**
     * @param value: An integer
     * @return: Find if there exists any pair of numbers which sum is equal to the value.
     */
    public boolean find(int target) {
        // write your code here
        Set<Integer> map = new HashSet<>();
        for (int i = 0, max = nums.size(); i < max; i++) {
            int diff = target - nums.get(i);
            if (map.contains(diff)) {
                return true;
            }
            else {
                map.add(nums.get(i));
            }
        }
        return false;
    }

    // Leetcode 653 Two Sum IV - Input is a BST:
    // https://leetcode.com/problems/two-sum-iv-input-is-a-bst/
    /**
    Given a Binary Search Tree and a target number, return true if there exist two elements in the BST
    such that their sum is equal to the given target.
    Example 1:
    Input:
    5
   / \
  3   6
 / \   \
2   4   7

    Target = 9

    Output: True
    Example 2:
    Input:
    5
   / \
  3   6
 / \   \
2   4   7

    Target = 28

    Output: False
*/
    // Running Time Complexity: O(Nh), Space Complexity: O(N) for recurrsion
    // Each node will be the baseNode, for each baseNode, it will go through the height of the tree.
    // So that the running time complexity is O(Nh)
    public boolean findTarget(TreeNode root, int k) {
        return findTarget(root, root, k);
    }

    private boolean findTarget(TreeNode baseNode, TreeNode currNode, int k) {
        if (baseNode == null) {
            return false;
        }
        return findTarget(baseNode.left, currNode, k) || findTarget(baseNode.right, currNode, k) ||
            find(baseNode, currNode, k-baseNode.val);
    }

    private boolean find(TreeNode baseNode, TreeNode currNode, int diff) {
        if (currNode != null && currNode != baseNode) {
            if (currNode.val == diff) {
                return true;
            }
            else if (currNode.val < diff) {
                return find(baseNode, currNode.right, diff);
            }
            else {
                return find(baseNode, currNode.left, diff);
            }
        }
        return false;
    }

    // Solution 3: Use inorder traversal to get all nums into a list, and use normal two sum solution to solve it.
    public boolean findTarget(TreeNode root, int k) {
        List<Integer> nums = new ArrayList<Integer>();
        // Inorder Traversal: left, val, right
        Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
        TreeNode currNode = root;
        while (!stack.isEmpty() || currNode != null) {
            while (currNode != null) {
                stack.push(currNode);
                currNode = currNode.left;
            }
            currNode = stack.pop();
            nums.add(currNode.val);
            currNode = currNode.right;
        }

        int low = 0, high = nums.size() - 1;
        while (low < high) {
            int sum = nums.get(low) + nums.get(high);
            if (sum == k) {
                return true;
            }
            else if (sum < k) {
                low++;
            }
            else {
                high--;
            }
        }
        return false;
    }

    // Lintcode 587 Two Sum - Unique Pairs:
    // https://www.lintcode.com/problem/two-sum-unique-pairs/
    /**
    Given an array of integers, find how many unique pairs in the array such that their sum is equal to a specific target number. Please return the number of pairs.
    Example
    Example 1:

    Input: nums = [1,1,2,45,46,46], target = 47
    Output: 2
    Explanation:

    1 + 46 = 47
    2 + 45 = 47

    Example 2:

    Input: nums = [1,1], target = 2
    Output: 1
    Explanation:
    1 + 1 = 2
    */
    // Running Time Complexity:
    public int twoSumCountUniquePairs(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        Set<Integer> set = new HashSet<>();
        Arrays.sort(nums);
        for (int i = 0, n = nums.length; i < n; i++) {
            int num = nums[i];
            int diff = target - num;
            if (!map.containsKey(diff)) {
                map.put(num, i);
            }
            else {
                set.add(num);
            }
        }
        return set.size();
    }

    // Leetcode 15: 3 Sum:
    // https://leetcode.com/problems/3sum
    /**
    Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

    Note:

    The solution set must not contain duplicate triplets.

    Example:

    Given array nums = [-1, 0, 1, 2, -1, -4],

    A solution set is:
    [
    [-1, 0, 1],
    [-1, -1, 2]
    ]
    */
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length < 3) {
            return result;
        }
        Arrays.sort(nums);
        int target = 0;
        for (int i = 0, max = nums.length; i < max-2; i++) {
            if (i > 0 && nums[i] == nums[i-1]) {
                continue;
            }
            int j = i+1, k = max-1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == target) {
                    result.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    while (j < k && nums[j] == nums[j+1]) {
                        j++;
                    }
                    while (j < k && nums[k] == nums[k-1]) {
                        k--;
                    }
                    j++; k--;
                }
                else if (sum < target) {
                    j++;
                }
                else {
                    k--;
                }
            }
        }
        return result;
    }
    // Use Set to avoid duplicate answers:
    public List<List<Integer>> threeSum(int[] nums) {
        int target = 0;
        Set<List<Integer>> result = new HashSet<List<Integer>>();
        if (nums == null || nums.length < 3) {
            return new ArrayList<List<Integer>>(result);
        }
        Arrays.sort(nums);
        for (int i = 0, n = nums.length; i < n-2; i++) {
            int low = i+1, high = n-1;
            while (low < high) {
                int sum = nums[i] + nums[low] + nums[high];
                if (sum == target) {
                    result.add(Arrays.asList(nums[i], nums[low], nums[high]));
                    low++; high--;
                }
                else if (sum < target) {
                    low++;
                }
                else {
                    high--;
                }
            }
        }
        return new ArrayList<List<Integer>>(result);
    }

    // Leetcode 16: 3Sum Closest:
    // https://leetcode.com/problems/3sum-closest/
    /**
    * Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers.
    *   You may assume that each input would have exactly one solution.

        For example, given array S = {-1 2 1 -4}, and target = 1.

        The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
    **/
    // Running Time O(n^2), Space(1)
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        // Initilize result, cannot use Integer.MAX_VALUE because it may max out int in result-target when target is negative.
        int res = nums[0] + nums[1] + nums[2];
        for (int i = 0, max = nums.length; i < max-2; i++) {
            if (i > 0 && nums[i] == nums[i-1]) {
                i++;
            }
            int j = i+1, k = max-1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == target) {
                    res = sum;
                    break;
                }
                else if (sum < target) {
                    while (j < k && nums[j] == nums[j+1]) {
                        j++;
                    }
                    j++;
                }
                else {
                    while (j < k && nums[k] == nums[k-1]) {
                        k--;
                    }
                    k--;
                }
                if (Math.abs(sum - target) < Math.abs(res - target)) {
                    res = sum;
                }
            }
        }
        return res;
    }

    // It may not need to consider duplicate cases
    public int threeSumClosest(int[] nums, int target) {
        if (nums == null || nums.length < 3) {
            return -1;
        }
        Arrays.sort(nums);
        int closestTarget = nums[0] + nums[1] + nums[2];
        for (int i = 0, max = nums.length; i < max-2; i++) {
            int j = i+1, k = max - 1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum < target) {
                    j++;
                }
                else {
                    k--;
                }
                if (Math.abs(target-sum) < Math.abs(target-closestTarget)) {
                    closestTarget = sum;
                }
            }
        }
        return closestTarget;
    }

    // Leetcode 259: Three Sum Smaller:
    // https://leetcode.com/problems/3sum-smaller
    /**
    Given an array of n integers nums and a target, find the number of index triplets i, j, k with 0 <= i < j < k < n that satisfy the condition nums[i] + nums[j] + nums[k] < target.

    Example
    Given nums = [-2,0,1,3], target = 2, return 2.

    Explanation:
    Because there are two triplets which sums are less than 2:
    [-2, 0, 1]
    [-2, 0, 3]
    */
    // Iterate the array 3 times:
    // Running Time Complexity: O(N^3), Space Complexity: O(1)
    public int threeSumSmaller(int[] nums, int target) {
        if (nums == null || nums.length < 3) {
            return 0;
        }
        Arrays.sort(nums);
        int count = 0;
        for (int i = 0, n = nums.length; i < n-2; i++) {
            for (int j = i+1; j < n-1; j++) {
                for (int k = j+1; k < n; k++) {
                    int sum = nums[i] + nums[j] + nums[k];
                    if (sum < target) {
                        count++;
                    }
                }
            }
        }
        return count;
    }
    // Running time: O(n^2) Space Complexity O(1)
    public int threeSumSmaller(int[] nums, int target) {
        if (nums == null || nums.length < 3) {
            return 0;
        }
        Arrays.sort(nums);
        int cnt = 0;
        for (int i = 0, max = nums.length; i < max-2; i++) {
            int low = i+1, high = max-1;
            while (low < high) {
                int sum = nums[i] + nums[low] + nums[high];
                // i 和 left 不动，介于 left 和 right (包括 right) 之间的所有元素 sum 也一定小于 target 的单调性。
                if (sum < target) {
                    cnt += high - low;
                    low++;
                }
                else {
                    high--;
                }
            }
        }
        return cnt;
    }

    // Leetcode 923. 3Sum With Mulitplicity:
    /**
    Given an integer array A, and an integer target, return the number of tuples i, j, k  such that i < j < k and A[i] + A[j] + A[k] == target.
    As the answer can be very large, return it modulo 10^9 + 7.

    Example 1:

    Input: A = [1,1,2,2,3,3,4,4,5,5], target = 8
    Output: 20
    Explanation:
    Enumerating by the values (A[i], A[j], A[k]):
    (1, 2, 5) occurs 8 times;
    (1, 3, 4) occurs 8 times;
    (2, 2, 4) occurs 2 times;
    (2, 3, 3) occurs 2 times.
    Example 2:

    Input: A = [1,1,2,2,2,2], target = 5
    Output: 12
    Explanation:
    A[i] = 1, A[j] = A[k] = 2 occurs 12 times:
    We choose one 1 from [1,1] in 2 ways,
    and two 2s from [2,2,2,2] in 6 ways.
    */
    // Running Time Complexity: O(N^2), Space Complexity: O(N)
    public int threeSumMulti(int[] A, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        int res = 0;
        int mod = 1000000007;
        for (int i = 0, max = A.length; i < max; i++) {
            int diff = target - A[i];
            res = (res + map.getOrDefault(diff, 0)) % mod;
            for (int j = 0; j < i; j++) {
                int sum = A[i] + A[j];
                map.put(sum, map.getOrDefault(sum, 0) + 1);
            }
        }
        return res;
    }

    // Leetcode 018: 4Sum:
    // https://leetcode.com/problems/4sum/
    /*
    Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.

    Note:
    Elements in a quadruplet (a,b,c,d) must be in non-descending order. (ie, a ≤ b ≤ c ≤ d)
    The solution set must not contain duplicate quadruplets.
        For example, given array S = {1 0 -1 0 -2 2}, and target = 0.

        A solution set is:
        (-1,  0, 0, 1)
        (-2, -1, 1, 2)
        (-2,  0, 0, 2)
    */
    // Running Time Complexity: O(n^3) running time
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums == null) {
            return res;
        }
        Arrays.sort(nums);
        for (int i = 0, max = nums.length; i < max-3; i++) {
            // Skip the duplicates:
            if (i > 0 && nums[i] == nums[i-1]) {
                continue;
            }
            // Three Sum:
            for (int j = i+1; j < max-2; j++) {
                // Skip the duplicates:
                if (j > i+1 && nums[j] == nums[j-1]) {
                    continue;
                }
                int low = j+1, high = max-1;
                while (low < high) {
                    int sum = nums[i] + nums[j] + nums[low] + nums[high];
                    if (sum == target) {
                        // Skip the duplicates:
                        res.add(Arrays.asList(nums[i], nums[j], nums[low], nums[high]));
                        while (low < high && nums[low] == nums[low+1]) {
                        	low++;
                        }
                        // Skip the duplicates:
                        while (low < high && nums[high] == nums[high-1]) {
                        	high--;
                        }
                        low++; high--;
                    }
                    else if (sum < target) {
                        low++;
                    }
                    else {
                        high--;
                    }
                }
            }
        }
        return res;
    }

    // Leetcode 454 Four Sum II:
    // https://leetcode.com/problems/4sum-ii/
    /**
    Given four lists A, B, C, D of integer values, compute how many tuples (i, j, k, l) there are such that A[i] + B[j] + C[k] + D[l] is zero.

    To make problem a bit easier, all A, B, C, D have same length of N where 0 ≤ N ≤ 500. All integers are in the range of -228 to 228 - 1 and the result is guaranteed to be at most 231 - 1.

    Example:

    Input:
    A = [ 1, 2]
    B = [-2,-1]
    C = [-1, 2]
    D = [ 0, 2]

    Output:
    2

    Explanation:
    The two tuples are:
    1. (0, 0, 0, 1) -> A[0] + B[0] + C[0] + D[1] = 1 + (-2) + (-1) + 2 = 0
    2. (1, 1, 0, 0) -> A[1] + B[1] + C[0] + D[0] = 2 + (-1) + (-1) + 0 = 0
    */
    // Running Time Complexity: O(N^2)
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        int count = 0, target = 0;
        // key -> sum of A[i] + B[i], val -> freq
        Map<Integer, Integer> map = new HashMap<>();
        for (int num1 : A) {
            for (int num2 : B) {
                int sum = num1 + num2;
                map.put(sum, map.getOrDefault(sum,0) + 1);
            }
        }
        int cnt = 0;
        int target = 0;
        for (int num1 : C) {
            for (int num2 : D) {
                int sum = C[i] + D[j];
                if (map.containsKey(sum)) {
                    count += map.get(sum);
                }
            }
        }
        return count;
    }
}