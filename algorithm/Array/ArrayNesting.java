public class Solution {
    // Leetcode 565:
    // https://leetcode.com/problems/array-nesting/
    /**
    A zero-indexed array A of length N contains all integers from 0 to N-1.
    Find and return the longest length of set S, where S[i] = {A[i], A[A[i]], A[A[A[i]]], ... } subjected to the rule below.

    Suppose the first element in S starts with the selection of element A[i] of index = i,
    the next element in S should be A[A[i]], and then A[A[A[i]]]…
    By that analogy, we stop adding right before a duplicate element occurs in S.

    Example 1:

    Input: A = [5,4,0,3,1,6,2]
    Output: 4
    Explanation:
    A[0] = 5, A[1] = 4, A[2] = 0, A[3] = 3, A[4] = 1, A[5] = 6, A[6] = 2.

    One of the longest S[K]:
    S[0] = {A[0], A[5], A[6], A[2]} = {5, 6, 2, 0}
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int arrayNesting(int[] nums) {
        int n = nums.length;
        boolean[] visited = new boolean[n];
        int maxLen = 0;
        for (int num : nums) {
            int cnt = 0;
            while (!visited[num]) {
                cnt++;
                visited[num] = true;
                num = nums[num];
            }
            maxLen = Math.max(maxLen, cnt);
        }
        return maxLen;
    }

    // Leetcode 339: Nested List Weight Sum:
    // https://leetcode.com/problems/nested-list-weight-sum/
    // https://www.lintcode.com/problem/nested-list-weight-sum/
    /**
    Given a nested list of integers, return the sum of all integers in the list weighted by their depth.

    Each element is either an integer, or a list -- whose elements may also be integers or other lists.

    Example 1:

    Input: [[1,1],2,[1,1]]
    Output: 10
    Explanation: Four 1's at depth 2, one 2 at depth 1.
    Example 2:

    Input: [1,[4,[6]]]
    Output: 27
    Explanation: One 1 at depth 1, one 4 at depth 2, and one 6 at depth 3; 1 + 4*2 + 6*3 = 27.
    */
    // Recursive:
    public int depthSum(List<NestedInteger> nestedList) {
        int sum = 0;
        sum = depthSum(nestedList, 1);
        return sum;
    }

    private int depthSum(List<NestedInteger> nestedList, int depth) {
        if (nestedList == null || nestedList.isEmpty()) {
            return 0;
        }
        int sum = 0;
        for (NestedInteger ni : nestedList) {
            if (ni.isInteger()) {
                sum += ni.getInteger() * depth;
            }
            else {
                sum += depthSum(ni.getList(), depth+1);
            }
        }
        return sum;
    }

    // Iterative: level order traversal O(n), each node visit once
    public int depthSum(List<NestedInteger> nestedList) {
        int sum = 0, level = 1;
        Queue<NestedInteger> queue = new LinkedList<>(nestedList);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (ni.isInteger()) {
                    sum += ni.getInteger() * level;
                }
                else {
                    queue.addAll(ni.getList());
                }
            }
            level++;
        }
        return sum;
    }

    // Leetcode 364: Nested List Weight Sum II:
    // https://leetcode.com/problems/nested-list-weight-sum-ii
    // https://www.lintcode.com/problem/nested-list-weight-sum-ii
    /**
    Given a nested list of integers, return the sum of all integers in the list weighted by their depth.

    Each element is either an integer, or a list -- whose elements may also be integers or other lists.

    Different from the previous question where weight is increasing from root to leaf, now the weight is defined from bottom up. i.e., the leaf level integers have weight 1, and the root level integers have the largest weight.

    Example 1:

    Input: [[1,1],2,[1,1]]
    Output: 8
    Explanation: Four 1's at depth 1, one 2 at depth 2.
    Example 2:

    Input: [1,[4,[6]]]
    Output: 17
    Explanation: One 1 at depth 3, one 4 at depth 2, and one 6 at depth 1; 1*3 + 4*2 + 6*1 = 17.
    */
    // Running Time Complexity: O(N), Space Complexity: O(N),
    // Iterate the nestedInteger list twice
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int depth = calculateDepthOfNestedList(nestedList);
        int sum = 0;
        Queue<NestedInteger> queue = new LinkedList<>();
        queue.addAll(nestedList);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (ni.isInteger()) {
                    sum += ni.getInteger() * depth;
                }
                else {
                    queue.addAll(ni.getList());
                }
            }
            depth--;
        }
        return sum;
    }

    private int calculateDepthOfNestedList(List<NestedInteger> nestedList) {
        Queue<NestedInteger> queue = new LinkedList<>();
        queue.addAll(nestedList);
        int depth = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (!ni.isInteger()) {
                    queue.addAll(ni.getList());
                }
            }
            depth++;
        }
        return depth;
    }

    // Best Solution: Use two int variables
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int weighted = 0, unweighted = 0;
        Queue<NestedInteger> queue = new LinkedList<>();
        queue.addAll(nestedList);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                NestedInteger ni = queue.poll();
                if (ni.isInteger()) {
                    unweighted += ni.getInteger();
                }
                else {
                    queue.addAll(ni.getList());
                }
            }
            weighted += unweighted;
        }
        return weighted;
    }

    // Leetcode 341: Flatten Nested List Iterator:
    // https://leetcode.com/problems/flatten-nested-list-iterator/
    /**
    Given a nested list of integers, implement an iterator to flatten it.

    Each element is either an integer, or a list -- whose elements may also be integers or other lists.

    Example 1:

    Input: [[1,1],2,[1,1]]
    Output: [1,1,2,1,1]
    Explanation: By calling next repeatedly until hasNext returns false,
                the order of elements returned by next should be: [1,1,2,1,1].
    Example 2:

    Input: [1,[4,[6]]]
    Output: [1,4,6]
    Explanation: By calling next repeatedly until hasNext returns false,
                the order of elements returned by next should be: [1,4,6].
    */
    public class NestedIterator implements Iterator<Integer> {
        private Stack<NestedInteger> stack;
        public NestedIterator(List<NestedInteger> nestedList) {
            this.stack = new Stack<>();
            for (int max = nestedList.size(), i = max-1; i >= 0; i--) {
                stack.push(nestedList.get(i));
            }
        }

        @Override
        public Integer next() {
            return stack.pop().getInteger();
        }

        @Override
        public boolean hasNext() {
            while (!stack.isEmpty()) {
                NestedInteger ni = stack.peek();
                if (ni.isInteger()) {
                    return true;
                }
                stack.pop();
                List<NestedInteger> list = ni.getList();
                for (int max = list.size(), i = max-1; i >= 0; i--) {
                    stack.push(list.get(i));
                }
            }
            return false;
        }
    }
}