/**
1 billion integers, 10^9
only 1 MB ram

1 integer = 4 bytes.

1 MB = 10^6 bytes = 1000000 / 4 = 250000 ints

10^9 / 10^6 / 4 = 4 * 1000 = 4000 partitions

https://www.youtube.com/watch?v=Bp7fGofslng

N-way merge

1. For each 4000 partitions (1MB each), sort them in RAM and write them to disk

2. for each partition, load a small part to RAM (60 integers), and have 10000 ints spaces as buffer to write the N-way merge result. Once this buffer is full, we write it to disk

3. Whenever each partition is empty in RAM, we read it from disk to fill it up until all the partition files have been read through.
*/