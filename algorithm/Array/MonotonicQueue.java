public class Solution {
    /**
    单调栈：
    Leetcode 42: Trapping Rain Water
    https://leetcode.com/problems/trapping-rain-water/

    Given n non-negative integers representing an elevation map where the width of each bar is 1, 
    compute how much water it can trap after raining.

    Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
    Output: 6
    Explanation: The above elevation map (black section) is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped.
    Example 2:

    Input: height = [4,2,0,3,2,5]
    Output: 9

    */
    /**
    1. 单调递减栈:
        -- 当后面的柱子比签名的低时候，是无法接雨水的
        -- 当找到一根比前面高的柱子时候，就可以计算接到的雨水
    2. 对更低的柱子入栈:
        -- 更低的柱子以为这后面如果能找到高柱子，这里就能接到雨水，所以入栈把它保存起来
        -- 平地相当于高度为0的柱子
    3. 当出现高于栈顶的柱子时候:
        -- 说明可以对前面的柱子结算了
        -- 计算已经到手的雨水，然后出栈前面更低的柱子
    4. 计算雨水的时候需要注意:
        -- 雨水区域的右边 r 是指当前index i
        -- 底部是栈顶stack.peek() 因为遇到了更高的右边，所以它即将出栈，用curr 来记录它
        -- 左边 l 就是新的栈顶 stack.peek()
        -- 雨水的区域确定了，水坑的高度就是左右两边更低的一遍减去底部，宽度是在左右中间
        -- 使用乘法即可计算面积
    */
    public int trap(int[] height) {
        int n = height.length;
        int totalArea = 0;
        Stack<Integer> s = new Stack<>();
        for (int i = 0; i < n; i++) {
            while (!s.isEmpty() && height[s.peek()] < height[i]) {
                int curr = s.pop();
                if (s.isEmpty()) {
                    break;
                }
                int l = s.peek();
                int r = i;
                int h = Math.min(height[l], height[r]) - height[curr];
                totalArea += (r - l - 1) * h;
            }
            s.push(i);
        }
        return totalArea;
    }

    /*
        https://leetcode.com/problems/shortest-unsorted-continuous-subarray/
        https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/
        https://leetcode.com/problems/next-greater-element-i/
        https://leetcode.com/problems/largest-rectangle-in-histogram/
        https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
    */

    // Leetcode 239: Sliding Windown Maximum:
    // https://leetcode.com/problems/sliding-window-maximum/
    /**
    Given an array nums, there is a sliding window of size k which is moving from the very left of the array to the very right.
    You can only see the k numbers in the window. Each time the sliding window moves right by one position. Return the max sliding window.

    Follow up:
    Could you solve it in linear time?

    Example:

    Input: nums = [1,3,-1,-3,5,3,6,7], and k = 3
    Output: [3,3,5,5,6,7]
    Explanation:

    Window position                Max
    ---------------               -----
    [1  3  -1] -3  5  3  6  7       3
    1 [3  -1  -3] 5  3  6  7       3
    1  3 [-1  -3  5] 3  6  7       5
    1  3  -1 [-3  5  3] 6  7       5
    1  3  -1  -3 [5  3  6] 7       6
    1  3  -1  -3  5 [3  6  7]      7
    */
    class MaxQueue {
        Deque<Integer> queue = new ArrayDeque<>();
        // Decreasing Monotonic queue, the max item always on the leftmost
        public void push(int num) {
            while (!queue.isEmpty() && queue.peekLast() < num) {
                queue.pollLast();
            }
            queue.offerLast(num);
        }

        public int getFront() {
            return queue.peekFirst();
        }

        public void popMaxIfExists(int num) {
            if (num != queue.peekFirst()) {
                return;
            }
            queue.pollFirst();
        }
    }

    public int[] maxSlidingWindow(int[] nums, int k) {
        int n = nums.length;
        int[] res = new int[n-k+1];
        int idx = 0;
        MaxQueue mq = new MaxQueue();
        for (int i = 0, max = nums.length; i < max; i++) {
            // 1 to k-2 items
            int num = nums[i];
            mq.push(num);
            if (i >= k-1) {
                res[idx++] = mq.getFront();
                int leftItem = nums[i-k+1];
                // In next iteration, the leftItem will be exclude.
                // If it is the maximum, we need to pop it from the deque.
                mq.popMaxIfExists(leftItem);
            }
        }
        return res;
    }

    /**
    Leetcode 496 Next Greater Element I:
    // https://leetcode.com/problems/next-greater-element-i/

    You are given two arrays (without duplicates) nums1 and nums2 where nums1’s elements are subset of nums2. 
    Find all the next greater numbers for nums1's elements in the corresponding places of nums2.

    The Next Greater Number of a number x in nums1 is the first greater number to its right in nums2. If it does not exist, output -1 for this number.

    Example 1:
    Input: nums1 = [4,1,2], nums2 = [1,3,4,2].
    Output: [-1,3,-1]
    Explanation:
        For number 4 in the first array, you cannot find the next greater number for it in the second array, so output -1.
        For number 1 in the first array, the next greater number for it in the second array is 3.
        For number 2 in the first array, there is no next greater number for it in the second array, so output -1.
    Example 2:
    Input: nums1 = [2,4], nums2 = [1,2,3,4].
    Output: [3,-1]
    Explanation:
        For number 2 in the first array, the next greater number for it in the second array is 3.
        For number 4 in the first array, there is no next greater number for it in the second array, so output -1.
    Note:
    All elements in nums1 and nums2 are unique.
    The length of both nums1 and nums2 would not exceed 1000.
    */
    // Use monotonic stack: O(N):
    // 单调栈
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        
        Map<Integer, Integer> map = new HashMap<>();
        Stack<Integer> stack = new Stack<>();
        // monotonic stack
        // 倒着往栈里放
        for (int i = n-1; i >= 0; i--) {
            int num = nums2[i];
            // 判定个子高矮
            while (!stack.isEmpty() && stack.peek() <= num) {
                stack.pop();
            }
            // 这个元素身后的第一个高个
            int max = stack.isEmpty() ? -1 : stack.peek();
            map.put(num, max);
            //进队，接受之后的身高判定吧！
            stack.push(num);
        }
        
        int[] res = new int[m];
        for (int i = 0; i < m; i++) {
            int num = nums1[i];
            res[i] = map.get(num);
        }
        return res;
    }

    /**
    Leetcode 503: Next Greater Element II:
    https://leetcode.com/problems/next-greater-element-ii/

    Given a circular array (the next element of the last element is the first element of the array), 
    print the Next Greater Number for every element. The Next Greater Number of a number x is the first greater number to its traversing-order next in the array, 
    which means you could search circularly to find its next greater number. If it doesn't exist, output -1 for this number.

    Example 1:
    Input: [1,2,1]
    Output: [2,-1,2]
    Explanation: The first 1's next greater number is 2; 
    The number 2 can't find next greater number; 
    The second 1's next greater number needs to search circularly, which is also 2.

    Example 2:
    Input: [2,1,2,4,3]
    Output: [4,2,4,-1,4]

    Note: The length of given array won't exceed 10000.
    */
    // Monotonic Stack: Run time Complexity: O(N)
    public int[] nextGreaterElements(int[] nums) {
        int n = nums.length;
        int[] res = new int[n];
        
        // double the array:
        // [2,1,2,4,3, 2,1,2,4,3]
        Stack<Integer> stack = new Stack<>();
        for (int i = 2 * n -1; i >= 0; i--) {
            int idx = i % n;
            int num = nums[idx];
            while (!stack.isEmpty() && stack.peek() <= num) {
                stack.pop();
            }
            int max = stack.isEmpty() ? -1 : stack.peek();
            res[idx] = max;
            stack.push(num);
        }
        
        return res;
    }

    /**
    Leetcode 739: Daily Temperatures:
    https://leetcode.com/problems/daily-temperatures/

    Given a list of daily temperatures T, return a list such that, for each day in the input, 
    tells you how many days you would have to wait until a warmer temperature. 
    If there is no future day for which this is possible, put 0 instead.

    For example, given the list of temperatures T = [73, 74, 75, 71, 69, 72, 76, 73], 
    your output should be [1, 1, 4, 2, 1, 1, 0, 0].

    Note: The length of temperatures will be in the range [1, 30000]. 
    Each temperature will be an integer in the range [30, 100].

    */
    // Monotonic Stack:
    public int[] dailyTemperatures(int[] T) {
        int n = T.length;
        int[] res = new int[n];
        
        Stack<Integer> stack = new Stack<>();
        for (int i = n-1; i >= 0; i--) {
            int num = T[i];
            while (!stack.isEmpty() && T[stack.peek()] <= num) {
                stack.pop();
            }
            res[i] = stack.isEmpty() ? 0 : stack.peek()-i;
            stack.push(i);
        }
        
        return res;
    }
}