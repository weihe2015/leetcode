public class Solution {
    /** Note: This is a type of question that uses prefix sum method to be solved.
        Map<Integer, Integer> map = new HashMap<>();
        where it uses current cumulative sum as key: value can be anything which satisfy the question.
        Ex:
        array: [1, 2, 3, 4. 5]
        index 0 to 0, currSum = 1
        index 0 to 1, currSum = 3
        index 0 to 2, currSum = 6
        index 0 to 3, currSum = 10
        index 0 to 4, currSum = 15

        So if any subarray sum is 7, it means sum_i = 10, k = 7, sum_j = sum_i - k = 3
        we only needs to know if sum_j is in the map. If it is, we know there is a subarray whose sum is 7

        SUM_i - SUM_j = k, which means elements between i and j are the subarray whose sum is equals to k.

        Note: remember to handle the subarray which starts from index 0 by either the following:
         1. if (sum_i == k) { xxx }, which satisfy the requirement
         2. adding map.put(0, -1); // add this fake entry to make sum from 0 to j consistent
    */

    // Leetcode 325: Maximumn Size Subarray Sum Equals k
    // https://leetcode.com/problems/maximum-size-subarray-sum-equals-k
    // https://www.lintcode.com/problem/maximum-size-subarray-sum-equals-k/
    /**
     * Example 1:
    Given nums = [1, -1, 5, -2, 3], k = 3,
    return 4. (because the subarray [1, -1, 5, -2] sums to 3 and is the longest)

    Example 2:
    Given nums = [-2, -1, 2, 1], k = 1,
    return 2. (because the subarray [-1, 2] sums to 1 and is the longest)
    */
     // SUM_i - SUM_j = k, which means elements between i and j are the subarray whose sum is equals to k.
	 public int maxSubArrayLen(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int sum_i = 0, maxLen = -1;
        // key -> sum of array from 0 to i, value -> index i
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, n = nums.length; i < n; i++) {
            sum_i += nums[i];
            // SUM_i - SUM_j = k
            int sum_j = sum_i - k;
            // This code is for counting first i+1 subarray if its sum is equals to k
            // Ex: {1, -1, 5, -2, 3}, first 4 items sum is 3, so set max = 4
            if (sum_j == 0) {
            	maxLen = Math.max(maxLen, i + 1);
            }
            if (map.containsKey(sum_j)) {
                int j = map.get(sum_j);
                maxLen = Math.max(maxLen, i-j);
            }
            // keep only 1st duplicate as we want first index as left as possible
            if (!map.containsKey(sum_i)) {
                map.put(sum_i, i);
            }
        }
        return maxLen == -1 ? 0 : maxLen;
    }

    public int maxSubArrayLen(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int sum_i = 0, maxLen = -1;
        Map<Integer, Integer> map = new HashMap<>();
        // This code is for counting first i+1 subarray if its sum is equals to k
        // Ex: {1, -1, 5, -2, 3}, first 4 items sum is 3, so set max = 4
        map.put(0, -1);
        for (int i = 0, n = nums.length; i < n; i++) {
            sum_i += nums[i];

            // SUM_i - SUM_j = k
            int sum_j = sum_i - k;

            if (map.containsKey(sum_j)) {
                int j = map.get(sum_j);
                maxLen = Math.max(maxLen, i-j);
            }
            // keep only 1st duplicate as we want first index as left as possible
            if (!map.containsKey(sum_i)) {
                map.put(sum_i, i);
            }

        }
        return maxLen == -1 ? 0 : max;
    }

    // Path Sum III
    // Leetcode 437: https://leetcode.com/problems/path-sum-iii/
    /**
     * You are given a binary tree in which each node contains an integer value.

       Find the number of paths that sum to a given value.

       The path does not need to start or end at the root or a leaf, but it must go downwards (traveling only from parent nodes to child nodes).

       root = [10,5,-3,3,2,null,11,3,-2,null,1], sum = 8

      10
     /  \
    5   -3
   / \    \
  3   2   11
 / \   \
3  -2   1

Return 3. The paths that sum to 8 are:

1.  5 -> 3
2.  5 -> 2 -> 1
3. -3 -> 11
       *
    */
    // Recursive: Running Time: O(n)
    public int pathSum(TreeNode root, int sum) {
        int count = 0;
        if (root == null) {
            return count;
        }
        // key -> sum to this node, value: number of occurrence
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        count = pathSum(root, 0, sum, map);
        return count;
    }

    private int pathSum(TreeNode node, int currSum, int sum, Map<Integer, Integer> map) {
        if (node == null) {
            return 0;
        }
        int count = 0;
        currSum += node.val;

        int sum_j = currSum - sum;
        // handle path from root:
        if (sum_j == 0) {
            count++;
        }

        if (map.containsKey(sum_j)) {
            count += map.get(sum_j);
        }

        // Set currSum and its value to 1 into map if it is not existed,
        if (!map.containsKey(currSum)) {
            map.put(currSum, 1);
        }
        else {
            int prevCount = map.get(currSum);
            map.put(currSum, prevCount+1);
        }

        count += pathSum(node.left, currSum, sum, map) + pathSum(node.right, currSum, sum, map);
        // Reset the occurence time by reducing 1, when returning from children
        int prevCount = map.get(currSum);
        map.put(currSum, prevCount-1);
        return count;
    }

    // Leetcode 560 Subarray Sum Equals K: https://leetcode.com/problems/subarray-sum-equals-k/
    /**
     * Given an array of integers and an integer k, you need to find the total number of continuous subarrays whose sum equals to k.
        Example 1:
        Input:nums = [1,1,1], k = 2
        Output: 2
     *
    */
    public int subarraySum(int[] nums, int k) {
        int count = 0;
        // key -> sum, value -> freq of this sum:
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        int sum = 0;
        for (int num : nums) {
            sum += num;
            int sum_j = sum - k;
            // make sure that we count [0..i] subarray when its sum is k
            if (sum_j == 0) {
                count++;
            }
            if (map.containsKey(sum_j)) {
                count += map.get(sum_j);
            }
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return count;
    }

    public int subarraySum(int[] nums, int k) {
        int count = 0;
        // key -> sum, value -> freq of this sum:
        Map<Integer, Integer> map = new HashMap<>();
        // add this 0 index to make sure that we count [0..i] subarray when its sum is k
        map.put(0, 1);
        int sum = 0;
        for (int num : nums) {
            sum += num;
            int sum_j = sum - k;
            if (map.containsKey(sum_j)) {
                count += map.get(sum_j);
            }
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return count;
    }

    // Leetcode 523: Continuous Subarray Sum
    // https://leetcode.com/problems/continuous-subarray-sum/
    /**
     *  Given a list of non-negative numbers and a target integer k, write a function to check if the array has a continuous subarray of size at least 2 that sums up to the multiple of k, that is, sums up to n*k where n is also an integer.

Example 1:
Input: [23, 2, 4, 6, 7],  k=6
Output: True
Explanation: Because [2, 4] is a continuous subarray of size 2 and sums up to 6.
Example 2:
Input: [23, 2, 6, 4, 7],  k=6
Output: True
Explanation: Because [23, 2, 6, 4, 7] is an continuous subarray of size 5 and sums up to 42.
Note:
The length of the array won't exceed 10,000.
You may assume the sum of all the numbers is in the range of a signed 32-bit integer.
     *
     * */
    public boolean checkSubarraySum(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return false;
        }
        // key -> sum, value -> index:
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int sum = 0;
        for (int i = 0, max = nums.length; i < max; i++) {
            sum += nums[i];
            if (k != 0) {
                sum %= k;
            }
            if (!map.containsKey(sum)) {
                map.put(sum, i);
            }
            else {
                int index_j = map.get(sum);
                // size of array is at least 2
                if (i - index_j >= 2) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
    Leetcode 930 Binary Subarrays With Sum:
    https://leetcode.com/problems/binary-subarrays-with-sum/

    In an array A of 0s and 1s, how many non-empty subarrays have sum S?

    Example 1:

    Input: A = [1,0,1,0,1], S = 2
    Output: 4
    Explanation: 
    The 4 subarrays are bolded below:
    [1,0,1,0,1]
    [1,0,1,0,1]
    [1,0,1,0,1]
    [1,0,1,0,1]
    

    Note:

    A.length <= 30000
    0 <= S <= A.length
    A[i] is either 0 or 1.
    */
    public int numSubarraysWithSum(int[] A, int S) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        
        int res = 0;
        int sum = 0;
        for (int num : A) {
            sum += num;
            int sum_j = sum - S;
            if (map.containsKey(sum_j)) {
                res += map.get(sum_j);
            }
            int cnt = map.getOrDefault(sum, 0) + 1;
            map.put(sum, cnt);
        }
        return res;
    }

}