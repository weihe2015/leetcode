public class Histogram {

    // Leetcode: 084: Largest Rectangle in Histogram:
    // https://leetcode.com/problems/largest-rectangle-in-histogram
    // Running Time Complexity: O(n^2), Space Complexity: O(1)
    // Basic idea: for each bar, search its left hand side and right hand side until
    // it finds the bard lower than itself.
    // Use these two indexes to calculate the rectangle area: height * (r-l+1) and
    // update the maxArea
    public int largestRectangleArea(int[] heights) {
        int maxArea = 0;
        for (int i = 0, max = heights.length; i < max; i++) {
            int height = heights[i];
            // search left hand side of this bar until it finds the bar lower than itself:
            int l = i;
            for (int k = i - 1; k >= 0; k--) {
                if (heights[k] >= height) {
                    l--;
                } else {
                    break;
                }
            }
            // search right hand side of this bar until it finds the bar lower than itself:
            int r = i;
            for (int k = i + 1; k < max; k++) {
                if (heights[k] >= height) {
                    r++;
                } else {
                    break;
                }
            }
            int currArea = height * (r - l + 1);
            maxArea = Math.max(maxArea, currArea);
        }
        return maxArea;
    }

    // Better Solution:
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int largestRectangleArea(int[] heights) {
        int maxArea = 0, max = heights.length;
        Stack<Integer> s = new Stack<Integer>();
        int i = 0;
        while (i <= max) {
            int height = 0;
            ;
            if (i < max) {
                height = heights[i];
            }
            if (s.isEmpty() || height >= heights[s.peek()]) {
                s.push(i);
                i++;
            } else {
                // Get the previous top height
                int topIdx = s.pop();
                // If stack is empty, then it means previous elements are greater than
                // heights[topIdx]
                // Else, s.peek() is the starting point of histogram. width = i - (s.peek() + 1)
                int width = i;
                if (!s.isEmpty()) {
                    width = i - (s.peek() + 1);
                }
                int currArea = heights[topIdx] * width;
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }

    // Leetcode 11: Container With Most Water https://leetcode.com/problems/container-with-most-water/
    // Running Time Complexity: O(n^2), Space Complexity: O(1)
     public int maxArea(int[] heights) {
        int maxArea = 0;
        for (int i = 0, max = heights.length; i < max; i++) {
            for (int j = i+1; j < max; j++) {
                int minHeight = Math.min(heights[i], heights[j]);
                int currArea = minHeight * (j - i);
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }
    // Running Time Complexity: O(n), Space Complexity: O(1)
    // One Pass: use two pointer, low & high. Calculate the rectangle area
    // Move low pointer when height[low] < height[high]
    // Move high pointer when height[low] >= height[high]
    public int maxArea(int[] height) {
        int maxArea = 0, low = 0, high = height.length - 1;
        while (low < high) {
            int minHeight = Math.min(height[low],height[high]);
            int currArea = minHeight * (high - low);
            maxArea = Math.max(maxArea, currArea);
            if (height[low] < height[high]) {
                low++;
            }
            else {
                 high--;
            }
        }
        return maxArea;
    }

    // Leetcode 42: Trapping Rain Water: https://leetcode.com/problems/trapping-rain-water/
    // Native Solution:
    // Running Time complexity: O(N^2), Space Complexity: O(1)
    /**
     * Basic Idea:
     *  1. For each bar, search left and right side to find the max heigh bar on both left and right side
     *  2. Look at each single column, the water trap area is = Math.min(maxLeft, maxRight) - currHeight;
     *  3. Sum all water trap area of each bar.
     */
    public int trap(int[] height) {
        int totalArea = 0;
        for (int i = 0, max = height.length; i < max; i++) {
            int currHeight = height[i];
            int maxLeft = currHeight, maxRight = currHeight;
            // search the max high bar on the left hand side:
            for (int j = i - 1; j >= 0; j--) {
                maxLeft = Math.max(maxLeft, height[j]);
            }
            
            // search the max high bar on the right hand side:
            for (int j = i + 1; j < max; j++) {
                maxRight = Math.max(maxRight, height[j]);
            }
            totalArea += Math.min(maxLeft, maxRight) - currHeight;
        }
        return totalArea;
    }

    // Running Time Complexity: O(n), Space Complexity: O(1)
    /**
     * Basic Idea:
     *  1. Two pointer, left = 0, right = height.length;
     *      maxLeft = height[left], maxRight = height[right];
     *  2. Because the water trapped depends on the min(maxLeft, maxRight), 
     *     so when right bar is larger, we move the left bar, otherwise, we move the right bar.
     *  3. For each iteration, compare maxLeft/maxRight with next bar, and update the maxLeft and maxRight bar with next bar
     *       currArea of each column area: maxLeft - height[l] / maxRight - height[r]
    */
    public int trap(int[] height) {
        int totalArea = 0;
        if (height == null || height.length == 0) {
            return totalArea;
        }
        int l = 0, r = height.length - 1;
        int maxLeft = height[l], maxRight = height[r];
        
        while (l < r) {
            if (maxLeft < maxRight) {
                l++;
                maxLeft = Math.max(maxLeft, height[l]);
                totalArea += maxLeft - height[l];
            }    
            else {
                r--;
                maxRight = Math.max(maxRight, height[r]);
                totalArea += maxRight - height[r];
            }
        }
        return totalArea;
    }
}