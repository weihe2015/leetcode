public class Solution {
    // Leetcode 027: Remove Element:
    // https://leetcode.com/problems/remove-element/
    /**
    Given input array nums = [3,2,2,3], val = 3
    Your function should return length = 2, with the first two elements of nums being 2.

    Example 1:
    Given nums = [3,2,2,3], val = 3,
    Your function should return length = 2, with the first two elements of nums being 2.
    It doesn't matter what you leave beyond the returned length.
    Example 2:
    Given nums = [0,1,2,2,3,0,4,2], val = 2,
    Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.
    Note that the order of those five elements can be arbitrary.
    It doesn't matter what values are set beyond the returned length.

    */
    // Running time: O(n), Space Complexity: O(1)
    public int removeElement(int[] nums, int val) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int idx = 0;
        for (int i = 0, max = nums.length; i < max; i++) {
            if (nums[i] != val) {
                nums[idx] = nums[i];
                idx++;
            }
        }
        return idx;
    }

    // Leetcode 026: Remove Duplicates from Sorted Array
    // https://leetcode.com/problems/remove-duplicates-from-sorted-array/
    /**
        Given a sorted array,
        remove the duplicates in place such that each element appear only once and return the new length.
        Do not allocate extra space for another array, you must do this in place with constant memory.

        For example,
        Given input array nums = [1,1,2],
        Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
        It doesnt matter what you leave beyond the new length.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int idx = 1;
        for (int i = 1, max = nums.length; i < max; i++) {
            if (nums[i] != nums[i-1]) {
                nums[idx] = nums[i];
                idx++;
            }
        }
        return idx;
    }

    // Leetcode 080: Remove Duplicates from Sorted Array II
    // https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/
    /*
      Follow up for "Remove Duplicates":
      What if duplicates are allowed at most twice?

      For example,
      Given sorted array nums = [1,1,1,2,2,3],

      Your function should return length = 5, with the first five elements of nums being 1, 1, 2, 2 and 3.
      It doesnt matter what you leave beyond the new length.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int idx = 1;
        boolean meetTwice = false;
        for (int i = 1, max = nums.length; i < max; i++) {
            if (nums[i] == nums[i-1]) {
                if (!meetTwice) {
                    meetTwice = true;
                    nums[idx++] = nums[i];
                }
            }
            else {
                meetTwice = false;
                nums[idx++] = nums[i];
            }
        }
        return idx;
    }

    // Leetcode 169: Majority Element:
    // https://leetcode.com/problems/majority-element/
    /**
    Given an array of size n, find the majority element.
    The majority element is the element that appears more than ⌊ n/2 ⌋ times.
    You may assume that the array is non-empty and the majority element always exist in the array.

    Example 1:
    Input: [3,2,3]
    Output: 3

    Example 2:
    Input: [2,2,1,1,1,2,2]
    Output: 2
    */
    // Linear voting algorithm Boyer–Moore majority vote algorithm
    /**
    This method only works when there is majority element exists in the array.
    Running Time Complexity: O(N), Space Complexity: O(1)
    */
    public int majorityElement(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int majorElementIdx = 0, count = 0;
        for (int i = 0, n = nums.length; i < n; i++) {
            if (nums[i] == nums[majorElementIdx]) {
                count++;
            }
            else if (count == 0) {
                majorElementIdx = i;
                count = 1;
            }
            else {
                count--;
            }
        }
        return nums[majorElementIdx];
    }

    // Leetcode 229 Majority Element II:
    // https://leetcode.com/problems/majority-element-ii/
    /**
    Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.
    Note: The algorithm should run in linear time and in O(1) space.
    Example 1:
    Input: [3,2,3]
    Output: [3]

    Example 2:
    Input: [1,1,1,3,3,2,2,2]
    Output: [1,2]
    */
    public List<Integer> majorityElement(int[] nums) {
        List<Integer> res = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return res;
        }
        // At most two candidates whose count will greater than n/3
        // Pick up two candidates out, A and B:
        int majorA = 0, majorB = 0, countA = 0, countB = 0;
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            // Select A:
            if (nums[i] == nums[majorA]) {
                countA++;
            }
            // Select B:
            else if (nums[i] == nums[majorB]) {
                countB++;
            }
            // If candidate A's count is == 0, we switch the candidate
            else if (countA == 0) {
                majorA = i;
                countA = 1;
            }
            // If candidate B's count is == 0, we switch the candidate
            else if (countB == 0) {
                majorB = i;
                countB = 1;
            }
            // At this point, both countA and countB are not zero, and decrement both countA and countB
            // because currently no one is selecting candidate A and B
            // If we do not decrement countA and countB, new candidate cannot showup.
            else {
                countA--;
                countB--;
            }
        }
        // currently majorA and majorB are the candidates with the most selection.
        // reset countA and countB, iterate the array again to confirm the count.
        countA = 0;
        countB = 0;
        for (int num : nums) {
            if (num == nums[majorA]) {
                countA++;
            }
            else if (num == nums[majorB]) {
                countB++;
            }
        }
        if (countA > n/3) {
            res.add(nums[majorA]);
        }
        if (countB > n/3) {
            res.add(nums[majorB]);
        }
        return res;
    }

    // Lintcode 48: Majority Number III
    // https://www.lintcode.com/problem/majority-number-iii
    /*
    Given an array of integers and a number k, the majority number is the number that occurs more than 1/k of the size of the array.
    Example
    Example 1:

    Input: [3,1,2,3,2,3,3,4,4,4] and k=3,
    Output: 3.
    Example 2:

    Input: [1,1,2] and k=3,
    Output: 1.
    */
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int majorityNumber(List<Integer> nums, int k) {
        if (nums == null || nums.isEmpty() || k < 2) {
            return -1;
        }
        // key -> num, val -> freq
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        int n = nums.size();
        for (int num : map.keySet()) {
            int freq = map.get(num);
            if (freq > n / k) {
                return num;
            }
        }
        return -1;
    }
    // Solution: Time Complexity: O(N), Space Complexity: O(K)
    public int majorityNumber(List<Integer> nums, int k) {
        if (nums == null || nums.isEmpty()) {
            return -1;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
            if (map.size() >= k) {
                map.entrySet().removeIf(e -> e.getValue() == 1);
            }
        }

        // collect the true counter of these keys
        for (int key : map.keySet()) {
            map.put(key, 0);
        }
        for (int num : nums) {
            if (map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            }
        }

        // find the maxVal of the key:
        int maxKey = -1, maxVal = Integer.MIN_VALUE;
        for (int key : map.keySet()) {
            int val = map.get(key);
            if (val > maxVal) {
                maxVal = val;
                maxKey = key;
            }
        }
        return maxKey;
    }
    // Leetcode 238: Product of Array Except Self
    // https://leetcode.com/problems/product-of-array-except-self/
    /**
    Given an array of n integers where n > 1, nums, return an array output
    such that output[i] is equal to the product of all the elements of nums except nums[i].

    Solve it without division and in O(n).
    For example, given [1,2,3,4], return [24,12,8,6].
    Numbers:  2     3    4    5
    lefts     1     2    2*3  2*3*4
    rights:   3*4*5 4*5  5    1
    */
    // O(n) Running time complexity, O(1) Space complexity
    // https://www.youtube.com/watch?v=vB-81TB6GUc
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int[] res = new int[n];
        Arrays.fill(res, 1);
        int left = 1;
        for (int i = 0; i < n; i++) {
            res[i] *= left;
            left *= nums[i];
        }
        int right = 1;
        for (int i = n-1; i >= 0; i--) {
            res[i] *= right;
            right *= nums[i];
        }
        return res;
    }

    // Leetcode 243: Shortest Word Distance:
    // https://leetcode.com/problems/shortest-word-distance
    /**
    Given a list of words and two words word1 and word2,
    return the shortest distance between these two words in the list.
    Example:
    Assume that words = ["practice", "makes", "perfect", "coding", "makes"].

    Input: word1 = “coding”, word2 = “practice”
    Output: 3
    Input: word1 = "makes", word2 = "coding"
    Output: 1
    Note:
    You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.
    */
    // Running Time Complexity: O(N)
    public int shortestDistance(String[] words, String word1, String word2) {
        int idx1 = -1, idx2 = -1, dist = words.length;
        for (int i = 0, n = words.length; i < n; i++) {
            if (word1.equals(words[i])) {
                idx1 = i;
            }
            else if (word2.equals(words[i])) {
                idx2 = i;
            }
            if (idx1 != -1 && idx2 != -1) {
                dist = Math.min(dist, Math.abs(idx1 - idx2));
            }
        }
        return dist;
    }
    // Leetcode 244 Shortest Word Distance II:
    /**
    Design a class which receives a list of words in the constructor,
    and implements a method that takes two words word1 and word2 and
    return the shortest distance between these two words in the list.
    Your method will be called repeatedly many times with different parameters.

    Example:
    Assume that words = ["practice", "makes", "perfect", "coding", "makes"].

    Input: word1 = “coding”, word2 = “practice”
    Output: 3
    Input: word1 = "makes", word2 = "coding"
    Output: 1
    Note:
    You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.
    */
    public class ShortestWordDistance {
        // key -> word, val -> list of indexes
        private Map<String, List<Integer>> map;
        public ShortestWordDistance(String[] words) {
            this.map = new LinkedHashMap<>();
            for (int i = 0, max = words.length; i < max; i++) {
                String word = words[i];
                List<Integer> list;
                if (!map.containsKey(word)) {
                    list = new ArrayList<>();
                }
                else {
                    list = map.get(word);
                }
                list.add(i);
                map.put(word, list);
            }
        }

        public int shortestDistance(String word1, String word2) {
            if (!map.containsKey(words1) || map.containsKey(word2)) {
                return -1;
            }
            List<Integer> list1 = map.get(word1);
            List<Integer> list2 = map.get(word2);
            int minDis = Integer.MAX_VALUE;
            for (int num1 : list1) {
                for (int num2 : list2) {
                    minDis = Math.min(minDis, Math.abs(num2-num1));
                }
            }
            return minDis;
        }
    }

    // Leetcode 245: Shortest Word Distance III:
    // https://leetcode.com/problems/shortest-word-distance-iii
    /**
    Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.

    word1 and word2 may be the same and they represent two individual words in the list.

    Example:
    Assume that words = ["practice", "makes", "perfect", "coding", "makes"].

    Input: word1 = “makes”, word2 = “coding”
    Output: 1
    Input: word1 = "makes", word2 = "makes"
    Output: 3
    Note:
    You may assume word1 and word2 are both in the list.
    */
    public int shortestWordDistance(String[] words, String word1, String word2) {
        if (word1.equals(word2)) {
            return shortestWordDistanceWithSameWord(words, word1);
        }
        else {
            return shortestWordDistanceWithDiffWords(words, word1, word2);
        }
    }

    public int shortestWordDistanceWithSameWord(String[] words, String word1) {
        int idx1 = -1, idx2 = -1, minDis = words.length;
        for (int i = 0, max = words.length; i < max; i++) {
            String word = words[i];
            if (!word.equals(word1)) {
                continue;
            }
            if (idx1 == -1) {
                idx1 = i;
            }
            else {
                idx2 = i;
            }
            if (idx1 != -1 && idx2 != -1) {
                minDis = Math.min(minDis, Math.abs(idx1))
            }
        }
        return minDis;
    }

    public int shortestWordDistanceWithDiffWords(String[] words, String word1, String word2) {
        int idx1 = -1, idx2 = -1, dist = words.length;
        for (int i = 0, n = words.length; i < n; i++) {
            if (word1.equals(words[i])) {
                idx1 = i;
            }
            else if (word2.equals(words[i])) {
                idx2 = i;
            }
            if (idx1 != -1 && idx2 != -1) {
                dist = Math.min(dist, Math.abs(idx1 - idx2));
            }
        }
        return dist;
    }

    // Leetcode 277: Find the Celebrity
    // https://leetcode.com/problems/find-the-celebrity
    // https://www.lintcode.com/problem/find-the-celebrity/
    /**
    Suppose you are at a party with n people (labeled from 0 to n - 1)
    and among them, there may exist one celebrity.

    The definition of a celebrity is that all the other n - 1 people know him/her but he/she does not know any of them.

    Now you want to find out who the celebrity is or verify that there is not one.
    The only thing you are allowed to do is to ask questions like:
    "Hi, A. Do you know B?" to get information of whether A knows B.

    You need to find out the celebrity (or verify there is not one) by asking as few questions as possible (in the asymptotic sense).

    You are given a helper function bool knows(a, b) which tells you whether A knows B.

    Implement a function int findCelebrity(n), your function should minimize the number of calls to knows.

    Note: There will be exactly one celebrity if he/she is in the party.
    Return the celebrity's label if there is a celebrity in the party. If there is no celebrity, return -1.
    */
    public int findCelebrity(int n) {
        int candidate = 0;
        for (int i = 0; i < n; i++) {
            // if person candidate knows person i, then person i may be the celebrity
            if (knows(candidate, i)) {
                candidate = i;
            }
        }
        // verify this celebrity:
        for (int i = 0; i < n; i++) {
            if (i != candidate && (knows(candidate, i) || !knows(i, candidate))) {
                return -1;
            }
        }
        return candidate;
    }

    // Leetcode 041: First Missing Positive:
    // https://leetcode.com/problems/first-missing-positive/
    /*
    Given an unsorted integer array, find the first missing positive integer.

    For example,
    Given [1,2,0] return 3,
    and [3,4,-1,1] return 2.

    Your algorithm should run in O(n) time and uses constant space.
    */
    // Naive Solution:
    // Running Time Complexity: O(n), Space Complexity: O(n)
    /**
    第一遍遍历数组把所有的数都存入HashSet中，并且找出数组的最大值，下次循环从1开始递增找数字，哪个数字找不到就返回哪个数字，如果一直找到了最大的数字，则返回最大值+1
    */
    public int firstMissingPositive(int[] nums) {
        Set<Integer> set = new HashSet<>();
        int max = Integer.MIN_VALUE;
        for (int num : nums) {
            if (num <= 0) {
                continue;
            }
            max = Math.max(max, num);
            set.add(num);
        }
        int count = 1;
        int size = set.size();
        for (int i = 0; i < size; i++) {
            if (!set.contains(count)) {
                return count;
            }
            count++;
        }
        if (max == Integer.MIN_VALUE) {
            return 1;
        }
        return max+1;
    }

    /***
    我们的思路是把1放在数组第一个位置nums[0]，2放在第二个位置nums[1]，
    即需要把nums[i]放在nums[nums[i] - 1]上，那么我们遍历整个数组，如果nums[i] != i + 1,
    而nums[i]为整数且不大于n，另外nums[i]不等于nums[nums[i] - 1]的话，
    我们将两者位置调换，如果不满足上述条件直接跳过，最后我们再遍历一遍数组，
    如果对应位置上的数不正确则返回正确的数
    */
    public int firstMissingPositive(int[] nums) {
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            while (nums[i] > 0 && nums[i] < n) {
                if (nums[i] != nums[nums[i]-1]) {
                    swap(nums, i, nums[i]-1);
                }
                else {
                    break;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            if (nums[i] != i+1) {
                return i+1;
            }
        }
        return n+1;
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // Leetcode 442 Find all Duplicates in an Array:
    // https://leetcode.com/problems/find-all-duplicates-in-an-array
    /**
    Given an array of integers, 1 ≤ a[i] ≤ n (n = size of array),
    some elements appear twice and others appear once.

    Find all the elements that appear twice in this array.

    Could you do it without extra space and in O(n) runtime?

    Example:
    Input:
    [4,3,2,7,8,2,3,1]

    Output:
    [2,3]
    */
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> res = new ArrayList<>();
        for (int num : nums) {
            // 1 ≤ a[i] ≤ n, we choose idx = Math.abs(num) - 1
            int idx = Math.abs(num) - 1;
            if (nums[idx] < 0) {
                res.add(Math.abs(num));
            }
            nums[idx] = -nums[idx];
        }
        return res;
    }

    // Leetcode 448: Find All Numbers Disappeared in an Array
    // https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array
    /**
    Given an array of integers where 1 ≤ a[i] ≤ n (n = size of array),
    some elements appear twice and others appear once.

    Find all the elements of [1, n] inclusive that do not appear in this array.

    Could you do it without extra space and in O(n) runtime?
    You may assume the returned list does not count as extra space.

    Example:

    Input:
    [4,3,2,7,8,2,3,1]

    Output:
    [5,6]
    */
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> res = new ArrayList<>();
        for (int num : nums) {
            int idx = Math.abs(num) - 1;
            if (nums[idx] > 0) {
                nums[idx] = -nums[idx];
            }
        }
        for (int i = 0, max = nums.length; i < max; i++) {
            if (nums[i] > 0) {
                res.add(i+1);
            }
        }
        return res;
    }

    // Leetcode 299: Bulls and Cows
    // https://leetcode.com/problems/bulls-and-cows
    /**
    You are playing the following Bulls and Cows game with your friend:
    You write down a number and ask your friend to guess what the number is.
    Each time your friend makes a guess, you provide a hint that
    indicates how many digits in said guess match your secret number exactly in both digit and position (called "bulls") and
    how many digits match the secret number but locate in the wrong position (called "cows").
    Your friend will use successive guesses and hints to eventually derive the secret number.

    For example:

    Secret number:  "1807"
    Friend's guess: "7810"
    Hint: 1 bull and 3 cows. (The bull is 8, the cows are 0, 1 and 7.)
    Write a function to return a hint according to the secret number and friend's guess,
    use A to indicate the bulls and B to indicate the cows. In the above example, your function should return "1A3B".

    Please note that both secret number and friend's guess may contain duplicate digits, for example:

    Secret number:  "1123"
    Friend's guess: "0111"
    In this case, the 1st 1 in friend's guess is a bull, the 2nd or 3rd 1 is a cow, and your function should return "1A1B".
    You may assume that the secret number and your friend's guess only contain digits, and their lengths are always equal.
    */
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public String getHint(String secret, String guess) {
        // digits 0-9
        int[] nums = new int[10];
        int n = secret.length();
        boolean[] visited = new boolean[n];
        int bulls = 0, cows = 0;
        for (int i = 0; i < n; i++) {
            char c1 = secret.charAt(i);
            char c2 = guess.charAt(i);
            if (c1 == c2) {
                bulls++;
                visited[i] = true;
            }
            else {
                int digit = (int) c1 - '0';
                nums[digit]++;
            }
        }
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            char ch = guess.charAt(i);
            int digit = (int)ch - '0';
            if (nums[digit] > 0) {
                cows++;
                nums[digit]--;
            }
        }
        return bulls + "A" + cows + "B";
    }

    // Running Time Complexity: O(N), Space Complexity: O(1)
    public String getHint(String secret, String guess) {
        int[] nums = new int[10];
        int bulls = 0, cows = 0;
        int n = secret.length();
        for (int i = 0; i < n; i++) {
            char c1 = secret.charAt(i);
            char c2 = guess.charAt(i);
            if (c1 == c2) {
                bulls++;
            }
            else {
                if (nums[c1-'0'] < 0) {
                    cows++;
                }
                if (nums[c2-'0'] > 0) {
                    cows++;
                }
                nums[c1-'0']++;
                nums[c2-'0']--;
            }
        }
        return bulls + "A" + cows + "B";
    }

    // Leetcode 134 Gas Station:
    // https://leetcode.com/problems/gas-station/
    /**
        There are N gas stations along a circular route, where the amount of gas at station i is gas[i].

        You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from station i to its next station (i+1). You begin the journey with an empty tank at one of the gas stations.

        Return the starting gas station's index if you can travel around the circuit once in the clockwise direction, otherwise return -1.

        Note:

        If there exists a solution, it is guaranteed to be unique.
        Both input arrays are non-empty and have the same length.
        Each element in the input arrays is a non-negative integer.
        Example 1:

        Input:
        gas  = [1,2,3,4,5]
        cost = [3,4,5,1,2]

        Output: 3

        Explanation:
        Start at station 3 (index 3) and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
        Travel to station 4. Your tank = 4 - 1 + 5 = 8
        Travel to station 0. Your tank = 8 - 2 + 1 = 7
        Travel to station 1. Your tank = 7 - 3 + 2 = 6
        Travel to station 2. Your tank = 6 - 4 + 3 = 5
        Travel to station 3. The cost is 5. Your gas is just enough to travel back to station 3.
        Therefore, return 3 as the starting index.
        Example 2:

        Input:
        gas  = [2,3,4]
        cost = [3,4,3]

        Output: -1

        Explanation:
        You can't start at station 0 or 1, as there is not enough gas to travel to the next station.
        Let's start at station 2 and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
        Travel to station 0. Your tank = 4 - 3 + 2 = 3
        Travel to station 1. Your tank = 3 - 3 + 3 = 3
        You cannot travel back to station 2, as it requires 4 unit of gas but you only have 3.
        Therefore, you can't travel around the circuit once no matter where you start.
    */
    /**
    Basic Idea:
    假设开始设置起点start = 0, 并从这里出发，如果当前的gas值大于cost值，就可以继续前进，此时到下一个站点，剩余的gas加上当前的gas再减去cost，看是否大于0，
    若大于0，则继续前进。当到达某一站点时，若这个值小于0了，则说明从起点到这个点中间的任何一个点都不能作为起点，
    则把起点设为下一个点，继续遍历。当遍历完整个环时，当前保存的起点即为所求。
    */
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int tank = 0, total = 0, start = 0;
        for (int i = 0, n = gas.length; i < n; i++) {
            tank += gas[i] - cost[i];
            total += gas[i] - cost[i];
            if (tank < 0) {
                tank = 0;
                start = i + 1;
            }
        }
        return total < 0 ? -1 : start;
    }

    // Leetcode 274 H-index
    // https://leetcode.com/problems/h-index/
    /**
    Given an array of citations (each citation is a non-negative integer) of a researcher, write a function to compute the researcher's h-index.

    According to the definition of h-index on Wikipedia: "A scientist has index h if h of his/her N papers have at least h citations each,
    and the other N − h papers have no more than h citations each."

    Example:

    Input: citations = [3,0,6,1,5]
    Output: 3
    Explanation: [3,0,6,1,5] means the researcher has 5 papers in total and each of them had
                received 3, 0, 6, 1, 5 citations respectively.
                Since the researcher has 3 papers with at least 3 citations each and the remaining
                two with no more than 3 citations each, her h-index is 3.
    Note: If there are several possible values for h, the maximum one is taken as the h-index.
    */
    // Running Time Complexity: O(NlogN), Space Complexity: O(1)
    public int hIndex(int[] citations) {
        // sort array in descending order
        Arrays.sort(citations);
        reverse(citations);
        int n = citations.length;
        // 从前往后查找排序后的列表，直到某篇论文的序号大于该论文被引次数。
        for (int i = 0; i < n; i++) {
            if (i >= citations[i]) {
                return i;
            }
        }
        return n;
    }

    private void reverse(int[] nums) {
        int i = 0, j = nums.length-1;
        while (i < j) {
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // Solution 2: Use Bucket Sort
    /**
    https://leetcode.com/problems/h-index/discuss/70768/Java-bucket-sort-O(n)-solution-with-detail-explanation
    First, you may ask why bucket sort. Well, the h-index is defined as the number of papers with reference greater than the number.
    So assume n is the total number of papers, if we have n+1 buckets, number from 0 to n,
    then for any paper with reference corresponding to the index of the bucket, we increment the count for that bucket.

    Then we iterate from the back to the front of the buckets, whenever the total count exceeds the index of the bucket,
    meaning that we have the index number of papers that have reference greater than or equal to the index.
    Which will be our h-index result.

    The reason to scan from the end of the array is that we are looking for the greatest h-index.
    */
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int hIndex(int[] citations) {
        int n = citations.length;
        int[] buckets = new int[n+1];
        for (int citation : citations) {
            if (citation > n) {
                buckets[n]++;
            }
            else {
                buckets[citation]++;
            }
        }
        int total = 0;
        for (int max = buckets.length, i = max-1; i >= 0; i--) {
            total += buckets[i];
            if (total >= i) {
                return i;
            }
        }
        return -1;
    }

    // Leetcode 275: H-Index II
    // https://leetcode.com/problems/h-index-ii/
    /**
    Follow up:

    This is a follow up problem to H-Index, where citations is now guaranteed to be sorted in ascending order.
    Could you solve it in logarithmic time complexity?
    */
    public int hIndex(int[] citations) {
        int low = 0, high = citations.length - 1, n = citations.length;
        int res = 0;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (citations[mid] >= n-mid) {
                res = Math.max(res, n-mid);
                high = mid-1;
            }
            else {
                low = mid+1;
            }
        }
        return res;
    }

    // Leetcode 55: Jump Game:
    // https://leetcode.com/problems/jump-game
    /*
    Given an array of non-negative integers, you are initially positioned at the first index of the array.

    Each element in the array represents your maximum jump length at that position.

    Determine if you are able to reach the last index.

    Example 1:

    Input: [2,3,1,1,4]
    Output: true
    Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
    Example 2:

    Input: [3,2,1,0,4]
    Output: false
    Explanation: You will always arrive at index 3 no matter what. Its maximum
                jump length is 0, which makes it impossible to reach the last index.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public boolean canJump(int[] nums) {
        int reach = 0;
        for (int i = 0, n = nums.length; i < n; i++) {
            // the maximum idx that we can reach from current idx:
            int currMaxReach = i + nums[i];
            reach = Math.max(reach, currMaxReach);
            // If max position we can reach is further than last idx, we return true
            if (reach >= n-1) {
                return true;
            }
            // current reach could only reach up to current position, so break and return false
            if (reach <= i) {
                return false;
            }
        }
        return false;
    }

    // Leetocde 45: Jump Game II:
    // https://leetcode.com/problems/jump-game-ii/
    /**
    Given an array of non-negative integers, you are initially positioned at the first index of the array.

    Each element in the array represents your maximum jump length at that position.

    Your goal is to reach the last index in the minimum number of jumps.

    Example:

    Input: [2,3,1,1,4]
    Output: 2
    Explanation: The minimum number of jumps to reach the last index is 2.
        Jump 1 step from index 0 to 1, then 3 steps to the last index.
    Note:

    You can assume that you can always reach the last index.
    */
    // Running Time Complexity: O(N) Space Complexity: O(1)
    public int jump(int[] nums) {
        int reach = 0, furthest = 0, count = 0;
        for (int i = 0, n = nums.length; i < n-1; i++) {
            int currMaxReach = i + nums[i];
            reach = Math.max(reach, currMaxReach);
            if (i == furthest) {
                furthest = reach;
                count++;
                if (furthest >= n-1) {
                    break;
                }
            }
        }
        return count;
    }
}