public class Solution {
    // Leetcode 056: Merge Intervals:
    // https://leetcode.com/problems/merge-intervals
    /*
    Given a collection of intervals, merge all overlapping intervals.

    Example 1:

    Input: [[1,3],[2,6],[8,10],[15,18]]
    Output: [[1,6],[8,10],[15,18]]
    Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
    Example 2:

    Input: [[1,4],[4,5]]
    Output: [[1,5]]
    Explanation: Intervals [1,4] and [4,5] are considerred overlapping.
    */
    // Running Time Complexity: O(NlogN), Space Complexity: O(1)
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> result = new ArrayList<Interval>();
        if (intervals.isEmpty()) {
            return result;
        }
        // Sort intervals by its start
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        /**
        Collections.sort(intervals, new Comparator<Interval>(){
            @Override
            public int compare(Interval i1, Interval i2){
                return Integer.compare(i1.start, i2.start);
            }
        });
        */
        /**
        Collections.sort(intervals, new IntervalComparator());
        */
        int start = -1, end = -1;
        for (Interval interval : intervals) {
            if (start == -1 && end == -1) {
                start = interval.start;
                end = interval.end;
            }
            else {
                // Overlapping intervals, update end bound
                if (interval.start <= end) {
                    end = Math.max(end, interval.end);
                }
                // Disjoint intervals, add the previous one and reset bounds
                else {
                    result.add(new Interval(start, end));
                    start = interval.start;
                    end = interval.end;
                }
            }
        }
        // add last Interval
        if (start != -1) {
            result.add(new Interval(start, end));
        }
        return result;
    }
    // Another way of writing the comparator:
    private class IntervalComparator implements Comparator<Interval> {
        @Override
        public int compare(Interval i1, Interval i2) {
            return i1.start - i2.start;
        }
    }

    /**
        Comparator v.s Comparable:
            http://codecramp.com/java-comparable-vs-comparator/
        java.util.Comparator: interface int compare(Object o1, Object o2)
            A comparator object is capable of comparing two different objects.
            Gives a flexible way to order objects by this custom comparator

        java.lang.Comparable: int compareTo(Object o1)
            The class itself must implements the java.lang.Comparable interface in order to be able to compare its instances
            It gives the natual ordering of the class. Cannot modify once defined.

    */
    // Solution 2 without creating additional objects
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> res = new ArrayList<>();
        if (intervals == null || intervals.isEmpty()) {
            return res;
        }
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        Interval prevInterval = null;
        for (Interval interval : intervals) {
            if (prevInterval == null || prevInterval.end < interval.start) {
                res.add(interval);
                prevInterval = interval;
            }
            else if (prevInterval.end < interval.end) {
                // change the reference interval in result list directly.
                prevInterval.end = interval.end;
            }
        }
        return res;
    }

    // Solution 3:
    public List<Interval> merge(List<Interval> intervals) {
        int n = intervals.size();
        int[] starts = new int[n];
        int[] ends = new int[n];
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        // sort start & end
        Arrays.sort(starts);
        Arrays.sort(ends);

        List<Interval> result = new ArrayList<Interval>();
        // Iterate each start and end point.
        for (int i = 0, j = 0; i < n; i++) {
            // If i is last item or next item's start is larger than ends, add this into new Interval
            if (i == n-1 || starts[i+1] > ends[i]) {
                result.add(new Interval(starts[j], ends[i]));
                // Move j pointer to point to next interval
                j = i + 1;
            }
        }
        return result;
    }

    // Leetcode 57: Insert Interval:
    // https://leetcode.com/problems/insert-interval
    /**
    Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

    You may assume that the intervals were initially sorted according to their start times.

    Example 1:

    Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
    Output: [[1,5],[6,9]]
    Example 2:

    Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
    Output: [[1,2],[3,10],[12,16]]
    Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].
    */
    // Solution 1: Use the solution of Merge Interval
    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> res = new ArrayList<>();
        if (intervals == null || intervals.isEmpty()) {
            res.add(newInterval);
            return res;
        }
        intervals.add(newInterval);
        return merge(intervals);
    }

    // Solution 2:
    // Running Time Complexity: O(N)
    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> result = new ArrayList<Interval>();
        if (intervals == null || intervals.isEmpty()) {
            result.add(newInterval);
            return result;
        }
        int i = 0, n = intervals.size(), start = newInterval.start, end = newInterval.end;
        for (i = 0; i < n; i++) {
            Interval currInterval = intervals.get(i);
            if (currInterval.end < start) {
                result.add(currInterval);
            }
            // no overlap with new interval, break and add the rest intervals at the end.
            else if (currInterval.start > end) {
                break;
            }
            // Current Interval overlaps with newInterval:
            else {
                start = Math.min(start, currInterval.start);
                end = Math.max(end, currInterval.end);
            }
        }
        result.add(new Interval(start, end));
        // add the rest
        for (int j = i; j < n; j++) {
            result.add(intervals.get(j));
        }
        return result;
    }

    // Leetcode 252: Meeting Rooms I
    // https://leetcode.com/problems/meeting-rooms
    // https://www.lintcode.com/problem/meeting-rooms/description
    /*
    Given an array of meeting time intervals consisting of start and
    end times [[s1,e1],[s2,e2],...] (si < ei), determine if a person could attend all meetings.

    Example 1:

    Input: [[0,30],[5,10],[15,20]]
    Output: false
    Example 2:

    Input: [[7,10],[2,4]]
    Output: true
    */
    // Solution 1: use comparator O(nlogn) time, O(1) space
    public boolean canAttendMeetings(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return true;
        }
        // Sort List of Intervals by start time
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        int end = -1;
        for (Interval interval : intervals) {
            if (end == -1 || end <= interval.start) {
                end = interval.end;
            }
            else if (end > interval.start) {
                return false;
            }
        }
        return true;
    }

	// Solution 2
	// O(nlogn) time, O(n) space, but faster
    public boolean canAttendMeetings(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return true;
        }
        int n = intervals.size();
        int[] starts = new int[n];
        int[] ends = new int[n];
        for (int i = 0; i < n; i++) {
            Interval interval = intervals.get(i);
            starts[i] = interval.start;
            ends[i] = interval.end;
        }
        Arrays.sort(starts);
        Arrays.sort(ends);
        for (int i = 1; i < n; i++) {
            if (starts[i] < ends[i-1]) {
                return false;
            }
        }
        return true;
    }

    // Leetcode 253 Meeting Rooms II:
    // https://leetcode.com/problems/meeting-rooms-ii
    /**
    Given an array of meeting time intervals consisting of
    start and end times [[s1,e1],[s2,e2],...] (si < ei),
    find the minimum number of conference rooms required.

    Example 1:

    Input: [[0, 30],[5, 10],[15, 20]]
    Output: 2
    Example 2:

    Input: [[7,10],[2,4]]
    Output: 1
    */
    // Running Time Complexity: O(NlogN), N Nodes * (each node takes logN time to insert into Priority Queue)
	public int minMeetingRooms(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return 0;
        }
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        // Min Heap with min end time on the top
        Queue<Integer> pq = new PriorityQueue<Integer>();
        for (Interval interval : intervals) {
			// If there is no overlap, meaning no new room is needed, remove the top one in the heap.
            if (!pq.isEmpty() && pq.peek() <= interval.start) {
                pq.poll();
            }
            pq.offer(interval.end);
        }
        return pq.size();
    }
	// Running Time Complexity: O(NlogN), N Nodes * (each node takes logN time to insert into Priority Queue)
    public int minMeetingRooms(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return 0;
        }
        int n = intervals.size();
        Collections.sort(intervals, (i1, i2) -> (i1.start - i2.start));
		// PriorityQueue with min End Time on the top, Min Heap
        Queue<Interval> pq = new PriorityQueue<Interval>(n, (i1, i2) -> (i1.end - i2.end));
        /**
        Java 7:
        Queue<Interval> pq = new PriorityQueue<Interval>(n, new Comparator<Interval>(){
            public int compare(Interval i1, Interval i2) {
                return i1.end - i2.end;
            }
        })
        */
        for (Interval interval : intervals) {
            if (pq.isEmpty()) {
                pq.offer(interval);
            }
            else {
                Interval prevInterval = pq.poll();
				// no need for a room, merge these two intervals
                if (prevInterval.end <= interval.start) {
                    prevInterval.end = interval.end;
                }
                // need a new room
                else {
                    pq.offer(interval);
                }
                // put origin meeting room back
                pq.offer(prevInterval);
            }
        }
		// The same as solution 1:
		/**
		for (Interval interval : intervals) {
            if (!pq.isEmpty() && pq.peek().end <= interval.start) {
                pq.poll();
            }
            pq.offer(interval);
        }
		*/
        return pq.size();
    }

	// Running Time Complexity: O(NlogN), Space Complexity: O(N)
	public int minMeetingRooms(List<Interval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            return 0;
        }
        int n = intervals.size();
        int[] starts = new int[n], ends = new int[n];
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        Arrays.sort(starts);
        Arrays.sort(ends);
        int res = 0, j = 0;
        for (int i = 0; i < n; i++) {
			// If starting time is less than end time, that meeting times are overrlapped, new room is needed.
			// increase the room number
            if (starts[i] < ends[j]) {
                res++;
            }
			// Otherwise, no new room is needed. increase the j pointer
            else {
                j++;
            }
        }
        return res;
    }

    // Leetcode 435: Non-overlapping Intervals
    // https://leetcode.com/problems/non-overlapping-intervals/
    /**
    Given a collection of intervals, find the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.

    Note:
    You may assume the interval's end point is always bigger than its start point.
    Intervals like [1,2] and [2,3] have borders "touching" but they don't overlap each other.
    Example 1:
    Input: [ [1,2], [2,3], [3,4], [1,3] ]

    Output: 1

    Explanation: [1,3] can be removed and the rest of intervals are non-overlapping.
    Example 2:
    Input: [ [1,2], [1,2], [1,2] ]

    Output: 2

    Explanation: You need to remove two [1,2] to make the rest of intervals non-overlapping.
    Example 3:
    Input: [ [1,2], [2,3] ]

    Output: 0

    Explanation: You don't need to remove any of the intervals since they're already non-overlapping.
    */
    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals == null || intervals.length == 0) {
            return 0;
        }
        int n = intervals.length;
        Arrays.sort(intervals, (i1, i2) -> (i1.end - i2.end));
        // solution 1:
        int end = -1, count = 1;
        for (Interval interval : intervals) {
            if (end == -1) {
                end = interval.end;
            }
            else {
                if (end <= interval.start) {
                    count++;
                    end = interval.end;
                }
            }
        }
        return n - count;
        // Solution 2:
        // Max Heap, let max end point on the top:
        Queue<Integer> pq = new PriorityQueue<Integer>(n, (i1, i2) -> (i2 - i1));
        for (Interval interval : intervals) {
            if (pq.isEmpty()) {
                pq.offer(interval.end);
            }
            else {
                if (pq.peek() <= interval.start) {
                    pq.offer(interval.end);
                }
            }
        }
        return n - pq.size();
    }
    // Faster Solution: Running Time Complexity: O(NlogN)
    /** Two pointers solution, left and right pointer:
    Move left pointer to right pointer only when:
      1. New Interval is completely inside the previous interval
      2. New Interval is completely outside of previous interval
    Increase counter when two intervals are partially overlap.
    */
    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals == null || intervals.length <= 1) {
            return 0;
        }
        Arrays.sort(intervals, (i1, i2) -> (i1.start - i2.start));
        int prev = 0, count = 0, n = intervals.length;
        for (int i = 1; i < n; i++) {
            if (intervals[prev].end > intervals[i].start) {
                if (intervals[prev].end > intervals[i].end) {
                    prev = i;
                }
                count++;
            }
            else {
                prev = i;
            }
        }
        return count;
    }
}