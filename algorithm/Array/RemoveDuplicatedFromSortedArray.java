public class Solution {
    // Leetcode 026: Remove Duplicates from Sorted Array
    // https://leetcode.com/problems/remove-duplicates-from-sorted-array/
    /**
    Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.

    Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

    Example 1:

    Given nums = [1,1,2],

    Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.

    It doesn't matter what you leave beyond the returned length.
    Example 2:

    Given nums = [0,0,1,1,1,2,2,3,3,4],

    Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.

    It doesn't matter what values are set beyond the returned length.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        int n = nums.length;
        if (n <= 1) {
            return n;
        }
        int idx = 1;
        for (int i = 1; i < n; i++) {
            if (nums[i] != nums[i-1]) {
                nums[idx++] = nums[i];
            }
        }
        return idx;
    }

    // Leetcode 080: Remove Duplicates from Sorted Array II
    // https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        int n = nums.length;
        if (n <= 2) {
            return n;
        }
        int idx = 1;
        boolean isMoreThanTwo = false;
        for (int i = 1; i < n; i++) {
            if (nums[i] != nums[i-1]) {
                nums[idx++] = nums[i];
                isMoreThanTwo = false;
            }
            else {
                if (!isMoreThanTwo) {
                    nums[idx++] = nums[i];
                    isMoreThanTwo = true;
                }
            }
        }
        return idx;
    }

    // Leetcode 283 Move Zeroes
    // https://leetcode.com/problems/move-zeroes/
    /**
   Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

    Example:

    Input: [0,1,0,3,12]
    Output: [1,3,12,0,0]
    Note:

    You must do this in-place without making a copy of the array.
    Minimize the total number of operations.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public void moveZeroes(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int idx = 0, n = nums.length;
        for (int i = 0; i < n; i++) {
            if (nums[i] != 0) {
               nums[idx++] = nums[i];
            }
        }
        // mark the rest of array as 0.
        for (int i = idx; i < n; i++) {
            nums[i] = 0;
        }
    }
}