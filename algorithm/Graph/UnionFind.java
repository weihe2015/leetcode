// Union Find Template:
// https://www.youtube.com/watch?v=SpqjfGTOriQ
public class WeightedUnionFind {
    // always ensure that smaller tree goes below larger tree
    private int[] id;
    private int[] size;

    // Overall Running time: O(M + M * logN)
    // M union find operations on a set of N objects
    public WeightedUnionFind(int N) {
        this.id = new int[N];
        this.size = new int[N];
        for (int i = 0; i < N; i++) {
            this.id[i] = i;
            this.size[i] = 1;
        }
    }

    // Running time of Find: proportional to depth of p and q, depth of any node x is at most logN
    // Given the index of the node, find the root of its node:
    private int find(int i) {
        while (i != id[i]) {
            // path compression: Set node points to its grandparent
            id[i] = id[id[i]];
            i = id[i];
        }
        return i;
    }

    // Running time of Union: constant time, given roots
    private void union(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);
        // If two root nodes are the same, then they are connected.
        if (pRoot == qRoot) {
            return;
        }
        // Merge smaller tree to larger tree and update the size array
        if (size[pRoot] < size[qRoot]) {
            id[pRoot] = qRoot;
            size[qRoot] += size[pRoot];
        }
        else {
            id[qRoot] = pRoot;
            size[pRoot] += size[qRoot];
        }
    }
}

public class Solution {
    // Leetcode 200: Number of Islands:
    // https://leetcode.com/problems/number-of-islands
    /**
    Given a 2d grid map of '1's (land) and '0's (water),
    count the number of islands. An island is surrounded by water and
    is formed by connecting adjacent lands horizontally or vertically.
    You may assume all four edges of the grid are all surrounded by water.

    Example 1:

    Input:
    11110
    11010
    11000
    00000

    Output: 1
    Example 2:

    Input:
    11000
    11000
    00100
    00011

    Output: 3
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int N = m * n;
        UnionFind uf = new UnionFind(N);
        // build graph:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    // current index of the id array:
                    int idx = i * n + j;
                    for (int[] dir : dirs) {
                        int x = i + dir[0];
                        int y = j + dir[1];
                        if (isInside(x, y, m, n) && grid[x][y] == '1') {
                            int neighborIndex = x * n + y;
                            uf.union(idx, neighborIndex);
                        }
                    }
                }
            }
        }
        int count = 0;
        for (int i = 0; i < N; i++) {
            // count only the root with label 1
            if (i == uf.id[i] && grid[i/n][i%n] == '1') {
                count++;
            }
        }
        return count;
    }

    private boolean isInside(int i, int j, int m, int n) {
        return (i >= 0 && i < m && j >= 0 && j < n);
    }

    // Leetcode 305 Number of Islands II:
    /**
    A 2d grid map of m rows and n columns is initially filled with water.
    We may perform an addLand operation which turns the water at position (row, col) into a land.
    Given a list of positions to operate, count the number of islands after each addLand operation.
    An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
    You may assume all four edges of the grid are all surrounded by water.

    Example:

    Given m = 3, n = 3, positions = [[0,0], [0,1], [1,2], [2,1]].
    Initially, the 2d grid grid is filled with water.
    (Assume 0 represents water and 1 represents land).

    0 0 0
    0 0 0
    0 0 0
    Operation #1: addLand(0, 0) turns the water at grid[0][0] into a land.

    1 0 0
    0 0 0   Number of islands = 1
    0 0 0
    Operation #2: addLand(0, 1) turns the water at grid[0][1] into a land.

    1 1 0
    0 0 0   Number of islands = 1
    0 0 0
    Operation #3: addLand(1, 2) turns the water at grid[1][2] into a land.

    1 1 0
    0 0 1   Number of islands = 2
    0 0 0
    Operation #4: addLand(2, 1) turns the water at grid[2][1] into a land.

    1 1 0
    0 0 1   Number of islands = 3
    0 1 0
    We return the result as an array: [1, 1, 2, 3]
    */
    // Running Time Complexity: O(k * log m*n), Space Complexity: O(m * n)
    private static final int[][] dirs = {{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
    public List<Integer> numIslands2(int n, int m, Point[] operators) {
        List<Integer> result = new ArrayList<Integer>();
        if (operators == null || operators.length == 0 || n == 0) {
            return result;
        }
        int N = n * m;
        UnionFind uf = new UnionFind(N);
        for (Point operator : operators) {
            int idx = operator.x * m + operator.y;
            uf.add(idx);
            for (int[] dir : dirs) {
                int x = operator.x + dir[0];
                int y = operator.y + dir[1];
                if (isInside(x, y, n, m)) {
                    int neighborIdx = x * m + y;
                    if (uf.id[neighborIdx] != -1) {
                        uf.union(idx, neighborIdx);
                    }
                }
            }
            result.add(uf.count);
        }
        return result;
    }

    private boolean isInside(int i, int j, int n, int m) {
        return (i >= 0 && i < n && j >= 0 && j < m);
    }

    class UnionFind {
        int[] id;
        int[] size;
        int count;

        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            this.count = 0;
            for (int i = 0; i < N; i++) {
                id[i] = -1;
            }
        }

        public void add(int idx) {
            // If this idx already has a root, no need to initialize this node.
            if (id[idx] != -1) {
                return;
            }
            id[idx] = idx;
            size[idx] = 1;
            count++;
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            count--;
        }
    }

    // Leetcode 547: Friend Circles:
    // https://leetcode.com/problems/friend-circles
    /**
    There are N students in a class. Some of them are friends, while some are not. Their friendship is transitive in nature.
    For example, if A is a direct friend of B, and B is a direct friend of C, then A is an indirect friend of C.
    And we defined a friend circle is a group of students who are direct or indirect friends.

    Given a N*N matrix M representing the friend relationship between students in the class. If M[i][j] = 1,
    then the ith and jth students are direct friends with each other, otherwise not.
    And you have to output the total number of friend circles among all the students.

    Example 1:
    Input:
    [[1,1,0],
    [1,1,0],
    [0,0,1]]
    Output: 2
    Explanation:The 0th and 1st students are direct friends, so they are in a friend circle.
    The 2nd student himself is in a friend circle. So return 2.
    Example 2:
    Input:
    [[1,1,0],
    [1,1,1],
    [0,1,1]]
    Output: 1
    Explanation:The 0th and 1st students are direct friends, the 1st and 2nd students are direct friends,
    so the 0th and 2nd students are indirect friends. All of them are in the same friend circle, so return 1.
    Note:
    N is in range [1,200].
    M[i][i] = 1 for all students.
    If M[i][j] = 1, then M[j][i] = 1.
    */
    // Running Time Complexity: O(N + N*N*logN)
    public int findCircleNum(int[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        UnionFind uf = new UnionFind(n);
        for (int i = 0; i < m; i++) {
            for (int j = i+1; j < n; j++) {
                if (grid[i][j] == 1) {
                    uf.union(i, j);
                }
            }
        }
        return uf.count;
    }

    class UnionFind {
        int[] id;
        int[] size;
        int count;
        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            // All people are friend of themselves. So initialize count = N, where N is number of people.
            this.count = N;
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            // If there is one union of friend, we decrement the number of friends.
            count--;
        }
    }

    // Leetcode 128: Longest Consecutive Sequence:
    /**
    Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

    For example,
    Given [100, 4, 200, 1, 3, 2],
    The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.

    Your algorithm should run in O(n) complexity.
    */
    public int longestConsecutive(int[] nums) {
        int n = nums.length;
        UnionFind uf = new UnionFind(n);
        // Key -> nums[i], value -> index i
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < n; i++) {
            if (map.containsKey(nums[i])) {
                continue;
            }
            map.put(nums[i],i);
            if (map.containsKey(nums[i]+1)) {
                uf.union(i, map.get(nums[i]+1));
            }
            if (map.containsKey(nums[i]-1)) {
                uf.union(i, map.get(nums[i]-1));
            }
        }
        return uf.getMaxConnectedNumber();
    }

    class UnionFind {
        int[] id;
        int[] size;
        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }

        public int find(int idx) {
            while (idx != id[idx]) {
                id[idx] = id[id[idx]];
                idx = id[idx];
            }
            return idx;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
        }

        public int getMaxConnectedNumber() {
            int maxResult = 0;
            for (int num : size) {
                maxResult = Math.max(maxResult, num);
            }
            return maxResult;
        }
    }

    // Leetcode 130: Surrounded Regions:
    // https://leetcode.com/problems/surrounded-regions
    /**
    Given a 2D board containing 'X' and 'O' (the letter O), capture all regions surrounded by 'X'.

    A region is captured by flipping all 'O's into 'X's in that surrounded region.

    Example:

    X X X X
    X O O X
    X X O X
    X O X X
    After running your function, the board should be:

    X X X X
    X X X X
    X X X X
    X O X X
    Explanation:

    Surrounded regions shouldn’t be on the border, which means that any 'O' on the border of the board are not flipped to 'X'.
    Any 'O' that is not on the border and it is not connected to an 'O' on the border will be flipped to 'X'.
    Two cells are connected if they are adjacent cells connected horizontally or vertically.
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public void solve(char[][] board) {
        if (board == null || board.length == 0) {
            return;
        }
        int m = board.length, n = board[0].length;
        int N = m * n + 1;
        UnionFind uf = new UnionFind(N);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    int idx = i * n + j;
                    // connect any 'O' node which is on the boundary to a dumy node:
                    if (i == 0 || i == m-1 || j == 0 || j == n-1) {
                        uf.union(idx, N-1);
                    }
                    else {
                        // connect a 'O' node to its neighbour 'O' nodes
                        for (int[] dir : dirs) {
                            int x = i + dir[0];
                            int y = j + dir[1];
                            int newIdx = x * n + y;
                            if (board[x][y] == 'O') {
                                uf.union(idx, newIdx);
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int idx = i * n + j;
                // if a 'O' node is not connected to the dummy node, it is captured and convert it to 'X'
                if (!uf.isConnected(idx, N-1)) {
                    board[i][j] = 'X';
                }
            }
        }
    }

    class UnionFind {
        int[] id;
        int[] size;

        public UnionFind(int N) {
            id = new int[N];
            size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }

        public boolean isConnected(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            return pRoot == qRoot;
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
        }
    }

    // Leetcode 684: Redundant Connection
    // https://leetcode.com/problems/redundant-connection/
    /**
    The given input is a graph that started as a tree with N nodes (with distinct values 1, 2, ..., N), with one additional edge added.
    The added edge has two different vertices chosen from 1 to N, and was not an edge that already existed.

    The resulting graph is given as a 2D-array of edges. Each element of edges is a pair [u, v] with u < v, that represents an undirected edge connecting nodes u and v.

    Return an edge that can be removed so that the resulting graph is a tree of N nodes. If there are multiple answers, return the answer that occurs last in the given 2D-array. The answer edge [u, v] should be in the same format, with u < v.

    Example 1:
    Input: [[1,2], [1,3], [2,3]]
    Output: [2,3]
    Explanation: The given undirected graph will be like this:
    1
    / \
    2 - 3
    Example 2:
    Input: [[1,2], [2,3], [3,4], [1,4], [1,5]]
    Output: [1,4]
    Explanation: The given undirected graph will be like this:
    5 - 1 - 2
        |   |
        4 - 3
    Note:
    The size of the input 2D-array will be between 3 and 1000.
    Every integer represented in the 2D-array will be between 1 and N, where N is the size of the input array.
    */

    /**
    Initially, each node can be regarded as a disjoint set.
    When we count an edge in, we UNION two nodes.
    If two nodes have already been in the same disjoint set (we detect that with the help of FIND), the current edge is redundant.
    For example,

    1
    / \
    2 - 3
    Initially, there are 3 disjoint sets: 1, 2, 3.
    Edge [1,2] connects 1 to 2, i.e., 1 and 2 are grouped together.
    Edge [1,3] connects 1 to 3, i.e., 1 and 3 are grouped together.
    Edge [2,3] connects 2 to 3, but 2 and 3 have been in the same disjoint set already, so [2, 3] is redundant.
    */
    public int[] findRedundantConnection(int[][] edges) {
        int N = edges.length + 1;
        UnionFind uf = new UnionFind(N);
        for (int[] edge : edges) {
            int from = edge[0];
            int to = edge[1];
            if (!uf.union(from, to)) {
                return edge;
            }
        }
        return null;
    }

    class UnionFind {
        int[] id;
        int[] size;

        public UnionFind(int N) {
            id = new int[N];
            size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public boolean union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return false;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            return true;
        }
    }

    // Leetcode 261: Graph Valid Tree
    // https://leetcode.com/problems/graph-valid-tree
    // https://www.lintcode.com/problem/graph-valid-tree
    /**
    Given n nodes labeled from 0 to n-1 and a list of undirected edges (each edge is a pair of nodes),
    write a function to check whether these edges make up a valid tree.

    Example 1:

    Input: n = 5, and edges = [[0,1], [0,2], [0,3], [1,4]]
    Output: true
    Example 2:

    Input: n = 5, and edges = [[0,1], [1,2], [2,3], [1,3], [1,4]]
    Output: false
    Note: you can assume that no duplicate edges will appear in edges.
    Since all edges are undirected, [0,1] is the same as [1,0] and
    thus will not appear together in edges.
    */
    public boolean validTree(int n, int[][] edges) {
        // write your code here
        UnionFind uf = new UnionFind(n);
        for (int[] edge : edges) {
            if (uf.isConnected(edge[0], edge[1])) {
                return false;
            }
            uf.union(edge[0], edge[1]);
        }
        return uf.count == 1;
    }

    class UnionFind {
        int[] id;
        int[] size;
        int count;

        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
            this.count = N;
        }

        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }

        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            count--;
        }

        public boolean isConnected(int p, int q) {
            return find(p) == find(q);
        }
    }
}