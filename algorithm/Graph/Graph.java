public class Solution {
    // Find cycle in undirected graph:
    /** 
    DFS: For every visited vertex 'v', if there is an adjacent 'u' 
    such that u is already visited and u is not parent of v, then
    there is a cycle in graph 
    */
    public boolean canFinish(int num, int[][] edges) {
        List<List<Integer>> graph = new ArrayList<>();
        // Build Graph:
        for (int i = 0; i < num; i++) {
            graph.add(new ArrayList<Integer>());
        }
        for (int[] edge : edges) {
            int from = edge[0];
            int to = edge[1];
            graph.get(from).add(to);
            graph.get(to).add(from);
        }
        boolean[] visited = new boolean[num];
        for (int i = 0; i < num; i++) {
            if (!visited[i] && isCyclic(graph, visited, from, -1)) {
                return false;
            }
        }
        return true;
    }

    private boolean isCyclic(List<List<Integer>> graph, boolean[] visited, int from, int parent) {
        visited[from] = true;
        for (int neighbor : graph.get(from)) {
            // neighbor 没visited的会在这里下去
            if (!visited[neighbor] &&　isCyclic(graph, visited, neighbor, from)) {
                return true;
            }
            // 如果邻居是visited的，一定是parent，如果不是，就证明有环
            else if (neighbor != parent) {
                return true;
            }
        }
        return false;
    }

    // Find Cycle in undirected graph with BFS:
    private boolean isCyclic(List<List<Integer>> graph, int num) {
        boolean[] visited = new boolean[num];
        for (int i = 0; i < num; i++) {
            if (!visited[i] && isCyclic(graph, i, num)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isCyclic(List<List<Integer>> grpah, boolean[] visited, int from, int num) {
        int[] parent = new int[num];
        Arrays.fill(parent, -1);
        Queue<Integer> q = new LinkedList<Integer>();
        visited[from] = true;
        q.offer(from);
        while (!q.isEmpty()) {
            int u = q.poll();
            for (int neighbor : graph.get(u)) {
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    parent[neighbor] = u;
                    q.offer(neighbor);
                }
                // 如果邻居是visited的，一定是parent，如果不是，就证明有环
                else if (parent[u] != neighbor) {
                    return true;
                }
            }
        }
        return false;
    }

    // Leetcode 207: Course Schedule
    // https://leetcode.com/problems/course-schedule/
    // Check if there is cycle in a directed graph:
    // DFS: Use DFS to detect whether there is a cycle
    // Adjacent List:
    // https://www.youtube.com/watch?v=joqmqvHC_Bo
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        List<List<Integer>> graph = new ArrayList<>();
        // Build Graph:
        for (int i = 0; i < numCourses; i++) {
        	graph.add(new ArrayList<Integer>());
        }
        for (int[] prerequisite : prerequisites) {
            int to = prerequisite[0];
            int from = prerequisite[1];
            graph.get(from).add(to);
        }
        boolean[] visited = new boolean[numCourses];
        boolean[] recStack = new boolean[numCourses];
        for (int i = 0; i < numCourses; i++) {
            if (!visited[i] && isCyclic(graph, visited, recStack, i)) {
                return false;
            }        
        }
        return true;
    }
    
    private boolean isCyclic(List<List<Integer>> graph, boolean[] visited, boolean[] recStack, int from) {
        visited[from] = true;
        recStack[from] = true;
        for (int childNode : graph.get(from)) {
            if (!visited[childNode] && isCyclic(graph, visited, recStack, childNode)) {
                return true;
            }
            else if (recStack[childNode]) {
                return true;
            }
        }
        recStack[from] = false;
        return false;
    }

    // Another solution of DFS:
    // Running Time Complexity: O(|V| + |E|), Space Complexity: O(V)
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < numCourses; i++) {
            graph.add(new ArrayList<Integer>());
        }
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph.get(from).add(to);
        }
        int[] visited = new int[numCourses];
        for (int i = 0; i < numCourses; i++) {
            if (visited[i] == 0 && isCyclic(graph, visited, i)) {
                return false;
            }
        }
        return true;
    }
    /**
        0 -> not yet visited
        1 -> is visiting:
        2 -> already visited:
    */
    private boolean isCyclic(List<List<Integer>> graph, int[] visited, int from) {
        if (visited[from] == 2) {
            return false;
        }
        else if (visited[from] == 1) {
            return true;
        }
        else {
            visited[from] = 1;
            for (int neighbor : graph.get(from)) {
                if (visited[neighbor] == 1 && isCyclic(graph, visited, neighbor)) {
                    return true;
                }
            }
            visited[from] = 2;
            return false;
        }
    }

    // BFS with adjacent matrix version:
    /**
    Kahn's algorithm for Topological Sorting:
    A DAG has at least one vertex with in-degree 0 and one vertex with out-degree 0
        Otherwise, it contains a cycle
    1. Compute in-degree (number of incoming edges) for each of the vertex present in DAG
       and intialize the count of visited nodes as 0
    2. Pick all the vertices with in-degree as 0 and add them into a queue
    3. Remove a vertex from the queue and then.
       3.1 Increment count of visited nodes by 1
       3.2 Decrease in-degree by 1 for all its neighboring nodes
       3.3 If in-degree of a neighboring nodes is reduced to zero, then add it to queue
    4. Repeat step 3 until queue is empty
    5. If count of visited nodes is not equals to the number of nodes in the graph:
       then it has cycle, topological sort is not possible for given graph.
    */
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        // Build Graph:
        int[][] graph = new int[numCourses][numCourses];
        int[] indegree = new int[numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph[from][to] = 1;
            // We assume that there is no duolicate edges
            indegree[to]++;
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        // Add all vertex with in-degree = 0 into queue, these vertex are the source vertex
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int count = 0;
        while (!queue.isEmpty()) {
            int from = queue.poll();
            count++;
            // Explore all neighbors:
            for (int i = 0; i < numCourses; i++) {
                if (graph[from][i] == 1) {
                    indegree[i]--;
                    if (indegree[i] == 0) {
                        queue.offer(i);
                    }
                }
            }
        }
        return count == numCourses;
    }

    // BFS with adjacent list version:
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        // Build Graph:
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < numCourses; i++) {
            graph.add(new ArrayList<Integer>());
        }
        int[] indegree = new int[numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph.get(from).add(to);
            indegree[to]++;
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int count = 0;
        while (!queue.isEmpty()) {
            int from = queue.poll();
            count++;
            for (int to : graph.get(from)) {
                indegree[to]--;
                if (indegree[to] == 0) {
                    queue.offer(to);
                }
            }
        }
        return count == numCourses;
    }

    // Leetcode 210: Course Schedule II:
    // https://leetcode.com/problems/course-schedule-ii/
    // BFS Kahn's algorithm for Topological Sorting:
    // Running Time Complexity: O(|V| + |E|)
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        int[][] graph = new int[numCourses][numCourses];
        int[] indegree = new int[numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph[from][to] = 1;
            indegree[to]++;
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int count = 0;
        int[] result = new int[numCourses];
        while (!queue.isEmpty()) {
            int from = queue.poll();
            // We always starts with source node, which indegree = 0, so it is at the beginning in topological sorting.
            result[count] = from;
            count++;
            for (int i = 0; i < numCourses; i++) {
                if (graph[from][i] == 1) {
                    indegree[i]--;
                    if (indegree[i] == 0) {
                        queue.offer(i);
                    }
                }
            }
        }
        return count == numCourses ? result : new int[0];
    }

    // DFS Solution:
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < numCourses; i++) {
            graph.add(new ArrayList<Integer>());
        }
        
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            graph.get(from).add(to);
        }
        boolean[] visited = new boolean[numCourses];
        boolean[] recStack = new boolean[numCourses];
        Deque<Integer> stack = new ArrayDeque<Integer>();
        for (int i = 0; i < numCourses; i++) {
            if (!visited[i] && isCyclic(graph, visited, recStack, i, stack)) {
                return new int[0];
            }
        }
        int[] result = new int[numCourses];
        int idx = 0; 
        while (!stack.isEmpty()) {
            result[idx] = stack.pop();
            idx++;
        }
        return result;
    }
    
    private boolean isCyclic(List<List<Integer>> graph, boolean[] visited, boolean[] recStack, int from, Deque stack) {
        visited[from] = true;
        recStack[from] = true;
        for (int neighbor : graph.get(from)) {
            if (!visited[neighbor] && isCyclic(graph, visited, recStack, neighbor, stack)) {
                return true;
            }
            else if (recStack[neighbor]) {
                return true;
            }
        }
        stack.push(from);
        recStack[from] = false;
        return false;
    }

    /**
    Leetcode 269 Alien Dictionary:
    https://leetcode.com/problems/alien-dictionary
    https://www.lintcode.com/problem/alien-dictionary/description
    There is a new alien language which uses the latin alphabet. 
    However, the order among letters are unknown to you. 
    You receive a list of non-empty words from the dictionary, 
    where words are sorted lexicographically by the rules of this new language. 
    Derive the order of letters in this language.

Example 1:

Input:
[
  "wrt",
  "wrf",
  "er",
  "ett",
  "rftt"
]

Output: "wertf"
Example 2:

Input:
[
  "z",
  "x"
]

Output: "zx"
Example 3:

Input:
[
  "z",
  "x",
  "z"
] 

Output: "" 
Explanation: The order is invalid, so return "".
    */
    // BFS Kahn's algorithm for Topological Sorting:
    /**
     * @param words: a list of words
     * @return: a string which is correct order
     */
    public String alienOrder(String[] words) {
        // build graph
        int[] indegree = new int[26];

        Map<Character, Set<Character>> graphMap = new HashMap<>();
        for (String word : words) {
            for (char c : word.toCharArray()) {
                if (!graphMap.containsKey(c)) {
                    graphMap.put(c, new HashSet<>());
                }
            }
        }
    
        for (int i = 1, n = words.length; i < n; i++) {
            String word1 = words[i-1];
            String word2 = words[i];
            int minLen = Math.min(word1.length(), word2.length());
            
            for (int j = 0; j < minLen; j++) {
                char c1 = word1.charAt(j);
                char c2 = word2.charAt(j);
                if (c1 != c2) {
                    if (!graphMap.get(c1).contains(c2)) {
                        indegree[c2-'a']++;
                        graphMap.get(c1).add(c2);
                    }
                    break;
                }
            }
        }
                
        StringBuffer sb = new StringBuffer();
        Queue<Character> queue = new LinkedList<>();
        for (char c : graphMap.keySet()) {
            if (indegree[c-'a'] == 0) {
                queue.offer(c);
            }
        }

        while (!queue.isEmpty()) {
            char c = queue.poll();
            sb.append(c);
            if (!graphMap.containsKey(c)) {
                continue;
            }
            Set<Character> neighbors = graphMap.get(c);
            for (char neighbor : neighbors) {
                indegree[neighbor-'a']--;
                if (indegree[neighbor-'a'] == 0) {
                    queue.offer(neighbor);
                }
            }
        }
        return sb.length() == graphMap.size() ? sb.toString() : "";
    }

    // Connected Component:
    public class CC {
        private boolean[] visited;
        private int[] componentId;

        public CC(Graph G) {
            visited = new boolean[G.V()];
            int count = 0;
            componentId = new int[G.V()];
            for (int i = 0, n = G.V(); i < n; i++) {
                if (!visited[i]) {
                    dfs(G, i, count);
                    count++;
                }
            }
        }

        private void dfs(Graph G, int v, int count) {
            visited[v] = true;
            componentId[v] = count;
            for (int w : G.adj(v)) {
                if (!visited[w]) {
                    dfs(G, w, count);
                }
            }
        }
    }

    // Leetcode 785: Is Graph Bipartite:
    // https://leetcode.com/problems/is-graph-bipartite
    /**
    Given an undirected graph, return true if and only if it is bipartite.

    Recall that a graph is bipartite if we can split it's set of nodes into two independent subsets A and B such that every edge in the graph has one node in A and another node in B.

    The graph is given in the following form: graph[i] is a list of indexes j for which the edge between nodes i and j exists.  Each node is an integer between 0 and graph.length - 1.  There are no self edges or parallel edges: graph[i] does not contain i, and it doesn't contain any element twice.

    Example 1:
    Input: [[1,3], [0,2], [1,3], [0,2]]
    Output: true
    Explanation: 
    The graph looks like this:
    0----1
    |    |
    |    |
    3----2
    We can divide the vertices into two groups: {0, 2} and {1, 3}.
    Example 2:
    Input: [[1,2,3], [0,2], [0,1,3], [0,2]]
    Output: false
    Explanation: 
    The graph looks like this:
    0----1
    | \  |
    |  \ |
    3----2
    We cannot find a way to divide the set of nodes into two independent subsets.
    */
    public boolean isBipartite(int[][] graph) {
        int n = graph.length;
        // 0 => not visited, 1 => black 2 => white
        int[] visited = new int[n];
        for (int i = 0; i < n; i++) {
            if (visited[i] == 0) {
                visited[from] = 1;
                if (!isBipartiteBFS(graph, visited, i)) {
                    return false;
                }
            }
        }
        return true;
    }
    // BFS:
    private boolean isBipartiteBFS(int[][] graph, int[] visited, int from) {
        Queue<Integer> q = new LinkedList<Integer>();
        q.offer(from);
        while (!q.isEmpty()) {
            int u = q.poll();
            for (int v : graph[u]) {
                if (visited[v] == 0) {
                    visited[u] = visited[from] == 1 ? 2 : 1;
                    q.offer(v);
                }
                else if (visited[u] == visited[v]) {
                    return false;
                }
            }
        }
        return true;
    }
    // DFS:
    public boolean isBipartite(int[][] graph) {
        int n = graph.length;
        // 0 => not visited, 1 => black 2 => white
        int[] visited = new int[n];
        for (int i = 0; i < n; i++) {
            if (visited[i] == 0) {
                visited[i] = 1;
                if (!isBipartiteDFS(graph, visited, i)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    private boolean isBipartiteDFS(int[][] graph, int[] visited, int from) {
        for (int u : graph[from]) {
            if (visited[u] == 0) {
                visited[u] = visited[from] == 1 ? 2 : 1;
                if (!isBipartiteDFS(graph, visited, u)) {
                    return false;
                }
            }
            else if (visited[u] == visited[from]) {
                return false;
            }
        }
        return true;
    }

    // Leetcode 310: Minimum Height Tree:
    // https://leetcode.com/problems/minimum-height-trees/
    /**
    For a undirected graph with tree characteristics, we can choose any node as the root. 
    The result graph is then a rooted tree. Among all possible rooted trees, those with minimum height are called minimum height trees (MHTs). 
    Given such a graph, write a function to find all the MHTs and return a list of their root labels.

    Format
    The graph contains n nodes which are labeled from 0 to n - 1. You will be given the number n and a list of undirected edges (each edge is a pair of labels).

    You can assume that no duplicate edges will appear in edges. 
    Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.

    Example 1 :

    Input: n = 4, edges = [[1, 0], [1, 2], [1, 3]]

            0
            |
            1
        / \
        2   3 

    Output: [1]
    Example 2 :

    Input: n = 6, edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]]

        0  1  2
        \ | /
            3
            |
            4
            |
            5 

    Output: [3, 4]
    */
    /**
    基本思路是“逐层删去叶子节点，直到剩下根节点为止”
    有点类似于拓扑排序
    最终剩下的节点个数可能为1或者2
    */
    // Running Time Complexity: O(|V| + |E|)
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> result = new ArrayList<Integer>();
        if (n <= 0) {
            return result;
        }
        if (n == 1) {
            result.add(0);
            return result;
        }
        // Build Graph:
        List<Set<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new HashSet<Integer>());
        }
        for (int[] edge : edges) {
            int from = edge[0];
            int to = edge[1];
            graph.get(from).add(to);
            graph.get(to).add(from);
        }
        Queue<Integer> q = new LinkedList<Integer>();
        // Add all leaves into a queue. A leaf meaning only has one vertex neighbor:
        for (int i = 0; i < n; i++) {
            if (graph.get(i).size() == 1) {
                q.offer(i);
            }
        }
        while (!q.isEmpty()) {
            int size = q.size();
            result.clear();
            for (int i = 1; i <= size; i++) {
                int u = q.poll();
                result.add(u);
                for (int v : graph.get(u)) {
                    // Remove obj u from list v
                    graph.get(v).remove(u);
                    if (graph.get(v).size() == 1) {
                        q.offer(v);
                    }
                }
            }
        }
        return result;
    }
    // Solution 2:
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> result = new ArrayList<Integer>();
        if (n == 0) {
            return result;
        }
        if (n == 1) {
            result.add(0);
            return result;
        }
        // Build Graph with Adjacent List:
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<Integer>());
        }
        int[] indegree = new int[n];
        for (int[] edge : edges) {
            int from = edge[0];
            int to = edge[1];
            graph.get(from).add(to);
            graph.get(to).add(from);
            indegree[from]++;
            indegree[to]++;
        }
        // Add all source vertex into Queue
        Queue<Integer> q = new LinkedList<Integer>();
        for (int i = 0; i < n; i++) {
            if (indegree[i] == 1) {
                q.offer(i);
            }
        }
        // BFS
        /**
        所有indegree = 1的vertex都是叶子，从叶子开始做BFS，历遍叶子的所有
        邻居，到最后的时候就是Minimum Height Trees的root，结果有一个或者两个vertex。
        */ 
        boolean[] visited = new boolean[n];
        while (!q.isEmpty()) {
            int size = q.size();
            // 舍去之前的结果，因为
            result.clear();
            for (int i = 1; i <= size; i++) {
                int u = q.poll();
                visited[u] = true;
                result.add(u);
                for (int v : graph.get(u)) {
                    indegree[v]--;
                    if (indegree[v] == 1 && !visited[v]) {
                        q.offer(v);
                    }
                }
            }
        }
        return result;
    }
}