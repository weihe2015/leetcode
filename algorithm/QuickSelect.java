public class Solution {
    // https://www.youtube.com/watch?v=D35llNtkCps

    /**
    Find the smallest item in the unsorted array.
    */
    public int findSmallest(int[] nums) {
        int min = Integer.MAX_VALUE;
        for (int num : nums) {
            min = Math.min(min, num);
        }
        return min;
    }

    /**
    Find the second smallest item in the unsorted array.
    */
    public int findSecondSmallest(int[] nums) {
        int min1 = Integer.MAX_VALUE;
        int min2 = Integer.MAX_VALUE;
        for (int num : nums) {
            if (num < min1) {
                min2 = min1;
                min1 = num;
            }
            else if (min1 < num && num < min2) {
                min2 = num;
            }
        }
        return min2;
    }

    /**
    For any valid k, find the kth smallest item in the unsorted array.
    */
    private int findKSmallest(int[] nums, int k) {
        return findKSmallest(nums, 0, nums.length-1, k);
    }

    private int findKSmallest(int[] nums, int low, int high, int k) {
        int idx = partition(nums, low, high);
        if (idx == k) {
            return nums[k];
        }
        else if (idx < k) {
            return findKSmallest(nums, idx+1, high, k);
        }
        else {
            return findKSmallest(nums, low, idx-1, k);
        }
    }

    private int partition(int[] nums, int low, int high) {
        int pivot = high;
        int i = low;
        for (int j = low; j < high; j++) {
            if (nums[j] < nums[pivot]) {
                swap(nums, i, j);
                i++;
            }
        }
        swap(nums, i, pivot);
        return i;
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}