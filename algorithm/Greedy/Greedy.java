public class Solution {
    // Leetcode 630 Course Schedule III
    // https://leetcode.com/problems/course-schedule-iii
    /**
    There are n different online courses numbered from 1 to n. Each course has some duration(course length) t and closed on dth day. A course should be taken continuously for t days and must be finished before or on the dth day. You will start at the 1st day.

    Given n online courses represented by pairs (t,d), your task is to find the maximal number of courses that can be taken.

    Example:
    Input: [[100, 200], [200, 1300], [1000, 1250], [2000, 3200]]
    Output: 3
    Explanation: 
    There're totally 4 courses, but you can take 3 courses at most:
    First, take the 1st course, it costs 100 days so you will finish it on the 100th day, and ready to take the next course on the 101st day.
    Second, take the 3rd course, it costs 1000 days so you will finish it on the 1100th day, and ready to take the next course on the 1101st day. 
    Third, take the 2nd course, it costs 200 days so you will finish it on the 1300th day. 
    The 4th course cannot be taken now, since you will finish it on the 3300th day, which exceeds the closed date.
    Note:
    The integer 1 <= d, t, n <= 10,000.
    You can't take two courses simultaneously.
    */
    // Running Time Complexity: O(NlogN + N * logN) = O(NlogN)
    public int scheduleCourse(int[][] courses) {
        // Sort the courses by their deadlines (Greedy! We have to deal with courses with early deadlines first)
        Arrays.sort(courses, (a, b) -> (a[1] - b[1]));
        // Max Heap: Sort by latest deadline
        Queue<Integer> pq = new PriorityQueue<Integer>((a, b) -> (b - a));
        int time = 0;
        for (int[] course : courses) {
            time += course[0];
            pq.add(course[0]);
            if (time > course[1]) {
                time -= pq.poll();
            }
        }
        return pq.size();
    }

    // Leetcode 55 Jump Game:
    // https://leetcode.com/problems/jump-game
    /*
    Given an array of non-negative integers, you are initially positioned at the first index of the array.

    Each element in the array represents your maximum jump length at that position.

    Determine if you are able to reach the last index.

    Example 1:

    Input: [2,3,1,1,4]
    Output: true
    Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
    Example 2:

    Input: [3,2,1,0,4]
    Output: false
    Explanation: You will always arrive at index 3 no matter what. Its maximum
                jump length is 0, which makes it impossible to reach the last index.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public boolean canJump(int[] nums) {
        int reach = 0;
        for (int i = 0, n = nums.length; i < n; i++) {
            // previous reach cannot reach current position, so break and return false
            if (i > reach) {
                break;
            }
            // the maximum idx that we can reach from current idx:
            int currMaxReach = i + nums[i];
            reach = Math.max(reach, currMaxReach);
            // If max position we can reach is further than last idx, we return true
            if (reach >= n-1) {
                return true;
            }
        }
        return false;
    }

    // Leetcode 122: Best Time to Buy and Sell Stock II:
    // https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
    /**
    Say you have an array for which the ith element is the price of a given stock on day i.

    Design an algorithm to find the maximum profit. You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).

    Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).

    Example 1:

    Input: [7,1,5,3,6,4]
    Output: 7
    Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
                Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
    Example 2:

    Input: [1,2,3,4,5]
    Output: 4
    Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
                Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
                engaging multiple transactions at the same time. You must sell before buying again.
    Example 3:

    Input: [7,6,4,3,1]
    Output: 0
    Explanation: In this case, no transaction is done, i.e. max profit = 0.
    */
    public int maxProfit(int[] prices) {
        int profit = 0;
        for (int i = 1, n = prices.length; i < n; i++) {
            // Make the locally optimal choice at each stage
            profit += Math.max(0, prices[i] - prices[i-1]);
        }
        return profit;
    }

    // Leetcode 134: Gas Station:
    // https://leetcode.com/problems/gas-station/
    /**
    There are N gas stations along a circular route, where the amount of gas at station i is gas[i].

    You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from station i to its next station (i+1). You begin the journey with an empty tank at one of the gas stations.

    Return the starting gas station's index if you can travel around the circuit once in the clockwise direction, otherwise return -1.

    Note:

    If there exists a solution, it is guaranteed to be unique.
    Both input arrays are non-empty and have the same length.
    Each element in the input arrays is a non-negative integer.
    Example 1:

    Input: 
    gas  = [1,2,3,4,5]
    cost = [3,4,5,1,2]

    Output: 3

    Explanation:
    Start at station 3 (index 3) and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
    Travel to station 4. Your tank = 4 - 1 + 5 = 8
    Travel to station 0. Your tank = 8 - 2 + 1 = 7
    Travel to station 1. Your tank = 7 - 3 + 2 = 6
    Travel to station 2. Your tank = 6 - 4 + 3 = 5
    Travel to station 3. The cost is 5. Your gas is just enough to travel back to station 3.
    Therefore, return 3 as the starting index.
    Example 2:

    Input: 
    gas  = [2,3,4]
    cost = [3,4,3]

    Output: -1

    Explanation:
    You can't start at station 0 or 1, as there is not enough gas to travel to the next station.
    Let's start at station 2 and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
    Travel to station 0. Your tank = 4 - 3 + 2 = 3
    Travel to station 1. Your tank = 3 - 3 + 3 = 3
    You cannot travel back to station 2, as it requires 4 unit of gas but you only have 3.
    Therefore, you can't travel around the circuit once no matter where you start.
    */
    /**
    Basic Idea:
    假设开始设置起点start = 0, 并从这里出发，如果当前的gas值大于cost值，就可以继续前进，此时到下一个站点，剩余的gas加上当前的gas再减去cost，看是否大于0，
    若大于0，则继续前进。当到达某一站点时，若这个值小于0了，则说明从起点到这个点中间的任何一个点都不能作为起点，
    则把起点设为下一个点，继续遍历。当遍历完整个环时，当前保存的起点即为所求。
    */
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int tank = 0, total = 0, start = 0;
        for (int i = 0, n = gas.length; i < n; i++) {
            tank += gas[i] - cost[i];
            total += gas[i] - cost[i];
            if (tank < 0) {
                tank = 0;
                start = i + 1;
            }
        }
        return total < 0 ? -1 : start;
    }

    // Leetcode 135: Candy:
    // https://leetcode.com/problems/candy
    /**
    There are N children standing in a line. Each child is assigned a rating value.

    You are giving candies to these children subjected to the following requirements:

    Each child must have at least one candy.
    Children with a higher rating get more candies than their neighbors.
    What is the minimum candies you must give?

    Example 1:

    Input: [1,0,2]
    Output: 5
    Explanation: You can allocate to the first, second and third child with 2, 1, 2 candies respectively.
    Example 2:

    Input: [1,2,2]
    Output: 4
    Explanation: You can allocate to the first, second and third child with 1, 2, 1 candies respectively.
                The third child gets 1 candy because it satisfies the above two conditions.
    */
    /** 1, 0, 2
       init: 1, 1, 1
       -> 1, 1, 2
       <- 2, 1, 2
    */
    /**
    1. 首先每个人分一个糖果
    2. 从左向右历遍，如果右边的小朋友的等级比左边高，那么右边的小朋友的糖果数加1
    3. 从右向左历遍，如果左边的小朋友的等级比右边高，那么左边的小朋友的糖果就是自己原来的，或者是右边的小朋友的糖果数加1
    4. 把所有小朋友的糖果数加起来就是答案。
    */
    public int candy(int[] ratings) {
        int n = ratings.length;
        int[] nums = new int[n];
        Arrays.fill(nums, 1);
        for (int i = 1; i < n; i++) {
            if (ratings[i-1] < ratings[i]) {
                nums[i] = nums[i-1] + 1; 
            }
        }
        for (int i = n-1; i > 0; i--) {
            if (ratings[i-1] > ratings[i]) {
                nums[i-1] = Math.max(nums[i-1], nums[i]+1);
            }
        }
        int count = 0;
        for (int num : nums) {
            count += num;
        }
        return count;
    }
}