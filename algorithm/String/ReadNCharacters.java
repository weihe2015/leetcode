public class ReadNCharacters extends Reader4 {
    
    /**
    * The API: int read4(char *buf) reads 4 characters at a time from a file.

    The return value is the actual number of characters read. For example, it returns 3 if there is only 3 characters left in the file.

    By using the read4 API, implement the function int read(char *buf, int n) that reads n characters from the file.

    Example 1:

    Input: buf = "abc", n = 4
    Output: "abc"
    Explanation: The actual number of characters read is 3, which is "abc".
    Example 2:

    Input: buf = "abcde", n = 5 
    Output: "abcde"
    Note:
    The read function will only be called once for each test case.
    * */
    public int read1(char[] buf, int n) {
        char[] tmp = new char[4];
        boolean eof = false;
        int readBytes = 0;
        while (readBytes < n && !eof) {
            int currReadBytes = read4(tmp);
            if (currReadBytes < 4) {
                eof = true;
            }
            // 取buf剩下要读的 n-readBytes 和已读的currReadBytes 的较小值
            int len = Math.min(n - readBytes, currReadBytes);
            System.arraycopy(tmp, 0, buf, readBytes, len);
            readBytes += len;
        }
        return readBytes;
    }

    // Solution 1:
    private int buffPtr = 0;
    private int buffCnt = 0;
    private char[] cache = new char[4];
    public int read(char[] buf, int n) {
        int i = 0;
        while (i < n) {
            // When all characters in cache are read, get new 4 characters from read4
            if (buffPtr == 0) {
                buffCnt = read4(cache);
            }
            // There are no more new characters, break
            if (buffCnt == 0) {
                break;
            }
            // copy cache characters to buf
            while (i < n && buffPtr < buffCnt) {
                buf[i] = cache[buffPtr];
                i++;
                buffPtr++;
            }
            // When all characters are transfered to buf, reset buffPtr
            if (buffPtr == buffCnt) {
                buffPtr = 0;
            }
        }
        return i;
    }
    
    // Solution 2:
    private Queue<Character> queue = new LinkedList<>();
    public int read(char[] buf, int n) {
        // Write your code here
        int i = 0;
        while (i < n && !queue.isEmpty()) {
            buf[i] = queue.poll();
            i++;
        }
        while (i < n) {
            char[] cache = new char[4];
            int buffCnt = read4(cache);
            // When no new character, break and return i
            if (buffCnt == 0) {
                break;
            }
            int j = 0;
            while (i < n && j < buffCnt) {
                buf[i] = cache[j];
                i++; j++;
            }
            // Set all remaining unused characters in cache into a global queue
            while (j < buffCnt) {
                queue.offer(cache[j]);
                j++;
            }
        }
        return i;
    }

}