public class Solution {
    // Leetcode 28 Implement strStr() https://leetcode.com/problems/implement-strstr/
    /**
    Implement strStr().
    Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

    Example 1:

    Input: haystack = "hello", needle = "ll"
    Output: 2
    Example 2:

    Input: haystack = "aaaaa", needle = "bba"
    Output: -1
    */
    // Running Time Complexity: O((n-m) * m)
    // Space Complexity: O(1)
    public int strStr(String haystack, String needle) {
        if (haystack == null || needle == null) {
            return 0;
        }
        int n = haystack.length(), m = needle.length();
        for (int i = 0; i <= n-m; i++) {
            int j = 0;
            for (; j < m; j++) {
                if (haystack.charAt(i+j) != needle.charAt(j)) {
                    break;
                }
            }
            if (j == m) {
                return i;
            }
        }
        return -1;
    }

    // Leetcode 415: Add Strings:
    /**
    Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.
    "123" + "123" = "246"
    */
    // Running Time Complexity: O((m + n) * Math.max(m,n)), Space Complexity: O(1)
    public String addStrings(String num1, String num2) {
        StringBuffer sb = new StringBuffer();
        int m = num1.length(), n = num2.length(), i = m-1, j = n-1;
        int sum = 0;
        while (i >= 0 || j >= 0) {
            if (i >= 0) {
                char c1 = num1.charAt(i);
                sum += c1 - '0';
                i--;
            }
            if (j >= 0) {
                char c2 = num2.charAt(j);
                sum += c2 - '0';
                j--;
            }
            sb.insert(0, sum % 10);
            sum /= 10;
        }
        if (sum == 1) {
            sb.insert(0, 1);
        }
        return sb.toString();
    }

    // Running Time Complexity: O(m + n), Space Complexity: O(m+n)
    public String addStrings(String num1, String num2) {
        int m = num1.length(), n = num2.length(), i = m-1, j = n-1;
        char[] res = new char[m+n+1];
        int sum = 0;
        int count = 0;
        while (i >= 0 || j >= 0) {
            if (i >= 0) {
                char c = num1.charAt(i);
                sum += c - '0';
                i--;
            }
            if (j >= 0) {
                char c = num2.charAt(j);
                sum += c - '0';
                j--;
            }
            res[res.length - count - 1] = (char)(sum % 10 + '0');
            sum /= 10;
            count++;
        }
        if (sum == 1) {
            res[res.length - count - 1] = '1';
            count++;
        }
        return new String(res).substring(res.length - count);
    }

    // Leetcode 67: Add Binary:
    /**
    Given two binary strings, return their sum (also a binary string).

    The input strings are both non-empty and contains only characters 1 or 0.

    Example 1:

    Input: a = "11", b = "1"
    Output: "100"
    Example 2:

    Input: a = "1010", b = "1011"
    Output: "10101"
    */
    // Running Time Complexity: O((m + n) * Math.max(m,n)), Space Complexity: O(1)
    public String addBinary(String a, String b) {
        StringBuffer sb = new StringBuffer();
        int m = a.length(), n = b.length(), i = m-1, j = n-1;
        int sum = 0;
        while (i >= 0 || j >= 0) {
            if (i >= 0) {
                char c = a.charAt(i);
                sum += c - '0';
                i--;
            }
            if (j >= 0) {
                char c = b.charAt(j);
                sum += c - '0';
                j--;
            }
            sb.insert(0, sum % 2);
            sum /= 2;
        }
        if (sum == 1) {
            sb.insert(0, 1);
        }
        return sb.toString();
    }

    // Running Time Complexity: O(m + n), Space Complexity: O(m+n)
    public String addBinary(String a, String b) {        
        int m = a.length(), n = b.length(), i = m-1, j = n-1;
        char[] res = new char[m+n+1];
        int sum = 0;
        int count = 0;
        while (i >= 0 || j >= 0) {
            if (i >= 0) {
                char c = a.charAt(i);
                sum += c - '0';
                i--;
            }
            if (j >= 0) {
                char c = b.charAt(j);
                sum += c - '0';
                j--;
            }
            res[res.length - count - 1] = (char)(sum % 2 + '0');
            sum /= 2;
            count++;
        }
        if (sum == 1) {
            res[res.length - count - 1] = '1';
            count++;
        }
        return new String(res).substring(res.length - count);
    }
}