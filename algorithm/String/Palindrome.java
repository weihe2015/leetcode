public class Solution {
    // Leetcode 5: Longest Palindromic Substring
    // https://leetcode.com/problems/longest-palindromic-substring
    /**
    Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

    Example 1:

    Input: "babad"
    Output: "bab"
    Note: "aba" is also a valid answer.
    Example 2:

    Input: "cbbd"
    Output: "bb"
    */
    // Naive Solution: O(N^3)
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int max = Integer.MIN_VALUE, start = -1, end = -1;
        for (int i = 0, n = s.length(); i < n; i++) {
            for (int j = i; j < n; j++) {
                if (isPalindrome(s, i, j)) {
                    if (max < j-i+1) {
                        max = j-i+1;
                        start = i;
                        end = j+1;
                    }
                }
            }
        }
        if (start == -1) {
            return "";
        }
        return s.substring(start, end);
    }

    private boolean isPalindrome(String s, int l, int r) {
        if (l > r) {
            return false;
        }
        while (l < r) {
            if (s.charAt(l) != s.charAt(r)) {
                return false;
            }
            l++; r--;
        }
        return true;
    }

    // Dynamic Programming:
    // Running Time: O(n^2), Space Complexity: O(n^2)
    /**
    Basic idea: when substring length is less than 3, only compare s.charAt(l) == s.charAt(r)
        when substring length is >= 3, use s.charAt(l) == s.charAt(r) && dp[l+1][r-1]
         bdb is palindrome
         l r
        a   b   d   b  a is palindorm
           l+1      r-1 
    */
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int n = s.length();
        boolean[][] dp = new boolean[n][n];
        int start = -1, end = -1, max = Integer.MIN_VALUE;
        for (int r = 0; r < n; r++) {
            for (int l = r; l >= 0; l--) {
                // If substring is at least 3 characters long, use previous step to calculate current step:
                if (r - l >= 3) {
                    dp[l][r] = s.charAt(l) == s.charAt(r) && dp[l+1][r-1];
                }
                else {
                    dp[l][r] = s.charAt(l) == s.charAt(r);
                }
                // If s.substring(i,j+1) is palindrome:
                int len = r - l + 1;
                if (dp[l][r] && max < len) {
                    max = len;
                    start = l;
                    end = r+1;
                }
            }
        }
        // no substring palindrome is found:
        if (start == -1) {
            return "";
        }
        else {
            return s.substring(start, end);
        }
    }

    /**
    This is a function to expand string from index l to two sides
    until its substring(l, r+1) does not form a palindrome
    @param: String s, original String
    @param: int l, left pointer
    @param: int r, right pointer
    @param: int n, length of String s
    @return: int array which contains start and end pointer of longest palindrome string, which is expanded from index l.  
    */
    private int[] expandPalindrome(String s, int l, int r, int n) {
        int[] res = new int[2];
        if (l > r) {
            return res;
        }
        while (l >= 0 && r < n) {
            if (s.charAt(l) != s.charAt(r)) {
                break;
            }
            else {
                res[0] = l;
                res[1] = r;
            }
            l--; r++;
        }
        return res;
    }

    // Running Time Complexity: O(n^2), Space Compexity: O(1)
    /**
    Basic Idea: For each character in String s, expand it from its index to two sides, 
                until this expanded substring is not palindrome.
                Record the starting point of each expanded substring
                There are two cases: odd length palindrome and even length palindrome.
                   odd length palindrome: l = i, r = i: expandPalindrome(s, i, i, n);
                   even length palindrome: l = i, r = i+1: expandPalindrome(s, i, i+1, n);
    */
    public String longestPalindrome(String s) {
        if (s == null || s.length() <= 1) {
            return s;
        }
        int start = -1, max = Integer.MIN_VALUE;
        for (int i = 0, n = s.length(); i < n-1; i++) {
            // Expand from index i, odd length case:
            int[] res1 = expandPalindrome(s, i, i, n);
            // Expand from index i and i+1, even length case:
            int[] res2 = expandPalindrome(s, i, i+1, n);

            // Compute the substring len to get the larger one, and set the starting pointer.
            int len1 = res1[1] - res1[0] + 1;
            int len2 = res2[1] - res2[0] + 1;
            int maxLen = Math.max(len1, len2);
            if (max < maxLen) {
                max = maxLen;
                if (len1 > len2) {
                    start = res1[0];
                }
                else {
                    start = res2[0];
                }
            }
        }
        // No substring palindrome is found:
        if (start == -1) {
            return "";
        }
        else {
            return s.substring(start, start+max);
        }
    }

    // Leetcode 131: Palindrome Partitioning
    // https://leetcode.com/problems/palindrome-partitioning
    /*
    Given a string s, partition s such that every substring of the partition is a palindrome.

    Return all possible palindrome partitioning of s.

    For example, given s = "aab",
    Return

    [["aa","b"],["a","a","b"]]
    */
    public List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return result;
        }
        partition(result, s, 0, new ArrayList<String>());
        return result; 
    }
    
    private void partition(List<List<String>> result, String s, int index, List<String> list) {
        if (index == s.length() && list.size() > 0) {
            result.add(new ArrayList<String>(list));
            return;
        }
        for (int i = index, max = s.length(); i < max; i++) {
            String str = s.substring(index, i+1);
            if (!isValidPalindrome(str)) {
                continue;
            }
            list.add(str);
            partition(result, s, i+1, list);
            list.remove(list.size()-1);
        }
    }
    
    private boolean isValidPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }
        int i = 0, j = s.length()-1;
        while (i < j) {
            char c1 = s.charAt(i);
            char c2 = s.charAt(j);
            if (c1 != c2) {
                return false;
            }
            else {
                i++; j--;
            }
        }
        return true;
    }

    // Leetcode 132: Palindrome Partitioning II
    // https://leetcode.com/problems/palindrome-partitioning-ii
    /**
    Given a string s, partition s such that every substring of the partition is a palindrome.

    Return the minimum cuts needed for a palindrome partitioning of s.

    Example:

    Input: "aab"
    Output: 1
    Explanation: The palindrome partitioning ["aa","b"] could be produced using 1 cut.
    */
    /**
    if [j,i] is palindrome:
        dp[i] is the min cut of dp[j-1] + 1, j <= i 
    [j, i] is palindrome is [j+1, i-1] is palindrome and cList[j] == cList[i]
    */
    // Running Time Complexity: O(N^2), Space Complexity: O(N^2)
    public int minCut(String s) {
        int n = s.length();
        char[] cList = s.toCharArray();
        int[] dp = new int[n];
        boolean[][] pal = new boolean[n][n];
        for (int r = 0; r < n; r++) {
            int min = r;
            for (int l = 0; l <= r; l++) {
                if (cList[l] == cList[r] && (l + 1 > r - 1 || pal[l+1][r-1])) {
                    pal[l][r] = true;
                    if (l == 0) {
                        min = 0;
                    }
                    else {
                        min = Math.min(min, dp[l-1]+1);
                    }
                }
            }
            dp[r] = min;
        }
        return dp[n-1];
    }

    // Best Solution:
    // Running Time Complexity: O(N ^ 2), Space Complexity: O(N)
    public int minCut(String s) {
        int n = s.length();
        char[] cList = s.toCharArray();
        int[] dp = new int[n];
        // initial array:
        for (int i = 0; i < n; i++) {
            dp[i] = i;
        }
        
        for (int i = 0; i < n; i++) {
            expandString(cList, i, i, n, dp);
            expandString(cList, i, i+1, n, dp);
        }
        return dp[n-1];
    }
    
    private void expandString(char[] cList, int l, int r, int n, int[] dp) {
        while (l >= 0 && r < n) {
            if (cList[l] != cList[r]) {
                break;
            }
            if (l == 0) {
                dp[r] = 0;
            }
            else {
                dp[r] = Math.min(dp[r], dp[l-1]+1);
            }
            l--; r++;
        }
    }

    // Bruth Force DP solution:
    // Running Time Complexity: O(N^3)
    /**
    dp[i][j] represents the min cut of substring(i,j) so that substring(i, j) is palindrome
    1. For each string with 1 character, min cut = 0
    2. For each string with 2 characters,
            if two characters are the same, min cut = 1
            else, min cut = 1
    3.  For each string with 3 or more characters:
        if substring(i,j) is palindrom,
            dp[i][j] = 0
        else:
            dp[i][j] = 1 + min {
                        dp[i][k] + T[k+1][j], where k = i ... j-1
                    }
    */
    public int minCut(String s) {
        int n = s.length();
        if (isPalindrome(s, 0, n-1)) {
            return 0;
        }
        // dp[i][j] represents the min cut of substring(i,j) so that substring(i, j) is palindrome 
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dp[i], n+1);
        }
        
        // For each string with 1 character, min cut = 0
        for (int i = 0; i < n; i++) {
            dp[i][i] = 0;
        }
        
        // For each string with 2 characters:
        for (int i = 0; i < n-1; i++) {
            if (s.charAt(i) == s.charAt(i+1)) {
                dp[i][i+1] = 0;
            }
            else {
                dp[i][i+1] = 1;
            }
        }
        
        // For each string with 3 or more characters:
        for (int l = 3; l <= n; l++) {
            for (int i = 0; i <= n-l; i++) {
                int j = i+l-1;
                if (isPalindrome(s, i, j)) {
                    dp[i][j] = 0;
                }
                else {
                    for (int k = i; k < j; k++) {
                        int curr = 1 + dp[i][k] + dp[k+1][j];
                        dp[i][j] = Math.min(dp[i][j], curr);
                    }
                }
            }
        }
        return dp[0][n-1];
    }
}