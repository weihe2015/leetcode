public class Solution {
    
    // Leetcode 344: Reverse String
    // https://leetcode.com/problems/reverse-string/
    /**
    Write a function that takes a string as input and returns the string reversed.
    Example 1:

    Input: "hello"
    Output: "olleh"
    Example 2:

    Input: "A man, a plan, a canal: Panama"
    Output: "amanaP :lanac a ,nalp a ,nam A"
     */
    public String reverseString(String s) {
        if (s == null) {
            return "";
        }
        char[] cList = s.toCharArray();
        int l = 0, r = s.length() - 1;
        while (l < r) {
            swap(cList, l, r);
            l++; r--;
        }
        return new String(cList);
    }
    
    private void swap(char[] cList, int l, int r) {
        char tmp = cList[l];
        cList[l] = cList[r];
        cList[r] = tmp;
    }
    
    // Leetcode 541: Reverse String II
    // https://leetcode.com/problems/reverse-string-ii/
    /**
    Given a string and an integer k, you need to reverse the first k characters for every 2k characters counting from the start of the string. If there are less than k characters left, reverse all of them. If there are less than 2k but greater than or equal to k characters, then reverse the first k characters and left the other as original.
    Example:
    Input: s = "abcdefg", k = 2
    Output: "bacdfeg"
    Restrictions:
    The string consists of lower English letters only.
    Length of the given string and k will in the range [1, 10000]
    */
    public String reverseStr(String s, int k) {
        if (s == null) {
            return "";
        }
        char[] cList = s.toCharArray();
        int i = 0, n = s.length();
        while (i < n) {
            int j = Math.min(i+k-1,n-1);
            reverse(cList, i, j);
            i += 2 * k;
        }
        return new String(cList);
    }

    // Leetcode 557: Reverse Words in a String III
    // https://leetcode.com/problems/reverse-words-in-a-string-iii
    /**
    Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

    Example 1:
    Input: "Let's take LeetCode contest"
    Output: "s'teL ekat edoCteeL tsetnoc"
    Note: In the string, each word is separated by single space and there will not be any extra space in the string.
    */
    public String reverseWords(String s) {
        String[] strs = s.split(" ");
        StringBuffer sb = new StringBuffer();
        for (String str : strs) {
            sb.append(reverseString(str));
            sb.append(" ");
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }
    
    private String reverseString(String str) {
        StringBuffer sb = new StringBuffer();
        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }

    // Solution 2: Running Time Compleixty: O(2N)
    public String reverseWords(String s) {
        if (s == null) {
            return "";
        }
        int l = 0, r = 0, n = s.length();
        char[] cList = s.toCharArray();
        while (r < n) {
            while (r < n && cList[r] != ' ') {
                r++;
            }
            reverse(cList, l, r-1);
            r++;
            l = r;
        }
        return new String(cList);
    }
    private void reverse(char[] cList, int l, int r) {
        while (l < r) {
            char tmp = cList[l];
            cList[l] = cList[r];
            cList[r] = tmp;
            l++; r--;
        }
    }
}