public class Solution {
    // Sliding window:
    // Use two pointers and a hashmap to solve problems.
    // 滑动窗口，就是利用双指针技巧，以及map数据结构，维护一个不断扩展、伸缩的窗口，在窗口内探测记录我们感兴趣的结果
    // 可以说滑动窗口这种思想，关键点在于：
    // 1、map中存储值的意义
    // 2、窗口什么时候扩展和收缩，对应于left和right值什么时候发生变化。
    // 在解题的时候，首先尝试扩展窗口right，看看什么时候包含了一个结果，记录结果。
    // 然后缩小左边界left，直到窗口不在包含一个可能解！接着就可以继续扩展窗口了，以此类推。

    /**
     * 总结：
     *    设立两个指针l和r, l = 0, r = 0.
     *      情况1： 如果是minimum leng/size的，if (窗口数组是不符合条件) { 先扩展右指针r },
     *              while (符合条件的窗口) { 移动左指针直到窗口不满足条件，计算最小的minLen }
     *      情况2:  如果是maximum length/size的， if (窗口满足条件) { 扩展右指针 r }
     *              while (不符合条件的窗口) { 移动左指针直到窗口满足条件 }
     *              最后的窗口就是符合条件的最大窗口：计算最大的maxLen
     *
    */

    // Leetcode 3: Longest Substring Without Repeating Characters
    // https://leetcode.com/problems/longest-substring-without-repeating-characters/
    /**
    *  Given a string, find the length of the longest substring without repeating characters.
        Examples:
        Given "abcabcbb", the answer is "abc", which the length is 3.
        Given "bbbbb", the answer is "b", with the length of 1.
        Given "pwwkew", the answer is "wke", with the length of 3.
        Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
    */
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int maxLen = 0;
        int l = 0, r = 0, n = s.length();
        Set<Character> set = new HashSet<Character>();
        // 设下标 l 和 r, 把左开右闭 [l, r) 想象成一个窗口。
        // 当 s[r] 和窗口内字符重复时， 则 l 向右滑动，缩小窗口。
        // 当s[r] 和窗口内字符不重复时，则 r 向右滑动，扩大窗口，此时窗口内的字符串一个无重复子字符串。
        while (r < n) {
            char lc = s.charAt(l);
            char rc = s.charAt(r);
            if (!set.contains(rc)) {
                set.add(rc);
                r++;
            }
            else {
                set.remove(lc);
                l++;
            }
            maxLen = Math.max(maxLen, set.size());
        }
        return maxLen;
    }

    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int maxLen = 0, l = 0, r = 0, n = s.length();
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        while (r < n) {
            char c = s.charAt(r);
            if (!map.containsKey(c)) {
                maxLen = Math.max(maxLen, r-l+1);
            }
            else {
                // If the duplicated element is in current window, move left pointer to its right.
                int prevIdx = map.get(c);
                if (prevIdx >= l && prevIdx <= r) {
                    l = prevIdx + 1;
                }
                // If the duplicated element is not in current window, include it into the window:
                else {
                    maxLen = Math.max(maxLen, r-l+1);
                }
            }
            map.put(c, r);
            r++;
        }
        return maxLen;
    }

    // Unified Code Style of Sliding Window Problem, by using count
    // Running Time Complexity: O(n);
    // Space Complexity: O(1)
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int[] charMap = new int[256];
        int maxLen = 0, l = 0, r = 0, n = s.length();
        boolean isContainUniqueChars = true;
        while (r < n) {
            // When this window statisfies the requirement, we keep expanding the window:
            if (isContainUniqueChars) {
                char rc = s.charAt(r);
                charMap[rc]++;
                // When there is duplicate character, meaning map[rc] == 1, we increment the count by 1
                if (charMap[rc] == 2) {
                    isContainUniqueChars = false;
                }
                r++;
            }
            // when substring(l,r) contains duplicate character, shrink the window to find the match:
            while (!isContainUniqueChars) {
                char lc = s.charAt(l);
                charMap[lc]--;
                if (charMap[lc] == 1) {
                    isContainUniqueChars = true;
                }
                l++;
            }
            // when substring(l,r) contains only unique character, calculate the length:
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // Minimum Window Substring
    // Leetcode 076: https://leetcode.com/problems/minimum-window-substring/
    /**
    Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).
    Example:
    Input: S = "ADOBECODEBANC", T = "ABC"
    Output: "BANC"
    Note:
    If there is no such window in S that covers all characters in T, return the empty string "".
    If there is such window, you are guaranteed that there will always be only one unique minimum window in S.
    */
    public String minWindow(String s, String t) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int[] freqMap = new int[256];
        for (char c : t.toCharArray()) {
            freqMap[c]++;
        }
        int count = t.length(), l = 0, r = 0, n = s.length();
        int start = -1, end = -1;
        while (r < n) {
            // substring(l,r) does not contains all characters in string t:
            if (count > 0) {
                char rc = s.charAt(r);
                // If this character is in string t and have not been included before, reduce the counter.
                if (freqMap[rc] >= 1) {
                    count--;
                }
                freqMap[rc]--;
                r++;
            }
            // substring(l, r) contains all characters in string t:
            while (count == 0) {
                // if it finds a shorter substring containing all characters in string t, update the start and end pointer
                if (start == -1 || (r - l) < (end - start)) {
                    start = l;
                    end = r;
                }
                char lc = s.charAt(l);
                // left pointer meets characters in string t, increment counter.
                if (freqMap[lc] >= 0) {
                    count++;
                }
                freqMap[lc]++;
                l++;
            }
        }
        // If no such substring found, return "":
        return start == -1 ? "" : s.substring(start, end);
    }

    // Leetcode 395 Longest Substring with At Least K Repeating Characters
    // https://leetcode.com/problems/longest-substring-with-at-least-k-repeating-characters/
    /**
     * Find the length of the longest substring T of a given string (consists of lowercase letters only) such that every character in T appears no less than k times.
    Example 1:
    Input:
    s = "aaabb", k = 3

    Output:
    3
    The longest substring is "aaa", as 'a' is repeated 3 times.
    Example 2:

    Input:
    s = "ababbc", k = 2

    Output:
    5
    The longest substring is "ababb", as 'a' is repeated 2 times and 'b' is repeated 3 times.
     *
    */
   /**
    * Idea: Condition: We only contains lowercase letter.
    * First we count the number of unique characters in string s:
    * For num from 1 to the number of unique characters in string s,
    * we use sliding window to check maxLength of substring str from string s
    * which contains exactly num unique characters, and for each characters, it appears at least k times.
    * For example, when num = 3 and k = 5, we are going to find the longest substring str that contains exact 3 unique characters
    * and each repeating 5 times.
   */
    // Running Time Complexity: O(N + L * N) where L is the number of distinct characters in String s, N is the length of string s.
    // O(26 * N): since string s will only contiains lower case characters.
    // Space Complexity: O(1). We treact O(256) as constant O(1)
    public int longestSubstring(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        // Count unique characters in string s:
        Set<Character> set = new HashSet<Character>();
        for (char c : s.toCharArray()) {
            set.add(c);
        }
        int maxLen = 0, size = set.size();
        // calculate the maxLen of each substring containing 1,2, or size unique character
        for (int num = 1; num <= size; num++) {
            int[] map = new int[256];
            int uniqueCount = 0, l = 0, r = 0, n = s.length(), noLessThanK = 0;
            while (r < n) {
                // The numbet of unique character in substring(l,r) is less than num, we continue to expand r:
                // substring(l,r) satisfy the requirement:
                if (uniqueCount <= num) {
                    char rc = s.charAt(r);
                    map[rc]++;
                    if (map[rc] == 1) {
                        uniqueCount++;
                    }
                    if (map[rc] == k) {
                        noLessThanK++;
                    }
                    r++;
                }
                // while substring(l, r) has too many unique character, we shrink l pointer until uniqueCount == num
                // substring(r,l) is too long, and does not satisfy the requirement
                while (uniqueCount > num) {
                    char lc = s.charAt(l);
                    map[lc]--;
                    if (map[lc] == 0) {
                        uniqueCount--;
                    }
                    if (map[lc] == k-1){
                        noLessThanK--;
                    }
                    l++;
                }
                // At this point, the unique characters in substring(l, r) is num:
                // If num Of Characters no less than K is the same as num, we record the length
                if (uniqueCount == noLessThanK) {
                    maxLen = Math.max(maxLen, r-l);
                }
            }
        }
        return maxLen;
    }

    // Leetcode 159: Longest Substring with At Most Two Distinct Characters
    // https://leetcode.com/problems/longest-substring-with-at-most-two-distinct-characters/
    // https://www.lintcode.com/problem/longest-substring-with-at-most-k-distinct-characters
    /**
    Given a string s , find the length of the longest substring t that contains at most 2 distinct characters.

    Example 1:
    Input: "eceba"
    Output: 3
    Explanation: t is "ece" which its length is 3.

    Example 2:
    Input: "ccaabbb"
    Output: 5
    Explanation: t is "aabbb" which its length is 5.
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        if (s == 0 || s.length() == 0) {
            return 0;
        }
        int k = 2;
        int[] map = new int[256];
        // distinctCharCnt: number of distinct characters
        int l = 0, r = 0, n = s.length(), maxLen = 0, distinctCharCnt = 0;
        while (r < n) {
            // while the window [l,r] satisfy the requirement: we continue expanding the r pointer:
            if (distinctCharCnt <= k) {
                char rc = s.charAt(r);
                map[rc]++;
                // We meet a new character, increment the count
                if (map[rc] == 1) {
                    distinctCharCnt++;
                }
                r++;
            }
            // while the window [l,r] does not satisfy the requirement,
            // we shrink the window by moving l pointer to the right:
            while (distinctCharCnt > k) {
                char lc = s.charAt(l);
                map[lc]--;
                // At this time, after removing char lc, there is no more char lc in substring(l, r)
                // So we decrement count by 1
                if (map[lc] == 0) {
                    distinctCharCnt--;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // Leetcode 340: Longest Substring with At Most K Distinct Characters
    // https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters
    // https://www.lintcode.com/problem/longest-substring-with-at-most-k-distinct-characters
    /**
    Given a string, find the length of the longest substring T that contains at most k distinct characters.
    Example 1:

    Input: s = "eceba", k = 2
    Output: 3
    Explanation: T is "ece" which its length is 3.
    Example 2:

    Input: s = "aa", k = 1
    Output: 2
    Explanation: T is "aa" which its length is 2.
    */
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // write your code here
        if (s == null || s.length() == 0) {
            return 0;
        }
        int l = 0;
        int r = 0;
        int n = s.length();
        int maxLen = 0;
        int distinctCharCnt = 0;
        int[] charMap = new int[256];

        while (r < n) {
            if (distinctCharCnt <= k) {
                char rc = s.charAt(r);
                charMap[rc]++;
                if (charMap[rc] == 1) {
                    distinctCharCnt++;
                }
                r++;
            }
            while (distinctCharCnt > k) {
                char lc = s.charAt(l);
                charMap[lc]--;
                if (charMap[lc] == 0) {
                    distinctCharCnt--;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    /**
    Leetcode 209: Minimum Size Subarray Sum
    https://leetcode.com/problems/minimum-size-subarray-sum

    Given an array of n positive integers and a positive integer s, find the minimal length of a subarray of which the sum ≥ s. 
    If there isnt one, return 0 instead.

    For example, given the array [2,3,1,2,4,3] and s = 7,
    the subarray [4,3] has the minimal length under the problem constraint.
    */
    public int minSubArrayLen(int s, int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int l = 0; 
        int r = 0;
        int n = nums.length;
        int minLen = Integer.MAX_VALUE;
        int sum = 0;
        // 右边界向右移动：
        while (r < n) {
            // 如果还没满足条件，移动右边界来扩大窗口：
            if (sum < s) {
                sum += nums[r];
                r++;
            }
            // 移动左指针，缩小左边界 l, 直到窗口不满足条件为止
            while (sum >= s) {
                minLen = Math.min(minLen, r-l);
                sum -= nums[l];
                l++;
            }
        }
        return minLen == Integer.MAX_VALUE ? 0 : minLen;
    }

    // Leetcode 424 Longest Repeating Character Replacement
    // https://leetcode.com/problems/longest-repeating-character-replacement/
    /**
    Given a string that consists of only uppercase English letters, you can replace any letter in the string with another letter at most k times. Find the length of a longest substring containing all repeating letters you can get after performing the above operations.

    Note:
    Both the string's length and k will not exceed 104.

    Example 1:

    Input:
    s = "ABAB", k = 2

    Output:
    4

    Explanation:
    Replace the two 'A's with two 'B's or vice versa.
    Example 2:

    Input:
    s = "AABABBA", k = 1

    Output:
    4

    Explanation:
    Replace the one 'A' in the middle with 'B' and form "AABBBBA".
    The substring "BBBB" has the longest repeating letters, which is 4.
    */
    // Running Time Complexity: O(n*26) = O(n), Space Complexity: O(1)
    public int characterReplacement(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        // Count of one character in current sliding window.
        int maxCharCount = 0;
        int l = 0, r = 0, n = s.length(), maxLen = 0;
        int[] charMap = new int[26];
        boolean canBeChanged = true;
        while (r < n) {
            if (canBeChanged) {
                char rc = s.charAt(r);
                charMap[rc-'A']++;
                r++;
                maxCharCount = Math.max(maxCharCount, charMap[rc-'A']);
                // Window size - countOfSameChar = countOfChar can be perform operations.
                if (r-l+1-maxCharCount > k) {
                    canBeChanged = false;
                }
                r++;
            }
            while (!canBeChanged) {
                char lc = s.charAt(l);
                charMap[lc-'A']--;
                l++;
                maxCharCount = Arrays.stream(charMap).max().getAsInt();
                if (r-l-maxCharCount <= k) {
                    canBeChanged = true;
                }
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // Leetcode 487: Max Consecutive ones II:
    // https://leetcode.com/problems/max-consecutive-ones-ii
    // https://www.lintcode.com/problem/max-consecutive-ones-ii/
    /**
    Given a binary array, find the maximum number of consecutive 1s in this array if you can flip at most one 0.

    Example 1:
    Input: [1,0,1,1,0]
    Output: 4
    Explanation: Flip the first zero will get the the maximum number of consecutive 1s.
        After flipping, the maximum number of consecutive 1s is 4.
    Note:

    The input array will only contain 0 and 1.
    The length of input array is a positive integer and will not exceed 10,000
    Follow up:
    What if the input numbers come in one by one as an infinite stream?
    In other words, you can't store all numbers coming from the stream as
    it's too large to hold in memory. Could you solve it efficiently?
    */
    // Sliding Window, find the longest subarray with at most one zero.
    // Running Time Complexity: O(1), Space Complexity: O(1)
    public int findMaxConsecutiveOnes(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int maxLen = 0;
        int l = 0;
        int r = 0;
        int n = nums.length;
        // counter of 0 and 1
        int[] count = {0,0};
        boolean containsLessThanTwoZeros = true;
        while (r < n) {
            if (containsLessThanTwoZeros) {
                int num = nums[r];
                count[num]++;
                if (count[0] == 2) {
                    containsLessThanTwoZeros = false;
                }
                r++;
            }
            while (!containsLessThanTwoZeros) {
                int num = nums[l];
                count[num]--;
                if (count[0] == 1) {
                    containsLessThanTwoZeros = true;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // Leetcode 1004: Max Consecutive Ones III
    // https://leetcode.com/problems/max-consecutive-ones-iii/
    /**
    Given an array A of 0s and 1s, we may change up to K values from 0 to 1.
    Return the length of the longest (contiguous) subarray that contains only 1s.
    Example 1:

    Input: A = [1,1,1,0,0,0,1,1,1,1,0], K = 2
    Output: 6
    Explanation:
    [1,1,1,0,0,1,1,1,1,1,1]
    Bolded numbers were flipped from 0 to 1.  The longest subarray is [1,1,1,1,1,1].
    Example 2:

    Input: A = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], K = 3
    Output: 10
    Explanation:
    [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
    Bolded numbers were flipped from 0 to 1.  The longest subarray is [1,1,1,1,1,1,1,1,1,1].
    */
    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int longestOnes(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length, l = 0, r = 0, maxLen = 0;
        boolean canFlipZeroToOne = true;
        int[] count = {0,0};
        while (r < n) {
            if (canFlipZeroToOne) {
                int num = nums[r];
                count[num]++;
                if (count[0] == k+1) {
                    canFlipZeroToOne = false;
                }
                r++;
            }
            while (!canFlipZeroToOne) {
                int num = nums[l];
                count[num]--;
                if (count[0] == k) {
                    canFlipZeroToOne = true;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    // Leetcode 438: Find All Anagrams in a String
    // https://leetcode.com/problems/find-all-anagrams-in-a-string/
    /**
    Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
    Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.
    The order of output does not matter.
    Example 1:
    Input:
    s: "cbaebabacd" p: "abc"
    Output:
    [0, 6]
    Explanation:
    The substring with start index = 0 is "cba", which is an anagram of "abc".
    The substring with start index = 6 is "bac", which is an anagram of "abc".
    Example 2:
    Input:
    s: "abab" p: "ab"
    Output:
    [0, 1, 2]
    Explanation:
    The substring with start index = 0 is "ab", which is an anagram of "ab".
    The substring with start index = 1 is "ba", which is an anagram of "ab".
    The substring with start index = 2 is "ab", which is an anagram of "ab".
    */
    // Use Sliding Window Solution:
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> result = new ArrayList<>();
        int[] map = new int[256];
        for (char c : p.toCharArray()) {
            map[c]++;
        }
        int count = p.length(), l = 0, r = 0, n = s.length();
        while (r < n) {
            // substring(l, r) does not contains all characters in string p, move r pointer to right
            if (count > 0) {
                char rc = s.charAt(r);
                map[rc]--;
                if (map[rc] >= 0) {
                    count--;
                }
                r++;
            }
            // substring(l, r) contains all characters in string t:
            while (count == 0) {
                char lc = s.charAt(l);
                // If lc is in string p, then we increment count.
                map[lc]++;
                if (map[lc] >= 1) {
                    count++;
                    // If the window size is same as p, an anagram is found
                    if (r - l == p.length()) {
                        result.add(l);
                    }
                }
                l++;
            }
        }
        return result;
    }

    // Leetcode 567: Permutation in String
    // https://leetcode.com/problems/permutation-in-string/
    /**
    Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1.
    In other words, one of the first string's permutations is the substring of the second string.
    Example 1:
    Input:s1 = "ab" s2 = "eidbaooo"
    Output:True
    Explanation: s2 contains one permutation of s1 ("ba").
    Example 2:
    Input:s1= "ab" s2 = "eidboaoo"
    Output: False
    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public boolean checkInclusion(String s1, String s2) {
        if (s2 == null || s2.length() == 0) {
            return false;
        }
        int[] map = new int[256];
        for (char c : s1.toCharArray()) {
            map[c]++;
        }
        int l = 0, r = 0, n = s2.length(), count = s1.length();
        while (r < n) {
            if (count > 0) {
                char rc = s2.charAt(r);
                map[rc]--;
                if (map[rc] >= 0) {
                    count--;
                }
                r++;
            }
            while (count == 0) {
                char lc = s2.charAt(l);
                map[lc]++;
                if (map[lc] >= 1) {
                    count++;
                    if (r - l == s1.length()) {
                        return true;
                    }
                }
                l++;
            }
        }
        return false;
    }

    // Leetcode 209: Minimum Size Subarray Sum
    // https://leetcode.com/problems/minimum-size-subarray-sum
    /**
        Given an array of n positive integers and a positive integer s,
        find the minimal length of a contiguous subarray of which the sum ≥ s. If there isn't one, return 0 instead.
        Example:
        Input: s = 7, nums = [2,3,1,2,4,3]
        Output: 2
        Explanation: the subarray [4,3] has the minimal length under the problem constraint.
    */
    public int minSubArrayLen(int s, int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int l = 0, r = 0, n = nums.length, minLen = Integer.MAX_VALUE, sum = 0;
        // 右边界向右移动：
        while (r < n) {
            // 如果还没满足条件，移动右边界来扩大窗口：
            if (sum < s) {
                sum += nums[r];
                r++;
            }
            // 移动左指针，缩小左边界 l, 直到窗口不满足条件为止
            while (sum >= s) {
                sum -= nums[l];
                minLen = Math.min(minLen, r-l);
                l++;
            }
        }
        return minLen == Integer.MAX_VALUE ? 0 : minLen;
    }

    // Leetcode 30: Substring with Concatenation of All Words
    // https://leetcode.com/problems/substring-with-concatenation-of-all-words/
    /**
    You are given a string, s, and a list of words, words, that are all of the same length.
    Find all starting indices of substring(s) in s that is a concatenation of each word in words exactly once and without any intervening characters.

    Example 1:

    Input:
    s = "barfoothefoobarman",
    words = ["foo","bar"]
    Output: [0,9]
    Explanation: Substrings starting at index 0 and 9 are "barfoor" and "foobar" respectively.
    The output order does not matter, returning [9,0] is fine too.
    Example 2:

    Input:
    s = "wordgoodgoodgoodbestword",
    words = ["word","good","best","word"]
    Output: []
    */
    public List<Integer> findSubstring(String s, String[] words) {
        List<Integer> result = new ArrayList<Integer>();
        if (s == null || words == null || words.length == 0) {
            return result;
        }
        int wsLen = words.length, wLen = words[0].length(), sLen = s.length();
        // Condition: list of words, words, that are all of the same length
        // If string s length() is less than the total length of words,
        // then it does not contains substring containing all word in words.
        if (sLen < wLen * wsLen) {
            return result;
        }
        // Use HashMap to store occurance of each word
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            map.put(word, map.getOrDefault(word, 0) + 1);
        }
        // iterate all character in words[0]
        for (int i = 0; i < wLen; i++) {
            int l = i, r = i, count = 0;
            Map<String, Integer> currMap = new HashMap<>();
            while (r <= sLen - wLen) {
                // substring starts from r, with len wLen:
                String str = s.substring(r, r+wLen);
                if (map.containsKey(str)) {
                    // Save substr into currMap:
                    currMap.put(str, currMap.getOrDefault(str, 0) + 1);
                    // handle multiple same words in substring subStr
                    // handle multiple same strings like example 2:
                    while (currMap.get(str) > map.get(str)) {
                        String tmp = s.substring(l, l+wLen);
                        currMap.put(tmp, currMap.get(tmp) - 1);
                        count--;
                        l += wLen;
                    }
                    count++;
                    if (count == wsLen) {
                        result.add(l);
                    }
                }
                else {
                    currMap.clear();
                    count = 0;
                    l = r + wLen;
                }
                r += wLen;
            }
        }
        return result;
    }

    // Leetcode 53: Maximum Subarray:
    // https://leetcode.com/problems/maximum-subarray/
    /**
    Given an integer array nums, find the contiguous subarray 
    (containing at least one number) which has the largest sum and return its sum.
    Example:

    Input: [-2,1,-3,4,-1,2,1,-5,4],
    Output: 6
    Explanation: [4,-1,2,1] has the largest sum = 6.
    */
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int r = 0;
        int n = nums.length;
        int max = Integer.MIN_VALUE;
        int sum = 0;
        while (r < n) {
            sum += nums[r];
            if (sum > max) {
            	max = sum;
            }
            // the numbers within window l and r cannot be the largest number:
            if (sum <= 0) {
                sum = 0;
            }
            r++;
        }
        return max;
    }

    // Leetcode 713: Subarray Product Less Than K
    // https://leetcode.com/problems/subarray-product-less-than-k/
    /**
    Your are given an array of positive integers nums.

    Count and print the number of (contiguous) subarrays where the product of all the elements in the subarray is less than k.

    Example 1:
    Input: nums = [10, 5, 2, 6], k = 100
    Output: 8
    Explanation: The 8 subarrays that have product less than 100 are: [10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2, 6].
    Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.
    Note:

    0 < nums.length <= 50000.
    0 < nums[i] < 1000.
    0 <= k < 10^6.
    */
    public int numSubarrayProductLessThanK(int[] nums, int k) {
        int i = 0, j = 0, count = 0, n = nums.length;
        long product = 1;
        while (j < n) {
            product *= nums[j];
            while (i <= j && product >= k) {
                product /= nums[i];
                i++;
            }
            count += j - i + 1;
            j++;
        }
        return count;
    }

    // Leetcode 1438 Longest Continuous Subarray With Absolute Diff Less Than or Equal to Limit
    // https://leetcode.com/problems/longest-continuous-subarray-with-absolute-diff-less-than-or-equal-to-limit
    /**
    Given an array of integers nums and an integer limit, return the size of the longest non-empty subarray such that the absolute difference between any two elements of this subarray is less than or equal to limit.

    Example 1:

    Input: nums = [8,2,4,7], limit = 4
    Output: 2
    Explanation: All subarrays are:
    [8] with maximum absolute diff |8-8| = 0 <= 4.
    [8,2] with maximum absolute diff |8-2| = 6 > 4.
    [8,2,4] with maximum absolute diff |8-2| = 6 > 4.
    [8,2,4,7] with maximum absolute diff |8-2| = 6 > 4.
    [2] with maximum absolute diff |2-2| = 0 <= 4.
    [2,4] with maximum absolute diff |2-4| = 2 <= 4.
    [2,4,7] with maximum absolute diff |2-7| = 5 > 4.
    [4] with maximum absolute diff |4-4| = 0 <= 4.
    [4,7] with maximum absolute diff |4-7| = 3 <= 4.
    [7] with maximum absolute diff |7-7| = 0 <= 4.
    Therefore, the size of the longest subarray is 2.
    Example 2:

    Input: nums = [10,1,2,4,7,2], limit = 5
    Output: 4
    Explanation: The subarray [2,4,7,2] is the longest since the maximum absolute diff is |2-7| = 5 <= 5.
    Example 3:

    Input: nums = [4,2,2,2,4,4,2,2], limit = 0
    Output: 3


    Constraints:

    1 <= nums.length <= 10^5
    1 <= nums[i] <= 10^9
    0 <= limit <= 10^9
    */
    public int longestSubarray(int[] nums, int limit) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int l = 0, r = 0, n = nums.length, maxLen = 0;
        Queue<Integer> minQueue = new PriorityQueue<>();
        Queue<Integer> maxQueue = new PriorityQueue<>(Collections.reverseOrder());
        boolean lessThanLimit = true;
        while (r < n) {
            if (lessThanLimit) {
                int num = nums[r];
                maxQueue.offer(num);
                minQueue.offer(num);
                int diff = maxQueue.peek() - minQueue.peek();
                if (diff > limit) {
                    lessThanLimit = false;
                }
                r++;
            }
            while (!lessThanLimit) {
                int num = nums[l];
                maxQueue.remove(num);
                minQueue.remove(num);
                int diff = maxQueue.peek() - minQueue.peek();
                if (diff <= limit) {
                    lessThanLimit = true;
                }
                l++;
            }
            maxLen = Math.max(maxLen, r-l);
        }
        return maxLen;
    }

    /**
    Leetcode 992 Subarrays with K Different Integers:
    https://leetcode.com/problems/subarrays-with-k-different-integers/

    Given an array A of positive integers, call a (contiguous, not necessarily distinct) subarray of 
    A good if the number of different integers in that subarray is exactly K.

    (For example, [1,2,3,1,2] has 3 different integers: 1, 2, and 3.)

    Return the number of good subarrays of A.

    
    Example 1:

    Input: A = [1,2,1,2,3], K = 2
    Output: 7
    Explanation: Subarrays formed with exactly 2 different integers: [1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2].
    Example 2:

    Input: A = [1,2,1,3,4], K = 3
    Output: 3
    Explanation: Subarrays formed with exactly 3 different integers: [1,2,1,3], [2,1,3], [1,3,4].
    

    Note:

    1 <= A.length <= 20000
    1 <= A[i] <= A.length
    1 <= K <= A.length

    */
    public int subarraysWithKDistinct(int[] A, int K) {
        return subarraysWithMostKDistinct(A, K) - subarraysWithMostKDistinct(A, K-1);
    }
    
    private int subarraysWithMostKDistinct(int[] A, int K) {
        Map<Integer, Integer> freqMap = new HashMap<>();
        int l = 0;
        int r = 0;
        int n = A.length;
        int cnt = 0;
        int uniqueNum = 0;
        while (r < n) {
            if (uniqueNum <= K) {
                int num = A[r];
                int freq = freqMap.getOrDefault(num, 0);
                if (freq == 0) {
                    uniqueNum++;
                }
                freq++;
                freqMap.put(num, freq);
                r++;
            }
            while (uniqueNum > K) {
                int num = A[l];
                int freq = freqMap.get(num);
                if (freq == 1) {
                    uniqueNum--;
                }
                freq--;
                freqMap.put(num, freq);
                l++;
            }
            cnt += r - l;
        }
        return cnt;
    } 
}