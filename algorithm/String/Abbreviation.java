public class Solution {
    // Leetcode 288: Unique Word Abbreviation:
    // https://leetcode.com/problems/unique-word-abbreviation
    /**
    An abbreviation of a word follows the form <first letter><number><last letter>. Below are some examples of word abbreviations:

    a) it                      --> it    (no abbreviation)

        1
    b) d|o|g                   --> d1g

                1    1  1
        1---5----0----5--8
    c) i|nternationalizatio|n  --> i18n

                1
        1---5----0
    d) l|ocalizatio|n          --> l10n
    Assume you have a dictionary and given a word, find whether its abbreviation is unique in the dictionary. A words abbreviation is unique if no other word from the dictionary has the same abbreviation.

    Example: 
    Given dictionary = [ "deer", "door", "cake", "card" ]

    isUnique("dear") -> false
    isUnique("cart") -> true
    isUnique("cane") -> false
    isUnique("make") -> true

    1. dictionary = {"dear"},  isUnique("door") -> false
    2. dictionary = {"door", "door"}, isUnique("door") -> true
    3. dictionary = {"dear", "door"}, isUnique("door") -> false
    */
    // Key -> abbreviated word, value: list of word that matches this abbreviated word.
    Map<String, Set<String>> map = new HashMap<>();
    // Solution 1: Running Time Complexity: O(N), Space Complexity: O(N * L)
    public ValidWordAbbr(String[] dictionary) {
        for (String word : dictionary) {
            String newWord = abbreviateWord(word);
            Set<String> subset = null;
            if (!map.containsKey(newWord)) {
                subset = new HashSet<String>();
            }
            else {
                subset = map.get(newWord);
            }
            if (!subset.contains(word)) {
                subset.add(word);
            }
            map.put(newWord, subset);
        }
    }

    // Solution 1: Running Time Complexity: O(1) 
    public boolean isUnique(String word) {
        String newWord = abbreviateWord(word);
        if (!map.containsKey(newWord) || (map.get(newWord).contains(word) && map.get(newWord).size() == 1)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private String abbreviateWord(String word) {
        if (word.length() <= 2) {
            return word;
        }
        StringBuffer sb = new StringBuffer();
        char[] cList = word.toCharArray();
        sb.append(cList[0]);
        sb.append(cList.length - 2);
        sb.append(cList[cList.length - 1]);
        return sb.toString();
    }

    // Solution 2:
    // 如果该缩写形式应经存在，如果映射的单词不是当前单词，我们将映射单词改为空字符串
    // Key -> abbreviated word, Value -> Actual value
    Map<String, String> map = new HashMap<>();
    public ValidWordAbbr(String[] dictionary) {
        for (String word : dictionary) {
            String newWord = abbreviateWord(word);
            if (map.containsKey(newWord) && !map.get(newWord).equals(word)) {
                map.put(newWord, "");
            }
            else { 
                map.put(newWord, word);
            }
        }
    }

    public boolean isUnique(String word) {
        String newWord = abbreviateWord(word);
        return !map.containsKey(newWord) || map.get(newWord).equals(word);
    }

    // Leetcode 408: Valid Word Abbreviation
    // https://leetcode.com/problems/valid-word-abbreviation
    /**
    Given a non-empty string s and an abbreviation abbr, return whether the string matches with the given abbreviation.

    A string such as "word" contains only the following valid abbreviations:

    ["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
    Notice that only the above abbreviations are valid abbreviations of the string "word". Any other string is not a valid abbreviation of "word".

    Note:
    Assume s contains only lowercase letters and abbr contains only lowercase letters and digits.

    Example 1:
    Given s = "internationalization", abbr = "i12iz4n":

    Return true.
    Example 2:
    Given s = "apple", abbr = "a2e":

    Return false.
    */
    public boolean validWordAbbreviation(String word, String abbr) {
        int m = word.length(), n = abbr.length();
        int i = 0, j = 0;
        while (i < m && j < n) {
            char c1 = word.charAt(i);
            char c2 = abbr.charAt(j);
            if (Character.isLetter(c2)) {
                if (c1 != c2) {
                    return false;
                }
                else {
                    i++; j++;
                }
            }
            else {
                // Skip leading 0:
                while (j < n && abbr.charAt(j) == '0') {
                    i++; j++;
                }
                int num = 0;
                while (j < n && !Character.isLetter(abbr.charAt(j))) {
                    num = 10 * num + (abbr.charAt(j) - '0');
                    j++;
                }
                if (num > 0) {
                    i += num;
                }
            }
        }
        return i == m && j == n;
    }
    // Leetcode 320: Generalized Abbreviation:
    // https://leetcode.com/problems/generalized-abbreviation
    /**
    Write a function to generate the generalized abbreviations of a word.

    Example:
    Given word = "word", return the following list (order does not matter):
    ["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
    */
    // Running Time Complexity: O(2^N), Space Complexity: O(N)
    public List<String> generateAbbreviations(String word) {
        List<String> result = new ArrayList<String>();
        if (word == null || word.length() == 0) {
            return result;
        }
        generateAbbreviations(result, word, new StringBuffer(), 0, 0);
        return result;
    }
    
    private void generateAbbreviations(List<String> result, String word, StringBuffer sb, int count, int level) {
        if (level == word.length()) {
            if (count > 0) {
                sb.append(count);
            }
            result.add(sb.toString());
            return;
        }
        int prevLen = sb.length();
        generateAbbreviations(result, word, sb, count+1, level+1);
        sb.setLength(prevLen);

        if (count > 0) {
            sb.append(count);
        }
        sb.append(word.charAt(level));
        generateAbbreviations(result, word, sb, 0, level + 1);
        sb.setLength(prevLen);
    }

    // Leetcode 527: Word Abbreviation:
    // https://leetcode.com/problems/word-abbreviation
    /**
    Given an array of n distinct non-empty strings, you need to generate minimal possible abbreviations for every word following rules below.

    Begin with the first character and then the number of characters abbreviated, which followed by the last character.
    If there are any conflict, that is more than one words share the same abbreviation, a longer prefix is used instead of only the first character until making the map from word to abbreviation become unique. In other words, a final abbreviation cannot map to more than one original words.
    If the abbreviation doesn't make the word shorter, then keep it as original.
    Example:
    Input: ["like", "god", "internal", "me", "internet", "interval", "intension", "face", "intrusion"]
    Output: ["l2e","god","internal","me","i6t","interval","inte4n","f2e","intr4n"]
    Note:
    Both n and the length of each word will not exceed 400.
    The length of each word is greater than 1.
    The words consist of lowercase English letters only.
    The return answers should be in the same order as the original array.
    */
    // Running Time Complexity: O(N^2), Space Complexity: O(N)
    public String[] wordsAbbreviation(String[] dict) {
        int n = dict.length;
        String[] result = new String[n];
        // An array to record the length of prefix we use on each word:
        // Ex: k = 1: word -> w2d
        // k = 2: word -> wo1d
        int[] prefixNum = new int[n];
        // Generate Abbreivate Words:
        for (int i = 0; i < n; i++) {
            result[i] = abbreviateWord(dict[i], 1);
            prefixNum[i] = 1;
        }
        
        for (int i = 0; i < n; i++) {
            Set<Integer> set = new HashSet<Integer>();;
            do {
                set.clear();
                for (int j = i+1; j < n; j++) {
                    if (result[j].equals(result[i])) {
                        set.add(j);
                    }
                }
                if (set.isEmpty()) {
                    break;
                }
                else {
                    set.add(i);
                    // increase the prefixNum and generate the new abbreivated word
                    for (int num : set) {
                        prefixNum[num]++;
                        result[num] = abbreviatedWord(dict[num], prefixNum[num]);
                    }
                }
            } while (!set.isEmpty());  
        }
        return result;
    }
    
    private String abbreviatedWord(String word, int k) {
        StringBuffer sb = new StringBuffer();
        sb.append(word.substring(0,k));
        sb.append(word.length() - k - 1);
        sb.append(word.charAt(word.length() - 1));
        if (sb.length() < word.length()) {
            return sb.toString();
        }
        else {
            return word;
        }
    }
}