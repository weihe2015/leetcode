public class Solution {
    // Leetcode 242: https://leetcode.com/problems/valid-anagram/
    /**
    Given two strings s and t , write a function to determine if t is an anagram of s.
    Example 1:

    Input: s = "anagram", t = "nagaram"
    Output: true
    Example 2:

    Input: s = "rat", t = "car"
    Output: false
    Note:
    You may assume the string contains only lowercase alphabets.

    Follow up:
    What if the inputs contain unicode characters? How would you adapt your solution to such case?
    */
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] cList = new int[256];
        for (char c : s.toCharArray()) {
            cList[c]++;
        }
        for (char c : t.toCharArray()) {
            cList[c]--;
            if (cList[c] < 0) {
                return false;
            }
        }
        return true;
    }

    // Leetcode 49: https://leetcode.com/problems/group-anagrams/
    /**
    Given an array of strings, group anagrams together.
    Example:

    Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
    Output:
    [
    ["ate","eat","tea"],
    ["nat","tan"],
    ["bat"]
    ]
    Note:

    All inputs will be in lowercase.
    The order of your output does not matter.
    */
    public List<List<String>> groupAnagrams(String[] strs) {
        // key -> sorted char array, value -> list of string that has the same sorted char array:
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] cList = str.toCharArray();
            Arrays.sort(cList);
            // char array cannot be key, string is unmutable, more like symbol in ruby
            String key = String.valueOf(cList);
            List<String> subList = null;
            if (!map.containsKey(key)) {
                subList = new ArrayList<String>();
            }
            else {
               subList = map.get(key);
            }
            subList.add(str);
            map.put(key, subList);
        }
        List<List<String>> result = new ArrayList<>();
        for (String key : map.keySet()) {
            List<String> subList = map.get(key);
            result.add(subList);
        }
        return result;
    }
    // My brute force solution:
    // Running Time Complexity: O(n * 2L)
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> result = new ArrayList<>();
        for (String str : strs) {
            if (result.isEmpty()) {
                List<String> tmp = new ArrayList<String>();
                tmp.add(str);
                result.add(tmp);
            }
            else {
                boolean isAdded = false;
                for (List<String> list : result) {
                    String head = list.get(0);
                    if (isAnagram(head, str)) {
                        list.add(str);
                        isAdded = true;
                        break;
                    }
                }
                if (!isAdded) {
                    List<String> tmp = new ArrayList<String>();
                    tmp.add(str);
                    result.add(tmp);
                }
            }
        }
        return result;
    }
    
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] cList = new int[256];
        for (char c : s.toCharArray()) {
            cList[c]++;
        }
        for (char c : t.toCharArray()) {
            cList[c]--;
            if (cList[c] < 0) {
                return false;
            }
        }
        return true;
    }

    // Leetcode 438: Find All Anagrams in a String
    /**
    Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
    Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.
    The order of output does not matter.
    Example 1:
    Input:
    s: "cbaebabacd" p: "abc"
    Output:
    [0, 6]
    Explanation:
    The substring with start index = 0 is "cba", which is an anagram of "abc".
    The substring with start index = 6 is "bac", which is an anagram of "abc".
    Example 2:
    Input:
    s: "abab" p: "ab"
    Output:
    [0, 1, 2]
    Explanation:
    The substring with start index = 0 is "ab", which is an anagram of "ab".
    The substring with start index = 1 is "ba", which is an anagram of "ab".
    The substring with start index = 2 is "ab", which is an anagram of "ab".
    */
    // Brute Force Solution: O(m * (2p)) 
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> result = new ArrayList<Integer>();
        int m = s.length(), n = p.length();
        for (int i = 0; i <= m-n; i++) {
            String substr = s.substring(i, i+n);
            if (isAnagram(substr, p)) {
                result.add(i);
            }
        }
        return result;
    }
}