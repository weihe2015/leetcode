public class Reader4 {
	private static final int READ_SIZE = 4;
	private char[] content;
	private int offset;
	
	public Reader4() {
		this.content = null;
		this.offset = 0;
	}
	
	public void setContent(char[] content) {
		this.offset = 0;
		this.content = content;
	}
	
	public int read4(char[] buff) {
		int n = content.length;
		int sz = Math.min(n-offset, READ_SIZE);
		for (int i = 0; i < sz; i++) {
			buff[i] = content[offset + i];
		}
		offset += sz;
		return sz;
	}
}