public class Solution {
    /**
    // Leetcode 031: Next Permutation:
    // https://leetcode.com/problems/next-permutation/

    下一个全排列
    implement next permutation, 
    which rearranges numbers into the lexicographically 
    next greater permutation of numbers.

    If such arrangement is not possible, 
    it must rearrange it as the lowest possible order 
    (ie, sorted in ascending order).


    1,2,3 → 1,3,2
    3,2,1 → 1,2,3
    1,1,5 → 1,5,1

    ex: 6 8 7 4 3 2
    1. find 6, which breaks the backward increasing thrend
    2. find 7, which is the element larger than 6 in the right
    3. swap 6 and 7
    4. reverse everything to the 7

    Explanation Video: https://www.youtube.com/watch?v=quAS1iydq7U

    */
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public void nextPermutation(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int k = -1, n = nums.length;
        for (int i = n-1; i > 0; i--){
            if (nums[i] > nums[i-1]){
                k = i-1;
                break;
            }
        }
        // if all array is decesending, reverse all array
        if (k == -1){
            reverse(nums, 0, n-1);
            return;
        }
        // find the smallest element to the right of k
        // that is larger than nums[k]
        int l = -1;
        for (int i = n-1; i > k; i--){
            if (nums[i] > nums[k]){
                l = i;
                break;
            }
        }
        // swap nums[k] and nums[l]
        swap(nums, k, l);
        // reverse array of k+1 to the end.
        reverse(nums, k+1, n-1);
    }
    
    private void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
    
    private void reverse(int[] nums, int i, int j){
        while(i < j){
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    /**
    Leecode 556. Next Greater Element III:
    https://leetcode.com/problems/next-greater-element-iii/

    Given a positive 32-bit integer n, you need to find the smallest 32-bit integer which has exactly the same digits 
    existing in the integer n and is greater in value than n. If no such positive 32-bit integer exists, 
    you need to return -1.

    Example 1:

    Input: 12
    Output: 21
    

    Example 2:

    Input: 21
    Output: -1
    */
    public int nextGreaterElement(int n) {
        String s = String.valueOf(n);
        char[] cList = s.toCharArray();
        int m = s.length();
        // check if it is decending order:
        int k = -1;
        for (int i = m-1; i > 0; i--) {
            if (cList[i] > cList[i-1]) {
                k = i-1;
                break;
            }
        }
        if (k == -1) {
            return -1;
        }
        // find the smallest element to the right of k
        // that is larger than nums[k]:
        int l = -1;
        for (int i = m-1; i > k; i--) {
            if (cList[i] > cList[k]) {
                l = i;
                break;
            }
        }
        
        swap(cList, k, l);
        reverse(cList, k+1, m-1);

        // Ex: 1999999999
        Long res = Long.parseLong(String.valueOf(cList));
        if (res > Integer.MAX_VALUE) {
            return -1;
        }
        return res.intValue();
    }
    
    private void swap(char[] cList, int i, int j) {
        char temp = cList[i];
        cList[i] = cList[j];
        cList[j] = temp;
    }
    
    private void reverse(char[] cList, int i, int j){
        while (i < j){
            swap(cList, i, j);
            i++;
            j--;
        }
    }
}