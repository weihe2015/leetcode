public class Calculator {
    // Leetcode 150: Evaluate Reverse Polish Notation
    // https://leetcode.com/problems/evaluate-reverse-polish-notation/
    /**
    Evaluate the value of an arithmetic expression in Reverse Polish Notation.

    Valid operators are +, -, *, /. Each operand may be an integer or another expression.

    Note:

    Division between two integers should truncate toward zero.
    The given RPN expression is always valid. That means the expression would always evaluate to a result and there won't be any divide by zero operation.
    Example 1:

    Input: ["2", "1", "+", "3", "*"]
    Output: 9
    Explanation: ((2 + 1) * 3) = 9
    Example 2:

    Input: ["4", "13", "5", "/", "+"]
    Output: 6
    Explanation: (4 + (13 / 5)) = 6
    Example 3:

    Input: ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
    Output: 22
    Explanation:
    ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
    = ((10 * (6 / (12 * -11))) + 17) + 5
    = ((10 * (6 / -132)) + 17) + 5
    = ((10 * 0) + 17) + 5
    = (0 + 17) + 5
    = 17 + 5
    = 22
    */
    public int evalRPN(String[] tokens) {
        int res = 0;
        if (tokens == null || tokens.length == 0) {
            return res;
        }
        Stack<Integer> stack = new Stack<>();
        for (String token : tokens) {
            if (isOperand(token) && stack.size() >= 2) {
                int num2 = stack.pop();
                int num1 = stack.pop();
                int sum = calculate(token, num1, num2);
                stack.push(sum);
            }
            else if (!isOperand(token)) {
                int num = Integer.parseInt(token);
                stack.push(num);
            }
        }
        res = stack.peek();
        return res;
    }

    private boolean isOperand(String token) {
        return token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/");
    }

    private int calculate(String token, int num1, int num2) {
        int sum = 0;
        switch (token) {
            case "+":
                sum = num1 + num2;
                break;
            case "-":
                sum = num1 - num2;
                break;
            case "*":
                sum = num1 * num2;
                break;
            case "/":
                sum = num1 / num2;
                break;
        }
        return sum;
    }

    // Leetcode 224 Basic Calculator
    // https://leetcode.com/problems/basic-calculator/
    /**
    Implement a basic calculator to evaluate a simple expression string.

    The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

    Example 1:

    Input: "1 + 1"
    Output: 2
    Example 2:

    Input: " 2-1 + 2 "
    Output: 3
    Example 3:

    Input: "(1+(4+5+2)-3)+(6+8)"
    Output: 23
    Note:
    You may assume that the given expression is always valid.
    Do not use the eval built-in library function.
     */
    // only have + - ( )
    /**
     * Basic Idea:
     *  1. for each digit, use num = 10 * num + (c - '0') to accumulate number
     *  2. For either + or -, use previous sign to add into result, and set sign to current char
     *  3. For (, push sign and res int stack
     *  4. For ), pop sign and res for previous result. calculate current res, acculate current res as res = prevResult + sign * res;
     *    Reset num = 0 to mark finish calculation
     *  5. If at the end, num != 0, use prevSign to calculate res and add it into res.
     */
    public int calculate(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        Stack<Integer> stack = new Stack<>();
        // Set first number as positive:
        int sum = 0;
        int num = 0;
        int sign = 1;
        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)){
                // read number like "23" "412"
                num = 10 * num + (int)(c - '0');
            }
            else if (c == '+'){
                // save previous num into result with previous sign:
                sum += sign * number;
                // Reset positive sign for next number
                sign = 1;
                // reset num:
                num = 0;
            }
            else if (c == '-') {
                // save previous num into result with previous sign:
                sum += sign * num;
                // set negative sign for next number
                sign = -1;
                // reset num:
                num = 0;
            }
            else if (c == '(') {
                // push previous result and previous sign into stack to save it for later
                stack.push(sum);
                stack.push(sign);
                // reset sign for positive:
                sign = 1;
                // reset sum and num:
                sum = 0;
                num = 0;
            }
            else if (c == ')') {
                // calculate res for current expression in parentheses
                sum += sign * num;
                int prevSign = stack.pop();
                int prevResult = stack.pop();
                sum = prevResult + prevSign * sum;
                // reset num to indicate finish of calculation
                num = 0;
                sign = 1;
            }
        }
        // handle the last number:
        if (num != 0) {
            sum += sign * num;
        }
        return sum;
    }

    // Leetcode 227: Basic Calculator II
    // https://leetcode.com/problems/basic-calculator-ii/
    /**
    Implement a basic calculator to evaluate a simple expression string.
    The expression string contains only non-negative integers, +, -, *, / operators and empty spaces . The integer division should truncate toward zero.

    Example 1:

    Input: "3+2*2"
    Output: 7
    Example 2:

    Input: " 3/2 "
    Output: 1
    Example 3:

    Input: " 3+5 / 2 "
    Output: 5
    Note:

    You may assume that the given expression is always valid.
    Do not use the eval built-in library function.
    */
    public int calculate(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        Stack<Integer> stack = new Stack<Integer>();
        int num = 0;
        int sum = 0;
        char prevSign = '+';
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                continue;
            }
            else if (Character.isDigit(c)) {
                num = 10 * num + (c - '0');
            }
            else {
                handleOperators(stack, num, prevSign);
                // set current sign as previous sign:
                prevSign = c;
                // reset num:
                num = 0;
            }
        }
        if (num != 0) {
            handleOperators(stack, num, prevSign);
        }
        // add all numbers in the stack:
        for (int n : stack) {
            sum += n;
        }
        return sum;
    }

    private void handleOperators(Stack<Integer> stack, int num, char prevSign) {
        // use previous sign to calculate the new num:
        if (prevSign == '+') {
            stack.push(num);
        }
        else if (prevSign == '-') {
            stack.push(-num);
        }
        else if (prevSign == '*') {
            int prevNum = stack.pop();
            num = prevNum * num;
            stack.push(num);
        }
        else if (prevSign == '/') {
            int prevNum = stack.pop();
            num = prevNum / num;
            stack.push(num);
        }
    }

    // Running Time Complexity: O(n), Space Complexity: O(1)
    // Basic Idea, for prevSign == '+' or '-', set prevNum = num and save prevNum into result
    // For prevSign == '*' or '/', substract preNum from result, calculate prevNum with current Num and add the prevNum back to result.
    public int calculate(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int result = 0,
        int num = 0,
        int prevNum = 0;
        char prevSign = '+';
        for (char c : s.toCharArray()) {
            if (c == ' '){
                continue;
            }
            if (Character.isDigit(c)) {
                num = 10 * num + (c - '0');
            }
            else {
                int[] arr = handleOperators(result, prevNum, num, prevSign);
                result = arr[0];
                prevNum = arr[1];
                prevSign = c;
                num = 0;
            }
        }
        if (num != 0) {
            int[] arr = handleOperators(result, prevNum, num, prevSign);
            result = arr[0];
            prevNum = arr[1];
        }
        return result;
    }

    private int[] handleOperators(int result, int prevNum, int num, char prevSign) {
        int[] arr = new int[2];
        if (prevSign == '+') {
            prevNum = num;
        }
        else if (prevSign == '-') {
            prevNum = -1 * num;
        }
        else if (prevSign == '*') {
            result -= prevNum;
            prevNum = prevNum * num;
        }
        else if (prevSign == '/') {
            result -= prevNum;
            prevNum = prevNum / num;
        }
        result += prevNum;
        arr[0] = result;
        arr[1] = prevNum;
        return arr;
    }

    // Leetcode 772: Basic Calculator III:
    // https://leetcode.com/problems/basic-calculator-iii
    public int calculate(String s) {
        Stack<Object> stack = new Stack<Object>();
        int result = 0;
        int num = 0;
        int prevNum = 0;
        char prevSign = '+';
        for (char c : s.toCharArray()) {
            if (c == ' '){
                continue;
        }
            if (Character.isDigit(c)) {
                num = 10 * num + (c - '0');
            }
            else if (c == '(') {
                stack.push(result);
                stack.push(prevSign);
                prevSign = '+';
                result = 0;
                num = 0;
            }
            else if (c == ')') {
                int[] arr = handleOperators(result, prevNum, num, prevSign);
                result = arr[0];

                // set num as the result of this expression inside the parentheses
                num = result;
                // get the prevSign from stack:
                prevSign = (Character) stack.pop();
                // get result from stack and set prevNum the same as result.
                result = (Integer) stack.pop();
                prevNum = result;
            }
            else {
                int[] arr = handleOperators(result, prevNum, num, prevSign);
                result = arr[0];
                prevNum = arr[1];
                prevSign = c;
                num = 0;
            }
        }
        if (num != 0) {
            int[] arr = handleOperators(result, prevNum, num, prevSign);
            result = arr[0]; prevNum = arr[1];
        }
        return result;
    }

    private int[] handleOperators(int result, int prevNum, int num, char prevSign) {
        int[] arr = new int[2];
        if (prevSign == '+') {
            prevNum = num;
        }
        else if (prevSign == '-') {
            prevNum = -1 * num;
        }
        else if (prevSign == '*') {
            result -= prevNum;
            prevNum = prevNum * num;
        }
        else if (prevSign == '/') {
            result -= prevNum;
            prevNum = prevNum / num;
        }
        result += prevNum;
        arr[0] = result;
        arr[1] = prevNum;
        return arr;
    }

}