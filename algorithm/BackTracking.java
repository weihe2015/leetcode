
public class Solution {
    // Letter Combination of a Phone Number:
    // Leetcode 17: https://leetcode.com/problems/letter-combinations-of-a-phone-number/
    /**
    Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent.

    A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.

    Example:

    Input: "23"
    Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
    */
    // Backtracking Methods, Running Time Complexity: T(n) = k * T(n-1).
    // T(n) = values.length() ^ digits.length(), Space Complexity O(n)
    private static Map<Character, String> map = new LinkedHashMap<>();
    static {
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxzy");
    }
    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<String>();
        if (digits == null || digits.length() == 0) {
            return result;
        }
        StringBuffer sb = new StringBuffer();
        letterCombinations(result, digits, 0, sb);
        return result;
    }

    private void letterCombinations(List<String> result, String digits, int level, StringBuffer sb) {
        // ensure that only same length of digits string will be produced
        if (sb.length() == digits.length()) {
            result.add(sb.toString());
        }
        for (int i = level, max = digits.length(); i < max; i++) {
            char num = digits.charAt(i);
            String str = map.get(num);
            for (char c : str.toCharArray()) {
                sb.append(c);
                letterCombinations(result, digits, i+1, sb);
                sb.setLength(sb.length()-1);
                // sb.setLength: modify the count and overwrites the unwanted value in the array with a zero byte
                // sb.deleteCharAt(sb.length()-1) : perform an array copy internally, before altering the count.
            }
        }
    }

    /**
    https://www.1point3acres.com/bbs/thread-682299-1-1.html

    Given a list of pairs, where each pair defines the min and max number it can choose,
    return all the possible number of combinations:
    Input: [(2,4) (4,5) (4,5)]
    Output: [244, 245, 254, 255, 344, 345, 354, 355, 444, 445, 454, 455]

    Cons of recursive: it needs to visit all the combination before returning.
    Consider the case of [(0,0), (0,0), (4,5)]的case，
    The output number cannot have leading 0
    */
    public List<Integer> numberCombination(int[][] pairs) {
        List<Integer> res = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        int m = pairs.length;
        numberCombination(list, pairs, 0, m, sb);
        return res;
    }

    private void numberCombination(List<Integer> list, int[][] pairs, int level, int m, StringBuffer sb) {
        if (sb.length() == m) {
            String s = sb.toString();
            if (isValidNumber(s)) {
                list.add(Integer.parseInt(s));
            }
            return;
        }
        for (int i = level; i < m; i++) {
            int[] pair = pairs[i];
            int minVal = pair[0];
            int maxVal = pair[1];
            for (int j = minVal; j <= maxVal; j++) {
                sb.append(j);
                numberCombination(list, pair, i+1, m, sb);
                sb.setLength(sb.length()-1);
            }
        }
    }

    private boolean isValidNumber(String s) {
        return s.length() > 1 && s.charAt(0) != '0';
    }

    // Leetcode 22: Generate Parentheses
    /**
    Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

    For example, given n = 3, a solution set is:

    [
    "((()))",
    "(()())",
    "(())()",
    "()(())",
    "()()()"
    ]
    */
    // use string buffer: Time Complexity -  n * Catalan number (n) ~ O(4n)， Space Complexity O(n^2)
    public List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<String>();
        if (n < 1) {
            return result;
        }
        dfs(result, n, n, new StringBuffer());
        return result;
    }

    private void dfs(List<String> result, int open, int close, StringBuffer sb) {
        if (open == 0 && close == 0) {
            result.add(sb.toString());
            return;
        }
        if (open > 0) {
            sb.append('(');
            open--;
            dfs(result, open, close, sb);
            open++;
            sb.setLength(sb.length()-1);
        }
        if (open < close) {
            sb.append(')');
            close--;
            dfs(result, open, close, sb);
            close++;
            sb.setLength(sb.length()-1);
        }
    }

    // Leetcode 39: Combination Sum:
    // https://leetcode.com/problems/combination-sum/
    /**
    Given a set of candidate numbers (candidates) (without duplicates) and a target number (target),
    find all unique combinations in candidates where the candidate numbers sums to target.
    The same repeated number may be chosen from candidates unlimited number of times.

    Note:

    All numbers (including target) will be positive integers.
    The solution set must not contain duplicate combinations.
    Example 1:

    Input: candidates = [2,3,6,7], target = 7,
    A solution set is:
    [
    [7],
    [2,2,3]
    ]
    Example 2:

    Input: candidates = [2,3,5], target = 8,
    A solution set is:
    [
    [2,2,2,2],
    [2,3,3],
    [3,5]
    ]
    */
    // Running Time Complexity: T(N) = N * T(N-1) = O(N!)
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        if (candidates == null || candidates.length == 0) {
            return result;
        }
        int n = candidates.length;
        List<Integer> list = new ArrayList<>();
        combinationSum(result, candidates, 0, n, list, 0, target);
        return result;
    }

    private void combinationSum(List<List<Integer>> result, int[] candidates, int level, int n, List<Integer> list, int sum, int target) {
        if (sum > target) {
            return;
        }
        if (sum == target) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        // use level variable because we don't [3,3,2] in case [2,3,5] target = 8, only for [2,3,3]
        for (int i = level; i < n; i++) {
            int num = candidates[i];
            sum += num;
            list.add(num);
            // 这里用i，而不是i+1是因为一个元素可以无限次用，如果只能用一次的话，那就是i+1
            combinationSum(result, candidates, i, n, list, sum, target);
            sum -= num;
            list.remove(list.size()-1);
        }
        /***
         * 如果是以下的code：没有用level这个变量的话，就会出现以下的结果：
        candidates=[2,3,6,7], target=7, result= [[2,2,3],[2,3,2],[3,2,2],[7]]
        private void dfs(List<List<Integer>> result, int[] candidates, int target, List<Integer> list) {
            if (target < 0) {
                return;
            }
            if (target == 0) {
                result.add(new ArrayList<Integer>(list));
                return;
            }
            for (int i = 0, n = candidates.length; i < n; i++) {
                int num = candidates[i];
                list.add(num);
                dfs(result, candidates, target-num, list);
                list.remove(list.size()-1);
            }
        }
        */
    }

    // Leetcode 40: Combination Sum II
    // https://leetcode.com/problems/combination-sum-ii
    /**
    Given a collection of candidate numbers (candidates) and a target number (target), find all unique combinations in candidates where the candidate numbers sums to target.

    Each number in candidates may only be used once in the combination.

    Note:

    All numbers (including target) will be positive integers.
    The solution set must not contain duplicate combinations.
    Example 1:

    Input: candidates = [10,1,2,7,6,1,5], target = 8,
    A solution set is:
    [
    [1, 7],
    [1, 2, 5],
    [2, 6],
    [1, 1, 6]
    ]
    Example 2:

    Input: candidates = [2,5,2,1,2], target = 5,
    A solution set is:
    [
    [1,2,2],
    [5]
    ]
    */
    // Running Time Complexity: T(N) = N * T(N-1) = O(N!)
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        if (candidates == null || candidates.length == 0) {
            return result;
        }
        Arrays.sort(candidates);
        List<Integer> list = new ArrayList<>();
        combinationSum(result, candidates, 0, target, 0, list);
        return result;
    }

    private void combinationSum(List<List<Integer>> result, int[] candidates, int sum, int target, int level, List<Integer> list) {
        if (sum > target) {
            return;
        }
        if (sum == target) {
            res.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = level, max = candidates.length; i < max; i++) {
            if (i > level && candidates[i] == candidates[i-1]) {
                continue;
            }
            sum += candidates[i];
            list.add(candidates[i]);
            // i + 1 because each number in the set is used only once.
            combinationSum(result, candidates, sum, target, i+1, list);
            sum -= candidates[i];
            list.remove(list.size()-1);
        }
    }

    // Leetcode 216: Combination Sum III:
    /**
     * Find all possible combinations of k numbers that add up to a number n, given that only numbers from 1 to 9 can be used and each combination should be a unique set of numbers.
    Note:

    All numbers will be positive integers.
    The solution set must not contain duplicate combinations.
    Example 1:

    Input: k = 3, n = 7
    Output: [[1,2,4]]
    Example 2:

    Input: k = 3, n = 9
    Output: [[1,2,6], [1,3,5], [2,3,4]]
    */
    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> result = new ArrayList<>();
        if (k <= 0 || n <= 0) {
            return result;
        }
        List<Integer> list = new ArrayList<>();
        combinationSum(result, k, n, 1, list);
        return result;
    }

    private void combinationSum(List<List<Integer>> result, int k, int target, int sum, int level, List<Integer> list) {
        if (sum == target && list.size() == k) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        if (sum >= target || list.size() >= k) {
            return;
        }
        for (int i = level; i <= 9; i++) {
            sum += i;
            list.add(i);
            // i+1 because each number can be used only once.
            combinationSum(result, k, target, sum, i+1, list);
            list.remove(list.size()-1);
            sum -= i;
        }
    }

    // Leetcode 46: Permutation: https://leetcode.com/problems/permutations/
    /**
    Given a collection of distinct integers, return all possible permutations.
    Example:

    Input: [1,2,3]
    Output:
    [
    [1,2,3],
    [1,3,2],
    [2,1,3],
    [2,3,1],
    [3,1,2],
    [3,2,1]
    ]
    */
    // Running Time Complexity: T(N) = N * T(N-1) = O(N!)
    // Space Complexity: O(N)
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        int n = nums.length;
        boolean[] visited = new boolean[n];
        dfs(result, nums, visited, n, new ArrayList<Integer>());
        return result;
    }

    private void dfs(List<List<Integer>> result, int[] nums, boolean[] visited, int n, List<Integer> list) {
        if (list.size() == n) {
            // use new ArrayList<Integer>(list) to create a new array
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            visited[i] = true;
            list.add(nums[i]);
            dfs(result, nums, visited, n, list);
            list.remove(list.size()-1);
            visited[i] = false;
        }
    }

    // Leetcode 47: Permutation II:
    // https://leetcode.com/problems/permutations-ii
    /**
    Given a collection of numbers that might contain duplicates, return all possible unique permutations.
    Example:

    Input: [1,1,2]
    Output:
    [
    [1,1,2],
    [1,2,1],
    [2,1,1]
    ]
    */
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        boolean[] usedList = new boolean[nums.length];
        generatePermutationUnique(result, nums, new ArrayList<Integer>(), usedList);
        return result;
    }

    private void generatePermutationUnique(List<List<Integer>> result, int[] nums, ArrayList<Integer> list, boolean[] usedList) {
        int max = nums.length;
        if (list.size() == max) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = 0; i < max; i++) {
            if (usedList[i] || (i > 0 && nums[i] == nums[i-1] && !usedList[i-1])) {
                continue;
            }
            list.add(nums[i]);
            usedList[i] = true;
            generatePermutationUnique(result, nums, list, usedList);
            list.remove(list.size()-1);
            usedList[i] = false;
        }
    }

    // Leetcode 60: Permutation Sequence:
    // https://leetcode.com/problems/permutation-sequence/
    /***
    The set [1,2,3,...,n] contains a total of n! unique permutations.

    By listing and labeling all of the permutations in order, we get the following sequence for n = 3:

    "123"
    "132"
    "213"
    "231"
    "312"
    "321"
    Given n and k, return the kth permutation sequence.

    Note:

    Given n will be between 1 and 9 inclusive.
    Given k will be between 1 and n! inclusive.
    Example 1:

    Input: n = 3, k = 3131
    Output: "213"
    Example 2:

    Input: n = 4, k = 9
    Output: "2314"
    */
    // Brute Force Solution:
    // Running Time Complexity: O(n!), Space Complexity: O(n)
    public String getPermutation(int n, int k) {
        List<String> res = new ArrayList<>();
        boolean[] visited = new boolean[n+1];
        permutations(n, res, new StringBuffer(), visited);
        return res.get(k-1);
    }

    private void permutations(int n, List<String> res, StringBuffer sb, boolean[] visited) {
        if (sb.length() == n) {
            res.add(sb.toString());
            return;
        }
        for (int i = 1; i <= n; i++) {
            if (visited[i]) {
                continue;
            }
            visited[i] = true;
            sb.append(i + "");
            permutations(n, res, sb, visited);
            visited[i] = false;
            sb.setLength(sb.length()-1);
        }
    }
    // Leetcode 78: SubSets:
    /**
    Given a set of distinct integers, nums, return all possible subsets (the power set).
    Note: The solution set must not contain duplicate subsets.

    Example:

    Input: nums = [1,2,3]
    Output:
    [
    [3],
    [1],
    [2],
    [1,2,3],
    [1,3],
    [2,3],
    [1,2],
    []
    ]
    */
    // Given a set of distinct integers, nums, return all possible subsets (the power set).
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        int n = nums.length;
        List<Integer> list = new ArrayList<>();
        dfs(result, nums, 0, n, list);
        return result;
    }

    private void dfs(List<List<Integer>> result, int[] nums, int level, int n, List<Integer> list) {
        // 这里我们不用加条件判断list的大小，直接加，是因为每个subarray的大小不一定是nums array的大小
        result.add(new ArrayList<Integer>(list));

        for (int i = level; i < n; i++) {
            list.add(nums[i]);
            dfs(result, nums, i+1, n, list);
            list.remove(list.size()-1);
        }
    }

    // Leetcode 90, SubSets II:  containing duplicate numbers
    /**
    Given a collection of integers that might contain duplicates, nums, return all possible subsets (the power set).
    Note: The solution set must not contain duplicate subsets.

    Example:

    Input: [1,2,2]
    Output:
    [
    [2],
    [1],
    [1,2,2],
    [2,2],
    [1,2],
    []
    ]
    */
    // Given a set of distinct integers, nums, return all possible subsets (the power set).
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        generateSubSetsWithDup(result, nums, 0, new ArrayList<Integer>());
        return result;
    }

    private void generateSubSetsWithDup(List<List<Integer>> result, int[] nums, int level, List<Integer> list) {
        result.add(new ArrayList<Integer>(list));
        for (int i = level, n = nums.length; i < n; i++) {
            // Key to avoid duplicate in the result.
            if (i > level && nums[i] == nums[i-1]) {
                continue;
            }
            list.add(nums[i]);
            generateSubSetsWithDup(result, nums, i+1, list);
            list.remove(list.size()-1);
        }
    }

    // Leetcode 77 Combinations:
    // https://leetcode.com/problems/combinations
    /**
    Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.
    Example:

    Input: n = 4, k = 2
    Output:
    [
    [2,4],
    [3,4],
    [2,3],
    [1,2],
    [1,3],
    [1,4],
    ]
    */
    // Running Time Complexity: T(N) = T(N-1) + T(N-2) + ... + T(1) = O(2^N)
    // Space Complexity: O(N)
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        if (n < 1 || k < 1) {
            return result;
        }
        generateCombination(result, n, k, 1, new ArrayList<Integer>());
        return result;
    }

    private void generateCombination(List<List<Integer>> result, int n, int k, int level, List<Integer> list) {
        // This step depends on the subArray size
        if (list.size() == k) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = level; i <= n; i++) {
            list.add(i);
            generateCombination(result, n, k, i+1, list);
            list.remove(list.size()-1);
        }
    }

    // Leetcode 377: Combination Sum IV
    // https://leetcode.com/problems/combination-sum-iv
    /**
    Given an integer array with all positive numbers and no duplicates, find the number of possible combinations that add up to a positive integer target.

    Example:

    nums = [1, 2, 3]
    target = 4

    The possible combination ways are:
    (1, 1, 1, 1)
    (1, 1, 2)
    (1, 2, 1)
    (1, 3)
    (2, 1, 1)
    (2, 2)
    (3, 1)
    Note that different sequences are counted as different combinations.

    Therefore the output is 7.
    */
    // Backtracking Solution: Memory Limit Exceeded
    public int combinationSum4(int[] nums, int target) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return 0;
        }
        Arrays.sort(nums);
        combinationSum4(result, nums, target, new ArrayList<Integer>());
        return result.size();
    }

    private void combinationSum4(List<List<Integer>> result, int[] nums, int target, ArrayList<Integer> list) {
        if (target < 0) {
            return;
        }
        if (target == 0) {
            result.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = 0, max = nums.length; i < max; i++) {
            int val = nums[i];
            list.add(val);
            combinationSum4(result, nums, target-val, list);
            list.remove(list.size()-1);
        }
    }

    // Leetcode 131: Palindrome Partitioning:
    // https://leetcode.com/problems/palindrome-partitioning/
    /**
    Given a string s, partition s such that every substring of the partition is a palindrome.

    Return all possible palindrome partitioning of s.

    Example:

    Input: "aab"
    Output:
    [
    ["aa","b"],
    ["a","a","b"]
    ]
    */
    public List<List<String>> partition(String s) {
        List<List<String>> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        List<String> list = new ArrayList<>();
        partition(res, s, 0, list);
        return res;
    }

    private void partition(List<List<String>> res, String s, int level, List<String> list) {
        if (level == s.length() && !list.empty()) {
            res.add(new ArrayList<>(list));
            return;
        }
        for (int i = level, n = s.length(); i < n; i++) {
            String str = s.substring(level, i+1);
            if (!isPalindrome(str)) {
                continue;
            }
            list.add(str);
            partition(result, s, i+1, list);
            list.remove(list.size()-1);
        }
    }

    private boolean isPalindrome(String s) {
        int i = 0;
        int j = s.length()-1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    // Leetcode 93: Restore IP Addresses
    // https://leetcode.com/problems/restore-ip-addresses
    /**
    Given a string containing only digits, restore it by returning all possible valid IP address combinations.

    Example:

    Input: "25525511135"
    Output: ["255.255.11.135", "255.255.111.35"]
    */
    // Backtracking, Running Time Complexity: O(1), ipv4: 32bits, 2^32 total numbers of IP address.
    // Space Complexity: O(1)
    public List<String> restoreIpAddresses2(String s) {
        List<String> result = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return result;
        }
        restoreIpAddresses(result, s, 0, 0, new StringBuffer());
        return result;
    }

    private void restoreIpAddresses(List<String> result, String s, int index, int numCount, StringBuffer sb) {
        // If there are more than 4 numbers, invalid case, return
        if (numCount > 4) {
            return;
        }
        if (numCount == 4 && index == s.length()) {
            result.add(sb.toString());
            return;
        }
        // Find 4 numbers for 3 times:
        for (int i = 1; i <= 3; i++) {
            if (index + i > s.length()) {
                break;
            }
            String numStr = s.substring(index, index + i);
            // If number is starts with 0 and the number's length is greater than 1, skip. Ex: avoid 022, 013 ...
            // If there are three nums and last number is greater than 255, skip.
            if ((numStr.charAt(0) == '0' && numStr.length() > 1) || (i == 3 && Integer.parseInt(numStr) > 255)) {
                continue;
            }
            sb.append(numStr);
            // If there are less than 3 numbers, add . at the end
            if (numCount < 3) {
            	sb.append(".");
            }
            restoreIpAddresses(result, s, index + i, numCount+1, sb);
            // reset stringbuffer back to previous state.
            if (numCount < 3) {
                sb.setLength(sb.length() - i - 1);
            }
            else {
            	sb.setLength(sb.length() - i);
            }
        }
    }
    // Solution 2: https://www.youtube.com/watch?v=KU7Ae2513h0
    public List<String> restoreIpAddresses(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        int[] paths = new int[4];
        int n = s.length();
        restoreIpAddresses(res, s, 0, n, 0, paths);
        return res;
    }

    private void restoreIpAddresses(List<String> res, String s, int idx, int n, int segmentIdx, int[] paths) {
        if (idx == n && segmentIdx == 4) {
            String ipAddressStr = buildStringFromArray(paths);
            res.add(ipAddressStr);
            return;
        }
        if (idx >= n || segmentIdx == 4) {
            return;
        }
        for (int i = 1; i <= 3 && idx + i <= n; i++) {
            String segment = s.substring(idx, idx+i);
            int num = Integer.parseInt(segment);
            if (num > 255 || (i >= 2 && segment.charAt(0) == '0')) {
                break;
            }
            paths[segmentIdx] = num;
            restoreIpAddresses(res, s, idx+i, n, segmentIdx+1, paths);
            paths[segmentIdx] = -1;
        }
    }

    private String buildStringFromArray(int[] paths) {
        StringBuffer sb = new StringBuffer();
        for (int path : paths) {
            if (path != -1) {
                sb.append(path).append('.');
            }
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    // Leetcode 051: N-Queens
    // https://leetcode.com/problems/n-queens/
    /**
    Given an integer n, return all distinct solutions to the n-queens puzzle.

    Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a queen and an empty space respectively.

    Example:

    Input: 4
    Output: [
    [".Q..",  // Solution 1
    "...Q",
    "Q...",
    "..Q."],

    ["..Q.",  // Solution 2
    "Q...",
    "...Q",
    ".Q.."]
    ]
    Explanation: There exist two distinct solutions to the 4-queens puzzle as shown above.
    */
    // Running Time Complexity: O(N^4N). Use list to store current Queen position will reduce to O(N^2N)
    public List<List<String>> solveNQueens(int n) {
        List<List<String>> res = new ArrayList<>();
        if (n < 1) {
            return res;
        }
        char[][] board = new char[n][n];
        constructBoard(board, n);
        solveNQueens(res, board, n, 0);
        return res;
    }

    private void solveNQueens(List<List<String>> res, char[][] board, int n, int idx) {
        if (idx == n) {
            List<String> list = constructList(board, n);
            res.add(list);
            return;
        }
        for (int i = 0; i < n; i++) {
            if (validateQueen(board, i, idx, n)) {
                board[i][idx] = 'Q';
                solveNQueens(res, board, n, idx+1);
                board[i][idx] = '.';
            }
        }
    }

    private boolean validateQueen(char[][] board, int i, int j, int n) {
        // validate row:
        for (int x = 0; x < n; x++) {
            if (board[x][j] == 'Q') {
                return false;
            }
        }

        // validate column:
        for (int y = 0; y < n; y++) {
            if (board[i][y] == 'Q') {
                return false;
            }
        }

        // validate diagonal:
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < j; y++) {
                if (board[x][y] == 'Q' && Math.abs(x-i) == Math.abs(y-j)) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<String> constructList(char[][] board, int n) {
        List<String> list = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        for (char[] cList : board) {
            for (char c : cList) {
                sb.append(c);
            }
            list.add(sb.toString());
            sb.setLength(0);
        }
        return list;
    }

    private void constructBoard(char[][] board, int n) {
        for (int i = 0; i < n; i++) {
            Arrays.fill(board[i], '.');
        }
    }

    // Leetcode 052: N-Queens II:
    // https://leetcode.com/problems/n-queens-ii/
    /**
    Given an integer n, return the number of distinct solutions to the n-queens puzzle.

    Example:

    Input: 4
    Output: 2
    Explanation: There are two distinct solutions to the 4-queens puzzle as shown below.
    [
    [".Q..",  // Solution 1
    "...Q",
    "Q...",
    "..Q."],

    ["..Q.",  // Solution 2
    "Q...",
    "...Q",
    ".Q.."]
    ]
    */
    public int totalNQueens(int n) {
        boolean[] cols = new boolean[n];     // columns   |
        boolean[] d1 = new boolean[2 * n];   // diagonals \
        boolean[] d2 = new boolean[2 * n];   // diagonals /
        int[] res = new int[1];
        totalNQueens(cols, d1, d2, 0, n, res);
        return res[0];
    }

    private void totalNQueens(boolean[] cols, boolean[] d1, boolean[] d2, int row, int n, int[] res) {
        if (row == n) {
            res[0]++;
            return;
        }
        for (int col = 0; col < n; col++) {
            int id1 = col - row + n;
            int id2 = col + row;
            if (cols[col] || d1[id1] || d2[id2]) {
                continue;
            }
            cols[col] = true;
            d1[id1] = true;
            d2[id2] = true;
            totalNQueens(cols, d1, d2, row+1, n, res);
            cols[col] = false;
            d1[id1] = false;
            d2[id2] = false;
        }
    }

    // Leetcode 254: Factor Combinations
    /**
    Numbers can be regarded as product of its factors. For example,

    8 = 2 x 2 x 2;
    = 2 x 4.
    Write a function that takes an integer n and return all possible combinations of its factors.

    Note:
    Each combinations factors must be sorted ascending, for example: The factors of 2 and 6 is [2, 6], not [6, 2].
    You may assume that n is always positive.
    Factors should be greater than 1 and less than n.
    Examples:
    input: 1
    output:
    []
    input: 37
    output:
    []
    input: 12
    output:
    [[2, 6],[2, 2, 3],[3, 4]]
    input: 32
    output:
    [[2, 16],[2, 2, 8],[2, 2, 2, 4],[2, 2, 2, 2, 2],[2, 4, 4],[4, 8]]
    */
    // https://leetcode.com/problems/factor-combinations
    // https://www.lintcode.com/problem/factor-combinations/description
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> res = new ArrayList<>();
        if (n <= 1) {
            return res;
        }
        List<Integer> list = new ArrayList<>();
        getFactors(res, 2, n, list);
        return res;
    }

    private void getFactors(List<List<Integer>> res, int idx, int n, List<Integer> list) {
        if (n == 1 && list.size() > 1) {
            res.add(new ArrayList<Integer>(list));
            return;
        }
        for (int i = idx; i <= n; i++) {
            if (n % i != 0) {
                continue;
            }
            list.add(i);
            getFactors(res, i, n/i, list);
            list.remove(list.size()-1);
        }
    }

    // Leetcode 267: Palindrome Permutation II
    // https://leetcode.com/problems/palindrome-permutation-ii
    // https://www.lintcode.com/problem/palindrome-permutation-ii
    /**
        1. Check if original String can be form palindrome or not
        2. If it cannot form palindrome, return empty list
        3. It it can form palindrome:
           3.1 Distinguish characters that has odd or even occurrence
           3.2 put 1 char of each odd occurrence as middle string
           3.3 form all permutation of all strings that has half occurrence:
    Ex: abcccba
        1. Generate all permutation of abc
        2. for each permutation, result string will be abc + midStr + abc.reverse();
    Note: Use HashSet or boolean array to avoid generate duplicate string
    */
    // Running Time Complexity: O(N!), Space Complexity: O(N)
    public List<String> generatePalindromes(String s) {
        List<String> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        if (!canBePalindrome(s)) {
            return res;
        }
        // Count frequency of each character
        Map<Character, Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        // Permutation: even char + odd char + even char
        // We only need to find all permutation of first half of str, append with odd char and reverse of first half of str
        StringBuffer oddStr = new StringBuffer();
        StringBuffer sb = new StringBuffer();
        for (char key : map.keySet()) {
            int freq = map.get(key);
            if (freq % 2 != 0) {
                oddStr.append(key);
            }
            for (int i = 0; i < freq/2; i++) {
                sb.append(key);
            }
        }
        int n = str.length();
        s = str.toString();
        boolean[] visited = new boolean[n];
        genernatePermutation(res, n, s, new StringBuffer(), oddStr, visited);
        return res;
    }

    private void genernatePermutation(List<String> res, int n, String s, StringBuffer sb, StringBuffer oddStr, boolean[] visited) {
        if (sb.length() == n) {
            String palindromeStr = sb.toString() + oddStr + sb.reverse().toString();
            res.add(palindromeStr);
            sb.reverse();
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i] || (i > 0 && s.charAt(i) == s.charAt(i-1) && !visited[i-1])) {
                continue;
            }
            sb.append(cList[i]);
            visited[i] = true;
            genernatePermutation(res, n, s, sb, oddStr, visited);
            sb.setLength(sb.length()-1);
            visited[i] = false;
        }
    }

    private boolean canBePalindrome(String s) {
        Set<Character> set = new HashSet<>();
        for (char c : s.toCharArray()) {
            if (!set.contains(c)) {
                set.add(c);
            }
            else {
                set.remove(c);
            }
        }
        return set.size() <= 1;
    }

    private boolean isPalindrome(String s) {
        int i = 0, j = s.length()-1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++; j--;
        }
        return true;
    }

    // Word Ladder II: Word Ladder II
    // https://leetcode.com/problems/word-ladder-ii/
    /**
    Given two words (beginWord and endWord), and a dictionary's word list, find all shortest transformation sequence(s) from beginWord to endWord, such that:

    Only one letter can be changed at a time
    Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
    Note:

    Return an empty list if there is no such transformation sequence.
    All words have the same length.
    All words contain only lowercase alphabetic characters.
    You may assume no duplicates in the word list.
    You may assume beginWord and endWord are non-empty and are not the same.
    Example 1:

    Input:
    beginWord = "hit",
    endWord = "cog",
    wordList = ["hot","dot","dog","lot","log","cog"]

    Output:
    [
    ["hit","hot","dot","dog","cog"],
    ["hit","hot","lot","log","cog"]
    ]
    Example 2:

    Input:
    beginWord = "hit"
    endWord = "cog"
    wordList = ["hot","dot","dog","lot","log"]

    Output: []

    Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
    */
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> result = new ArrayList<>();
        Set<String> wordDict = new HashSet<String>(wordList);
        if (!wordDict.contains(endWord)) {
            return result;
        }

        Map<String, List<String>> nodeNeighborLists = new HashMap<>();
        nodeNeighborLists.put(beginWord, new ArrayList<String>());
        for (String word : wordList) {
            nodeNeighborLists.put(word, new ArrayList<String>());
        }

        Map<String, Integer> distanceMap = new HashMap<String, Integer>();
        distanceMap.put(beginWord, 0);

        bfs(beginWord, endWord, wordDict, nodeNeighborLists, distanceMap);
        dfs(result, beginWord, endWord, nodeNeighborLists, distanceMap, new ArrayList<String>());

        return result;
    }

    private void bfs(String beginWord, String endWord, Set<String> wordDict, Map<String,
                     List<String>> nodeNeighborLists, Map<String, Integer> distanceMap) {
        Set<String> reached = new HashSet<String>();
        reached.add(beginWord);
        while (!reached.contains(endWord)) {
            Set<String> neighbors = new HashSet<String>();
            for (String str : reached) {
                int currDistance = distanceMap.get(str);
                char[] cList = str.toCharArray();
                for (int i = 0, n = cList.length; i < n; i++) {
                    char oldChar = cList[i];
                    for (char ch = 'a'; ch <= 'z'; ch++) {
                        if (ch == oldChar) {
                            continue;
                        }
                        cList[i] = ch;
                        String newWord = new String(cList);
                        if (wordDict.contains(newWord)) {
                            nodeNeighborLists.get(str).add(newWord);
                            if (!distanceMap.containsKey(newWord)) {
                                distanceMap.put(newWord, currDistance+1);
                                neighbors.add(newWord);
                            }
                        }
                        cList[i] = oldChar;
                    }
                }
            }
            if (neighbors.isEmpty()) {
                break;
            }
            reached = neighbors;
        }
    }

    private void dfs(List<List<String>> result, String beginWord, String endWord, Map<String,
                     List<String>> nodeNeighborLists, Map<String, Integer> distanceMap, List<String> list) {
        list.add(beginWord);
        if (endWord.equals(beginWord)) {
            result.add(new ArrayList<String>(list));
            list.remove(list.size()-1);
            return;
        }
        List<String> nodeNeighborList = nodeNeighborLists.get(beginWord);
        for (String neighbor : nodeNeighborList) {
            int currDistance = distanceMap.get(beginWord);
            int nextDistance = distanceMap.get(neighbor);
            if (currDistance == nextDistance - 1) {
                dfs(result, neighbor, endWord, nodeNeighborLists, distanceMap, list);
            }
        }
        list.remove(list.size()-1);
    }

    // Leetcode 140 Word Break II:
    // https://leetcode.com/problems/word-break-ii/
    /**
    Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, add spaces in s to construct a sentence
    where each word is a valid dictionary word.
    Return all such possible sentences.

    Note:

    The same word in the dictionary may be reused multiple times in the segmentation.
    You may assume the dictionary does not contain duplicate words.
    Example 1:

    Input:
    s = "catsanddog"
    wordDict = ["cat", "cats", "and", "sand", "dog"]
    Output:
    [
    "cats and dog",
    "cat sand dog"
    ]
    Example 2:

    Input:
    s = "pineapplepenapple"
    wordDict = ["apple", "pen", "applepen", "pine", "pineapple"]
    Output:
    [
    "pine apple pen apple",
    "pineapple pen apple",
    "pine applepen apple"
    ]
    Explanation: Note that you are allowed to reuse a dictionary word.
    Example 3:

    Input:
    s = "catsandog"
    wordDict = ["cats", "dog", "sand", "and", "cat"]
    Output:
    []
    */
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> result = new ArrayList<String>();
        if (s == null || s.length() == 0) {
            return result;
        }
        Set<String> set = new HashSet<String>(wordDict);
        return wordBreak(s, set, new HashMap<String, List<String>>());
    }

    private List<String> wordBreak(String s, Set<String> wordDict, Map<String, List<String>> map) {
        // To reduce the time when s already iterated
        if (map.containsKey(s)) {
            return map.get(s);
        }
        List<String> result = new ArrayList<String>();
        // when s is the leading words like cats or cat, it will be in the list:
        // It cannot return because if s = pineapple, and wordDict has pine, apple, it will need to continue to scan:
        if (wordDict.contains(s)) {
            result.add(s);
        }
        int n = s.length();
        for (int i = n-1; i >= 1; i--) {
            String currStr = s.substring(i);
            if (wordDict.contains(currStr)) {
                String prevStr = s.substring(0, i);
                List<String> strs = wordBreak(prevStr, wordDict, map);
                for (String str : strs) {
                    StringBuffer sb = new StringBuffer(str);
                    sb.append(" ");
                    sb.append(currStr);
                    result.add(sb.toString());
                }
            }
        }
        map.put(s, result);
        return result;
    }

    // Solution 2, by iterating wordDict:
    private List<String> wordBreak(String s, Set<String> wordDict, Map<String, List<String>> map) {
        if (map.containsKey(s)) {
            return map.get(s);
        }
        List<String> result = new ArrayList<String>();
        if (wordDict.contains(s)) {
            result.add(s);
        }
        for (String word : wordDict) {
            if (s.startsWith(word)) {
                String substr = s.substring(word.length());
                List<String> strs = wordBreak(substr, wordDict, map);
                for (String str : strs) {
                    StringBuffer sb = new StringBuffer(word);
                    sb.append(" ");
                    sb.append(str);
                    result.add(sb.toString());
                }
            }
        }
        map.put(s, result);
        return result;
    }

    // Leetcode 494: Target Sum
    // https://leetcode.com/problems/target-sum/
    /**
    You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.

    Find out how many ways to assign symbols to make sum of integers equal to target S.

    Example 1:
    Input: nums is [1, 1, 1, 1, 1], S is 3.
    Output: 5
    Explanation:

    -1+1+1+1+1 = 3
    +1-1+1+1+1 = 3
    +1+1-1+1+1 = 3
    +1+1+1-1+1 = 3
    +1+1+1+1-1 = 3

    There are 5 ways to assign symbols to make the sum of nums be target 3.
    */
    // BackTracking Solution:
    public int findTargetSumWays(int[] nums, int S) {
        return findTargetSum(nums, S, 0);
    }

    private int findTargetSum(int[] nums, int sum, int level) {
        if (level == nums.length) {
            if (sum == 0) {
                return 1;
            }
            else {
                return 0;
            }
        }
        int addVal = findTargetSum(nums, sum + nums[level], level+1);
        int minusVal = findTargetSum(nums, sum - nums[level], level+1);
        return addVal + minusVal;
    }

    // backtracking with memorization:
    public int findTargetSumWays(int[] nums, int S) {
        return findTargetSum(nums, S, 0, new HashMap<String, Integer>());
    }

    private int findTargetSum(int[] nums, int sum, int level, Map<String, Integer> map) {
        String path = level + "->" + sum;
        if (map.containsKey(path)) {
            return map.get(path);
        }
        if (level == nums.length) {
            if (sum == 0) {
                return 1;
            }
            else {
                return 0;
            }
        }
        int addVal = findTargetSum(nums, sum + nums[level], level+1, map);
        int minusVal = findTargetSum(nums, sum - nums[level], level+1, map);
        int val = addVal + minusVal;
        map.put(path, val);
        return val;
    }
    // Leetcode 282 Expression Add Operator:
    /**
    Given a string that contains only digits 0-9 and a target value, return all possibilities to add binary operators (not unary) +, -, or * between the digits so they evaluate to the target value.

    Example 1:

    Input: num = "123", target = 6
    Output: ["1+2+3", "1*2*3"]
    Example 2:

    Input: num = "232", target = 8
    Output: ["2*3+2", "2+3*2"]
    Example 3:

    Input: num = "105", target = 5
    Output: ["1*0+5","10-5"]
    Example 4:

    Input: num = "00", target = 0
    Output: ["0+0", "0-0", "0*0"]
    Example 5:

    Input: num = "3456237490", target = 9191
    Output: []
    */
    public List<String> addOperators(String num, int target) {
        List<String> result = new ArrayList<String>();
        addOperators(result, new StringBuffer(), num, target, 0, 0, 0);
        return result;
    }

    private void addOperators(List<String> result, StringBuffer sb, String s, int target, int level, long sum, long prevNum) {
        int n = s.length();
        if (level == n) {
            if (target == sum) {
                result.add(sb.toString());
            }
            return;
        }
        long num = 0;
        for (int i = level; i < n; i++) {
            char c = s.charAt(i);
            // stop when leading zero found
            // to skip case like: "105", target = 5
            num = num * 10 + (c - '0');
            if (i > level && num < 10) {
                break;
            }
            int prevLen = sb.length();
            if (level == 0) {
                sb.append(num);
                addOperators(result, sb, s, target, i+1, sum + num, num);
                sb.setLength(prevLen);
            }
            else {
                // Add
                sb.append("+").append(num);
                addOperators(result, sb, s, target, i+1, sum + num, num);
                sb.setLength(prevLen);

                // Minus
                sb.append("-").append(num);
                addOperators(result, sb, s, target, i+1, sum - num, -num);
                sb.setLength(prevLen);

                // Multiply
                sb.append("*").append(num);
                addOperators(result, sb, s, target, i+1, sum - prevNum + prevNum * num, prevNum * num);
                sb.setLength(prevLen);
            }
        }
    }

    // Leetcode 320: Generalized Abbreviation:
    // https://leetcode.com/problems/generalized-abbreviation
    /**
    Write a function to generate the generalized abbreviations of a word.

    Note: The order of the output does not matter.

    Example:

    Input: "word"
    Output:
    ["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
    */
    // Running Time Complexity: O(2^N), Space Complexity: O(N)
    public List<String> generateAbbreviations(String word) {
        List<String> result = new ArrayList<String>();
        if (word == null || word.length() == 0) {
            return result;
        }
        generateAbbreviations(result, word, new StringBuffer(), 0, 0);
        return result;
    }

    private void generateAbbreviations(List<String> result, String word, StringBuffer sb, int count, int level) {
        if (level == word.length()) {
            if (count > 0) {
                sb.append(count);
            }
            result.add(sb.toString());
            return;
        }
        int prevLen = sb.length();
        generateAbbreviations(result, word, sb, count+1, level+1);
        sb.setLength(prevLen);

        if (count > 0) {
            sb.append(count);
        }
        sb.append(word.charAt(level));
        generateAbbreviations(result, word, sb, 0, level + 1);
        sb.setLength(prevLen);
    }

    // Leetcode 301 Remove Invalid Parenthesis:
    // https://leetcode.com/problems/remove-invalid-parentheses/
    /**
    Remove the minimum number of invalid parentheses in order to make the input string valid. Return all possible results.

    Note: The input string may contain letters other than the parentheses ( and ).

    Example 1:

    Input: "()())()"
    Output: ["()()()", "(())()"]
    Example 2:

    Input: "(a)())()"
    Output: ["(a)()()", "(a())()"]
    Example 3:

    Input: ")("
    Output: [""]
    */
    public List<String> removeInvalidParentheses(String s) {
        Set<String> result = new HashSet<String>();
        int l = 0, r = 0, n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                l++;
            }
            else if (c == ')') {
                if (l != 0) {
                    l--;
                }
                else {
                    r++;
                }
            }
        }
        dfs(result, s, n, l, r, 0, 0, new StringBuffer());
        return new ArrayList<String>(result);
    }
    /**
    * For either '(' or ')', there are two options, add it or not add it.
    * We use l and r to record which side of parthensis is extra, and use 'open' to keep track of number of parenthesis
    */
    private void dfs(Set<String> result, String s, int n, int l, int r, int level, int open, StringBuffer sb) {
        if (level == n && l == 0 && r == 0 && open == 0) {
            result.add(sb.toString());
            return;
        }
        if (level == n || l < 0 || r < 0 || open < 0) {
            return;
        }
        char c = s.charAt(level);
        int prevLen = sb.length();
        if (c == '(') {
            // not use (
            dfs(result, s, n, l-1, r, level+1, open, sb);
            sb.append(c);
            // use (
            dfs(result, s, n, l, r, level+1, open+1, sb);
        }
        else if (c == ')') {
            // not use )
            dfs(result, s, n, l, r-1, level+1, open, sb);
            sb.append(c);
            // use (
            dfs(result, s, n, l, r, level+1, open-1, sb);
        }
        else {
            sb.append(c);
            dfs(result, s, n, l, r, level+1, open, sb);
        }
        sb.setLength(prevLen);
    }
}