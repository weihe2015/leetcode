public class Solution {
    // Leetcode 50: Pow(x, n)
    // https://leetcode.com/problems/powx-n/
    /**
    Implement pow(x, n), which calculates x raised to the power n (xn).
    Example 1:

    Input: 2.00000, 10
    Output: 1024.00000
    Example 2:

    Input: 2.10000, 3
    Output: 9.26100
    Example 3:

    Input: 2.00000, -2
    Output: 0.25000
    Explanation: 2-2 = 1/22 = 1/4 = 0.25
    Note:

    -100.0 < x < 100.0
    n is a 32-bit signed integer, within the range [−231, 231 − 1]
     */
    /**
        Break n into binary form:
        9 = 2^3 + 2^0
        3^9 = 3^(2^3) * 3^(2^0)
        when absN & 1 == 1, which means there is a odd number, we need to make res = absN * x;
    */
    // Running Time Complexity: O(log(N)), Space Complexity: O(1)
    public double myPow(double x, int n) {
        double res = 1;
        // make N to be long variable to handle N = Integer.MIN_VALUE;
        long absN = Math.abs((long) n);
        while (absN > 0) {
            long temp = absN & 1;
            if (temp == 1) {
                res *= x;
            }
            x *= x;
            absN >>= 1;
        }
        return n < 0 ? 1/res : res;
    }

    // Leetcode 069: Sqrt(x)
    // https://leetcode.com/problems/sqrtx/
    /**
    Implement int sqrt(int x).

    Compute and return the square root of x, where x is guaranteed to be a non-negative integer.

    Since the return type is an integer, the decimal digits are truncated and only the integer part of the result is returned.

    Example 1:

    Input: 4
    Output: 2
    Example 2:

    Input: 8
    Output: 2
    Explanation: The square root of 8 is 2.82842..., and since
                the decimal part is truncated, 2 is returned.
     */
    // Running Time Complexity: O(log(x))
    public int mySqrt(int x) {
        if (x == 0) {
            return x;
        }
        int low = 1, high = x, res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (mid <= x/mid) {
                res = mid;
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }
    // Use Newtone's method:
    // Newton method
    /**
    f(x) = x^2 - a, x = sqrt(a)
    f'(x) = 2x

    x_n+1 = x_n - f(x_n)/f'(x_n)
          = x_n - ((x_n)^2 - a) / 2 * x_n
          = (x_n + (a/x_n))/2;
    r = (r + x/r) / 2;
    */
    public int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }
        // Use long to avoid overflow.
        long r = x;
        while (r > x/r) {
            r = (r + x/r) / 2;
        }
        return (int)r;
    }
}