/**
Design and implement a data structure for Least Frequently Used (LFU) cache. It should support the following operations: get and put.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present. When the cache reaches its capacity, it should invalidate the least frequently used item before inserting a new item. For the purpose of this problem, when there is a tie (i.e., two or more keys that have the same frequency), the least recently used key would be evicted.

Follow up:
Could you do both operations in O(1) time complexity?

Example:

LFUCache cache = new LFUCache( 2 -- capacity  );

cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // returns 1
cache.put(3, 3);    // evicts key 2
cache.get(2);       // returns -1 (not found)
cache.get(3);       // returns 3.
cache.put(4, 4);    // evicts key 1.
cache.get(1);       // returns -1 (not found)
cache.get(3);       // returns 3
cache.get(4);       // returns 4
*/
/**
    Solution of using PriorityQueue minHeap as Data Structure:
    Use a Node structure containing key, value, number of frequency, and created time stamp.
        So in PriorityQueue, if two nodes have same the frequency, then we compare their timestamp and rank higher for the one with earlier time stamp
    
    The same as LRU: Least Recently Use Cache:
    GET:
        If this key is not in the map, 
            we return -1 as not found.
        If this key is in the map:
            We get this node from map, remove it from minHeap, update its frequency by increment 1 and update its timestap to current timestamp.
              Increment the global timestamp
    PUT:
        If this key is not in the map:
            If we still have capacity:
                decrement the global variable capacity by 1, create a new node with new key, value, freq = 1, and current timestamp, put it into map and minHeap
            If we don't have capacity:
                Pop the top Node from minHeap. This node will have least frequency number and earliest timestamp
                    create a new node with new key, value, freq = 1, and current timestamp, put it into map and minHeap
        If this key is in the map:
            Get it from map by key, remove it from minHeap.
                update its value to current value, update its frequency by incrementing 1, and update timestap to current timestamp.
             Then add it back to minHeap and HashMap.

   PriorityQueue Solution: Running Time Complexity: GET O(logN), PUT: O(logN)
*/
class LFUCache {
    class Node {
        long stamp;
        int key, value;
        int freq;
        public Node(int key, int value, int freq, long stamp) {
            this.key = key;
            this.value = value;
            this.freq = freq;
            this.stamp = stamp;
        }
    }
    Map<Integer, Node> map;
    long stamp;
    int capacity;
    Queue<Node> minHeap;
    public LFUCache(int capacity) {
        this.capacity = capacity;
        this.stamp = 0;
        this.map = new HashMap<Integer, Node>();
        this.minHeap = new PriorityQueue<Node>((o1, o2) -> (o1.freq == o2.freq ? (int)(o1.stamp - o2.stamp) : (o1.freq - o2.freq)));
    }
    
    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        else {
            Node node = map.get(key);
            minHeap.remove(node);
            int val = node.value;
            // update the time stamp and its frequency
            node.freq++;
            node.stamp = stamp;

            // update the node in map:
            map.put(key, node);
            // update the node in minHeap:
            minHeap.offer(node);
            stamp++;
            return val;
        }
    }
    
    public void put(int key, int value) {
        Node node = null;
        // If this node is not in the map, we create a new one.
        if (!map.containsKey(key)) {
            if (capacity > 0) {
                capacity--;
            }
            else {
                if (minHeap.isEmpty()) {
                    return;
                }
                Node prevNode = minHeap.poll();
                map.remove(prevNode.key);
            }
            node = new Node(key, value, 1, stamp);
        }
        // If this node is already in the map, we update the value, freq and stamp:
        else {
            node = map.get(key);
            minHeap.remove(node);
            node.freq++;
            node.stamp = stamp;
            node.value = value;
        }
        stamp++;
        // Add it to the minHeap:
        minHeap.offer(node);
        // Add it to the map
        map.put(key, node);
    }
}

/** 
    Optimial Solution with Two HashMap, and DoubleLinkedList as Solution: PUT: O(1), GET: O(1)
    Idea:
        Use two HashMap, one stores the key and the Node, the other stores the frequency as key and double linked List as value:
        DoubleLinkedList:
            Head: the most recently used. Tail: The least Recently Used.

        PUT:
            If the key is not in the nodeMap, we check whether it reaches the capacity or not.
                If the cache is full:
                    we use the minFreq global variable to get the double linked list, remove the last node from this list, 
                        and remove it from the nodeMap.
                    Then we create a new Node, with new key and new value, add it to new frequency double linked list, and 
                        add it into freqMap. In the end, we update the nodeMap with new key and new Node
                If the cache is not full:
                    We increment the size, create a new Node, with new key and new value, 
                        add it to new frequency double linked list, and 
                        add it into freqMap. In the end, we update the nodeMap with new key and new Node
            If the key is in the nodeMap:
                We get the node from nodeMap, update its value and do following update:
                    1.1: use its old freq to get the double linked list from freqMap.
                            Remove itself from that list
                            At this time, if its old freq is the same as minFreq, while this double linked list has no more nodes,
                            We know there is no more node with this minFreq. We increment the minFreq by 1
                        Then we increment its freq by 1, add it to new frequency double linked list, and 
                        add it into freqMap. In the end, we update the nodeMap with new key and new Node
                    
        GET:
            If the key is not in the nodeMap, return -1
            If the key is in the nodeMap:
                We get the node from nodeMap, update the node with following:
                    2.1: use its old freq to get the double linked list from freqMap.
                            Remove itself from that list
                            At this time, if its old freq is the same as minFreq, while this double linked list has no more nodes,
                            We know there is no more node with this minFreq. We increment the minFreq by 1
                        Then we increment its freq by 1, add it to new frequency double linked list, and 
                        add it into freqMap. In the end, we update the nodeMap with new key and new Node
                return node's value.

                 Increasing frequencies
              ----------------------------->

+--------+     +---+      +---+    +---+
| HashMap|---- | 1 |------| 5 |----| 9 |  Frequencies
+--------+     +-+-+      +-+-+    +-+-+
                  |         |        |
                +--+-+    +-+-+    +-+-+
                |Head|   |Head|    |Head|
                +----+    +-+-+    +-+-+
                | |       | |        |        ^
                +-+-+     +-+-+    +-+-+      |
                |2,3|     |4,3|    |6,2|      |
                +-+-+     +-+-+    +-+-+      | Most recent 
                |  |      |  |      | |       |
                +-+--+    +-+-+    +-+-+      | 
key,value,freq  |Tail|    |1,2|    |7,9|      |
                +-+--+    +---+    +---+     
                           | |      |  |
                          +-+-+    +-+-+
                          |Tail|   |Tail|
                          +-+-+    +-+-+                    


        https://leetcode.com/problems/lfu-cache/discuss/94602/C++-list-with-hashmap-with-explanation
*/
class LFUCache {
    class Node {
        Node prev;
        Node next;
        int key;
        int value;
        int freq;
        public Node(){ }
        public Node(int key, int value) {
            this.key = key;
            this.value = value;
            this.freq = 1;
        }
    }
    
    class DoubleLinkedList {
        Node head, tail;
        int size;
        public DoubleLinkedList() {
            head = new Node();
            tail = new Node();
            head.next = tail;
            tail.prev = head;
        }
        
        public void removeNode(Node node) {
            node.prev.next = node.next;
            node.next.prev = node.prev;
            resetPtrs(node);
            size--;
        }
        
        public Node removeLastNode(){
            Node lastNode = tail.prev;
            removeNode(lastNode);
            return lastNode;
        }
        
        public void addNode(Node node) {
            head.next.prev = node;
            node.prev = head;
            node.next = head.next;
            head.next = node;
            size++;
        }
        
        private void resetPtrs(Node node) {
            node.prev = null;
            node.next = null;
        }
    }
    
    int size, capacity, minFreq;
    Map<Integer, Node> map;
    Map<Integer, DoubleLinkedList> freqMap;
    
    public LFUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<Integer, Node>();
        this.freqMap = new HashMap<Integer, DoubleLinkedList>();
    }
    
    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        else {
            Node node = map.get(key);
            updateNode(node);
            return node.value;
        }
    }
    
    public void put(int key, int value) {
        if (capacity == 0) {
            return;
        }
        if (!map.containsKey(key)) {
            // The cache is not full yet, increment the size;
            if (size < capacity) {
                size++;
            }
            // The cache is full, we need to evict the least frequently used item:
            else {
                DoubleLinkedList oldList = freqMap.get(minFreq);
                Node oldNode = oldList.removeLastNode();
                map.remove(oldNode.key);
            }
            // reset the minFreq as 1
            minFreq = 1;
            Node newNode = new Node(key, value);
            updateFreqMap(newNode);
            map.put(key, newNode);
        }
        else {
            // update the freq of the node:
            Node node = map.get(key);
            node.value = value;
            updateNode(node);
        }
    }
    /**
    This function helps to remove node from previous frequency double linked list,
    After incrementing its frequency, this node will be added in double linked list with new frequency
    */
    private void updateNode(Node node) {
        DoubleLinkedList oldList = freqMap.get(node.freq);
        oldList.removeNode(node);
        if (oldList.size == 0 && node.freq == minFreq) {
            minFreq++;
        }
        node.freq++;
        updateFreqMap(node);
        map.put(node.key, node);
    }
    
    private void updateFreqMap(Node node) {
        DoubleLinkedList newList = freqMap.getOrDefault(node.freq, new DoubleLinkedList());
        newList.addNode(node);
        freqMap.put(node.freq, newList);
    }
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache obj = new LFUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */