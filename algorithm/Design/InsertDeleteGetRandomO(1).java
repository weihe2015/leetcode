/**
Design a data structure that supports all following operations in average O(1) time.

Note: Duplicate elements are allowed.
insert(val): Inserts an item val to the collection.
remove(val): Removes an item val from the collection if present.
getRandom: Returns a random element from current collection of elements. The probability of each element being returned is linearly related to the number of same value the collection contains.
Example:

// Init an empty collection.
RandomizedCollection collection = new RandomizedCollection();

// Inserts 1 to the collection. Returns true as the collection did not contain 1.
collection.insert(1);

// Inserts another 1 to the collection. Returns false as the collection contained 1. Collection now contains [1,1].
collection.insert(1);

// Inserts 2 to the collection, returns true. Collection now contains [1,1,2].
collection.insert(2);

// getRandom should return 1 with the probability 2/3, and returns 2 with the probability 1/3.
collection.getRandom();

// Removes 1 from the collection, returns true. Collection now contains [1,2].
collection.remove(1);

// getRandom should return 1 and 2 both equally likely.
collection.getRandom();
*/
/**
 * If there is no duplicate, use ArrayList + HashMap, where key => num and val => index of num in ArrayList
 * If there is duplicates, use ArrayList + HashMap<Integer, LinkedHashSet<Integer>> where key => num and value => List of indexes of nums in ArrayList.
 *     why LinkedHashSet not hashSet? Because set does not have original inserted order, and iterator().next() is not O(1).
 *          LinkedHashSet has original inserted order and interator().next() of O(1)
 *           It maintains a doubly-linked list running through all of its entries
 */
import java.util.Random;
class RandomizedCollection {
    // List of nums
    ArrayList<Integer> nums;
    // key -> num, val: LinkedHashSet stores list of index of num in List of nums:
    Map<Integer, Set<Integer>> map;
    Random random;
    /** Initialize your data structure here. */
    public RandomizedCollection() {
        nums = new ArrayList<Integer>();
        map = new HashMap<>();
        random = new Random();
    }
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    public boolean insert(int val) {
        nums.add(val);
        if (!map.containsKey(val)) {
            Set<Integer> set = new LinkedHashSet<>();
            set.add(nums.size()-1);
            map.put(val, set);
            return false;
        }
        else {
            Set<Integer> set = map.get(val);
            set.add(nums.size()-1);
            map.put(val, set);
            return true;
        }
    }
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    public boolean remove(int val) {
        if (!map.containsKey(val)) {
            return false;
        }
        int idx = map.get(val).iterator().next();
        map.get(val).remove(idx);
        // If this item to be removed is not the last item on the list, swap with the last item.
        int lastIdx = nums.size() - 1;
        if (idx < lastIdx) {
            int lastVal = nums.get(lastIdx);
            map.get(lastVal).remove(lastIdx);
            map.get(lastVal).add(idx);
            nums.set(idx, lastVal);
        }
        // remove the last item on the list:
        nums.remove(lastIdx);
        if (map.get(val).isEmpty()) {
            map.remove(val);
        }
        return true;
    }
    /** Get a random element from the collection. */
    public int getRandom() {
        int idx = random.nextInt(nums.size());
        return nums.get(idx);
    }
}