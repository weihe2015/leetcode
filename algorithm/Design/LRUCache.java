/**
Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

Follow up:
Could you do both operations in O(1) time complexity?

Example:

LRUCache cache = new LRUCache( 2 / capacity / );

cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // returns 1
cache.put(3, 3);    // evicts key 2
cache.get(2);       // returns -1 (not found)
cache.put(4, 4);    // evicts key 1
cache.get(1);       // returns -1 (not found)
cache.get(3);       // returns 3
cache.get(4);       // returns 4
*/
// https://www.youtube.com/watch?v=4wVp97-uqr0
// Leetcode 146 Least Recently Used Cached:
class LRUCache {
    class DoubleLinkedListNode {
        DoubleLinkedListNode prev, next;
        int key, val;
        public DoubleLinkedListNode() {
            
        }
        public DoubleLinkedListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }
    
    int capacity;
    // key: original key value from put(key, value), value: the DoubleLinkedListNode
    Map<Integer, DoubleLinkedListNode> map;
    DoubleLinkedListNode head, tail;
    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<Integer, DoubleLinkedListNode>();
        this.head = new DoubleLinkedListNode();
        this.tail = new DoubleLinkedListNode();
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        else {
            DoubleLinkedListNode node = map.get(key);
            int val = node.val;
            // If the first item is not this key: we pop it to the top of the list:
            if (head.next.key != key) {
                // remove itself:
                removeNodeItself(node);
                addNodeToFront(node);
            }
            return val;
        }
    }

    public void put(int key, int value) {
        DoubleLinkedListNode node = null;
        if (!map.containsKey(key)) {
            if (capacity > 0) {
                node = new DoubleLinkedListNode(key, value);
                capacity--;
            }
            else {
                // Remove the tail node from the list
                removeTailNode();
                node = new DoubleLinkedListNode(key, value);
            }
        }
        else {
            // When this item is in the map, we update this item in the order list
            // make this key on the top of the order list queue:
        	node = map.get(key);
            // update the node value:
            node.val = value;
            // remove itself:
            removeNodeItself(node);
        }
        // add itself to the header:
        addNodeToFront(node);
        map.put(key, node);
    }

    private void removeTailNode() {
        DoubleLinkedListNode lastNode = tail.prev;
        lastNode.prev.next = tail;
        lastNode.next = null;
        tail.prev = lastNode.prev;
        lastNode.prev = null;
        // Remove this key from HashMap
        int prevKey = lastNode.key;
        map.remove(prevKey);
    }

    private void removeNodeItself(DoubleLinkedListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    private void addNodeToFront(DoubleLinkedListNode node) {
        // add itself to the header:
        DoubleLinkedListNode prevItem = head.next;
        node.next = prevItem;
        node.prev = head;
        prevItem.prev = node;
        head.next = node;
    }
}