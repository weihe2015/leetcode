public class BinaryTree {
    /**
    Concepts:
    * Full Binary Tree: Every node has 0 or 2 children:
               18
           /       \  
         15         30  
        /  \        /  \
      40    50    100   40

             18
           /    \   
         15     20    
        /  \       
      40    50   
    /   \
   30   50
               18
            /     \  
          40       30  
                   /  \
                 100   40

    * Complete Binary Tree: All levels are completely filled except possibly the last level and 
      the last level has all keys as left as possible
      完全二叉树从根结点到倒数第二层满足完美二叉树，最后一层可以不完全填充，其叶子结点都靠左对齐。
      Index of last leaf: n-1
      Index of parent of this last leaf: (n-1-1) / 2 = n/2 - 1
      如果节点 x 存储在数组中下标为 i 的位置，下标为 2*i 的位置就是左子节点，下标为 2*i+1 就是右子节点。反过来 ，下标为 i/2 的位置存储的就是它的父节点。

               18
           /       \  
         15         30  
        /  \        /  \
      40    50    100   40


               18
           /       \  
         15         30  
        /  \        /  \
      40    50    100   40
     /  \   /
    8   7  9 

    * Perfect Binary Tree: All internal nodes have two children and all leaf nodes are at the same level:
               18
           /       \  
         15         30  
        /  \        /  \
      40    50    100   40


               18
           /       \  
         15         30  

    Level i has 2^(i-1) nodes.
    Depth = k, the number of nodes is 2^(k) - 1
    */


    // Leetcode 94, Binary Tree Inorder Traversal:
    // https://leetcode.com/problems/binary-tree-inorder-traversal/
    // Recursive:
    // Running Time Complexity: T(N) = 2T(N/2) + O(1) = O(N)
    // Space Complexity: O(logN), depth of Tree. Worse Case: O(N), when Tree is linear
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        inorderTraversal(result, root);
        return result;
    }

    private void inorderTraversal(List<Integer> result, TreeNode node) {
        if (node == null) {
            return;
        }
        inorderTraversal(result, node.left);
        result.add(node.val);
        inorderTraversal(result, node.right);
    }

    // Iterative:
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode currNode = root;
        while (!stack.isEmpty() || currNode != null) {
            while (currNode != null) {
                stack.push(currNode);
                currNode = node.left;
            }
            currNode = stack.pop();
            result.add(currNode.val);
            currNode = currNode.right;
            // Cannot be if (currNode.right != null) {currNode = currNode.right}
            // Because in this case, in next round, currNode stil be itself, add be added into stack and pop() it back again.
            // Become an infinite loop
        }
        return result;
    }

    // Leetcode 144: Binary Tree Preorder Traversal
    // https://leetcode.com/problems/binary-tree-preorder-traversal
    // Recursive:
    // Running Time Complexity: T(N) = 2T(N/2) + O(1) = O(N)
    // Space Complexity: O(logN), depth of Tree. Worse Case: O(N), when Tree is linear
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        preorderTraversal(result, root);
        return result;
    }

    private void preorderTraversal(List<Integer> result, TreeNode node) {
        if (node == null) {
            return;
        }
        result.add(node.val);
        preorderTraversal(result, node.left);
        preorderTraversal(result, node.right);
    }

    // Iterative:
    /**
     * Preorder Traversal: currNode, left child, right child.
     * Basic Idea, Use stack to add root.
     *    while stack is not empty, pop the TreeNode as currNode, add its value into list;
     *    Add its right node to the stack, then add left node to the stack, because we wants to iterate left side first
     *
    */
    // Running Time Complexity: T(N) = 2T(N/2) + O(1) = O(N)
    // Space Complexity: O(logN), depth of Tree. Worse Case: O(N), when Tree is linear
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode currNode = stack.pop();
            result.add(currNode.val);
            if (currNode.right != null) {
                stack.push(currNode.right);
            }
            if (currNode.left != null) {
                stack.push(currNode.left);
            }
        }
        return result;
    }

    // Leetcode 145: Binary Tree Postorder Traversal:
    // https://leetcode.com/problems/binary-tree-postorder-traversal/
    // Recursive:
    // Running Time Complexity: T(N) = 2T(N/2) + O(1) = O(N)
    // Space Complexity: O(logN), depth of Tree. Worse Case: O(N), when Tree is linear
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        postorderTraversal(result, root);
        return result;
    }

    private void postorderTraversal(List<Integer> result, TreeNode node) {
        if (node == null) {
            return;
        }
        postorderTraversal(result, node.left);
        postorderTraversal(result, node.right);
        result.add(node.val);
    }

    // Iterative Solution:
    /**
     * Basic Idea: the same as Binary Tree Inorder Traversal,
     *  1. iterate until the end of left leaf and add those nodes into stack.
     *  2. Use peek to get currNode
     *  3. If current Node does not have right child, pop it out and add its value into list.
     *  4. Use prevNode to mark currNode whose value added into list, to avoid infinite loop
     *  5. Mark node as currNode.right if currNode has right child and this right child has not been visited. (prevNode != currNode.right)
    */
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = root;
        TreeNode prev = null;

        while (!stack.isEmpty() || curr != null) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.peek();
            // Check if currNode has right child, if not, pop it out and add the val into list
            if (curr.right != null && prev != curr.right) {
                curr = curr.right;
            }
            else {
                // add currNode into list and mark prevNode as currNode;
                curr = stack.pop();
                res.add(curr.val);
                prev = curr;
            }
        }
        return res;
    }

    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            result.add(curr.val);
            if (curr.left != null) {
                stack.push(curr.left);
            }
            if (curr.right != null) {
                stack.push(curr.right);
            }
        }
        Collections.reverse(result);
        return result;
    }

    // Leetcode 102: Binary Tree Level order Traversal: from top to bottom
    // https://leetcode.com/problems/binary-tree-level-order-traversal/
    /**
        Given a binary tree, return the level order traversal of its nodes' values.
        (ie, from left to right, level by level).
    */
    // Recursive:
    // Running Time Complexity: T(N) = 2T(N/2) + O(1) = O(N)
    // Space Complexity: O(logN), depth of Tree. Worse Case: O(N), when Tree is linear
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrder(result, root, 0);
        return result;
    }

    private void levelOrder(List<List<Integer>> result, TreeNode node, int level) {
        if (node == null) {
            return;
        }
        // if result.get(level) == null
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);
        list.add(node.val);
        levelOrder(result, node.left, level+1);
        levelOrder(result, node.right, level+1);
    }

    // Iterative:
    /**
     * Basic Idea:
     *    1. use queue to store all nodes in the same level.
     *    2. Iterate all nodes in the level with size = queue.size() times.
     */
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<>();
            int level = queue.size();
            for (int s = 1; s <= level; s++) {
                TreeNode curr = queue.poll();
                list.add(curr.val);
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
            res.add(list);
        }
        return res;
    }

    // Leetcode 107 Binary Tree Level Order Traversal II:
    // https://leetcode.com/problems/binary-tree-level-order-traversal-ii/
    /**
    Given a binary tree, return the bottom-up level order traversal of its nodes' values.
    (ie, from left to right, level by level from leaf to root).
    */
    // Iterative:
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<>();
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                list.add(currNode.val);
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
            }
            // Only Difference with Leetcode 102:
            result.add(0, list);
        }
        return result;
    }
    // Recursive:
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrderBottom(result, root, 0);
        // Only Difference with Leetcode 102:
        Collections.reverse(result);
        return result;
    }

    private void levelOrderBottom(List<List<Integer>> result, TreeNode node, int level) {
        if (node == null) {
            return;
        }
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);
        list.add(node.val);
        levelOrderBottom(result, node.left, level+1);
        levelOrderBottom(result, node.right, level+1);
    }

    // Leetcode 103: Binary Tree ZigZag Level Order Traversal:
    // https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal
    // Recursion
    // Running Time Complexity: T(N) = 2T(N/2) + O(1) = O(N)
    // Space Complexity: O(logN), depth of Tree. Worse Case: O(N), when Tree is linear
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        zigzagLevelOrder(result, root, 0);
        return result;
    }

    private void zigzagLevelOrder(List<List<Integer>> result, TreeNode node, int level) {
        if (node == null) {
            return;
        }
        if (result.size() == level) {
            result.add(new ArrayList<>());
        }
        List<Integer> list = result.get(level);
        // if level is odd: add item to the beginning of the list, to reverse the list
        if (level % 2 != 0) {
            list.add(0, node.val);
        }
        else {
            list.add(node.val);
        }
        zigzagLevelOrder(result, node.left, level+1);
        zigzagLevelOrder(result, node.right, level+1);
    }

    // Iterative
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        int level = 0;
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<Integer>();
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                list.add(currNode.val);
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
            }
            // Reverse the odd level list
            if (level % 2 != 0) {
                Collections.reverse(list);
            }
            result.add(list);
            level++;
        }
        return result;
    }

    // Leetcode 429: N-ary Tree Level Order
    // https://leetcode.com/problems/n-ary-tree-level-order-traversal/
    /**
    Given an n-ary tree, return the level order traversal of its nodes' values.
    Nary-Tree input serialization is represented in their level order traversal,
    each group of children is separated by the null value (See examples).
    */
    // Iterative
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> list = new ArrayList<>();
            for (int i = 1; i <= size; i++) {
                Node curr = queue.poll();
                list.add(curr.val);
                for (Node child : curr.children) {
                    queue.offer(child);
                }
            }
            result.add(list);
        }
        return result;
    }

    // Recursive:
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrder(result, root, 0);
        return result;
    }

    private void levelOrder(List<List<Integer>> result, Node node, int level) {
        if (node == null) {
            return;
        }
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);
        list.add(node.val);
        for (Node childNode : node.children) {
            levelOrder(result, childNode, level+1);
        }
    }

    // Leetcode 589: N-ary Preorder Traversal:
    // https://leetcode.com/problems/n-ary-tree-preorder-traversal/
    // Given an n-ary tree, return the preorder traversal of its nodes' values.
    // Recursive:
    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<>();
        preorder(result, root);
        return result;
    }

    private void preorder(List<Integer> result, Node node) {
        if (node == null) {
            return;
        }
        result.add(node.val);
        for (Node child : node.children) {
            preorder(result, child);
        }
    }
    // Iterative:
    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node curr = stack.pop();
            result.add(curr.val);
            for (int max = curr.children.size(), i = max-1; i >= 0; i--) {
                stack.push(curr.children.get(i));
            }
        }
        return result;
    }

    // Leetcode 590:  N-ary Tree Postorder Traversal
    // https://leetcode.com/problems/n-ary-tree-postorder-traversal/
    // Given an n-ary tree, return the postorder traversal of its nodes' values.
    // Recursive:
    public List<Integer> postorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        postorder(result, root);
        return result;
    }

    private void postorder(List<Integer> result, Node node) {
        if (node == null) {
            return;
        }
        for (Node childNode : node.children) {
            postorder(result, childNode);
        }
        result.add(node.val);
    }

    // Iterative:
    public List<Integer> postorder(Node root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node curr = stack.pop();
            result.add(curr.val);
            for (Node child : curr.children) {
                stack.push(child);
            }
        }
        Collections.reverse(result);
        return result;
    }

    // Leetcode 098: Validate Binary Tree:
    // https://leetcode.com/problems/validate-binary-search-tree/
    /**
    Given a binary tree, determine if it is a valid binary search tree (BST).

    Assume a BST is defined as follows:

    The left subtree of a node contains only nodes with keys less than the node's key.
    The right subtree of a node contains only nodes with keys greater than the node's key.
    Both the left and right subtrees must also be binary search trees.
    Example 1:

    2
   / \
  1   3

    Input: [2,1,3]
    Output: true
    Example 2:

    5
   / \
  1   4
     / \
    3   6

    Input: [5,1,4,null,null,3,6]
    Output: false
    Explanation: The root node's value is 5 but its right child's value is 4.
    */
    // Iterative: inorder traversal
    public boolean isValidBST(TreeNode root) {
        // Iterative: inorder traversal:
        if (root == null) {
            return true;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode node = root, prev = null;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode curr = stack.pop();
            if (prev != null && prev.val >= curr.val) {
                return false;
            }
            prev = curr;
            node = curr.right;
        }
        return true;
    }
    // Recursive:
    public boolean isValidBST(TreeNode root) {
        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }
    private boolean isValidBST(TreeNode node, long min, long max) {
        if (node == null) {
            return true;
        }
        if (node.val <= min || node.val >= max) {
            return false;
        }
        return isValidBST(node.left, min, node.val) && isValidBST(node.right, node.val, max);
    }

    // Leetcode 99: Recover Binary Search Tree
    // https://leetcode.com/problems/recover-binary-search-tree/
    /**
    Two elements of a binary search tree (BST) are swapped by mistake.
    Recover the tree without changing its structure.
    Example 1:

Input: [1,3,null,null,2]

   1
  /
 3
  \
   2

Output: [3,1,null,null,2]

   3
  /
 1
  \
   2
Example 2:

Input: [3,1,4,null,null,2]

  3
 / \
1   4
   /
  2

Output: [2,1,4,null,null,3]

  2
 / \
1   4
   /
  3
    */
    public void recoverTree(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode node = root, prev = null, node1 = null, node2 = null;
        while (!stack.isEmpty() || node != null)  {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode curr = stack.pop();
            // find the incorrect order nodes:
            if (prev != null && prev.val >= curr.val) {
                if (node1 == null) {
                    node1 = prev;
                }
                node2 = curr;
            }
            prev = curr;
            node = curr.right;
        }
        swap(node1, node2);
    }

    private void swap(TreeNode node1, TreeNode node2) {
        int temp = node1.val;
        node1.val = node2.val;
        node2.val = temp;
    }

    // Leetcode 100: Same Tree:
    // https://leetcode.com/problems/same-tree/
    /*
    Given two binary trees, write a function to check if they are the same or not.

    Two binary trees are considered the same if they are structurally identical and the nodes have the same value.
    Example 1:

Input:     1         1
          / \       / \
         2   3     2   3

        [1,2,3],   [1,2,3]

Output: true
Example 2:

Input:     1         1
          /           \
         2             2

        [1,2],     [1,null,2]

Output: false
Example 3:

Input:     1         1
          / \       / \
         2   1     1   2

        [1,2,1],   [1,1,2]

Output: false
    */
    // Recursive:
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }
        return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    // Iterative:
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }
        // Use preorder traversal:
        Stack<TreeNode> pStack = new Stack<>();
        Stack<TreeNode> qStack = new Stack<>();
        pStack.push(p);
        qStack.push(q);
        while (!pStack.isEmpty() && !qStack.isEmpty()) {
            TreeNode pNode = pStack.pop();
            TreeNode qNode = qStack.pop();
            if (pNode.val != qNode.val) {
                return false;
            }
            if (pNode.left != null && qNode.left != null) {
                pStack.push(pNode.left);
                qStack.push(qNode.left);
            }
            else if (pNode.left != null || qNode.left != null) {
                return false;
            }

            if (pNode.right != null && qNode.right != null) {
                pStack.push(pNode.right);
                qStack.push(qNode.right);
            }
            else if (pNode.right != null || qNode.right != null) {
                return false;
            }
        }
        return true;
    }

    // Leetcode 101 Symmetric Tree
    // https://leetcode.com/problems/symmetric-tree/
    /**
    Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3


But the following [1,2,2,null,3,null,3] is not:

    1
   / \
  2   2
   \   \
   3    3
    */
    // Recursively:
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.left, root.right);
    }

    private boolean isSymmetric(TreeNode leftNode, TreeNode rightNode) {
        if (leftNode == null && rightNode == null) {
            return true;
        }
        else if (leftNode == null || rightNode == null) {
            return false;
        }
        else if (leftNode.val != rightNode.val) {
            return false;
        }

        return isSymmetric(leftNode.left, rightNode.right) && isSymmetric(leftNode.right, rightNode.left);
    }

    // Iterative: use preorder traversal
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        else if (root.left == null && root.right == null) {
            return true;
        }
        else if (root.left == null || root.right == null) {
            return false;
        }
        Stack<TreeNode> leftStack = new Stack<>();
        Stack<TreeNode> rightStack = new Stack<>();
        leftStack.push(root.left);
        rightStack.push(root.right);

        while (!leftStack.isEmpty() && !rightStack.isEmpty()) {
            TreeNode leftNode = leftStack.pop();
            TreeNode rightNode = rightStack.pop();
            if (leftNode.val != rightNode.val) {
                return false;
            }
            if (leftNode.left != null && rightNode.right != null) {
                leftStack.push(leftNode.left);
                rightStack.push(rightNode.right);
            }
            else if (leftNode.left != null || rightNode.right != null) {
                return false;
            }

            if (leftNode.right != null && rightNode.left != null) {
                leftStack.push(leftNode.right);
                rightStack.push(rightNode.left);
            }
            else if (leftNode.right != null || rightNode.left != null) {
                return false;
            }
        }
        return true;
    }

    // Leetcode 104: Maximum Depth of Binary Tree:
    // https://leetcode.com/problems/maximum-depth-of-binary-tree/
    /**
    Given a binary tree, find its maximum depth.

    The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

    Note: A leaf is a node with no children.

    Example:

    Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
    return its depth = 3.
    */
    // recursive:
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }
    // Iterative:
    public int maxDepth(TreeNode root) {
        int maxDepth = 0;
        if (root == null) {
            return maxDepth;
        }
        // Use level order traversal:
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            maxDepth++;
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
        }
        return maxDepth;
    }

    // Iterative:
    public int minDepth(TreeNode root) {
        int depth = 0;
        if (root == null) {
            return depth;
        }
        // use level order traversal:
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            depth++;
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                // Once we meet a leaf, we return the height.
                if (curr.left == null && curr.right == null) {
                    return depth;
                }
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
        }
        // Should not reach here, since we return the result on line 65:
        return -1;
    }

    // Leetcode 105: Construct Binary Tree from Preorder and Inorder Traversal
    // https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
    /**
    Given preorder and inorder traversal of a tree, construct the binary tree.

    Note:
    You may assume that duplicates do not exist in the tree.

    For example, given

    preorder = [3,9,20,15,7]
    inorder = [9,3,15,20,7]
    Return the following binary tree:

    3
   / \
  9  20
    /  \
   15   7
    */
    /*
             root <--left--> <--right-->
    preorder: 50, 30, 10, 40, 70, 60, 90
            <---left-->  root <--right-->
    inorder: 10, 30, 40, 50, 60, 70, 90
    */
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return buildTree(preorder, 0, preorder.length-1, inorder, 0, inorder.length-1);
    }

    private TreeNode buildTree(int[] preorder, int preStart, int preEnd, int[] inorder, int inStart, int inEnd) {
        // firstly check if there is no element or only one element
        // then immediately return
        if (preStart > preEnd || inStart > inEnd) {
            return null;
        }
        int rootVal = preorder[preStart];
        TreeNode root = new TreeNode(rootVal);

        // Otherwise, we use our defined strategy, as first element in
        // preorder is the root and we find the value in in-order array
        // to determine how many 'left' elemnt in there and construct the left sub tree
        // Find the root index in inorder array
        int rootIdx = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }
         // calculate the left subtree size in inorder array:
        int leftTreeSize = rootIdx - inStart;
        root.left = buildTree(preorder, preStart+1, preStart + leftSubTreeSize, inorder, inStart, rootIdx-1);
        root.right = buildTree(preorder, preStart + leftSubTreeSize + 1, preEnd, inorder, rootIdx+1, inEnd);
        return root;
    }

    // Leetcode 106: Construct Binary Tree from Inorder and Postorder Traversal
    // https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/
    /**
    Given inorder and postorder traversal of a tree, construct the binary tree.

    Note:
    You may assume that duplicates do not exist in the tree.

    For example, given

    inorder = [9,3,15,20,7]
    postorder = [9,15,7,20,3]
    Return the following binary tree:

    3
   / \
  9  20
    /  \
   15   7

    */
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return buildTree(inorder, 0, inorder.length-1, postorder, 0, postorder.length-1);
    }

    private TreeNode buildTree(int[] inorder, int inStart, int inEnd, int[] postorder, int postStart, int postEnd) {
        if (inStart > inEnd || postStart > postEnd) {
            return null;
        }
        int rootVal = postorder[postEnd];
        TreeNode root = new TreeNode(rootVal);
        int rootIdx = -1;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }

        int leftTreeSize = rootIdx - inStart;
        root.left = buildTree(inorder, inStart, rootIdx-1, postorder, postStart, postStart+leftTreeSize-1);
        root.right = buildTree(inorder, rootIdx+1, inEnd, postorder, postStart+leftTreeSize, postEnd-1);
        return root;
    }

    // Leetcode xxx: Construct Binary Tree from Inorder and Level Order Traversal
    // https://www.geeksforgeeks.org/construct-tree-inorder-level-order-traversals
    // Running Time Complexity: O(N^3)
    public TreeNode buildTree(int[] inorder, int[] levelorder) {
        return buildTree(inorder, 0, inorder.length-1, levelorder);
    }

    private TreeNode buildTree(int[] inorder, int inStart, int inEnd, int[] levelorder) {
        if (inStart > inEnd) {
            return null;
        }
        int rootIdx = 0;
        boolean found = false;
        for (int num : levelOrder) {
            for (int i = inStart; i <= inEnd; i++) {
                if (inorder[i] == data) {
                    rootIdx = i;
                    found = true;
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        int rootVal = inorder[rootIdx];
        TreeNode root = new TreeNode(rootVal);
        root.left = buildTree(inorder, inStart, rootIdx-1, levelorder);
        root.right = buildTree(inorder, rootIdx+1, inEnd, levelorder);
        return root;
    }

    // Solution 2:
    public TreeNode buildTree(int[] inorder, int[] levelorder) {
        return buildTree(inorder, 0, inorder.length-1, levelorder);
    }

    private TreeNode buildTree(int[] inorder, int inStart, int inEnd, int[] levelorder) {
        if (inStart > inEnd) {
            return null;
        }
        // First node of level order is root:
        int rootVal = levelorder[0];
        TreeNode root = new TreeNode(rootVal);

        int rootIdx = -1;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }

        // Insert all left nodes in hash set:
        Set<Integer> set = new LinkedHashSet<>();
        for (int i = inStart; i < rootIdx; i++) {
            set.add(inorder[i]);
        }
        // Separate level order traversal of left and right subtrees.
        int[] lLevelOrder = new int[set.size()];
        int[] rLevelOrder = new int[inEnd-inStart-set.size()];
        int li = 0, ri = 0, n = inEnd - inStart + 1;
        for (int i = 1; i < n; i++) {
            if (set.contains(levelorder[i])) {
                lLevelOrder[li++] = levelorder[i];
            }
            else {
                rLevelOrder[ri++] = levelorder[i];
            }
        }

        root.left = buildTree(inorder, inStart, rootIdx-1, lLevelOrder);
        root.right = buildTree(inorder, rootIdx+1, inEnd, rLevelOrder);
        return root;
    }

    // Leetcode 112: Path Sum:
    // https://leetcode.com/problems/path-sum/
    /**
    Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.

    Note: A leaf is a node with no children.

    Example:

    Given the below binary tree and sum = 22,

      5
     / \
    4   8
   /   / \
  11  13  4
 /  \      \
7    2      1
    return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
    */
    // Recursive:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        int val = sum - root.val;
        if (root.left == null && root.right == null && val == 0) {
            return true;
        }
        return hasPathSum(root.left, val) || hasPathSum(root.right, val);
    }

    // Iterative: Use Level Order Traversal:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        Queue<Integer> sumQueue = new LinkedList<>();
        sumQueue.offer(sum-root.val);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                int res = sumQueue.poll();
                if (curr.left == null && curr.right == null && res == 0) {
                    return true;
                }
                if (curr.left != null) {
                    queue.offer(curr.left);
                    sumQueue.offer(res - curr.left.val);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                    sumQueue.offer(res - curr.right.val);
                }
            }
        }
        return false;
    }

    // Path Sum II:
    // https://leetcode.com/problems/path-sum-ii
    /*
    Given a binary tree and a sum, find all root-to-leaf paths where each paths sum equals the given sum.

    For example:
    Given the below binary tree and sum = 22,
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \    / \
        7    2  5   1
    return
    [
    [5,4,11,2],
    [5,8,4,5]
    ]
    */
    // Recursive:
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        List<Integer> list = new ArrayList<>();
        list.add(root.val);
        dfs(result, root, sum-root.val, list);
        return result;
    }

    private void dfs(List<List<Integer>> result, TreeNode node, int sum, List<Integer> list) {
        if (node.left == null && node.right == null && sum == 0) {
            result.add(new ArrayList<>(list));
            return;
        }
        if (node.left != null) {
            list.add(node.left.val);
            dfs(result, node.left, sum-node.left.val, list);
            list.remove(list.size()-1);
        }
        if (node.right != null) {
            list.add(node.right.val);
            dfs(result, node.right, sum-node.right.val, list);
            list.remove(list.size()-1);
        }
    }
    // Post order traversal
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        List<Integer> list = new ArrayList<Integer>();
        int tempSum = 0;
        TreeNode node = root, prevNode = null;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                tempSum += node.val;
                list.add(node.val);
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.peek();
            if (currNode.left == null && currNode.right == null && tempSum == sum) {
                result.add(new ArrayList<Integer>(list));
            }
            if (currNode.right != null && currNode.right != prevNode) {
                node = currNode.right;
            }
            else {
                currNode = stack.pop();
                prevNode = currNode;
                tempSum -= currNode.val;
                list.remove(list.size()-1);
            }
        }
        return result;
    }

    // Leetcode 108 Converted Sorted Array To Binary Tree:
    // https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
    /**
    Given an array where elements are sorted in ascending order, convert it to a height balanced BST.

    For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

    Example:

    Given the sorted array: [-10,-3,0,5,9],

    One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:

      0
     / \
   -3   9
   /   /
 -10  5
    */
    public TreeNode sortedArrayToBST(int[] nums) {
        return sortedArrayToBST(nums, 0, nums.length-1);
    }

    private TreeNode sortedArrayToBST(int[] nums, int low, int high) {
        if (low > high) {
            return null;
        }
        int mid = low + (high - low) / 2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = sortedArrayToBST(nums, low, mid-1);
        root.right = sortedArrayToBST(nums, mid+1, high);
        return root;
    }

    // Leetcode 116: Populating Next Right Pointers in Each Node
    /**
    You are given a perfect binary tree where all leaves are on the same level, and every parent has two children. The binary tree has the following definition:

    struct Node {
    int val;
    Node *left;
    Node *right;
    Node *next;
    }
    Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.

    Initially, all next pointers are set to NULL.

    Follow up:

    You may only use constant extra space.
    Recursive approach is fine, you may assume implicit stack space does not count as extra space for this problem.
    */


    // Leetcode 700: Search in a Binary Search Tree:
    // https://leetcode.com/problems/search-in-a-binary-search-tree
    /**
     * Given the root node of a binary search tree (BST) and a value.
     * You need to find the node in the BST that the node's value equals the given value.
     * Return the subtree rooted with that node.
     * If such node doesn't exist, you should return NULL.
        For example,
        Given the tree:
                4
            / \
            2   7
            / \
            1   3
        And the value to search: 2
        You should return this subtree:
            2
            / \
            1   3
        In the example above, if we want to search the value 5, since there is no node with value 5, we should return NULL.
        Note that an empty tree is represented by NULL,
        therefore you would see the expected output (serialized tree format) as [], not null.
    */
    // Recursive:
    // Running Time Complexity: O(logN) in average, O(N) in worse case
    // Space Complexity: O(logN) in average, O(N) in worse case, includes stack space.
    public TreeNode searchBST(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        if (root.val == val) {
            return root;
        }
        else if (val < root.val) {
            return searchBST(root.left, val);
        }
        else {
            return searchBST(root.right, val);
        }
    }
    // Iterative:
    public TreeNode searchBST(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        TreeNode curr = root;
        while (curr != null) {
            if (curr.val == val) {
                return curr;
            }
            else if (val < curr.val) {
                curr = curr.left;
            }
            else {
                curr = curr.right;
            }
        }
        return null;
    }

    // Insert into a Binary Search Tree:
    // Leetcode 701: https://leetcode.com/problems/insert-into-a-binary-search-tree/
    // Note: Given the root node of a binary search tree (BST) and a value to be inserted into the tree,
    // insert the value into the BST. Return the root node of the BST after the insertion. It is guaranteed that the new value does not exist in the original BST.
    // Note that there may exist multiple valid ways for the insertion, as long as the tree remains a BST after insertion
    // Recursive:
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        else if (val < root.val) {
            root.left = insertIntoBST(root.left, val);
        }
        else {
            root.right = insertIntoBST(root.right, val);
        }
        return root;
    }
    // Iterative:
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        TreeNode curr = root;
        while (curr != null) {
            /* If current node has no left child and the node inserting is less than current node,
				this node will be inserted as the left child of current node */
            if (val < curr.val) {
                if (curr.left == null) {
                    curr.left = new TreeNode(val);
                    break;
                }
                /* If current node has left child, keep searching in its left subtree. */
                else {
                    curr = curr.left;
                }
            }
            /* If current node has no right child and the node inserting is larger than current node
			This node will be inserted as the right child of current node */
            else if (val > curr.val) {
                if (curr.right == null) {
                    curr.right = new TreeNode(val);
                    break;
                }
                /* If current node has right child, keep finding in its right subtree. */
                else {
                    curr = curr.right;
                }
            }
            else {
                // this node's value exist in the BST, which does not satisfy the requirement.
                break;
            }
        }
        return root;
    }

    // Delete Node in a BST
    // Leetcode: 450: https://leetcode.com/problems/delete-node-in-a-bst/
    // Iterative:
    public TreeNode deleteNode(TreeNode root, int key) {
        TreeNode curr = root, prev = null;
        while (curr != null) {
            if (key < curr.val) {
                prev = curr;
                curr = curr.left;
            }
            else if (key > curr.val) {
                prev = curr;
                curr = curr.right;
            }
            else {
                break;
            }
        }
        // If node to be deleted is not found:
        if (curr == null) {
            return root;
        }
        // If the node to be deleted is the root:
        if (prev == null) {
            if (curr.left == null && curr.right == null) {
                root = null;
            }
            else if (curr.left != null && curr.right != null) {
                deleteNodeWithTwoChildren(curr);
            }
            else {
                if (curr.left != null) {
                    root = curr.left;
                    curr.left = null;
                }
                else {
                    root = curr.right;
                    curr.right = null;
                }
            }
        }
        else {
            // If the node to be deleted is a leaf:
            if (curr.left == null && curr.right == null) {
                // curr node is the right node:
                if (prev.val < curr.val) {
                    prev.right = null;
                }
                // curr node is the left node:
                else {
                    prev.left = null;
                }
            }
            else if (curr.left != null && curr.right != null) {
                deleteNodeWithTwoChildren(curr);
            }
            // Delete Node with One Child
            else if (curr.left != null){
                if (prev.val < curr.val) {
                    prev.right = curr.left;
                }
                else {
                    prev.left = curr.left;
                }
            }
            else {
                if (prev.val < curr.val) {
                    prev.right = curr.right;
                }
                else {
                    prev.left = curr.right;
                }
            }
        }
        return root;
    }

    private void deleteNodeWithTwoChildren(TreeNode curr) {
        if (curr == null || curr.left == null || curr.right == null) {
            return;
        }
        // Either find the maxNode on the left subtree or the minNode in the right subtree.
        if (true) {
            TreeNode minNode = findMinNode(curr.right);
            curr.right = deleteNode(curr.right, minNode.val);
            curr.val = minNode.val;
        }
        else {
            TreeNode maxNode = findMaxNode(curr.left);
            curr.left = deleteNode(curr.left, maxNode.val);
            curr.val = maxNode.val;
        }
    }

    private TreeNode findMinNode(TreeNode node) {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }

    private TreeNode findMaxNode(TreeNode node) {
        while (node.right != null) {
            node = node.right;
        }
        return node;
    }

    public class AVLTree {

        class TreeNode {
            int val;
            int height;
            TreeNode left, right;
            public TreeNode(int val) {
                this.val = val;
                this.height = 1;
            }
        }

        private int height(TreeNode node) {
            if (node == null) {
                return 0;
            }
            return node.height;
        }


        private TreeNode rotateRight(TreeNode y) {
            TreeNode x = y.left;
            TreeNode t2 = x.right;

            // perform rotation:
            x.right = y;
            y.left = t2;

            // update heights:
            y.height = Math.max(height(y.left), height(y.right)) + 1;
            x.height = Math.max(height(x.left), height(x.right)) + 1;
            return x;
        }

        private TreeNode rotateLeft(TreeNode x) {
            TreeNode y = x.right;
            TreeNode t2 = y.left;

            // perform rotation:
            y.left = x;
            x.right = t2;

            // update heights:
            x.height = Math.max(height(x.left), height(x.right)) + 1;
            y.height = Math.max(height(y.left), height(y.right)) + 1;
            return y;
        }

        private int getBalanceDiff(TreeNode node) {
            if (node == null) {
                return 0;
            }
            return height(node.left) - height(node.right);
        }

        private TreeNode insert(TreeNode node, int val) {
            if (node == null) {
                return new TreeNode(val);
            }
            if (val < node.val) {
                node.left = insert(node.left, val);
            }
            else if (val > node.val) {
                node.right = insert(node.right, val);
            }
            // we don't allow duplicate keys
            else {
                return node;
            }

            // update the height of this ancestor node:
            node.height = 1 + Math.max(height(node.left), height(node.right));

            int balance = getBalanceDiff(node);
            // If this node becomes unbalanced, then there
            // are 4 cases Left Left Case
            if (balance > 1 && val < node.left.val)
                return rotateRight(node);

            // Right Right Case
            if (balance < -1 && val > node.right.val)
                return rotateLeft(node);

            // Left Right Case
            if (balance > 1 && val > node.left.val) {
                node.left = rotateLeft(node.left);
                return rotateRight(node);
            }

            // Right Left Case
            if (balance < -1 && val < node.right.val) {
                node.right = rotateRight(node.right);
                return rotateLeft(node);
            }

            /* return the (unchanged) node pointer */
            return node;
        }
    }
}