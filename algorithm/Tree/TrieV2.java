public class Solution {

    // Leetcode 208. Implement Trie (Prefix Tree)
    // https://leetcode.com/problems/implement-trie-prefix-tree/
    /**
    Implement a trie with insert, search, and startsWith methods.

    Example:

    Trie trie = new Trie();

    trie.insert("apple");
    trie.search("apple");   // returns true
    trie.search("app");     // returns false
    trie.startsWith("app"); // returns true
    trie.insert("app");
    trie.search("app");     // returns true
    Note:

    You may assume that all inputs are consist of lowercase letters a-z.
    All inputs are guaranteed to be non-empty strings.
    */
    // Array version:
    // Space Complexity: O(26 * N)
    class Trie {
        private TrieNode root;
        /** Initialize your data structure here. */
        public Trie() {
            this.root = new TrieNode();
        }

        /** Inserts a word into the trie. */
        // Running Time Complexity: O(N)
        public void insert(String word) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    curr.next[c-'a'] = new TrieNode();
                }
                curr = curr.next[c-'a'];
            }
            curr.isWord = true;
        }

        /** Returns if the word is in the trie. */
        // Running Time Complexity: O(N)
        public boolean search(String word) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    return false;
                }
                curr = curr.next[c-'a'];
            }
            return curr.isWord;
        }

        /** Returns if there is any word in the trie that starts with the given prefix. */
        // Running Time Complexity: O(N)
        public boolean startsWith(String prefix) {
            TrieNode curr = root;
            for (char c : prefix.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    return false;
                }
                curr = curr.next[c-'a'];
            }
            return true;
        }

        class TrieNode {
            boolean isWord;
            TrieNode[] next;
            public TrieNode() {
                this.isWord = false;
                this.next = new TrieNode[26];
            }
        }
    }

    // HashMap Version:
    // Space Time Complexity O(N):
    class Trie {
        private TrieNode root;
        /** Initialize your data structure here. */
        public Trie() {
            this.root = new TrieNode();
        }

        /** Inserts a word into the trie. */
        public void insert(String word) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (!curr.next.containsKey(c)) {
                    curr.next.put(c, new TrieNode());
                }
                curr = curr.next.get(c);
            }
            curr.isWord = true;
        }

        /** Returns if the word is in the trie. */
        public boolean search(String word) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (!curr.next.containsKey(c)) {
                    return false;
                }
                curr = curr.next.get(c);
            }
            return curr.isWord;
        }

        /** Returns if there is any word in the trie that starts with the given prefix. */
        public boolean startsWith(String prefix) {
            TrieNode curr = root;
            for (char c : prefix.toCharArray()) {
                if (!curr.next.containsKey(c)) {
                    return false;
                }
                curr = curr.next.get(c);
            }
            return true;
        }

        class TrieNode {
            boolean isWord;
            Map<Character, TrieNode> next;
            public TrieNode() {
                this.isWord = false;
                this.next = new HashMap<>();
            }
        }
    }

    // Leetcode 211. Add and Search Word - Data structure design
    // https://leetcode.com/problems/add-and-search-word-data-structure-design/
    /***
    Design a data structure that supports the following two operations:

    void addWord(word)
    bool search(word)
    search(word) can search a literal word or a regular expression string containing only letters a-z or .. A . means it can represent any one letter.

    Example:

    addWord("bad")
    addWord("dad")
    addWord("mad")
    search("pad") -> false
    search("bad") -> true
    search(".ad") -> true
    search("b..") -> true
    Note:
    You may assume that all words are consist of lowercase letters a-z.
    */
    class WordDictionary {
        private TrieNode root;

        /** Initialize your data structure here. */
        public WordDictionary() {
            this.root = new TrieNode();
        }

        /** Adds a word into the data structure. */
        public void addWord(String word) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    curr.next[c-'a'] = new TrieNode();
                }
                curr = curr.next[c-'a'];
            }
            curr.isWord = true;
        }

        /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
        public boolean search(String word) {
            TrieNode curr = root;
            int n = word.length();
            return search(word, 0, n, curr);
        }

        private boolean search(String word, int idx, int n, TrieNode curr) {
            if (curr == null) {
                return false;
            }
            if (idx == n && curr != null) {
                return curr.isWord;
            }
            char c = word.charAt(idx);
            if (c != '.') {
                return search(word, idx+1, n, curr.next[c-'a']);
            }
            else {
                for (char ch = 'a'; ch <= 'z'; ch++) {
                    if (search(word, idx+1, n, curr.next[ch-'a'])) {
                        return true;
                    }
                }
            }
            return false;
        }

        class TrieNode {
            boolean isWord;
            TrieNode[] next;
            public TrieNode() {
                this.isWord = false;
                this.next = new TrieNode[26];
            }
        }
    }

    // HashMap Version:
    class WordDictionary {
        private TrieNode root;
        /** Initialize your data structure here. */
        public WordDictionary() {
            root = new TrieNode();
        }

        /** Adds a word into the data structure. */
        public void addWord(String word) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (!curr.next.containsKey(c)) {
                    curr.next.put(c, new TrieNode());
                }
                curr = curr.next.get(c);
            }
            curr.isWord = true;
        }

        /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
        public boolean search(String word) {
            TrieNode curr = root;
            int n = word.length();
            return search(word, 0, n, curr);
        }

        private boolean search(String word, int idx, int n, TrieNode curr) {
            if (curr == null) {
                return false;
            }
            if (idx == n && curr != null) {
                return curr.isWord;
            }
            char c = word.charAt(idx);
            if (c != '.') {
                return search(word, idx+1, n, curr.next.get(c));
            }
            else {
                for (char ch : curr.next.keySet()) {
                    if (search(word, idx+1, n, curr.next.get(ch))) {
                        return true;
                    }
                }
            }
            return false;
        }

        class TrieNode {
            boolean isWord;
            Map<Character, TrieNode> next;
            public TrieNode() {
                this.isWord = false;
                this.next = new HashMap<>();
            }
        }
    }

    // Leetcode 014: Longest Common Prefix
    // https://leetcode.com/problems/longest-common-prefix/
    /**
    Write a function to find the longest common prefix string amongst an array of strings.

    If there is no common prefix, return an empty string "".

    Example 1:

    Input: ["flower","flow","flight"]
    Output: "fl"
    Example 2:

    Input: ["dog","racecar","car"]
    Output: ""
    Explanation: There is no common prefix among the input strings.
    Note:

    All given inputs are in lowercase letters a-z.
    */
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        TrieNode root = new TrieNode();
        addWords(strs, root);

        String str = strs[0];
        int n = strs.length;
        StringBuffer res = new StringBuffer();
        TrieNode curr = root;
        for (char c : str.toCharArray()) {
            curr = curr.next.get(c);
            if (curr.count == n) {
                res.append(c);
            }
            else {
                break;
            }
        }
        return res.toString();
    }

    private void addWords(String[] strs, TrieNode root) {
        for (String str : strs) {
            TrieNode curr = root;
            for (char ch : str.toCharArray()) {
                if (!curr.next.containsKey(ch)) {
                    curr.next.put(ch, new TrieNode());
                }
                curr = curr.next.get(ch);
                curr.count++;
            }
            curr.isWord = true;
        }
    }

    class TrieNode {
        boolean isWord;
        int count;
        Map<Character, TrieNode> next;
        public TrieNode() {
            this.isWord = false;
            this.count = 0;
            this.next = new HashMap<>();
        }
    }

    // Leetcode 336: Palindrome Pairs
    // https://leetcode.com/problems/palindrome-pairs/
    /***
    Given a list of unique words, find all pairs of distinct indices (i, j) in the given list,
    so that the concatenation of the two words, i.e. words[i] + words[j] is a palindrome.

    Example 1:

    Input: ["abcd","dcba","lls","s","sssll"]
    Output: [[0,1],[1,0],[3,2],[2,4]]
    Explanation: The palindromes are ["dcbaabcd","abcddcba","slls","llssssll"]
    Example 2:

    Input: ["bat","tab","cat"]
    Output: [[0,1],[1,0]]
    Explanation: The palindromes are ["battab","tabbat"]
    */
    /**
     *  1) Create an empty Trie.
        2) Do following for every word:-
            a) Insert reverse of current word.
            b) Also store up to which index it is a palindrome.
        3) Traverse list of words again and do following for every word.
            a) If it is available in Trie then return true
            b) If it is partially available
                Check the remaining word is palindrome or not
                If yes then return true that means a pair forms a palindrome.
                Note: Position up to which the word is palindrome
                    is stored because of these type of cases.
     * */
    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> res = new ArrayList<>();
        if (words == null || words.length == 0) {
            return res;
        }
        TrieNode root = new TrieNode();
        int n = words.length;
        for (int i = 0; i < n; i++) {
            String word = words[i];
            addWord(root, word, i);
        }

        for (int i = 0; i < n; i++) {
            String word = words[i];
            searchWord(res, root, word, i);
        }
        return res;
    }

    private void searchWord(List<List<Integer>> res, TrieNode curr, String word, int idx) {
        for (int i = 0, n = word.length(); i < n; i++) {
            char c = word.charAt(i);
            if (curr.idx != -1 && curr.idx != idx && isPalindrome(word, i, n-1)) {
                res.add(Arrays.asList(idx, curr.idx));
            }
            if (curr.next[c-'a'] == null) {
                return;
            }
            curr = curr.next[c-'a'];
        }
        // abcd -> dcba case:
        if (curr.idx != -1 && curr.idx != idx) {
            res.add(Arrays.asList(idx, curr.idx));
        }
        // s -> sslls case:
        for (int pos : curr.palindromePositions) {
            if (pos != idx) {
                res.add(Arrays.asList(idx, pos));
            }
        }
    }

    private void addWord(TrieNode curr, String word, int idx) {
        for (int n = word.length(), i = n-1; i >= 0; i--) {
            char c = word.charAt(i);
            if (curr.next[c-'a'] == null) {
                curr.next[c-'a'] = new TrieNode();
            }
            if (isPalindrome(word, 0, i)) {
                curr.palindromePositions.add(idx);
            }
            curr = curr.next[c-'a'];
        }
        curr.idx = idx;
    }

    private boolean isPalindrome(String str, int i, int j) {
        while (i < j) {
            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
            i++; j--;
        }
        return true;
    }

    class TrieNode {
        int idx;
        TrieNode[] next;
        List<Integer> palindromePositions;
        public TrieNode() {
            this.idx = -1;
            this.next = new TrieNode[26];
            this.palindromePositions = new ArrayList<>();
        }
    }

    // Leetcode 648: Replace Words:
    // https://leetcode.com/problems/replace-words/
    /**
    In English, we have a concept called root, which can be followed by some other words to form another longer word - let's call this word successor. For example, the root an, followed by other, which can form another word another.

    Now, given a dictionary consisting of many roots and a sentence. You need to replace all the successor in the sentence with the root forming it. If a successor has many roots can form it, replace it with the root with the shortest length.

    You need to output the sentence after the replacement.

    Example 1:

    Input: dict = ["cat", "bat", "rat"]
    sentence = "the cattle was rattled by the battery"
    Output: "the cat was rat by the bat"
    */
    public String replaceWords(List<String> dict, String sentence) {
        if (dict == null || dict.isEmpty()) {
            return "";
        }
        TrieNode root = new TrieNode();
        for (String word : dict) {
            addWord(root, word);
        }
        StringBuffer sb = new StringBuffer();
        String[] strs = sentence.split(" ");
        for (String word : strs) {
            searchWord(sb, root, word);
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    private void searchWord(StringBuffer sb, TrieNode curr, String word) {
        for (char c : word.toCharArray()) {
            // found:
            if (curr.isWord) {
                sb.append(curr.word).append(" ");
                return;
            }
            // not found:
            if (curr.next[c-'a'] == null) {
                sb.append(word).append(" ");
                return;
            }
            curr = curr.next[c-'a'];
        }
        if (curr.isWord) {
            sb.append(curr.word).append(" ");
        }
        else {
            sb.append(word).append(" ");
        }
    }

    private void addWord(TrieNode curr, String word) {
        for (char c : word.toCharArray()) {
            if (curr.next[c-'a'] == null) {
                curr.next[c-'a'] = new TrieNode();
            }
            curr = curr.next[c-'a'];
        }
        curr.isWord = true;
        curr.word = word;
    }

    class TrieNode {
        boolean isWord;
        String word;
        TrieNode[] next;
        public TrieNode() {
            this.isWord = false;
            this.word = "";
            this.next = new TrieNode[26];
        }
    }

    // Leetcode 212: Word Search II
    // https://leetcode.com/problems/word-search-ii/
    /**
    Given a 2D board and a list of words from the dictionary, find all words in the board.

    Each word must be constructed from letters of sequentially adjacent cell,
    where "adjacent" cells are those horizontally or vertically neighboring.
    The same letter cell may not be used more than once in a word.

    Example:

    Input:
    board = [
    ['o','a','a','n'],
    ['e','t','a','e'],
    ['i','h','k','r'],
    ['i','f','l','v']
    ]
    words = ["oath","pea","eat","rain"]

    Output: ["eat","oath"]


    Note:

    All inputs are consist of lowercase letters a-z.
    The values of words are distinct.
    */
    // Running Time: O(m*n * L)
    // Build Trie:   n * wordMaxLength
    // Search: boardWidth * boardHeight * (4^wordMaxLength + wordMaxLength[Trie Search])
    private static final int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
    public List<String> findWords(char[][] board, String[] words) {
        List<String> res = new ArrayList<>();
        if (board == null || board.length == 0 || words.length == 0) {
            return res;
        }
        TrieNode root = new TrieNode();
        for (String word : words) {
            addWord(root, word);
        }
        int m = board.length, n = board[0].length;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                searchWord(res, board, root, i, j, m, n, visited);
            }
        }
        return res;
    }

    private void searchWord(List<String> res, char[][] board, TrieNode curr, int i, int j, int m, int n, boolean[][] visited) {
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j]) {
            return;
        }
        char c = board[i][j];
        if (curr.next[c-'a'] == null) {
            return;
        }
        visited[i][j] = true;
        curr = curr.next[c-'a'];
        if (curr.isWord) {
            res.add(curr.word);
            curr.isWord = false;
        }
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            searchWord(res, board, curr, x, y, m, n, visited);
        }
        visited[i][j] = false;
    }

    private void addWord(TrieNode curr, String word) {
        for (char c : word.toCharArray()) {
            if (curr.next[c-'a'] == null) {
                curr.next[c-'a'] = new TrieNode();
            }
            curr = curr.next[c-'a'];
        }
        curr.isWord = true;
        curr.word = word;
    }

    class TrieNode {
        boolean isWord;
        String word;
        TrieNode[] next;
        public TrieNode() {
            this.isWord = false;
            this.next = new TrieNode[26];
        }
    }
}