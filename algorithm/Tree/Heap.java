public class Solution {
    // Leetcode 347: Top k frequent elements
    // https://leetcode.com/problems/top-k-frequent-elements/
    /**
    Given a non-empty array of integers, return the k most frequent elements.

    Example 1:

    Input: nums = [1,1,1,2,2,3], k = 2
    Output: [1,2]
    Example 2:

    Input: nums = [1], k = 1
    Output: [1]
    Note:

    You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
    Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
    */
    public List<Integer> topKFrequent(int[] nums, int k) {
        List<Integer> result = new ArrayList<Integer>();
        if (nums == null || nums.length == 0 || k <= 0) {
            return result;
        }
        // key -> value, value -> frequency
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (!map.containsKey(num)) {
                map.put(num, 1);
            }
            else {
                int freq = map.get(num);
                freq++;
                map.put(num, freq);
            }
        }
        // Min Heap so that the least frequent one will be removed first.
        Queue<FrequencyItem> pq = new PriorityQueue<FrequencyItem>((f1, f2) -> f1.freq - f2.freq);
        for (int key : map.keySet()) {
            pq.offer(new FrequencyItem(key, map.get(key)));
            if (pq.size() > k) {
                pq.poll();
            }
        }
        
        while (!pq.isEmpty()) {
            FrequencyItem freqItem = pq.poll();
            result.add(freqItem.val);
        }
        return result;
    }

    class FrequencyItem {
        int val;
        int freq;
        public FrequencyItem(int val, int freq) {
            this.val = val;
            this.freq = freq;
        } 
    }

    // Solution 2: Add all items into the Max Heap and get them by K
    // Running Time Complexity: O(N + N * logN + K) = O(NlogN), Space Complexity: O(N)
    public List<Integer> topKFrequent(int[] nums, int k) {
        List<Integer> result = new ArrayList<Integer>();
        if (nums == null || nums.length == 0 || k <= 0) {
            return result;
        }
        // key -> value, value -> frequency
        Map<Integer, FrequencyItem> map = new HashMap<>();
        for (int num : nums) {
            if (!map.containsKey(num)) {
                map.put(num, new FrequencyItem(num, 1));
            }
            else {
                FrequencyItem freqItem = map.get(num);
                freqItem.freq++;
                map.put(num, freqItem);
            }
        }
        // Max Heap so that we have max frequent items on the top
        Queue<FrequencyItem> pq = new PriorityQueue<FrequencyItem>((f1, f2) -> f2.freq - f1.freq);
        for (int key : map.keySet()) {
            pq.offer(map.get(key));
        }
        
        while (k > 0) {
            FrequencyItem freqItem = pq.poll();
            result.add(freqItem.val);
            k--;
        }
        return result;
    }

    // Leetcode 692: Top K Frequent Words
    /**
    Given a non-empty list of words, return the k most frequent elements.

    Your answer should be sorted by frequency from highest to lowest. If two words have the same frequency, then the word with the lower alphabetical order comes first.

    Example 1:
    Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
    Output: ["i", "love"]
    Explanation: "i" and "love" are the two most frequent words.
        Note that "i" comes before "love" due to a lower alphabetical order.
    Example 2:
    Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
    Output: ["the", "is", "sunny", "day"]
    Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
        with the number of occurrence being 4, 3, 2 and 1 respectively.
    Note:
    You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
    Input words contain only lowercase letters.
    Follow up:
    Try to solve it in O(n log k) time and O(n) extra space.
    */
    // Running Time Complexity: O(NlogK), Space Complexity: O(N)
    public List<String> topKFrequent(String[] words, int k) {
        List<String> res = new ArrayList<>();
        if (words == null || words.length == 0) {
            return res;
        }
        Map<String, Integer> freqMap = new HashMap<>();
        for (String word : words) {
            freqMap.put(word, freqMap.getOrDefault(word, 0) + 1);
        }
        Queue<FreqWord> pq = new PriorityQueue<FreqWord>(new Comparator<FreqWord>(){
            @Override
            public int compare(FreqWord fw1, FreqWord fw2) {
                // If two words frequency the same, we want the lower alphabetical order.
                if (fw1.freq == fw2.freq) {
                    return fw1.word.compareTo(fw2.word);
                }
                // Else, the higher freq item gets the higer order
                else {
                    return fw2.freq - fw1.freq;
                }
            }
        });
        
        for (String word : freqMap.keySet()) {
            int freq = freqMap.get(word);
            pq.add(new FreqWord(freq, word));
        }
        while (!pq.isEmpty() && res.size() < k) {
            res.add(pq.poll().word);
        }
        return res;
    }
    
    class FreqWord {
        int freq;
        String word;
        public FreqWord(int freq, String word) {
            this.freq = freq;
            this.word = word;
        }
    }
}