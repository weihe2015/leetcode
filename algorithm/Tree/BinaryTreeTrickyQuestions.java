public class Solution {
    // Populating Next Right Pointers in Each Node:
    // Leetcode 116: https://leetcode.com/problems/populating-next-right-pointers-in-each-node
    /**
     * Given the following perfect binary tree,
          1
        /  \
        2    3
        / \  / \
        4  5  6  7
        After calling your function, the tree should look like:

          1 -> NULL
        /  \
        2 -> 3 -> NULL
        / \  / \
        4->5->6->7 -> NULL
Note:
You may only use constant extra space.
Recursive approach is fine, implicit stack space does not count as extra space for this problem.
You may assume that it is a perfect binary tree (ie, all leaves are at the same level, and every parent has two children).
    */
    // Recursive:
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        if (root.left != null) {
            root.left.next = root.right;
            if (root.next != null) {
                root.right.next = root.next.left;
            }
        }
        connect(root.left);
        connect(root.right);
    }

    // Iterative:
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        // PrevNode is always the leftmost node of each level in the tree:
        TreeLinkNode prev = root, curr = null;
        while (prev.left != null) {
            curr = prev;
            while (curr != null) {
                // connect left and right node in same parent
                if (curr.left != null) {
                    curr.left.next = curr.right;
                }
                // connect right and left node in diff parent
                if (curr.next != null) {
                    curr.right.next = curr.next.left;
                }
                // move to next right sub tree 
                curr = curr.next;
            }
            // Go to next level:
            prev = prev.left; 
        }
    }
    // Solution to any Binary Tree:
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        TreeLinkNode newHead = new TreeLinkNode(0), prev = newHead, curr = root;
        while (curr != null) {
            // get the first Node of next level and move prev node to one node rightward:
            if (curr.left != null) {
                prev.next = curr.left;
                prev = prev.next;
            }
            if (curr.right != null) {
                prev.next = curr.right;
                prev = prev.next;
            }

            // Move curr node to one node rightward
            curr = curr.next;
            // Reaching the end of 
            if (curr == null) {
                // Move the curr node back to the first node of each level.
                curr = newHead.next;
                // Set prev node back to newHead pointer:
                prev = newHead;
                newHead.next = null;
            }
        }
    }

    // Leetcode Flatten Binary Tree into LinkedList:
    // Leetcode 114: https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
    /**
     * Given a binary tree, flatten it to a linked list in-place.

For example, given the following tree:

    1
   / \
  2   5
 / \   \
3   4   6
The flattened tree should look like:

1
 \
  2
   \
    3
     \
      4
       \
        5
         \
          6
    */
    private TreeNode prevNode = null;
    public void flatten(TreeNode root) {
        dfs(root);
    }
    
    private void dfs(TreeNode node) {
        if (node == null) {
            return;
        }
        dfs(node.right);
        dfs(node.left);
        node.left = null;
        node.right = prevNode;
        prevNode = node;
    }
}