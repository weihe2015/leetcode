public class Solution {
    // Leetcode 098: Validate Binary Search Tree
    // https://leetcode.com/problems/validate-binary-search-tree/
    /**
     * Basic Idea: Set a min and max range for each node. If node's value is outside
     * of this range, return false For left subtree: max = node.val For right
     * subtree: min = node.val
     */
    // Recursive:
    public boolean isValidBST(TreeNode root) {
        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    private boolean isValidBST(TreeNode node, long min, long max) {
        if (node == null) {
            return true;
        }
        if (node.val <= min || node.val >= max) {
            return false;
        }
        return isValidBST(node.left, min, node.val) && isValidBST(node.right, node.val, max);
    }

    // Iterative:
    // Use In order Traversal method, and use prevNode to mark the last visited node
    // and compare its value.
    // In order Traversal BST should produces a sorted array
    public boolean isValidBST(TreeNode root) {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node = root, prevNode = null;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.pop();
            if (prevNode != null && prevNode.val >= currNode.val) {
                return false;
            }
            prevNode = currNode;
            node = currNode.right;
        }
        return true;
    }

    // Is Same Binary Tree
    // Leetcode 100: https://leetcode.com/problems/same-tree
    // Recrusive
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        } else if (p == null || q == null) {
            return false;
        } else if (p.val != q.val) {
            return false;
        }
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    // Iterative
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        } else if (p == null || q == null) {
            return false;
        }
        Queue<TreeNode> pQueue = new LinkedList<TreeNode>();
        Queue<TreeNode> qQueue = new LinkedList<TreeNode>();
        pQueue.offer(p);
        qQueue.offer(q);
        while (!pQueue.isEmpty() && !qQueue.isEmpty()) {
            TreeNode currPNode = pQueue.poll();
            TreeNode currQNode = qQueue.poll();
            if (currPNode.val != currQNode.val) {
                return false;
            }
            if (currPNode.left != null && currQNode.left != null) {
                pQueue.offer(currPNode.left);
                qQueue.offer(currQNode.left);
            } else if (currPNode.left != null || currQNode.left != null) {
                return false;
            }
            if (currPNode.right != null && currQNode.right != null) {
                pQueue.offer(currPNode.right);
                qQueue.offer(currQNode.right);
            } else if (currPNode.right != null || currQNode.right != null) {
                return false;
            }

        }
        return pQueue.isEmpty() && qQueue.isEmpty();
    }

    // Recover Binary Tree:
    // Leetcode: 99: https://leetcode.com/problems/recover-binary-search-tree/
    // Iterative:
    /**
     * Basic Idea: User in order traversal to find the first node in wrong order
     * then find secondNode = currNode when nodes are in wrong order At the end,
     * swap first and second Nodes
     */
    public void recoverTree(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node = root, prevNode = null, firstNode = null, secondNode = null;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.pop();
            // find the incorrect order nodes:
            if (prevNode != null && prevNode.val >= currNode.val) {
                if (firstNode == null) {
                    firstNode = prevNode;
                }
                secondNode = currNode;
            }
            prevNode = currNode;
            node = currNode.right;
        }
        // Swap two nodes by their values
        if (firstNode != null && secondNode != null) {
            // Swap two nodes:
            int temp = firstNode.val;
            firstNode.val = secondNode.val;
            secondNode.val = temp;
        }
    }

    // Balanced Binary Tree:
    // Leetcode 110: https://leetcode.com/problems/balanced-binary-tree/
    // Recursive:
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        int leftDepth = depth(root.left);
        int rightDepth = depth(root.right);
        if (Math.abs(leftDepth - rightDepth) > 1) {
            return false;
        }
        return isBalanced(root.left) && isBalanced(root.right);
    }

    private int depth(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return Math.max(depth(node.left), depth(node.right)) + 1;
    }

    // Leetcode 101 Symmetric Tree https://leetcode.com/problems/symmetric-tree/
    /**
     * Basic idea is the same for Leetcode 100: Same Binary Tree:
     * https://leetcode.com/problems/same-tree
     */
    // recursive
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.left, root.right);
    }

    private boolean isSymmetric(TreeNode leftNode, TreeNode rightNode) {
        if (leftNode == null && rightNode == null) {
            return true;
        } else if (leftNode == null || rightNode == null) {
            return false;
        } else if (leftNode.val != rightNode.val) {
            return false;
        }
        return isSymmetric(leftNode.left, rightNode.right) && isSymmetric(leftNode.right, rightNode.left);
    }

    // Iterative
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        if (root.left == null && root.right == null) {
            return true;
        } else if (root.left == null || root.right == null) {
            return false;
        }

        Queue<TreeNode> leftQueue = new LinkedList<TreeNode>();
        Queue<TreeNode> rightQueue = new LinkedList<TreeNode>();
        leftQueue.offer(root.left);
        rightQueue.offer(root.right);
        while (!leftQueue.isEmpty() && !rightQueue.isEmpty()) {
            TreeNode leftNode = leftQueue.poll();
            TreeNode rightNode = rightQueue.poll();
            if (leftNode.val != rightNode.val) {
                return false;
            }
            if (leftNode.left != null && rightNode.right != null) {
                leftQueue.offer(leftNode.left);
                rightQueue.offer(rightNode.right);
            } else if (leftNode.left != null || rightNode.right != null) {
                return false;
            }

            if (leftNode.right != null && rightNode.left != null) {
                leftQueue.offer(leftNode.right);
                rightQueue.offer(rightNode.left);
            } else if (leftNode.right != null || rightNode.left != null) {
                return false;
            }
        }
        return leftQueue.isEmpty() && rightQueue.isEmpty();
    }

    // Minimum Depth of Binary Tree:
    // Leetcode 111: https://leetcode.com/problems/minimum-depth-of-binary-tree/
    // Recursive
    /**
     * Basic Idea: When there is left or right child, add 1 to depth and continue
     * When it is a leaf, return current depth When it is a full leaf, which has
     * both left and right child, return its smaller depth of left and right child.
     */
    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return minDepth(root, 1);
    }

    private int minDepth(TreeNode node, int height) {
        if (node.left == null && node.right == null) {
            return height;
        else if (node.left != null && node.right != null) {
            int leftDepth = minDepth(node.left, height + 1);
            int rightDepth = minDepth(node.right, height + 1);
            return Math.min(leftDepth, rightDepth);
        } else  {
            return minDepth(node.left, height + 1);
        }
    }

    // Iterative:
    /**
     * Basic Idea: Use level order Traversal. Whenever it reaches a leaf, return
     * current depth as result.
     * 
     */
    public int minDepth(TreeNode root) {
        int depth = 0;
        if (root == null) {
            return depth;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            depth++;
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
                if (currNode.left == null && currNode.right == null) {
                    return depth;
                }
            }
        }
        // Whenever it meets a leave, it will return depth, so code will not reach here.
        return -1;
    }

    // Maximum Depth of a binary Tree, aka the depth of a binary tree
    // Leetcode 104: https://leetcode.com/problems/maximum-depth-of-binary-tree/
    // Recursive:
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }

    // Iterative
    /**
     * Basic Idea: Use Level order traversal, to get the total level == height of
     * the tree Running Time Complexity: O(N), Space Complexity: O(N) when tree is
     * linear
     */
    public int maxDepth(TreeNode root) {
        int depth = 0;
        if (root == null) {
            return depth;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            depth++;
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
            }
        }
        return depth;
    }

    // Invert Binary Tree
    // Leetcode 226: https://leetcode.com/problems/invert-binary-tree/
    // Recursion
    public TreeNode invertTree(TreeNode root) {
        invert(root);
        return root;
    }

    private void invert(TreeNode node) {
        if (node == null) {
            return;
        }
        swap(node);
        invert(node.left);
        invert(node.right);
    }

    private void swap(TreeNode node) {
        TreeNode temp = node.left;
        node.left = node.right;
        node.right = temp;
    }

    // Iterative:
    /**
     * Basic Idea: Use Level Order traversal and swap the node.
     */
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return root;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                swap(currNode);
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
            }
        }
        return root;
    }

    private void swap(TreeNode node) {
        TreeNode temp = node.left;
        node.left = node.right;
        node.right = temp;
    }

    // Construct Binary Tree from Preorder and Inorder Traversal
    // Leetcode 105:
    // https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
    /**
     * root <--left--> <--right--> preorder: 50, 30, 10, 40, 70, 60, 90 <---left-->
     * root <--right--> inorder: 10, 30, 40, 50, 60, 70, 90
     */
    /**
     * Basic Idea: preorder[preStart] is rootNode, anynode in inorder array before
     * it is left subtree any node in inorder array after it is right subtree.
     */
    public TreeNode buildTreeFromPreorderInorder(int[] preorder, int[] inorder) {
        return buildTree(preorder, 0, preorder.length - 1, inorder, 0, inorder.length - 1);
    }

    private TreeNode buildTreeFromPreorderInorder(int[] preorder, int preStart, int preEnd, int[] inorder, int inStart,
            int inEnd) {
        // firstly check if there is no element or only one element
        // then immediately return
        if (preStart > preEnd || inStart > inEnd) {
            return null;
        }
        int rootVal = preorder[preStart];
        TreeNode root = new TreeNode(rootVal);

        // Otherwise, we use our defined strategy, as first element in
        // preorder is the root and we find the value in in-order array
        // to determine how many 'left' elemnt in there and construct the left sub tree
        // Find the root index in inorder array
        int rootIdx = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }
        // calculate the left subtree size in inorder array:
        int leftTreeSize = rootIdx - inStart;
        root.left = buildTreeFromPreorderInorder(preorder, preStart + 1, preStart + leftSubTreeSize, inorder, inStart,
                rootIdx - 1);
        root.right = buildTreeFromPreorderInorder(preorder, preStart + leftSubTreeSize + 1, preEnd, inorder,
                rootIdx + 1, inEnd);
        return root;
    }

    // Construct Binary Tree from Inorder and Postorder Traversal
    // Leetcode 106:
    // https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal
    public TreeNode buildTreeFromInorderPostorder(int[] inorder, int[] postorder) {
        return buildTrbuildTreeFromInorderPostorderee(inorder, 0, inorder.length - 1, postorder, 0,
                postorder.length - 1);
    }

    private TreeNode buildTreeFromInorderPostorder(int[] inorder, int inStart, int inEnd, int[] postorder,
            int postStart, int postEnd) {
        if (postStart > postEnd || inStart > inEnd) {
            return null;
        }
        int rootVal = postorder[postEnd];
        TreeNode root = new TreeNode(rootVal);
        int rootIdx = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == rootVal) {
                rootIdx = i;
                break;
            }
        }
        int leftSubTreeSize = rootIdx - inStart;
        root.left = buildTreeFromInorderPostorder(inorder, inStart, rootIdx - 1, postorder, postStart,
                postStart + leftSubTreeSize - 1);
        root.right = buildTreeFromInorderPostorder(inorder, rootIdx + 1, inEnd, postorder, postStart + leftSubTreeSize,
                postEnd - 1);
        return root;
    }

    // Construct Binary Tree from Preorder and Postorder Traversal:
    // Tricky: Preorder and postorder do not uniquely define a tree.
    // But if know that the Binary Tree is Full, we can construct the tree without
    // ambiguity

    // Path Sum Sequence:
    // Path Sum 1:
    // Leetcode 112: https://leetcode.com/problems/path-sum
    // Given a binary tree and a sum, determine if the tree has a root-to-leaf path
    // such that adding up all the values along the path equals the given sum.
    // Recursive:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        int val = sum - root.val;
        if (val == 0 && root.left == null && root.right == null) {
            return true;
        }
        return hasPathSum(root.left, val) || hasPathSum(root.right, val);
    }

    // Level Order Traversal:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        Queue<Integer> sumQueue = new LinkedList<Integer>();
        queue.offer(root);
        sumQueue.offer(root.val);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                int currSum = sumQueue.poll();
                if (currNode.left == null && currNode.right == null && currSum == sum) {
                    return true;
                }
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                    sumQueue.offer(currSum + currNode.left.val);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                    sumQueue.offer(currSum + currNode.right.val);
                }
            }
        }
        return false;
    }

    // Post order Traversal:
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node = root, prevNode = null;
        int tempSum = 0;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                tempSum += node.val;
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.peek();
            if (currNode.left == null && currNode.right == null && tempSum == sum) {
                return true;
            }
            if (currNode.right != null && currNode.right != prevNode) {
                node = currNode.right;
            } else {
                currNode = stack.pop();
                prevNode = currNode;
                tempSum -= currNode.val;
            }
        }
        return false;
    }

    // Path Sum II
    // Leetcode 112: https://leetcode.com/problems/path-sum-ii/
    // Given a binary tree and a sum, find all root-to-leaf paths where each path's
    // sum equals the given sum.
    // Recursive:
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        pathSum(result, root, sum, new ArrayList<Integer>());
        return result;
    }

    private void pathSum(List<List<Integer>> result, TreeNode node, int sum, List<Integer> list) {
        if (node == null) {
            return;
        }
        int diff = sum - node.val;
        if (node.left == null && node.right == null && diff == 0) {
            list.add(node.val);
            result.add(new ArrayList<Integer>(list));
            list.remove(list.size() - 1);
            return;
        }
        list.add(node.val);
        pathSum(result, node.left, diff, list);
        pathSum(result, node.right, diff, list);
        list.remove(list.size() - 1);
    }

    // Post order traversal
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        List<Integer> list = new ArrayList<Integer>();
        int tempSum = 0;
        TreeNode node = root, prevNode = null;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                tempSum += node.val;
                list.add(node.val);
                stack.push(node);
                node = node.left;
            }
            TreeNode currNode = stack.peek();
            if (currNode.left == null && currNode.right == null && tempSum == sum) {
                result.add(new ArrayList<Integer>(list));
            }
            if (currNode.right != null && currNode.right != prevNode) {
                node = currNode.right;
            } else {
                currNode = stack.pop();
                prevNode = currNode;
                tempSum -= currNode.val;
                list.remove(list.size() - 1);
            }
        }
        return result;
    }

    // Path Sum III
    // Leetcode 437: https://leetcode.com/problems/path-sum-iii/
    /**
     * You are given a binary tree in which each node contains an integer value.
     * 
     * Find the number of paths that sum to a given value.
     * 
     * The path does not need to start or end at the root or a leaf, but it must go
     * downwards (traveling only from parent nodes to child nodes).
     * 
     * root = [10,5,-3,3,2,null,11,3,-2,null,1], sum = 8
     * 
     * 10 / \ 5 -3 / \ \ 3 2 11 / \ \ 3 -2 1
     * 
     * Return 3. The paths that sum to 8 are:
     * 
     * 1. 5 -> 3 2. 5 -> 2 -> 1 3. -3 -> 11
     * 
     */
    // Recursive: Running Time: O(NlogN), T(N) = O(N) + 2T(N/2)
    public int pathSum(TreeNode root, int sum) {
        if (root == null) {
            return 0;
        }
        return pathSumFromNode(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
    }

    private int pathSumFromNode(TreeNode node, int sum) {
        if (node == null) {
            return 0;
        }
        int diff = sum - node.val;
        if (diff == 0) {
            return 1 + pathSumFromNode(node.left, diff) + pathSumFromNode(node.right, diff);
        } else {
            return pathSumFromNode(node.left, diff) + pathSumFromNode(node.right, diff);
        }
    }

    // Recursive: Running Time: O(N)
    public int pathSum(TreeNode root, int sum) {
        int count = 0;
        if (root == null) {
            return count;
        }
        // key -> sum to this node, value: number of occurrence
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        count = pathSum(root, 0, sum, map);
        return count;
    }

    private int pathSum(TreeNode node, int currSum, int sum, Map<Integer, Integer> map) {
        if (node == null) {
            return 0;
        }
        int count = 0;
        currSum += node.val;

        int sum_j = currSum - sum;
        // handle path from root:
        if (sum_j == 0) {
            count++;
        }

        if (map.containsKey(sum_j)) {
            count += map.get(sum_j);
        }

        // Set currSum and its value to 1 into map if it is not existed,
        if (!map.containsKey(currSum)) {
            map.put(currSum, 1);
        } else {
            int prevCount = map.get(currSum);
            map.put(currSum, prevCount + 1);
        }

        count += pathSum(node.left, currSum, sum, map) + pathSum(node.right, currSum, sum, map);
        // Reset the occurence time by reducing 1, when returning from children
        int prevCount = map.get(currSum);
        map.put(currSum, prevCount - 1);
        return count;
    }

    // Binary Tree Paths:
    // Leetcode 257: https://leetcode.com/problems/binary-tree-paths/
    // Given a binary tree, return all root-to-leaf paths.
    /**
     * Example: Input: 1 / \ 2 3 \ 5 Output: ["1->2->5", "1->3"] Explanation: All
     * root-to-leaf paths are: 1->2->5, 1->3
     */
    // Recursive:
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<String>();
        binaryTreePaths(result, root, new StringBuffer());
        return result;
    }

    private void binaryTreePaths(List<String> result, TreeNode node, StringBuffer sb) {
        if (node == null) {
            return;
        }
        int n = sb.length();
        if (node.left == null && node.right == null) {
            sb.append(node.val);
            result.add(sb.toString());
            // Delete Characters from index n to index sb.length();
            sb.delete(n, sb.length());
            return;
        }
        sb.append(node.val);
        sb.append("->");
        binaryTreePaths(result, node.left, sb);
        binaryTreePaths(result, node.right, sb);
        // Delete Characters from index n to index sb.length();
        sb.delete(n, sb.length());
    }

    // Level Order Traversal:
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<String>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        Queue<StringBuffer> pathQueue = new LinkedList<StringBuffer>();
        queue.offer(root);
        pathQueue.offer(new StringBuffer());

        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                StringBuffer currPath = pathQueue.poll();

                if (currNode.left == null && currNode.right == null) {
                    StringBuffer stringBuffer = new StringBuffer(currPath.toString());
                    stringBuffer.append(currNode.val);
                    result.add(stringBuffer.toString());
                }

                if (currNode.left != null) {
                    StringBuffer stringBuffer = new StringBuffer(currPath.toString());
                    stringBuffer.append(currNode.val);
                    stringBuffer.append("->");
                    queue.offer(currNode.left);
                    pathQueue.offer(stringBuffer);
                }

                if (currNode.right != null) {
                    StringBuffer stringBuffer = new StringBuffer(currPath.toString());
                    stringBuffer.append(currNode.val);
                    stringBuffer.append("->");
                    queue.offer(currNode.right);
                    pathQueue.offer(stringBuffer);
                }
            }
        }
        return result;
    }

    // Sum Root to Leaf Numbers
    // Leetcode 129: https://leetcode.com/problems/sum-root-to-leaf-numbers/
    /**
     * Given a binary tree containing digits from 0-9 only, each root-to-leaf path
     * could represent a number. An example is the root-to-leaf path 1->2->3 which
     * represents the number 123. Find the total sum of all root-to-leaf numbers.
     * Note: A leaf is a node with no children. Example: Input: [1,2,3] 1 / \ 2 3
     * Output: 25 Explanation: The root-to-leaf path 1->2 represents the number 12.
     * The root-to-leaf path 1->3 represents the number 13. Therefore, sum = 12 + 13
     * = 25.
     */
    // Recursive
    public int sumNumbers(TreeNode root) {
        List<Integer> paths = new ArrayList<Integer>();
        sumNumbers(paths, root, 0);
        int sum = 0;
        for (int num : paths) {
            sum += num;
        }
        return sum;
    }
    
    private void sumNumbers(List<Integer> paths, TreeNode node, int num) {
        if (node == null) {
            return;
        }
        num = num * 10 + node.val;
        if (node.left == null && node.right == null) {  
            paths.add(num);
            return;
        }
        sumNumbers(paths, node.left, num);
        sumNumbers(paths, node.right, num);
    }

    // Iterative, Level Order Traversal    
    public int sumNumbers(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int sum = 0;
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        Queue<Integer> sumQueue = new LinkedList<Integer>();
        queue.offer(root); sumQueue.offer(0);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                int num = sumQueue.poll();
                num = 10 * num + currNode.val;
                if (currNode.left == null && currNode.right == null) {
                    sum += num;
                }
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                    sumQueue.offer(num);
                }
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                    sumQueue.offer(num);
                }
            }
        }
        return sum;
    }

    // Binary Tree Right Side View
    // Leetcode 199 https://leetcode.com/problems/binary-tree-right-side-view/
    /**
     * Given a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.
        Example:
        Input: [1,2,3,null,5,null,4]
        Output: [1, 3, 4]
        Explanation:
          1            <---
        /   \
        2     3         <---
        \     \
        5     4       <---
    */
    // Recursive
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        rightSideView(result, root, 0);
        return result;
    }
    
    private void rightSideView(List<Integer> result, TreeNode node, int depth) {
        if (node == null) {
            return;
        }
        if (depth == result.size()) {
            result.add(node.val);
        }
        rightSideView(result, node.right, depth+1);
        rightSideView(result, node.left, depth+1);
    }

    // Iterative BFS
    // level order traversal
    // always put the rightest node into queue first
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                // always add the first node into the result list:
                if (i == 1) {
                    result.add(currNode.val);
                }
                // always put the rightest node into queue first
                if (currNode.right != null) {
                    queue.offer(currNode.right);
                }
                if (currNode.left != null) {
                    queue.offer(currNode.left);
                }
            }
        }
        return result;
    }

    // Leetcode 515 Find largest Value in Each Tree Row:
    /*
    You need to find the largest value in each row of a binary tree.
    Example:
    Input: 
Input: 

          1
         / \
        3   2
       / \   \  
      5   3   9 

Output: [1, 3, 9]
    */
    public List<Integer> largestValues(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.offer(root);
        while (!q.isEmpty()) {
            int size = q.size();
            int max = Integer.MIN_VALUE;
            for (int i = 1; i <= size; i++) {
                TreeNode node = q.poll();
                max = Math.max(max, node.val);
                if (node.right != null) {
                    q.offer(node.right);
                }
                if (node.left != null) {
                    q.offer(node.left);
                }
            }
            result.add(max);
        }
        return result;
    }
    
    // Leetcode: 230 Kth Smallest Element in a BST
    // https://leetcode.com/problems/kth-smallest-element-in-a-bst/
    // Recursive: Use Inorder Traversal:
    public int kthSmallest(TreeNode root, int k) {
        List<Integer> list = new ArrayList<Integer>();
        inorderTraversal(list, root);
        return list.get(k-1);
    }
    
    private void inorderTraversal(List<Integer>list, TreeNode node) {
        if (node == null) {
            return;
        }
        inorderTraversal(list, node.left);
        list.add(node.val);
        inorderTraversal(list, node.right);
    }
    // Second solution for Recursive In order Traversal.
    private int count = 0, num = 0;
    public int kthSmallest(TreeNode root, int k) {
        List<Integer> list = new ArrayList<Integer>();
        inorderTraversal(root, k);
        return num;
    }
    
    private void inorderTraversal(TreeNode node, int k) {
        if (node == null) {
            return;
        }
        inorderTraversal(node.left, k);
        count++;
        if (count == k) {
            num = node.val;
            return;
        }
        inorderTraversal(node.right, k);
    }
    // Iterative: Use Inorder Traversal
    public int kthSmallest(TreeNode root, int k) {
        int num = 0;
        if (root == null) {
            return 0;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node = root;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            TreeNode curr = stack.pop();
            num++;
            if (num == k) {
                return curr.val;
            }
            node = curr.right;
        }
        return -1;
    }
    // Follow up:
    // What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently? 
    // How would you optimize the kthSmallest routine?
    /**
     * Answer: add rank field inside the TreeNode class, which represents number of nodes smaller than this node.
     *   Update this rank field when we construct the tree.
     * Then we can use search method in BST to find this kth smallest node, O(h) time
    */
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        int rank;
        public TreeNode(int x) {val = x;}
    }

    public int kthSmallest(TreeNode root, int k) {
        if (root == null) {
            return;
        }
        TreeNode curr = root;
        while (curr != null) {
            if (curr.rank + 1 == k) {
                return curr;
            }
            else if (curr.rank + 1 < k) {
                curr = curr.right;
            }
            else {
                curr = curr.left;
            }
        }
    }

    // Lowest Common Ancestor of a Binary Search Tree
    // Leetcode 235: https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/
    // Recursive
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // If both p and q's value are less than root.val, then their LCA is on root.left
        if (p.val < root.val && q.val < root.val) {
            return lowestCommonAncestor(root.left, p, q);
        }
        // If both p and q's value are greater than root.val, then their LCA is on root.right
        else if (p.val > root.val && q.val > root.val) {
            return lowestCommonAncestor(root.right, p, q);
        }
        else {
            return root;
        }
    }

    // Iterative
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        TreeNode curr = root;
        while (curr != null) {
            if (p.val < curr.val && q.val < curr.val) {
                curr = curr.left;
            }
            else if (p.val > curr.val && q.val > curr.val) {
                curr = curr.right;
            }
            else {
                return curr;
            }
        }
        return null;
    }

    // Iterative: Save all parents pointers into a HashMap.
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        }
        // Key: childNode, Value: ParentNode
        Map<TreeNode, TreeNode> parentMap = new HashMap<TreeNode, TreeNode>();
        parentMap.put(root, null);
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!(parentMap.containsKey(p) && parentMap.containsKey(q))) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode currNode = queue.poll();
                if (currNode.left != null) {
                    parentMap.put(currNode.left, currNode);
                    queue.offer(currNode.left);
                }
                if (currNode.right != null) {
                    parentMap.put(currNode.right, currNode);
                    queue.offer(currNode.right);
                }
            }
        }
        // find all ancestors of node p:
        Set<TreeNode> ancestor = new HashSet<TreeNode>();
        TreeNode curr = p;
        while (curr != null) {
            ancestor.add(curr);
            curr = parentMap.get(curr);
        }
        
        // Get all ancestor of node q until it reaches the one of node p:
        curr = q;
        while (!ancestor.contains(curr)) {
            curr = parentMap.get(curr);
        }
        return curr;
    }

    // Leetcode 297 Serialize and Deserialize Binary Tree 
    //  https://leetcode.com/problems/serialize-and-deserialize-binary-tree/
    /**
    Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, 
    or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

    Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. 
    You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.
    */
    private static final String NULL = "NULL";
    private static final String COMMA = ",";

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuffer sb = new StringBuffer();
        if (root == null) {
            return sb.toString();
        }
        // Use Level Order Traversal to serialize the tree.
        int maxDepth = depth(root);
        // The node number includes NULL node.
        int numOfNodeInLastLevel = (int) Math.pow(2, maxDepth-1);
        
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        boolean isEnd = false;
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            // If curr queue contains 2^(depth - 1) nodes,
            // then we know it has reaches the lowest level of the tree.
            if (size == numOfNodeInLastLevel) {
                isEnd = true;
            }
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                if (curr == null) {
                    sb.append(NULL).append(COMMA);
                }
                else {
                    sb.append(curr.val).append(COMMA);
                    queue.offer(curr.left);
                    queue.offer(curr.right);
                }
            }
            if (isEnd) {
                break;
            }
        }
        // remove the last comma ,
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    private int depth(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return Math.max(depth(node.left), depth(node.right)) + 1;
    }
    
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        // Handle Empty Tree: [], and empty data
        if (data == null || data.length() == 0) {
            return null;
        }
        
        String[] arr = data.split(COMMA);
        if (arr == null || arr.length == 0) {
            return null;
        }
        TreeNode root = null;
        String nodeVal = arr[0];
        if ("NULL".equals(nodeVal)) {
            return root;
        }
        else {
            int val = Integer.parseInt(arr[0]);
            root = new TreeNode(val);
        }
         
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        
        int i = 1, n = arr.length;
        while (i < n) {
            int size = queue.size();
            for (int j = 1; j <= size; j++) {
                TreeNode currNode = queue.poll();
                if (currNode != null) {
                    nodeVal = arr[i++];
                    if (NULL.equals(nodeVal)) {
                        currNode.left = null;
                    }
                    else {
                        int val = Integer.parseInt(nodeVal);
                        currNode.left = new TreeNode(val);
                    }
                    
                    nodeVal = arr[i++];
                    if (NULL.equals(nodeVal)) {
                        currNode.right = null;
                    }
                    else {
                        int val = Integer.parseInt(nodeVal);
                        currNode.right = new TreeNode(val);
                    }
                    queue.offer(currNode.left);
                    queue.offer(currNode.right);
                }
            }
        }
        return root;
    }

    // Serialize and Deserialize BST
    // Leetcode 449: https://leetcode.com/problems/serialize-and-deserialize-bst/
    /**
    Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, 
    or transmitted across a network connection link to be reconstructed later in the same or another computer environment.
    Design an algorithm to serialize and deserialize a binary search tree. 
    There is no restriction on how your serialization/deserialization algorithm should work. 
    You just need to ensure that a binary search tree can be serialized to a string and this string can be deserialized to the original tree structure.

    The encoded string should be as compact as possible.
    */

    private static String COMMA = ",";
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuffer sb = new StringBuffer();
        if (root == null) {
            return sb.toString();
        }
        // Use Preorder traversal to add nodes into string.
        Stack<TreeNode> stack = new Stack<TreeNode>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            sb.append(curr.val).append(COMMA);
            if (curr.right != null) {
                stack.push(curr.right);
            }
            if (curr.left != null) {
                stack.push(curr.left);
            }
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data == null || data.length() == 0) {
            return null;
        }
        String[] nodes = data.split(COMMA);
        int[] pos = new int[1];
        return buildTree(nodes, pos, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    // Recursive:
    private TreeNode buildTree(String[] nodes, int[] pos, int min, int max) {
        if (pos[0] == nodes.length) {
            return null;
        }
        int val = Integer.parseInt(nodes[pos[0]]);
        if (val < min || val > max) {
            return null;
        }
        TreeNode root = new TreeNode(val);
        pos[0]++;
        root.left = buildTree(nodes, pos, min, val);
        root.right = buildTree(nodes, pos, val, max);
        return root;
    }

    // Iterative:
    public TreeNode deserialize(String data) {
        if (data == null || data.length() == 0) {
            return null;
        }
        String[] nodes = data.split(COMMA);
        int val = Integer.parseInt(nodes[0]);
        TreeNode root = new TreeNode(val);
        Stack<TreeNode> stack = new Stack<TreeNode>();
        stack.push(root);
        int i = 1, n = nodes.length;
        while (i < n) {
            val = Integer.parseInt(nodes[i++]);
            TreeNode node = new TreeNode(val);
            TreeNode currNode = stack.peek();
            
            if (val < currNode.val) {
                currNode.left = node;
            }
            else {
                // new node's value is greater than previous node, find its parent node in the stack.
                // If not found, use the one near the root node for parent node.
                TreeNode parNode = null;
                do {
                	parNode = stack.pop();
                } 
                while (!stack.isEmpty() && stack.peek().val < val);
                parNode.right = node;
            }
            stack.push(node);
        }
        return root;
    }

    // Leetcode 729: My Calendar:
    // https://leetcode.com/problems/my-calendar-i
    /**

    Implement a MyCalendar class to store your events. A new event can be added if adding the event will not cause a double booking.

    Your class will have the method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

    A double booking happens when two events have some non-empty intersection (ie., there is some time that is common to both events.)

    For each call to the method MyCalendar.book, return true if the event can be added to the calendar successfully without causing a double booking. Otherwise, return false and do not add the event to the calendar.

    Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
    Example 1:
    MyCalendar();
    MyCalendar.book(10, 20); // returns true
    MyCalendar.book(15, 25); // returns false
    MyCalendar.book(20, 30); // returns true
    Explanation: 
    The first event can be booked.  The second can't because time 15 is already booked by another event.
    The third event can be booked, as the first event takes every time less than 20, but not including 20.
    Note:

    The number of calls to MyCalendar.book per test case will be at most 1000.
    In calls to MyCalendar.book(start, end), start and end are integers in the range [0, 10^9].
    */
    class Node {
        Node left, right;
        int start, end;
        public Node(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
    Node root;
    public MyCalendar() {
        
    }
    
    public boolean book(int start, int end) {
        if (root == null) {
            root = new Node(start, end);
            return true;
        }
        else {
            return insertNode(root, start, end);
        }
    }
    
    private boolean insertNode(Node node, int start, int end) {
        if (node.start >= end) {
            if (node.left == null) {
                node.left = new Node(start, end);
                return true;
            }
            else {
                return insertNode(node.left, start, end);
            }
        }
        else if (node.end <= start) {
            if (node.right == null) {
                node.right = new Node(start, end);
                return true;
            }
            else {
                return insertNode(node.right, start, end);
            }
        }
        else {
            return false;
        }
    }
}