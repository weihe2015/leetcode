public class Trie<Value> {
    private static final int R = 256; // R can be 26, 256 or 65536
    private Node root = new Node();

    private static class TrieNode {
        private Object value;
        private TrieNode[] next = new TrieNode[R];
    }

    private void put(String key, Value val) {
        root = put(root, key, val, 0);
    }

    private TrieNode put(TrieNode node, String key, Value val, int d) {
        if (node == null) {
            node = new TrieNode();
        }
        if (d == key.length()) {
            node.val = val;
            return node;
        }
        char c = key.charAt(d);
        node.next[c] = put(node.next[c], key, val, d+1);
        return node;
    }

    public boolean contains(String key) { 
        return get(key) != null; 
    }

    public Value get(String key) {
        TrieNode node = get(root, key, 0);
        if (node == null) {
            return node;
        }
        else {
            return (Value) node.val;
        }
    }

    private TrieNode get(TrieNode node, String key, int d) {
        if (node == null) {
            return node;
        }
        if (d == key.length()) {
            return node;
        }
        char c = key.charAt(d);
        return get(node.next[c], key, d+1);
    }
}

// array version:
public class Trie {
    class TrieNode {
        private boolean isWord;
        private TrieNode[] next;
        
        public TrieNode() {
            this.isWord = false;
            this.next = new TrieNode[26];
        }
        
        public boolean getIsWord() {
            return this.isWord;
        }
        
        public void setIsWord(boolean isWord) {
            this.isWord = isWord;
        }
        
        public boolean isExistNextTrieNode(char c) {
            return this.next[c-'a'] != null;
        }

        public TrieNode getNextTrieNode(char c) {
            return this.next[c-'a'];
        }
        
        public void setNextTrieNode(char c) {
            this.next[c-'a'] = new TrieNode();
        }
    }

    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    // Inserts a word into the Trie
    public void insert(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExistNextTrieNode(c)) {
                curr.setNextTrieNode(c);
            }
            curr = curr.getNextTrieNode(c);
        }
        curr.isWord = true;
    }

    // Return true/false if the word is in the Trie
    public boolean search(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExistNextTrieNode(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return curr.isWord;
    }

    // Return True/Flase if there is any word in the Trie
    // that starts with the given prefix:
    public boolean startsWith(String prefix) {
        TrieNode curr = root;
        for (char c : prefix.toCharArray()) {
            if (!curr.isExistNextTrieNode(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return true;
    }
}

// HashMap Version:
public class Trie {
    private TrieNode root;
    public Trie() {
        root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExistNextTrieNode(c)) {
                curr.setNextTrieNode(c);
            }
            curr = curr.getNextTrieNode(c);
        }
        curr.setIsWord(true);
    }

    public boolean search(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExistNextTrieNode(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return curr.isWord;
    }

    public boolean startsWith(String prefix) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.isExistNextTrieNode(c)) {
                return false;
            }
            curr = curr.getNextTrieNode(c);
        }
        return true;
    }

    class TrieNode {
        private boolean isWord;
        private Map<Character, TrieNode> map;
        
        public TrieNode() {
            this.isWord = false;
            this.map = new HashMap<>();
        }

        public boolean getIsWord() {
            return this.isWord;
        }

        public void setIsWord(boolean isWord) {
            this.isWord = isWord;
        }

        public boolean isExistNextTrieNode(char c) {
            return this.map.containsKey(c);
        }

        public void setNextTrieNode(char c) {
            this.map.put(c, new TrieNode());
        }

        public TrieNode getNextTrieNode(char c) {
            if (!this.map.containsKey(c)) {
                return null;
            }
            else {
                return this.map.get(c);
            }
        }
    }
}

    // Leetcode 14: Longest Common Prefix
    // https://leetcode.com/problems/longest-common-prefix/
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        Trie trie = new Trie();
        trie.insert(strs);
        return trie.findLongestCommonPrefix(strs[0], strs.length);
    }
    
    class Trie {
        private TrieNode root;
        public Trie() {
            this.root = new TrieNode();
        }
        
        public void insert(String[] strs) {
            for (String str : strs) {
                insert(str);
            }
        }
        
        public void insert(String str) {
            TrieNode curr = root;
            for (char c : str.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    curr.next[c-'a'] = new TrieNode();
                }
                curr = curr.next[c-'a'];
                curr.count++;
            }
        }
        
        public String findLongestCommonPrefix(String str, int totalNum) {
            StringBuffer sb = new StringBuffer();
            TrieNode curr = root;
            for (char c : str.toCharArray()) {
                curr = curr.next[c-'a'];
                if (curr.size != totalNum) {
                    return sb.toString();
                }
                else {
                    sb.append(c);
                }
            }
            return sb.toString();
        }
    }
    
    class TrieNode {
        private int size;
        private TrieNode[] next;
        public TrieNode() {
            this.size = 0;
            this.next = new TrieNode[26];
        }
    }

// Leetcode 211 Add and Search Word
// https://leetcode.com/problems/add-and-search-word-data-structure-design
/**
Design a data structure that supports the following two operations:

void addWord(word)
bool search(word)
search(word) can search a literal word or a regular expression string containing only letters a-z or .. A . means it can represent any one letter.

Example:

addWord("bad")
addWord("dad")
addWord("mad")
search("pad") -> false
search("bad") -> true
search(".ad") -> true
search("b..") -> true
*/
class WordDictionary {
    private TrieNode root;
    /** Initialize your data structure here. */
    public WordDictionary() {
        root = new TrieNode();
    }
    
    /** Adds a word into the data structure. */
    public void addWord(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            if (!curr.containsKey(c)) {
                curr.setNextTrieNode(c);
            }
            curr = curr.getNextTrieNode(c);
        }
        curr.setIsWord(true);
    }
    
    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {
        return search(word, 0, root);
    }
    
    private boolean search(String word, int index, TrieNode node) {
        if (index == word.length()) {
            return node.getIsWord();
        }
        char c = word.charAt(index);
        if (c == '.') {
            Set<Character> keys = node.getAllKeys();
            for (char cc : keys) {
                TrieNode nextNode = node.getNextTrieNode(cc);
                if (search(word, index+1, nextNode)) {
                    return true;
                }
            }
            return false;
        }
        else {
            if (!node.containsKey(c)) {
                return false;
            }
            TrieNode nextNode = node.getNextTrieNode(c);
            return search(word, index+1, nextNode);
        }
    }
    
    class TrieNode {
        public boolean isWord;
        public Map<Character, TrieNode> childrenMap;
        public TrieNode() {
            this.isWord = false;
            childrenMap = new HashMap<Character, TrieNode>();
        }
        
        public void setIsWord(boolean isWord) {
            this.isWord = isWord;
        }
        
        public boolean getIsWord() {
            return this.isWord;
        }
        
        public void setNextTrieNode(char c) {
            this.childrenMap.put(c, new TrieNode());
        }
        
        public TrieNode getNextTrieNode(char c) {
            return this.childrenMap.get(c);
        }
        
        public boolean containsKey(char c) {
            return this.childrenMap.containsKey(c);
        }

        public Set<Character> getAllKeys() {
            return this.childrenMap.keySet();
        }
    }
}

    // Leetcode 212: Word Search II:
    // https://leetcode.com/problems/word-search-ii/
    /**
    * Given a 2D board and a list of words from the dictionary, find all words in
    * the board.
    * 
    * Each word must be constructed from letters of sequentially adjacent cell,
    * where "adjacent" cells are those horizontally or vertically neighboring. The
    * same letter cell may not be used more than once in a word.
    * 
    * For example, Given words = ["oath","pea","eat","rain"] and board =
    * 
    * [ ['o','a','a','n'], ['e','t','a','e'], ['i','h','k','r'], ['i','f','l','v']
    * ] Return ["eat","oath"].
    */
    private static int[][] dirs = {{0,1}, {0,-1}, {1,0}, {-1,0}};
    // Running Time: O(m*n * L)
    // Build Trie:   n * wordMaxLength
    // Search: boardWidth * boardHeight * (4^wordMaxLength + wordMaxLength[Trie Search])
    public List<String> findWords(char[][] board, String[] words) {
        List<String> result = new ArrayList<>();
        if (board == null || board.length == 0) {
            return result;
        }
        Trie trie = new Trie();
        trie.insert(words);
        TrieNode root = trie.getRootNode();
        
        int m = board.length, n = board[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                findWords(result, board, root, i, j, m, n);
            }
        }
        return result;
    }
    
    private void findWords(List<String> result, char[][] board, TrieNode root, int i, int j, int m, int n) {
        if (i < 0 || i >= m || j < 0 || j >= n) {
            return;
        }
        char c = board[i][j];
        if (c == '*' || root.next[c-'a'] == null) {
            return;
        }
        root = root.next[c-'a'];
        if (root.isWord) {
            result.add(root.word);
            root.isWord = false;
        }
        board[i][j] = '*';
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            findWords(result, board, root, x, y, m, n);
        }
        board[i][j] = c;
    }
    
    class Trie {
        private TrieNode root;
        public Trie() {
            this.root = new TrieNode();
        }
        
        public TrieNode getRootNode() {
            return this.root;
        }
        
        public void insert(String[] words) {
            for (String word : words) {
                insert(word);
            }
        }
        
        public void insert(String word) {
            TrieNode curr = root;
            for (char c : word.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    curr.next[c-'a'] = new TrieNode();
                }
                curr = curr.next[c-'a'];
            }
            curr.word = word;
            curr.isWord = true;
        }
    }
    
    class TrieNode {
        private boolean isWord;
        private String word;
        private TrieNode[] next;
        
        public TrieNode() {
            this.isWord = false;
            next = new TrieNode[26];
        }
    }

    // Leetcode 472: Concatenated Words:
    // https://leetcode.com/problems/concatenated-words/
    /**
    Given a list of words (without duplicates), please write a program that returns all concatenated words in the given list of words.
    A concatenated word is defined as a string that is comprised entirely of at least two shorter words in the given array.

    Example:
    Input: ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]

    Output: ["catsdogcats","dogcatsdog","ratcatdogcat"]

    Explanation: "catsdogcats" can be concatenated by "cats", "dog" and "cats"; 
    "dogcatsdog" can be concatenated by "dog", "cats" and "dog"; 
    "ratcatdogcat" can be concatenated by "rat", "cat", "dog" and "cat".
    Note:
    The number of elements of the given array will not exceed 10,000
    The length sum of elements in the given array will not exceed 600,000.
    All the input string will only include lower case letters.
    The returned elements order does not matter.
    */
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        List<String> result = new ArrayList<String>();
        
        Trie trie = new Trie();
        trie.addWords(words);
        
        for (String word : words) {
            if (search(word, 0, 0, trie)) {
                result.add(word);
            }
        }
        return result;
    }
    
    private boolean searchWord(String word, Trie trie, int count) {
        TrieNode currNode = trie.root;
        if (word.length() == 0) {
            return count > 1;
        }
        for (int i = 0, n = word.length(); i < n; i++) {
            char c = word.charAt(i);
            if (currNode.next[c-'a'] == null) {
                return false;
            }
            else {
            	String substr = word.substring(i+1);
                if (currNode.next[c-'a'].isWord && searchWord(substr, trie, count+1)) {
                    return true;
                }
                else {
                    currNode = currNode.next[c-'a'];
                }
            }
        }
        return false;
    }
    
    class Trie {
        TrieNode root;
        public Trie() {
            root = new TrieNode();
        }
        
        public void addWords(String[] words) {
            for (String word : words){
                addWord(word);
            }
        }
        
        public void addWord(String word) {
            TrieNode node = root;
            for (char c : word.toCharArray()) {
                if (node.next[c-'a'] == null) {
                    node.next[c-'a'] = new TrieNode();
                }
                node = node.next[c-'a'];
            }
            node.isWord = true;
        }
        
        public boolean searchWord(String word) {
            TrieNode node = root;
            for (char c : word.toCharArray()) {
                if (node.next[c-'a'] == null) {
                    return false;
                }
                node = node.next[c-'a'];
            }
            return node.isWord;
        }
    }
     
    class TrieNode {
        boolean isWord;
        TrieNode[] next;
        public TrieNode() {
            next = new TrieNode[26];
        }
    }