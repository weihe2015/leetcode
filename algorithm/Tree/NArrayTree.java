public class NArrayTree {
    class Node {
        public int val;
        public List<Node> children;
    
        public Node() {}
        public Node(int _val) {
            this.val = _val;
            this.children = new ArrayList<>();
        }
        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    // Preorder Traversal:
    // Leetcode 589: https://leetcode.com/problems/n-ary-tree-preorder-traversal/
    // Recursive:
    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        preorder(result, root);
        return result;
    }
    
    private void preorder(List<Integer> result, Node node) {
        if (node == null) {
            return;
        }
        result.add(node.val);
        for (Node childNode : node.children) {
            preorder(result, childNode);
        }
    }

    // Iterative:
    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<Node>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node currNode = stack.pop();
            result.add(currNode.val);
            for (int i = currNode.children.size() - 1; i >= 0; i--) {
                stack.push(currNode.children.get(i));
            }
        }
        return result;
    }

    // Postorder Traversal:
    // Recursive:
    public List<Integer> postorder(Node root) {
        List<Integer> result = new ArrayList<Integer>();
        postorder(result, root);
        return result;
    }
    
    private void postorder(List<Integer> result, Node node) {
        if (node == null) {
            return;
        }
        for (Node childNode : node.children) {
            postorder(result, childNode);
        }
        result.add(node.val);
    }

    // Iterative:
    public List<Integer> postorder(Node root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<Node>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node currNode = stack.pop();
            result.add(currNode.val);
            for (Node childNode : currNode.children) {
                stack.push(childNode);
            }
        }
        Collections.reverse(result);
        return result;
    }

    // N-ary Tree Level Order Traversal
    // Leetcode 429: https://leetcode.com/problems/n-ary-tree-level-order-traversal/
    // Recursive:
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrder(result, root, 0);
        return result;
    }
    
    private void levelOrder(List<List<Integer>> result, Node node, int level) {
        if (node == null) {
            return;
        }
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);
        list.add(node.val);
        for (Node childNode : node.children) {
            levelOrder(result, childNode, level+1);
        }
    }

    // Iterative:
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<Node> queue = new LinkedList<Node>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<Integer>();
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                Node currNode = queue.poll();
                list.add(currNode.val);
                for (Node childNode : currNode.children) {
                    queue.offer(childNode);
                }
            }
            result.add(list);
        }
        return result;
    }

    // Max Depth of N-Array Tree:
    // Leetcode 559: https://leetcode.com/problems/maximum-depth-of-n-ary-tree/
    // Recursive:
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }
        int max = 0;
        for (Node childNode : root.children) {
            int depth = maxDepth(childNode);
            if (depth > max) {
                max = depth;
            }
        }
        return max+1;
    }

    // Iterative Solution: Use Level Order Traversal:
    public int maxDepth(Node root) {
        int depth = 0;
        if (root == null) {
            return depth;
        }
        Queue<Node> queue = new LinkedList<Node>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            depth++;
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                Node currNode = queue.poll();
                for (Node childNode : currNode.children) {
                    queue.offer(childNode);
                }
            }
        }
        return depth;
    }

    /**
Serialization is the process of converting a data structure or object into a sequence of bits 
so that it can be stored in a file or memory buffer, 
or transmitted across a network connection link to be reconstructed later 
in the same or another computer environment.

Design an algorithm to serialize and deserialize an N-ary tree. 
An N-ary tree is a rooted tree in which each node has no more than N children. 
There is no restriction on how your serialization/deserialization algorithm should work. 
You just need to ensure that an N-ary tree can be serialized to a string and 
this string can be deserialized to the original tree structure.

For example, you may serialize the following 3-ary tree
         1
    3    2    4
  5   6
as [1 [3[5 6] 2 4]]. You do not necessarily need to follow this format, 
so please be creative and come up with different approaches yourself.
*/
// https://www.lintcode.com/problem/serialize-and-deserialize-n-ary-tree
// https://leetcode.com/problems/serialize-and-deserialize-n-ary-tree

    private static final String BAR = "|";
    private static final String REGX_BAR = "\\|";
    private static final String COMMA = ",";
    private static final String POUND = "#";
    
    // 1|3,2,4|#3|5,6|#2||#4||
    public String serialize(Node root) {
        if (root == null) {
            return "";
        }
        // Level order traversal
        StringBuffer sb = new StringBuffer();
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                Node currNode = queue.poll();
                sb.append(currNode.label).append(BAR);
                for (Node child : currNode.children) {
                    sb.append(child.label).append(COMMA);
                    queue.offer(child);
                }
                if (!currNode.children.isEmpty()) {
                    sb.setLength(sb.length()-1);
                }
                sb.append(BAR).append(POUND);
            }
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    public Node deserialize(String data) {
       if (data == null || data.length() == 0) {
           return null;
       }    
       String[] datas = data.split(POUND);
       Node root = convertTextToNode(datas[0], null);
       if (root == null) {
           return null;
       }
       int idx = 1;
       int n = datas.length;
       
       Queue<Node> queue = new LinkedList<>();
       queue.offer(root);
       
      while (idx < n) {
          int size = queue.size();
          for (int i = 1; i <= size; i++) {
              Node currNode = queue.poll();
              for (Node child : currNode.children) {
                  convertTextToNode(datas[idx++], child);
                  queue.offer(child);
              }
          }
      }
       
      return root;
    }
    
    private Node convertTextToNode(String text, Node root) {
        String[] datas = text.split(REGX_BAR);
        int n = datas.length;
        if (n == 0) {
            return null;
        }
        int num = Integer.parseInt(datas[0]);
        // for initial root node, we create the root node.
        // But for neighbors the node is created and we don't need to 
        // create them again.
        if (root == null) {
            root = new Node(num);
        }
        if (n == 1) {
            return root;
        }
        String[] childrenTexts = datas[1].split(COMMA);
       
        for (String childText : childrenTexts) {
            int childNum = Integer.parseInt(childText);
            Node childNode = new Node(childNum);
            root.children.add(childNode);
        }
        return root;
    }
}