public class LinkedList {
    // Leetcode 2: https://leetcode.com/problems/add-two-numbers/
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0), curr = head;
        int sum = 0;
        while (l1 != null || l2 != null) {
            if (l1 != null) {
                sum += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                sum += l2.val;
                l2 = l2.next;
            }
            curr.next = new ListNode(sum % 10);
            curr = curr.next;
            sum /= 10;
        }
        if (sum == 1) {
            curr.next = new ListNode(sum);
        }
        return head.next;
    }

    // Leetcode 206: Reverse LinkedList: https://leetcode.com/problems/reverse-linked-list/
    // Iterative
    public ListNode reverseList(ListNode head) {
        ListNode newHead = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;
        }
        return newHead;
    }

    // recursive
    public ListNode reverseList(ListNode head) {
        return reverseList(head, null);
    }

    public ListNode reverseList(ListNode head, ListNode newHead) {
        if (head == null) {
            return newHead;
        }
        ListNode next = head.next;
        head.next = newHead;
        newHead = head;
        return reverseList(next, newHead);
    }

    public ListNode reverseList(ListNode head) {
        ListNode newHead = new ListNode(0), curr = head;
        newHead.next = head;

        int max = countListLength(head);
        for (int i = 1; i < max; i++) {
            ListNode nextNode = curr.next;
            curr.next = nextNode.next;
            nextNode.next = newHead.next;
            newHead.next = nextNode;
        }
        return newHead.next;
    }

    private int countListLength(ListNode head) {
        int count = 0;
        while (head != null) {
            count++;
            head = head.next;
        }
        return count;
    }

    // Leetcode 92: https://leetcode.com/problems/reverse-linked-list-ii/
    public ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode newHead = new ListNode(0), prev = newHead, curr = head;
        newHead.next = head;
        for (int i = 1; i < m; i++) {
            prev = prev.next;
            curr = curr.next;
        }
        
        for (int i = 1, max = n-m; i <= max; i++) {
            ListNode nextNode = curr.next;
            curr.next = nextNode.next;
            nextNode.next = prev.next;
            prev.next = nextNode;
        }
        return newHead.next;
    }

    // Leetcode 203: Remove LinkedList Element:
    public ListNode removeElements(ListNode head, int val) {
        ListNode newHead = new ListNode(0), prev = newHead, curr = head;
        newHead.next = head;
        while (curr != null) {
            if (curr.val == val) {
                prev.next = curr.next;
            }
            else {
                prev = prev.next;
            }
            curr = curr.next;
        }
        return newHead.next;
    }

    public ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return null;
        }

        // remove element in the header
        while (head != null && head.val == val) {
            head = head.next;
        }
        
        ListNode curr = head;
        // move the curr pointer only when curr.next.val != val:
        // Handle case: 1 -> 2 -> 2 -> 3, val = 2
        while (curr != null && curr.next != null) {
            if (curr.next.val == val) {
                curr.next = curr.next.next;
            }
            else {
                curr = curr.next;
            }
        }
        return head;
    }

    // Leetcode 019: remove Nth Node from End of List:
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if(head == null) {
            return head;
        }
        ListNode curr = head, prev = head;
        for(int i = 0; i < n; i++){
            curr = curr.next;
        }
        // if remove the first node
        if(curr == null){
            head = head.next;
        }
        else {
            // Move curr pointer to last element:
            while (curr != null && curr.next != null){
                curr = curr.next;
                prev = prev.next;
            }
            prev.next = prev.next.next;
        }
        return head;
    }

    // two pass solution
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode curr = head;
        // Index of previous Node of deleted Node:
        int idx = countLinkedListLength(curr) - n;
        // delete header
        if (idx == 0) {
            head = head.next;
        }
        else {
            // move until the node before the deleted node
            idx--;
            curr = head;
            while (idx > 0) {
                curr = curr.next;
                idx--;
            }
            curr.next = curr.next.next;
        }
        return head;
    }
    
    private int countLinkedListLength(ListNode head) {
        int count = 0;
        while (head != null) {
            count++;
            head = head.next;
        }
        return count;
    }

    // Leetcode 141: https://leetcode.com/problems/linked-list-cycle/
    // Given a linked list, determine if it has a cycle in it.
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        ListNode slow = head, fast = head.next;
        while (slow != fast) {
            if (fast == null || fast.next == null) {
                return false;
            }
            fast = fast.next.next;
            slow = slow.next;
        }
        return true;
    }

    // Leetcode 142: https://leetcode.com/problems/linked-list-cycle-ii/
    // Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
    public ListNode detectCycle(ListNode head) {
        ListNode fast = head, slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
            if (fast == slow) {
                slow = head;
                while (slow != fast) {
                    slow = slow.next;
                    fast = fast.next;
                }
                return slow;
            }
        }
        return null;
    }
}