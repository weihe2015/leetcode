/**
 * 把边界确定更加准确点，[left,end]确定为闭区间，只要结果不在这个区间，就+1或者-1
 * 正确的终结条件：low > high, 也就是搜索空间为空
 *
 * 规则就是：
 * int low = 0, high = nums.length - 1;
 * 先设：while (low <= high)
 * 然后根据题目要求，看low和high是闭区间还是开区间，再决定while的结束条件
 * 如果是边界问题，最后还要用nums[left]和nums[right]来判断
 *
 * 步骤：
 * 1. 大体框架必然是二分，low=0, high=nums.length-1, 那么循环的target与array[mid]的比较必不可少，这是基本框架;
 * 2. 循环的条件可以先写一个粗略的，比如原始的while(left<=right)就行，这个循环条件在后面可能需要修改；
 * 3. 确定每次二分的过程，要保证所求的元素必然不在被排除的元素中，换句话说，所求的元素要么在保留的其余元素中，要么可能从一开始就不存在于原始的元素中；
 * 4. 检查每次排除是否会导致保留的候选元素个数的减少？如果没有，分析这个边界条件，如果它能导致循环的结束，那么没有问题；
 *      否则，就会陷入死循环。为了避免死循环，需要修改循环条件，使这些情况能够终结。
 *      新的循环条件可能有多种选择：while(left<right)、while(left<right-1)等等，
 *      这些循环条件的变种同时意味着循环终止时候选数组的形态。
 * 5. 结合新的循环条件，分析终止时的候选元素的形态，并对分析要查找的下标是否它们之中、同时是如何表示的。
 *
 * 规律：
 *  1. low = mid + 1, high = mid - 1 => while (low <= high)
 *  2. low = mid, high = mid - 1 => while (low < high - 1)
 *  3. low = mid, high = mid, => while (low < high - 1)
 *  4. low = mid + 1, high = mid => while (low < high)
 *
 *  如果 low = mid, 不管high怎么样，while都是(low < high - 1)
 *  如果 high = mid，while 就是 (low < high)
 *
*/
public class BinarySearch {
    /**
     * Find element in a sorted array:
     * If the array has duplicate elements, it cannot ensure which element to be find.
     * 左闭右闭区间:
     *  1. low = 0, high = nums.length - 1;
     *  2. while (low < high) //如果两边都是闭区间，就可以 low < high 或者 low <= high 因为永远low != high
     *  3. low = mid+1, high = mid-1 左闭右闭
     *
    */
    public int binarySearch(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
           return -1;
        }
        int low = 0, high = nums.length-1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            else if (nums[mid] < target) {
               low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return -1;
    }

    /**
     * 左闭右开区间
     * 1. low = 0, high = nums.length - 1;
     * 2. while (low < high)  //如果有一边是开区间，就一定是 low < high，不可以 low <= high 因为有可能low == high
     *     ex: {1,3,5,7,9}, search 2.
     *     low == mid < high, low = 0, mid = 0, high = 1, next while loop, high = 0, so that the loop will not end
     * 3. low = mid+1, high = mid;
     *
    */
    public int binarySearch2(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
           return -1;
        }
        int low = 0, high = nums.length;
        while (low < high) {
           int mid = low + (high - low) / 2;
           if (nums[mid] == target) {
             return mid;
           }
           else if (nums[mid] < target) {
               low = mid + 1;
           }
           else {
               high = mid;
           }
        }
        return -1;
    }

    /**
     * Binary search for finding first appearence of item in an array, which may have duplicates
     * 循环不变式：
     *    如果key存在于数组，那么key第一次出现的下标x一定在[left,right]中，且有array[left]<=key, array[right]>=key。
     *
     * 初始化：
     *    第一轮循环开始之前，处理的数组是[0,n-1]，这时显然成立。
     *
     * 保持：
     *    每次循环开始前，如果key存在于原数组，那么x存在于待处理数组array[left ... right]中。
     *    对于array[mid]<key，array[left ... mid]均小于key，x只可能存在于array[mid+1 ... right]中。数组减少的长度为mid-left+1，至少为1。
     *    否则，array[mid]>=key, array[mid]是array[mid, ..., right]中第一个大于等于key的元素，后续的等于key的元素（如果有）不可能对应于下标x，舍去。
     *      此时x在[left, ..., mid]之中。
     *      数组减少的长度为right-(mid+1)+1，即right-mid，根据while的条件，当right==mid时为0。此时right==left，循环结束。
     *
     * 终止：
     *    此时left>=right。在每次循环结束时，left总是x的第一个可能下标，array[right]总是第一个等于key或者大于key的元素。
     *    那么对应于left==right的情况，检查array[left]即可获得key是否存在，若存在则下标为x；
     *    对于left>right的情况，其实是不用考虑的。因为left==上一次循环的mid+1，而mid<=right。
     *        若mid+1>right，意味着mid == right，但此时必有left == right，这一轮循环从开始就不可能进入。
    */
    public int binarySearch3(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        while (low < high) {
            int mid = low + (high - low) / 2;
            // 对于array[mid] < key，array[left...mid]均小于key，x只可能存在于array[mid+1...right]中
            if (nums[mid] < target) {
                low = mid + 1;
            }
            else if (nums[mid] > target) {
                high = mid - 1;
            }
            // 舍去 index > high 的元素，因为这些元素都不可能了。
            else {
                high = mid; // bias on the left side
            }
        }
        // 最后只可能是left == right, 只需检查nums[left]的值便可, 不用考虑 left > right的情况
        if (nums[low] == target) {
            return low;
        }
        else {
            return -1;
        }
    }

    /**
     * https://www.cnblogs.com/wuyuegb2312/archive/2013/05/26/3090369.html
     * Binary search for finding last appearence of item in an array, which may have duplicates
     * 循环不变式：
     *    如果key存在于数组，那么key最后一次出现的下标x一定在[left,right]中，且有array[left]<=key, array[right]>=key。
     *
     * 初始化：
     *    第一轮循环开始之前，处理的数组是[0,n-1]，这时显然成立。
     *
     * 保持：
     *    每次循环开始前，如果key存在于原数组，那么x存在于待处理数组array[left, ..., right]中。
     *    对于array[mid]<key，array[left, ..., mid]均小于key，x只可能存在于array[mid+1, ..., right]中。数组减少的长度为mid-left+1，至少为1。
     *    对于array[mid]==key, array[mid]是array[left, ..., mid]中最后一个值为key的元素，那么x的候选只能在array[mid, ... ,right]中，数组减少长度为mid-left。
     *       除非left == right或left == right-1，否则数组长度至少减小1。
     *       由于while的条件，只有后一种情况可能发生，如果不进行干预会陷入死循环，加入判断分支即可解决。
     *
     * 修改版：
     *    在left+1==right且array[left]=key时，如果不加以人为干预会导致死循环。既然最终需要干预，干脆把需要干预的时机设置为终止条件就行了
     *    使用while(left<right-1)可以保证每次循环时数组长度都会至少减一，终止时数组长度可能为2(left+1==right)、1(left==mid，上一次循环时right取mid==left)，
     *       但是不可能为0。(每一次循环前总有left<=mid<=right，无论令left=mid还是令right=mid，都不会发生left>right)。
     *       同3一样，right总是指向数组中候选的最后一个可能为key的下标，此时只需先检查right后检查left是否为key就能确定x的位置。
     *       这样就说明了循环不变式的保持和终止，就不再形式化地写下来了。
     *    对于两种情况的合并：array[mid] == key时，mid有可能是x，不能将其排除；
     *      array[mid]<key时，如果让left = mid+1，不会违反循环不变式的条件。
     *      但是由上面的讨论可知，将left=mid也是可以的，在达到终止条件前能保证数组长度单调减少。因此把两种情况合并成最终形式。
    */
    public int binarySearch4(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        while (low < high - 1) {
            int mid = low + (high - low) / 2;
            if (nums[mid] <= target) {
                low = mid;
            }
            else {
                high = mid;
            }
        }
        if (nums[high] == target) {
            return high;
        }
        else if (nums[low] == target) {
            return low;
        }
        else {
            return -1;
        }
    }

    /**
     * My own solution to: Binary search for finding last appearence of item in an array, which may have duplicates
     * 1) 如果nums[mid] > target, 就意味着目标值肯定在[low...mid]里面，排除nums[mid]，所以将high=mid-1
     * 2) 如果nums[mid] == target, 不能确定边界，移动low到mid
     * 3) 如果nums[mid] < target，就意味着目标值在[mid...high]里面，所以将low=mid+1
     *   可以合并2和3为 nums[mid] <= target, low = mid;
     *
    */
    public int binarySearch44(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        while (low < high - 1) {
            int mid = low + (high - low)/2;
            if (nums[mid] > target) {
                high = mid - 1;
            }
            else if (nums[mid] == target) {
                low = mid; // bias on the right side
            }
            else {
                low = mid + 1;
            }
        }
        // 结束的时候有可能是low = high - 1 => low < high
        // ex: {0,0} find 0. end: low = 0, high = 1
        if (nums[high] == target) {
            return high;
        }
        else if (nums[low] == target) {
            return low;
        }
        else {
            return -1;
        }
    }

    /**
     * Binary Search for returning 刚好小于key的元素下标x，如果不存在返回-1
     * 循环不变式：
     *     如果原始数组中存在比key小的元素，那么原始数组中符合要求的元素存在于待处理的数组。
     *
     * 初始化：
     *     第一轮循环开始之前，处理的数组是[0,n-1]，这时显然成立。
     *
     * 保持：
     *     每次循环开始前，x存在于待处理数组array[left, ..., right]中。
     *     先用一个循环的条件为left <= right，违反则意味着x不存在。写下array[mid]的比较判断分支：
     *         (1) array[mid]<key, 意味着x只可能在array[mid, ..., right]之间，下一次循环令left = mid，
     *           数组长度减少了(mid-1)-left+1 == mid-left，这个长度减少量只有在right-left<=1时小于1。
     *         (2) array[mid]>=key，意味着x只可能在array[left ... mid-1]之间，
     *              下一次循环令right = mid-1，同样推导出数组长度至少减少了1。
     *      这样，把循环条件缩小为right>left+1，和4一样，保证了(1)中每次循环必然使数组长度减少，
     *      而且终止时也和4的情况类似：终止时待处理数组长度只能为2或1或者空（left > right）。
     *
     * 终止：
     *      接着保持中的讨论，结束时，符合的x要么在最终的数组中，要么既不在最终的数组中也不在原始的数组中（因为每一次循环都是剔除不符合要求的下标）。
     *      数组长度为2时，right==left+1，此时先检查right后检查left。如果都不符合其值小于key，那么返回-1。数组长度为1时，只用检查一次；数组长度为0时，这两个都是无效的，检查时仍然不符合条件。把这三种情况综合起来，可以写出通用的检查代码。
     *         反过来，根据精简的代码来理解这三种情况比正向地先给出直观方法再精简要难一些。
    */
    public int binarySearch5(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        while (low < high-1) {
            int mid = low + (high - low)/2;
            if (nums[mid] < target) {
                low = mid;
            }
            else {
                high = mid-1;
            }
        }
        if (nums[high] < target) {
            return high;
        }
        else if (nums[low] < target) {
            return low;
        }
        else {
            return -1;
        }
    }

    /**
     * Binary Search for returning 刚好大于key的元素下标x, 如果不存在返回-1
     * 循环不变式：
     *    如果原始数组中存在比key大的元素，那么原始数组中符合要求的元素对应下标x存在于待处理的数组。
     * 初始化：
     *    第一轮循环开始之前，处理的数组是[0,n-1]，这时显然成立。
     * 保持：
     *    每次循环开始前，x存在于待处理数组array[left ... right]中。
     *    仍然先把执行while循环的条件暂时写为right>=left，违反则意味着x不存在。
     *       写下array[mid]的比较判断分支：
     *         (1) array[mid]<=key, 意味着x只可能在array[mid+1 ... right]之间，
     *             下一次循环令left = mid，数组长度减少了mid-left+1，减少量至少为1。
     *         (2)array[mid]>key，意味着x只可能在array[left ... mid]之间，下一次循环令right = mid，数组长度减少了right-(mid+1)+1== right-mid，只有在right==mid时为0，
     *              此时left==right==mid。
     *              因此，循环条件必须由right>=left收缩为right>left才能避免left==right时前者会进入的死循环。
     * 终止：由循环的终止条件，此时left>=right。
     *          类似2的分析，left>right是不可能的，只有left==right。此时检查array[right]>key成立否就可以下结论了，
     *              它是唯一的候选元素。
     *
     *
    */
    public int binarySearch6(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int low = 0, high = nums.length - 1;
        while (low < high) {
            int mid = low + (high - low)/2;
            if (nums[mid] <= target) {
                low = mid + 1;
            }
            else {
                high = mid;
            }
        }
        // 这时 low 有可能越界，所以只需看nums[high]
        if (nums[high] > target) {
            return high;
        }
        else {
            return -1;
        }
    }
}
