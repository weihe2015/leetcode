public class Solution {
    // Longest Common Substring:
    // Leetcode 718: Maximum Length of Repeated Subarray
    // https://leetcode.com/problems/maximum-length-of-repeated-subarray/
    /**
    Given two integer arrays A and B, return the maximum length of an subarray that appears in both arrays.

    Example 1:
    Input:
    A: [1,2,3,2,1]
    B: [3,2,1,4,7]
    Output: 3
    Explanation:
    The repeated subarray with maximum length is [3, 2, 1].
    Note:
    1 <= len(A), len(B) <= 1000
    0 <= A[i], B[i] < 100
    */
    // Running Time Complexity: O(m * n), Space Complexity: O(m * n)
    // https://leetcode-cn.com/problems/maximum-length-of-repeated-subarray/solution/zhe-yao-jie-shi-ken-ding-jiu-dong-liao-by-hyj8/
    public int findLength(int[] A, int[] B) {
        int m = A.length;
        int n = B.length;
        if (m == 0 || n == 0) {
            return 0;
        }
        int maxLen = 0;
        // 长度为i，末尾项为A[i-1]的子数组，与，长度为j，末尾项为B[j-1]的子数组，二者的最大公共后缀子数组长度。
        int[][] dp = new int[m+1][n+1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (A[i-1] == B[j-1]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                    maxLen = Math.max(maxLen, dp[i][j]);
                }
                else {
                    dp[i][j] = 0;
                }
            }
        }
        return maxLen;
    }

    // Running Time Complexity: O(m * n), Space Complexity: O(n)
    public int findLength(int[] A, int[] B) {
        int m = A.length;
        int n = B.length;
        if (m == 0 || n == 0) {
            return 0;
        }
        int[] dp = new int[n+1];
        int maxLen = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = n; j >= 1; j--) {
                if (A[i-1] == B[j-1]) {
                    dp[j] = dp[j-1] + 1;
                    maxLen = Math.max(maxLen, dp[j]);
                }
                else {
                    dp[j] = 0;
                }
            }
        }
        return maxLen;
    }

    // Longest Palindromic Substring
    // Leetcode 5: https://leetcode.com/problems/longest-palindromic-substring/
    /**
    Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
        Example 1:
        Input: "babad"
        Output: "bab"

    Basic idea: when substring length is less than 3, only compare s.charAt(l) == s.charAt(r)
        when substring length is >= 3, use s.charAt(l) == s.charAt(r) && dp[l+1][r-1]
         bdb is palindrome
        l              r
        a   b   d   b  a is palindome
           l+1      r-1
        dp[i][j] = isPalindrome(s.substring(i,j))
        ==> dp[i][j] = dp[i+1][j-1] && s.charAt(i) == s.charAt(j)
    */
    // Running Time: O(n^2), Space Complexity: O(n^2)
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int n = s.length();
        // dp[i][j]: substring(i, j) is the palindrome
        boolean[][] dp = new boolean[n][n];
        int start = -1;
        int end = -1;
        int max = -1;
        for (int r = 0; r < n; r++) {
            for (int l = r; l >= 0; l--) {
                // If substring is at least 3 characters long, use previous step to calculate current step:
                if (r - l >= 3) {
                    dp[l][r] = s.charAt(l) == s.charAt(r) && dp[l+1][r-1];
                }
                else {
                    dp[l][r] = s.charAt(l) == s.charAt(r);
                }
                // If s.substring(i, j+1) is palindrome:
                int len = r - l + 1;
                if (dp[l][r] && max < len) {
                    max = len;
                    start = l;
                    end = r;
                }
            }
        }
        // no substring palindrome is found:
        if (start == -1) {
            return "";
        }
        return s.substring(start, end+1);
    }

    public int fibonacci(int n) {
        // write your code here
        if (n == 1) {
            return 0;
        }
        else if (n == 2) {
            return 1;
        }
        return fibonacci(n-1) + fibonacci(n-2);
    }

    public int fibonacci(int n) {
        // write your code here
        int[] mem = new int[n+1];
        for (int i = 0; i <= n; i++) {
            mem[i] = -1;
        }
        return fibonacci(n, mem);
    }

    private int fibonacci(int n, int[] mem) {
        if (mem[n] != -1) {
            return mem[n];
        }
        if (n == 0) {
            mem[n] = 0;
        }
        else if (n <= 2) {
            mem[n] = 1;
        }
        else {
            mem[n] = fibonacci(n-1, mem) + fibonacci(n-2, mem);
        }
        return mem[n];
    }

    public int fibonacci(int n) {
        // write your code here
        if (n <= 0) {
            return 0;
        }
        int[] mem = new int[n+1];
        mem[0] = 0;
        mem[1] = 1;
        for (int i = 2; i <= n; i++) {
            mem[i] = mem[i-1] + mem[i-2];
        }
        return mem[n];
    }

    // Leetcode 53: Maximum Subarray:
    // https://leetcode.com/problems/maximum-subarray/
    /**
    Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
    Example:

    Input: [-2,1,-3,4,-1,2,1,-5,4],
    Output: 6
    Explanation: [4,-1,2,1] has the largest sum = 6.
    Follow up:

    If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
    */
    // maxArray[i] = maxArray[i-1] > 0 ? (maxArray[i-1] + nums[i]) : nums[i]
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length, max = nums[0];
        int[] dp = new int[n];
        dp[0] = nums[0];
        for (int i = 1; i < n; i++) {
            if (dp[i-1] > 0) {
                dp[i] = dp[i-1] + nums[i];
            }
            else {
                dp[i] = nums[i];
            }
            // or : dp[i] = Math.max(nums[i], dp[i-1] + nums[i]);
            max = Math.max(max, dp[i]);
        }
        return max;
    }

    // Leetcode 120: Triangle: https://leetcode.com/problems/triangle/
    /*
    Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row below.

    For example, given the following triangle
    [
        [2],
        [3,4],
    [6,5,7],
    [4,1,8,3]
    ]
    The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

    Note:
    Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the triangle.
    */
    // 2D Array:
    public int minimumTotal(int[][] triangle) {
        if (triangle == null || triangle.length == 0) {
            return 0;
        }
        int m = triangle.length;
        int[][] dp = new int[m][m];
        dp[0][0] = triangle[0][0];

        for (int i = 1; i < m; i++) {
            for (int j = 0, n = triangle[i].length; j < n; j++) {
                int l = Math.max(0, j-1);
                int r = Math.min(j, triangle[i-1].length-1);
                dp[i][j] = Math.min(dp[i-1][l], dp[i-1][r]) + triangle[i][j];
            }
        }
        // Use last row to get the min cost result:
        int minCost = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            minCost = Math.min(minCost, dp[m-1][i]);
        }
        return minCost;
    }

    // Buttom up DP: 1 Dimension: Complexity: O(m * m), Space Complexity: O(m)
    public int minimumTotal(List<List<Integer>> triangle) {
        if (triangle == null || triangle.isEmpty()) {
            return 0;
        }
        int m = triangle.size();
        int[] dp = new int[m+1];
        for (int i = m-1; i >= 0; i--) {
            List<Integer> list = triangle.get(i);
            for (int j = 0, n = list.size(); j < n; j++) {
                dp[j] = Math.min(dp[j], dp[j+1]) + list.get(j);
            }
        }
        return dp[0];
    }

    // Leetcode 300: Longest Increasing Subsequence
    // https://leetcode.com/problems/longest-increasing-subsequence/
    /**
    Given an unsorted array of integers, find the length of longest increasing subsequence.

    Example:

    Input: [10,9,2,5,3,7,101,18]
    Output: 4
    Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.
    Note:

    There may be more than one LIS combination, it is only necessary for you to return the length.
    Your algorithm should run in O(n2) complexity.
    Follow up: Could you improve it to O(n log n) time complexity?
    */
    // Running Time Complexity: O(N^2), Space Complexity: O(N)
    public int lengthOfLIS(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int maxLen = 1, n = nums.length;
        int[] dp = new int[n];
        for (int i = 0; i < n; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    dp[i] = Math.max(dp[i], dp[j]+1);
                }
            }
            maxLen = Math.max(maxLen, dp[i]);
        }
        return maxLen;
    }

    /**
    Knapsack problem
    完全背包 vs 0/1 背包
    无重复背包即0/1背包，得到的结果dp[i]是根据之前的结果来的，换句话说，是否选入当前物品是根据之前没有当前物品的子结果而做出的选择，是为了保证每一个物品的唯一性。
    有重复背包即完全背包，每个物品都有无限的可重复性，所以当前的结果要从之前已经出现过当前物品的子结果中得到。
    所以循环的方向要反一下，一个从下到上，一个从上到下。
    Ex: 0/1 背包: for (int j = m; j >= 1; j--)
        完全 背包: for (int j = 1; j <= m; j++)

    当然拿到问题后，需要做到以下几个步骤：
    1.分析是否为背包问题。
    2.是以上三种背包问题中的哪一种。
    3.是0-1背包问题还是完全背包问题。也就是题目给的nums数组中的元素是否可以重复使用。
    4.如果是组合问题，是否需要考虑元素之间的顺序。需要考虑顺序有顺序的解法，不需要考虑顺序又有对应的解法。

    接下来讲一下背包问题的判定
    背包问题具备的特征：给定一个target，target可以是数字也可以是字符串，再给定一个数组nums，nums中装的可能是数字，也可能是字符串，问：能否使用nums中的元素做各种排列组合得到target。

    背包问题技巧：
    1.如果是0-1背包，即数组中的元素不可重复使用，nums放在外循环，target在内循环，且内循环倒序；
    for (int i = 1; i <= n; i++) {
        int currItem = nums[i-1];
        for (int j = m; j >= 1; j--) {

        }
    }

    2.如果是完全背包，即数组中的元素可重复使用，nums放在外循环，target在内循环。且内循环正序。
    for (int i = 1; i <= n; i++) {
        int currItem = nums[i-1];
        for (int j = 1; j <= m; j++) {

        }
    }

    3.如果组合问题需考虑元素之间的顺序，需将target放在外循环，将nums放在内循环。
    for (int j = 1; j <= m; j++) {
        for (int i = 1; i <= n; i++) {
            int currItem = nums[i-1];
        }
    }
    */

    // 背包问题一：
    // 在n个物品中挑选若干物品装入背包，最多能装多满？假设背包的大小为m，每个物品的大小为A[i]
    // https://www.lintcode.com/problem/backpack
    /**
    Example
    Example 1:
        Input:  [3,4,8,5], backpack size=10
        Output:  9 (we choose item 4 and item 5)

    Example 2:
        Input:  [2,3,5,7], backpack size=12
        Output:  12 (we choose item 5 and item 7)
    */
    /**
     * @param m: An integer m denotes the size of a backpack
     * @param A: Given n items with size A[i]
     * @return: The maximum size
     */]
    // Top to buttom Solution: Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    public int backPack(int m, int[] A) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        int[][] dp = new int[n+1][m+1];
        // i -> 物品， j -> 容量
        for (int i = 1; i <= n; i++) {
            // 也可以：for (int j = m; j >= 0; j--)
            for (int j = 1; j <= m; j++) {
                // 当背包容量j大于等于当前物品的大小A[i-1]：物品才能放进去
                if (j >= A[i-1]) {
                    // 不加现在这个物品的容量的时候，最大的容量dp[i-1][j-A[i-1]] 和加上当前物品的大小
                    // j >= A[i-1] 确保 dp[i-1][j-A[i-1]] 不为负数
                    // 假如 j = 8, 证明当前容量是8, 物品大小是5, 我们就要找当容量是3的时候，最大的容量，加上当前物品A[i-1]的容量
                    // 和不加这个物品的容量的比较：
                    int val = dp[i-1][j-A[i-1]] + A[i-1];
                    dp[i][j] = Math.max(dp[i-1][j], val);
                }
                // A[i-1] 这个物品放不进去背包里，所以当前能装的最大容量是之前一个物品的容量。
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }

    // Buttom up Solution: Running Time Complexity: O(n * m), Space Complexity: O(m)
    public int backPack(int m, int[] A) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int[] dp = new int[m+1];
        int n = A.length;
         // i -> 物品， j -> 容量
        for (int i = 1; i <= n; i++) {
            // 当前物品：A[i]
            int currItem = A[i];
            // 0/1背包问题，就是一个物品只能使用一次，就必须要 for (int j = m; j >= 1; j--) { }
            for (int j = m; j >= 1; j--) {
                if (j >= currItem) {
                    int val = dp[j-currItem] + currItem;
                    dp[j] = Math.max(dp[j], val);
                }
            }
        }
        return dp[m];
    }

    // 背包问题二：
    // https://www.lintcode.com/problem/backpack-ii/
    /**
    给出 n 个物品的体积 A[i] 和其价值 V[i] ，将他们装入一个大小为 m 的背包，最多能装入的总价值有多大？
    注意事项：A[i], V[i], n, m均为整数。你不能将物品进行切分。你所挑选的物品总体积需要小于等于给定的 m。

    ##### 多项式规律
    - 1. picked A[i-1]: 就是A[i-1]被用过, weight j 应该减去A[i-1]. 那么dp[i][j]就取决于dp[i-1][j-A[i-1]]的结果.
    - 2. did not pick A[i-1]: 那就是说, 没用过A[i-1], 那么dp[i][j]就取决于上一行d[i-1][j]

    dp[0][0] = 0; no item, bag size = 0
    dp[0][j] = 0; no item, can't fill bag
    dp[i][0] = 0; ith item, but bag size = 0, can't fill
    */
    /**
     * @param m: An integer m denotes the size of a backpack
     * @param A: Given n items with size A[i]
     * @param V: Given n items with value V[i]
     * @return: The maximum value
     */

    // Top to buttom Solution: Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    public int backPackII(int m, int[] A, int[] V) {
        int n = A.length;
        int[][] dp = new int[n+1][m+1];
        for (int i = 1; i <= n; i++) {
            int currItem = A[i-1];
            for (int j = 1; j <= m; j++) {
                if (j >= currItem) {
                    int val = dp[i-1][j-currItem] + V[i-1];
                    dp[i][j] = Math.max(dp[i-1][j], val);
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }

    // Buttom up Solution: Running Time Complexity: O(n * m), Space Complexity: O(m)
    public int backPackII(int m, int[] A, int[] V) {
        if (A == null || A.length == 0 || V == null || V.length == 0) {
            return 0;
        }
        int[] dp = new int[m+1];
        int n = A.length;
         // i -> 物品， j -> 容量
        for (int i = 1; i <= n; i++) {
            // 当前物品：A[i]
            int currItem = A[i-1];
            // 0/1背包问题，就是一个物品只能使用一次，就必须要 for (int j = m; j >= 1; j--) { }
            for (int j = m; j >= 1; j--) {
                if (j >= currItem) {
                    int val = dp[j - currItem] + V[i-1];
                    dp[j] = Math.max(dp[j], val);
                }
            }
        }
        return dp[m];
    }

    // 完全背包问题：
    // https://www.lintcode.com/problem/backpack-iii/
    /**
    Given n kind of items with size Ai and value Vi (each item has an infinite number available)
    and a backpack with size m.
    What's the maximum value can you put into the backpack?
    */
        /**
     * @param A: an integer array
     * @param V: an integer array
     * @param m: An integer
     * @return: a max value
     */
    // Running Time Complexity: O(n*m*k), Space Complexity: O()
    public int backPackIII(int[] A, int[] V, int m) {
        if (A == null || A.length == 0 || V == null || V.length == 0) {
            return 0;
        }
        int n = A.length;
        /**
        dp[i][w]: first i types of items to fill weight w, find the max value.
        1st loop: which type of item to pick from A
        2nd loop: weight from 0 ~ m
        3rd loop: # times when A[i] is used.
        */
        int[][] dp = new int[n+1][m+1];
        for (int i = 1; i <= n; i++) {
             // default: A[i-1] not used:
            for (int j = 0; j <= m; j++) {
                // didn't pick A[i - 1], dp[i][j] = dp[i - 1][j];
                dp[i][j] = dp[i-1][j];
                for (int k = 1; k * A[i-1] <= j; k++) {
                    // use A[i-1] for k times
                    // picked A[i - 1], dp[i][j] = dp[i - 1][j - k * A[i - 1]] + k * V[i - 1];
                    int val = dp[i-1][j - k*A[i-1]] + k * V[i-1];
                    dp[i][j] = Math.max(dp[i][j], val);
                }
            }
        }
        return dp[n][m];
    }
    /**
    Optimization1:
    - 优化时间复杂度, 画图发现:
    - 所计算的 (dp[i - 1][j - k*A[i - 1]] + k * V[i - 1])
    - 其实跟同一行的 dp[i][j-A[i-1]] 那个格子, 就多出了 V[i-1]
    - 所以没必要每次都 loop over k times
    - 简化: dp[i][j] 其中一个可能就是: dp[i][j - A[i - 1]] + V[i - 1]
    */
    // Running Time Complexity: O(n * m), Space Complexity: O(n*m)
    public int backPackIII(int[] A, int[] V, int m) {
        if (A == null || A.length == 0 || V == null || V.length == 0) {
            return 0;
        }
        int n = A.length;
        /**
        dp[i][w]: first i types of items to fill weight w, find the max value.
        1st loop: which type of item to pick from A
        2nd loop: weight from 0 ~ m
        */
        int[][] dp = new int[n+1][m+1];
        for (int i = 1; i <= n; i++) {
            // default: A[i-1] not used:
            int currItem = A[i-1];
            for (int j = 0; j <= m; j++) {
                if (j >= currItem) {
                    // 如果一个物品可以使用多次，那就是dp[i][j-currItem]
                    // 如果一个物品只能只用一次，那就是dp[i-1][j-currItem]
                    int val = dp[i][j - currItem] + V[i-1];
                    dp[i][j] = Math.max(dp[i-1][j], val);
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }
    /**
    Optimization3:
    - 根据上一个优化的情况, 画出 2 rows 网格
    - 发现 dp[i][j] 取决于: 1. dp[i - 1][j], 2. dp[i][j - A[i - 1]]
    - 其中: dp[i - 1][j] 是上一轮 (i-1) 的结算结果, 一定是已经算好, ready to be used 的
    - 然而, 当我们 i++,j++ 之后, 在之前 row = i - 1, col < j的格子, 全部不需要.
    - 降维简化: 只需要留着 weigth 这个 dimension, 而i这个dimension 可以省略:
    - (i - 1) row 不过是需要用到之前算出的旧value: 每一轮, j = [0 ~ m], 那么dp[j]本身就有记录旧值的功能.
    */
    // Running Time Complexity: O(n * m), Space Complexity: O(m)
    public int backPackIII(int[] A, int[] V, int m) {
        // write your code here
        if (A == null || V == null || A.length == 0 || V.length == 0) {
            return 0;
        }
        int n = A.length;
        int[] dp = new int[m+1];
        for (int i = 1; i <= n; i++) {
            int currItem = A[i-1];
            for (int j = m; j >= 1; j--) {
                if (j >= currItem) {
                    int val = dp[j - currItem] + V[i-1];
                    dp[j] = Math.max(dp[j], val);
                }
            }
        }
        return dp[m];
    }

    /**
    Given n items with size nums[i] which an integer array and all positive numbers, no duplicates.
    An integer target denotes the size of a backpack. Find the number of possible fill the backpack.

    Each item may be chosen unlimited number of times

    Example
    Given candidate items [2,3,6,7] and target 7,

    A solution set is:
    [7]
    [2, 2, 3]
    return 2
    */
    // Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    // The same as Leetcode 518 Coin Change 2:
    // https://leetcode.com/problems/coin-change-2/
    public int backPackIV(int[] A, int m) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        int m = amount;
        // 若只使用前 i 个物品，当背包容量为 j 时，有 dp[i][j] 种方法可以装满背包。
        // dp[i][j]
        int[][] dp = new int[n+1][m+1];
        // dp[0][x] = 0: 不使用任何一个物品，就无法凑出任何背包容量
        // dp[x][0] = 1: 背包容量为0，就只有1种方法
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }

        for (int i = 1; i <= n; i++) {
            int currItem = coins[i-1];
            for (int j = 1; j <= m; j++) {
                if (j >= currItem) {
                    // dp[i-1][j]: 只使用前i-1个物品 凑出背包容量j 的方法数量
                    // dp[i][j-currItem]: 使用前i个物品，凑出j-currItem金额的方法数量
                    // Ex: 你想用面值为2的硬币凑出金额5，那么如果你知道了凑出金额 3 的方法，再加上一枚面额为 2 的硬币，不就可以凑出5了嘛
                    dp[i][j] = dp[i-1][j] + dp[i][j-currItem];
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }

    // Running Time Complexity: O(n * m), Space Complexity: O(m)
    public int backPackIV(int[] A, int m) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        int[] dp = new int[m+1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            int currItem = A[i-1];
            // we cannot reverse the j order like for (int j = m; j >= 1; j--) { }
            // which is incorrect.
            // 完全背包问题，就是一个物品可以多次使用，就必须要 for (int j = 1; j <= m; j++) { }
            for (int j = 1; j <= m; j++) {
                if (j >= currItem) {
                    dp[j] = dp[j] + dp[j-currItem];
                }
            }
        }
        return dp[m];
    }

    /*
    Given n items with size nums[i] which an integer array and all positive numbers.
    An integer target denotes the size of a backpack. Find the number of possible fill the backpack.
    Each item may only be used once
    Example:
        Given candidate items [1,2,3,3,7] and target 7,
        A solution set is:
        [7]
        [1, 3, 3]
        return 2
    We want to know with i items, how many ways can we add up to equal target?
    dp[i][w]: the # of ways to add up to w with i items.
    track back to i - 1:
    1. A[i] was not picked : dp[i][w] = dp[i - 1][w]
    2. A[i] was picked: dp[i][w] = dp[i - 1][w - nums[i]];

    initialization:
    dp[0][0~w] = 0;
    dp[0][0] = 1;
    */
        /**
     * @param nums: an integer array and all positive numbers
     * @param m: An integer
     * @return: An integer
     */
    // 无重复背包 == 0/1 背包问题
    // Running Time Complexity: O(n * m), Space Complexity: O(m * n)
    public int backPackV(int[] A, int m) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        int[][] dp = new int[n+1][m+1];
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }
        for (int i = 1; i <= n; i++) {
            int currItem = A[i-1];
            for (int j = 1; j <= m; j++) {
                // 这道题就是与IV相比，去掉了无限背包的条件，每个item用一次.
                // dp[i][j] 为前i个item，去size <= j 的方案个数。
                // 所以是dp[i-1][j-currItem] 而不是dp[i][j-currItem]
                // 如果一个物品可以使用多次，那就是dp[i][j-currItem]
                // 如果一个物品只能只用一次，那就是dp[i-1][j-currItem]
                if (j >= currItem) {
                    dp[i][j] = dp[i-1][j] + dp[i-1][j-currItem];
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }
    /**
    Improvement 降维
    Inner loop of weight needs to iterate from j = M -> 0
    We always use dp[i-1][w] or dp[i - 1][w - nums[i - 1]]:
    always using old values from last row right above at w index, or on the left side of the w index.
    All indexes on right side of w is not needed.
    Therefore, we can reduce the dp[][] into 1-D array.
    Note: j has to iterate from M -> A[i-1], because we know on i - 1 row all indexes
    on right side of w on the right side can be overriden.
    if j = A[i-1] -> M, it will override useful indexes.
    */
    // Running Time Complexity: O(n * m), Space Complexity: O(m)
    public int backPackV(int[] A, int m) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        int[] dp = new int[m+1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            int currItem = A[i-1];
            // 完全背包问题：一个物品可以使用多次，那就是 for (int j = 1; j <= m; j++)
            // 0/1 背包问题，一个物品只能只用一次 那就是 for (int j = m; j >= 1; j--)
            for (int j = m; j >= 1; j--) {
                if (j >= currItem) {
                    dp[j] = dp[j] + dp[j - currItem];
                }
            }
        }
        return dp[m];
    }

    /**
    Given an integer array nums with all positive numbers and no duplicates,
    find the number of possible combinations that add up to a positive integer target.
    Example
    Given nums = [1, 2, 4], target = 4

    The possible combination ways are:
    [1, 1, 1, 1]
    [1, 1, 2]
    [1, 2, 1]
    [2, 1, 1]
    [2, 2]
    [4]
    return 6
    */
    // Running Time Complexity: O(n * m), Space Complexity: O(m)
    /**
    dp[j]表示背包容量为j时，数组nums能装满j的方法数量。
    可以想到的方法是对于当前背包容量j，假设最后加入的元素为当前遍历的元素i，
    则将i元素最后加入的方法数量为dp[j - nums[i]]（此时要求j - nums[i] >= 0），将数组元素全部遍历当过i为止。
    其中，初始化dp[0]=1，表示数组元素将容量为0的背包装满的方法数为1（即什么元素都不取这一种方法）。
    这道题并不难，但需要注意要和IV，V的区别。IV，V中的方法和元素位置有关系，元素的相对位置不能变化，
    不能先取后面的元素再取前面的元素，即相同元素组成的方法被视为一种方法（就算排列不同），其dp[j]的含义为前i件物品装满容量j的方法数，
    因此只和i之前的物品相关，而和i之后的物品无关。
    这道题则说明不同的顺序被认为是不同的方法，因此和元素位置无关，可以先取后面的元素再取前面的元素，
    即相同元素的不同排列被视为不同的方法，其dp[j]表示的是数组nums装满容量j的方法数，是和数组中所有元素有关。
    之前几题能够用一维dp表示的本质其实是因为当前行的值只和上一行有关，因此用动态数组两行就行，
    如果直接在上一行更新当前行的状态则只需要一行即可，因此其本质就是还是二维dp。但是这道题真的是一维dp，
    即当前容量的填装数量只和之前容量填装的结果有关，
    只不过每次填装都要遍历整个nums数组来寻找相关的之前容量的状态，因此要用两重for循环。
    */
    public int backPackVI(int[] A, int m) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        int[] dp = new int[m+1];
        dp[0] = 1;
        for (int j = 1; j <= m; j++) {
            for (int i = 1; i <= n; i++) {
                int currItem = A[i-1];
                if (j >= currItem) {
                    dp[j] = dp[j] + dp[j - currItem];
                }
            }
        }
        return dp[m];
    }

    // Leetcode 322 Coin Change:
    // https://leetcode.com/problems/coin-change
    /**
    You are given coins of different denominations and a total amount of money amount.
    Write a function to compute the fewest number of coins that you need to make up that amount.
    If that amount of money cannot be made up by any combination of the coins, return -1.

    Example 1:

    Input: coins = [1, 2, 5], amount = 11
    Output: 3
    Explanation: 11 = 5 + 5 + 1
    Example 2:

    Input: coins = [2], amount = 3
    Output: -1
    Note:
    You may assume that you have an infinite number of each kind of coin.
    */

    // Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return 0;
        }
        int n = coins.length;
        // dp[i][j]: min coins to make up j amount using the first i types of coins
        int[][] dp = new int[n+1][amount+1];
        for (int[] list : dp) {
            Arrays.fill(list, amount+1);
        }
        dp[0][0] = 0;
        for (int i = 1; i <= n; i++) {
            int currCoin = coins[i-1];
            for (int j = 0; j <= amount; j++) {
                if (j >= currCoin) {
                    // 跟backPackIV是一样的，用dp[i][j-currCoin] + 1 来比较，而不是dp[i-1][j-currCoin] + 1，因为每个coin可以用无限个
                    dp[i][j] = Math.min(dp[i-1][j], dp[i][j-currCoin] + 1);
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][amount] > amount ? -1 : dp[n][amount];
    }

    // Running Time Complexity: O(m*n), Space Complexity: O(m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return -1;
        }
        int n = coins.length;
        int m = amount;
        int[] dp = new int[amount+1];
        // can not be Integer.MAX_VALUE because of overflow
        Arrays.fill(dp, amount+1);
        dp[0] = 0;
        for (int i = 1; i <= n; i++) {
            int currCoin = coins[i-1];
            for (int j = 1; j <= m; j++) {
                if (j >= currCoin) {
                    dp[j] = Math.min(dp[j], dp[j-currCoin] + 1);
                }
            }
        }
        return dp[m] > amount ? -1 : dp[m];
    }

    // If one coin can be used only once:
    // Running Time Complexity: O(m*n), Space Complexity: O(m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return -1;
        }
        int n = coins.length;
        int m = amount;
        int[] dp = new int[amount+1];
        Arrays.fill(dp, amount+1);
        dp[0] = 0;
        for (int i = 0; i < n; i++) {
            int currCoin = coins[i];
            // 0/1 背包问题，就要 for (int j = m; j >= 1; j--)
            for (int j = m; j >= 1; j--) {
                if (j >= currCoin) {
                    dp[j] = Math.min(dp[j], dp[j-currCoin] + 1);
                }
            }
        }
        return dp[m] > amount ? -1 : dp[m];
    }

    // Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0) {
            return 0;
        }
        int n = coins.length;
        int m = amount;
        int[][] dp = new int[n+1][amount+1];
        for (int i = 0; i <= n; i++) {
            Arrays.fill(dp[i], amount+1);
        }
        dp[0][0] = 0;
        for (int i = 1; i <= n; i++) {
            int currCoin = coins[i-1];
            for (int j = 0; j <= m; j++) {
                if (j >= currCoin) {
                    // 跟backPackV是一样的，用dp[i-1][j-currCoin] + 1 来比较，而不是dp[i][j-currCoin] + 1，因为每个coin只能用一次
                    dp[i][j] = Math.min(dp[i-1][j], dp[i-1][j-currCoin] + 1);
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m] > amount ? -1 : dp[n][m];
    }

    // Leetcode 70: Climbing Stairs:
    // https://leetcode.com/problems/climbing-stairs/
    /**
    You are climbing a stair case. It takes n steps to reach to the top.

    Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

    Note: Given n will be a positive integer.

    Example 1:

    Input: 2
    Output: 2
    Explanation: There are two ways to climb to the top.
    1. 1 step + 1 step
    2. 2 steps
    Example 2:

    Input: 3
    Output: 3
    Explanation: There are three ways to climb to the top.
    1. 1 step + 1 step + 1 step
    2. 1 step + 2 steps
    3. 2 steps + 1 step
    */
    public int climbStairs(int n) {
        if (n <= 2) {
            return n;
        }
        int[] dp = new int[n+1];
        dp[0] = 0;
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }

    // Leetcode 062: Unique Paths
    // https://leetcode.com/problems/unique-paths/
    /**
    A robot is located at the top-left corner of a m x n grid
    (marked 'Start' in the diagram below).

    The robot can only move either down or right at any point in time.
    The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

    How many possible unique paths are there?

    Example 1:
    Input: m = 3, n = 7
    Output: 28

    Example 2:
    Input: m = 3, n = 2
    Output: 3
    Explanation:
    From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
    1. Right -> Down -> Down
    2. Down -> Down -> Right
    3. Down -> Right -> Down

    Example 3:
    Input: m = 7, n = 3
    Output: 28

    Example 4:
    Input: m = 3, n = 3
    Output: 6
    */
    // Running Time Complexity: O(m * n), Space Complexity: O(m * n)
    public int uniquePaths(int m, int n) {
        if (m < 0 || n < 0) {
            return 0;
        }
        // sub problem: dp[i][j] = dp[i-1][j] + dp[i][j-1]
        // result: dp[m-1][n-1] since we starts at dp[0][0]
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // all boundary can only have one way to go.
                if (i == 0 || j == 0) {
                    dp[i][j] = 1;
                }
                // else, use sub problem to form current position.
                else {
                    dp[i][j] = dp[i-1][j] + dp[i][j-1];
                }
            }
        }
        return dp[m-1][n-1];
    }

    // Running Time Complexity: O(m * n), Space Complexity: O(n)
    public int uniquePaths(int m, int n) {
        if (m < 0 || n < 0) {
            return 0;
        }
        int[] dp = new int[n];
        // initial array
        Arrays.fill(dp, 1);
        for (int i = 1; i < m; i++) {
        	for (int j = 1; j < n; j++) {
                // dp[i][j] = dp[i-1][j] + dp[i][j-1]
                // dp[i-1][j]: last dp[j]
                // dp[i][j-1]: dp[j-1]
        		dp[j] = dp[j] + dp[j-1];
        	}
        }
        return dp[n-1];
    }

    // Leetcode 063: Unique Paths II:
    // https://leetcode.com/problems/unique-paths-ii/
    /*
    A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

    The robot can only move either down or right at any point in time.
    The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

    An obstacle and empty space is marked as 1 and 0 respectively in the grid.

    Note: m and n will be at most 100.

    Example 1:

    Input:
    [
      [0,0,0],
      [0,1,0],
      [0,0,0]
    ]
    Output: 2
    Explanation:
    There is one obstacle in the middle of the 3x3 grid above.
    There are two ways to reach the bottom-right corner:
    1. Right -> Right -> Down -> Down
    2. Down -> Down -> Right -> Right
    */
    // Running Time Complexity: O(m * n), Space Complexity: O(m * n)
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid == null || obstacleGrid.length == 0) {
            return 0;
        }
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            if (obstacleGrid[i][0] == 1) {
                break;
            }
            dp[i][0] = 1;
        }
        for (int j = 0; j < n; j++) {
            if (obstacleGrid[0][j] == 1) {
                break;
            }
            dp[0][j] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (obstacleGrid[i][j] != 1) {
                    dp[i][j] = dp[i-1][j] + dp[i][j-1];
                }
                else {
                    dp[i][j] = 0;
                }
            }
        }
        return dp[m-1][n-1];
    }

    // Running Time Complexity: O(m * n), Space Complexity: O(n)
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid == null || obstacleGrid.length == 0) {
            return 0;
        }
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;

        int[] dp = new int[n];
        dp[0] = 1;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (obstacleGrid[i][j] == 1) {
                    dp[j] = 0;
                }
                else if (j > 0) {
                    dp[j] = dp[j] + dp[j-1];
                }
            }
        }

        return dp[n-1];
    }

    // Leetcode 064: Minimum Path Sum:
    // https://leetcode.com/problems/minimum-path-sum/
    /**
    Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right which minimizes the sum of all numbers along its path.

    Note: You can only move either down or right at any point in time.

    Example:

    Input:
    [
      [1,3,1],
      [1,5,1],
      [4,2,1]
    ]
    Output: 7
    Explanation: Because the path 1→3→1→1→1 minimizes the sum.
    */
    // Running Time Complexity: O(m * n),
    // Space Complexity: O(m * n)
    public int minPathSum(int[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int[][] dp = new int[m][n];
        int sum = 0;
        for (int i = 0; i < m; i++) {
            sum += grid[i][0];
            dp[i][0] = sum;
        }
        sum = 0;
        for (int j = 0; j < n; j++) {
            sum += grid[0][j];
            dp[0][j] = sum;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = grid[i][j] + Math.min(dp[i-1][j], dp[i][j-1]);
            }
        }
        return dp[m-1][n-1];
    }

    // Running Time Complexity: O(m * n),
    // Space Complexity: O(n)
    public int minPathSum(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;

        int[] dp = new int[n];
        int sum = 0;
        for (int j = 0; j < n; j++) {
            sum += grid[0][j];
            dp[j] = sum;
        }

        for (int i = 1; i < m; i++) {
            dp[0] += grid[i][0];
            for (int j = 1; j < n; j++) {
                dp[j] = grid[i][j] + Math.min(dp[j], dp[j-1]);
            }
        }
        return dp[n-1];
    }

    // Leetcode 72: Edit Distance a.k.a Levenshtein distance
    // https://leetcode.com/problems/edit-distance/
    /**
    For two strings:
    X of length m,
    Y of length n:

    dp[i][j]: the minimum edit distance between X[0...i] and Y[0...j]
    i.e., the first i characters of X and the first j characters of Y
    dp[m][n]: The edit distance between X and Y

    1. Initialization:
    dp[i][0] = i
    dp[0][j] = j

    2. Recurrence Relation:
    for each i = 1 ... m
        for each j = 1 ... n
        dp[i][j] = min {
                            dp[i-1][j] + 1  => Deleting 1 character from X
                            dp[i][j-1] + 1  => Inserting 1 character to Y
                            dp[i-1][j-1] + 0 if (X[i-1] == Y[j-1])
                            dp[i-1][j-1] + 1 if (X[i-1] != Y[j-1]) Substitution 1 character from X to Y to make them match
                        }
    3. Result:
    dp[m][n]
    */
    // http://www.stanford.edu/class/cs124/lec/med.pdf
    // https://www.youtube.com/watch?v=Xxx0b7djCrs
    // Runinng Time Complexity: O(m * n), Space Complexity: O(m * n)
    public int minDistance(String word1, String word2) {
        int m = word1.length(), n = word2.length();
        int[][] dp = new int[m+1][n+1];
        for (int i = 1; i <= m; i++) {
            dp[i][0] = dp[i-1][0] + 1;
        }
        for (int j = 1; j <= n; j++) {
            dp[0][j] = dp[0][j-1] + 1;
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (word1.charAt(i-1) == word2.charAt(j-1)) {
                    dp[i][j] = dp[i-1][j-1];
                }
                else {
                    // 如果在字符串 word1末尾插入一个与 word2[j] 相等的字符，则有 dp[i][j] = dp[i][j-1] + 1;
                    // insert a character
                    int num1 = dp[i][j-1] + 1;
                    // 如果把字符 word1[i] 删除，则有 dp[i][j] = dp[i-1][j] + 1;
                    // delete a character
                    int num2 = dp[i-1][j] + 1;
                    // 如果把字符 word1[i] 替换成与 word2[j] 相等，则有 dp[i][j] = dp[i-1][j-1] + 1;
                    // replace a character
                    int num3 = dp[i-1][j-1] + 1;
                    dp[i][j] = minOfThreeValues(num1, num2, num3);
                }
            }
        }
        return dp[m][n];
    }

    private int minOfThreeValues(int num1, int num2, int num3) {
        int minVal = Math.min(num1, num2);
        return Math.min(minVal, num3);
    }

    // Running Time Complexity: O(m * n),
    // Space Complexity: O(n)
    public int minDistance(String word1, String word2) {
        int m = word1.length();
        int n = word2.length();

        int[] dp = new int[n+1];
        for (int j = 1; j <= n; j++) {
            dp[j] = dp[j-1] + 1;
        }

        for (int i = 1; i <= m; i++) {
            // prev 相当于之前的 dp[i-1][j-1]
            int prev = dp[0];
            dp[0] = i;
            for (int j = 1; j <= n; j++) {
                int temp = dp[j];
                if (word1.charAt(i-1) == word2.charAt(j-1)) {
                    dp[j] = prev;
                }
                else {
                    // insert a character
                    int num1 = dp[j-1] + 1;
                    // delete a character
                    int num2 = dp[j] + 1;
                    // replace a character
                    int num3 = prev + 1;
                    dp[j] = minOfThreeValues(num1, num2, num3);
                }
                prev = temp;
            }
        }
        return dp[n];
    }

    // Leetcode 053: Maximum Subarray
    // https://leetcode.com/problems/maximum-subarray/
    /*
    Given an integer array nums, find the contiguous subarray (containing at least one number)
    which has the largest sum and return its sum.

    Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
    Output: 6
    Explanation: [4,-1,2,1] has the largest sum = 6.
    */
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length, max = nums[0];
        // dp[i]: largest sum of (0...i):
        int[] dp = new int[n];
        dp[0] = nums[0];
        for (int i = 1; i < n; i++) {
            if (dp[i-1] > 0) {
                dp[i] = dp[i-1] + nums[i];
            }
            else {
                dp[i] = nums[i];
            }
            // or : dp[i] = Math.max(nums[i], dp[i-1] + nums[i]);
            max = Math.max(max, dp[i]);
        }
        return max;
    }

    // Leetcode 746 Min Cost Climbing Stairs
    // https://leetcode.com/problems/min-cost-climbing-stairs/
    /**
    On a staircase, the i-th step has some non-negative cost cost[i] assigned (0 indexed).

    Once you pay the cost, you can either climb one or two steps. You need to find minimum cost to reach the top of the floor,
    and you can either start from the step with index 0, or the step with index 1.

    Example 1:
    Input: cost = [10, 15, 20]
    Output: 15
    Explanation: Cheapest is start on cost[1], pay that cost and go to the top.
    Example 2:
    Input: cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
    Output: 6
    Explanation: Cheapest is start on cost[0], and only step on 1s, skipping cost[3].
    Note:
    cost will have a length in the range [2, 1000].
    Every cost[i] will be an integer in the range [0, 999].
    */
    // Running Time complexity: O(n), Space Complexity: O(n)
    public int minCostClimbingStairs(int[] cost) {
        if (cost == null || cost.length == 0) {
            return 0;
        }
        int n = cost.length;
        int[] dp = new int[n];
        dp[0] = cost[0];
        dp[1] = cost[1];
        for (int i = 2; i < n; i++) {
            dp[i] = cost[i] + Math.min(dp[i-1], dp[i-2]);
        }
        return Math.min(dp[n-2], dp[n-1]);
    }

    // Running Time complexity: O(n), Space Complexity: O(1)
    public int minCostClimbingStairs(int[] cost) {
        if (cost == null || cost.length == 0) {
            return 0;
        }
        int n = cost.length;
        int minCost1 = cost[0]; // 2 steps away from current step.
        int minCost2 = cost[1]; // 1 steps away from current step.
        int minCost = 0;
        for (int i = 2; i < n; i++) {
            minCost = Math.min(minCost1, minCost2) + cost[i];
            // previous one step of current step = 2 steps from next step.
            minCost1 = minCost2;
            // current cost of this step = one step from next step
            minCost2 = minCost;
        }
        return Math.min(minCost1, minCost2);
    }

    // Leetcode 91: Decode Ways:
    // https://leetcode.com/problems/decode-ways
    /*
    A message containing letters from A-Z is being encoded to numbers using the following mapping:

    'A' -> 1
    'B' -> 2
    ...
    'Z' -> 26
    Given a non-empty string containing only digits, determine the total number of ways to decode it.

    Example 1:

    Input: "12"
    Output: 2
    Explanation: It could be decoded as "AB" (1 2) or "L" (12).
    Example 2:

    Input: "226"
    Output: 3
    Explanation: It could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF" (2 2 6).
    */
    // Running Time Complexity: O(len(s)), Space Complexity: O(len(s))
    public int numDecodings(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int n = s.length();
        char[] cList = s.toCharArray();
        int[] dp = new int[n+1];
        dp[0] = 1;
        dp[1] = (cList[0] - '0') == 0 ? 0 : 1;
        for (int i = 2; i <= n; i++) {
            // one digit number:
            int n1 = cList[i-1] - '0';
            // two digits number
            int n2 = n1 + 10 * (cList[i-2] - '0');
            if (n1 >= 1 && n1 <= 9) {
                dp[i] += dp[i-1];
            }
            if (n2 >= 10 && n2 <= 26) {
                dp[i] += dp[i-2];
            }
        }
        return dp[n];
    }

    // Leetcode 139: Word Break:
    // https://leetcode.com/problems/word-break
    /**
    Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

    Note:

    The same word in the dictionary may be reused multiple times in the segmentation.
    You may assume the dictionary does not contain duplicate words.
    Example 1:

    Input: s = "leetcode", wordDict = ["leet", "code"]
    Output: true
    Explanation: Return true because "leetcode" can be segmented as "leet code".
    Example 2:

    Input: s = "applepenapple", wordDict = ["apple", "pen"]
    Output: true
    Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
                Note that you are allowed to reuse a dictionary word.
    Example 3:

    Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
    Output: false
    */
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet<String>(wordDict);
        int n = s.length();
        // dp[i]: substring(0, i) can be segmented with words in wordDict:
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for(int i = 1; i <= s.length(); i++){
            for(int j = 0; j < i; j++){ // faster if j -> i-1 to 0
                String substr = s.substring(j, i);
                // dp[j] means that substring(0, j) can be segmented with words in wordDict,
                // if substring(j, i) is in wordDict, it means all substring(0, i) can be segmented with words in wordDict
                if(dp[j] && wordDict.contains(substr)){
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }

    // Running Time Complexity: O(n * w), Space Complexity: O(n)
    public boolean wordBreak(String s, List<String> wordDict) {
        int n = s.length();
        // dp[i]: substring(0, i) can be segmented with words in wordDict:
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for (int i = 0; i < n; i++) {
            // If substring(0...i) cannot be segmented with words in wordDict,
            // there is no need to see if appended word in wordDict can be segmented.
            if (!dp[i]) {
                continue;
            }
            // Try each word and find substring(i, i+word.length()):
            // "applepenapple": word = pen, i = 5, dp[i] = true, substring(i, i+word.length()) = substring(5, 8) = pen:
            // so dp[8] = true;
            for (String word : wordDict) {
                int end = i + word.length();
                // If either appended word to substring(0,i) is longer than s, or we already found dp[end] == true
                if (end > n || dp[end]) {
                    continue;
                }
                // substr like "pen"
                String substr = s.substring(i, end);
                if (substr.equals(word)) {
                    dp[end] = true;
                }
            }
        }
        return dp[n];
    }

    // Leetcode Regular Expression Matching:
    // https://leetcode.com/problems/regular-expression-matching/
    // with Follow up: add + and ?
    /**
    Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.

    '.' Matches any single character.
    '*' Matches zero or more of the preceding element.
    The matching should cover the entire input string (not partial).

    Note:

    s could be empty and contains only lowercase letters a-z.
    p could be empty and contains only lowercase letters a-z, and characters like . or *.
    Example 1:

    Input:
    s = "aa"
    p = "a"
    Output: false
    Explanation: "a" does not match the entire string "aa".
    Example 2:

    Input:
    s = "aa"
    p = "a*"
    Output: true
    Explanation: '*' means zero or more of the precedeng element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
    Example 3:

    Input:
    s = "ab"
    p = ".*"
    Output: true
    Explanation: ".*" means "zero or more (*) of any character (.)".
    Example 4:

    Input:
    s = "aab"
    p = "c*a*b"
    Output: true
    Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore it matches "aab".
    Example 5:

    Input:
    s = "mississippi"
    p = "mis*is*p*."
    Output: false
    */
    /* dp[i][j] means s.substring(0,i), p.substring(0,j) is match.
    Initialization:
       for regular expression string p, check if there is an empty string match like a*b*
          for each j = 1 ... n
             if p.charAt(j-1) == '*' // treat a* as empty match.
                dp[0][j] = dp[0][j-2]
    Recurrence Relation:
        for each i = 1 ... m
           for each j = 1 ... n
                if s.charAt(i-1) == p.charAt(j-1)
                   dp[i][j] = dp[i-1][j-1];
                if p.charAt(j-1) == '.'
                   dp[i][j] = dp[i-1][j-1];
                else if p.charAt(j) == '*'
                      if p.charAt(j-2) == '.' || s.charAt(i-1) == p.charAt(j-2):
                         dp[i][j] = dp[i-1][j] // in this case: a* count as multiple a
                                               // dp[i-1][j]: match previous character with p.substring(0,j) regex
                                 or dp[i][j-1] // in this case: a* count as single a
                                 or dp[i][j-2] // in this case: a* count as empty
                      if p.charAt(j-2) != '.' && s.charAt(i-1) != p.charAt(j-2):
                         in this case: a* or .* count as empty:
                         dp[i][j] = dp[i][j-2]
                else if p.charAt(j-1) == '+'
                      if p.charAt(j-2) == '.' || s.charAt(i-1) == p.charAt(j-2)
                         dp[i][j] = dp[i-1][j] // in this case: a+ count as multiple a
                                 or dp[i][j-1] // in this case: a+ count as single a
                      else
                         dp[i][j] = false;
                else if p.charAt(j-1) == '?'
                      if p.charAt(j-2) == '.' || s.charAt(i-1) == p.charAt(j-2)
                         dp[i][j] = dp[i][j-2] // in this case: a? count as empty
                                 or dp[i][j-1] // in this case: a? count as single a
                      else
                         in this case: a? count as empty:
                         dp[i][j] = dp[i][j-2]
                else
                    dp[i][j] = false

    */

    // follow up: add + and ?
    // Running Time Complexity: O(m*n), Space Complexity: O(m*n)
    public boolean isMatch(String s, String p) {
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m+1][n+1];
        // empty regex matches empty string:
        dp[0][0] = true;
        // scan the regex string and initialize the * match with 2 characters before:
        // ba* = b
        for (int j = 1; j <= n; j++) {
            if (p.charAt(j-1) == '*') {
                dp[0][j] = dp[0][j-2];
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if two characters from s and p at same index are matching,
                // or current regex character is .
                if (s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) == '.') {
                    dp[i][j] = dp[i-1][j-1];
                }
                // if current regex character is *:
                else if (p.charAt(j-1) == '*') {
                    // assume regex string is always valid, no * at the beginning:
                    // the character before * is a .
                    // or current character from s matches character before *:
                    if (p.charAt(j-2) == '.' || s.charAt(i-1) == p.charAt(j-2)) {
                        // dp[i-1][j]: match previous character with p.substring(0,j) regex
                        dp[i][j] = dp[i-1][j] || dp[i][j-1] || dp[i][j-2];
                    }
                    // in this case: a* or .* count as empty:
                    else {
                        dp[i][j] = dp[i][j-2];
                    }
                }
                else if (p.charAt(j-1) == '+') {
                    // in this case: a+ count as multiple a or single a:
                	if (p.charAt(j-2) == '.' || s.charAt(i-1) == p.charAt(j-2)) {
                		dp[i][j] = dp[i-1][j] || dp[i][j-1];
                	}
                	else {
                		dp[i][j] = false;
                	}
                }
                else if (p.charAt(j-1) == '?') {
                	if (p.charAt(j-2) == '.' || s.charAt(i-1) == p.charAt(j-2)) {
                		dp[i][j] = dp[i][j-1] || dp[i][j-2];
                	}
                    // in this case: a? or .? count as empty:
                	else {
                		dp[i][j] = dp[i][j-2];
                	}
                }
                else {
                    dp[i][j] = false;
                }
            }
        }
        return dp[m][n];
    }

    // Leetcode 44: Wildcard Matching:
    /**
    Given an input string (s) and a pattern (p), implement wildcard pattern matching with support for '?' and '*'.
    '?' Matches any single character.
    '*' Matches any sequence of characters (including the empty sequence).
    The matching should cover the entire input string (not partial).

    Note:

    s could be empty and contains only lowercase letters a-z.
    p could be empty and contains only lowercase letters a-z, and characters like ? or *.
    Example 1:

    Input:
    s = "aa"
    p = "a"
    Output: false
    Explanation: "a" does not match the entire string "aa".
    Example 2:

    Input:
    s = "aa"
    p = "*"
    Output: true
    Explanation: '*' matches any sequence.
    Example 3:

    Input:
    s = "cb"
    p = "?a"
    Output: false
    Explanation: '?' matches 'c', but the second letter is 'a', which does not match 'b'.
    Example 4:

    Input:
    s = "adceb"
    p = "*a*b"
    Output: true
    Explanation: The first '*' matches the empty sequence, while the second '*' matches the substring "dce".
    Example 5:

    Input:
    s = "acdcb"
    p = "a*c?b"
    Output: false
    */
    public boolean isMatch(String s, String p) {
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m+1][n+1];
        // empty regex matches empty string
        dp[0][0] = true;
        for (int j = 1; j <= n; j++) {
            if (p.charAt(j-1) == '*') {
                // match whatever before:
                dp[0][j] = dp[0][j-1];
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) == '?') {
                    dp[i][j] = dp[i-1][j-1];
                }
                else if (p.charAt(j-1) == '*') {
                    // dp[i-1][j]: match one or multiple:
                    // dp[i][j-1]: match empty string:
                    dp[i][j] = dp[i-1][j] || dp[i][j-1];
                }
                else {
                    dp[i][j] = false;
                }
            }
        }
        return dp[m][n];
    }

    // Leetcode 516: Longest Palindromic Subsequence:
    // https://leetcode.com/problems/longest-palindromic-subsequence/
    /**
    dp[i][j]: length of longest palindrome subsequence in substring(i,j)
    dp[i][j]: if s.charAt(i) == s.charAt(j)
                 dp[i][j] = dp[i+1][j-1] + 2
              else
                 dp[i][j] = Math.max(dp[i+1][j], dp[i][j-1])
    */
    public int longestPalindromeSubseq(String s) {
        int m = s.length();
        int[][] dp = new int[m][m];
        // Longest Palindrome Subsequence for 1 character will be 1:
        for (int i = 0; i < m; i++) {
            dp[i][i] = 1;
        }
        // we can't start the outer loop from 0, because dp[i][j] = dp[i+1][j-1] + 2,
        // the new dp[i][j] use the information from dp[i+1][j-1] which is 0 if the outer loop starts from 0.
        for (int i = m-1; i >= 0; i--) {
            for (int j = i+1; j < m; j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    dp[i][j] = dp[i+1][j-1] + 2;
                }
                else {
                    dp[i][j] = Math.max(dp[i+1][j], dp[i][j-1]);
                }
            }
        }
        return dp[0][m-1];
    }

    // Leetcode 221: Maximal Square
    // https://leetcode.com/problems/maximal-square/
    /**
    Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

    Example:

    Input:

    1 0 1 0 0
    1 0 1 1 1
    1 1 1 1 1
    1 0 0 1 0

    Output: 4
    */
    /**
    dp[i][j]: maximum square size you can achieve at matrix[i-1][j-1]
    dp[i][j] min of {
              dp[i-1][j]
              dp[i-1][j-1]
              dp[i][j-1]
             } + 1;
    dp[i][j] can only be 2 only when dp[i-1][j], dp[i][j-1], dp[i-1][j-1] are all 1
    Running Time Complexity: O(n * m), Space Complexity: O(n * m)
    */
    public int maximalSquare(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[][] dp = new int[m+1][n+1];
        int size = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // if current number is 1, find the min of dp[i-1][j], dp[i][j-1], dp[i-1][j-1]
                if (matrix[i-1][j-1] == '1') {
                    int minVal = Math.min(dp[i-1][j], dp[i][j-1]);
                    dp[i][j] = Math.min(dp[i-1][j-1], minVal) + 1;
                    size = Math.max(size, dp[i][j]);
                }
            }
        }
        return size * size;
    }

    // Running Time Complexity: O(m*n), Space Complexity: O(n)
    /**
    use variable lastLeftItem to keep track or dp[i][j-1] item, so that we can use an dp[n+1] array to store the state
    */
    public int maximalSquare(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[] dp = new int[n+1];
        int leftSizeItem = 0, size = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                int prevLeftItem = dp[j];
                if (matrix[i-1][j-1] == '1') {
                    int minVal = Math.min(dp[j], dp[j-1]);
                    dp[j] = Math.min(leftSizeItem, minVal) + 1;
                    size = Math.max(size, dp[j]);
                }
                else {
                    dp[j] = 0;
                }
                leftSizeItem = prevLeftItem;
            }
        }
        return size * size;
    }

    // Leetcode 85: Maximal Rectangle:
    // https://leetcode.com/problems/maximal-rectangle/
    /**
    Given a 2D binary matrix filled with 0's and 1's, find the largest rectangle containing only 1's and return its area.
    Example:

    Input:
    [
    ["1","0","1","0","0"],
    ["1","0","1","1","1"],
    ["1","1","1","1","1"],
    ["1","0","0","1","0"]
    ]
    Output: 6
    */
    // Running Time Complexity: O(m * 4n), Space Complexity: O(3n)
    public int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        // int[] left: the start column index of continuous '1' block
        // int[] right: the end column index of continuous '1' block
        // int[] height: the height of continuous '1' block
        int[] left = new int[n], right = new int[n], height = new int[n];
        Arrays.fill(right, n-1);
        int maxArea = 0;
        for (int i = 0; i < m; i++) {
            int currLeft = 0, currRight = n-1;
            // compute height:
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    height[j]++;
                }
                else {
                    height[j] = 0;
                }
            }
            // compute left boundary:
            // If 1 is continuous, then left[j] is currLeft.
            // If matrix[i][j] == '0',  then left[j] will be the max of currLeft idx, or previous left[j],
            // to make sure that left boundary shrink to current place.
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    left[j] = Math.max(left[j], currLeft);
                }
                else {
                    left[j] = 0;
                    // set currLeft to index of next item
                    currLeft = j+1;
                }
            }
            // compute right boundary:
            // If 1 is continuous from right to left, then right[j] is always the right most index
            // If matrix[i][j] == '0', then right[j] will be the min of currRight idx, or previous right[j]
            for (int j = n-1; j >= 0; j--) {
                if (matrix[i][j] == '1') {
                    right[j] = Math.min(right[j], currRight);
                }
                else {
                    right[j] = n-1;
                    // set currLeft to index of next item
                    currRight = j-1;
                }
            }
            // compute the area:
            for (int j = 0; j < n; j++) {
                int currArea = (right[j] - left[j] + 1) * height[j];
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }

    // Solution 2: for each row, calculate the heights,
    // and use the solution of Leetcode 84: Largest Rectangle in Histogram:
    public int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[] heights = new int[n];
        int maxArea = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    heights[j]++;
                }
                else {
                    heights[j] = 0;
                }
            }
            int currArea = largestRectangleArea(heights);
            maxArea = Math.max(maxArea, currArea);
        }
        return maxArea;
    }

    public int largestRectangleArea(int[] heights) {
        int maxArea = 0, max = heights.length;
        Deque<Integer> s = new ArrayDeque<Integer>();
        int i = 0;
        while (i <= max) {
            int height = 0;
            if (i < max) {
                height = heights[i];
            }
            if (s.isEmpty() || height >= heights[s.peek()]) {
                s.push(i);
                i++;
            }
            else {
                // Get the previous top height
                int topIdx = s.pop();
                // If stack is empty, then it means previous elements are greater than heights[topIdx]
                // Else, s.peek() is the starting point of histogram. width = i - (s.peek() + 1)
                int width = i;
                if (!s.isEmpty()) {
                    width = i - (s.peek() + 1);
                }
                int currArea = heights[topIdx] * width;
                maxArea = Math.max(maxArea, currArea);
            }
        }
        return maxArea;
    }

    // Leetcode 139: Word Break:
    // https://leetcode.com/problems/word-break
    /**
    Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

    Note:

    The same word in the dictionary may be reused multiple times in the segmentation.
    You may assume the dictionary does not contain duplicate words.
    Example 1:

    Input: s = "leetcode", wordDict = ["leet", "code"]
    Output: true
    Explanation: Return true because "leetcode" can be segmented as "leet code".
    Example 2:

    Input: s = "applepenapple", wordDict = ["apple", "pen"]
    Output: true
    Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
                Note that you are allowed to reuse a dictionary word.
    Example 3:

    Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
    Output: false


    01234567
    leetcode
    j..i
    dp[i] -> break here
    it is likely that the match word will be found at the end of the finished part of the string,
    but not a really long word which begins from the beginning of the string
    */
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet<String>(wordDict);
        int n = s.length();
        // dp[i]: substring(0, i) can be segmented with words in wordDict:
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for (int i = 1; i <= n; i++) {
            for (int j = i-1; j >= 0; j--) {
                // The same as for (int j = 0; j < i; j++), but it is slower
                String substr = s.substring(j, i);
                // dp[j] means that substring(0, j) can be segmented with words in wordDict,
                // if substring(j, i) is in wordDict, it means all substring(0, i) can be segmented with words in wordDict
                if(dp[j] && wordDict.contains(substr)){
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }

    // Leetcode 140: Word Break II:
    /**
    Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, add spaces in s to construct a sentence
    where each word is a valid dictionary word.
    Return all such possible sentences.

    Note:

    The same word in the dictionary may be reused multiple times in the segmentation.
    You may assume the dictionary does not contain duplicate words.
    Example 1:

    Input:
    s = "catsanddog"
    wordDict = ["cat", "cats", "and", "sand", "dog"]
    Output:
    [
    "cats and dog",
    "cat sand dog"
    ]
    Example 2:

    Input:
    s = "pineapplepenapple"
    wordDict = ["apple", "pen", "applepen", "pine", "pineapple"]
    Output:
    [
    "pine apple pen apple",
    "pineapple pen apple",
    "pine applepen apple"
    ]
    Explanation: Note that you are allowed to reuse a dictionary word.
    Example 3:

    Input:
    s = "catsandog"
    wordDict = ["cats", "dog", "sand", "and", "cat"]
    Output:
    []
    */
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> result = new ArrayList<String>();
        int n = s.length();
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        Set<String> set = new HashSet<String>(wordDict);
        Map<Integer, List<Integer>> map = new HashMap<>();
        // construct graph:
        for (int i = 1; i <= n; i++) {
            for (int j = i-1; j >= 0; j--) {
                String substr = s.substring(j, i);
                if (dp[j] && set.contains(substr)) {
                    dp[i] = true;
                    if (!map.containsKey(i)) {
                        List<Integer> list = new ArrayList<Integer>();
                        list.add(j);
                        map.put(i, list);
                    }
                    else {
                        map.get(i).add(j);
                    }
                }
            }
        }
        if (!dp[n]) {
            return result;
        }

        // DFS to generate all paths from 0 to n:
        dfs(s, result, map, new StringBuffer(), n, n);
        return result;
    }

    private void dfs(String s, List<String> result, Map<Integer, List<Integer>> map, StringBuffer sb, int index, int n) {
        if (index == 0) {
            // create a new StringBuffer because StringBuffer is mutable
            StringBuffer res = new StringBuffer(sb);
            // remove leading space
            res.deleteCharAt(0);
            result.add(res.toString());
            return;
        }
        List<Integer> prevIndex = map.get(index);
        for (int i : prevIndex) {
            String substr = s.substring(i, index);
            sb.insert(0, substr);
            sb.insert(0, " ");
            dfs(s, result, map, sb, i, n);
            // remove previous leading result
            sb.delete(0, substr.length()+1);
        }
    }

    // Leetcode 132: Palindrome Partitioning II
    // https://leetcode.com/problems/palindrome-partitioning-ii
    /**
    Given a string s, partition s such that every substring of the partition is a palindrome.

    Return the minimum cuts needed for a palindrome partitioning of s.

    Example:

    Input: "aab"
    Output: 1
    Explanation: The palindrome partitioning ["aa","b"] could be produced using 1 cut.
    */
    /**
    if [j,i] is palindrome:
        dp[i] is the min cut of dp[j-1] + 1, j <= i
    [j, i] is palindrome is [j+1, i-1] is palindrome and cList[j] == cList[i]
    */
    // Running Time Complexity: O(N^2), Space Complexity: O(N^2)
    public int minCut(String s) {
        int n = s.length();
        char[] cList = s.toCharArray();
        int[] dp = new int[n];
        boolean[][] pal = new boolean[n][n];
        for (int r = 0; r < n; r++) {
            int min = r;
            for (int l = 0; l <= r; l++) {
                if (cList[l] == cList[r] && (l + 1 > r - 1 || pal[l+1][r-1])) {
                    pal[l][r] = true;
                    if (l == 0) {
                        min = 0;
                    }
                    else {
                        min = Math.min(min, dp[l-1]+1);
                    }
                }
            }
            dp[r] = min;
        }
        return dp[n-1];
    }

    // Best Solution:
    // Running Time Complexity: O(N ^ 2), Space Complexity: O(N)
    public int minCut(String s) {
        int n = s.length();
        char[] cList = s.toCharArray();
        int[] dp = new int[n];
        // initial array:
        for (int i = 0; i < n; i++) {
            dp[i] = i;
        }

        for (int i = 0; i < n; i++) {
            expandString(cList, i, i, n, dp);
            expandString(cList, i, i+1, n, dp);
        }
        return dp[n-1];
    }

    private void expandString(char[] cList, int l, int r, int n, int[] dp) {
        while (l >= 0 && r < n) {
            if (cList[l] != cList[r]) {
                break;
            }
            if (l == 0) {
                dp[r] = 0;
            }
            else {
                dp[r] = Math.min(dp[r], dp[l-1]+1);
            }
            l--; r++;
        }
    }

    // Bruth Force DP solution:
    // Running Time Complexity: O(N^3)
    /**
    dp[i][j] represents the min cut of substring(i,j) so that substring(i, j) is palindrome
    1. For each string with 1 character, min cut = 0
    2. For each string with 2 characters,
            if two characters are the same, min cut = 1
            else, min cut = 1
    3.  For each string with 3 or more characters:
        if substring(i,j) is palindrom,
            dp[i][j] = 0
        else:
            dp[i][j] = 1 + min {
                        dp[i][k] + T[k+1][j], where k = i ... j-1
                    }
    */
    public int minCut(String s) {
        int n = s.length();
        if (isPalindrome(s, 0, n-1)) {
            return 0;
        }
        // dp[i][j] represents the min cut of substring(i,j) so that substring(i, j) is palindrome
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dp[i], n+1);
        }

        // For each string with 1 character, min cut = 0
        for (int i = 0; i < n; i++) {
            dp[i][i] = 0;
        }

        // For each string with 2 characters:
        for (int i = 0; i < n-1; i++) {
            if (s.charAt(i) == s.charAt(i+1)) {
                dp[i][i+1] = 0;
            }
            else {
                dp[i][i+1] = 1;
            }
        }

        // For each string with 3 or more characters:
        for (int l = 3; l <= n; l++) {
            for (int i = 0; i <= n-l; i++) {
                int j = i+l-1;
                if (isPalindrome(s, i, j)) {
                    dp[i][j] = 0;
                }
                else {
                    for (int k = i; k < j; k++) {
                        int curr = 1 + dp[i][k] + dp[k+1][j];
                        dp[i][j] = Math.min(dp[i][j], curr);
                    }
                }
            }
        }
        return dp[0][n-1];
    }
}