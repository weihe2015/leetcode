public class Solution {
    // Leetcode 198: House Robber:
    // https://leetcode.com/problems/house-robber
    /**
    You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.

    Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.

    Example 1:

    Input: [1,2,3,1]
    Output: 4
    Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
                Total amount you can rob = 1 + 3 = 4.
    Example 2:

    Input: [2,7,9,3,1]
    Output: 12
    Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
    Total amount you can rob = 2 + 9 + 1 = 12.
    */
    // dp solution O(n) time O(n) space
    public int rob(int[] nums) {
        int n = nums.length;
        if(n <= 1) {
            return n == 0 ? 0 : nums[0];
        }
        int[] dp = new int[n];
        dp[0] = nums[0];
        // Either rob the first one or the second one.
        dp[1] = Math.max(nums[0],nums[1]); 
        for(int i = 2; i < n; i++){
            // either rob the current one, nums[i], and add dp[i-2]
            // or not rob the current one, = dp[i-1]
            dp[i] = Math.max(dp[i-1], dp[i-2]+nums[i]);
        }
        return dp[n-1];
    }

    // Leetcode 213: House Robber II:
    // https://leetcode.com/problems/house-robber-ii
    /**
    You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed. 
    All houses at this place are arranged in a circle. That means the first house is the neighbor of the last one. 
    Meanwhile, adjacent houses have security system connected and it will automatically contact the police 
    if two adjacent houses were broken into on the same night.
    Given a list of non-negative integers representing the amount of money of each house, 
    determine the maximum amount of money you can rob tonight without alerting the police.

    Example 1:

    Input: [2,3,2]
    Output: 3
    Explanation: You cannot rob house 1 (money = 2) and then rob house 3 (money = 2),
                because they are adjacent houses.
    Example 2:

    Input: [1,2,3,1]
    Output: 4
    Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
                Total amount you can rob = 1 + 3 = 4.
    */
    // DP Solution: break the problem into two parts: rob from 0 to n-2, or rob from 1 to n-1, since 0th house == n-1 house.
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        if (n == 1) {
            return nums[0];
        }
        else if (n == 2) {
            return Math.max(nums[0], nums[1]);
        }
        return Math.max(rob(nums, 0, n-2, n), rob(nums, 1, n-1, n));
    }
    
    private int rob(int[] nums, int low, int high, int n) {
        int[] dp = new int[n];
        dp[low] = nums[low];
        dp[low+1] = Math.max(nums[low],nums[low+1]);
        for (int i = low+2; i <= high; i++) {
            dp[i] = Math.max(dp[i-1], dp[i-2] + nums[i]);   
        }
        return dp[high];
    }

    // dp solution
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        if (n == 1) {
            return nums[0];
        }
        int[] dp0 = new int[n+1], dp1 = new int[n+1];
        dp0[1] = nums[0];
        for (int i = 2; i <= n; i++) {
            dp0[i] = Math.max(dp0[i-1], dp0[i-2]+nums[i-1]);
            dp1[i] = Math.max(dp1[i-1], dp1[i-2]+nums[i-1]);
        }
        return Math.max(dp0[n-1], dp1[n]);
    }

    // Leetcode 319: House Robber III
    // https://leetcode.com/problems/house-robber-iii
    /**
    The thief has found himself a new place for his thievery again. There is only one entrance to this area, called the "root." Besides the root, each house has one and only one parent house. After a tour, the smart thief realized that "all houses in this place forms a binary tree". It will automatically contact the police if two directly-linked houses were broken into on the same night.

    Determine the maximum amount of money the thief can rob tonight without alerting the police.

    Example 1:

    Input: [3,2,3,null,3,null,1]

         3
        / \
       2   3
        \   \ 
         3   1

    Output: 7 
    Explanation: Maximum amount of money the thief can rob = 3 + 3 + 1 = 7.
    Example 2:

    Input: [3,4,5,1,3,null,1]

      3
     / \
    4   5
    / \   \ 
   1   3   1

    Output: 9
    Explanation: Maximum amount of money the thief can rob = 4 + 5 = 9.
    */
    // Bruth Force Solution With Memorization
    public int rob(TreeNode root) {
        return rob(root, new HashMap<TreeNode, Integer>());
    }
    
    public int rob(TreeNode root, Map<TreeNode, Integer> map) {
        if (root == null) {
            return 0;
        }
        if (map.containsKey(root)) {
            return map.get(root);
        }
        int val = 0;
        if (root.left != null) {
            val += rob(root.left.left, map) + rob(root.left.right, map);
        }
        if (root.right != null) {
            val += rob(root.right.left, map) + rob(root.right.right, map);
        }
        // If root is rob:
        int robVal = root.val + val;
        // If root is not rob:
        int notRobVal = rob(root.left, map) + rob(root.right, map);
        val = Math.max(robVal, notRobVal);
        map.put(root, val);
        return val;
    }

    // solution 2, O(n) time and O(logN) space
    public int rob(TreeNode root) {
        int[] result = robSub(root);
        return Math.max(result[0],result[1]);
    }
    
    public int[] robSub(TreeNode root){
        if(root == null)
            return new int[2]; 
        int[] left = robSub(root.left);
        int[] right = robSub(root.right);
        
        int[] result = new int[2];
        // 0 index: root is not robbed
        result[0] = Math.max(left[0],left[1]) + Math.max(right[0],right[1]);
        // 1 index: root is robbed
        result[1] = root.val + left[0] + right[0];   
        return result;
    }
}