public class Solution {
    // Leetcode 79: Word Search:
    // https://leetcode.com/problems/word-search/
    /**
    * Given a 2D board and a list of words from the dictionary, find all words in
    * the board.
    *
    * Each word must be constructed from letters of sequentially adjacent cell,
    * where "adjacent" cells are those horizontally or vertically neighboring. The
    * same letter cell may not be used more than once in a word.
    *
    * For example, Given words = ["oath","pea","eat","rain"] and board =
    *
    * [ ['o','a','a','n'], ['e','t','a','e'], ['i','h','k','r'], ['i','f','l','v']
    * ] Return ["eat","oath"].
    */
    /**
    Given a 2D board and a word, find if the word exists in the grid.

    The word can be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once.

    For example,
    Given board =

    [
    ['A','B','C','E'],
    ['S','F','C','S'],
    ['A','D','E','E']
    ]
    word = "ABCCED", -> returns true,
    word = "SEE", -> returns true,
    word = "ABCB", -> returns false.
    */
    // Running Time Complexity: O(m*n * 4^k), Space Complexity: O(m*n)
    // where k is the length of the string; mn for for loop and for the dfs method its 4^k.
    // Since the dfs method goes only as deep as the word length
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public boolean exist(char[][] board, String word) {
        if (board == null || board.length == 0) {
            return false;
        }
        int m = board.length, n = board[0].length, idx = 0;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (exist(board, word, i, j, m, n, visited, idx)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean exist(char[][] board, String word, int i, int j, int m, int n, boolean[][] visited, int index) {
        if (index == word.length()) {
            return true;
        }
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j] || board[i][j] != word.charAt(idx)) {
            return false;
        }
        visited[i][j] = true;
        // scan four directions: visited its four direction, return true when one is true.
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            // only one true is enough
            if (exist(board, word, x, y, m, n, visited, idx+1)) {
                return true;
            }
        }
        // if four directions are false, then return false.
        visited[i][j] = false;
        return false;
    }

    // Leetcode 212: Word Search II:
    // https://leetcode.com/problems/word-search-ii/
    /***
    Given a 2D board and a list of words from the dictionary, find all words in the board.

    Each word must be constructed from letters of sequentially adjacent cell,
    where "adjacent" cells are those horizontally or vertically neighboring.
    The same letter cell may not be used more than once in a word.

    Example:

    Input:
    board = [
    ['o','a','a','n'],
    ['e','t','a','e'],
    ['i','h','k','r'],
    ['i','f','l','v']
    ]
    words = ["oath","pea","eat","rain"]

    Output: ["eat","oath"]

    Note:

    All inputs are consist of lowercase letters a-z.
    The values of words are distinct.
    */
    // Running Time: O(m*n * L)
    // Build Trie:   n * wordMaxLength
    // Search: boardWidth * boardHeight * (4^wordMaxLength + wordMaxLength[Trie Search])
    private static final int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
    public List<String> findWords(char[][] board, String[] words) {
        List<String> res = new ArrayList<>();
        if (board == null || board.length == 0 || words.length == 0) {
            return res;
        }
        TrieNode root = new TrieNode();
        for (String word : words) {
            addWord(root, word);
        }
        int m = board.length, n = board[0].length;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                searchWord(res, board, root, i, j, m, n, visited);
            }
        }
        return res;
    }

    private void searchWord(List<String> res, char[][] board, TrieNode curr, int i, int j, int m, int n, boolean[][] visited) {
        if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j]) {
            return;
        }
        char c = board[i][j];
        if (curr.next[c-'a'] == null) {
            return;
        }
        visited[i][j] = true;
        curr = curr.next[c-'a'];
        if (curr.isWord) {
            res.add(curr.word);
            curr.isWord = false;
        }
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            searchWord(res, board, curr, x, y, m, n, visited);
        }
        visited[i][j] = false;
    }

    private void addWord(TrieNode curr, String word) {
        for (char c : word.toCharArray()) {
            if (curr.next[c-'a'] == null) {
                curr.next[c-'a'] = new TrieNode();
            }
            curr = curr.next[c-'a'];
        }
        curr.isWord = true;
        curr.word = word;
    }

    class TrieNode {
        boolean isWord;
        String word;
        TrieNode[] next;
        public TrieNode() {
            this.isWord = false;
            this.next = new TrieNode[26];
        }
    }

    // Leetcode 221: Maximal Square
    // https://leetcode.com/problems/maximal-square/
    /**
    Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

    Example:

    Input:

    1 0 1 0 0
    1 0 1 1 1
    1 1 1 1 1
    1 0 0 1 0

    Output: 4
    */
    // DFS: Running Time Complexity: O(m * n), Space Complexity: O(L) largest square size in the matrix
    public int maximalSquare(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int size = 0;
        for (int i = 0; i < m - size; i++) {
            for (int j = 0; j < n - size; j++) {
                int maxVal = maximalSquare(matrix, i, j, m, n, size);
                size = Math.max(size, maxVal);
            }
        }
        return size * size;
    }

    private int maximalSquare(char[][] matrix, int i, int j, int m, int n, int size) {
        if (i + size >= m || j + size >= n) {
            return 0;
        }
        if (matrix[i][j] == '0') {
            return 0;
        }
        for (int row = i; row <= i + size; row++) {
            for (int col = j; col <= j + size; col++) {
                if (matrix[row][col] == '0') {
                    return 0;
                }
            }
        }
        int maxVal = maximalSquare(matrix, i, j, m, n, size+1);
        return Math.max(size + 1, maxVal);
    }

    // Leetcode 140 Word Break II:
    // https://leetcode.com/problems/word-break-ii/
    /**
    Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, add spaces in s to construct a sentence
    where each word is a valid dictionary word.
    Return all such possible sentences.

    Note:

    The same word in the dictionary may be reused multiple times in the segmentation.
    You may assume the dictionary does not contain duplicate words.
    Example 1:

    Input:
    s = "catsanddog"
    wordDict = ["cat", "cats", "and", "sand", "dog"]
    Output:
    [
    "cats and dog",
    "cat sand dog"
    ]
    Example 2:

    Input:
    s = "pineapplepenapple"
    wordDict = ["apple", "pen", "applepen", "pine", "pineapple"]
    Output:
    [
    "pine apple pen apple",
    "pineapple pen apple",
    "pine applepen apple"
    ]
    Explanation: Note that you are allowed to reuse a dictionary word.
    Example 3:

    Input:
    s = "catsandog"
    wordDict = ["cats", "dog", "sand", "and", "cat"]
    Output:
    []
    */
    // Solution 3: Use DFS:
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> result = new ArrayList<String>();
        int n = s.length();
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        Set<String> set = new HashSet<String>(wordDict);
        Map<Integer, List<Integer>> map = new HashMap<>();
        // construct graph:
        for (int i = 1; i <= n; i++) {
            for (int j = i-1; j >= 0; j--) {
                String substr = s.substring(j, i);
                if (dp[j] && set.contains(substr)) {
                    dp[i] = true;
                    if (!map.containsKey(i)) {
                        List<Integer> list = new ArrayList<Integer>();
                        list.add(j);
                        map.put(i, list);
                    }
                    else {
                        map.get(i).add(j);
                    }
                }
            }
        }
        if (!dp[n]) {
            return result;
        }

        // DFS to generate all paths from 0 to n:
        dfs(s, result, map, new StringBuffer(), n, n);
        return result;
    }

    private void dfs(String s, List<String> result, Map<Integer, List<Integer>> map, StringBuffer sb, int index, int n) {
        if (index == 0) {
            // create a new StringBuffer because StringBuffer is mutable
            StringBuffer res = new StringBuffer(sb);
            // remove leading space
            res.deleteCharAt(0);
            result.add(res.toString());
            return;
        }
        List<Integer> prevIndex = map.get(index);
        for (int i : prevIndex) {
            String substr = s.substring(i, index);
            sb.insert(0, substr);
            sb.insert(0, " ");
            dfs(s, result, map, sb, i, n);
            // remove previous leading result
            sb.delete(0, substr.length()+1);
        }
    }

    // Leetcode 200: Number of Islands:
    // https://leetcode.com/problems/number-of-islands
    /**
    Given a 2d grid map of '1's (land) and '0's (water),
    count the number of islands. An island is surrounded by water and
    is formed by connecting adjacent lands horizontally or vertically.
    You may assume all four edges of the grid are all surrounded by water.

    Example 1:

    Input:
    11110
    11010
    11000
    00000

    Output: 1
    Example 2:

    Input:
    11000
    11000
    00100
    00011

    Output: 3
    */
    /*
    The algorithm works as follow:

    1.Scan each cell in the grid.
    2.If the cell value is '1' explore that island.
    3.Mark the explored island cells with 'x'.
    4.Once finished exploring that island, increment islands counter.
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    res++;
                    numIslands(grid, i, j, m, n);
                }
            }
        }
        return res;
    }

    private void numIslands(char[][] grid, int i, int j, int m, int n) {
        if (i < 0 || i >= m || j < 0 || j >= n) {
            return;
        }
        if (grid[i][j] != '1') {
            return;
        }
        grid[i][j] = 'X';
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            numIslands(grid, x, y, m, n);
        }
    }

    // Leetcode 547: Friend Circles:
    // https://leetcode.com/problems/friend-circles
    /**
    There are N students in a class. Some of them are friends, while some are not. Their friendship is transitive in nature.
    For example, if A is a direct friend of B, and B is a direct friend of C, then A is an indirect friend of C.
    And we defined a friend circle is a group of students who are direct or indirect friends.

    Given a N*N matrix M representing the friend relationship between students in the class. If M[i][j] = 1,
    then the ith and jth students are direct friends with each other, otherwise not.
    And you have to output the total number of friend circles among all the students.

    Example 1:
    Input:
    [[1,1,0],
    [1,1,0],
    [0,0,1]]
    Output: 2
    Explanation:The 0th and 1st students are direct friends, so they are in a friend circle.
    The 2nd student himself is in a friend circle. So return 2.
    Example 2:
    Input:
    [[1,1,0],
    [1,1,1],
    [0,1,1]]
    Output: 1
    Explanation:The 0th and 1st students are direct friends, the 1st and 2nd students are direct friends,
    so the 0th and 2nd students are indirect friends. All of them are in the same friend circle, so return 1.
    Note:
    N is in range [1,200].
    M[i][i] = 1 for all students.
    If M[i][j] = 1, then M[j][i] = 1.
    */
    public int findCircleNum(int[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int n = grid.length;
        boolean[] visited = new boolean[n];
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                findCircleNum(grid, visited, i, n);
                count++;
            }
        }
        return count;
    }

    private void findCircleNum(int[][] grid, boolean[] visited, int idx, int n) {
        if (visited[idx]) {
            return;
        }
        visited[idx] = true;
        for (int i = 0; i < n; i++) {
            if (i == idx) {
                continue;
            }
            if (grid[idx][i] == 1) {
                findCircleNum(grid, visited, i, n);
            }
        }
    }

    // Leetcode 130: Surrounded Regions:
    // https://leetcode.com/problems/surrounded-regions/
    /**
    Given a 2D board containing 'X' and 'O' (the letter O), capture all regions surrounded by 'X'.

    A region is captured by flipping all 'O's into 'X's in that surrounded region.

    Example:

    X X X X
    X O O X
    X X O X
    X O X X
    After running your function, the board should be:

    X X X X
    X X X X
    X X X X
    X O X X
    Explanation:

    Surrounded regions shouldn’t be on the border, which means that any 'O' on the border of the board are not flipped to 'X'.
    Any 'O' that is not on the border and it is not connected to an 'O' on the border will be flipped to 'X'.
    Two cells are connected if they are adjacent cells connected horizontally or vertically.
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public void solve(char[][] board) {
        if (board == null || board.length == 0) {
            return;
        }
        int m = board.length, n = board[0].length;
        // Any 'O' connected to the column boundary cannot be turned to 'X', so mark them as '*'
        for (int i = 0; i < m; i++) {
            if (board[i][0] == 'O') {
                dfs(board, i, 0, m, n);
            }
            if (board[i][n-1] == 'O') {
                dfs(board, i, n-1, m, n);
            }
        }

        // Any 'O' connected to the row boundary cannot be turned to 'X', so mark them as '*'
        for (int j = 0; j < n; j++) {
            if (board[0][j] == 'O') {
                dfs(board, 0, j, m, n);
            }
            if (board[m-1][j] == 'O') {
                dfs(board, m-1, j, m, n);
            }
        }

        // post-processing the board: turn any '*' back to 'O' and turn any 'O' to 'X'
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
                else if (board[i][j] == '*') {
                    board[i][j] = 'O';
                }
            }
        }
    }

    private void dfs(char[][] board, int i, int j, int m, int n) {
        if (i < 0 || i >= m || j < 0 || j >= n || board[i][j] != 'O') {
            return;
        }
        board[i][j] = '*';
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(board, x, y, m, n);
        }
    }

    /**
    Leetcode 207, Course Schedule (Detech if there is a cycle in graph)
    https://leetcode.com/problems/course-schedule/
    There are a total of numCourses courses you have to take, labeled from 0 to numCourses-1.

    Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

    Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?

    Example 1:

    Input: numCourses = 2, prerequisites = [[1,0]]
    Output: true
    Explanation: There are a total of 2 courses to take. 
                To take course 1 you should have finished course 0. So it is possible.
    Example 2:

    Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
    Output: false
    Explanation: There are a total of 2 courses to take. 
                To take course 1 you should have finished course 0, and to take course 0 you should
                also have finished course 1. So it is impossible.
    */
    // DFS
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int i = 0; i < numCourses; i++) {
            graph.put(i, new ArrayList<>());
        }
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            List<Integer> list = graph.get(from);
            list.add(to);
            graph.put(from, list);
        }
        int[] visited = new int[numCourses];
        for (int i = 0; i < numCourses; i++) {
            if (isCyclic(graph, from, visited)) {
                return false;
            }
        }
        return true;
    }

    private boolean isCyclic(Map<Integer, List<Integer>> graph, int from, int[] visited) {
        if (visited[from] == 2) {
            return false;
        }
        else if (visited[from] == 1) {
            return true;
        }
        else {
            visited[from] = 1;
            List<Integer> neighbors = graph.get(from);
            for (int to : neighbors) {
                if (isCyclic(graph, to, visited)) {
                    return true;
                }
            }
            visited[from] = 2;
            return false;
        }
    }

    // Leetcode 286: Walls And Gates:
    // https://www.lintcode.com/problem/walls-and-gates/
    // https://leetcode.com/problems/walls-and-gates
/**
You are given a m x n 2D grid initialized with these three possible values.

-1 - A wall or an obstacle.
0 - A gate.
INF - Infinity means an empty room. We use the value 2^31 - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than 2147483647.
Fill each empty room with the distance to its nearest gate. If it is impossible to reach a gate, it should be filled with INF.

Example:

Given the 2D grid:

INF  -1  0  INF
INF INF INF  -1
INF  -1 INF  -1
  0  -1 INF INF
After running your function, the 2D grid should be:

  3  -1   0   1
  2   2   1  -1
  1  -1   2  -1
  0  -1   3   4
 */

    // DFS:
    private static int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
    public void wallsAndGates(int[][] rooms) {
        // write your code here
        if (rooms.length == 0 || rooms[0].length == 0) {
            return;
        }
        int m = rooms.length, n = rooms[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (rooms[i][j] == 0) {
                    dfs(rooms, i, j, m, n, 0);
                }
            }
        }
    }

    private void dfs(int[][] rooms, int i, int j, int m, int n, int step) {
        if (i < 0 || i >= m || j < 0 || j >= n || rooms[i][j] == -1) {
            return;
        }
        if (rooms[i][j] < step) {
            return;
        }
        rooms[i][j] = step;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(rooms, x, y, m, n, step+1);
        }
    }

    // Leetcode 329: Longest Increasing Path in a Matrix:
    // https://leetcode.com/problems/longest-increasing-path-in-a-matrix
    /**
    Given an integer matrix, find the length of the longest increasing path.

    From each cell, you can either move to four directions: left, right, up or down. You may NOT move diagonally or move outside of the boundary (i.e. wrap-around is not allowed).

    Example 1:

    nums = [
    [9,9,4],
    [6,6,8],
    [2,1,1]
    ]
    Return 4
    The longest increasing path is [1, 2, 6, 9].

    Example 2:

    nums = [
    [3,4,5],
    [3,2,6],
    [2,2,1]
    ]
    Return 4
    The longest increasing path is [3, 4, 5, 6]. Moving diagonally is not allowed.

    1. Do DFS from every cell
    2. Compare every 4 direction and skip cells that are out of boundary or smaller
    3. Get matrix max from every cells max
    4. Use matrix[x][y] <= matrix[i][j] so we dont need a visited[m][n] array
    5. The key is to cache the distance because its highly possible to revisit a cell
    */
    // Running Time Complexity: O(m * n), Space Complexity: O(m * n)
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        // 记忆化搜索，省去很大的空间
        int[][] cached = new int[m][n];
        int maxLen = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int len = dfs(matrix, cached, i, j, m, n, Integer.MIN_VALUE);
                maxLen = Math.max(maxLen, len);
            }
        }
        return maxLen;
    }

    private int dfs(int[][] matrix, int[][] cached, int i, int j, int m, int n, int prevNum) {
        if (i < 0 || i >= m || j < 0 || j >= n || matrix[i][j] <= prevNum) {
            return 0;
        }
        if (cached[i][j] != 0) {
            return cached[i][j];
        }
        // 自身长度至少是1
        int maxLen = 1;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            int currLen = 1 + dfs(matrix, cached, x, y, m, n, matrix[i][j]);
            maxLen = Math.max(maxLen, currLen);
        }
        // 四个方向更新完之后，记得保存以这个点出发的最长路径，下次就不用算了
        cached[i][j] = maxLen;
        return maxLen;
    }

    // Leetcode 463: Island Perimeter:
    // https://leetcode.com/problems/island-perimeter
    /**
     * You are given a map in form of a two-dimensional integer grid where 1 represents land and 0 represents water.
        Grid cells are connected horizontally/vertically (not diagonally).
        The grid is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells).
        The island doesn't have "lakes" (water inside that isn't connected to the water around the island).
        One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100.

        Determine the perimeter of the island.

        Example:

        [[0,1,0,0],
        [1,1,1,0],
        [0,1,0,0],
        [1,1,0,0]]

        Answer: 16
        Explanation: The perimeter is the 16 yellow stripes in the image below:
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int islandPerimeter(int[][] grid) {
        int len = 0;
        if (grid == null || grid.length == 0) {
            return len;
        }
        int m = grid.length, n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    len += dfs(grid, i, j, m, n, visited);
                }
            }
        }
        return len;
    }

    private int dfs(int[][] grid, int i, int j, int m, int n, boolean[][] visited) {
        // 遇到水，就加一条边
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 0) {
            return 1;
        }
        // 如果当前cell已经被访问过了，那就返回 0
        if (visited[i][j]) {
            return 0;
        }
        visited[i][j] = true;
        int count = 0;
        for (int[] dir : dirs) {
            int x = dir[0] + i;
            int y = dir[1] + j;
            count += dfs(grid, x, y, m, n, visited);
        }
        return count;
    }

    // Leetcode 695 Max Area of Island:
    // https://leetcode.com/problems/island-perimeter
    /**
    Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

    Find the maximum area of an island in the given 2D array. (If there is no island, the maximum area is 0.)

    Example 1:
    [[0,0,1,0,0,0,0,1,0,0,0,0,0],
    [0,0,0,0,0,0,0,1,1,1,0,0,0],
    [0,1,1,0,1,0,0,0,0,0,0,0,0],
    [0,1,0,0,1,1,0,0,1,0,1,0,0],
    [0,1,0,0,1,1,0,0,1,1,1,0,0],
    [0,0,0,0,0,0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,0,1,1,1,0,0,0],
    [0,0,0,0,0,0,0,1,1,0,0,0,0]]
    Given the above grid, return 6. Note the answer is not 11, because the island must be connected 4-directionally.
    Example 2:
    [[0,0,0,0,0,0,0,0]]
    Given the above grid, return 0.
    Note: The length of each dimension in the given grid does not exceed 50.
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int maxAreaOfIsland(int[][] grid) {
        int max = 0;
        if (grid == null || grid.length == 0) {
            return max;
        }
        int m = grid.length, n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    int num = dfs(grid, i, j, m, n, visited);
                    max = Math.max(max, num);
                }
            }
        }
        return max;
    }

    private int dfs(int[][] grid, int i, int j, int m, int n, boolean[][] visited) {
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 0 || visited[i][j]) {
            return 0;
        }
        visited[i][j] = true;
        int count = 1;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            count += dfs(grid, x, y, m, n, visited);
        }
        return count;
    }

    // Leetcode 417 Pacific Atlantic Water Flow:
    // https://leetcode.com/problems/pacific-atlantic-water-flow/
    /**
     * Given an m x n matrix of non-negative integers representing the height of each unit cell in a continent,
     * the "Pacific ocean" touches the left and top edges of the matrix and the "Atlantic ocean" touches the right and bottom edges.
    Water can only flow in four directions (up, down, left, or right) from a cell to another one with height equal or lower.

    Find the list of grid coordinates where water can flow to both the Pacific and Atlantic ocean.

    Note:
    The order of returned grid coordinates does not matter.
    Both m and n are less than 150.
    Example:

    Given the following 5x5 matrix:

    Pacific ~   ~   ~   ~   ~
        ~  1   2   2   3  (5) *
        ~  3   2   3  (4) (4) *
        ~  2   4  (5)  3   1  *
        ~ (6) (7)  1   4   5  *
        ~ (5)  1   1   2   4  *
            *   *   *   *   * Atlantic

    Return:

    [[0, 4], [1, 3], [1, 4], [2, 2], [3, 0], [3, 1], [4, 0]] (positions with parentheses in above matrix).
     *
     */
    private static final int[][] dirs = {{0,1}, {0,-1}, {1,0}, {-1,0}};
    public List<int[]> pacificAtlantic(int[][] matrix) {
        List<int[]> result = new ArrayList<int[]>();
        if (matrix == null || matrix.length == 0) {
            return result;
        }
        int m = matrix.length, n = matrix[0].length;
        // find all the points that can flows to pacific:
        boolean[][] pacificMap = new boolean[m][n];
        boolean[][] atlanticMap = new boolean[m][n];

        // scan the first and last row, from first column and last column:
        for (int j = 0; j < n; j++) {
            dfs(matrix, 0, j, m, n, pacificMap, 0);
            dfs(matrix, m-1, j, m, n, atlanticMap, 0);
        }

        // scan the first column and last column, from first row to last row:
        for (int i = 0; i < m; i++) {
            dfs(matrix, i, 0, m, n, pacificMap, 0);
            dfs(matrix, i, n-1, m, n, atlanticMap, 0);
        }

        // find out which cells can go to both pacific and atlantic:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (pacificMap[i][j] && atlanticMap[i][j]) {
                    result.add(new int[]{i, j});
                }
            }
        }
        return result;
    }

    private void dfs(int[][] matrix, int i, int j, int m, int n, boolean[][] map, int prev) {
        if (i < 0 || i >= m || j < 0 || j >= n || map[i][j] || matrix[i][j] < prev) {
            return;
        }
        map[i][j] = true;
        // scan four directions:
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(matrix, x, y, m, n, map, matrix[i][j]);
        }
    }

    /**
    Leetcode 743: Network Delay Time
    https://leetcode.com/problems/network-delay-time/

    There are N network nodes, labelled 1 to N.

    Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the target node, and w is the time it takes for a signal to travel from source to target.

    Now, we send a signal from a certain node K. How long will it take for all nodes to receive the signal? If it is impossible, return -1.

    Ex: 1

    // time[0]: from, time[1]: to, time[2]: cost
    Input: times = [[2,1,1],[2,3,1],[3,4,1]], N = 4, K = 2
    Output: 2

    Note:

    N will be in the range [1, 100].
    K will be in the range [1, N].
    The length of times will be in the range [1, 6000].
    All edges times[i] = (u, v, w) will have 1 <= u, v <= N and 0 <= w <= 100.
    */
    // key -> nodeId, val -> dist from node k to itself
    private Map<Integer, Integer> distMap;
    // DFS:
    // Time Complexity: O(N^N + ElogE), where E is the length of int[] times
    // Space Complexity: O(N + E), graph size is O(E) + the DFS stack depth O(N)
    public int networkDelayTime(int[][] times, int N, int K) {
        // build graph:
        Map<Integer, List<int[]>> graph = new HashMap<>();
        for (int[] time : times) {
            int from = time[0];
            int to = time[1];
            int cost = time[2];
            List<int[]> children = graph.getOrDefault(from, new ArrayList<>());
            children.add(new int[]{to, cost});
            graph.put(from, children);
        }
        for (int key : graph.keySet()) {
            List<int[]> list = graph.get(key);
            // sort by the cost in the children
            Collections.sort(list, Comparator.comparingInt(l -> l[1]));
        }
        this.distMap = new HashMap<>();
        for (int i = 1; i <= N; i++) {
            distMap.put(i, Integer.MAX_VALUE);
        }
        dfs(graph, K, 0);

        int maxVal = 0;
        for (int val : distMap.values()) {
            if (val == Integer.MAX_VALUE) {
                return -1;
            }
            maxVal = Math.max(maxVal, val);
        }
        return maxVal;
    }

    private void dfs(Map<Integer, List<int[]>> graph, int from, int cost) {
        if (distMap.get(from) <= cost) {
            return;
        }
        distMap.put(from, cost);
        if (!graph.containsKey(from)) {
            return;
        }
        List<int[]> children = graph.get(from);
        for (int[] child : children) {
            dfs(graph, child[0], cost + child[1]);
        }
    }

    /**
    Leetcode 1376: Time Needed to Inform All Employees
    https://leetcode.com/problems/time-needed-to-inform-all-employees/

    A company has n employees with a unique ID for each employee from 0 to n - 1. The head of the company has is the one with headID.

    Each employee has one direct manager given in the manager array where manager[i] is the direct manager of the i-th employee, manager[headID] = -1. Also it's guaranteed that the subordination relationships have a tree structure.

    The head of the company wants to inform all the employees of the company of an urgent piece of news. He will inform his direct subordinates and they will inform their subordinates and so on until all employees know about the urgent news.

    The i-th employee needs informTime[i] minutes to inform all of his direct subordinates (i.e After informTime[i] minutes, all his direct subordinates can start spreading the news).

    Return the number of minutes needed to inform all the employees about the urgent news.

    
    Example 1:

    Input: n = 1, headID = 0, manager = [-1], informTime = [0]
    Output: 0
    Explanation: The head of the company is the only employee in the company.
    Example 2:
        2
    / / | \ \  
    0 1  3 4 5

    Input: n = 6, headID = 2, manager = [2,2,-1,2,2,2], informTime = [0,0,1,0,0,0]
    Output: 1
    Explanation: The head of the company with id = 2 is the direct manager of all the employees in the company and needs 1 minute to inform them all.
    The tree structure of the employees in the company is shown.

    Example 3:

    6 - 5 - 4 - 3 - 2 - 1 - 0

    Input: n = 7, headID = 6, manager = [1,2,3,4,5,6,-1], informTime = [0,6,5,4,3,2,1]
    Output: 21
    Explanation: The head has id = 6. He will inform employee with id = 5 in 1 minute.
    The employee with id = 5 will inform the employee with id = 4 in 2 minutes.
    The employee with id = 4 will inform the employee with id = 3 in 3 minutes.
    The employee with id = 3 will inform the employee with id = 2 in 4 minutes.
    The employee with id = 2 will inform the employee with id = 1 in 5 minutes.
    The employee with id = 1 will inform the employee with id = 0 in 6 minutes.
    Needed time = 1 + 2 + 3 + 4 + 5 + 6 = 21.


    Example 4:

    Input: n = 15, headID = 0, manager = [-1,0,0,1,1,2,2,3,3,4,4,5,5,6,6], informTime = [1,1,1,1,1,1,1,0,0,0,0,0,0,0,0]
    Output: 3
    Explanation: The first minute the head will inform employees 1 and 2.
    The second minute they will inform employees 3, 4, 5 and 6.
    The third minute they will inform the rest of employees.
    Example 5:

    Input: n = 4, headID = 2, manager = [3,3,-1,2], informTime = [0,0,162,914]
    Output: 1076
    */
    // DFS: From top to down
    private int maxSum = Integer.MIN_VALUE;
    public int numOfMinutes(int n, int headID, int[] manager, int[] informTime) {
        // build graph:
        // key -> parNode, val -> list of childNode
        Map<Integer, List<Integer>> childMap = new HashMap<>();
        for (int i = 0; i < n; i++) {
            if (i == headID) {
                continue;
            }
            int mgrId = manager[i];
            List<Integer> children = childMap.getOrDefault(mgrId, new ArrayList<>());
            children.add(i);
            childMap.put(mgrId, children);
        }
        // DFS:
        int sum = informTime[headID];
        dfs(childMap, informTime, headID, sum);
        return maxSum;
    }
    
    private void dfs(Map<Integer, List<Integer>> childMap, int[] informTime,  int from, int sum) {
        if (!childMap.containsKey(from)) {
            maxSum = Math.max(maxSum, sum);
            return;
        }
        List<Integer> children = childMap.get(from);
        for (int child : children) {
            dfs(childMap, informTime, child, sum + informTime[child]);
        }
    }


}