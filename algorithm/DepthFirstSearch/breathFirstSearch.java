public class Solution {
    /**
    Leetcode 139: Word Break:
    https://leetcode.com/problems/word-break

    Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

    Note:

    The same word in the dictionary may be reused multiple times in the segmentation.
    You may assume the dictionary does not contain duplicate words.
    Example 1:

    Input: s = "leetcode", wordDict = ["leet", "code"]
    Output: true
    Explanation: Return true because "leetcode" can be segmented as "leet code".
    Example 2:

    Input: s = "applepenapple", wordDict = ["apple", "pen"]
    Output: true
    Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
                Note that you are allowed to reuse a dictionary word.
    Example 3:

    Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
    Output: false

    01234567
    leetcode
    j..i
    dp[i] -> break here
    it is likely that the match word will be found at the end of the finished part of the string, 
    but not a really long word which begins from the beginning of the string
    */
    // Running Time Complexity: O(n * L), where n is s.length() and L is average length of words in wordDict
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet<String>(wordDict);
        if (set.contains(s)) {
            return true;
        }
        Queue<Integer> q = new LinkedList<Integer>();
        q.offer(0);
        Set<Integer> visited = new HashSet<Integer>();
        visited.add(0);
        while (!q.isEmpty()) {
            int currIndex = q.poll();
            for (int i = currIndex+1, n = s.length(); i <= n; i++) {
                if (visited.contains(i)) {
                    continue;
                }
                String substr = s.substring(currIndex, i);
                if (set.contains(substr)) {
                    if (i == n) {
                        return true;
                    }
                    q.offer(i);
                    visited.add(i);
                }
            }
        }
        return false;
    }

    /**
    Leetcode 207, Course Schedule (Detech if there is a cycle in graph)
    https://leetcode.com/problems/course-schedule/

    There are a total of numCourses courses you have to take, labeled from 0 to numCourses-1.

    Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

    Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?

    Example 1:

    Input: numCourses = 2, prerequisites = [[1,0]]
    Output: true
    Explanation: There are a total of 2 courses to take. 
                To take course 1 you should have finished course 0. So it is possible.
    Example 2:

    Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
    Output: false
    Explanation: There are a total of 2 courses to take. 
                To take course 1 you should have finished course 0, and to take course 0 you should
                also have finished course 1. So it is impossible.
    */
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int i = 0; i < numCourses; i++) {
            graph.put(i, new ArrayList<>());
        }
        int[] indegree = new int[numCourses];
        for (int[] prerequisite : prerequisites) {
            int from = prerequisite[1];
            int to = prerequisite[0];
            List<Integer> list = graph.get(from);
            list.add(to);
            graph.put(from, list);
            indegree[to]++;
        }

        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                queue.offer(i);
            }
        }
        int cnt = 0;
        while (!queue.isEmpty()) {
            int from = queue.poll();
            cnt++;
            for (int to : graph.get(from)) {
                indegree[to]--;
                if (indegree[to] == 0) {
                    queue.offer(to);
                }
            }
        }
        return cnt == numCourses;
    }

    /**
    Leetcode 317: Shortest Distance from All buildings:
    https://leetcode.com/problems/shortest-distance-from-all-buildings
    https://www.lintcode.com/problem/shortest-distance-from-all-buildings/

    You want to build a house on an empty land which reaches all buildings in the shortest amount of distance. 
    You can only move up, down, left and right. You are given a 2D grid of values 0, 1 or 2, where:

    Each 0 marks an empty land which you can pass by freely.
    Each 1 marks a building which you cannot pass through.
    Each 2 marks an obstacle which you cannot pass through.
    For example, given three buildings at (0,0), (0,4), (2,2), and an obstacle at (0,2):

    1 - 0 - 2 - 0 - 1
    |   |   |   |   |
    0 - 0 - 0 - 0 - 0
    |   |   |   |   |
    0 - 0 - 1 - 0 - 0
    The point (1,2) is an ideal empty land to build a house, as the total travel distance of 3+3+1=7 is minimal. So return 7.

    Note:
    There will be at least one building. If it is not possible to build such house according to the above rules, return -1.
    
    */
    /**
    我们还是用BFS来做，其中dist是累加距离场，cnt表示某个位置已经计算过的建筑数，变量buildingCnt为建筑的总数，
    我们还是用queue来辅助计算，注意这里的dist的更新方式跟上面那种方法的不同，这里的dist由于是累积距离场，
    所以不能用dist其他位置的值来更新，而是需要直接加上和建筑物之间的距离，这里用level来表示，每遍历一层，level自增1，
    这样我们就需要所加个for循环，来控制每一层中的level值是相等的

    for each building, perform BFS on the whole graph, and mark the shorest distance from this building to
    cell with value 0.
    Whenever the cell has the lowest distance and have reachable count the same as total number
    of building, it is the answer.
    */
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int shortestDistance(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int[][] distance = new int[m][n];
        int[][] reach = new int[m][n];
        int buildingCnt = 0;
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != 1) {
                    continue;
                }
                buildingCnt++;
                Queue<int[]> queue = new LinkedList<>();
                q.offer(new int[]{i, j});
                
                boolean[][] visited = new boolean[m][n];
                int level = 1;
                
                while (!queue.isEmpty()) {
                    int size = queue.size();
                    for (int s = 1; s <= size; s++) {
                        int[] curr = q.poll();
                        for (int[] dir : dirs) {
                            int x = curr[0] + dir[0];
                            int y = curr[1] + dir[1];
                            if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] != 0 || visited[x][y]) {
                                continue;
                            }
                            distance[x][y] += level;
                            reach[x][y]++;
                            visited[x][y] = true;
                            queue.offer(new int[]{x, y});
                        }
                    }
                    level++;
                }
            }
        }
        
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0 && reach[i][j] == buildingCnt) {
                    res = Math.min(res, distance[i][j]);
                }
            }
        }
        return res == Integer.MAX_VALUE ? -1 : res;
    }

    // Leetcode 542: 01 Matrix
    // https://leetcode.com/problems/01-matrix/
    /**
    Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.

    The distance between two adjacent cells is 1.
    Example 1: 
    Input:
    0 0 0
    0 1 0
    0 0 0

    Output:
    0 0 0
    0 1 0
    0 0 0

    Example 2: 
    Input:
    0 0 0
    0 1 0
    1 1 1

    Output:
    0 0 0
    0 1 0
    1 2 1
    Note:
    The number of elements of the given matrix will not exceed 10,000.
    There are at least one 0 in the given matrix.
    The cells are adjacent in only four directions: up, down, left and right.
    */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int[][] updateMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return matrix;
        }
        int m = matrix.length, n = matrix[0].length;
        Queue<Point> queue = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    queue.offer(new Point(i, j));
                }
                else {
                    matrix[i][j] = Integer.MAX_VALUE;
                }
            }
        }
        while (!queue.isEmpty()) {
            Point point = queue.poll();
            for (int[] dir : dirs) {
                int i = point.x;
                int j = point.y;
                int x = i + dir[0];
                int y = j + dir[1];
                // If newly calculated distance >= current distance, then we don't need to explore that cell again.
                if (x < 0 || x >= m || y < 0 || y >= n || matrix[x][y] <= matrix[i][j] + 1) {
                    continue;
                }
                queue.add(new Point(x, y));
                matrix[x][y] = matrix[i][j] + 1;
            }
        }
        return matrix;
    }
    
    class Point {
        int x, y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    // Leetcode 417 Pacific Atlantic Water Flow:
    // https://leetcode.com/problems/pacific-atlantic-water-flow/
    /**
     * Given an m x n matrix of non-negative integers representing the height of each unit cell in a continent, 
     * the "Pacific ocean" touches the left and top edges of the matrix and the "Atlantic ocean" touches the right and bottom edges.
    Water can only flow in four directions (up, down, left, or right) from a cell to another one with height equal or lower.

    Find the list of grid coordinates where water can flow to both the Pacific and Atlantic ocean.

    Note:
    The order of returned grid coordinates does not matter.
    Both m and n are less than 150.
    Example:

    Given the following 5x5 matrix:

    Pacific ~   ~   ~   ~   ~ 
        ~  1   2   2   3  (5) *
        ~  3   2   3  (4) (4) *
        ~  2   4  (5)  3   1  *
        ~ (6) (7)  1   4   5  *
        ~ (5)  1   1   2   4  *
            *   *   *   *   * Atlantic

    Return:

    [[0, 4], [1, 3], [1, 4], [2, 2], [3, 0], [3, 1], [4, 0]] (positions with parentheses in above matrix).
     * 
     */
    private static final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public List<int[]> pacificAtlantic(int[][] matrix) {
        List<int[]> result = new ArrayList<int[]>();
        if (matrix == null || matrix.length == 0) {
            return result;
        }
        int m = matrix.length, n = matrix[0].length;
        boolean[][] pacificMap = new boolean[m][n];
        boolean[][] atlanticMap = new boolean[m][n];
        // scan the first and last row, from first column and last column:
        for (int j = 0; j < n; j++) {
            bfs(matrix, pacificMap, 0, j, m, n);
            bfs(matrix, atlanticMap, m-1, j, m, n);
        }
        // scan the first column and last column, from first row to last row:
        for (int i = 0; i < m; i++) {
            bfs(matrix, pacificMap, i, 0, m, n);
            bfs(matrix, atlanticMap, i, n-1, m, n);
        }
        // find out which cells can go to both pacific and atlantic:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (pacificMap[i][j] && atlanticMap[i][j]) {
                    result.add(new int[]{i, j});
                }
            }
        }
        return result;
    }
    
    private void bfs(int[][] matrix, boolean[][] visited, int i, int j, int m, int n) {
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{i, j});
        while (!queue.isEmpty()) {
            int[] curr = queue.poll();
            visited[curr[0]][curr[1]] = true;
            for (int[] dir : dirs) {
                int x = point.x + dir[0];
                int y = point.y + dir[1];
                if (x < 0 || x >= m || y < 0 || y >= n || visited[x][y] || matrix[x][y] < matrix[point.x][point.y]) {
                    continue;
                }
                queue.offer(new int[]{x, y});
            }
        }
    }

    /**
    Leetcode 1293: Shortest Path in a Grid with Obstracle.
    https://leetcode.com/problems/shortest-path-in-a-grid-with-obstacles-elimination

    Given a m * n grid, where each cell is either 0 (empty) or 1 (obstacle). In one step, you can move up, down, left or right from and to an empty cell.

    Return the minimum number of steps to walk from the upper left corner (0, 0) to the lower right corner (m-1, n-1) given that you can eliminate at most k obstacles. If it is not possible to find such walk return -1.

    Example 1:

    Input: 
    grid = 
    [[0,0,0],
    [1,1,0],
    [0,0,0],
    [0,1,1],
    [0,0,0]], 
    k = 1
    Output: 6
    Explanation: 
    The shortest path without eliminating any obstacle is 10. 
    The shortest path with one obstacle elimination at position (3,2) is 6. Such path is (0,0) -> (0,1) -> (0,2) -> (1,2) -> (2,2) -> (3,2) -> (4,2).
    

    Example 2:

    Input: 
    grid = 
    [[0,1,1],
    [1,1,1],
    [1,0,0]], 
    k = 1
    Output: -1
    Explanation: 
    We need to eliminate at least two obstacles to find such a walk.

    Constraints:

    grid.length == m
    grid[0].length == n
    1 <= m, n <= 40
    1 <= k <= m*n
    grid[i][j] == 0 or 1
    grid[0][0] == grid[m-1][n-1] == 0

    https://leetcode-cn.com/problems/shortest-path-in-a-grid-with-obstacles-elimination/solution/wang-ge-zhong-de-zui-duan-lu-jing-bfssuan-fa-shi-x/
    */

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    /**
    Run Time Complexity: O(m * n * min(m+n, k))
    */
    public int shortestPath(int[][] grid, int k) {
        int m = grid.length;
        int n = grid[0].length;
        // k 的上限是 m * n, 但考虑一条从 (0, 0) 向下走到 (m - 1, 0) 再向右走到 (m - 1, n - 1) 的路径，它经过了 m + n - 1 个位置，其中起点 (0, 0) 和终点 (m - 1, n - 1) 没有障碍物，那么这条路径上最多只会有 m + n - 3 个障碍物
        k = Math.min(k, m+n-3);

        boolean[][][] visited = new boolean[m][n][k+1];
        int[] start = new int[]{0,0};
        int[] end = new int[]{m-1, n-1};
        
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{0,0,0});
        visited[0][0][0] = true;
        
        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] curr = queue.poll();
                int i = curr[0];
                int j = curr[1];
                int oneCnt = curr[2];
                
                if (i == end[0] && j == end[1]) {
                    return steps;
                }
                
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    // 越界 或者 穿越障碍次数已满
                    if (x < 0 || x >= m || y < 0 || y >= n || (grid[x][y] == 1 && oneCnt >= k)) {
                        continue;
                    }
                    int newCnt = grid[x][y] == 1 ? oneCnt + 1 : oneCnt;
                    // 四个方向节点是否被访问过（第三维度）
                    if (visited[x][y][newCnt]) {
                        continue;
                    }
                    visited[x][y][newCnt] = true;
                    queue.offer(new int[]{x,y,newOneCnt});
                }
            }
            steps++;
        }
        return -1;
    }

    /**
    Leetcode 1553: Minimum Number of Days to Eat N Oranges
    https://leetcode.com/problems/minimum-number-of-days-to-eat-n-oranges

    There are n oranges in the kitchen and you decided to eat some of these oranges every day as follows:

    Eat one orange.
    If the number of remaining oranges (n) is divisible by 2 then you can eat  n/2 oranges.
    If the number of remaining oranges (n) is divisible by 3 then you can eat  2*(n/3) oranges.
    You can only choose one of the actions per day.

    Return the minimum number of days to eat n oranges.

    Example 1:

    Input: n = 10
    Output: 4
    Explanation: You have 10 oranges.
    Day 1: Eat 1 orange,  10 - 1 = 9.  
    Day 2: Eat 6 oranges, 9 - 2*(9/3) = 9 - 6 = 3. (Since 9 is divisible by 3)
    Day 3: Eat 2 oranges, 3 - 2*(3/3) = 3 - 2 = 1. 
    Day 4: Eat the last orange  1 - 1  = 0.
    You need at least 4 days to eat the 10 oranges.
    Example 2:

    Input: n = 6
    Output: 3
    Explanation: You have 6 oranges.
    Day 1: Eat 3 oranges, 6 - 6/2 = 6 - 3 = 3. (Since 6 is divisible by 2).
    Day 2: Eat 2 oranges, 3 - 2*(3/3) = 3 - 2 = 1. (Since 3 is divisible by 3)
    Day 3: Eat the last orange  1 - 1  = 0.
    You need at least 3 days to eat the 6 oranges.
    Example 3:

    Input: n = 1
    Output: 1
    Example 4:

    Input: n = 56
    Output: 6

    Constraints:

    1 <= n <= 2*10^9
    */
    // BFS:
    public int minDays(int n) {
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(n);
        Set<Integer> visited = new HashSet<>();
        
        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int curr = queue.poll();
                if (curr == 0) {
                    return steps;
                }
                if (visited.contains(curr)) {
                    continue;
                }
                visited.add(curr);
                if (curr % 3 == 0) {
                    queue.offer(curr/3);
                }
                if (curr % 2 == 0) {
                    queue.offer(curr/2);
                }
                queue.offer(curr - 1);
            }
            steps++;
        }
        return steps;
    }

    /*
    Leetcode 1091: Shortest Path in Binary Matrix
    https://leetcode.com/problems/shortest-path-in-binary-matrix/

    In an N by N square grid, each cell is either empty (0) or blocked (1).

    A clear path from top-left to bottom-right has length k if and only if it is composed of cells C_1, C_2, ..., C_k such that:

    Adjacent cells C_i and C_{i+1} are connected 8-directionally (ie., they are different and share an edge or corner)
    C_1 is at location (0, 0) (ie. has value grid[0][0])
    C_k is at location (N-1, N-1) (ie. has value grid[N-1][N-1])
    If C_i is located at (r, c), then grid[r][c] is empty (ie. grid[r][c] == 0).
    Return the length of the shortest such clear path from top-left to bottom-right.  If such a path does not exist, return -1.

    Ex: [[0,1],[1,0]]
    Output: 2

    Ex: [[0,0,0],[1,1,0],[1,1,0]]
    Output: 4

    Note:

    1 <= grid.length == grid[0].length <= 100
    grid[r][c] is 0 or 1
    */
    static private int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0},{1,1},{1,-1},{-1,1},{-1,-1}};
    public int shortestPathBinaryMatrix(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        if (grid[0][0] == 1) {
            return -1;
        }
        boolean[][] visited = new boolean[m][n];
        int[] start = new int[]{0,0};
        int[] end   = new int[]{m-1, n-1};
        visited[0][0] = true;
        
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{0,0});

        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] curr = queue.poll();
                int i = curr[0];
                int j = curr[1];
                if (i == end[0] && j == end[1]) {
                    return steps + 1;
                }
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];
                    if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] == 1 || visited[x][y]) {
                        continue;
                    }
                    visited[x][y] = true;
                    queue.offer(new int[]{x, y});
                }
            }
            steps++;
        }
        return -1;
    }

    /**
    Leetcode 542: 01 Matrix
    https://leetcode.com/problems/01-matrix/

    Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.

    The distance between two adjacent cells is 1.
    Example 1: 
    Input:

    0 0 0
    0 1 0
    0 0 0
    Output:
    0 0 0
    0 1 0
    0 0 0
    Example 2: 
    Input:

    0 0 0
    0 1 0
    1 1 1
    Output:
    0 0 0
    0 1 0
    1 2 1
    Note:
    The number of elements of the given matrix will not exceed 10,000.
    There are at least one 0 in the given matrix.
    The cells are adjacent in only four directions: up, down, left and right.
    */
    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    public int[][] updateMatrix(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        
        Queue<int[]> queue = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    queue.offer(new int[]{i, j});
                }
                else {
                    matrix[i][j] = Integer.MAX_VALUE;
                }
            }
        }
        
        while (!queue.isEmpty()) {
            int[] curr = queue.poll();
            int i = curr[0];
            int j = curr[1];
            for (int[] dir : dirs) {
                int x = i + dir[0];
                int y = j + dir[1];
                if (x < 0 || x >= m || y < 0 || y >= n || matrix[x][y] <= matrix[i][j] + 1) {
                    continue;
                }
                matrix[x][y] = matrix[i][j] + 1;
                queue.offer(new int[]{x, y});
            }
        }
        return matrix;
    }

    /**
    Leetcode 1376: Time Needed to Inform All Employees
    https://leetcode.com/problems/time-needed-to-inform-all-employees/

    A company has n employees with a unique ID for each employee from 0 to n - 1. The head of the company has is the one with headID.

    Each employee has one direct manager given in the manager array where manager[i] is the direct manager of the i-th employee, manager[headID] = -1. Also it's guaranteed that the subordination relationships have a tree structure.

    The head of the company wants to inform all the employees of the company of an urgent piece of news. He will inform his direct subordinates and they will inform their subordinates and so on until all employees know about the urgent news.

    The i-th employee needs informTime[i] minutes to inform all of his direct subordinates (i.e After informTime[i] minutes, all his direct subordinates can start spreading the news).

    Return the number of minutes needed to inform all the employees about the urgent news.

    
    Example 1:

    Input: n = 1, headID = 0, manager = [-1], informTime = [0]
    Output: 0
    Explanation: The head of the company is the only employee in the company.
    Example 2:
        2
    / / | \ \  
    0 1  3 4 5

    Input: n = 6, headID = 2, manager = [2,2,-1,2,2,2], informTime = [0,0,1,0,0,0]
    Output: 1
    Explanation: The head of the company with id = 2 is the direct manager of all the employees in the company and needs 1 minute to inform them all.
    The tree structure of the employees in the company is shown.

    Example 3:

    6 - 5 - 4 - 3 - 2 - 1 - 0

    Input: n = 7, headID = 6, manager = [1,2,3,4,5,6,-1], informTime = [0,6,5,4,3,2,1]
    Output: 21
    Explanation: The head has id = 6. He will inform employee with id = 5 in 1 minute.
    The employee with id = 5 will inform the employee with id = 4 in 2 minutes.
    The employee with id = 4 will inform the employee with id = 3 in 3 minutes.
    The employee with id = 3 will inform the employee with id = 2 in 4 minutes.
    The employee with id = 2 will inform the employee with id = 1 in 5 minutes.
    The employee with id = 1 will inform the employee with id = 0 in 6 minutes.
    Needed time = 1 + 2 + 3 + 4 + 5 + 6 = 21.


    Example 4:

    Input: n = 15, headID = 0, manager = [-1,0,0,1,1,2,2,3,3,4,4,5,5,6,6], informTime = [1,1,1,1,1,1,1,0,0,0,0,0,0,0,0]
    Output: 3
    Explanation: The first minute the head will inform employees 1 and 2.
    The second minute they will inform employees 3, 4, 5 and 6.
    The third minute they will inform the rest of employees.
    Example 5:

    Input: n = 4, headID = 2, manager = [3,3,-1,2], informTime = [0,0,162,914]
    Output: 1076
    */
    // BFS:
    public int numOfMinutes(int n, int headID, int[] manager, int[] informTime) {
        // build graph:
        // key -> parNode, val -> list of childNode
        Map<Integer, List<Integer>> childMap = new HashMap<>();
        for (int i = 0; i < n; i++) {
            if (i == headID) {
                continue;
            }
            int mgrId = manager[i];
            List<Integer> children = childMap.getOrDefault(mgrId, new ArrayList<>());
            children.add(i);
            childMap.put(mgrId, children);
        }
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{headID, 0});

        int maxSum = 0;
        while (!queue.isEmpty()) {
            int[] curr = queue.poll();
            int id = curr[0];
            int sum = curr[1];
            if (!childMap.containsKey(id)) {
                continue;
            }
            List<Integer> children = childMap.get(id);
            for (int child : children) {
                queue.offer(new int[]{child, sum + informTime[id]});
                maxSum = Math.max(maxSum, sum + informTime[id]);
            }
        }
        return maxSum;
    }