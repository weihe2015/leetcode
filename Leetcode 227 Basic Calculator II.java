
/*
Implement a basic calculator to evaluate a simple expression string.

The expression string contains only non-negative integers, +, -, *, / operators and empty spaces . The integer division should truncate toward zero.

You may assume that the given expression is always valid.

Some examples:
"3+2*2" = 7
" 3/2 " = 1
" 3+5 / 2 " = 5
*/
public class Solution {
    /**
     * Basic Idea, treat + and - as a part of the number: ex: 8 - 2 * 3 = 8 + (-2) * 3
     * 1. For prev sign is: + and -, use prev sign on the num, and add it into stack.
     * 2. For prev sign is: * and /, pop the last number out and make the calculation, add the result into stack in the end.
     * 3. If at the end, num != 0, handle the last number with prev sign
     * 4. Add all number in the stack into result.
    */
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<>();
        int num = 0;
        int sum = 0;
        char prevSign = '+';
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                continue;
            }
            else if (Character.isDigit(c)) {
                num = 10 * num + (c - '0');
            }
            else {
                handleOperators(stack, num, prevSign);
                // set current sign as previous sign:
                prevSign = c;
                // reset num:
                num = 0;
            }
        }
        // handle the last number:
        if (num != 0) {
            handleOperators(stack, num, prevSign);
        }
        // add all numbers in the stack:
        for (int n : stack) {
            sum += n;
        }
        return sum;
    }
    
    private void handleOperators(Stack<Integer> stack, int num, char prevSign) {
        // use previous sign to calculate the new num:
        if (prevSign == '+') {
            num = 1 * num;
        }
        else if (prevSign == '-') {
            num = -1 * num;
        }
        else if (prevSign == '*') {
            int prevNum = stack.pop();
            num = prevNum * num;
        }
        else if (prevSign == '/') {
            int prevNum = stack.pop();
            num = prevNum / num;
        }
        stack.push(num);
    }

    // Running Time Complexity: O(n), Space Complexity: O(1)
    // Basic Idea, for prevSign == '+' or '-', set prevNum = num and save prevNum into result
    // For prevSign == '*' or '/', substract preNum from result, calculate prevNum with current Num and add the prevNum back to result. 
    public int calculate(String s) {
        int n = s.length();
        if (n == 0) {
            return 0;
        }
        Set<Character> operatorSet = new HashSet<>(Arrays.asList('+', '-', '*', '/'));
        int res = 0;
        int num = 0;
        int prevNum = 0;
        char prevSign = '+';
        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) {
                num = 10 * num + (c - '0');
            }
            else if (operatorSet.contains(c)) {
                int[] arr = handleOperators(res, prevNum, num, prevSign);
                res = arr[0]; 
                prevNum = arr[1];
                prevSign = c;
                num = 0;
            }
        }
        if (num != 0) {
			int[] arr = handleOperators(res, prevNum, num, prevSign);
			res = arr[0]; 
            prevNum = arr[1];
        }
        return res;
    }

    private int[] handleOperators(int result, int prevNum, int num, char prevSign) {
		int[] arr = new int[2];
		if (prevSign == '+') {
			prevNum = num;
		}
		else if (prevSign == '-') {
			prevNum = -1 * num;
		}
		else if (prevSign == '*') {
			result -= prevNum;
			prevNum = prevNum * num;
		}
		else if (prevSign == '/') {
			result -= prevNum;
			prevNum = prevNum / num;
		}
		result += prevNum;
		arr[0] = result;
		arr[1] = prevNum;
		return arr;
	}
}