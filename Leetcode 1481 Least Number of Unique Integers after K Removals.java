/**
Given an array of integers arr and an integer k. Find the least number of unique integers after removing exactly k elements.

Example 1:

Input: arr = [5,5,4], k = 1
Output: 1
Explanation: Remove the single 4, only 5 is left.
Example 2:
Input: arr = [4,3,1,1,3,3,2], k = 3
Output: 2
Explanation: Remove 4, 2 and either one of the two 1s or three 3s. 1 and 3 will be left.
*/
class Solution {
    public int findLeastNumOfUniqueInts(int[] arr, int k) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        // occurance:
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : arr) {
            map.put(num, map.getOrDefault(num, 0)+1);
        }
        Queue<Integer> pq = new PriorityQueue<>(Comparator.comparing(map::get));
        pq.addAll(map.keySet());
        while (k > 0) {
            int num = pq.poll();
            k -= map.get(num);
        }
        if (k < 0) {
            return pq.size() + 1;
        }
        else {
            return pq.size();
        }
    }
}