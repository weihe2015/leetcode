/*
Given an unsorted integer array, find the first missing positive integer.

For example,
Given [1,2,0] return 3,
and [3,4,-1,1] return 2.

Your algorithm should run in O(n) time and uses constant space.
*/
public class Solution {
    // Naive Solution:
    // Running Time Complexity: O(n), Space Complexity: O(n)
    /**
    第一遍遍历数组把所有的数都存入HashSet中，并且找出数组的最大值，下次循环从1开始递增找数字，哪个数字找不到就返回哪个数字，如果一直找到了最大的数字，则返回最大值+1
    */
    public int firstMissingPositive(int[] nums) {
        Set<Integer> set = new HashSet<>();
        int max = -1;
        int res = 1;
        for (int num : nums) {
            if (num > 0) {
                max = Math.max(max, num);
                set.add(num);
            }
        }
        if (max == -1) {
            return res;
        }
        for (int i = 1; i <= max; i++) {
            if (!set.contains(i)) {
                return i;
            }
        }
        return max+1;
    }

    /***
    我们的思路是把1放在数组第一个位置nums[0]，2放在第二个位置nums[1]，
    即需要把nums[i]放在nums[nums[i] - 1]上，那么我们遍历整个数组，如果nums[i] != i + 1,
    而nums[i]为整数且不大于n，另外nums[i]不等于nums[nums[i] - 1]的话，
    我们将两者位置调换，如果不满足上述条件直接跳过，最后我们再遍历一遍数组，
    如果对应位置上的数不正确则返回正确的数
    */
    public int firstMissingPositive(int[] nums) {
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            while (nums[i] > 0 && nums[i] <= n && nums[i] != nums[nums[i]-1]) {
                swap(nums, i, nums[i]-1);
            }
        }
        for (int i = 0; i < n; i++) {
            if (nums[i] != i+1) {
                return i+1;
            }
        }
        return n+1;
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}