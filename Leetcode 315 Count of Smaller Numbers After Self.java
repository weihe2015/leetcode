/*
You are given an integer array nums and you have to return a new counts array. The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].

Example:

Input: [5,2,6,1]
Output: [2,1,1,0] 
Explanation:
To the right of 5 there are 2 smaller elements (2 and 1).
To the right of 2 there is only 1 smaller element (1).
To the right of 6 there is 1 smaller element (1).
To the right of 1 there is 0 smaller element.
*/

public class Solution {
    // Brute Force Solution:
    // Running Time Complexity: O(N^2)
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        for (int i = 0, n = nums.length; i < n; i++) {
            int count = 0;
            for (int j = i+1; j < n; j++) {
                if (nums[j] < nums[i]) {
                    count++;
                }
            }
            result.add(count);
        }
        return result;
    }

    // Solution 2: Scan from right to left,
    // Use Binary Search to find the proper position to insert the new item.
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> result = new ArrayList<Integer>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        int n = nums.length;
        List<Integer> sortedList = new ArrayList<Integer>();
        for (int i = n-1; i >= 0; i--) {
            int idx = findIndex(sortedList, nums[i]);
            result.add(idx);
            sortedList.add(idx, nums[i]);
        }
        Collections.reverse(result);
        return result;
    }
    // Find the first element that is greater than num:
    private int findIndex(List<Integer> sortedList, int num) {
        int low = 0, high = sortedList.size();
        while (low < high) {
            int mid = low + (high - low)/2;
            if (sortedList.get(mid) < num) {
                low = mid+1;
            }
            else {
                high = mid;
            }
        }
        return low;
    }

    // Solution 3: Use Binary Index Tree
    /**
     * Convert the question into: Prefix sum of frequencies
     *  [5,2,6,1] => 翻转数组： [1,6,2,5] => 找出之前比自己小的数的个数
     *    用prefix sum来取得这个个数
     * 1. sort 原数组 到一个新的数组里，保留原数组
     * 2. 计算每个元素的排名：
     *     就是历遍sorted的数组，把每个数的排名放在HashMap里
     * 3. 从右向左历遍原数组，
     *      对于每个元素，得到它相应的rank，然后计算rank-1的prefix sum
     *      更新这个rank by 增加1
     * Time Complexity: O(NlogN) NlogN for sorting, + N for creating rankMap, + N * logN for scanning the array
     *            from right to left, and tree.getSum & tree.update is logN
     * Space Complexity: O(N): Store extra array for ranking purpose.
     * 
    */
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> result = new ArrayList<Integer>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        int n = nums.length;
        // 先求出每个元素的rank, 重复的元素一样的rank
        int[] sortedArr = Arrays.copyOf(nums, n);
        Arrays.sort(sortedArr);
        // key -> num, value -> its rank
        // [5,2,6,1] => {5->3, 2->1, 1->1, 6->4}
        Map<Integer, Integer> rankMap = new HashMap<Integer, Integer>();
        int rank = 1;
        for (int num : sortedArr) {
            if (!rankMap.containsKey(num)) {
                rankMap.put(num, rank);  
                rank++;
            }
        }
        // Use number of unique items + 1 to build the Fenwick Tree 
        FenwickTree tree = new FenwickTree(rank);
        // Scan the array from right to left:
        for (int i = n-1; i >= 0; i--) {
            // 得到这个元素的rank
            rank = rankMap.get(nums[i]);
            // 找在这个元素的rank之前一共有多少个。
            int sum = tree.getSum(rank - 1);
            result.add(sum);
            // 更新这个数的rank by increment 1
            tree.update(rank, 1);
        }
        Collections.reverse(result);
        return result;
    }
    
    class FenwickTree {
        int[] BIT;
        int N;
        public FenwickTree(int N) {
            this.N = N;
            this.BIT = new int[N];
        }
        
        public int getSum(int idx) {
            // 这里不用idx = idx + 1 是因为：rankMap是从1开始的。
            int sum = 0;
            while (idx > 0) {
                sum += BIT[idx];
                idx = getParentIdx(idx);
            }
            return sum;
        }
        
        public void update(int idx, int value) {
            while (idx < N) {
                BIT[idx] += value;
                idx = getNextIdx(idx);
            }
        }
        
        public int getParentIdx(int idx) {
            return idx - (idx & (-idx));
        }
        
        public int getNextIdx(int idx) {
            return idx + (idx & (-idx));
        }
    }
}