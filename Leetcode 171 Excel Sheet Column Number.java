Given a column title as appear in an Excel sheet, return its corresponding column number.

For example:

    A -> 1
    B -> 2
    C -> 3
    ...
    Z -> 26
    AA -> 27
    AB -> 28 

public class Solution {
    public int titleToNumber(String s) {
        if(s.length() == 0 || s == null)
            return 0;
        int num = 0;
        for(char c : s.toCharArray()){
            num = num * 26 + 1 + (c - 'A');
        }
        return num;
    }
}