/**
You are given K eggs, and you have access to a building with N floors from 1 to N. 

Each egg is identical in function, and if an egg breaks, you cannot drop it again.

You know that there exists a floor F with 0 <= F <= N such that any egg dropped at a floor higher than F will break, and any egg dropped at or below floor F will not break.

Each move, you may take an egg (if you have an unbroken one) and drop it from any floor X (with 1 <= X <= N). 

Your goal is to know with certainty what the value of F is.

What is the minimum number of moves that you need to know with certainty what F is, regardless of the initial value of F?


Example 1:

Input: K = 1, N = 2
Output: 2
Explanation: 
Drop the egg from floor 1.  If it breaks, we know with certainty that F = 0.
Otherwise, drop the egg from floor 2.  If it breaks, we know with certainty that F = 1.
If it didn't break, then we know with certainty F = 2.
Hence, we needed 2 moves in the worst case to know what F is with certainty.
Example 2:

Input: K = 2, N = 6
Output: 3
Example 3:

Input: K = 3, N = 14
Output: 4

*/
public class Solution {
    // https://leetcode-cn.com/problems/super-egg-drop/solution/dong-tai-gui-hua-zhi-jie-shi-guan-fang-ti-jie-fang/
    // https://www.youtube.com/watch?v=iOaRjDT0vjc
    /**
    第 1 步：定义状态
dp[i][j]：一共有 i 层楼梯（注意：这里 i 不表示高度）的情况下，使用 j 个鸡蛋的最少实验的次数。

说明：

i 表示的是楼层的大小，不是高度（第几层）的意思，例如楼层区间 [8, 9, 10] 的大小为 33。
j 表示可以使用的鸡蛋的个数，它是约束条件。
第一个维度最先容易想到的是表示楼层的高度，这个定义的调整是在状态转移的过程中完成的。因为如果通过实验知道了鸡蛋的 F 值在高度区间 [8, 9, 10] 里，这个时候只有 1 枚鸡蛋，显然需要做 3 次实验，和区间的大小是相关的。

注意：这里我定义的维度顺序和官方解答的定义是反着的，我个人习惯将约束的那个条件，放置在后面的维度，表示消除后效性的意思。

第 2 步：推导状态转移方程
推导状态转移方程经常做的事情是「分类讨论」，这里「分类讨论」的依据就是，在指定的层数里扔下鸡蛋，根据这个鸡蛋是否破碎，就把问题拆分成了两个子问题。

设指定的楼层为 k，k >= 1 且 k <= i：

如果鸡蛋破碎，测试 F 值的实验就得在 k 层以下做（不包括 k 层），这里已经使用了一个鸡蛋，因此测出 F 值的最少实验次数是：dp[k - 1][j - 1]；
如果鸡蛋完好，测试 F 值的实验就得在 k 层以上做（不包括 k 层），这里这个鸡蛋还能使用，因此测出 F 值的最少实验次数是：dp[i - k][j]，例如总共 8 层，在第 5 层扔下去没有破碎，则需要在 [6, 7, 8] 层继续做实验，因此区间的大小就是 8 - 5 = 3。
最坏情况下，是这两个子问题的较大者，由于在第 k 层扔下鸡蛋算作一次实验，k 的值在 1 \le k \le i1≤k≤i，对于每一个 k 都对应了一组值的最大值，取这些 k 下的最小值（最优子结构），因此：

dp[i][j] = \min_{1 \le k \le i} \left(\max(dp[k - 1][j - 1], dp[i - k][j]) + 1 \right)
dp[i][j]= 
1≤k≤i
min
​	
 (max(dp[k−1][j−1],dp[i−k][j])+1)

解释：

由于仍那一个鸡蛋需要记录一次操作，所以末尾要加上 11；
每一个新值的计算，都参考了比它行数少，列数少的值，这些值一定是之前已经计算出来的，这样的过程就叫做「状态转移」。
这个问题只是状态转移方程稍显复杂，但空间换时间，逐层递推填表的思想依然是常见的动态规划的思路。

第 3 步：考虑初始化
一般而言，需要 0 这个状态的值，这里 0 层楼和 0 个鸡蛋是需要考虑进去的，它们的值会被后来的值所参考，并且也比较容易得到。

因此表格需要 N + 1 行，K + 1 列。

由于 F 值不会超过最大楼层的高度，要求的是最小值，因此初始化的时候，可以叫表格的单元格值设置成一个很大的数，但是这个数肯定也不会超过当前考虑的楼层的高度。

第 0 行：楼层为 0 的时候，不管鸡蛋个数多少，都测试不出鸡蛋的 F 值，故全为 0；
第 1 行：楼层为 1 的时候，00 个鸡蛋的时候，扔 0 次，1 个以及 1 个鸡蛋以上只需要扔 1 次；
第 0 列：鸡蛋个数为 0 的时候，不管楼层为多少，也测试不出鸡蛋的 F 值，故全为 00，虽然不符合题意，但是这个值有效，它在后面的计算中会被用到；
第 1 列：鸡蛋个数为 1 的时候，这是一种极端情况，要试出 F 值，最少次数就等于楼层高度；
第 4 步：考虑输出
输出就是表格的最后一个单元格的值 dp[N][K]。

第 5 步：思考状态压缩
看状态转移方程，当前单元格的值只依赖之前的行，当前列和它左边一列的值。可以状态压缩，让「列」滚动起来。但是「状态压缩」的代码增加了理解的难度，我们这里不做。

    */
    public int superEggDrop(int K, int N) {

        // dp[i][j]：一共有 i 层楼梯的情况下，使用 j 个鸡蛋的最少实验的次数
        // 注意：
        // 1、i 表示的是楼层的大小，不是第几层的意思，例如楼层区间 [8, 9, 10] 的大小为 3，这一点是在状态转移的过程中调整的定义
        // 2、j 表示可以使用的鸡蛋的个数，它是约束条件，我个人习惯放在后面的维度，表示消除后效性的意思

        // 0 个楼层和 0 个鸡蛋的情况都需要算上去，虽然没有实际的意义，但是作为递推的起点，被其它状态值所参考
        int[][] dp = new int[N + 1][K + 1];

        // 由于求的是最小值，因此初始化的时候赋值为一个较大的数，9999 或者 i 都可以
        for (int i = 0; i <= N; i++) {
            Arrays.fill(dp[i], i);
        }

        // 初始化：填写下标为 0、1 的行和下标为 0、1 的列
        // 第 0 行：楼层为 0 的时候，不管鸡蛋个数多少，都测试不出鸡蛋的 F 值，故全为 0
        for (int j = 0; j <= K; j++) {
            dp[0][j] = 0;
        }

        // 第 1 行：楼层为 1 的时候，0 个鸡蛋的时候，扔 0 次，1 个以及 1 个鸡蛋以上只需要扔 1 次
        dp[1][0] = 0;
        for (int j = 1; j <= K; j++) {
            dp[1][j] = 1;
        }

        // 第 0 列：鸡蛋个数为 0 的时候，不管楼层为多少，也测试不出鸡蛋的 F 值，故全为 0
        // 第 1 列：鸡蛋个数为 1 的时候，这是一种极端情况，要试出 F 值，最少次数就等于楼层高度（想想复杂度的定义）
        for (int i = 0; i <= N; i++) {
            dp[i][0] = 0;
            dp[i][1] = i;
        }

        // 从第 2 行，第 2 列开始填表
        for (int i = 2; i <= N; i++) {
            for (int j = 2; j <= K; j++) {
                for (int k = 1; k <= i; k++) {
                    // 碎了，就需要往低层继续扔：层数少 1 ，鸡蛋也少 1
                    // 不碎，就需要往高层继续扔：层数是当前层到最高层的距离差，鸡蛋数量不少
                    // 两种情况都做了一次尝试，所以加 1
                    dp[i][j] = Math.min(dp[i][j], Math.max(dp[k - 1][j - 1], dp[i - k][j]) + 1);
                }
            }
        }
        return dp[N][K];
    }

    public int superEggDrop(int K, int N) {
        int[][] dp = new int[N+1][K+1];
        // Base Case:
        // 只有一个鸡蛋, 有 N 层楼的话要尝试 N 次:
        // Worst amount of work have to do to guarentee that the number of moves is minimum.
        for (int j = 1; j <= N; j++) {
            dp[1][j] = j;
        }
    } 
}