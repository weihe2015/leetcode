/**
Given a string s which consists of lowercase or uppercase letters, return the length of the longest palindrome that can be built with those letters.

Letters are case sensitive, for example, "Aa" is not considered a palindrome here.

Example 1:

Input: s = "abccccdd"
Output: 7
Explanation:
One longest palindrome that can be built is "dccaccd", whose length is 7.
Example 2:

Input: s = "a"
Output: 1
Example 3:

Input: s = "bb"
Output: 2
*/
public class Solution {
    // Use hashset to count pairs. So palindrome can be count * 2 + 1 
    // if hashset is not empty
    // else: length = count * 2
    // O(n) time O(n) space
    public int longestPalindrome(String s) {
        HashSet<Character> hs = new HashSet<Character>();
        int count = 0;
        for (char c : s.toCharArray()) {
            if (hs.contains(c)) {
                hs.remove(c);
                count++;
            }
            else {
                hs.add(c);
            }

        }
        if (!hs.IsEmpty()) {
            return count * 2 + 1;
        }
        else {
            return count*2;
        }
    }
    
    // O(n) time O(1) space
    public int longestPalindrome(String s) {
        int n = s.length();
        int[] cList = new int[256];
        int count = 0;
        for (char c : s.toCharArray()) {
            if (cList[c] == 1) {
                cList[c]--;
                count++;
            }
            else {
                cList[c]++;
            }
        }
        if (count * 2 < n) {
            return count * 2 + 1;
        }
        else {
            return count * 2;
        }
    }
}