/**
Given an array of n positive integers and a positive integer s, find the minimal length of a subarray of which the sum ≥ s. 
If there isnt one, return 0 instead.

For example, given the array [2,3,1,2,4,3] and s = 7,
the subarray [4,3] has the minimal length under the problem constraint.

*/
public class Solution {
    /**
     * Two pointers. Time complexity: O(n), Space complexity: O(1)
     * Each element can be visited atmost twice, 
     *  once by the right pointer(ii) and (at most) once by the left pointer.
     * Increment sum by adding nums[i], set left pointer to the most left.
     *  shrink the subarray from left, by incrementing left point to the right,
     *   until the sum less than target.
     * */ 
    public int minSubArrayLen(int s, int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int l = 0; 
        int r = 0;
        int n = nums.length;
        int minLen = n+1;
        int sum = 0;
        // 右边界向右移动：
        while (r < n) {
            // 如果还没满足条件，移动右边界来扩大窗口：
            if (sum < s) {
                sum += nums[r];
                r++;
            }
            // 移动左指针，缩小左边界 l, 直到窗口不满足条件为止
            while (sum >= s) {
                minLen = Math.min(minLen, r-l);
                sum -= nums[l];
                l++;
            }
        }
        return minLen == n+1 ? 0 : minLen;
    }

    /**
     * My Solution on 6-24-18.
     * Time complexity O(n^2), Space complexity: O(1)
     * scan the array twice.
    */
    public int minSubArrayLen2(int s, int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0, max = nums.length; i < max; i++) {
            int sum = 0;
            for (int j = i; j < max; j++) {
                sum += nums[j];
                if (sum >= s) {
                    min = Math.min(min, j-i+1);
                }
            }
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }

    // Binary Search Solution:
    public int minSubArrayLen(int s, int[] nums) {
        // Binary Search version:
        int minLen = 0;
        if (nums == null || nums.length == 0) {
            return minLen;
        }
        int low = 0, high = nums.length-1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int val = maxSubArraySumWithLen(nums, mid);
            if (val >= s) {
                minLen = mid;
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        // Handle the case of nums = [1,2,3,4,5], s = 15
        if (minLen == 0) {
            int sum = 0;
            for (int num : nums) {
                sum += num;
            }
            if (sum >= s) {
                minLen = nums.length;
            }
        }
        return minLen;
    }
    
    private int maxSubArraySumWithLen(int[] nums, int len) {
        int maxVal = 0, sum = 0;
        // For subarray size with len, get its sum, which is the maxVal
        for (int i = 0; i < len; i++) {
            sum += nums[i];
        }
        maxVal = sum;
        // starting from idx i, get the subarray sum of array[i] to array[i+len], and update the maxVal
        for (int i = len, max = nums.length; i < max; i++) {
            sum += nums[i] - nums[i-len];
            maxVal = Math.max(maxVal, sum);
        }
        return maxVal;
    }
}