/**
Let's call an array A a mountain if the following properties hold:

A.length >= 3
There exists some 0 < i < A.length - 1 such that A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1]
Given an array that is definitely a mountain, return any i such that A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1].

Example 1:

Input: [0,1,0]
Output: 1
Example 2:

Input: [0,2,1,0]
Output: 1
Note:

3 <= A.length <= 10000
0 <= A[i] <= 10^6
A is a mountain, as defined above.
*/
class Solution {
    // Iterate the whole array:
    public int peakIndexInMountainArray(int[] nums) {
        for (int i = 1, max = nums.length; i < max-1; i++) {
            if (nums[i-1] < nums[i] && nums[i] > nums[i+1]) {
                return i;
            }
        }
        return -1;
    }

    // Binary Search:
    public int peakIndexInMountainArray(int[] nums) {
        // binary search:
        int low = 0, high = nums.length - 1;
        int res = -1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid-1] < nums[mid] && nums[mid] > nums[mid+1]) {
                res = mid;
                break;
            }
            else if (nums[mid] < nums[mid+1]) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return res;
    }
}
