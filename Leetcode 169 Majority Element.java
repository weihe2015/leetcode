/**
Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

You may assume that the array is non-empty and the majority element always exist in the array.

Example 1:

Input: [3,2,3]
Output: 3
Example 2:

Input: [2,2,1,1,1,2,2]
Output: 2
*/
public class Solution {
    // Brute force:
    // Use hashmap as counter:
    // Running Time Complexity: O(N), Space Complexity: O(N)
    public int majorityElement(int[] nums) {
        // key -> num, value -> freq
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        
        int n = nums.length;
        int res = -1;
        for (int key : map.keySet()) {
            int freq = map.get(key);
            if (freq > n/2) {
                return key;
            }
        }
        return -1;
    }

    // Linear voting algorithm Boyer–Moore majority vote algorithm
    /** 
    This method only works when there is majority element exists in the array.
    Running Time Complexity: O(N), Space Complexity: O(1)
    */
    public int majorityElement(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int majorElementIdx = 0, count = 0;
        for (int i = 0, n = nums.length; i < n; i++) {
            if (nums[i] == nums[majorElementIdx]) {
                count++;
            }
            else if (count == 0) {
                majorElementIdx = i;
                count = 1;
            }
            else {
                count--;
            }
        }
        return nums[majorElementIdx];
    }

    // Use sort, O(nlogn), half element
    public int majorityElement(int[] nums) {
        if(nums.length == 1)
            return nums[0];
        Arrays.sort(nums);
        return nums[nums.length/2];
    }
}