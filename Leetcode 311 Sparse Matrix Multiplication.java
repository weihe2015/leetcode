/**
Given two sparse matrices A and B, return the result of AB.

You may assume that A's column number is equal to B's row number.

Example:

A = [
  [ 1, 0, 0],
  [-1, 0, 3]
]

B = [
  [ 7, 0, 0 ],
  [ 0, 0, 0 ],
  [ 0, 0, 1 ]
]


     |  1 0 0 |   | 7 0 0 |   |  7 0 0 |
AB = | -1 0 3 | x | 0 0 0 | = | -7 0 3 |
                  | 0 0 1 |
*/
public class Solution {
    // Brute Force Solution:
    public int[][] multiply(int[][] A, int[][] B) {
        // Matrix multiplication, A[0].length should == B.length
        // If A = m1 * n1, B = m2 * n2, the result should be m1 * n2, while n1 == m2;
        int m1 = A.length;
        int n1 = A[0].length;
        int m2 = B.length;
        int n2 = B[0].length;
        int[][] res = new int[m1][n2];
        
        for (int i = 0; i < m1; i++) {
            for (int j = 0; j < n2; j++) {
                int sum = 0;
                for (int k = 0; k < n1; k++) {
                    sum += A[i][k] * B[k][j];
                }
                res[i][j] = sum;
            }
        }
        return res;
    }

    // Better Solution:
    /** 
     * 我们知道一个 i x k 的矩阵A乘以一个 k x j 的矩阵B会得到一个 i x j 大小的矩阵C，
     * 那么我们来看结果矩阵中的某个元素C[i][j]是怎么来的，起始是A[i][0]*B[0][j] + A[i][1]*B[1][j] + ... + A[i][k]*B[k][j]，
     * 那么为了不重复计算0乘0，我们首先遍历A数组，要确保A[i][k]不为0，
     * 才继续计算，然后我们遍历B矩阵的第k行，如果B[k][j]不为0，
     * 我们累加结果矩阵res[i][j] += A[i][k] * B[k][j]; 这样我们就能高效的算出稀疏矩阵的乘法
    */
    public int[][] multiply(int[][] A, int[][] B) {
        // Matrix multiplication, Matrix A: i x k, Matrix B: k x j, Result Matrix: i x j
        // res[i][j] = A[i][0] * B[0][j] + A[i][1] * B[1][j] + ... + A[i][k] * B[k][j]
        // m1 * n1 x m2 * n2 where n1 == m2
        int m1 = A.length;
        int n1 = A[0].length;
        int m2 = B.length;
        int n2 = B[0].length;
        int[][] res = new int[m1][n2];
        
        for (int i = 0; i < m1; i++) {
            for (int k = 0; k < n1; k++) {
                if (A[i][k] == 0) {
                    continue;
                }
                for (int j = 0; j < n2; j++) {
                    if (B[k][j] == 0) {
                        continue;
                    }
                    res[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        return res;
    }

    // Convert Sparse Matrix into Dense Matrix:
    public int[][] multiply(int[][] A, int[][] B) {
        // Matrix multiplication, Matrix A: i x k, Matrix B: k x j
        // res[i][j] = A[i][0] * B[0][j] + A[i][1] * B[1][j] + ... + A[i][k] * B[k][j]
        // m1 * n1 x m2 * n2 where n1 == m2
        int m1 = A.length;
        int n1 = A[0].length;
        int m2 = B.length;
        int n2 = B[0].length;
        int[][] res = new int[m1][n2];
        
        // Convert Sparse Matrix A and B into Dense Matrix
        // key -> row idx, val: (key -> col idx, val -> matrix[i][j])
        Map<Integer, Map<Integer, Integer>> denseA = createDenseMap(A);
        Map<Integer, Map<Integer, Integer>> denseB = createDenseMap(B);
    
        // Calculate denseA * denseB:
        for (int i : denseA.keySet()) {
            // Matrix A 的每一column对应着Matrix B的每一Row
            for (int k : denseA.get(i).keySet()) {
                // 如果DenseB matrix有col的key, 说明Matrix B的row=i 有值,
                if (denseB.containsKey(k)) {
                    // 对于denseB的每一个subMap来说，j = 值所在的col，所以res[i][j] += denseA.get(i).get(k) * denseB.get(k).get(j);
                    for (int j : denseB.get(k).keySet()) {
                        res[i][j] += denseA.get(i).get(k) * denseB.get(k).get(j);
                    }
                }
            }
        }
        return res;
    }

    private Map<Integer, Map<Integer, Integer>> createDenseMap(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        Map<Integer, Map<Integer, Integer>> denseMap = new HashMap<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    continue;
                }
                Map<Integer, Integer> map = denseMap.getOrDefault(i, new HashMap<>());
                map.put(j, matrix[i][j]);
                denseMap.put(i, map);
            }
        }
        return denseMap;
    }
    
}