/**
Given a complete binary tree, count the number of nodes.

Note:

Definition of a complete binary tree from Wikipedia:
In a complete binary tree every level, except possibly the last,
is completely filled, and all nodes in the last level are as far left as possible.
It can have between 1 and 2h nodes inclusive at the last level h.

Example:

Input:
    1
   / \
  2   3
 / \  /
4  5 6

Output: 6
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // recursion
    public int countNodes(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftH = 0, rightH = 0;
        TreeNode leftNode = root;
        while (leftNode != null) {
            leftH++;
            leftNode = leftNode.left;
        }

        TreeNode rightNode = root;
        while (rightNode != null) {
            rightH++;
            rightNode = rightNode.right;
        }

        if (leftH == rightH) {
            return (int) Math.pow(2, leftH) - 1;
        }
        return 1 + countNodes(root.left) + countNodes(root.right);
    }

    // Solution 2:
    public int countNodes(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.val != Integer.MIN_VALUE) {
            root.val = Integer.MIN_VALUE;
            return 1 + countNodes(root.left) + countNodes(root.right);
        }
        else {
            return 0;
        }
    }

    // Solution 3:
    public int countNodes(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int cnt = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            cnt += size;
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
        }
        return cnt;
    }
}