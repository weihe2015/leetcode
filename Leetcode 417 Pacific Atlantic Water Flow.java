/**
Given an m x n matrix of non-negative integers representing the height of each unit cell in a continent, the "Pacific ocean" touches the left and top edges of the matrix and the "Atlantic ocean" touches the right and bottom edges.

Water can only flow in four directions (up, down, left, or right) from a cell to another one with height equal or lower.

Find the list of grid coordinates where water can flow to both the Pacific and Atlantic ocean.

Note:
The order of returned grid coordinates does not matter.
Both m and n are less than 150.
Example:

Given the following 5x5 matrix:

  Pacific ~   ~   ~   ~   ~ 
       ~  1   2   2   3  (5) *
       ~  3   2   3  (4) (4) *
       ~  2   4  (5)  3   1  *
       ~ (6) (7)  1   4   5  *
       ~ (5)  1   1   2   4  *
          *   *   *   *   * Atlantic

Return:

[[0, 4], [1, 3], [1, 4], [2, 2], [3, 0], [3, 1], [4, 0]] (positions with parentheses in above matrix).
*/
public class Solution {
    private static final int[][] dirs = {{0,1}, {0,-1}, {1,0}, {-1,0}};
    public List<int[]> pacificAtlantic(int[][] matrix) {
        List<int[]> result = new ArrayList<int[]>();
        if (matrix == null || matrix.length == 0) {
            return result;
        }
        int m = matrix.length, n = matrix[0].length;
        // find all the points that can flows to pacific:
        boolean[][] pacificMap = new boolean[m][n];
        boolean[][] atlanticMap = new boolean[m][n];
        
        // scan the first and last row, from first column and last column:
        for (int j = 0; j < n; j++) {
            dfs(matrix, 0, j, m, n, pacificMap, 0);
            dfs(matrix, m-1, j, m, n, atlanticMap, 0);
        }
        
        // scan the first column and last column, from first row to last row:
        for (int i = 0; i < m; i++) {
            dfs(matrix, i, 0, m, n, pacificMap, 0);
            dfs(matrix, i, n-1, m, n, atlanticMap, 0);
        }
        
        // find out which cells can go to both pacific and atlantic:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (pacificMap[i][j] && atlanticMap[i][j]) {
                    result.add(new int[]{i, j});
                }
            }
        }
        return result;
    }
    
    private void dfs(int[][] matrix, int i, int j, int m, int n, boolean[][] map, int prev) {
        if (i < 0 || i >= m || j < 0 || j >= n || map[i][j] || matrix[i][j] < prev) {
            return;
        }
        map[i][j] = true;
        // scan four directions:
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            dfs(matrix, x, y, m, n, map, matrix[i][j]);
        }
    }
}