/**
Given a string, your task is to count how many palindromic substrings in this string.

The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.

Example 1:

Input: "abc"
Output: 3
Explanation: Three palindromic strings: "a", "b", "c".


Example 2:

Input: "aaa"
Output: 6
Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".
*/
class Solution {
    public int countSubstrings(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int res = 0;
        for (int i = 0, max = s.length(); i < max; i++) {
            res += countPalindrome(s, i, i, max);
            res += countPalindrome(s, i, i+1, max);
        }
        return res;
    }

    private int countPalindrome(String s, int i, int j, int max) {
        int count = 0;
        while (i >= 0 && j < max && s.charAt(i) == s.charAt(j)) {
            i--;
            j++;
            count++;
        }
        return count;
    }

    // Get only unique strings:
    public int countSubstrings(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        Set<String> set = new HashSet<>();
        for (int i = 0, max = s.length(); i < max; i++) {
            countPalindrome(set, s, i, i, max);
            countPalindrome(set, s, i, i+1, max);
        }
        return set.size();
    }

    private void countPalindrome(Set<String> set, String s, int i, int j, int max) {
        while (i >= 0 && j < max && s.charAt(i) == s.charAt(j)) {
            set.add(s.substring(i, j+1));
            i--;
            j++;
        }
    }
}
