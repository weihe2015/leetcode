/*
Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, 
find the one that is missing from the array.

For example,
Given nums = [0, 1, 3] return 2.
*/
public class Solution {
    public int missingNumber(int[] nums) {
        int sum = 0;
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            sum += nums[i];
        }
        int res = n * (n + 1) / 2 - sum;
        return res;
    }

    // xor
    // Use XOR, 2 ^ 2 = 0;
    // If we xor all num and its index, the finally result will be the missing number.
    public int missingNumber(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int res = 0, n = nums.length;
        for (int i = 0; i < n; i++) {
            res ^= i;
            res ^= nums[i];
        }
        res ^= n;
        return res;
    }

	// binary search
    public int missingNumber(int[] nums) {
        Arrays.sort(nums);
        int l = 0, r = nums.length;
        while (l < r) {
            int m = l + (r-l) / 2;
            if (nums[m] > m) {
                r = m;
            } 
            else {
                l = m + 1;
            } 
        }
        return l;
    }
}