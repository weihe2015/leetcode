/**
Given a binary tree, return the level order traversal of its nodes values. (ie, from left to right, level by level).

For example:
Given binary tree {3,9,20,#,#,15,7},
    3
   / \
  9  20
    /  \
   15   7
return its level order traversal as:
[
  [3],
  [9,20],
  [15,7]
]
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive:
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrder(result, root, 0);
        return result;
    }
    
    private void levelOrder(List<List<Integer>> result, TreeNode node, int level) {
        if (node == null) {
            return;
        }
        // if result.get(level) == null
        if (result.size() == level) {
            result.add(new ArrayList<Integer>());
        }
        List<Integer> list = result.get(level);
        list.add(node.val);
        levelOrder(result, node.left, level+1);
        levelOrder(result, node.right, level+1);
    }
    // Iterative:
    /**
     * Basic Idea: 
     *    1. use queue to store all nodes in the same level.
     *    2. Iterate all nodes in the level with size = queue.size() times.
     */
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> list = new ArrayList<>();
            int level = queue.size();
            for (int i = 1; i <= level; i++) {
                TreeNode curr = queue.poll();
                list.add(curr.val);
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
            res.add(list);
        }
        return res;
    }
}