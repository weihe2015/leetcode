
/**
Given an array of characters, compress it in-place.

The length after compression must always be smaller than or equal to the original array.

Every element of the array should be a character (not int) of length 1.

After you are done modifying the input array in-place, return the new length of the array.


Follow up:
Could you solve it using only O(1) extra space?


Example 1:
Input:
["a","a","b","b","c","c","c"]

Output:
Return 6, and the first 6 characters of the input array should be: ["a","2","b","2","c","3"]

Explanation:
"aa" is replaced by "a2". "bb" is replaced by "b2". "ccc" is replaced by "c3".
Example 2:
Input:
["a"]

Output:
Return 1, and the first 1 characters of the input array should be: ["a"]

Explanation:
Nothing is replaced.
Example 3:
Input:
["a","b","b","b","b","b","b","b","b","b","b","b","b"]

Output:
Return 4, and the first 4 characters of the input array should be: ["a","b","1","2"].

Explanation:
Since the character "a" does not repeat, it is not compressed. "bbbbbbbbbbbb" is replaced by "b12".
Notice each digit has it's own entry in the array.
Note:
All characters have an ASCII value in [35, 126].
1 <= len(chars) <= 1000.
*/
public class Solution {
    // Running Time Complexity: O(N), Space Complexity O(1)
    public int compress(char[] chars) {
        int l = 0;
        int r = 0;
        int n = chars.length;
        while (r < n) {
            char curr = chars[r];
            int count = 1;
            r++;
            while (r < n && chars[r] == curr) {
                count++;
                r++;
            }
            chars[l] = curr;
            l++;
            if (count > 1) {
                String countText = String.valueOf(count);
                for (char c : countText.toCharArray()) {
                    chars[l] = c;
                    l++;
                }
            }
        }
        return l;
    }

    public int compress(char[] chars) {
        if (chars == null || chars.length <= 1) {
            return chars.length;
        }
        int l = 0, r = 1, n = chars.length;
        int count = 1;
        char c = chars[l];
        while (r < n) {
            if (chars[r] == c) {
                r++;
                count++;
            }
            else {
                chars[l] = c;
                l++;
                if (count > 1) {
                    String countStr = String.valueOf(count);
                    for (int i = 0, m = countStr.length(); i < m; i++) {
                        chars[l] = countStr.charAt(i);
                        l++;
                    }
                    count = 1;
                }
                c = chars[r];
                r++;
            }
        }
        // last character:
        if (count >= 1) {
            chars[l] = c;
        	l++;
            if (count > 1) {
                String countStr = String.valueOf(count);
                for (int i = 0, m = countStr.length(); i < m; i++) {
                    chars[l] = countStr.charAt(i);
                    l++;
                }
            }
        }
        return l;
    }

    /**  Compress string and return string:
       Ex: a => a
           aa => a2
           abbbcc => ab3c2
    */   
    public String compress(String s) {
        StringBuffer sb = new StringBuffer();
        char[] cList = s.toCharArray();
        int r = 0;
        int n = cList.length;
        while (r < n) {
            char currChar = cList[r];
            r++;
            int cnt = 1;
            while (r < n && cList[r] == currChar) {
                cnt++;
                r++;
            }
            sb.append(currChar);
            if (cnt > 1) {
                sb.append(cnt);
            }
        }
        return sb.toString();
    }

    public String uncompress(String s) {
        StringBuffer sb = new StringBuffer();
        int r = 0;
        int n = s.length();
        while (r < n) {
            char c = s.charAt(r);
            r++;
            if (!Character.isLetter(c)) {
                continue;
            }
            sb.append(c);
            int cnt = 0;
            while (r < n && Character.isDigit(s.charAt(r))) {
                cnt = cnt * 10 + (s.charAt(r) - '0');
                r++;
            }
            if (cnt > 1) {
                for (int j = 1; j < cnt; j++) {
                    sb.append(c);
                }
            }
            
        }
        return sb.toString();
    }
}