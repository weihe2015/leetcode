/**
In an infinite chess board with coordinates from -infinity to +infinity, you have a knight at square [0, 0].

A knight has 8 possible moves it can make, as illustrated below. Each move is two squares in a cardinal direction, then one square in an orthogonal direction.

Return the minimum number of steps needed to move the knight to the square [x, y].  It is guaranteed the answer exists.

Example 1:

Input: x = 2, y = 1
Output: 1
Explanation: [0, 0] → [2, 1]
Example 2:

Input: x = 5, y = 5
Output: 4
Explanation: [0, 0] → [2, 1] → [4, 2] → [3, 4] → [5, 5]
 

Constraints:
|x| + |y| <= 300
*/
public class Solution {

    static private final int[][] dirs = {{1,2},{-1,2},{1,-2},{-1,-2},{2,1},{-2,1},{2,-1},{-2,-1}};
    // BFS:
    public int minKnightMoves(int x, int y) {
        x = Math.abs(x);
        y = Math.abs(y);

        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{0,0});
        Set<String> visited = new HashSet<>();
        visited.add("0,0");
        int step = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                int[] curr = queue.poll();
                if (curr[0] == x && curr[1] == y) {
                    return step;
                }
                int i = curr[0];
                int j = curr[1];
                for (int[] dir : dirs) {
                    int x = i + dir[0];
                    int y = j + dir[1];

                    String posText = x + "," + y;
                    if (Math.abs(x) + Math.abs(y) > 300 || visited.contains(posText)) {
                        continue;
                    }
                    queue.offer(new int[]{x, y});
                    visited.add(posText);
                }
            }
            step++;
        }
        return step;
    }

    // DFS:
    private Map<String, Integer> map;
    public int minKnightMoves(int x, int y) {
        this.map = new HashMap<>();
        x = Math.abs(x);
        y = Math.abs(y);

        return dfs(x, y);
    }

    private int dfs(int x, int y) {
        // (0,0)
        if (x + y == 0) {
            return 0;
        }
        // (0,2), (1,1), (2,0);
        if (x + y == 2) {
            return 2;
        }
        String key = x + "," + y;
        if (map.containsKey(key)) {
            return map.get(key);
        }

        int val1 = dfs(Math.abs(x-2), Math.abs(y-1));
        int val2 = dfs(Math.abs(x-1), Math.abs(y-2));
        int minMove = Math.min(val1, val2) + 1;

        map.put(key, minMove);
        return minMove;
    }
}