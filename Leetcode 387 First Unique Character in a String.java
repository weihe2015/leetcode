/**
Given a string, find the first non-repeating character in it and return its index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode"
return 2.
 
Note: You may assume the string contains only lowercase English letters.
*/

public class Solution {

    // Time Complexity: O(N), Space Complexity: O(N)
    public int firstUniqChar(String s) {
        Map<Character, Integer> freqMap = new HashMap<>();
        for (char c : s.toCharArray()) {
            int freq = freqMap.getOrDefault(c, 0) + 1;
            freqMap.put(c, freq);
        }
        
        for (int i = 0, n = s.length(); i < n; i++) {
            char c = s.charAt(i);
            int freq = freqMap.get(c);
            if (freq == 1) {
                return i;
            }
        }
        return -1;
    }

    public int firstUniqChar(String s) {
        int[] freqMap = new int[256];
        for (char c : s.toCharArray()) {
            freqMap[c]++;
        }
        
        for (int i = 0, n = s.length(); i < n; i++) {
            char c = s.charAt(i);
            int freq = freqMap[c];
            if (freq == 1) {
                return i;
            }
        }
        return -1;
    }
}

public class FirstUniqueCharInStream {

    class ListNode {
        ListNode prev;
        ListNode next;
        char symbol;

        public ListNode() {}

        public ListNode(char symbol) {
            this.symbol = symbol;
        }
    }

    private Character findFirstUniqueChar(String s) {

        ListNode head = new ListNode();
        ListNode tail = new ListNode();

        head.next = tail;
        tail.prev = head;

        // indicate whether this character is repeated or not
        boolean[] repeatedMap = new boolean[26];
        ListNode[] hashArray = new ListNode[26];

        for (char c : s.toCharArray()) {
            // this character has been repeated
            if (repeatedMap[c-'a']) {
                continue;
            }
            // this character has been appeared previously,
            else if (hashArray[c-'a'] != null) {
                repeatedMap[c-'a'] = true;
                // remove it from head:
                ListNode node = hashArray[c-'a'];
                unLinkNode(node);
            }
            // new unique character has been introduced:
            else {
                ListNode node = new ListNode(c);
                appendNode(node, tail);
                hashArray[c-'a'] = node;
            }
            if (head.next == tail) {
                System.out.println("current char: " + c + ", DO not exists fst unique char.");
            }
            else {
                System.out.println("current char: " + c + ", First unique char: " + head.next.symbol);
            }

        }
        if (head.next == tail) {
            return null;
        }
        return head.next.symbol;
    }

    private void unLinkNode(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;

        node.prev = null;
        node.next = null;
    }

    private void appendNode(ListNode node, ListNode tail) {
        ListNode prevNode = tail.prev;
        tail.prev = node;
        node.next = tail;

        prevNode.next = node;
        node.prev = prevNode;
    }

    @Test
    public void test1() {
        String s = "abcbbac";
        Character res = findFirstUniqueChar(s);
        assertNull(res);

        s = "loveleetcode";
        res = findFirstUniqueChar(s);
        assertNotNull(res);
    }
}