/*
Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.

An example is the root-to-leaf path 1->2->3 which represents the number 123.

Find the total sum of all root-to-leaf numbers.

For example,

    1
   / \
  2   3
The root-to-leaf path 1->2 represents the number 12.
The root-to-leaf path 1->3 represents the number 13.

Return the sum = 12 + 13 = 25.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive
    public int sumNumbers(TreeNode root) {
        return sumNumbers(root, 0);
    }

    private int sumNumbers(TreeNode root, int sum) {
        if (root == null) {
            return 0;
        }
        int total = sum * 10 + root.val;
        if (root.left == null && root.right == null) {
            return total;
        }
        return sumNumbers(root.left, total) + sumNumbers(root.right, total);
    }

    // Recursive
    public int sumNumbers(TreeNode root) {
        List<Integer> paths = new ArrayList<Integer>();
        sumNumbers(paths, root, 0);
        int sum = 0;
        for (int num : paths) {
            sum += num;
        }
        return sum;
    }

    private void sumNumbers(List<Integer> paths, TreeNode node, int num) {
        if (node == null) {
            return;
        }
        num = num * 10 + node.val;
        if (node.left == null && node.right == null) {
            paths.add(num);
            return;
        }
        sumNumbers(paths, node.left, num);
        sumNumbers(paths, node.right, num);
    }

    // Iterative, Level Order Traversal
    public int sumNumbers(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int sum = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        Queue<Integer> sumQueue = new LinkedList<>();

        queue.offer(root);
        sumQueue.offer(0);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                int num = sumQueue.poll();
                num = 10 * num + curr.val;
                if (curr.left == null && curr.right == null) {
                    sum += num;
                }
                if (curr.left != null) {
                    queue.offer(curr.left);
                    sumQueue.offer(num);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                    sumQueue.offer(num);
                }
            }
        }
        return sum;
    }

    // Bruth Force:
    public int sumNumbers(TreeNode root) {
        List<List<Integer>> paths = new ArrayList<>();
        getAllPaths(paths, root, new ArrayList<Integer>());
        int sum = 0;
        for (List<Integer> list : paths) {
            int num = convertListToNumber(list);
            sum += num;
        }
        return sum;
    }

    private void getAllPaths(List<List<Integer>> paths, TreeNode node, List<Integer> list) {
        if (node == null) {
            return;
        }

        if (node.left == null && node.right == null) {
            list.add(node.val);
            paths.add(new ArrayList<Integer>(list));
            list.remove(list.size()-1);
            return;
        }
        list.add(node.val);
        getAllPaths(paths, node.left, list);
        getAllPaths(paths, node.right, list);
        list.remove(list.size()-1);
    }

    private int convertListToNumber(List<Integer> list) {
        if (list == null || list.isEmpty()) {
            return 0;
        }
        int sum = 0, n = list.size() - 1;
        for (int num : list) {
            sum += num * Math.pow(10, n);
            --n;
        }
        return sum;
    }
}