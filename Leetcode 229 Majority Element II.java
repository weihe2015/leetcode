/** 
Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.

Note: The algorithm should run in linear time and in O(1) space.

Example 1:

Input: [3,2,3]
Output: [3]
Example 2:

Input: [1,1,1,3,3,2,2,2]
Output: [1,2]
*/
public class Solution {
    // Brute Force Solution:
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public List<Integer> majorityElement(int[] nums) {
        List<Integer> res = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return res;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        int n = nums.length;
        for (int key : map.keySet()) {
            int freq = map.get(key);
            if (freq > n/3) {
                res.add(key);
            }
        }
        return res;
    }

    public List<Integer> majorityElement(int[] nums) {
        List<Integer> res = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return res;
        }
        // At most two candidates whose count will greater than n/3
        // Pick up two candidates out, A and B:
        int majorA = 0, majorB = 0, countA = 0, countB = 0;
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            // Select A:
            if (nums[i] == nums[majorA]) {
                countA++;
            }
            // Select B:
            else if (nums[i] == nums[majorB]) {
                countB++;
            }
            // If candidate A's count is == 0, we switch the candidate
            else if (countA == 0) {
                majorA = i;
                countA = 1;
            }
            // If candidate B's count is == 0, we switch the candidate
            else if (countB == 0) {
                majorB = i;
                countB = 1;
            }
            // At this point, both countA and countB are not zero, and decrement both countA and countB
            // because currently no one is selecting candidate A and B
            // If we do not decrement countA and countB, new candidate cannot showup.
            else {
                countA--;
                countB--;
            }
        }
        // currently majorA and majorB are the candidates with the most selection.
        // reset countA and countB, iterate the array again to confirm the count.
        countA = 0; 
        countB = 0;
        for (int num : nums) {
            if (num == nums[majorA]) {
                countA++;
            }
            else if (num == nums[majorB]) {
                countB++;
            }
        }
        if (countA > n/3) {
            res.add(nums[majorA]);
        }
        if (countB > n/3) {
            res.add(nums[majorB]);
        }
        return res;
    }
}