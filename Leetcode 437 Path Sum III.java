/**
 * You are given a binary tree in which each node contains an integer value.

Find the number of paths that sum to a given value.

The path does not need to start or end at the root or a leaf, but it must go downwards (traveling only from parent nodes to child nodes).

The tree has no more than 1,000 nodes and the values are in the range -1,000,000 to 1,000,000.

Example:

root = [10,5,-3,3,2,null,11,3,-2,null,1], sum = 8

      10
     /  \
    5   -3
   / \    \
  3   2   11
 / \   \
3  -2   1

Return 3. The paths that sum to 8 are:

1.  5 -> 3
2.  5 -> 2 -> 1
3. -3 -> 11
 * 
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive: Running Time: O(NlogN), T(N) = O(N) + 2T(N/2) 
    public int pathSum(TreeNode root, int sum) {
        if (root == null) {
            return 0;
        }
        return pathSumFromNode(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
    }
    
    private int pathSumFromNode(TreeNode node, int sum) {
        if (node == null) {
            return 0;
        }
        int diff = sum - node.val;
        if (diff == 0) {
            return 1 + pathSumFromNode(node.left, diff) + pathSumFromNode(node.right, diff);
        }
        else {
            return pathSumFromNode(node.left, diff) + pathSumFromNode(node.right, diff);
        }
    }

    // Recursive: Running Time: O(n)
    public int pathSum(TreeNode root, int sum) {
        int count = 0;
        if (root == null) {
            return count;
        }
        // key -> sum to this node, value: number of occurrence
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        count = pathSum(root, 0, sum, map);
        return count;
    }
    
    private int pathSum(TreeNode node, int currSum, int sum, Map<Integer, Integer> map) {
        if (node == null) {
            return 0;
        }
        int count = 0;
        currSum += node.val;
        
        int sum_j = currSum - sum;
        // handle path from root:
        if (sum_j == 0) {
            count++;
        }
        
        if (map.containsKey(sum_j)) {
            count += map.get(sum_j);
        }
        
        // Set currSum and its value to 1 into map if it is not existed,
        if (!map.containsKey(currSum)) {
            map.put(currSum, 1);
        }
        else {
            int prevCount = map.get(currSum);
            map.put(currSum, prevCount+1);
        }
        
        count += pathSum(node.left, currSum, sum, map) + pathSum(node.right, currSum, sum, map);
        // Reset the occurence time by reducing 1, when returning from children
        int prevCount = map.get(currSum);
        map.put(currSum, prevCount-1);
        return count;
    }
}