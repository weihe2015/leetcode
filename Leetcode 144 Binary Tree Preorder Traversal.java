
/**
 * Given a binary tree, return the preorder traversal of its nodes' values.

Example:

Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,2,3]
 * 
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive:
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        preorderTraversal(result, root);
        return result;
    }
    
    private void preorderTraversal(List<Integer> result, TreeNode node) {
        if (node == null) {
            return;
        }
        result.add(node.val);
        preorderTraversal(result, node.left);
        preorderTraversal(result, node.right);
    }

    // solution 2
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        if (root == null) {
            return result;
        }

        // Divide
        List<Integer> left = preorderTraversal(root.left);
        List<Integer> right = preorderTraversal(root.right);
        
        // Conquer
        result.add(root.val);
        result.addAll(left);
        result.addAll(right);
        return result;
    }
    // Iterative:
    /**
     * Preorder Traversal: currNode, left child, right child.
     * Basic Idea, Use stack to add root.
     *    while stack is not empty, pop the TreeNode as currNode, add its value into list;
     *    Add its right node to the stack, then add left node to the stack, because we wants to iterate left side first
     * 
    */
    // Note: we cannot use queue because we need to let left nodes to exhaust first.
    // Stack has first in last out property
    /**
           1
        4    3
      2
      Queue: [1,4,3,2]
      Stack: [1,4,2,3]
    */
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            result.add(curr.val);
            if (curr.right != null) {
                stack.push(curr.right);
            }
            if (curr.left != null) {
                stack.push(curr.left);
            }
        }
        return result;
    }

}