/**
We are given a binary tree (with root node root), a target node, and an integer value K.

Return a list of the values of all nodes that have a distance K from the target node.  The answer can be returned in any order.

Example 1:

Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, K = 2

            3
       5          1
 6     2        0    8
     7   4  


Output: [7,4,1]

Explanation: 
The nodes that are a distance 2 from the target node (with value 5)
have values 7, 4, and 1.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    // Solution 1: Build Graph and perform BFS on it:
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        
        Map<TreeNode, List<TreeNode>> graph = new HashMap<>();
        buildGraph(graph, root);
        
        // BFS:
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(target);
        
        Set<TreeNode> visited = new HashSet<>();
        visited.add(target);
        
        int step = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int s = 1; s <= size; s++) {
                TreeNode curr = queue.poll();
                if (step == K) {
                    res.add(curr.val);
                }
                if (!graph.containsKey(curr)) {
                    continue;
                }
                List<TreeNode> neighbors = graph.get(curr);
                for (TreeNode neighbor : neighbors) {
                    if (visited.contains(neighbor)) {
                        continue;
                    }
                    visited.add(neighbor);
                    queue.offer(neighbor);
                }
            }
            step++;
            if (step > K) {
                break;
            }
        }
        return res;
    }
    
    private void buildGraph(Map<TreeNode, List<TreeNode>> graph, TreeNode node) {
        if (node == null) {
            return;
        }

        if (node.left != null) {
            addNode(graph, node, node.left);
            addNode(graph, node.left, node);
            buildGraph(graph, node.left);
        }
        
        if (node.right != null) {
            addNode(graph, node, node.right);
            addNode(graph, node.right, node);
            buildGraph(graph, node.right);
        }
    }
    
    private void addNode(Map<TreeNode, List<TreeNode>> graph, TreeNode key, TreeNode child) {
        List<TreeNode> list = graph.getOrDefault(key, new ArrayList<>());
        list.add(child);
        graph.put(key, list);
    }

    // Solution 2, use recursion
    // https://www.cnblogs.com/grandyang/p/10686922.html
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        if (K == 0) {
            res.add(target.val);
        }
        else {
            traverse(root, target, 0, K, res);
        }
        return res;
    }
    
    private int traverse(TreeNode root, TreeNode target, int dist, int K, List<Integer> res) {
        if (root == null) {
            return 0;
        }
        if (dist == K) {
            res.add(root.val);
            return 0;
        }
        
        int left = 0;
        int right = 0;
        
        // traversing down: when found the target ever
        if (root == target || dist > 0) {
            left = traverse(root.left, target, dist+1, K, res);
            right = traverse(root.right, target, dist+1, K, res);
        }
        else {
            left = traverse(root.left, target, dist, K, res);
            right = traverse(root.right, target, dist, K, res);
        }
        
        if (left == K || right == K) {
            res.add(root.val);
        }
        
        if (root == target) {
            return 1;
        }
        
        // repropogate the dist in the other subtree
        if (left > 0) {
            traverse(root.right, target, left+1, K, res);
            return left + 1;
        }
        else if (right > 0) {
            traverse(root.left, target, right+1, K, res);
            return right+1;
        }
        return 0;
    }
}