/** 
Given a binary tree, find the maximum path sum.

For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree 
along the parent-child connections. 
The path does not need to go through the root.

For example:
Given the below binary tree,

       1
      / \
     2   3
Return 6.

Example: 
        -10
       /   \
      9    20
         /   \
       15      7

null node => 0
非空节点的最大贡献值等于节点值与其子节点中的最大贡献值之和（对于叶节点而言，最大贡献值等于节点值）。
*/


/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {

    private int maxVal;
    public int maxPathSum(TreeNode root) {
        this.maxVal = Integer.MIN_VALUE;
        maxSum(root);
        return maxVal;
    }
    
    private int maxSum(TreeNode node) {
        if (node == null) {
            return 0;
        }
         // 只有在最大贡献值大于 0 时，才会选取对应子节点
        int leftMax = Math.max(0, maxSum(node.left));
        int rightMax = Math.max(0, maxSum(node.right));
        
        // 当前最大的贡献值是left + right + 自己
        maxVal = Math.max(maxVal, leftMax + rightMax + node.val);
        // 只能选左边或者右边 + 上自己的node
        return Math.max(leftMax, rightMax) + node.val;
    }

    /**
        If this question changes to be the max Path Sum includes root node:
    */
    public int maxPathSumWithRoot(TreeNode root) {
        return helper(root);
    }

    private int helper(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int leftMax = Math.max(0, helper(node.left));
        int rightMax = Math.max(0, helper(node.right));

        int val = leftMax + rightMax + node.val;
        return val;
    }
}