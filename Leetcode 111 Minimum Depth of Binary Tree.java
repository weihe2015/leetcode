/**
Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    // Recursive
    /**
     * Basic Idea:
     *   When there is left or right child, add 1 to depth and continue
     *   When it is a leaf, return current depth
     *   When it is a full leaf, which has both left and right child, return its smaller depth of left and right child.
    */
    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return minDepth(root, 1);
    }

    private int minDepth(TreeNode node, int height) {
        if (node.left == null && node.right == null) {
            return height;
        }
        else if (node.left != null && node.right == null) {
            return minDepth(node.left, height+1);
        }
        else if (node.left == null && node.right != null) {
            return minDepth(node.right, height+1);
        }
        else {
            int leftDepth = minDepth(node.left, height+1);
            int rightDepth = minDepth(node.right, height+1);
            return Math.min(leftDepth, rightDepth);
        }
    }
    // Iterative: Use level order Traversal method.
    /**
     * Basic Idea: Use level order Traversal.
     * Whenever it reaches a leaf, return current depth as result.
     *
    */
    public int minDepth(TreeNode root) {
        int depth = 0;
        if (root == null) {
            return depth;
        }
        // use level order traversal:
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            depth++;
            int size = queue.size();
            for (int i = 1; i <= size; i++) {
                TreeNode curr = queue.poll();
                // Once we meet a leaf, we return the height.
                if (curr.left == null && curr.right == null) {
                    return depth;
                }
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
        }
        // Should not reach here, since we return the result on line 65:
        return -1;
    }
}