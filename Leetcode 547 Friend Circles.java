/**
There are N students in a class. Some of them are friends, while some are not. Their friendship is transitive in nature. 
For example, if A is a direct friend of B, and B is a direct friend of C, then A is an indirect friend of C. 
And we defined a friend circle is a group of students who are direct or indirect friends.

Given a N*N matrix M representing the friend relationship between students in the class. If M[i][j] = 1, 
then the ith and jth students are direct friends with each other, otherwise not. 
And you have to output the total number of friend circles among all the students.

Example 1:
Input: 
[[1,1,0],
 [1,1,0],
 [0,0,1]]
Output: 2
Explanation:The 0th and 1st students are direct friends, so they are in a friend circle. 
The 2nd student himself is in a friend circle. So return 2.
Example 2:
Input: 
[[1,1,0],
 [1,1,1],
 [0,1,1]]
Output: 1
Explanation:The 0th and 1st students are direct friends, the 1st and 2nd students are direct friends, 
so the 0th and 2nd students are indirect friends. All of them are in the same friend circle, so return 1.
Note:
N is in range [1,200].
M[i][i] = 1 for all students.
If M[i][j] = 1, then M[j][i] = 1.
*/
public class Solution {
    // Running Time Complexity: O(N + N*N*logN)
    public int findCircleNum(int[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        UnionFind uf = new UnionFind(n);
        for (int i = 0; i < m; i++) {
            for (int j = i+1; j < n; j++) {
                if (grid[i][j] == 1) {
                    uf.union(i, j);
                }
            }
        }
        return uf.count;
    }
    
    class UnionFind {
        int[] id;
        int[] size;
        int count;
        public UnionFind(int N) {
            this.id = new int[N];
            this.size = new int[N];
            // All people are friend of themselves. So initialize count = N, where N is number of people.
            this.count = N;
            for (int i = 0; i < N; i++) {
                id[i] = i;
                size[i] = 1;
            }
        }
        
        public int find(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }
        
        public void union(int p, int q) {
            int pRoot = find(p);
            int qRoot = find(q);
            if (pRoot == qRoot) {
                return;
            }
            if (size[pRoot] < size[qRoot]) {
                id[pRoot] = qRoot;
                size[qRoot] += size[pRoot];
            }
            else {
                id[qRoot] = pRoot;
                size[pRoot] += size[qRoot];
            }
            // If there is one union of friend, we decrement the number of friends.
            count--;
        }
    }

    // Solution 2: Use DFS:
    public int findCircleNum(int[][] isConnected) {
        int n = isConnected.length;
        
        int count = 0;
        boolean[] visited = new boolean[n];
        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                dfs(isConnected, i, n, visited);
                count++;
            }
        }
        return count;
    }
    
    private void dfs(int[][] isConnected, int idx, int n, boolean[] visited) {
        if (visited[idx]) {
            return;
        }
        visited[idx] = true;
        for (int i = 0; i < n; i++) {
            if (idx == i || isConnected[idx][i] == 0) {
                continue;
            }
            dfs(isConnected, i, n, visited);
        }
    }
}