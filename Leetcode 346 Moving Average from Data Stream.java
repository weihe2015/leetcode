/**
Given a stream of integers and a window size, calculate the moving average of all integers in the sliding window.

For example,
MovingAverage m = new MovingAverage(3);
m.next(1) = 1
m.next(10) = (1 + 10) / 2
m.next(3) = (1 + 10 + 3) / 3
m.next(5) = (10 + 3 + 5) / 3
*/
public class MovingAverage {

    Queue<Integer> queue;
    int size;

    public MovingAverage(int size){
        queue = new Linkedlist<>();
        this.size = size;
    }
    public double next(int val) {
        queue.offer(val);
        if (queue.size() > size) {
            queue.poll();
        }
        int sum = 0;
        for (int num : q) {
            sum += num;
        }
        return (double) sum / size;
    }

    // my solution
    private double sum;
    private Queue<Integer> q;
    private int size;

    public MovingAverage_346(int size){
        q = new LinkedList<>();
        sum = 0;
        this.size = size;
    }

    public double next(int val) {
        if (q.size() == this.size) {
            sum -= q.poll();
        }
        q.offer(val);
        this.sum += val;
        return (double) sum / q.size();
    }
}