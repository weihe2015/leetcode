/**
A binary matrix means that all elements are 0 or 1. For each individual row of the matrix, this row is sorted in non-decreasing order.

Given a row-sorted binary matrix binaryMatrix, return leftmost column index(0-indexed) with at least a 1 in it. If such index doesn't exist, return -1.

You can't access the Binary Matrix directly.  You may only access the matrix using a BinaryMatrix interface:

BinaryMatrix.get(row, col) returns the element of the matrix at index (row, col) (0-indexed).
BinaryMatrix.dimensions() returns a list of 2 elements [rows, cols], which means the matrix is rows * cols.
Submissions making more than 1000 calls to BinaryMatrix.get will be judged Wrong Answer.  Also, any solutions that attempt to circumvent the judge will result in disqualification.

For custom testing purposes you're given the binary matrix mat as input in the following four examples. You will not have access the binary matrix directly.

Example 1:
Input: mat = [[0,0],
              [1,1]]
Output: 0

Example 2:
Input: mat = [[0,0],
              [0,1]]
Output: 1

Example 3:
Input: mat = [[0,0],
              [0,0]]
Output: -1

Example 4:
Input: mat = [[0,0,0,1],
              [0,0,1,1],
              [0,1,1,1]]
Output: 1

Constraints:

rows == mat.length
cols == mat[i].length
1 <= rows, cols <= 100
mat[i][j] is either 0 or 1.
mat[i] is sorted in a non-decreasing way.
*/
public class Solution {
    
    /**
    Points:
    1. Reduce number of calls to binaryMatrix.get() call
    2. Every row is sorted. we start from the right up most cell.
       If cell is 0, we can go down,

    Ex:
       0 0 0 0 0
       0 0 0 1 1
       0 0 1 1 1

    1. (0,4)
    2. (1,4)
    3. (1,3)
    4. (1,2)
    5. (1,1)
    
    */
    // Running Time: O(M * N)
    public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        List<Integer> dimension = binaryMatrix.dimensions();
        int m = dimension.get(0);
        int n = dimension.get(1);

        int i = 0;
        int j = n-1;
        int idx = -1;
        while (i < m && j >= 0) {
            int res = binaryMatrix.get(i, j);
            if (res == 0) {
                i++;
            }
            else {
                idx = j;
                j--;
            }
        }
        return idx;
    }

    // Running Time: O(M * logN)
    private int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        List<Integer> dimension = binaryMatrix.dimensions();
        int m = dimension.get(0);
        int n = dimension.get(1);

        int idx = n;
        for (int i = 0; i < m; i++) {
            int low = 0;
            int high = idx;
            while (low < high) {
                int mid = low + (high - low) / 2;
                int res = binaryMatrix.get(i, mid);
                if (res == 1) {
                    high = mid;
                    idx = mid;
                }
                else {
                    low = mid + 1;
                }
            }
        }
        return idx == n ? -1 : idx;
    }
}
