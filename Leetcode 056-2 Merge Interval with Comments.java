/**
Given a set of inputs which represents [from, to, comment] in google docs.
Transform the input with overlapping offsets & unique comments to non overlapping offsets and duplicate comments.

Ex1 :
Input:
(0, 3): A
(2, 4): B
(5, 6): C

Output:
(0, 2): [A]
(2, 3): [A, B]
(3, 4): [B]
(5, 6): [C]

Ex2:
Input:
(0, 3): A
(0, 3): B
(2, 4): C
(5, 6): D

Output:
(0, 2): [A, B]
(2, 3): [A, B, C]
(3, 4): [C]
(5, 6): [D]
*/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class Solution {

    static private final String OPEN = "OPEN";
    static private final String CLOSE = "CLOSE";

    class Interval {
        int start;
        int end;
        List<String> names;
        public Interval(int start, int end, List<String> names) {
            this.start = start;
            this.end = end;
            this.names = names;
        }
    }

    class Event {
        int time;
        String type;
        String name;
        public Event(int time, String type, String name) {
            this.time = time;
            this.type = type;
            this.name = name;
        }
    }

    public List<Interval> mergeIntervalWithComments(List<String> contents) {
        // minQueue with time sorting
        Queue<Event> pq = new PriorityQueue<>(Comparator.comparingInt(e -> e.time));
        for (String content : contents) {
            String[] subContents = content.split(", ");
            String name = subContents[0];
            int start = Integer.parseInt(subContents[1]);
            int end = Integer.parseInt(subContents[2]);
            pq.offer(new Event(start, OPEN, name));
            pq.offer(new Event(end, CLOSE, name));
        }

        List<Interval> res = new ArrayList<>();
        // store all names currently
        Set<String> set = new LinkedHashSet<>();
        int lastTimestamp = -1;
        while (!pq.isEmpty()) {
            Event event = pq.poll();
            if (event.type.equals(OPEN)) {
                // set is not empty, meaning previous event is not overlap with current event:
                if (!set.isEmpty() && event.time != last) {
                    List<String> names = new ArrayList<>(set);
                    res.add(new Interval(last, event.time, names));
                }
                set.add(event.name);
            }
            // If event is close:
            else {
                // we need to check if the event.time > last
                // if it is, we need to add both of them into new interval
                if (event.time > last) {
                    List<String> names = new ArrayList<>();
                    res.add(new Interval(last, event.time, names));
                }
                else if (event.time < last) {
                    Interval prevInterval = res.get(res.size()-1);
                    prevInterval.names.add(event.name);
                }
                set.remove(event.name);
            }
            last = event.time;
        }
        return res;
    }

    private void print(List<Interval> intervals) {
        for (Interval interval : intervals) {
            StringBuffer sb = new StringBuffer();
            for (String name : interval.names) {
                sb.append(name + ",");
            }
            sb.setLength(sb.length()-1);
            System.out.println("(" + interval.start + "," + interval.end + "):[" + sb.toString() + "]");
        }
    }

    @Test
    public void test1() {
        List<String> strs = Arrays.asList("A, 0, 3", "B, 2, 4", "C, 5, 6");
        List<Interval> intervals = mergeIntervalWithComment(strs);
        print(intervals);
    }

    @Test
    public void test2() {
        List<String> strs = Arrays.asList("A, 0, 3", "B, 0, 3", "C, 2, 4", "D, 5, 6");
        List<Interval> intervals = mergeIntervalWithComment(strs);
        print(intervals);
    }

    @Test
    public void test3() {
        List<String> strs = Arrays.asList("Alice, 1, 5", "Bob, 2, 4", "Carol, 3, 5");
        List<Interval> intervals = mergeIntervalWithComment(strs);
        print(intervals);
    }
}

