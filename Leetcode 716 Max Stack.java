/**
 * Design a max stack that supports push, pop, top, peekMax and popMax.
push(x) -- Push element x onto stack.
pop() -- Remove the element on top of the stack and return it.
top() -- Get the element on the top.
peekMax() -- Retrieve the maximum element in the stack.
popMax() -- Retrieve the maximum element in the stack, and remove it. 
If you find more than one maximum elements, only remove the top-most one.
Ex:
MaxStack stack = new MaxStack();
stack.push(5); 
stack.push(1);
stack.push(5);
stack.top(); -> 5
stack.popMax(); -> 5
stack.top(); -> 1
stack.peekMax(); -> 5
stack.pop(); -> 1
stack.top(); -> 5
 * 
 * 
*/
public class MaxStack {
    private int maxNum;
    private Stack<Integer> stack;
    
    public MaxStack() {
        this.maxNum = Integer.MIN_VALUE;
        stack = new Stack<Integer>();
    }

    public void push(int value) {
        if (value >= maxNum) {
            stack.push(maxNum);
            maxNum = value;
        }
        stack.push(value);
    }

    public int pop() {
        int val = stack.pop();
        if (val == maxNum) {
            maxNum = stack.pop();
        }
        if (stack.isEmpty()) {
            maxNum = Integer.MIN_VALUE;
        }
        return val;
    }

    public int top() {
        return stack.peek();
    }

    public int peekMax() {
        return this.maxNum;
    }

    // Pop all item into a temp stack, until it reaches the max Item.
    // After that, push all items in temp stack back to 
    public int popMax() {
    	int res = maxNum;
    	Stack<Integer> temp = new Stack<Integer>();
    	while (!stack.isEmpty()) {
    		int val = stack.pop();
    		if (val != maxNum) {
    			temp.push(val);
    		}
    		else {
    			maxNum = stack.pop();
    			break;
    		}
    	}
    	while (!temp.isEmpty()) {
            int val = temp.pop();
            stack.push(val);
    	}
    	if (stack.isEmpty()) {
    		maxNum = Integer.MIN_VALUE;
    	}
        return res;
    }
}