/**
In an array A of 0s and 1s, how many non-empty subarrays have sum S?
Example 1:

Input: A = [1,0,1,0,1], S = 2
Output: 4
Explanation: 
The 4 subarrays are bolded below:
[1,0,1,0,1]
[1,0,1,0,1]
[1,0,1,0,1]
[1,0,1,0,1]
 

Note:

A.length <= 30000
0 <= S <= A.length
A[i] is either 0 or 1.
*/
public class Solution {
    public int numSubarraysWithSum(int[] A, int S) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        
        int res = 0;
        int sum = 0;
        for (int num : A) {
            sum += num;
            int sum_j = sum - S;
            if (map.containsKey(sum_j)) {
                res += map.get(sum_j);
            }
            int cnt = map.getOrDefault(sum, 0) + 1;
            map.put(sum, cnt);
        }
        return res;
    }
}