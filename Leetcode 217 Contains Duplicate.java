/**
Given an array of integers, find if the array contains any duplicates.

Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.

Example 1:

Input: [1,2,3,1]
Output: true
Example 2:

Input: [1,2,3,4]
Output: false
Example 3:

Input: [1,1,1,3,3,4,3,2,4,2]
Output: true
*/

public class Solution {
    // O(n^2) time, O(1) space
    public boolean containsDuplicate(int[] nums) {
        for (int i = 0, n = nums.length; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                if (nums[i] == nums[j]) {
                    return true;
                }
            }
        }
        return false;
    }

	// O(n) time, O(n) space
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (!set.contains(num)) {
                set.add(num);
            }
            else {
                return true;
            }
        }
        return false;
    }

    // O(nlogn) time, O(1) space
    public boolean containsDuplicate(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0, n = nums.length; i < n; i++) {
            if (i > 0 && nums[i] == nums[i-1]) {
                return true;
            }
        }
        return false;
    }
}