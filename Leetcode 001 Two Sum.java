/**
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
*/
public class Solution {
    // Running Time Complextiy: O(N^2) time, N = length of nums.
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        for(int i = 0, n = nums.length; i < n-1; i++){
            for(int j = i+1; j < n; j++){
                int ans = nums[i] + nums[j];
                if(ans == target){
                    result[0] = i;
                    result[1] = j;
                    break;
                }
            }
        }
        return result;
    }
    // O(n) time, O(n) space
    public int[] twoSum(int[] nums, int target){
        // key --> nums[i], value -> index
        Map<Integer, Integer> indexMap = new HashMap<>();
        int[] res = new int[2];
        if (nums == null || nums.length <= 1) {
            return res;
        }
        for (int i = 0, n = nums.length; i < n; i++){
            int diff = target - nums[i];
            if (map.containsKey(diff)){
                res[0] = indexMap.get(diff);
                res[1] = i;
                break;
            }
            indexMap.put(nums[i],i);
        }
        return res;
    }

}