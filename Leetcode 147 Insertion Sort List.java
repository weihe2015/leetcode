Sort a linked list using insertion sort.

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode insertionSortList(ListNode head) {
        if(head == null)
            return head;
        ListNode new_head = new ListNode(0);
        while(head != null){
            ListNode curr = new_head;
            while(curr.next != null && curr.next.val < head.val){
                curr = curr.next;
            }
            ListNode next = head.next;
            head.next = curr.next;
            curr.next = head;
            head = next;
        }
        return new_head.next;
    }
}