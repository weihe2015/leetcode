/**
Given an unsorted array of integers, find the length of longest continuous increasing subsequence (subarray).

Example 1:
Input: [1,3,5,4,7]
Output: 3
Explanation: The longest continuous increasing subsequence is [1,3,5], its length is 3. 
Even though [1,3,5,7] is also an increasing subsequence, it's not a continuous one where 5 and 7 are separated by 4. 
Example 2:
Input: [2,2,2,2,2]
Output: 1
Explanation: The longest continuous increasing subsequence is [2], its length is 1. 
*/
public class Solution {
    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int findLengthOfLCIS(int[] nums) {
        int n = nums.length;
        if (n == 0) {
            return 0;
        }
        int maxLen = 1;
        int count = 1;
        for (int i = 1; i < n; i++) {
            if (nums[i] > nums[i-1]) {
                count++;
                maxLen = Math.max(count, maxLen);
            }
            else {
                count = 1;
            }
        }
        maxLen = Math.max(count, maxLen);
        return maxLen;
    }

    // Running Time Complexity: O(N), Space Complexity: O(1)
    public int findLengthOfLCIS(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int maxLen = 1, n = nums.length;
        int[] dp = new int[n];
        dp[0] = 1;
        for (int i = 1; i < n; i++) {
            if (nums[i] > nums[i-1]) {
                dp[i] = dp[i-1] + 1;
                maxLen = Math.max(maxLen, dp[i]);
            }
            else {
                dp[i] = 1;
            }
        }
        return maxLen;
    }
}