/**
下一个全排列
implement next permutation, 
which rearranges numbers into the lexicographically 
next greater permutation of numbers.

If such arrangement is not possible, 
it must rearrange it as the lowest possible order 
(ie, sorted in ascending order).


1,2,3 → 1,3,2
3,2,1 → 1,2,3
1,1,5 → 1,5,1

ex: 6 8 7 4 3 2
1. find 6, which breaks the backward increasing thrend
2. find 7, which is the element larger than 6 in the right
3. swap 6 and 7
4. reverse everything to the 7

Explanation Video: https://www.youtube.com/watch?v=quAS1iydq7U

*/
public class Solution {
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public void nextPermutation(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        int n = nums.length;
        // K is the position where it breaks the decreasing order of subarray.
        // The decreasing order of array means that there is no possibility to explore next permutation.
        int k = -1;
        for (int i = n-1; i > 0; i--){
            if (nums[i-1] < nums[i]){
                k = i-1;
                break;
            }
        }
        // if all array is decesending, reverse all array
        if (k == -1){
            reverse(nums, 0, n-1);
            return;
        }
        // find the smallest element to the right of k
        // that is larger than nums[k]
        int l = -1;
        for (int i = n-1; i > k; i--){
            if (nums[i] > nums[k]){
                l = i;
                break;
            }
        }
        // swap nums[k] and nums[l]
        swap(nums, k, l);
        // reverse array of k+1 to the end.
        reverse(nums, k+1, n-1);
    }
    
    private void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
    
    private void reverse(int[] nums, int i, int j){
        while(i < j){
            swap(nums, i, j);
            i++;
            j--;
        }
    }
}