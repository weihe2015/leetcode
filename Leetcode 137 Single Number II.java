/**
Given a non-empty array of integers, every element appears three times except for one, which appears exactly once. Find that single one.

Note:

Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

Example 1:

Input: [2,2,3,2]
Output: 3
Example 2:

Input: [0,1,0,1,0,1,99]
Output: 99
*/

public class Solution {
    public int singleNumber(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int result = 0;
        int[] bits = new int[32];
        for(int i = 0; i < 32; i++){
            for(int j = 0; j < nums.length; j++){
                bits[i] += nums[j] >> i & 1;
                bits[i] %= 3;
            }
            result |= (bits[i] << i);
        }
        return result;
    }
    // solution 2
    // 00, 01, 10,  first bit, ones, second bit, twos
     public int singleNumber(int[] nums) {
         int ones = 0, twos = 0;
         for(int num : nums){
             ones = (ones ^ num) & ~twos;
             twos = (twos ^ num) & ~ones;
         }
         return ones;
     }
    // with extra memory
    // Running Time Complexity: O(n), Space Complexity: O(n)
    public int singleNumber(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (int key : map.keySet()) {
            if (map.get(key) == 1) {
                return key;
            }
        }
        return -1;
    }
}