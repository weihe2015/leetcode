/**
Return the length of the shortest, non-empty, contiguous subarray of A with sum at least K.

If there is no non-empty subarray with sum at least K, return -1.

Example 1:

Input: A = [1], K = 1
Output: 1
Example 2:

Input: A = [1,2], K = 4
Output: -1
Example 3:

Input: A = [2,-1,2], K = 3
Output: 3
 

Note:

1 <= A.length <= 50000
-10 ^ 5 <= A[i] <= 10 ^ 5
1 <= K <= 10 ^ 9
*/
public class Solution {

    // TreeMap solution:
    /**
    A = [84,-37,32,40,95]
    K = 167
    res = 3
    */
    public int shortestSubarray(int[] A, int K) {
        int n = A.length;
        int res = Integer.MAX_VALUE;
        int sum = 0;
        TreeMap<Integer, Integer> sumMap = new TreeMap<>();
        sumMap.put(0, -1);
        for (int i = 0; i < n; i++) {
            sum += A[i];
            int diff = sum - K;
            // 假设存在sums[i] >= sum[j], 并且 i < j 选择i不如选择j，因此i就没有意义了，可以舍弃
            Map.Entry<Integer, Integer> entry = sumMap.floorEntry(diff);
            if (entry != null) {
                res = Math.min(res, i-entry.getValue());
            }
            while (!sumMap.isEmpty() && sumMap.lastKey() > sum) {
                sumMap.pollLastEntry();
            }
            sumMap.put(sum, i);
        }
        return res == Integer.MAX_VALUE ? -1 : res;
    }

    // 如果数组中的数据均为非负数的话，那么就对应常规的子数组和问题，可以使用滑动窗口来解决。
    // 但是添加了负数之后，窗口的滑动便丢失了单向性，因此无法使用滑动窗口解决。这样的话子数组之和就不会随着长度的增加而增加
    public int shortestSubarray(int[] A, int K) {
        int n = A.length;
        int res = n+1;
        int[] sums = new int[n+1];
        // Use array sums represents prefix sum of A
        // where sums[i] = A[0] + ... + A[i-1]
        // we would like to find a pair (x,y), where sums[y]-sums[x] >= k and y-x is the minimal
        for (int i = 1; i <= n; i++) {
            sums[i] = sums[i-1] + A[i-1];
        }
        // monotonic queue: with index x0, x1, ..., xr, where sums[x0] <= sums[x1] <= sums[x2] ...
        // when we have a new index x', we will remove last elements to make sure it is increasing.
        // Meanwhile, we will remove head elements if sums[x'] - sums[x0] >= K
        Deque<Integer> dq = new ArrayDeque<>();
        for (int i = 0; i <= n; i++) {
            while (!dq.isEmpty() && sums[i] - sums[dq.getFirst()] >= K) {
                res = Math.min(res, i-dq.getFirst());
                dq.pollFirst();
            }
            // To keep sum[D[i]] increasing in the deque.
            while (!dq.isEmpty() && sums[i] <= sums[dq.getLast()]) {
                dq.pollLast();
            }
            dq.addLast(i);
        }
        return res == n+1 ? -1 : res;
    }
}