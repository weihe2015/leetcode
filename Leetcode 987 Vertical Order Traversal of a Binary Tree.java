/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    /**
    Given a binary tree, return the vertical order traversal of its nodes values.

    For each node at position (X, Y), its left and right children respectively will be at positions (X-1, Y-1) and (X+1, Y-1).

    Running a vertical line from X = -infinity to X = +infinity,

    whenever the vertical line touches some nodes, we report the values of the nodes in order from top to bottom (decreasing Y coordinates).

    If two nodes have the same position, then the value of the node that is reported first is the value that is smaller.

    Return an list of non-empty reports in order of X coordinate.  Every report will have a list of values of nodes.
    */
    class Point {
        int x;
        int y;
        TreeNode node;
        public Point(int x, int y, TreeNode node) {
            this.x = x;
            this.y = y;
            this.node = node;
        }
    }
    
    public List<List<Integer>> verticalTraversal(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        
        List<Point> points = new ArrayList<>();
        inorderTraversal(root, points, 0, 0);
        
        Collections.sort(points, (p1, p2) -> {
            if (p1.x != p2.x) {
                return Integer.compare(p1.x, p2.x);
            }
            // In case if points have same x, the point with larger y will be in front
            else if (p1.y != p2.y) {
                return Integer.compare(p2.y, p1.y);
            }
            // In case if points have same x and same y, the point with smaller val will be in front
            return Integer.compare(p1.node.val, p2.node.val);
        });
        
        // key -> x value, val -> list of values with this x value
        Map<Integer, List<Integer>> treeMap = new TreeMap<>();
        for (Point p : points) {
            List<Integer> list = treeMap.getOrDefault(p.x, new ArrayList<>());
            list.add(p.node.val);
            treeMap.put(p.x, list);
        }
        
        for (List<Integer> list : treeMap.values()) {
            res.add(list);
        }
        
        return res;
    }
    
    private void inorderTraversal(TreeNode root, List<Point> points, int x, int y) {
        if (root == null) {
            return;
        }
        inorderTraversal(root.left, points, x-1, y-1);
        points.add(new Point(x, y, root));
        inorderTraversal(root.right, points, x+1, y-1);
    }
}