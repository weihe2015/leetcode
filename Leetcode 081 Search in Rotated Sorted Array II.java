/*
Follow up for "Search in Rotated Sorted Array":
What if duplicates are allowed?
(i.e., [0,0,1,2,2,5,6] might become [2,5,6,0,0,1,2]).

You are given a target value to search. If found in the array return true, otherwise return false.

Example 1:

Input: nums = [2,5,6,0,0,1,2], target = 0
Output: true
Example 2:

Input: nums = [2,5,6,0,0,1,2], target = 3
Output: false

Would this affect the run-time complexity? How and why?
Write a function to determine if a given target is in the array.

  *  Yes, the run-time complexity will be O(n), because there may be a case
     where most items are duplicate and nums[low]==nums[mid], so that we need to move low pointer one by one.
      [1,1,1,3,1,1,1,1,1], find 3,
  * When we meet duplicates, just move the low/right pointer.

*/
public class Solution {
    // Runtime complexity: O(n) worst case
    // use mid and high to determine which part of subarray is sorted
    public boolean search(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return false;
        }
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low)/2;
            if (nums[mid] == target) {
                return true;
            }
            // if right side array is sorted
            if (nums[mid] < nums[high]) {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
            // if left side array is sorted
            else if (nums[mid] > nums[high]) {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
            // if it is duplicated, move high back one step
            // Ex: [3,1,1], target = 3. low = 0, high = 2, mid = 1.
            // If we don't decrement high pointer but jump into the second condition,
            // low = mid + 1 and this target will not be found.
            else {
                high--;
            }
        }
        return false;
    }
    // Same Runtime complexity: O(n) worst case
    // use low and mid to determine which part of subarray is sorted
    public boolean search(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return false;
        }
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return true;
            }
            // if left side array is sorted
            if (nums[low] < nums[mid]) {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                }
                else {
                    low = mid + 1;
                }
            }
            // if right side array is sorted.
            else if (nums[low] > nums[mid]) {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                }
                else {
                    high = mid - 1;
                }
            }
            // meet duplicate item, move low to one step right.
            else {
                low++;
            }
        }
        return false;
    }
}