/**
Given four lists A, B, C, D of integer values, compute how many tuples (i, j, k, l) there are such that A[i] + B[j] + C[k] + D[l] is zero.

To make problem a bit easier, all A, B, C, D have same length of N where 0 ≤ N ≤ 500. All integers are in the range of -228 to 228 - 1 and the result is guaranteed to be at most 231 - 1.

Example:

Input:
A = [ 1, 2]
B = [-2,-1]
C = [-1, 2]
D = [ 0, 2]

Output:
2

Explanation:
The two tuples are:
1. (0, 0, 0, 1) -> A[0] + B[0] + C[0] + D[1] = 1 + (-2) + (-1) + 2 = 0
2. (1, 1, 0, 0) -> A[1] + B[1] + C[0] + D[0] = 2 + (-1) + (-1) + 0 = 0
*/
public class Solution {
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        int count = 0;
        Arrays.sort(A);
        Arrays.sort(B);
        Arrays.sort(C);
        Arrays.sort(D);
        for (int i = 0, nA = A.length; i < nA; i++) {
            for (int j = 0, nB = B.length; j < nB; j++) {
                for (int k = 0, nC= C.length; k < nC; k++) {
                    for (int l = 0, nD = D.length; l < nD; l++) {
                        int sum = A[i] + B[j] + C[k] + D[l];
                        if (sum == 0) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    // Running Time Complexity: O(N^2)
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        int count = 0, target = 0;
        // Key -> sum of A[i] and B[j], Value -> number of o
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, n = A.length; i < n; i++) {
            for (int j = 0, m = B.length; j < m; j++) {
                int sum = target - (A[i] + B[j]);
                map.put(sum, map.getOrDefault(sum, 0) + 1);
            }
        }
        
        for (int i = 0, n = C.length; i < n; i++) {
            for (int j = 0, m = D.length; j < m; j++) {
                int sum = C[i] + D[j];
                if (map.containsKey(sum)) {
                    count += map.get(sum);
                }
            }
        }
        return count;
    }
}
}