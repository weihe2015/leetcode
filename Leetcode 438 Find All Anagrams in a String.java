/**
Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.

Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.

The order of output does not matter.

Example 1:

Input:
s: "cbaebabacd" p: "abc"

Output:
[0, 6]

Explanation:
The substring with start index = 0 is "cba", which is an anagram of "abc".
The substring with start index = 6 is "bac", which is an anagram of "abc".
Example 2:

Input:
s: "abab" p: "ab"

Output:
[0, 1, 2]

Explanation:
The substring with start index = 0 is "ab", which is an anagram of "ab".
The substring with start index = 1 is "ba", which is an anagram of "ab".
The substring with start index = 2 is "ab", which is an anagram of "ab".
*/
public class Solution {
    // Brute Force Solution:
    // Running Time Complexity: O(N^2 * (2p))
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return res;
        }
        int n = s.length();
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                String str = s.substring(i, j+1);
                if (isAnagram(str, p)) {
                    res.add(i);
                    break;
                }
            }
        }
        return res;
    }
    
    private boolean isAnagram(String s1, String s2) {
        if (s1.length() != s2.length()) {
            return false;
        }
        int[] charMap = new int[256];
        for (char c : s1.toCharArray()) {
            charMap[c]++;
        }
        
        for (char c : s2.toCharArray()) {
            charMap[c]--;
            if (charMap[c] < 0) {
                return false;
            }
        }
        return true;
    }

    // Brute Force Solution: O(m * (2p)) 
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> result = new ArrayList<Integer>();
        int m = s.length(), n = p.length();
        for (int i = 0; i <= m-n; i++) {
            String substr = s.substring(i, i+n);
            if (isAnagram(substr, p)) {
                result.add(i);
            }
        }
        return result;
    }
    
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] cList = new int[256];
        for (char c : s.toCharArray()) {
            cList[c]++;
        }
        for (char c : t.toCharArray()) {
            cList[c]--;
            if (cList[c] < 0) {
                return false;
            }
        }
        return true;
    }

    // Use Sliding Window Solution: 
    // Running Time Complexity: O(n), Space Complexity: O(1)
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> result = new ArrayList<>();
        int[] map = new int[256];
        for (char c : p.toCharArray()) {
            map[c]++;
        }
        int count = p.length();
        int l = 0;
        int r = 0;
        int n = s.length();
        int m = p.length();

        while (r < n) {
            // substring(l, r) does not contains all characters in string p, move r pointer to right
            if (count > 0) {
                char rc = s.charAt(r);
                map[rc]--;
                if (map[rc] >= 0) {
                    count--;
                }
                r++;
            }
            // substring(l, r) contains all characters in string t:
            while (count == 0) {
                char lc = s.charAt(l);
                // If lc is in string p, then we increment count.
                map[lc]++;
                if (map[lc] >= 1) {
                    count++;
                    // If the window size is same as p, an anagram is found
                    if (r - l == m) {
                        result.add(l);
                    }
                }
                l++;
            }
        }
        return result;
    }
}