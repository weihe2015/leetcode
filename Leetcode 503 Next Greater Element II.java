/**
Given a circular array (the next element of the last element is the first element of the array), 
print the Next Greater Number for every element. The Next Greater Number of a number x is the first greater number to its traversing-order next in the array, 
which means you could search circularly to find its next greater number. If it doesn't exist, output -1 for this number.

Example 1:
Input: [1,2,1]
Output: [2,-1,2]
Explanation: The first 1's next greater number is 2; 
The number 2 can't find next greater number; 
The second 1's next greater number needs to search circularly, which is also 2.
Note: The length of given array won't exceed 10000.
*/
public class Solution {
    // Monotonic Stack: Run time Complexity: O(N)
    public int[] nextGreaterElements(int[] nums) {
        int n = nums.length;
        int[] res = new int[n];
        
        // double the array:
        // [2,1,2,4,3,2,1,2,4,3]
        Stack<Integer> stack = new Stack<>();
        for (int i = 2 * n -1; i >= 0; i--) {
            int idx = i % n;
            int num = nums[idx];
            while (!stack.isEmpty() && stack.peek() <= num) {
                stack.pop();
            }
            int max = stack.isEmpty() ? -1 : stack.peek();
            res[idx] = max;
            stack.push(num);
        }
        
        return res;
    }

    // Bruth Force:
    // Running Time Complexity: O(N^2)
    public int[] nextGreaterElements(int[] nums) {
        int n = nums.length;
        int[] res = new int[n];
        Arrays.fill(res, -1);
        
        // [2,1,2,4,3]
        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < i+n; j++) {
                int idx = j % n;
                if (nums[idx] > nums[i]) {
                    res[i] = nums[idx];
                    break;
                }
            }
        }
        
        return res;
    }
}