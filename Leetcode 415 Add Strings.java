/*
Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.

Note:

The length of both num1 and num2 is < 5100.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library or convert the inputs to integer directly.
*/

public class Solution {
	// Running Time Complexity: O((m + n) * Math.max(m,n)), Space Complexity: O(1)
    public String addStrings(String num1, String num2) {
        StringBuffer sb = new StringBuffer();
        int m = num1.length(), n = num2.length(), i = m-1, j = n-1;
        int sum = 0;
        while (i >= 0 || j >= 0) {
            if (i >= 0) {
                char c1 = num1.charAt(i);
                sum += c1 - '0';
                i--;
            }
            if (j >= 0) {
                char c2 = num2.charAt(j);
                sum += c2 - '0';
                j--;
            }
            sb.insert(0, sum % 10);
            sum /= 10;
        }
        if (sum == 1) {
            sb.insert(0, 1);
        }
        return sb.toString();
    }
}
