/**
Input:Digit string "23"
Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
*/
public class Solution {
    // Backtracking Methods, Running Time Complexity: T(n) = k * T(n-1). T(n) = values.length() ^ digits.length(), Space Complexity O(n)
    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<String>();
        if (digits == null || digits.length() == 0) {
            return result;
        }
        Map<Character, String> map = new HashMap<Character, String>();
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
        letterCombinations(result, digits, map, 0, new StringBuffer());
        return result;
    }
    
    private void letterCombinations(List<String> result, String digits, Map<Character, String> map, int level, StringBuffer sb) {
        // ensure that only same length of digits string will be produced
        if (sb.length() == digits.length()) {
            result.add(sb.toString());
        }
        for (int i = level, max = digits.length(); i < max; i++) {
            char num = digits.charAt(i);
            String str = map.get(num);
            for (char c : str.toCharArray()) {
                sb.append(c);
                letterCombinations(result, digits, map, i+1, sb);
                sb.setLength(sb.length()-1);
                // sb.setLength: modify the count and overwrites the unwarted value in the array with a zero byte
                // sb.deleteCharAt(sb.length()-1) : perform an array copy internally, before altering the count.
            }
        }
    }

    // Iterative method:
    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<String>();
        if (digits == null || digits.length() == 0) {
            return result;
        }
        Map<Character, String> map = new HashMap<Character, String>();
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
        for (char c : digits.toCharArray()) {
            String str = map.get(c);
            if (result.isEmpty()) {
                for (char cc : str.toCharArray()) {
                    result.add(String.valueOf(cc));
                }
            }
            // copy each item in result for str.length() times.
            else {
                List<String> temp = new ArrayList<>();
                for (String subStr : result) {
                    for (int i = 0, max = str.length(); i < max; i++) {
                        StringBuffer sb = new StringBuffer(subStr);
                        sb.append(str.charAt(i));
                        temp.add(sb.toString());
                    }
                }
                result = temp;
            } 
        }
        return result;
    }
}