/**
There is a ball in a maze with empty spaces and walls. 
The ball can go through empty spaces by rolling up (u), down (d), left (l) or right (r), but it won't stop rolling until hitting a wall. 
When the ball stops, it could choose the next direction. There is also a hole in this maze. The ball will drop into the hole if it rolls on to the hole.

Given the ball position, the hole position and the maze, find out how the ball could drop into the hole by moving the shortest distance. 
The distance is defined by the number of empty spaces traveled by the ball from the start position (excluded) to the hole (included). 
Output the moving directions by using 'u', 'd', 'l' and 'r'. Since there could be several different shortest ways, you should output the lexicographically smallest way. If the ball cannot reach the hole, output "impossible".

The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. You may assume that the borders of the maze are all walls. The ball and the hole coordinates are represented by row and column indexes.

Example 1:

Input 1: a maze represented by a 2D array

0 0 0 0 0
1 1 0 0 1
0 0 0 0 0
0 1 0 0 1
0 1 0 0 0

Input 2: ball coordinate (rowBall, colBall) = (4, 3)
Input 3: hole coordinate (rowHole, colHole) = (0, 1)

Output: "lul"

Explanation: There are two shortest ways for the ball to drop into the hole.
The first way is left -> up -> left, represented by "lul".
The second way is up -> left, represented by 'ul'.
Both ways have shortest distance 6, but the first way is lexicographically smaller because 'l' < 'u'. So the output is "lul".

Example 2:

Input 1: a maze represented by a 2D array

0 0 0 0 0
1 1 0 0 1
0 0 0 0 0
0 1 0 0 1
0 1 0 0 0

Input 2: ball coordinate (rowBall, colBall) = (4, 3)
Input 3: hole coordinate (rowHole, colHole) = (3, 0)

Output: "impossible"

Explanation: The ball cannot reach the hole.
*/
public class Solution {

    static private final int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
    static private final String[] dirChars = {"r","l","d","u"};

    class Info {
        int dist;
        String path;
        public Info(int dist, String path) {
            this.dist = dist;
            this.path = path;
        }
    }

    public String findShortestWay(int[][] maze, int[] ball, int[] hole) {
        int m = maze.length;
        int n = maze[0].length;
        Info[][] infos = new Info[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                infos[i][j] = new Info(Integer.MAX_VALUE, "");
            }
        }
        int ex = hole[0];
        int ey = hole[1];

        int sx = ball[0];
        int sy = ball[1];
        infos[sx][sy].dist = 0;
        infos[sx][sy].path = "";

        Queue<int[]> queue = new LinkedList<>();
        queue.offer(ball);
        while (!queue.isEmpty()) {
            int[] curr = queue.poll();
            int i = curr[0];
            int j = curr[1];

            for (int k = 0; k < 4; k++) {
                int[] dir = dirs[k];
                int x = i;
                int y = j;
                int dist = infos[x][y].dist;
                String path = infos[x][y].path;
                while (x >= 0 && x < m && y >= 0 && y < n && maze[x][y] == 0 && (x != ex || y != ey)) {
                    x += dir[0];
                    y += dir[1];
                    dist++;
                }
                // 如果是洞口，不需要做回滚动作
                if (x != ex || y != ey) {
                    x -= dir[0];
                    y -= dir[1];
                    dist--;
                }

                String newPath = path + dirChars[k];
                if (isUpdateable(dist, infos, x, y, newPath)) {
                    infos[x][y].dist = dist;
                    infos[x][y].path = newPath;
                    if (x != ex || y != ey) {
                        queue.offer(new int[]{x, y});
                    }
                }
            }
        }

        if (infos[ex][ey].dist == Integer.MAX_VALUE) {
            return "impossible";
        }
        return infos[ex][ey].path;
    }

    private boolean isUpdateable(int dist, Info[][] infos, int x, int y, String newPath) {
        return dist < infos[x][y].dist || (dist == infos[x][y].dist && infos[x][y].path.compareTo(newPath) > 0);
    }
}

