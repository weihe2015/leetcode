/**
We have a list of points on the plane.  Find the K closest points to the origin (0, 0).

(Here, the distance between two points on a plane is the Euclidean distance.)

You may return the answer in any order.  The answer is guaranteed to be unique 
(except for the order that it is in.)

Example 1:

Input: points = [[1,3],[-2,2]], K = 1
Output: [[-2,2]]
Explanation: 
The distance between (1, 3) and the origin is sqrt(10).
The distance between (-2, 2) and the origin is sqrt(8).
Since sqrt(8) < sqrt(10), (-2, 2) is closer to the origin.
We only want the closest K = 1 points from the origin, so the answer is just [[-2,2]].
Example 2:

Input: points = [[3,3],[5,-1],[-2,4]], K = 2
Output: [[3,3],[-2,4]]
(The answer [[-2,4],[3,3]] would also be accepted.)

*/
class Solution {
    // Solution 1: use sorting:
    public int[][] kClosest(int[][] points, int K) {
        int[] originPoint = new int[]{0, 0};
        
        Arrays.sort(points, (p1, p2) -> Integer.compare(distance(p1, originPoint), 
                                                        distance(p2, originPoint)));
        
        int[][] res = new int[K][2];
        for (int i = 0; i < K; i++) {
            res[i] = points[i];
        }
        return res;
    }
    
    private int distance(int[] p1, int[] p2) {
        int x = p1[0] - p2[0];
        int y = p1[1] - p2[1];
        
        return x * x + y * y;
    }

    // Solution 2: Use Point Class
    class Point {
        int x;
        int y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public int[][] kClosest(int[][] points, int K) {
        Point originPt = new Point(0, 0);
        // minHeap
        Queue<Point> pq = new PriorityQueue<>((p1, p2) -> 
                                              Integer.compare(distance(p1, originPt), 
                                                              distance(p2, originPt)));
        
        for (int[] point : points) {
            int x = point[0];
            int y = point[1];
            Point p = new Point(x, y);
            pq.offer(p);
        }
        
        List<Point> list = new ArrayList<>();
        while (!pq.isEmpty() && list.size() < K) {
            list.add(pq.poll());
        }

        int n = list.size();
        int[][] res = new int[n][2];
        for (int i = 0; i < n; i++) {
            Point p = list.get(i);
            res[i][0] = p.x;
            res[i][1] = p.y;
        }
        
        return res;
    }
    
    private int distance(Point p1, Point p2) {
        int x = p1.x - p2.x;
        int y = p1.y - p2.y;
        return (x * x + y * y);
    }
}