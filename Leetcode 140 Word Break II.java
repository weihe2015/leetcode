/**
Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, add spaces in s to construct a sentence
where each word is a valid dictionary word.
Return all such possible sentences.

Note:

The same word in the dictionary may be reused multiple times in the segmentation.
You may assume the dictionary does not contain duplicate words.
Example 1:

Input:
s = "catsanddog"
wordDict = ["cat", "cats", "and", "sand", "dog"]
Output:
[
  "cats and dog",
  "cat sand dog"
]
Example 2:

Input:
s = "pineapplepenapple"
wordDict = ["apple", "pen", "applepen", "pine", "pineapple"]
Output:
[
  "pine apple pen apple",
  "pineapple pen apple",
  "pine applepen apple"
]
Explanation: Note that you are allowed to reuse a dictionary word.
Example 3:

Input:
s = "catsandog"
wordDict = ["cats", "dog", "sand", "and", "cat"]
Output:
[]
*/

public class Solution {
    // Backtracking and memorization:
    // Running Time Complexity: O(N!)
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> result = new ArrayList<>();
        if (s.length() == 0) {
            return result;
        }
        Set<String> set = new HashSet<>(wordDict);
        Map<String, List<String>> map = new HashMap<>();
        return wordBreak(s, set, map);
    }

    private List<String> wordBreak(String s, Set<String> wordDict, Map<String, List<String>> map) {
        // To reduce the time when s already iterated
        if (map.containsKey(s)) {
            return map.get(s);
        }
        List<String> result = new ArrayList<>();
        // when s is the leading words like cats or cat, it will be in the list:
        // It cannot return because if s = pineapple, and wordDict has pine, apple, it will need to continue to scan:
        if (wordDict.contains(s)) {
            result.add(s);
        }
        int n = s.length();
        for (int i = n-1; i >= 1; i--) {
            String currStr = s.substring(i);
            if (wordDict.contains(currStr)) {
                String prevStr = s.substring(0, i);
                List<String> strs = wordBreak(prevStr, wordDict, map);
                for (String str : strs) {
                    StringBuffer sb = new StringBuffer(str);
                    sb.append(" ");
                    sb.append(currStr);
                    result.add(sb.toString());
                }
            }
        }
        map.put(s, result);
        return result;
    }

    // Solution 2, by iterating wordSet:
    private List<String> wordBreak(String s, Set<String> wordSet, Map<String, List<String>> map) {
        if (map.containsKey(s)) {
            return map.get(s);
        }
        List<String> res = new ArrayList<>();
        // when s is the leading words like cats or cat, it will be in the list:
        // It cannot return because if s = pineapple, and wordDict has pine, apple, it will need to continue to scan:
        if (wordSet.contains(s)) {
            res.add(s);
        }
        for (String word : wordSet) {
            if (!s.startsWith(word)) {
                continue;
            }
            int n = word.length();
            String substr = s.substring(n);
            List<String> subList = wordBreak(substr, wordSet, map);
            for (String str : subList) {
                StringBuffer sb = new StringBuffer();
                sb.append(word);
                sb.append(" ");
                sb.append(str);
                res.add(sb.toString());
            }
        }

        map.put(s, res);
        return res;
    }

    // DFS and backtracking, build Graph and DFS from starting point 0 to n, and print all paths from it.
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> res = new ArrayList<>();
        int n = s.length();
        boolean[] dp = new boolean[n+1];
        dp[0] = true;

        Set<String> set = new HashSet<>(wordDict);
        Map<Integer, List<Integer>> map = new HashMap<>();
        // construct graph:
        for (int i = 1; i <= n; i++) {
            for (int j = i-1; j >= 0; j--) {
                String substr = s.substring(j, i);
                if (dp[j] && set.contains(substr)) {
                    dp[i] = true;
                    List<Integer> list = map.getOrDefault(i, new ArrayList<>());
                    list.add(j);
                    map.put(i, list);
                }
            }
        }
        if (!dp[n]) {
            return res;
        }

        // DFS to generate all paths from 0 to n:
        dfs(s, res, map, new StringBuffer(), n, n);
        return res;
    }

    private void dfs(String s, List<String> result, Map<Integer, List<Integer>> map, StringBuffer sb, int index, int n) {
        if (index == 0) {
            // create a new StringBuffer because StringBuffer is mutable
            StringBuffer res = new StringBuffer(sb);
            // remove leading space
            res.deleteCharAt(0);
            result.add(res.toString());
            return;
        }
        List<Integer> prevIndex = map.get(index);
        for (int i : prevIndex) {
            String substr = s.substring(i, index);
            sb.insert(0, substr);
            sb.insert(0, " ");
            dfs(s, result, map, sb, i, n);
            // remove previous leading result
            sb.delete(0, substr.length()+1);
        }
    }
}