/**
Given two integer arrays A and B, return the maximum length of an subarray that appears in both arrays.

Example 1:
Input:
A: [1,2,3,2,1]
B: [3,2,1,4,7]
Output: 3
Explanation:
The repeated subarray with maximum length is [3, 2, 1].
Note:
1 <= len(A), len(B) <= 1000
0 <= A[i], B[i] < 100
*/
public class Solution {
    // Bruth Force Solution:
    // Running Time Complexity: O(m * n * min(m, n)), Space Complexity: O(1)
    public int findLength(int[] A, int[] B) {
        if (A == null || A.length == 0 || B == null || B.length == 0) {
            return 0;
        }
        int maxLen = 0;
        for (int i = 0, m = A.length; i < m; i++) {
            for (int j = 0, n = B.length; j < n; j++) {
                int k = 0;
                while (i + k < m && j + k < n) {
                    if (A[i+k] != B[j+k]) {
                        break;
                    }
                    k++;
                }
                maxLen = Math.max(maxLen, k);
            }
        }
        return maxLen;
    }
    // DP Solution: Longest Common Substring:
    // Running Time Complexity: O(m * n), Space Complexity: O(m * n)
    // https://leetcode-cn.com/problems/maximum-length-of-repeated-subarray/solution/zhe-yao-jie-shi-ken-ding-jiu-dong-liao-by-hyj8/
    public int findLength(int[] A, int[] B) {
        int m = A.length;
        int n = B.length;
        if (m == 0 || n == 0) {
            return 0;
        }
        int maxLen = 0;
        // 长度为i，末尾项为A[i-1]的子数组，与，长度为j，末尾项为B[j-1]的子数组，二者的最大公共后缀子数组长度。
        int[][] dp = new int[m+1][n+1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (A[i-1] == B[j-1]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                    maxLen = Math.max(maxLen, dp[i][j]);
                }
                else {
                    dp[i][j] = 0;
                }
            }
        }
        return maxLen;
    }

    // Running Time Complexity: O(m * n), Space Complexity: O(n)
    public int findLength(int[] A, int[] B) {
        int m = A.length;
        int n = B.length;
        if (m == 0 || n == 0) {
            return 0;
        }
        int[] dp = new int[n+1];
        int maxLen = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = n; j >= 1; j--) {
                if (A[i-1] == B[j-1]) {
                    dp[j] = dp[j-1] + 1;
                    maxLen = Math.max(maxLen, dp[j]);
                }
                else {
                    dp[j] = 0;
                }
            }
        }
        return maxLen;
    }
}