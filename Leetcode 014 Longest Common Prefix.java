public class Solution {
    // Running Time Complexity: O(l * N), where l is the shortest string length, 
    // N is the number of strings in the str array
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        int n = strs.length;
        int min = Integer.MAX_VALUE;
        // find out the shortest string
        for (String str : strs) {
            if (str.length() < min) {
                min = str.length();
            }
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < min; i++) {
            char c = strs[0].charAt(i);
            for (int j = 1; j < n; j++) {
                if (strs[j].charAt(j) != c) {
                    return sb.toString();
                }
            }
            sb.append(c);
        }
        return sb.toString();
    }
    
    // solution 2
    public String longestCommonPrefix(String[] strs) {
        if(strs == null || strs.length == 0){
            return "";
        }   
        String pre = strs[0];
        int i = 1;
        while (i < strs.length){
            while (strs[i].indexOf(pre) != 0) {
                pre = pre.substring(0,pre.length()-1);
            }
            i++;
        }
        return pre;
    }

    // solution 3: Use Trie data structure to solve it
    // Running time: build Trie: O(S) , where s is the number of all characters in the array
    // find: O(m) where m is the string length of first string.
    public String longestCommonPrefix(String[] strs) {
        int n = strs.length;
        if (n == 0) {
            return "";
        }
        Trie trie = new Trie();
        trie.insert(strs);
        return trie.findLongestCommonPrefix(strs[0], n);
    }
    
    class Trie {
        private TrieNode root;
        public Trie() {
            this.root = new TrieNode();
        }
        
        public void insert(String[] strs) {
            for (String str : strs) {
                insert(str);
            }
        }
        
        public void insert(String str) {
            TrieNode curr = root;
            for (char c : str.toCharArray()) {
                if (curr.next[c-'a'] == null) {
                    curr.next[c-'a'] = new TrieNode();
                }
                curr = curr.next[c-'a'];
                curr.count++;
            }
        }
        
        public String findLongestCommonPrefix(String str, int n) {
            StringBuffer sb = new StringBuffer();
            TrieNode curr = root;
            for (char c : str.toCharArray()) {
                curr = curr.next[c-'a'];
                if (curr.size != n) {
                    break;
                }
                sb.append(c);
            }
            return sb.toString();
        }
    }
    
    class TrieNode {
        private int size;
        private TrieNode[] next;
        public TrieNode() {
            this.size = 0;
            this.next = new TrieNode[26];
        }
    }
}