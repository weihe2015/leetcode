/**
Given a positive integer num, write a function which returns True if num is a perfect square else False.

Follow up: Do not use any built-in library function such as sqrt.

Example 1:

Input: num = 16
Output: true
Example 2:

Input: num = 14
Output: false

Constraints:

1 <= num <= 2^31 - 1
*/
public class Solution {
    /**
    This is a math problem：
    1 = 1
    4 = 1 + 3
    9 = 1 + 3 + 5
    16 = 1 + 3 + 5 + 7
    25 = 1 + 3 + 5 + 7 + 9
    36 = 1 + 3 + 5 + 7 + 9 + 11
    ....
    so 1+3+...+(2n-1) = (2n-1 + 1)n/2 = n*n
    */
    // Running time Complexity: O(sqrt(n))
    public boolean isPerfectSquare(int num) {
        // a square number is 1 + 3 + 5 + 7...
        int i = 1;
        while (num > 0) {
            num -= i;
            i += 2;
        }
        return num == 0;
    }

    // Binary Search:
    public boolean isPerfectSquare(int num) {
        int low = 1, high = num;
        while (low <= high) {
            long mid = low + (high - low) / 2;
            long val = mid * mid;
            if (val == num) {
                return true;
            }
            else if (val < num) {
                low = (int) mid + 1;
            }
            else {
                high = (int) mid - 1;
            }
        }
        return false;
    }
}