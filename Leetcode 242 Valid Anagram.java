/*
Given two strings s and t, write a function to determine if t is an anagram of s.

For example,
s = "anagram", t = "nagaram", return true.
s = "rat", t = "car", return false.
*/

public class Solution {
    public boolean isAnagram(String s, String t) {
        if(s.length() != t.length())
            return false;
        int[] charList = new int[26];
        for(char c : s.toCharArray()){
            charList[c - 'a']++;
        }
        for(char c : t.toCharArray()){
            charList[c - 'a']--;
            if(charList[c - 'a'] < 0)
                return false;
        }
        return true;
    }

    // Follow up, contains unicode characters
    public boolean isAnagram(String s, String t) {
        if(s.length() != t.length())
            return false;
        int[] cList = new int[256];
        for(char c : s.toCharArray()){
            cList[c]++;
        }
        for(char c : t.toCharArray()){
            if(--cList[c] < 0)
                return false;
        }
        return true;
    }
}