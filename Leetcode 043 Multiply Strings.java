/*
Given two non-negative integers num1 and num2 represented as strings, 
return the product of num1 and num2, also represented as a string.

Example 1:

Input: num1 = "2", num2 = "3"
Output: "6"
Example 2:

Input: num1 = "123", num2 = "456"
Output: "56088"
Note:

The length of both num1 and num2 is < 110.
Both num1 and num2 contain only digits 0-9.
Both num1 and num2 do not contain any leading zero, except the number 0 itself.
You must not use any built-in BigInteger library or convert the inputs to integer directly.
*/
public class Solution {
    public String multiply(String num1, String num2) {
        if (num1 == null || num2 == null) {
            return "";
        }
        int m = num1.length();
        int n = num2.length();
        int[] res = new int[m+n];
        for (int i = m-1; i >= 0; i--) {
            int n1 = num1.charAt(i) - '0';
            for (int j = n-1; j >= 0; j--) {
                int n2 = num2.charAt(j) - '0';
                int sum = n1 * n2 + res[i+j+1];
                res[i+j+1] = sum % 10;
                res[i+j] += sum / 10;
            }
        }
        // skip leading 0:
        int i = 0;
        while (i < m+n && res[i] == 0) {
            i++;
        }
        if (i == m+n) {
            return "0";
        }
        // append int array into string:
        StringBuffer sb = new StringBuffer();
        while (i < m+n) {
            sb.append(res[i]);
            i++;
        }
        return sb.toString();
        // StringBuffer sb = new StringBuffer();
        // for (int num : res) {
        //     if (num != 0 || sb.length() > 0) {
        //         sb.append(num);
        //     }
        // }
        // return sb.length() == 0 ? "0" : sb.toString();
    }
}