/**
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
*/
public class Solution {
    // Solution 1: Running Time Complexity: O(N), Space Complexity: O(1)
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int max = Integer.MIN_VALUE, sum = 0;
        // keep adding each integer to the sequence until the sum drops below 0.
        // If sum is negative, then should reset the sequence.
        for (int num : nums) {
            sum += num;
            max = Math.max(max, sum);
            sum = Math.max(sum, 0);
        }
        return max;
    }

    // Solution 2: DP:
    // maxArray[i] = maxArray[i-1] > 0 ? (maxArray[i-1] + nums[i]) : nums[i]
    // Time Complexity: O(N), Space Complexity: O(N)
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        int max = nums[0];
        // dp[i]: largest sum of (0...i):
        int[] dp = new int[n];
        dp[0] = nums[0];
        for (int i = 1; i < n; i++) {
            if (dp[i-1] > 0) {
                dp[i] = dp[i-1] + nums[i];
            }
            else {
                dp[i] = nums[i];
            }
            // or : dp[i] = Math.max(nums[i], dp[i-1] + nums[i]);
            max = Math.max(max, dp[i]);
        }
        return max;
    }
    // Time Complexity: O(N), Space Complexity: O(1)
    public int maxSubArray(int[] nums) {
        int n = nums.length;
        int dp_0 = nums[0];
        int max = nums[0];
        for (int i = 1; i < n; i++) {
            int dp_i;
            if (dp_0 > 0) {
                dp_i = dp_0 + nums[i];
            }
            else {
                dp_i = nums[i];
            }
            dp_0 = dp_i;
            max = Math.max(max, dp_i);
        }
        return max;
    }

    // Solution 3: Divide and Conquer
    /*
    1) Divide the given array in two halves
    2) Return the maximum of following three
        a) Maximum subarray sum in left half (Make a recursive call)
        b) Maximum subarray sum in right half (Make a recursive call)
        c) Maximum subarray sum such that the subarray crosses the midpoint
    */
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        return maxSubArray(nums, 0, nums.length-1);
    }

    private int maxSubArray(int[] nums, int l, int r) {
         // Base Case: Only one element
        if (l == r) {
            return nums[l];
        }
        int m = l + (r - l) / 2;
        /* Return maximum of following three
        possible cases:
        a) Maximum subarray sum in left half
        b) Maximum subarray sum in right half
        c) Maximum subarray sum such that the
        subarray crosses the midpoint */
        int val = Math.max(maxSubArray(nums, l, m), maxSubArray(nums, m+1, r));
        return Math.max(val, maxCrossSum(nums, l, m, r));
    }

    private int maxCrossSum(int[] nums, int l, int m, int r) {
        int sum = 0;
        int leftSum = Integer.MIN_VALUE;
        for (int i = m; i >= l; i--) {
            sum += nums[i];
            leftSum = Math.max(leftSum, sum);
        }

        // Include elements on right of mid:
        sum = 0;
        int rightSum = Integer.MIN_VALUE;
        for (int i = m+1; i <= r; i++) {
            sum += nums[i];
            rightSum = Math.max(rightSum, sum);
        }
        // Return sum of elements on left and right of mid
        return leftSum + rightSum;
    }

    // Sliding Window solution:
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int r = 0;
        int n = nums.length;
        int maxSum = Integer.MIN_VALUE;
        int sum = 0;
        while (r < n) {
            sum += nums[r];
            maxSum = Math.max(maxSum, sum);
            // the numbers within window l and r cannot be the largest number:
            if (sum <= 0) {
                sum = 0;
            }
            r++;
        }
        return maxSum;
    }

    // sliding window solution:
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int l = 0, r = 0, n = nums.length, max = Integer.MIN_VALUE, sum = 0;
        int start = -1, end = -1;
        while (r < n) {
            sum += nums[r];
            if (sum > max) {
            	max = sum;
            	end = r;
            }
            // the numbers within window l and r cannot be the largest number:
            if (sum <= 0) {
                l = r;
                start = r+1;
                sum = 0;
            }
            r++;
        }
        return max;
    }

    // other solutions:
    public int maxSubArray(int[] nums){
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int max = Integer.MIN_VALUE;
        int sum = 0;
        int minSum = 0;
        for (int num : nums) {
            sum += num;
            max = Math.max(max, sum-minSum);
            minSum = Math.min(minSum, sum);
        }
        return max;
    }

    public int maxSubArray(int[] nums){
        int max = nums[0];
        int sum = nums[0];
        for (int i = 1; i < nums.length; i++) {
            sum = Math.max(sum+nums[i], nums[i]);
            max = Math.max(sum, max);
        }
        return max;
    }
}