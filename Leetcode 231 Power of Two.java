Given an integer, write a function to determine if it is a power of two.

public class Solution {
    // use loop
    public boolean isPowerOfTwo(int n) {
        if(n == 0)
            return false;
        while(n % 2 == 0)
            n /= 2;
        return n == 1;        
    }
    // use math
    Power of 2 means only one bit of n is '1', 
    so use the trick n&(n-1)==0 to judge whether that is the case
    public boolean isPowerOfTwo(int n){
        return ((n & (n-1)) == 0 && n > 0);
    }
}