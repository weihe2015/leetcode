/**
Design and implement a data structure for Least Frequently Used (LFU) cache. It should support the following operations: get and put.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present. When the cache reaches its capacity, it should invalidate the least frequently used item before inserting a new item. For the purpose of this problem, when there is a tie (i.e., two or more keys that have the same frequency), the least recently used key would be evicted.

Follow up:
Could you do both operations in O(1) time complexity?

Example:

LFUCache cache = new LFUCache( 2 -- capacity  );

cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // returns 1
cache.put(3, 3);    // evicts key 2
cache.get(2);       // returns -1 (not found)
cache.get(3);       // returns 3.
cache.put(4, 4);    // evicts key 1.
cache.get(1);       // returns -1 (not found)
cache.get(3);       // returns 3
cache.get(4);       // returns 4
*/
/**
    Solution of using PriorityQueue minHeap as Data Structure:
    Use a Node structure containing key, value, number of frequency, and created time stamp.
        So in PriorityQueue, if two nodes have same the frequency, then we compare their timestamp and rank higher for the one with earlier time stamp
    
    The same as LRU: Least Recently Use Cache:
    GET:
        If this key is not in the map, 
            we return -1 as not found.
        If this key is in the map:
            We get this node from map, remove it from minHeap, update its frequency by increment 1 and update its timestap to current timestamp.
              Increment the global timestamp
    PUT:
        If this key is not in the map:
            If we still have capacity:
                decrement the global variable capacity by 1, create a new node with new key, value, freq = 1, and current timestamp, put it into map and minHeap
            If we don't have capacity:
                Pop the top Node from minHeap. This node will have least frequency number and earliest timestamp
                    create a new node with new key, value, freq = 1, and current timestamp, put it into map and minHeap
        If this key is in the map:
            Get it from map by key, remove it from minHeap.
                update its value to current value, update its frequency by incrementing 1, and update timestap to current timestamp.
             Then add it back to minHeap and HashMap.

    Running Time Complexity: GET O(logN), PUT: O(logN)
*/
class LFUCache {
    class Node {
        long stamp;
        int key, value;
        int freq;
        public Node(int key, int value, int freq, long stamp) {
            this.key = key;
            this.value = value;
            this.freq = freq;
            this.stamp = stamp;
        }
    }
    // Key -> Object's key, Value -> Node
    Map<Integer, Node> map;
    long stamp;
    int capacity;
    Queue<Node> minHeap;
    public LFUCache(int capacity) {
        this.capacity = capacity;
        this.stamp = 0;
        this.map = new HashMap<Integer, Node>();
        this.minHeap = new PriorityQueue<Node>((o1, o2) -> (o1.freq == o2.freq ? (int)(o1.stamp - o2.stamp) : (o1.freq - o2.freq)));
    }
    
    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        else {
            Node node = map.get(key);
            minHeap.remove(node);
            int val = node.value;
            // update the time stamp and its frequency
            node.freq++;
            node.stamp = stamp;

            // update the node in map:
            map.put(key, node);
            // update the node in minHeap:
            minHeap.offer(node);
            stamp++;
            return val;
        }
    }
    
    public void put(int key, int value) {
        Node node = null;
        // If this node is not in the map, we create a new one.
        if (!map.containsKey(key)) {
            if (capacity > 0) {
                capacity--;
            }
            else {
                if (minHeap.isEmpty()) {
                    return;
                }
                Node prevNode = minHeap.poll();
                map.remove(prevNode.key);
            }
            node = new Node(key, value, 1, stamp);
        }
        // If this node is already in the map, we update the value, freq and stamp:
        else {
            node = map.get(key);
            minHeap.remove(node);
            node.freq++;
            node.stamp = stamp;
            node.value = value;
        }
        stamp++;
        // Add it to the minHeap:
        minHeap.offer(node);
        // Add it to the map
        map.put(key, node);
    }
}

/** 
    Optimial Solution: PUT: O(1), GET: O(1)
    Use two HashMap, one stores the key and the Node, the other stores the frequency as key and double linked List as value:
    DoubleLinkedList:
        Head: the most recently used. Tail: The least Recently Used.
*/
class LFUCache {
	class Node {
	    int key;
        int value; 
        int freq;
	    Node prev;
        Node next;
	    public Node(){
	        	
	    }
	    public Node(int key, int value) {
	        this.key = key;
	        this.value = value;
	        freq = 1;
	    }
	}
	    
	class DoubleLinkedList {
	    Node head, tail;
	    int size;
	    public DoubleLinkedList() {
	        head = new Node();
	        tail = new Node();
	        head.next = tail;
	        tail.prev = head;
	    }
	        
	    public void addNode(Node node) {
	        head.next.prev = node;
	        node.next = head.next;
	        node.prev = head;
	        head.next = node;
	        size++;
	    }
	        
	    public void removeNode(Node node) {
	        node.prev.next = node.next;
	        node.next.prev = node.prev;
	        size--;
	    }
	        
	    public Node removeLastNode() {
	        Node node = tail.prev;
	        removeNode(node);
	        return node;
	    }
	}
    
    private int size;
    private int minFreq;
    private int capacity;
    // Key -> Object's key, Value: Node
    private Map<Integer, Node> nodeMap;
    // Key -> Freqency, Value: Double LinkedList:
    private Map<Integer, DoubleLinkedList> freqMap;
    public LFUCache(int capacity) {
        this.capacity = capacity;
        this.nodeMap = new HashMap<Integer, Node>();
        this.freqMap = new HashMap<Integer, DoubleLinkedList>();
    }
    
    public int get(int key) {
        if (!nodeMap.containsKey(key)) {
            return -1;
        }
        else {
            Node node = nodeMap.get(key);
            updateNode(node);
            return node.value;
        }
    }
    
    public void put(int key, int value) {
        if (capacity == 0) {
            return;
        }
        // If this key is not in the map:
        if (!nodeMap.containsKey(key)) {
            // If we still have space in the cache:
            if (size < capacity) {
                size++;
            }
            // If the capacity is full: we need to move the least recently used Node from minFreq
            else {
                // remove the least frequent item from the nodeMap:
                DoubleLinkedList oldList = freqMap.get(minFreq);
                Node prevNode = oldList.removeLastNode();
                nodeMap.remove(prevNode.key);
            }
             // reset minFreq to 1
            minFreq = 1;
            // create a new Node and add it into the nodeMap.
            Node node = new Node(key, value);
            // Add it into the doubleLinkedList of frequency = 1
            DoubleLinkedList newList = freqMap.getOrDefault(node.freq, new DoubleLinkedList());
            newList.addNode(node);
            freqMap.put(node.freq, newList);
            nodeMap.put(key, node);
        }
        else {
            Node node = nodeMap.get(key);
            node.value = value;
            updateNode(node);
        }
    }
    
    private void updateNode(Node node) {
        // Remove itself from old freq DoubleLinkedList
        DoubleLinkedList oldList = freqMap.get(node.freq);
        oldList.removeNode(node);
        if (oldList.size == 0 && node.freq == minFreq) {
            minFreq++;
        }
        node.freq++;
        DoubleLinkedList newList = freqMap.getOrDefault(node.freq, new DoubleLinkedList());
        newList.addNode(node);
        freqMap.put(node.freq, newList);
    }
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache obj = new LFUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */