/*
Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).
For example,
S = "ADOBECODEBANC"
T = "ABC"
Minimum window is "BANC".
Note:
If there is no such window in S that covers all characters in T, return the empty string "".
If there are multiple such windows, you are guaranteed that there will always be only one unique minimum window in S.
*/
public class Solution {
    public String minWindow(String s, String t) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int[] map = new int[256];
        for (char c : t.toCharArray()) {
            map[c]++;
        }
        int count = t.length(), l = 0, r = 0, n = s.length();
        int start = -1, end = -1;
        while (r < n) {
            // substring(l,r) does not contains all characters in string t:
            if (count > 0) {
                char rc = s.charAt(r);
                map[rc]--;
                // If this character is in string t and have not been included before, reduce the counter.
                if (map[rc] >= 0) {
                    count--;
                }

                r++;
            }
            // substring(l, r) contains all characters in string t:
            while (count == 0) {
                // If it finds a shorter substring containing all characters in string t, update the start and end pointer
                if (start == -1 || (r - l < end - start)) {
                    start = l;
                    end = r;
                }
                char lc = s.charAt(l);
                map[lc]++;
                // left pointer meets characters in string t, increment counter.
                if (map[lc] >= 1) {
                    count++;
                }
                l++;
            }
        }
        // If no such substring found, return "":
        return start == -1 ? "" : s.substring(start, end);
    }

    public String minWindow(String s, String t) {
        if (s == null || s.length() == 0) {
            return "";
        }
        // Total number of characters in String t
        int tCount = t.length();
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for (char c : t.toCharArray()) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c)+1);
            }
            else {
                map.put(c, 1);
            }
        }
        int minLen  = Integer.MAX_VALUE, l = 0, r = 0, n = s.length();
        String res = "";
        while (r < n || tCount == 0) {
            if (tCount > 0) {
                char rc = s.charAt(r);
                if (map.containsKey(rc)) {
                    int occurances = map.get(rc);
                    if (occurances >= 1) {
                        tCount--;
                    }
                    map.put(rc, occurances-1);
                }
                r++;
            }
            else {
                int newLen = r - l + 1;
                if (newLen < minLen) {
                    minLen = Math.min(minLen, r - l + 1);
                    res = s.substring(l, r);
                }
                char lc = s.charAt(l);
                if (map.containsKey(lc)) {
                    int occurances = map.get(lc);
                    if (occurances >= 0) {
                        tCount++;
                    }
                    map.put(lc, occurances+1);
                }
                l++;
            }
        }
        return res;
    }

    public String minWindow(String s, String t) {
        if (s == null || s.length() == 0) {
            return "";
        }
        // Total number of characters in String t
        int tCount = t.length();
        int[] map = new int[256];
        for (char c : t.toCharArray()) {
            map[c]++;
        }
        int minLen  = Integer.MAX_VALUE, l = 0, r = 0, n = s.length();
        String res = "";
        while (r < n || tCount == 0) {
            if (tCount > 0) {
                char rc = s.charAt(r);
                if (map[rc] >= 1) {
                    tCount--;
                }
                map[rc]--;
                r++;
            }
            else {
                int newLen = r - l + 1;
                if (newLen < minLen) {
                    minLen = Math.min(minLen, r - l + 1);
                    res = s.substring(l, r);
                }
                char lc = s.charAt(l);
                if (map[lc] >= 0) {
                    tCount++;
                }
                map[lc]++;
                l++;
            }
        }
        return res;
    }
}